import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Alert, Col, Row, Table, Button, FormGroup, Label, Input, ButtonDropdown, DropdownItem,  DropdownMenu,  DropdownToggle } from 'reactstrap';
import { Link,NavLink } from 'react-router-dom';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';
import toaster from "toasted-notes";
import moment from 'moment';
import "toasted-notes/src/styles.css";
import * as router from 'react-router-dom';
import { Badge, UncontrolledDropdown, Nav, NavItem } from 'reactstrap';
import {AppBreadcrumb2 as AppBreadcrumb,AppSidebarNav2 as AppSidebarNav,} from '@coreui/react';
import { AppAsideToggler, AppSidebarToggler } from '@coreui/react';
import routes from './routes';
import axios from "axios";
import LoadingBar from 'react-top-loading-bar'
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
class Breadcrumbs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDisableSave:false,
      progress:0,
      invoicenumber:'',
      planid:0,
      desc:'',
      currency:'USD',
      quantity:1,
      planname:'',
      bankaccounts:[],
      subsdescshow:'',
      checkitem:'1',
      subs:[],
      paymentplanslist:[],
      price:'',
      time:1,
      invoiceitems:[],
      totalamount:0,
      billto:'',
      duedate:'',
      issuedate:'',
      lang:1,
      payementlisttoshow:[],
      planselected:'',
      bankaccount:'',
      notes:'',
      email:'',
      centerName:''
    };
    this.getnextinvoice();
    this.getbankaccount();
    this.getactivesub();
    this.getpaymentplans();
  }
  getAllAgents()
  {
    let token = localStorage.getItem('sessionToken')
    const url = 'http://tab-project.info/taboor-qms/adminpanel/agent/get/serviceCenter';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode===0)
      {
        
        for(var i=0;i<responseJson.data.agentList.length;i++)
        {
            responseJson.data.agentList[i].agent['isChecked']=false
            if(responseJson.data.agentList[i].agent.branch!=undefined)
            {
              this.signedAgents=this.signedAgents + 1
            }
            else if(responseJson.data.agentList[i].agent.branch!=undefined)
            {
              this.unsignedAgents=this.unsignedAgents + 1
            }
            this.tempAvialableAgents.push(responseJson.data.agentList[i])
        }
        
        this.totalAgents=this.tempAvialableAgents.length;
        this.setState({
          allAgents:responseJson.data.agentList,
         
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  changeAgentBranch(data)
  {
    for(var i=0;i<this.tempAvialableAgents.length;i++)
    {
      if(data.agent.agentId==this.tempAvialableAgents[i].agent.agentId)
      {
        if(this.tempAvialableAgents[i].agent.branch!=undefined)
        {
          toaster.notify(({ onClose }) => (
            <div className='assignedBranch'>
              <div style={{textAlign:'left'}}>
              <span>Agent cannot added to this Branch because he is already assigned to another Branch, Kindly contact Branch Admin to release agent.</span>
              </div>
              <div>
                <div style={{textAlign:'left'}}><span>Branch Details:</span></div>
                <div style={{textAlign:'left'}}><span>Name: {this.tempAvialableAgents[i].agent.branch.branchName}</span></div>
                <div style={{textAlign:'left'}}><span>Arabic Name: {this.tempAvialableAgents[i].agent.branch.branchNameArabic}</span></div>
                <div style={{textAlign:'left'}}><span>Email: {this.tempAvialableAgents[i].agent.branch.emailAddress}</span></div>
                <div style={{textAlign:'left'}} ><span>Phone Number: {this.tempAvialableAgents[i].agent.branch.phoneNumber}</span></div>
              </div>
            
              <button onClick={onClose} className='assigendBranchCross'><i className='font-lg font-weight-bold icon2-cancel'></i></button>
            </div>
          ),{
            duration:5000
          });
          this.tempAvialableAgents[i].agent['isChecked']=false
          return
        }
        else
        {
          if(this.tempAvialableAgents[i].agent['isChecked']==true)
          {
            this.tempAvialableAgents[i].agent['isChecked']=false
            
          }
          else
          {
            this.tempAvialableAgents[i].agent['isChecked']=true
          }
        }
      }
    }
    this.setState({allAgents:this.tempAvialableAgents})
  }
  search()
  {
    
    if(this.state.searchName!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempAvialableAgents.length;i++)
      {
        if(this.state.searchName==this.tempAvialableAgents[i].agent.serviceCenterEmployee.user.name)
        {
          data.push(this.tempAvialableAgents[i])
          this.setState({
            allAgents:data
          })
          isMatch=true
          this.totalAgents=data.length
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
          
        }
      }
      if(isMatch==false)
      {
        this.totalAgents=0
        this.setState({allAgents:[]})
      }
    }
    else
    {
      this.totalAgents=this.tempAvialableAgents.length
      this.setState({
        allAgents:this.tempAvialableAgents
      })
    }
  
  }
  changeSearch(agentName)
  {
    if(agentName=='')
    { 
      this.totalAgents=this.tempAvialableAgents.length
      this.setState({
        allAgents:this.tempAvialableAgents
      })
    }
    this.search(agentName)
    this.setState({
      searchName:agentName
      })
  }
  getnextinvoice=()=>{
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/invoice/getNextInvoiceNumber';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode===0)
      {
        console.log('==================getnextinvoice==================');
        console.log(responseJson.data);
        console.log('=====================getnextinvoice===============');
        this.setState({invoicenumber:responseJson.data.values.NextInvoiceNumber})
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  getbankaccount=()=>{
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/bankAccount/get/all';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode===0)
      {
        console.log('==============getbankaccount======================');
        console.log(responseJson.data);
        console.log('=================getbankaccount===================');
        this.setState({bankaccounts:responseJson.data.bankAccounts})
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  getactivesub=()=>{
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getActiveSubscriptionList';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode===0)
      {
        this.setState({subs:responseJson.data.subscriptions})
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  getpaymentplans=()=>{
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/paymentPlan/get/all';
    axios.get(url).then(responseJson => {
      console.log('===============paymentPlanList=====================');
      console.log(responseJson.data.paymentPlanList);
      console.log('=================paymentPlanList===================');
      if(responseJson.data.applicationStatusCode===0)
      {
        this.setState({paymentplanslist:responseJson.data.paymentPlanList});
        var temppaymentlist=[];
        for(var i=0;i<this.state.paymentplanslist.length;i++){
          for(var j=0;j<this.state.paymentplanslist[i].paymentPlan.prices.length;j++){
            if(this.state.paymentplanslist[i].paymentPlan.prices[j].currency==this.state.currency){
              temppaymentlist.push(this.state.paymentplanslist[i]);
            }
          }
      }
      this.setState({payementlisttoshow:temppaymentlist})
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  subname=(e)=>{
      if(e.target.value!=''){
        this.setState({subsdescshow:this.state.subs[e.target.value]});
        this.setState({billto:this.state.subs[e.target.value].subscriptionId})
        this.setState({email: this.state.subs[e.target.value].serviceCenter.email})
        
        this.setState({centerName: this.state.subs[e.target.value].serviceCenter.serviceCenterName})
      }
      else{
        this.setState({subsdescshow:''});
        this.setState({billto:''})
        this.setState({email: ''})
        this.setState({centerName: ''})
      }
  }
  itemsec=(e)=>{
      if(e.target.value!=''){
        this.setState({checkitem:e.target.value,desc:'',quantity:1,time:1,planid:0,price:''})
      }
      else{
        this.setState({checkitem:'',desc:'',quantity:1,time:1,planid:0,price:''})
      }
  }
  addplan=(e)=>{
    if(e.target.value!=''){
      this.setState({planselected:e.target.value})
      console.log('==============addplan value======================');
      console.log(e.target.value);
      console.log('===============addplan value=====================');
      for(var i=0;i<this.state.payementlisttoshow[e.target.value].paymentPlan.prices.length;i++){
        if(this.state.currency==this.state.payementlisttoshow[e.target.value].paymentPlan.prices[i].currency){
          this.setState({price:this.state.payementlisttoshow[e.target.value].paymentPlan.prices[i].priceMonthly});
          this.setState({pricetag:this.state.payementlisttoshow[e.target.value].paymentPlan.prices[i].priceMonthly});
        }
      }
      this.setState({quantity:1,time:1,desc:'',planid:this.state.payementlisttoshow[e.target.value].paymentPlan.paymentPlanId
      ,planname:this.state.payementlisttoshow[e.target.value].paymentPlan.planName})
    }
    else{
      this.setState({quantity:1,time:1,desc:'',planid:0})
      this.setState({price:'',pricetag:0});
      this.setState({planselected:''})
    }
  }
  addtime=(e)=>{
    this.setState({time:parseInt(e.target.value),price:parseInt(this.state.pricetag)*parseInt(e.target.value)});
  }
  addcurrency=(e)=>{
      this.setState({currency:e.target.value});
      this.setState({payementlisttoshow:[]})
      var temppaymentlist=[];
      for(var i=0;i<this.state.paymentplanslist.length;i++){
        for(var j=0;j<this.state.paymentplanslist[i].paymentPlan.prices.length;j++){
          if(this.state.paymentplanslist[i].paymentPlan.prices[j].currency==e.target.value){
            temppaymentlist.push(this.state.paymentplanslist[i]);
          }
        }
      }
      this.setState({payementlisttoshow:temppaymentlist});
      this.setState({desc:'',quantity:1,time:1,planid:0})
      this.setState({price:'',pricetag:0})
      this.setState({planselected:''})
  }
addfullserviceitem=()=>{
  var tempitem = {
    "description": this.state.checkitem=='1'?this.state.desc:this.state.planname,
    "itemType": this.state.checkitem=='1'?'SERVICE':'PRODUCT',
    "paymentPlanId": this.state.planid,
    "price": this.state.price,
    "quantity": this.state.quantity,
    "timeInMonths": this.state.time
  }
  var itemdataset=this.state.invoiceitems;
  itemdataset.push(tempitem);
  this.setState({invoiceitems:itemdataset});
  this.setState({checkitem:'1',planid:0,price:'',quantity:1,time:1,planname:'',desc:'',planselected:''})
  this.showtotal();
}
showsubtotal=()=>{
  if(this.state.invoiceitems.length!=0){
    var amount=0
    for(var i=0;i<this.state.invoiceitems.length;i++){
      amount = amount  + parseInt(this.state.invoiceitems[i].price)
    }
    return amount;
  }
  else{
    return 0
  }
}
showsubtotaltax=()=>{
  if(this.state.invoiceitems.length!=0){
    var amount=0
    for(var i=0;i<this.state.invoiceitems.length;i++){
      amount = amount  + parseInt(this.state.invoiceitems[i].price)
    }
    return amount*5/100;
  }
  else{
    return 0
  }
}
showtotal=()=>{
  if(this.state.invoiceitems.length!=0){
    var amount=0
    for(var i=0;i<this.state.invoiceitems.length;i++){
      amount = amount  + parseInt(this.state.invoiceitems[i].price)
    }
    // this.setState({totalamount:amount + amount*5/100});
    return amount + amount*5/100
  }
  else{
    return 0
    // this.setState({totalamount:0});
  }
}
saveinvoicetodb=(drafted)=>{
  
  let token = localStorage.getItem('sessionToken')
  var body = {
    "bankAccountId": this.state.bankaccount,
    "currency": this.state.currency,
    "draft": drafted,
    "dueOn": this.state.duedate,
    "expiredOn": this.state.duedate,
    "invoiceId": this.state.invoicenumber,
    "issuedOn": this.state.issuedate,
    "items": this.state.invoiceitems,
    "language": this.state.lang,
    "serviceCenterSubscriptionId": this.state.billto,
    "totalAmount": this.showtotal(),
    "updateStatus": 0,
    "email": this.state.email,
    "centerName":this.state.centerName,
    "notes":this.state.notes
  }
  console.log("THIS IS OF TYPE", typeof body.draft)
  const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/invoice/addOrUpdate';
  const header={headers: { 
    'Authorization': `Bearer ${token}`
  }} 
  if(this.state.bankaccount==''){
    toaster.notify('Bank Account is Required', {
      duration: 1000 
    });
  }
  else if(this.state.billto==''){
    toaster.notify('Bill To is Required', {
      duration: 1000 
    });
  }
  else if(this.state.issuedate==''){
    toaster.notify('Issue Date is Required', {
      duration: 1000 
    });
  }
  else if(this.state.duedate==''){
    toaster.notify('Due Date is Required', {
      duration: 1000 
    });
  }
  else{
    this.setState({
      progress:50,
      isDisableSave:true
    })
    axios.post(url,body,header).then(responseJson => {
      console.log(responseJson.data);
      if(responseJson.data.applicationStatusCode===0)
      {
        this.setState({
          progress:100,isDisableSave:false
        })
        this.props.history.push('/invoices')
      }
      else{
        this.setState({
          progress:100,isDisableSave:false
        })
        toaster.notify(responseJson.data.applicationStatusResponse, {
          duration: 1000 
        });
      }
    }).catch(err=>{
      console.log(err);
      toaster.notify('Network Error', {
        duration: 1000 
      });
    });
  }
  
}
deleteinvoiceitem=(i)=>{
  var array = [...this.state.invoiceitems]; // make a separate copy of the array
  var index = i
  if (index !== -1) {
    array.splice(index, 1);
    this.setState({invoiceitems: array});
  }
  this.showtotal();
}
  render() {
    return (
      <div className="animated fadeIn">
         <LoadingBar
        color='#2f49da'
        progress={this.state.progress}
        onLoaderFinished={() => this.setState({
          progress:0
        })}
        />
        <DefaultHeader/>
        <h4>Invoice Type</h4>
        <Card>
          <CardBody>
          <Row>
          <Col sm="8" className="pr-md-5">
            <h5 className="mb-4"><i className="icon2-align-left"></i> Details</h5>
            <FormGroup className="input-line">
              <Label>Invoice Number</Label>
              <Input type="text" placeholder="Invoice Number"  value={this.state.invoicenumber} disabled/>
            </FormGroup>
            <FormGroup className="input-line">
              <Label>Bank Account</Label> 
              <select className="form-control" onChange={(e)=>{this.setState({bankaccount:e.target.value})}}>
              <option value='0' key='0'> Select Option </option>
              {this.state.bankaccounts.map((item,index)=>(
                <option value={item.bankAccountId} key={index}>{item.cardTitle}</option>
              ))}
              </select>
            </FormGroup>
            <FormGroup className="input-line">
              <Label>Bill To</Label>
              <select className="form-control" onChange={this.subname}>
              <option value=''> Select Option </option>
              {this.state.subs.map((item,index)=>(
                <option value={index} key={index}>{item.serviceCenter.serviceCenterName}</option>
              ))}
              </select>
            </FormGroup>
            {(this.state.subsdescshow!='') && (

              <Card className="card-box">
                <CardHeader>
                  <h5 className="text-primary float-left">{this.state.subsdescshow.serviceCenter.serviceCenterName}</h5>
                  <div className="card-header-actions float-right">
                    <Link className="text-light-grey ml-3" onClick={()=>{this.setState({subsdescshow:''})}}><i className="fa fa-times"></i></Link>
                  </div>
                </CardHeader>
                <CardBody>
                <p className="text-light-grey">{this.state.subsdescshow.serviceCenter.email}<br></br>{this.state.subsdescshow.serviceCenter.phoneNumber}</p>
                </CardBody>
              </Card>
            )}
          </Col>
          <Col sm="4" className="pl-md-5">
            <h5 className="mb-4"><i className="icon2-calendar-dots"></i> Dates</h5>
            
              <FormGroup className="input-line">
              <Label className="text-muted" htmlhtmlFor="exampleEmail" >Issued On</Label>
              <Row>
                <Col sm="7">
                  <Input type="date" min={moment(new Date()).format('YYYY-MM-DD') } value={this.state.issuedate} onChange={(e)=>{this.setState({issuedate:e.target.value})}} />
                </Col>
                <Col sm="5" className="text-warning">
                Schedule
                {/* <ButtonDropdown id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-warning" color="transparent">
                Schedule
                </DropdownToggle>
                <DropdownMenu left>
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown> */}
              </Col>
              </Row>
            </FormGroup>
              
            
            <Row>
              <Col sm="7">
              <FormGroup className="input-line">
                <Label className="text-muted" htmlhtmlFor="exampleEmail" >Due On</Label>
                <Input type="date" min={moment(new Date()).format('YYYY-MM-DD')} value={this.state.duedate} onChange={(e)=>{this.setState({duedate:e.target.value})}}/>
              </FormGroup>
              </Col>
              
            </Row>
            <h5 className="mb-4"><i className="icon2-dotted-box"></i> Customize</h5>
            <FormGroup className="input-line">
              <Label>Currency</Label> 
              <select className="form-control" onChange={this.addcurrency}>
              <option value='USD'> US Dollar </option>
              <option value='AED'> AED Dirham </option>
              </select>
            </FormGroup>
            <FormGroup className="input-line">
              <Label>Language</Label>
              <select className="form-control" onChange={(e)=>{this.setState({lang:e.target.value})}}>
                <option value='1'> English </option>
                <option value='2'> Arabic </option>
              </select>
            </FormGroup>      
          </Col>
        </Row>
        <hr className="dashed-line"></hr>
        <h5 className="mb-4"><i className="icon2-cube-shape"></i> Item . {this.state.invoiceitems.length}</h5>          
        <Row>
          <Col sm="2">
            <FormGroup className="input-line">
            <Label className="text-primary">Item</Label>
            <select className="form-control" value={this.state.checkitem} onChange={this.itemsec}>
              <option value='1'> Services </option>
              <option value='2'> Product </option>
          </select>
          </FormGroup>
          </Col>
          <Col sm="2">
            <FormGroup className="input-line">
            {(this.state.checkitem=='1') && (
              <>
              <Label className="text-primary">Dscrp</Label>
              <Input type="text" placeholder="Dscrp" value={this.state.desc} onChange={(e)=>{this.setState({desc:e.target.value})}}/>
              </>
            )}
            {(this.state.checkitem=='2') && (
              <>
              <Label className="text-primary">Plan</Label>
              <select className="form-control" value={this.state.planselected} onChange={this.addplan}>
                <option value=''> Add Plan </option>
                {this.state.payementlisttoshow.map((item,index)=>(
                  <option value={index} key={index}>{item.paymentPlan.planName}</option>
                ))}
              </select>
              </>
            )}
            
            </FormGroup>
          </Col>
          <Col sm="2">
            <FormGroup className="input-line">
              <Label className="text-primary">Qty</Label>
              <Input type="number" min="0" placeholder="Qty" disabled={this.state.checkitem=='2'?true:false} value={this.state.quantity} onChange={(e)=>{this.setState({quantity:e.target.value})}}/>
            </FormGroup>
          </Col>
          <Col sm="2">
            <FormGroup className="input-line">
              <Label className="text-primary">Time (Month)</Label>
              <Input type="number" min="0" placeholder="Time" onChange={this.addtime} value={this.state.time}/>
            </FormGroup>
          </Col>
          <Col sm="2">
            <FormGroup className="input-line">
              <Label className="text-primary">Price ({this.state.currency})</Label>
              <Input type="number" min="0" placeholder="Price" disabled={this.state.checkitem=='2'?true:false} value={this.state.price} onChange={(e)=>{this.setState({price:e.target.value})}}/>
            </FormGroup>
          </Col>
          
          <Col sm="2" className="pt-3 text-center">
            <Button outline color="warning" onClick={this.addfullserviceitem} 
            disabled={(this.state.checkitem!='SERVICE' && this.state.desc!='' && this.state.quantity>0 && this.state.time>0 && this.state.price>0)?false:(this.state.checkitem!='PRODUCT' && this.state.planname!='' && this.state.quantity>0 && this.state.time>0 && this.state.price>0)?false:true}>Add</Button>
          </Col>
        </Row>
        <Table className="table-borderless table-sm">
          {this.state.invoiceitems.map((item,index)=>(
            <tr>
              <td width="30" className="pt-4">#{index+1}</td>
              <td>
              <Card className="card-box mb-0">
                <CardBody className="pt-3">
                <Row>
                  <Col sm="2" className="text-warning">{item.itemType}</Col>
                  <Col sm="3">{item.description}</Col>
                  <Col sm="2">{item.quantity}</Col>
                  <Col sm="2">{item.timeInMonths} months</Col>
                  <Col sm="2">{this.state.currency=='USD'?'$':'AED'} {item.price}</Col>
                  <Col sm="1" className="text-right text-light-grey"><Link onClick={()=>{this.deleteinvoiceitem(index)}} className="text-light-grey" >x</Link></Col>
                </Row>
                </CardBody>
              </Card>
              </td>
            </tr>
          ))}
          
        </Table>
        <hr className="dashed-line"></hr>
        <Row>
            <Col sm="6">
              <Card className="card-box">
                <CardBody className="p-0">
                    <textarea className="p-3 w-100 bg-transparent" placeholder="Note to receipt" rows="5" value={this.state.notes} onChange={(e)=>{this.setState({notes:e.target.value})}}></textarea>
                </CardBody>
              </Card>
            </Col>
            <Col sm="6">
              <Card className="card-light">
                <CardBody className="p-4">
                    <Row>
                      <Col sm="6">
                        <h5>Subtotal</h5>
                        <p className="text-light-grey mb-2">Tax (5%)</p>
                        <h3 className="text-primary">TOTAL</h3>
                      </Col>
                      <Col sm="6" className="text-right">
                        <h5>{this.state.currency=='USD'?'$':'AED'} {this.showsubtotal()}</h5>
                        <p className="text-light-grey mb-2">{this.state.currency=='USD'?'$':'AED'} {this.showsubtotaltax()}</p>
                        <h3 className="text-primary">{this.state.currency=='USD'?'$':'AED'} {this.showtotal()}</h3>
                      </Col>
                    </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        <div className="text-right">
            <Button outline color="primary" className="mr-2"><Link to="/invoices">Cancel</Link></Button>
            <Button outline color="primary" className="mr-2" 
            disabled={this.state.isDisableSave}
            onClick={()=>{this.saveinvoicetodb(true)}}>Save Draft</Button>
            <Button color="primary" onClick={()=>{this.saveinvoicetodb(false)}}
            disabled={this.state.isDisableSave}
            >Save and Send</Button>
          </div>
        </CardBody>
        </Card>
      </div>
    );
  }
}

export default Breadcrumbs;
