import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch,Link, NavLink  } from 'react-router-dom';
import * as router from 'react-router-dom';
import { Badge, UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem,Container } from 'reactstrap';
import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler,AppAside,AppFooter,AppHeader,AppSidebar,AppSidebarFooter,AppSidebarForm,AppSidebarHeader,AppSidebarMinimizer,AppBreadcrumb2 as AppBreadcrumb,AppSidebarNav2 as AppSidebarNav, } from '@coreui/react';
import logo from '../assets/img/brand/logo.png'
import axios from "axios";
import routes from './routes';
import adminNavigation from './adminNav';
import settingNavigation from './settingNav';
import { Subject } from 'rxjs';
import superAdminNavigation from './superAdminNav';
const DefaultAside = React.lazy(() => import('./profileAside'));
const NotificationAside = React.lazy(() => import('./notificationAside'));
const DefaultFooter = React.lazy(() => import('./footer'));
const Dashboard = React.lazy(() => import('./dashboard'))
export const signOut=()=> {
  localStorage.clear()
//  history.push('/login')
  }
class Home extends Component {
  
  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  async componentDidMount()
  {
    var type=localStorage.getItem('role')
    if(type!=undefined)
    {
      if(type.startsWith('Taboor'))
      {
        this.setState({superAdminNavData:superAdminNavigation})
        this.setState({
          roleType:'SuperAdmin'
        })
        this.getCenter()
      }
      else {
        this.setState({navData:adminNavigation})
        this.setState({
          roleType:'Admin'
        })
        this.getBranch() 
        this.getAgents()
        this.getQueues()
      }
    }
    else
    {
      this.setState({
        roleType:'none'
      })
    }
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Admin_Panel_View_Only')
        {
          this.setState({isShowProfileAsidePrev:true})
        }
      }
    }
    this.getServiceCenterProfile()
  }
  constructor()
  {
    super()
    // global.isShowArabicLanguage=false
    global.isBranchAgentCounterUpdated=new Subject()
    global.isCenterUpdated=new Subject()
    global.isNotificationUpdated=new Subject()
    this.state = {
      navData: null,
      superAdminNavData:null,
      roleType:'',
      isShowNotification:false,
      isShowProfile:false,
      profileDetails:'',
    };
    global.isBranchAgentCounterUpdated.subscribe(val => {
      if(val==true)
      {
        this.getBranch();
        this.getAgents()
        this.getQueues()
      }
     
    });
    global.isCenterUpdated.subscribe(val => {
      if(val==true)
      {
        this.getCenter()
      }
     
    });
    

  }
  async getBranch()
  {
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Branch_View')
        {
          isPrev=true
          break;
        }
        else {
          isPrev=false
        }
      }
    }
    if(isPrev!=false)
    {
      let token = localStorage.getItem('sessionToken')
      const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/branch/count';
      const header={headers: {
        'Authorization': `Bearer ${token}`
      }} 
      axios.get(url,header).then(responseJson => {
        if(responseJson.status==200)
        {
          if(responseJson.data.applicationStatusCode==0)
          {
            var data=this.state.navData
            data.items[1].badge.text=responseJson.data.values.BranchCount
            this.setState({
              navData:data
            })
          }
        }
      }).catch(err=>{
        console.log(err)
      });
    }
    else
    {
      var data=this.state.navData
      data.items[1].badge.text=0
      this.setState({
        navData:data
      })
    }  
   
  }
  async getAgents()
  {
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Agent_View')
        {
          isPrev=true
          break;
        }
        else {
          isPrev=false
        }
      }
    }
    if(isPrev!=false)
    {
      let token = localStorage.getItem('sessionToken')
      const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/agent/count';
      const header={headers: {
        'Authorization': `Bearer ${token}`
      }} 
      axios.get(url,header).then(responseJson => {
        if(responseJson.status==200)
        {
          if(responseJson.data.applicationStatusCode==0)
          {
            var data=this.state.navData
            data.items[2].badge.text=responseJson.data.values.AgentCount
            this.setState({
              navData:data
            })
          }
        }
        
      }).catch(err=>{
        console.log(err)
      });
    }
    else
    {
      var data=this.state.navData
      data.items[2].badge.text=0
      this.setState({
        navData:data
      })
    }

  }
  async getQueues()
  {
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Queue_View')
        {
          isPrev=true
          break;
        }
        else {
          isPrev=false
        }
      }
    }
    if(isPrev!=false)
    {
      let token = localStorage.getItem('sessionToken')
      const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/queue/count';
      const header={headers: {
        'Authorization': `Bearer ${token}`
      }} 
      axios.get(url,header).then(responseJson => {
        if(responseJson.status==200)
        {
          if(responseJson.data.applicationStatusCode==0)
          {
            var data=this.state.navData
            data.items[3].badge.text=responseJson.data.values.QueueCount
            this.setState({
              navData:data
            })    
          }
        }
       
      }).catch(err=>{
        console.log(err)
      });
    }
    else{
      var data=this.state.navData
      if(data!=null)
      {
        data.items[3].badge.text=0
        this.setState({
          navData:data
        })  
      }
      
    }
   
  }
  async getCenter()
  {
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/count';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.status==200)
        {
          if(responseJson.data.applicationStatusCode==0)
          {
              var data=this.state.superAdminNavData
              data.items[0].badge.text=responseJson.data.values.CenterCount
              this.setState({
                superAdminNavData:data
              })
          }
        }

      
    }).catch(err=>{
      console.log(err)
    });
  }
  onShow(data)
  {
    if(data=='profile')
    {
      this.setState({isShowNotification:false,isShowProfile:true})
    }
    else{
      this.setState({isShowNotification:true,isShowProfile:false})

    }
  }
  getServiceCenterProfile(){
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenterEmp/getProfile';
    const header={headers: { 'Authorization': `Bearer ${token}` }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode==0) {
        if(responseJson.data.user.profileImageUrl!='') {
          fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/fileData/get', {
            method: 'POST',
            body:responseJson.data.user.profileImageUrl,
            headers: { 'Authorization': `Bearer ${token}` },
          }).then(response => response.json()).then(responsejson => {
            if(responsejson.applicationStatusCode==0) {
              global.profileimg=responsejson.applicationStatusResponse
              this.setState({ profileDetails:responsejson.applicationStatusResponse});

            }
          })
        }
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  render() {
    return (
      <div className="app">
        <div className="app-body">
          <AppSidebar fixed display="lg">
          <div className="side-logo">
            {(global.profileimg) && (
            <AppNavbarBrand full={{ src: "data:image/png;base64,"+global.profileimg, width: 150,height:150,objectFit:'contain', alt: 'Taboor Logo' }} style={{objectFit:'contain'}} />
            )}
            {(!global.profileimg) && (
            <AppNavbarBrand full={{ src: logo, width: 150, alt: 'Taboor Logo' }}/>
            )}
          </div> 
          <AppSidebarHeader />
          <AppSidebarForm />
          <Suspense>
            {this.state.roleType=='Admin' && <AppSidebarNav navConfig={(this.state.navData === null) ? adminNavigation : this.state.navData} {...this.props} router={router}/>}
            {this.state.roleType=='SuperAdmin' && <AppSidebarNav navConfig={(this.state.superAdminNavData === null) ? superAdminNavigation : this.state.superAdminNavData} {...this.props} router={router}/>}
          </Suspense>
          <AppSidebarFooter />
          <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <div className="admin-header main-header"> 
              <Nav className="admin-nav" navbar> 
                <NavItem className="d-md-down-none">
                  <NavLink to="/users" className="btn btn-warning btn-sm"><i className="icon-plus"></i> Add New</NavLink>
                </NavItem>
                <NavItem className="d-md-down-none">
                  <NavLink to="#" className="nav-link top-icon"><i className="icon-magnifier"></i></NavLink>
                </NavItem> 
                </Nav>  
            </div> 
            <Container fluid>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => (
                          <route.component {...props} />
                        )} />
                    ) : (null);
                  })}
                   {this.state.roleType=='Admin' && <Redirect from="/" to="/dashboard" />}
                   {this.state.roleType=='SuperAdmin' && <Redirect from="/" to="/centerList" />}
                   {this.state.roleType=='SettingAdmin' && <Redirect from="/" to="/myAccount" />}
                   {this.state.roleType=='none' && <Redirect from="/" to="/login" /> }
                </Switch>
              </Suspense>
            </Container>
          </main> 
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default Home;
