import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, ButtonDropdown,Alert,Button, FormGroup,Label,Input, 
  Modal, ModalBody, ModalFooter,Dropdown, DropdownItem, DropdownMenu, DropdownToggle, ModalHeader, TabContent, TabPane,Collapse } from 'reactstrap';
import { Link, NavLink } from 'react-router-dom';
import { Nav, NavItem } from 'reactstrap';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import LoadingBar from 'react-top-loading-bar';
import { saveAs } from "file-saver";
var JSZip = require("jszip");
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
var formData=new FormData()
var manualFormdata=new FormData()
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
class Breadcrumbs extends Component {
  counter=0
  manualCounter=0
  totalTickets=0
  faqId=0
  manualId=0
  tempImageData=[]
  tempManualImageData=[]
  tempFAQs=[]
  tempManuals=[]
  tempSupportTicket=[]
  tempStatus=[{
    'name':'Pending'
  },{
    'name':'In Progress'
  },{
    'name':'Closed'
  }]
  constructor(props) {
    super();
    this.state = {
      progress:0,
      isSortBy:'ascending',
      empList:[],
      ticketId:0,
      isShowUpdateFaq:false,
      isShowUpdateManual:false,
      isShowSupportTicketPrev:false,
      isShowSupportTicketEditPrev:false,
      isShowFaqPrev:false,
      isShowFaqEditPrev:false,
      isShowManualPrev:false,
      isShowManualEditPrev:false,
      activeTab: new Array(4).fill('1'),
      imageArray:[],
      manualImageArray:[],
      collapse: false,
      isShowDropDownMenu:false,
      searchManualName:'',
      searchFAQName:'',
      searchTicketName:'',
      roleType:'',
      accordion: [true, false, false],
      allFAQs:[],
      allManuals:[],
      allSupportTicket:[],
      custom: [true, false],
      status: 'Closed',
      selectedEmp:'',
      fadeIn: true,
      timeout: 300,
      isManual:true,
      isFAQ:false,
      isTicket:false,
      isShowManual:false,
      modal: false,
      large: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
      subjectValid:false,subject:'',
      notesValid:false,notes:'',
      nameValid:false,name:'',
      quesValid:false,ques:'',
      ansValid:false,ans:'',
      reason:'',reasonValid:false,
      selectedStatus:'Pending',
      formManualValid:false,
      formErrorMsg:{},
      formQuesErrorMsg:{},
      isTicketModal:false,
      isTicketStatusModal:false,
      isAssignEmployeeModal:false,
      isShowFAQ:false,
      errorMsg:{},
      formValid:false,
      formFaqValid:false,
      formChangeStatusValid:false,
      formStatusErrorMsg:{},
      isUpdateTicketModal:false,
      isAll:false,
      manualToDownload:[],
      viewTicket:false,
      viewTicketImage:[]
    };
  }

  
  componentDidMount() {
    if(localStorage.getItem('isShowArabicLanguage')=='true') {
      global.isShowArabicLanguage=true
    }
    else {
      global.isShowArabicLanguage=false
    }
    formData=new FormData()
    manualFormdata= new FormData()
    var type=localStorage.getItem('role') 
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isSupportPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Support_Ticket_View')
        {
          this.setState({isShowSupportTicketPrev:true})
          isSupportPrev=true
        } 
        else {
          if(!isSupportPrev==true)
          {
            isSupportPrev=false
          }
        
        }
        if(prev[i]=='Support_Ticket_Edit')
        {
          this.setState({isShowSupportTicketEditPrev:true})
          if(type!=undefined)
          {
            if(type.startsWith('Taboor'))
            {
              this.setState({
                roleType:'SuperAdmin'
              })
            }
            else {
              this.setState({
                roleType:'Admin'
              })
            }
          }
          else
          {
            this.setState({
              roleType:'none'
            })
          }
        }
        
      }
    }
    if(isSupportPrev!=false)
    {
      this.getSupportTicket()
    }
    var isFAQPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='FAQ_View')
        {
          this.setState({isShowFaqPrev:true})
          isFAQPrev=true
        }
        else {
          if(!isFAQPrev==true)
          {
            isFAQPrev=false
          }
        
        }
        if(prev[i]=='FAQ_Edit')
        {
          this.setState({isShowFaqEditPrev:true})
        }
      }
    }
    if(isFAQPrev!=false)
    {
      this.getAllFAQs()
    }

    var isManualPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Manual_View')
        {
          this.setState({isShowManualPrev:true})
          isManualPrev=true
        }
        else {
          if(!isManualPrev==true)
          {
            isManualPrev=false
          }
        
        }
        if(prev[i]=='Manual_Edit')
        {
          this.setState({isShowManualEditPrev:true})
        }
      }
    }
    if(isManualPrev!=false)
    {
      this.getAllUserManual()
    }
    else
    {
      this.setState({isFAQ:true,isTicket:false,isManual:false})
    }  
  }

  validateForm = () => {
    const {subjectValid, notesValid} = this.state;
    this.setState({
      formValid: subjectValid  && notesValid 
    })
  }
  validateManualForm = () => {
    const {nameValid} = this.state;
    this.setState({
      formManualValid: nameValid 
    })
  }
  validateFaqForm = () => {
    const {quesValid,ansValid} = this.state;
    this.setState({
      formFaqValid: quesValid && ansValid
    })
  }
  validateChangeStatusForm = () => {
    const {reasonValid} = this.state;
    this.setState({
      formChangeStatusValid: reasonValid
    })
  }
  updateSubjectName = (subject) => {
    this.setState({subject}, this.validateSubjectName)
  }
  validateSubjectName = () => {
    const {subject} = this.state;
    let subjectValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (subject.length == 0 ||subject=='' ) {
      subjectValid = false;
      errorMsg.subject = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(subject))
    // {
    //   subjectValid = false;
    //   errorMsg.subject =  'Only alphabets are allowed'
    // }
    this.setState({subjectValid, errorMsg}, this.validateForm)
  }
  updateName = (name) => {
    this.setState({name}, this.validateName)
  }
  validateName = () => {
    const {name} = this.state;
    let nameValid = true;
    let formErrorMsg = {...this.state.errorMsg}

    if (name.length == 0 ||name=='' ) {
      nameValid = false;
      formErrorMsg.name = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(name))
    // {
    //   nameValid = false;
    //   formErrorMsg.name =  'Only alphabets are allowed'
    // }
    this.setState({nameValid, formErrorMsg}, this.validateManualForm)
  }
  updateQues = (ques) => {
    this.setState({ques}, this.validateQues)
  }
  validateQues = () => {
    const {ques} = this.state;
    let quesValid = true;
    let formQuesErrorMsg = {...this.state.errorMsg}

    if (ques.length == 0 ||ques=='' ) {
      quesValid = false;
      formQuesErrorMsg.ques = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(ques))
    // {
    //   quesValid = false;
    //   formQuesErrorMsg.ques =  'Only alphabets are allowed'
    // }
    this.setState({quesValid, formQuesErrorMsg}, this.validateFaqForm)
  }
  updateAns = (ans) => {
    this.setState({ans}, this.validateAns)
  }
  validateAns = () => {
    const {ans} = this.state;
    let ansValid = true;
    let formQuesErrorMsg = {...this.state.errorMsg}

    if (ans.length == 0 ||ans=='' ) {
      ansValid = false;
      formQuesErrorMsg.ans = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(ans))
    // {
    //   ansValid = false;
    //   formQuesErrorMsg.ans =  'Only alphabets are allowed'
    // }
    this.setState({ansValid, formQuesErrorMsg}, this.validateFaqForm)
  }
  updateNotes = (notes) => {
    this.setState({notes}, this.validateNotes)
  }
  validateNotes = () => {
    const {notes} = this.state;
    let notesValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (notes.length == 0 ||notes=='' ) {
      notesValid = false;
      errorMsg.notes = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(notes))
    // {
    //   notesValid = false;
    //   errorMsg.notes =  'Only alphabets are allowed'
    // }
    this.setState({notesValid, errorMsg}, this.validateForm)
  }
  updateReason = (reason) => {
    this.setState({reason}, this.validateReason)
  }
  validateReason = () => {
    const {reason} = this.state;
    let reasonValid = true;
    let formStatusErrorMsg = {...this.state.errorMsg}

    if (reason.length == 0 ||reason=='' ) {
      reasonValid = false;
      formStatusErrorMsg.reason = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(reason))
    // {
    //   reasonValid = false;
    //   formStatusErrorMsg.reason =  'Only alphabets are allowed'
    // }
    this.setState({reasonValid, formStatusErrorMsg}, this.validateChangeStatusForm)
  }
  getAllFAQs()
  {
    this.tempFAQs=[]
    let token = localStorage.getItem('sessionToken')
    this.setState({
      progress:50,isDisableSave:false
    })
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/faq/get/all', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          progress:100,isDisableSave:false
        })
        for(var i=0;i<responseJson.userFAQList.length;i++)
        {
          responseJson.userFAQList[i]['isChecked']=false
          this.tempFAQs.push(responseJson.userFAQList[i])
        }
        
        this.setState({allFAQs:responseJson.userFAQList})
        
      }
    })
  }
  searchFAQManual(faqName)
  {
    
    if(faqName!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempFAQs.length;i++)
      {
        if(this.tempFAQs[i].question.toLowerCase().includes(faqName.toLowerCase()))
        {
          data.push(this.tempFAQs[i])
          this.setState({
            allFAQs:data
          })
          isMatch=true
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
        }
      }
      if(isMatch==false)
      {
        this.setState({allFAQs:[]})
      }
    }
    else
    {
      this.setState({
        allFAQs:this.tempFAQs
      })
    }
  
  }
  changeFAQSearch(faqName)
  {
    if(faqName=='')
    { 
      this.setState({
        allFAQs:this.tempFAQs
      })
    }
    this.searchFAQManual(faqName)
    this.setState({
    searchFAQName:faqName
    })
  }
  addfaqmodal() {
    this.setState({
      isShowFAQ: true,
      isShowUpdateFaq:false
    });
  }
  closeFAQModel()
  {
    this.setState({
      isShowFAQ:false,
      ans:'',
      ques:'',
      formFaqValid:false,
    })
  }
  resetFAQModal()
  {
    this.setState({
      ans:'',
      ques:'',
      formFaqValid:false,
    })
  }
  addFAQ()
  {
    this.setState({
      formFaqValid:false,
      progress:50
    })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/faq/add', {
      method: 'POST',
      body:JSON.stringify({
        "answer": this.state.ans,
        "faqTypeId": 1,
        "question": this.state.ques
    
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.closeFAQModel()
        this.getAllFAQs()
        this.setState({
          isShowFAQ:false,
          progress:100
        })
      }
      else{
        this.setState({
          isShowFAQ:false,
          progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  

  getAllUserManual()
  {
    this.tempManuals=[];
    let token = localStorage.getItem('sessionToken')
    this.setState({
      progress:50,isDisableSave:false
    })
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/manual/get/all', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).then(response => response.json()).then(responseJson => {
      console.log('=================manual/get/all===================');
      console.log(responseJson);
      console.log('=================manual/get/all===================');
      var paths={}
      if(responseJson.applicationStatusCode==0) {
        this.setState({
          progress:100,isDisableSave:false
        })
        for(var i=0;i<responseJson.userManuals.length;i++) {
          responseJson.userManuals[i]['isShowMenu']=false
          this.tempManuals.push(responseJson.userManuals[i]);
          // paths[0]=
        }
        this.setState({allManuals:responseJson.userManuals});
        responseJson.userManuals.map((item,index)=>{
        });
      }
    })
  }
  searchManual(manualName) {
    
    if(manualName!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempManuals.length;i++)
      {
        if(this.tempManuals[i].name.toLowerCase().includes(manualName.toLowerCase()))
        {
          data.push(this.tempManuals[i])
          this.setState({
            allManuals:data
          })
          isMatch=true
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
        }
      }
      if(isMatch==false)
      {
        this.setState({allManuals:[]})
      }
    }
    else
    {
      this.setState({
        allBranches:this.tempManuals
      })
    }
  
  }
  changeManualSearch(manualName)
  {
    if(manualName=='')
    { 
      this.totalBranches=this.tempManuals.length
      this.setState({
        allManuals:this.tempManuals
      })
    }
    this.searchManual(manualName)
    this.setState({
    searchManualName:manualName
    })
  }
  onManualImageChange = event => {
    if (event.target.files && event.target.files[0]) {
      if(event.target.files[0].type!='application/pdf')
      {
        toaster.notify('Upload pdf file only',{
          duration:5000
        })
      }
      else
      {
        this.tempManualImageData=[]
        let img = event.target.files[0];
        if( event.target.files[0].size<5242880)
        {
          var imgsize=(event.target.files[0].size/1024).toFixed(2);
          event.target.files[0]['kbsize']=imgsize
          manualFormdata.append("UserManualFile", event.target.files[0]);
          this.manualCounter=this.manualCounter+1
          event.target.files[0]['id']=this.manualCounter
          this.tempManualImageData.push( event.target.files[0])
          this.setState({
            image: URL.createObjectURL(img),
            manualImageArray:this.tempManualImageData
          });
        }
        else{
          toaster.notify('Size must be less than 5mb',{
            duration:5000
          })
        }
      }
    }
   }
  onManualDismissImage(id)
  {
    manualFormdata=new FormData()
    document.getElementById("inputGroupFile01").value = "";
    for(var i=0;i<this.tempManualImageData.length;i++)
    {
      if(id==this.tempManualImageData[i].id)
      {
        this.tempManualImageData.splice(i,1)
      }else{
        manualFormdata.append("UserManualFile", this.tempManualImageData[i]);
      }
    }

    this.setState({ manualImageArray: this.tempManualImageData });
  }
  addmanualmodal() {
    this.setState({
      isShowManual: true,
      isShowUpdateManual:false
    });
  }
  closeManualModel()
  {
    manualFormdata=new FormData()
    document.getElementById("inputGroupFile01").value = "";
    this.tempManualImageData=[]
    this.setState({
      isShowManual:false,
      name:'',
      formManualValid:false,
      manualImageArray:[]
    })
  }
  resetManualModal()
  {
    this.tempManualImageData=[]
    document.getElementById("inputGroupFile01").value = "";
    manualFormdata=new FormData()
    this.setState({
      name:'',
      formManualValid:false,
      manualImageArray:[]
    })
  }
  addManauLFile()
  {
    this.setState({
      formManualValid:false,progress:50
    })
    let token = localStorage.getItem('sessionToken')
    manualFormdata.append('AddUserManualPayload',JSON.stringify({
      "name": this.state.name,  
    }))
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/manual/add', {
      method: 'POST',
      body:manualFormdata,
      headers: {
        // 'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${token}`
      },
    }).then(response => response.json()).then(responseJson => {
      console.log('====================================');
      console.log(responseJson);
      console.log('====================================');
      if(responseJson.applicationStatusCode==0)
      {
        manualFormdata=new FormData()
        this.closeManualModel()
        this.getAllUserManual()
        this.setState({
          progress:100,
          isShowManual:false
        })
      }
      else{
        this.setState({
          progress:100,
          isShowManual:false
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  getSupportTicket()
  {
    
    let token = localStorage.getItem('sessionToken')
    this.setState({
      progress:50,isDisableSave:false
    })
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/supportTicket/get', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).then(response => response.json()).then(responseJson => {
      console.log('==================supportTicket==================');
        console.log(responseJson);
        console.log('==================supportTicket==================');
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          progress:100,isDisableSave:false
        })
        this.tempSupportTicket=[]
        let sortedProductsAsc=[];
        for(var i=0;i<responseJson.supportTickets.length;i++)
        {
          responseJson.supportTickets[i]['isChecked']=false
          if(responseJson.supportTickets[i].supportTicketStatus=='In_Queue')
          {
            responseJson.supportTickets[i]['isShowUpdateDeleteMenu']=true
            responseJson.supportTickets[i].supportTicketStatus='Pending'
          }
          else if(responseJson.supportTickets[i].supportTicketStatus=='Assigned')
          {
            responseJson.supportTickets[i]['isShowUpdateDeleteMenu']=false
            responseJson.supportTickets[i].supportTicketStatus='In Progress'
          }
          else if(responseJson.supportTickets[i].supportTicketStatus=='Closed')
          {
            responseJson.supportTickets[i]['isShowUpdateDeleteMenu']=false
            responseJson.supportTickets[i].supportTicketStatus='Closed'
          }
          responseJson.supportTickets[i]['isShowMenu']=false
          if(responseJson.supportTickets[i].createdOn!=null)
          {
            var createdDate= new Date(responseJson.supportTickets[i].createdOn) 
            responseJson.supportTickets[i]['createdDate']=createdDate
            var sHours = createdDate.getHours();
            var sMinutes = createdDate.getMinutes();
            var sDate = createdDate.getDate();
            var sMonth = createdDate.getMonth()+1;
            var sYear = createdDate.getFullYear();
            var date= sDate+'.'+sMonth+'.'+sYear
            var ampm = sHours >= 12 ? 'PM' : 'AM';
            sHours = sHours % 12;
            sHours = sHours ? sHours : 12;
            sHours = sHours < 10 ?'0' + sHours : sHours;
            sMinutes = sMinutes < 10 ? '0' + sMinutes : sMinutes;
            var strTime = sHours + ':' + sMinutes + ' ' + ampm;
            responseJson.supportTickets[i]['time']=strTime
            responseJson.supportTickets[i]['date']=date
            
          }
          else
          {
            responseJson.supportTickets[i]['time']=null
          }
          if(responseJson.supportTickets[i].updatedOn!=null)
          {
            var updateDate= new Date(responseJson.supportTickets[i].updatedOn)
            var uDate = updateDate.getDate();
            var uMonth = updateDate.getMonth()+1;
            var uYear = updateDate.getFullYear();
            var updateDateAfterConvert=uDate+'.'+uMonth+'.'+uYear
            responseJson.supportTickets[i]['updatedDate']=updateDateAfterConvert
          }
          else
          {
            responseJson.supportTickets[i]['updatedDate']=null
          }

          responseJson.supportTickets[i]['isNew']=false
          this.tempSupportTicket.push(responseJson.supportTickets[i])
        }
        if(this.tempSupportTicket.length!=0)
        {
         
          sortedProductsAsc= this.tempSupportTicket.sort((a,b)=>{
          return (a.createdDate)  - (b.createdDate);
          })
        }
        if(sortedProductsAsc.length!=0)
        {
          for(var j=0;j<this.tempSupportTicket.length;j++)
          {
            if(sortedProductsAsc[sortedProductsAsc.length-1].supportTicketId==this.tempSupportTicket[j].supportTicketId)
            {
              console.log('====================================');
              console.log(this.tempSupportTicket[j].supportTicketStatus);
              console.log('====================================');
              if(this.tempSupportTicket[j].supportTicketStatus=="In_Queue" || this.tempSupportTicket[j].supportTicketStatus=="Pending"){
                this.tempSupportTicket[j]['isNew']=true;
              }
            }
          }
        }
        this.tempSupportTicket=this.tempSupportTicket.reverse()
        this.totalTickets=this.tempSupportTicket.length
        this.setState({allSupportTicket:this.tempSupportTicket})
        
      }
    })
  }
  searchSupportTicket(ticketName)
  {
    
    if(ticketName!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempSupportTicket.length;i++)
      {
        if(this.tempSupportTicket[i].subject.toLowerCase().includes(ticketName.toLowerCase()))
        {
          data.push(this.tempSupportTicket[i])
          this.setState({
            allSupportTicket:data
          })
          isMatch=true
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
        }
      }
      if(isMatch==false)
      {
        this.setState({allSupportTicket:[]})
      }
    }
    else
    {
      this.setState({
        allSupportTicket:this.tempSupportTicket
      })
    }
  
  }
  changeSupportTicket(ticketName)
  {
    if(ticketName=='')
    { 
      this.setState({
        allSupportTicket:this.tempSupportTicket
      })
    }
    this.searchSupportTicket(ticketName)
    this.setState({
    searchTicketName:ticketName
    })
  }
  onImageChange = event => {
  if (event.target.files && event.target.files[0]) {
    if(event.target.files[0].type!='image/png' && event.target.files[0].type!='image/gif' 
    && event.target.files[0].type!='image/jpeg' && event.target.files[0].type!='image/jpg')
    {
      toaster.notify('Upload image  only',{
        duration:5000
      })
    }
    else
    {
      let img = event.target.files[0];
      if( event.target.files[0].size<5242880)
      {
        var imgsize=(event.target.files[0].size/1024).toFixed(2);
        event.target.files[0]['kbsize']=imgsize
        formData.append("Attachements", event.target.files[0]);
        this.counter=this.counter+1
        event.target.files[0]['id']=this.counter
        this.tempImageData.push( event.target.files[0])
          console.log(formData.get('Attachements')) 
          // document.getElementById("file-upload").value = "";
        this.setState({
          image: URL.createObjectURL(img),
          imageArray:this.tempImageData
  
        });
      }
      else{
        toaster.notify('Size must be less than 5mb',{
          duration:5000
        })
      }
    }
   
   
  }
  }
  onDismissImage(id)
  {
    formData=new FormData()
    document.getElementById("inputGroupFile01").value = "";
    for(var i=0;i<this.tempImageData.length;i++)
    {
      if(id==this.tempImageData[i].id)
      {
        this.tempImageData.splice(i,1)
      }else{
        formData.append("ServiceCenterProfileImage", this.tempImageData[i]);
      }
    }

    this.setState({ imageArray: this.tempImageData });
  }
  sortTicketByDates()
  {
    if(this.tempSupportTicket.length!=0)
    {
      let sortedProductsAsc;
    sortedProductsAsc= this.tempSupportTicket.sort((a,b)=>{
       return (a.createdDate)  - (b.createdDate);
    })
    console.log(sortedProductsAsc)
    this.setState({
        allSupportTicket:sortedProductsAsc
    })
    }
  }
  toggleLarge() {
    this.setState({
      large: !this.state.large,
    });
  }
  addTicketModal()
  {
     this.setState({
      isTicketModal:true
    });
  }
  updateTicketModel(ticket,id) {
    this.updateSubjectName(ticket.subject)
    this.updateNotes(ticket.description)
    this.setState({
      isUpdateTicketModal:true,
      ticketId:id
    });
  }
  viewTicketModel(ticket,id) {
    this.updateSubjectName(ticket.subject)
    this.updateNotes(ticket.description)
    this.downloadTicketImage(ticket.filePathList)
    this.setState({
      viewTicket:true,
      ticketId:id
    });
  }
  addTicketStatusModal(type,id)
  {
     this.setState({
      isTicketStatusModal:true,
      selectedStatus:type,
      ticketId:id
    });
  }
  addAssignEmployeeModal(id)
  {
     this.setState({
      isAssignEmployeeModal:true,
      ticketId:id
    });
    this.getTaboorEmpList()
  }
  closeTicketModel()
  {
    formData=new FormData()
    document.getElementById("inputGroupFile01").value = "";
    this.tempImageData=[]
    this.setState({
      isTicketModal:false,
      subject:'',
      notes:'',
      formValid:false,
      imageArray:[]
    })
  }
  closeUpdateTicketModel()
  {
    formData=new FormData()
    // document.getElementById("file-upload").value = "";
    // this.tempImageData=[]
    this.setState({
      isUpdateTicketModal:false,
      subject:'',
      notes:'',
      formValid:false,
      // imageArray:[]
    })
  }
  closeViewTicketModel()
  {
    formData=new FormData();
    this.setState({
      viewTicket:false,
      subject:'',
      notes:'',
      formValid:false,
      viewTicketImage:[]
      // imageArray:[]
    })
  }
  closeTicketStatusModel()
  {
    this.setState({
      isTicketStatusModal:false,
      role:'',
      reason:'',
      formChangeStatusValid:false
    })
  }
  closeAssignEmployeeModel()
  {
    this.setState({
      isAssignEmployeeModal:false,
    })
  }
  resetTicketModal()
  {
    this.tempImageData=[]
    document.getElementById("inputGroupFile01").value = "";
    formData=new FormData()
    this.setState({
      subject:'',
      notes:'',
      formValid:false,
      imageArray:[]
    })
  }
  resetUpdateTicketModal()
  {
    // this.tempImageData=[]
    // document.getElementById("file-upload").value = "";
    formData=new FormData()
    this.setState({
      subject:'',
      notes:'',
      formValid:false,
      // imageArray:[]
    })
  }
  addTicketSupport()
  {
    this.setState({
      formValid:false,progress:50
    })
    // if(this.state.imageArray.length==0)
    // {
    //   toaster.notify('kindly choose a image', {
    //     // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
    //     duration:5000 // This notification will not automatically close
    //   });
    //   return;
    // }
    let token = localStorage.getItem('sessionToken')
    formData.append('AddSupportTicketPayload',JSON.stringify({
      "subject": this.state.subject,
      "description": this.state.notes
  
    }))
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/supportTicket/add', {
      method: 'POST',
      body:formData,
      headers: {
        // 'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        formData=new FormData()
        this.closeTicketModel()
        this.getSupportTicket()
        this.setState({
          isTicketModal:false,progress:100
          
        })
      }
      else{
        this.setState({
          formValid:true,progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  updateTicketSupport()
  {
    this.setState({
      formValid:false,progress:50
    })
    let token = localStorage.getItem('sessionToken')
    formData.append("Attachements", null);
    formData.append('AddSupportTicketPayload',JSON.stringify({
      "subject": this.state.subject,
      "description": this.state.notes,
      "supportTicketId": this.state.ticketId,
  
    }))
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/supportTicket/add', {
      method: 'POST',
      body:formData,
      headers: {
        // 'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        formData=new FormData()
        this.closeUpdateTicketModel()
        this.getSupportTicket()
        this.setState({
          isUpdateTicketModal:false,progress:100,
          ticketId:0
          
        })
      }
      else{
        this.setState({
          formValid:true,progress:100,
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  changeTicketStatus()
  {
    this.setState({
      formChangeStatusValid:false,progress:50
    })
    let token = localStorage.getItem('sessionToken')
    var status=''
    if(this.state.selectedStatus=='Pending')
    {
      status='In_Queue'
    }
    else if(this.state.selectedStatus=='In Progress')
    {
      status='Assigned'
    }
    else if(this.state.selectedStatus=='Closed')
    {
      status='Closed'
    }
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/supportTicket/status/update', {
      method: 'POST',
      body:JSON.stringify({
        "reason": this.state.reason,
        "supportTicketId": this.state.ticketId,
        "supportTicketStatus": status
      
    
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          formChangeStatusValid:false,progress:100
        })
        this.closeTicketStatusModel()
        this.getSupportTicket()
      
      }
      else{
        this.setState({
          formChangeStatusValid:false,progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  changeStatus=e=>
  {
    this.setState({selectedStatus:e.target.value})
  }
  getTaboorEmpList()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/taboorEmployee/getList', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          empList:responseJson.employees,
          selectedEmp:responseJson.employees[0].employee.taboorEmpId
        })
      }
    })
  }
  changeEmp=e=>
  {
    this.setState({selectedEmp:parseInt(e.target.value)})
  }
  assignEmp()
  {
    this.setState({
      progress:50
    })
     var data=[]
     data.push(this.state.ticketId)
     data.push(this.state.selectedEmp)
     let token = localStorage.getItem('sessionToken')

    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/supportTicket/employee/assign', {
      method: 'POST',
      body:JSON.stringify({
       ids:data
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          progress:100
        })
        this.closeAssignEmployeeModel()
        this.getSupportTicket()
      
      }
      else{
        this.setState({
          progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  toggle(type) {
    if(type=='manual')
    {
      this.getAllUserManual()
      this.setState({isManual:true,isFAQ:false,isTicket:false})
    }
    else if(type=='faq')
    {
      this.getAllFAQs()
      this.setState({isFAQ:true,isTicket:false,isManual:false})
    }
    else if(type=='ticket')
    {
      this.getSupportTicket()
      this.setState({isTicket:true,isFAQ:false,isManual:false})
    }
  }
  toggleAccordion(data) {
    for(var i=0;i<this.tempFAQs.length;i++)
    {
      if(this.tempFAQs[i].userFaqId==data.userFaqId)
      {
        if(this.tempFAQs[i].isChecked==true)
        {
          this.tempFAQs[i].isChecked=false
        }
        else
        {
          this.tempFAQs[i].isChecked=true
        }
        
      }
    }
    this.setState({
      allFAQs:this.tempFAQs
    })
  }
  showMenu(id)
  {
      for(var i=0;i<this.tempSupportTicket.length;i++)
      {
        if(this.tempSupportTicket[i].supportTicketId==id)
        {
          this.tempSupportTicket[i]['isShowMenu']=true
          this.setState({
            isShowDropDownMenu:true
          })
        }
        else
        {
          this.tempSupportTicket[i]['isShowMenu']=false
        }
      }
      this.setState({
        allSupportTicket:this.tempSupportTicket
      })
  }
  deleteTicket(id)
  {
    this.setState({
      progress:50
    })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/supportTicket/delete', {
      method: 'POST',
      body:JSON.stringify({
        
          "id": id 
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          progress:100
        })
        this.totalTickets=0
        this.getSupportTicket()
      }
      else{
        this.setState({
          progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  deleteManual(id)
  {
    this.setState({
      progress:50
    })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/manual/delete', {
      method: 'POST',
      body:JSON.stringify({
        
          "id": id 
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          progress:100
        })
        this.getAllUserManual()
      }
      else{
        this.setState({
          progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  deleteFAQ(id)
  {
    this.setState({
      progress:50
    })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/faq/delete', {
      method: 'POST',
      body:JSON.stringify({
        
          "id": id 
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          progress:100
        })
        this.getAllFAQs()
      }
      else{
        this.setState({
          progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  downloadTicketImage  (pathname)  {
    console.log('====================================');
    console.log(pathname);
    console.log(pathname.length);
    console.log('====================================');
    var pathstosend=[];
    var i;
    for (i=0;i<pathname.length;i++){
      pathstosend[i]={
        "id":i.toString(),
        "path":pathname[i]
      }
    }
    let token = localStorage.getItem('sessionToken')
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer "+token);
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({
      "filePaths" : pathstosend
    });
    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
    };
    console.log('====================================');
    console.log(requestOptions);
    console.log('====================================');
    setTimeout(() => {
      fetch("https://apicall.taboor.ae/taboor-qms/usermanagement/fileData/get/many", requestOptions).then(response => response.text())
      .then(result => {
        var data=JSON.parse(result);
        console.log('===============fileData=====================');
        console.log(data.filesData);
        console.log('================fileData====================');
        if(data.applicationStatusCode!=-1){
          this.setState({viewTicketImage:data.filesData})
        }
      }).catch(error => console.log('error', error));
    }, 200);
    
  }
  downloadFile  (pathname)  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/fileData/get', {
      method: 'POST',
      body:pathname,
      headers: {
          'Authorization': `Bearer ${token}`
      },
    }).then(response => response.json()).then(responseJson => {
      if(responseJson.applicationStatusCode==0) {
        const linkSource = `data:application/pdf;base64,${responseJson.applicationStatusResponse}`;
        const downloadLink = document.createElement("a");
        const fileName = "download.pdf";
        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
      }
      else{
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
  downloadAllFile=async()=> {
    var filepath=[]
    this.state.allManuals.map(async (item,index)=>{
      filepath[index]={"id":item.userManualId,"path":item.path}
    });
    setTimeout(() => {
      this.fetchPdf(filepath);
    }, 500);
    
  }
  fetchPdf (items) {
    console.log('====================================');
    console.log(items);
    console.log('====================================');
    var linkSource=[];
    var fileName=[];
    const downloadLink = document.createElement("a");
    var zip = new JSZip();
    var pdf = zip.folder("Manuals");
    let token = localStorage.getItem('sessionToken')
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer "+token);
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({
      "filePaths" : items
    });
    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
    };

    fetch("https://apicall.taboor.ae/taboor-qms/usermanagement/fileData/get/many", requestOptions)
    .then(response => response.text())
    .then(result => {
      var data=JSON.parse(result);
      console.log('====================================');
      console.log(data.filesData);
      console.log('====================================');
       
        
      data.filesData.map(async (item,index)=>{
        pdf.file(`manual${index}.pdf`, `data:application/pdf;base64,${item.data}`);
        // linkSource[index]= `data:application/pdf;base64,${item.data}`;
        // fileName[index] = `manual${index}.pdf`;
        // downloadLink.href = linkSource[index];
        // downloadLink.download = fileName[index];
        // downloadLink.click();
      });
      setTimeout(() => {
        pdf.generateAsync({type:"blob"}).then(function (blob) { 
          saveAs(blob, "Manuals.zip"); 
        }, function (err) {
            console.log(err);
        });
      }, 1000);
    })
    .catch(error => console.log('error', error));
  }
  sortBy(data)
  {
    this.sortByTicketId(data)
    this.setState({
      isSortBy:data
    })
  }
  sortByTicketId(data)
  {
    if(this.tempSupportTicket.length!=0)
    {
      let sortedProductsAsc;
      if(data=='ascending')
      {
        sortedProductsAsc= this.tempSupportTicket.sort((a,b)=>{
          return Number(a.supportTicketId)  - Number(b.supportTicketId);
        })
      }
      else
      {
        sortedProductsAsc= this.tempSupportTicket.sort((a,b)=>{
          return Number(a.supportTicketId)  - Number(b.supportTicketId);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
     
    console.log(sortedProductsAsc)
    this.setState({
        allSupportTicket:sortedProductsAsc
    })
    }
    
  }
  dotsMenu(i) {
    const newArray = this.state.dropdownOpen.map((element, index) => {
      return (index === i ? !element : false);
    });
    this.setState({
      dropdownOpen: newArray,
    });
  }
  showManualMenu(id)
  {
      for(var i=0;i<this.tempManuals.length;i++)
      {
        if(this.tempManuals[i].userManualId==id)
        {
          this.tempManuals[i]['isShowMenu']=true
          this.setState({
            isShowDropDownMenu:true
          })
        }
        else
        {
          this.tempManuals[i]['isShowMenu']=false
        }
      }
      this.setState({
        allManuals:this.tempManuals
      })
  }
  updateFAQModel(ques,ans,id)
  {
    this.setState({
      isShowUpdateFaq:true,
      isShowFAQ:true
    })
    this.faqId=id
    this.updateQues(ques)
    this.updateAns(ans)
  }
  updateManualModel(fileName,id)
  {
    this.manualId=id
    this.updateName(fileName)
    this.setState({
      isShowManual: true,
      isShowUpdateManual:true
    })
  }
  updateFAQ()
  {
    this.setState({
      formFaqValid:false,progress:50
    })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/faq/add', {
      method: 'POST',
      body:JSON.stringify({
        "answer": this.state.ans,
        "faqTypeId": 1,
        "question": this.state.ques,
        'userFAQId': this.faqId
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.closeFAQModel()
        this.getAllFAQs()
        this.setState({
          isShowFAQ:false,progress:100
        })
      }
      else{
        this.setState({
          isShowFAQ:false,progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }

  updateManauLFile()
  {
    this.setState({
      formManualValid:false,progress:50
    })
    let token = localStorage.getItem('sessionToken')
    //manualFormdata.append("Attachements", null);
    manualFormdata.append('AddUserManualPayload',JSON.stringify({
      "name": this.state.name,  
      "userManualId": this.manualId,

    }))
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/manual/add', {
      method: 'POST',
      body:manualFormdata,
      headers: {
        // 'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        manualFormdata=new FormData()
        this.closeManualModel()
        this.getAllUserManual()
        this.setState({
          isShowManual:false,progress:100
        })
      }
      else{
        this.setState({
          isShowManual:false,progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  selectAll()
  {
    var temp=this.state.isAll
    if(this.tempSupportTicket.length!=0)
    {
      for(var i=0;i<this.tempSupportTicket.length;i++)
      {
        if(temp==false)
        {
          this.tempSupportTicket[i]['isChecked']=true
          this.count=this.tempSupportTicket.length
        }
        else
        {
          this.count=0
          this.tempSupportTicket[i]['isChecked']=false
        }
        
      }
    }
    this.setState({
      isAll:!temp,
      allSupportTicket:this.tempSupportTicket,
    })
  }
  select(data)
  {
    for(var i=0;i<this.tempSupportTicket.length;i++)
    {
      if(data.supportTicketId==this.tempSupportTicket[i].supportTicketId)
      {
        if(this.tempSupportTicket[i]['isChecked']==true)
        {
          this.tempSupportTicket[i]['isChecked']=false
          this.setState({
            isAll:false
          })
          this.count=this.count-1
          break;
        }
        else
        {         
          this.count=this.count+1
          this.tempSupportTicket[i]['isChecked']=true
        }
  
      }
    }
    if(this.count==this.tempSupportTicket.length)
    {
      this.setState({
        isAll:true
      })
    }
    this.setState({allSupportTicket:this.tempSupportTicket})
  }
  render() {
    return (

        <div className="animated fadeIn">
          <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress:0
          })}
        />
        <DefaultHeader/>
        <div className="tabs-line">

              <Nav tabs>
              {this.state.isShowManualPrev==true && <NavItem>
                <NavLink className="nav-link" to="#1"
                  active={this.state.isManual.toString()}
                  isActive={(match, location) => {
                    if (!this.state.isManual) {
                      return false;
                    }
                    else{
                      return true;
                    }
                  }}
                  onClick={() => this.toggle('manual')}
                >
                  <h5 className="mb-0">
                    {global.isShowArabicLanguage==false &&<span>Manual</span>}
                    {global.isShowArabicLanguage==true &&<span>كتيّب</span>}
                  </h5>
                </NavLink>
              </NavItem>}
              {this.state.isShowFaqPrev==true &&  <NavItem>
                <NavLink className="nav-link" to="#2"
                  active={this.state.isFAQ.toString()}
                  isActive={(match, location) => {
                    if (!this.state.isFAQ) {
                      return false;
                    }
                    else{
                      return true;
                    }
                  }}
                  onClick={() => this.toggle('faq')}
                >
                  <h5 className="mb-0">
                    {global.isShowArabicLanguage==false &&<span>FAQs</span>}
                    {global.isShowArabicLanguage==true &&<span>الأسئلة الشائعة</span>}
                  </h5>
                </NavLink>
              </NavItem>}
             {this.state.isShowSupportTicketPrev==true && <NavItem>
                <NavLink className="nav-link" to="#3"
                  active={this.state.isTicket.toString()}
                  isActive={(match, location) => {
                    if (!this.state.isTicket) {
                      return false;
                    }
                    else{
                      return true;
                    }
                  }}
                  onClick={() => this.toggle('ticket')}
                >
                  <h5 className="mb-0">
                    {global.isShowArabicLanguage==false &&<span>Tickets</span>}
                    {global.isShowArabicLanguage==true &&<span>تذاكر الشكوى</span>}
                  </h5>
                </NavLink>
              </NavItem> }
            </Nav>
            <TabContent className="pt-4">
              {this.state.isManual==true && 
              <TabPane >
                
                <Row className="mb-5 hidden">
                  <Col sm="4">
                    
                  <h4 className="font-weight-normal">
                    {global.isShowArabicLanguage==false &&<span>Manuals</span>}
                    {global.isShowArabicLanguage==true &&<span>كتيّب</span>}
                  </h4>
                  </Col>

                  <Col sm="8" className="text-right">
                  
                    <div className="float-right">
                    <button className="btn btn-outline-warning ml-2" onClick={()=>this.downloadAllFile()}>Download Manuals <i className="icon2-download-outline"></i></button>
                    {this.state.isShowManualEditPrev==true &&<button className="btn btn-outline-warning ml-2" onClick={()=>this.addmanualmodal()}><i className="icon2-addition-sign"></i></button>}
                    </div>
                    <div className="input-group float-right w-auto">
                        <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                        value={this.state.searchManualName} onChange={(e) => this.changeManualSearch(e.target.value)}
                        onKeyPress={event => {
                          if (event.key === 'Enter') {
                            this.searchManual()
                          }
                        }}
                        >                  
                        </input>
                      <div className="input-group-append"><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
                    </div>
                    <div className="clearfix"></div>

                  </Col>
                </Row>
                <Row className="mb-3">
                  
                  {this.state.allManuals.map((manual,index) => (
                    <Col xs="12" sm="6" lg="3" key={index}>
                    <Card className="theme-widget text-center card-hover">
                      <CardHeader className="b-b-0 bg-transparent text-right">
                        <Link to="#" className="dots-menu"><i className="icon2-keyboard_control" onClick={()=>this.showManualMenu(manual.userManualId)}></i></Link>
                       {manual.isShowMenu==true && 
                        <Dropdown  isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu:false }); }}>
                        <DropdownToggle  tag="span"
                              onClick={() => {this.setState({ isShowDropDownMenu:false })}}
                              data-toggle="dropdown">
                        {/* <Link className="dots-menu"><i className="icon2-keyboard_control"></i></Link> */}
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem onClick={()=>this.downloadFile(manual.path)} >Download</DropdownItem>
                          {this.state.roleType=='SuperAdmin' && this.state.isShowManualEditPrev==true &&<DropdownItem onClick={()=>this.updateManualModel(manual.name,manual.userManualId)}>Update</DropdownItem>}
                          {this.state.roleType=='SuperAdmin' &&  this.state.isShowManualEditPrev==true && <DropdownItem onClick={()=>this.deleteManual(manual.userManualId)}>Delete</DropdownItem>}
                        </DropdownMenu>
                        </Dropdown>
                       }
                      </CardHeader>
                    <CardBody className="pb-4 pt-0">
                    <Link to="#">
                      <div className="text-center mb-4 mt-3">
                      <i className="fa-4x icon2-pdf-file text-primary"></i>
                      </div>

                      <h5 className="text-dark">{manual.name}</h5>
                    </Link>

                  
                    </CardBody>
                    
                  </Card>
                  </Col>
                  ))}
                </Row>
        
  
                </TabPane>}
                {this.state.isFAQ==true && <TabPane>

            <Row className="mb-5 hidden">
              <Col sm="6">
                {global.isShowArabicLanguage==false &&<h4 className="font-weight-normal">Frequently Asked Questions</h4>}
                {global.isShowArabicLanguage==true &&<h4 className="font-weight-normal">أسئلة مكررة</h4>}
              </Col>

              <Col sm="6" className="text-right">
            
              <div className="float-right">
              {/* <button className="btn btn-outline-secondary ml-2">Filters <i className="icon2-filter2"></i></button> */}
              {this.state.isShowFaqEditPrev==true && <button className="btn btn-outline-warning ml-2" onClick={()=>this.addfaqmodal()}><i className="icon2-addition-sign"></i></button>}
              </div> 
              <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                  value={this.state.searchFAQName} onChange={(e) => this.changeFAQSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.searchFAQManual()
                  }
                }}>
                </input>
                <div className="input-group-append"><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
              </div>
              <div className="clearfix"></div>

            </Col>
            
            </Row>
             
            <div id="accordion">
            {this.state.allFAQs.map((faq,index) => (
              <Card className={(faq.isChecked?'active':'')} key={index}>
                <CardHeader id="headingOne">
                  <Button block color="link" className="text-left m-0 p-0"  onClick={() => this.toggleAccordion(faq)} aria-expanded={faq.isChecked} aria-controls="collapseOne">
                    <h5 className="m-0 p-0 float-left">{faq.question}</h5>
                    <div className="float-right ml-3">
                      <i className="icon-arrow-down"></i>
                      <i className="icon-arrow-up"></i>
                    </div>
                    <div className="float-right">
                    {this.state.roleType=='SuperAdmin' && this.state.isShowFaqEditPrev==true && <Link to="#" className="text-light-grey" title="Edit"><i className="icon2-edit-pencil" onClick={()=>this.updateFAQModel(faq.question,faq.answer,faq.userFaqId)}></i></Link>}
                    {this.state.roleType=='SuperAdmin' && this.state.isShowFaqEditPrev==true &&  <Link to="#" className="text-light-grey ml-3" title="Delete" onClick={()=>this.deleteFAQ(faq.userFaqId)}><i className="font-sm font-weight-bold icon2-cancel"></i></Link>}
                    </div>
                    <div className="clearfix"></div>
                  </Button>
                </CardHeader>
                <Collapse isOpen={faq.isChecked} data-parent="#accordion" id="collapseOne" aria-labelledby="headingOne">
                  <CardBody>
                    {faq.answer}
                  </CardBody>
                </Collapse>
              </Card>
            ))}
            </div>
          

              </TabPane>}
              
              {this.state.isTicket==true && <TabPane >
                
              <Row className="mb-3 hidden">
          <Col sm="4">
            <h3>
              <div className="checkbox form-check d-inline-block ml-2">
                  <input  type="checkbox" className="form-check-input" onChange={()=>this.selectAll()} checked={this.state.isAll} />
                  <label  className="form-check-label">{this.totalTickets}&nbsp;Tickets</label>
              </div>
              </h3>
          </Col>

          <Col sm="8" className="text-right">
          
            <div className="float-right">
            {/* <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button> */}
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                onClick={() => {this.setState({ isShowSortByMenu:false })}}
                >
                <button className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></button>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={()=>this.sortBy('ascending')}><input type="radio" checked={this.state.isSortBy=='ascending'} name="sort-order" onChange={()=>{}}></input> Ascending</DropdownItem>
                  <DropdownItem onClick={()=>this.sortBy('descending')}><input type="radio" checked={this.state.isSortBy=='descending'} name="sort-order" onChange={()=>{}}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            {this.state.isShowSupportTicketEditPrev ==true && this.state.roleType=='Admin' &&
            <button className="btn btn-outline-warning ml-2" onClick={()=>this.addTicketModal()}><i className="icon2-addition-sign"></i>
            </button>}
            </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                 value={this.state.searchTicketName} onChange={(e) => this.changeSupportTicket(e.target.value)}
                 onKeyPress={event => {
                   if (event.key === 'Enter') {
                     this.searchSupportTicket()
                   }
                 }}
                >

                  
                </input>
              <div className="input-group-append"><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>
        </Row>


          <Table className="table-borderless table-sm">

          {this.state.allSupportTicket.map(ticket => (
             <tr>
             <td width="39" className="text-right">
                 <div className="checkbox form-check mt-5 pl-0 pt-3">
                       <input  type="checkbox" className="form-check-input" checked={ticket.isChecked} onChange={()=>this.select(ticket)}/>
                       <label className="form-check-label form-check-label"></label>
                   </div>
             </td>
             <td>
              <Card className="card-simple">
              <CardHeader>
                <div className="float-left mr-2">
                  {ticket.isNew==true && <span className="badge badge-danger">New</span>}
                </div>
                <p className="float-left text-warning" style={{cursor:'pointer'}} onClick={()=>this.viewTicketModel(ticket,ticket.supportTicketId)}>#{ticket.supportTicketId}</p>

                <div className="float-right">
                  <div className="card-header-actions float-left">
                    {ticket.updatedDate!=null &&  <span className="text-light-grey">Last Update  {ticket.updatedDate}</span>}
                    {ticket.updatedDate==null && <span className="text-light-grey">Last Update  Nil</span>}
                    {ticket.supportTicketStatus=='Pending' && global.isShowArabicLanguage==false && <span className="badge badge-danger ml-3">{ticket.supportTicketStatus}</span>}
                    {ticket.supportTicketStatus=='Pending' && global.isShowArabicLanguage==true && <span className="badge badge-danger ml-3">قيد الانتظار</span>}

                    {ticket.supportTicketStatus=='Closed' && global.isShowArabicLanguage==false &&<span className="badge badge-success ml-3">{ticket.supportTicketStatus}</span>}
                    {ticket.supportTicketStatus=='Closed' && global.isShowArabicLanguage==true &&<span className="badge badge-success ml-3">تم/أغلقت</span>}

                    {ticket.supportTicketStatus=='In Progress' && global.isShowArabicLanguage==false &&<span className="badge badge-warning ml-3">{ticket.supportTicketStatus}</span>}
                    {ticket.supportTicketStatus=='In Progress' && global.isShowArabicLanguage==true &&<span className="badge badge-warning ml-3">قيد الإنجاز</span>}
                  </div>
                  <div className="card-header-actions float-right ml-4">
                    {this.state.isShowSupportTicketEditPrev ==true && <Link to="#" onClick={()=>this.showMenu(ticket.supportTicketId)}><i className="f20 icon2-menu-options text-light-grey"></i></Link>}
                    {ticket.isShowMenu==true && 
                      <Dropdown isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu:false }); }} >
                        <DropdownToggle tag="span" onClick={() => {this.setState({ isShowDropDownMenu:false })}} data-toggle="dropdown" >
                        </DropdownToggle>
                        <DropdownMenu right>
                          {this.state.isShowSupportTicketEditPrev ==true && this.state.roleType=='SuperAdmin' &&<DropdownItem  onClick={()=>this.addTicketStatusModal(ticket.supportTicketStatus,ticket.supportTicketId)}>Change Ticket Status</DropdownItem>}
                          {this.state.isShowSupportTicketEditPrev ==true && this.state.roleType=='SuperAdmin' && <DropdownItem  onClick={()=>this.addAssignEmployeeModal(ticket.supportTicketId)}>Assign Employee</DropdownItem>}
                          {this.state.isShowSupportTicketEditPrev ==true && this.state.roleType=='Admin' &&<DropdownItem disabled={!ticket.isShowUpdateDeleteMenu} onClick={()=>this.updateTicketModel(ticket,ticket.supportTicketId)}>Update</DropdownItem>}
                          {this.state.isShowSupportTicketEditPrev ==true && this.state.roleType=='Admin' &&<DropdownItem disabled={!ticket.isShowUpdateDeleteMenu} onClick={()=>this.deleteTicket(ticket.supportTicketId)}>Close</DropdownItem>}
                        </DropdownMenu>
                      </Dropdown>
                    }
                  </div>
                </div>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col sm="8">
                  <h5 className="text-primary mb-3"  style={{cursor:'pointer'}}  onClick={()=>this.viewTicketModel(ticket,ticket.supportTicketId)}>{ticket.subject}<small> <i className="icon2-info-outline text-light-grey ml-2"></i></small></h5>
                  <Table className="table-borderless mb-0">
                    <tr>
                     {ticket.time!=null && <td>
                       <p className="text-light-grey mb-1">Created on</p>
                       {ticket.time}   {ticket.date}
                     </td>}
                     {ticket.time==null && <td>
                      <p className="text-light-grey mb-1">Created on</p>
                        Nil
                     </td>}
                     <td>
                     <p className="text-light-grey mb-1">Center</p>
                       <span className="text-primary">{ticket.empCreatedBy.serviceCenter.serviceCenterName}</span>
                     </td>
                     <td>
                     <p className="text-light-grey mb-1">Issued by</p>
                       {ticket.empCreatedBy.user.name}
                     </td>
                     {ticket.empAssignedTo!=null && <td>
                      <p className="text-light-grey mb-1">Assigned To</p>
                       {ticket.empAssignedTo.user.name}
                     </td>}
                   </tr>
                  </Table>
                 </Col>
                 <Col sm="4">
                 {ticket.supportTicketStatus=="In Progress" && <div className="bs-wizard mt-4 pt-2 row">
                   <div className="col-4 bs-wizard-step complete">
                   {global.isShowArabicLanguage==false && <div className="text-center bs-wizard-stepnum">Pending</div>}
                   {global.isShowArabicLanguage==true && <div className="text-center bs-wizard-stepnum">ريثما</div>}
                     <div className="progress"><div className="progress-bar" style={{backgroundColor:'#eeaf26'}}></div></div>
                     <a href="javascript:void(0)" className="bs-wizard-dot badge-warning" style={{backgroundColor:'#eeaf26'}}></a>
                   </div> 
                   
                   <div className="col-4 bs-wizard-step disabled">
                   {global.isShowArabicLanguage==false && <div className="text-center bs-wizard-stepnum">Closed</div>}
                   {global.isShowArabicLanguage==true && <div className="text-center bs-wizard-stepnum">تم/أغلقت</div>}

                     <div className="progress"><div className="progress-bar"></div></div>
                     <a href="javascript:void(0)" className="bs-wizard-dot"></a>
                   </div>
                  </div>
                }
                {ticket.supportTicketStatus=='Pending' && <div className="bs-wizard mt-4 pt-2 row">
               
                  <div className="col-4 bs-wizard-step active">
                   {global.isShowArabicLanguage==false && <div className="text-center bs-wizard-stepnum">Pending</div>}
                   {global.isShowArabicLanguage==true && <div className="text-center bs-wizard-stepnum">ريثما</div>}
                    <div className="progress"><div className="progress-bar"></div></div>
                    <a href="javascript:void(0)" className="bs-wizard-dot badge-warning"  style={{backgroundColor:'#f72c39'}}></a>
                  </div>
                  
                  <div className="col-4 bs-wizard-step disabled">
                  {global.isShowArabicLanguage==false && <div className="text-center bs-wizard-stepnum">Closed</div>}
                   {global.isShowArabicLanguage==true && <div className="text-center bs-wizard-stepnum">تم/أغلقت</div>}
                    <div className="progress"><div className="progress-bar"></div></div>
                    <a href="javascript:void(0)" className="bs-wizard-dot"></a>
                  </div>
                  </div>
                }

              {ticket.supportTicketStatus=="Closed" && <div className="bs-wizard mt-4 pt-2 row">
               
               <div className="col-4 bs-wizard-step complete">
                  {global.isShowArabicLanguage==false && <div className="text-center bs-wizard-stepnum">Pending</div>}
                  {global.isShowArabicLanguage==true && <div className="text-center bs-wizard-stepnum">ريثما</div>}
                 <div className="progress"><div className="progress-bar badge-success" style={{backgroundColor:'#06cf99'}}></div></div>
                 <a href="javascript:void(0)" className="bs-wizard-dot badge-success" style={{backgroundColor:'#06cf99'}}></a>
               </div>
               
               <div className="col-4 bs-wizard-step active">
                  {global.isShowArabicLanguage==false && <div className="text-center bs-wizard-stepnum">Closed</div>}
                  {global.isShowArabicLanguage==true && <div className="text-center bs-wizard-stepnum">تم/أغلقت</div>}
                 <div className="progress"><div className="progress-bar badge-success" style={{backgroundColor:'#06cf99'}}></div></div>
                 <a href="javascript:void(0)" className="bs-wizard-dot badge-success" style={{backgroundColor:'#06cf99'}}></a>
               </div>
              </div>}


                 </Col>
               </Row>
                 


                 </CardBody>
               </Card>
             </td>
           </tr>
          ))}
                  
          </Table>

              </TabPane>}
              
            </TabContent>
            </div>


            <Modal isOpen={this.state.isTicketModal}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader className="d-block">
                  <Row>
                      <Col sm="6">
                        {global.isShowArabicLanguage==false &&<span>New Ticket</span>}
                        {global.isShowArabicLanguage==true &&<span>شكوى جديدة</span>}

                      </Col>
                      {/* <Col sm="6" className="text-right"><h5 className="text-warning">#679849</h5></Col> */}
                    </Row>
                  
                  </ModalHeader>
                  <ModalBody>

                  <FormGroup className="input-line">
                  {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Subject</Label>}
                  {global.isShowArabicLanguage==true &&<Label className="text-light-grey">العنوان</Label>}
                  {global.isShowArabicLanguage==false &&  <Input type="text" placeholder="Subject" 
                     value={this.state.subject} onChange={(e) => this.updateSubjectName(e.target.value)} />}
                  {global.isShowArabicLanguage==true &&  <Input type="text" placeholder="العنوان" 
                     value={this.state.subject} onChange={(e) => this.updateSubjectName(e.target.value)} />}
                     < ValidationMessage valid={this.state.subjectValid} message={this.state.errorMsg.subject} />
                  </FormGroup>
                  <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Dscrp/Notes</Label>}
                    {global.isShowArabicLanguage==true &&<Label className="text-light-grey">الوصف/الملاحظة</Label>}
                    {global.isShowArabicLanguage==false &&  <Input type="text" placeholder="Dscrp/Notes" 
                     value={this.state.notes} onChange={(e) => this.updateNotes(e.target.value)} />}
                     {global.isShowArabicLanguage==true &&  <Input type="text" placeholder="الوصف/الملاحظة" 
                     value={this.state.notes} onChange={(e) => this.updateNotes(e.target.value)} />}
                     < ValidationMessage valid={this.state.notesValid} message={this.state.errorMsg.notes} />
                  </FormGroup> 
                  <FormGroup className="input-line">
                  <Row>
                      <Col sm="8">
                        {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Attachment (Screenshots/...)</Label>} 
                        {global.isShowArabicLanguage==true &&<Label className="text-light-grey">المرفقات</Label>} 
                      </Col>
                      <Col sm="4" className="text-light-grey"><small>Type: .jpg , .png</small>        <small className="ml-4">Size: 5MB</small></Col>
                    </Row>
                    
                      <div className="custom-file">
                        <input type="file" className="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01"
                        onChange={this.onImageChange} accept="image/x-png,image/gif,image/jpeg"
                        ></input>
                        {global.isShowArabicLanguage==false &&<label className="custom-file-label text-primary" >Click here to upload files</label>}
                        {global.isShowArabicLanguage==true &&<label className="custom-file-label text-primary" >الضغط هنا لإرفاق الصور</label>}
                      </div> 
                  </FormGroup>

                  {this.state.imageArray.map(img => (
                     <Alert color="light" className="theme-alert" isOpen={this.state.visible}  toggle={()=>this.onDismissImage(img.id)} >
                     <Row>
                        <Col sm="8">{img.name}</Col>
                       <Col sm="4" className="text-light-grey">{img.kbsize} KB</Col>
                     </Row>
                 </Alert>
                  ))}
                 
                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeTicketModel()}>Cancel</Button>
                    <Button outline color="primary" onClick={()=>this.resetTicketModal()}>Reset</Button>
                    <Button color="primary" disabled={!this.state.formValid} onClick={()=>this.addTicketSupport()}>Send
                    </Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.isUpdateTicketModal}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader className="d-block">
                  <Row>
                      <Col sm="6">Update Ticket</Col>
                    </Row>
                  
                  </ModalHeader>
                  <ModalBody>

                  <FormGroup className="input-line">
                    <Label>Subject</Label>
                    <Input type="text" placeholder="Subject" 
                     value={this.state.subject} onChange={(e) => this.updateSubjectName(e.target.value)} />
                     < ValidationMessage valid={this.state.subjectValid} message={this.state.errorMsg.subject} />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label>Dscrp/Notes</Label> 
                    <Input type="text" placeholder="Dscrp/Notes" 
                     value={this.state.notes} onChange={(e) => this.updateNotes(e.target.value)} />
                     < ValidationMessage valid={this.state.notesValid} message={this.state.errorMsg.notes} />
                  </FormGroup> 
                  {/* <FormGroup className="input-line">
                  <Row>
                    <Col sm="8">
                      {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Attachment (Screenshots/...)</Label>} 
                      {global.isShowArabicLanguage==true &&<Label className="text-light-grey">المرفقات</Label>} 
                    </Col>
                    <Col sm="4" className="text-light-grey"><small>Type: .jpg , .png</small>        <small className="ml-4">Size: 5MB</small></Col>
                  </Row>
                  <div className="custom-file">
                    <input type="file" className="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01"
                    onChange={this.onImageChange} accept="image/x-png,image/gif,image/jpeg" disabled ></input>
                    {global.isShowArabicLanguage==false &&<label className="custom-file-label text-primary" >Click here to upload files</label>}
                    {global.isShowArabicLanguage==true &&<label className="custom-file-label text-primary" >الضغط هنا لإرفاق الصور</label>}
                  </div>
                  </FormGroup> */}
                  {/* {this.state.imageArray.map(img => (
                     <Alert color="light" className="theme-alert" isOpen={this.state.visible}  toggle={()=>this.onDismissImage(img.id)} >
                      <Row>
                          <Col sm="8">{img.name}</Col>
                        <Col sm="4" className="text-muted">{img.kbsize} KB</Col>
                      </Row>
                    </Alert>
                  ))} */}
                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeUpdateTicketModel()}>Cancel</Button>
                    {/* <Button outline color="primary" onClick={()=>this.resetUpdateTicketModal()}>Reset</Button> */}
                    <Button color="primary" disabled={!this.state.formValid} onClick={()=>this.updateTicketSupport()}>Update</Button>
                  </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.isTicketStatusModal}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader className="d-block">
                  <Row>
                      <Col sm="6">Change Ticket Status</Col>
                      {/* <Col sm="6" className="text-right"><h5 className="text-warning">#679849</h5></Col> */}
                    </Row>
                  
                  </ModalHeader>
                  <ModalBody>

                    <Row>
                      <Col sm="4">
                      <FormGroup className="input-line">
                  <Label className="text-light-grey">Status</Label>
                  <select className="form-control" value={this.state.selectedStatus} onChange={this.changeStatus}>
                  {this.tempStatus.map(status => (
                    
                    <option key={status.name} value={status.name}>
                     {status.name}
                    </option>
                  
                  ))}
                  </select>
                  
                </FormGroup>
                      </Col>
                    </Row>
                  

                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Status Reason</Label> 
                    <Input type="text" placeholder="Subject" 
                     value={this.state.reason} onChange={(e) => this.updateReason(e.target.value)} />
                     < ValidationMessage valid={this.state.reasonValid} message={this.state.formStatusErrorMsg.reason} />
                  </FormGroup> 
                  
                 
                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeTicketStatusModel()}>Cancel</Button>
                    <Button color="primary" disabled={!this.state.formChangeStatusValid} onClick={()=>this.changeTicketStatus()}>Apply</Button>
                  </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.isAssignEmployeeModal}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader className="d-block">
                  <Row>
                      <Col sm="6">Assign Employee</Col>
                      {/* <Col sm="6" className="text-right"><h5 className="text-warning">#679849</h5></Col> */}
                    </Row>
                  
                  </ModalHeader>
                  <ModalBody>

                  <FormGroup className="input-line">
                  <Label className="text-light-grey">Select Employee</Label>
                  <select className="form-control" onChange={this.changeEmp}
                  //  value={this.state.selectedStatus} onChange={this.changeStatus}
                   >
                  {this.state.empList.map(emp => (
                    
                    <option key={emp.employee.taboorEmpId} value={emp.employee.taboorEmpId}>
                     {emp.employee.user.name}
                    </option>
                  
                  ))}
                  </select>
                </FormGroup>
                 
                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeAssignEmployeeModel()}>Cancel</Button>
                    
                    <Button color="primary" onClick={()=>this.assignEmp()}>Apply</Button>
                  </ModalFooter>
                </Modal>

      
                <Modal isOpen={this.state.isShowFAQ} 
                       className={'modal-lg ' + this.props.className}>
                  {this.state.isShowUpdateFaq==false &&<ModalHeader >Add FAQ</ModalHeader>}
                  {this.state.isShowUpdateFaq==true &&<ModalHeader >Edit FAQ</ModalHeader>}

                  <ModalBody>


                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Question</Label>
                    <Input type="text" placeholder="Question" 
                     value={this.state.ques} onChange={(e) => this.updateQues(e.target.value)} />
                     < ValidationMessage valid={this.state.quesValid} message={this.state.formQuesErrorMsg.ques} />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Answer</Label> 
                    <Input type="text" placeholder="Answer" 
                     value={this.state.ans} onChange={(e) => this.updateAns(e.target.value)} />
                     < ValidationMessage valid={this.state.ansValid} message={this.state.formQuesErrorMsg.ans} />
                  </FormGroup>
                  

                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeFAQModel()}>Cancel</Button>
                   {this.state.isShowUpdateFaq==false &&
                   <Button outline color="primary" onClick={()=>this.resetFAQModal()}>Reset</Button>} 
                    {this.state.isShowUpdateFaq==false && <Button color="primary" disabled={!this.state.formFaqValid} onClick={()=>this.addFAQ()}>Add</Button>}
                    {this.state.isShowUpdateFaq==true && <Button color="primary" disabled={!this.state.formFaqValid} onClick={()=>this.updateFAQ()}>Update</Button>}

                  </ModalFooter>
                </Modal>

                



                <Modal isOpen={this.state.isShowManual}
                       className={'modal-lg ' + this.props.className}>
                  {this.state.isShowUpdateManual==false && <ModalHeader>Add Manual</ModalHeader>}
                  {this.state.isShowUpdateManual==true && <ModalHeader>Edit Manual</ModalHeader>}

                  <ModalBody>


                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label>
                    <Input type="text" placeholder="Name" 
                     value={this.state.name} onChange={(e) => this.updateName(e.target.value)} />
                     < ValidationMessage valid={this.state.nameValid} message={this.state.formErrorMsg.name} />
                  </FormGroup>

                  <FormGroup className="input-line">
                    <Label className="text-light-grey">File Upload</Label> 
                    <div className="custom-file">
                        <input type="file" className="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01"  
                        onChange={this.onManualImageChange} accept="application/pdf"></input>
                        <label className="custom-file-label text-primary">Click here to upload files</label>
                      </div> 
                  </FormGroup>
                  {this.state.manualImageArray.map(img => (
                      <Alert color="light" className="theme-alert" 
                      toggle={()=>this.onManualDismissImage(img.id)}
                      >
                        {img.name}
                      </Alert>  
                  ))}
                  

                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeManualModel()}>Cancel</Button>
                  {this.state.isShowUpdateManual==false &&<Button outline color="primary" onClick={()=>this.resetManualModal()}>Reset</Button>}
                    {this.state.isShowUpdateManual==false && <Button color="primary" onClick={()=>this.addManauLFile()} disabled={!this.state.formManualValid}>Add</Button>}
                    {this.state.isShowUpdateManual==true && <Button color="primary" onClick={()=>this.updateManauLFile()} disabled={!this.state.formManualValid}>Update</Button>}

                  </ModalFooter>
                </Modal>
      


                <Modal isOpen={this.state.viewTicket} className={'modal-lg ' + this.props.className}>
                  <ModalHeader>Ticket Details</ModalHeader>
                  <ModalBody>
                    <FormGroup className="input-line">
                      <Label>Subject</Label>
                      <Input type="text" placeholder="Subject" 
                      value={this.state.subject} onChange={(e) => this.updateSubjectName(e.target.value)} readOnly/>
                      < ValidationMessage valid={this.state.subjectValid} message={this.state.errorMsg.subject} />
                    </FormGroup>
                    <FormGroup className="input-line">
                      <Label>Dscrp/Notes</Label> 
                      <Input type="text" placeholder="Dscrp/Notes" 
                      value={this.state.notes} onChange={(e) => this.updateNotes(e.target.value)} readOnly/>
                      < ValidationMessage valid={this.state.notesValid} message={this.state.errorMsg.notes} />
                    </FormGroup> 
                    <FormGroup className="input-line">
                      <Label style={{width:'100%'}}>Attachments</Label> 
                      {this.state.viewTicketImage.map((item,index)=>{
                        return(
                          <img key={index} src={"data:image/png;base64,"+item.data} style={{width:100,height:100,objectFit:'cover',marginTop:10}}/>
                        )
                      })}
                    </FormGroup> 
                  </ModalBody>
                  <ModalFooter>
                    <Button outline color="primary" onClick={()=>this.closeViewTicketModel()}>Done</Button>
                    {/* <Button color="primary" onClick={()=>this.updateTicketSupport()}>Update</Button> */}
                  </ModalFooter>
                </Modal>
      </div>

    );
  }
}

export default Breadcrumbs;
