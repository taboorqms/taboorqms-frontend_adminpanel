import React, { Component } from 'react';
import { Card, CardBody, Col, Row, Table,FormGroup, ButtonDropdown,  DropdownItem,  DropdownMenu,  DropdownToggle,Dropdown,Input,Button } from 'reactstrap';
import { DatePickerComponent  } from '@syncfusion/ej2-react-calendars';
import { Link } from 'react-router-dom';
import axios from "axios";
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css"; 
import LoadingBar from 'react-top-loading-bar'
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
class Branch extends Component {
  tempAvialablePlans=[]
  tempMontlyCurrency=[]
  tempAnuallyCurrency=[]
  constructor(props) {
    super(props);
    this.state = {
      progress:0,
      isSortBy:'ascending',
      isShowSortByMenu:false,
      isShowFilters:false,
      isShowDropDownMenu:false,
      isShowPlanPrev:false,
      isShowPlanEditPrev:false,
      searchPlanName:'',
      allPlans:[],
      allMontlyCurrency:[],
      allAnuallyCurrency:[],
      selectedMontlyCurrency:'',
      selectedAnuallyCurrency:'',
      totalPlans:0,
      card1:false,
      card2:false,
      filterPlan:'',filterBranches:'',filterUserPerBranch:'',sDate:null,filterAccount:'',
    };
  }
  componentDidMount()
  {
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Subscription_Plan_View')
        {
          this.setState({isShowPlanPrev:true})
          isPrev=true
        }
        else {
          if(!isPrev==true)
          {
            isPrev=false
          }
         
        }
        if(prev[i]=='Subscription_Plan_Edit')
        {
          this.setState({isShowPlanEditPrev:true})
        }
      }
    }
    if(isPrev==false)
    {
      toaster.notify("User don't have privilege", {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
    else{
      this.getPlans()
    }
  }
  getPlans()
  {
    this.tempAvialablePlans=[]
    this.tempMontlyCurrency=[]
    this.tempAnuallyCurrency=[]
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/paymentPlan/getListing';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    this.setState({
      progress:50,isDisableSave:false
    })
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode==0)
      {
        this.setState({
          progress:100,isDisableSave:false
        })
        let sortedProductsAsc;
        for(var i=0;i<responseJson.data.paymentPlanList.length;i++)
        {
          responseJson.data.paymentPlanList[i].paymentPlan['isChecked']=false
          for(var x=0;x<responseJson.data.paymentPlanList[i].paymentPlan.prices.length;x++)
          {
            this.tempMontlyCurrency.push(responseJson.data.paymentPlanList[i].paymentPlan.prices[x])
            this.tempAnuallyCurrency.push(responseJson.data.paymentPlanList[i].paymentPlan.prices[x])
          }
          var createdDate=new Date(responseJson.data.paymentPlanList[i].paymentPlan.createdTime)
          var uDate = createdDate.getDate();
          var uMonth = createdDate.getMonth()+1;
          var uYear = createdDate.getFullYear();
          uDate = uDate < 10 ? '0' + uDate : uDate;
          uMonth = uMonth < 10 && uMonth > 0 ? '0' + uMonth : uMonth;
          var date= uDate+'.'+uMonth+'.'+uYear
          responseJson.data.paymentPlanList[i].paymentPlan['createdDate']=date
          responseJson.data.paymentPlanList[i]['isShowMenu']=false
          this.tempAvialablePlans.push(responseJson.data.paymentPlanList[i])
         
        }
        
        var tempMC=this.tempMontlyCurrency[0].currency
        var tempAC=this.tempAnuallyCurrency[0].currency
        for(var k=0;k<this.tempAvialablePlans.length;k++)
        { 
          this.tempAvialablePlans[k].paymentPlan['showPriceMonthly']=null
          for(var o=0;o<this.tempAvialablePlans[k].paymentPlan.prices.length;o++)
          {
            if(tempMC==this.tempAvialablePlans[k].paymentPlan.prices[o].currency)
            {
              this.tempAvialablePlans[k].paymentPlan['showPriceMonthly']=this.tempAvialablePlans[k].paymentPlan.prices[o].priceMonthly
            }
          }
        }
        for(var y=0;y<this.tempAvialablePlans.length;y++)
        { 
          this.tempAvialablePlans[y].paymentPlan['showPriceAnnually']=null
          for(var m=0;m<this.tempAvialablePlans[y].paymentPlan.prices.length;m++)
          {
            if(tempAC==this.tempAvialablePlans[y].paymentPlan.prices[m].currency)
            {
              this.tempAvialablePlans[y].paymentPlan['showPriceAnnually']=this.tempAvialablePlans[y].paymentPlan.prices[m].priceAnnually
            }
          }
        }
        if(this.tempAvialablePlans.length!=0)
        {
         
          sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
          return (a.noOfSubscribers)  - (b.noOfSubscribers);
          })
        }
        if(sortedProductsAsc.length!=0)
        {
          this.tempAvialablePlans=sortedProductsAsc
          this.tempAvialablePlans[this.tempAvialablePlans.length-1]['isMostUsed']=true
          this.tempAvialablePlans[0]['isLeastUsed']=true
        }
        var tempArray=[...new Map(this.tempMontlyCurrency.map(o => [o.currency, o])).values()]
        this.tempMontlyCurrency=tempArray
        var tempArray1=[...new Map(this.tempAnuallyCurrency.map(o => [o.currency, o])).values()]
        this.tempAnuallyCurrency=tempArray1
        this.setState({
          totalPlans:this.tempAvialablePlans.length,
          allPlans:this.tempAvialablePlans,
          allMontlyCurrency:this.tempMontlyCurrency,
          allAnuallyCurrency:this.tempAnuallyCurrency,
          selectedMontlyCurrency:this.tempMontlyCurrency[0].currency,
          selectedAnuallyCurrency:this.tempAnuallyCurrency[0].currency
         
        })
      }
      else{
        this.setState({
          progress:50,isDisableSave:false
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  showMenu(id)
  {
      for(var i=0;i<this.state.allPlans.length;i++)
      {
        if(this.state.allPlans[i].paymentPlan.paymentPlanId==id)
        {
          this.state.allPlans[i]['isShowMenu']=true
          this.setState({
            isShowDropDownMenu:true
          })
        }
        else
        {
          this.state.allPlans[i]['isShowMenu']=false
        }
      }
      var data=this.state.allPlans
      this.setState({
        allPlans:data
      })
  }
  changeMontlyCurrency=e=>{
    this.setState({selectedMontlyCurrency: e.currentTarget.textContent})
    for(var k=0;k<this.tempAvialablePlans.length;k++)
    {
      this.tempAvialablePlans[k].paymentPlan['showPriceMonthly']=null
    }
    for(var j=0;j<this.tempAvialablePlans.length;j++)
    { 
      for(var i=0;i<this.tempAvialablePlans[j].paymentPlan.prices.length;i++)
      {
        if( e.currentTarget.textContent==this.tempAvialablePlans[j].paymentPlan.prices[i].currency)
        {
          this.tempAvialablePlans[j].paymentPlan['showPriceMonthly']=this.tempAvialablePlans[j].paymentPlan.prices[i].priceMonthly
        }
      }
    }
    this.setState({
      allPlans: this.tempAvialablePlans,
    })
  }
  changeAnuallyCurrency=e=>{
    this.setState({selectedAnuallyCurrency: e.currentTarget.textContent})
    for(var k=0;k<this.tempAvialablePlans.length;k++)
    {
      this.tempAvialablePlans[k].paymentPlan['showPriceAnnually']=null
    }
    for(var j=0;j<this.tempAvialablePlans.length;j++)
    { 
      for(var i=0;i<this.tempAvialablePlans[j].paymentPlan.prices.length;i++)
      {
        if( e.currentTarget.textContent==this.tempAvialablePlans[j].paymentPlan.prices[i].currency)
        {
          this.tempAvialablePlans[j].paymentPlan['showPriceAnnually']=this.tempAvialablePlans[j].paymentPlan.prices[i].priceAnnually       
        }
      }
    }
    this.setState({
      allPlans: this.tempAvialablePlans,
    })
  }
  planDetails(id)
  {
    this.props.history.push(
      {
        pathname:'/packageDetails',
        paymentId:id
      }
    
    )
  }
  searchPlan(planName)
  {
    if(planName!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempAvialablePlans.length;i++)
      {
        if(this.tempAvialablePlans[i].paymentPlan.planName.toLowerCase().includes(planName.toLowerCase()))
        {
          data.push(this.tempAvialablePlans[i])
          this.setState({
            allPlans:data,
            totalPlans:data.length

          })
          isMatch=true
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
        }
      }
      if(isMatch==false)
      {
        this.setState({allPlans:[],totalPlans:0
        })
      }
    }
    else
    {
      this.setState({
        allPlans:this.tempAvialablePlans
      })
    }
  
  }
  changePlanSearch(planName)
  {
    if(planName=='')
    { 
      this.setState({
        allPlans:this.tempAvialablePlans,
        totalPlans:this.tempAvialablePlans.length
      })
    }
    this.searchPlan(planName)
    this.setState({
    searchPlanName:planName
    })
  }
  sortBy(data)
  {
    this.sortByDates(data)
    this.setState({ isSortBy:data })
  }
  sortByDates(data)
  {
    if(this.tempAvialablePlans.length!=0) {
      let sortedProductsAsc;
      if(data=='ascending'){ 
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
          return new Date(a.paymentPlan.createdTime) - new Date(b.paymentPlan.createdTime);
        })
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>a.paymentPlan.createdDate.localeCompare(b.paymentPlan.createdDate))
      }
      else {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
          return new Date(a.paymentPlan.createdTime) - new Date(b.paymentPlan.createdTime);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      console.log(sortedProductsAsc)
      this.setState({
          allPlans:sortedProductsAsc
      })
    }
  }
  sortByPlanName()
  {
    if(this.tempAvialablePlans.length!=0) { 
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending') {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>a.paymentPlan.planName.localeCompare(b.paymentPlan.planName))
      }
      else {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>b.paymentPlan.planName.localeCompare(a.paymentPlan.planName))
      }
      console.log(sortedProductsAsc)
      this.setState({ allPlans:sortedProductsAsc })
    } 
  }
  sortByBranchLimit()
  {
    if(this.tempAvialablePlans.length!=0) {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending') {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
        return Number(a.paymentPlan.branchLimit)  - Number(b.paymentPlan.branchLimit);
        })
      }
      else {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
        return Number(a.paymentPlan.branchLimit)  - Number(b.paymentPlan.branchLimit);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      } 
      console.log(sortedProductsAsc)
      this.setState({ allPlans:sortedProductsAsc })
    }
  }
  sortByUserPerBranches()
  {
    if(this.tempAvialablePlans.length!=0) {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
        return Number(a.paymentPlan.noOfUserPerBranch)  - Number(b.paymentPlan.noOfUserPerBranch);
        })
      }
      else {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
        return Number(a.paymentPlan.noOfUserPerBranch)  - Number(b.paymentPlan.noOfUserPerBranch);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      } 
      console.log(sortedProductsAsc)
      this.setState({ allPlans:sortedProductsAsc })
    }
  }
  sortByAccounts()
  {
    if(this.tempAvialablePlans.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
        return Number(a.noOfSubscribers)  - Number(b.noOfSubscribers);
      })
      }
      else
      {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
        return Number(a.noOfSubscribers)  - Number(b.noOfSubscribers);
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    console.log(sortedProductsAsc)
    this.setState({
      allPlans:sortedProductsAsc
    })
    }
  }
  sortByMontlyPrice()
  {
    if(this.tempAvialablePlans.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
        return Number(a.paymentPlan.showPriceMonthly)  - Number(b.paymentPlan.showPriceMonthly);
      })
      }
      else
      {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
        return Number(a.paymentPlan.showPriceMonthly)  - Number(b.paymentPlan.showPriceMonthly);
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    console.log(sortedProductsAsc)
    this.setState({
      allPlans:sortedProductsAsc
    })
    }
  }
  sortByAnuallyPrice()
  {
    if(this.tempAvialablePlans.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
        return Number(a.paymentPlan.showPriceAnnually)  - Number(b.paymentPlan.showPriceAnnually);
      })
      }
      else
      {
        sortedProductsAsc= this.tempAvialablePlans.sort((a,b)=>{
        return Number(a.paymentPlan.showPriceAnnually)  - Number(b.paymentPlan.showPriceAnnually);
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    console.log(sortedProductsAsc)
    this.setState({
      allPlans:sortedProductsAsc
    })
    }
  }
  selectAll()
  {
    var temp=this.state.isAll
    if(this.tempAvialablePlans.length!=0)
    {
      for(var i=0;i<this.tempAvialablePlans.length;i++)
      {
        if(temp==false)
        {
          this.tempAvialablePlans[i].paymentPlan['isChecked']=true
          this.count=this.tempAvialablePlans.length
        }
        else
        {
          this.count=0
          this.tempAvialablePlans[i].paymentPlan['isChecked']=false
        }
        
      }
    }
    this.setState({
      isAll:!temp,
      allPlans:this.tempAvialablePlans,
    })
  }
  select(data)
  {
    for(var i=0;i<this.tempAvialablePlans.length;i++)
    {
      if(data.paymentPlanId==this.tempAvialablePlans[i].paymentPlan.paymentPlanId)
      {
        if(this.tempAvialablePlans[i].paymentPlan['isChecked']==true)
        {
          this.tempAvialablePlans[i].paymentPlan['isChecked']=false
          this.setState({
            isAll:false
          })
          this.count=this.count-1
          break;
        }
        else
        {         
          this.count=this.count+1
          this.tempAvialablePlans[i].paymentPlan['isChecked']=true
        }
  
      }
    }
    if(this.count==this.tempAvialablePlans.length)
    {
      this.setState({
        isAll:true
      })
    }
    this.setState({allPlans:this.tempAvialablePlans})
  }
  changePlanFilter(data)
  {
    this.setState({
      filterPlan:data
    })
  }
  changeBranchesFilter(data)
  {
    this.setState({
      filterBranches:data
    })
  }
  changeUserPerBranchFilter(data)
  {
    this.setState({
      filterUserPerBranch:data
    })
  }
  changeAccountFilter(data)
  {
    this.setState({
      filterAccount:data
    })
  }
  changeStartDate(date)
  {
    this.setState({sDate:date.value})
  }
  applyFilter()
  {
    var filterData=JSON.parse(JSON.stringify(this.tempAvialablePlans))
    if(this.state.filterPlan!='')
    {
      var val=this.state.filterPlan
      filterData =  filterData.filter(function(status) {
        return status.paymentPlan.planName.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterBranches!='')
    {
      var val=this.state.filterBranches
      filterData =  filterData.filter(function(status) {
        return status.paymentPlan.branchLimit == parseInt(val);
      });
    }
    if(this.state.filterUserPerBranch!='')
    {
      var val=this.state.filterUserPerBranch
      filterData =  filterData.filter(function(status) {
        return status.paymentPlan.noOfUserPerBranch == parseInt(val);
      });
    }
    if(this.state.filterAccount!='')
    {
      var val=this.state.filterAccount
      filterData =  filterData.filter(function(status) {
        return status.noOfSubscribers == parseInt(val);
      });
    }
    if(this.state.sDate!=null)
    {
      let d = this.state.sDate.getDate();
      let m = this.state.sDate.getMonth()+1;
      let y=this.state.sDate.getFullYear()
      d = d < 10 ? '0' + d : d;
      m = m < 10 && m > 0 ? '0' + m : m;
      var val=d+"."+m+"."+y 
      filterData =  filterData.filter(function(status) {
        return status.paymentPlan.createdDate == val;
      });
    }
    this.setState({
      allPlans:filterData,
      totalPlans:filterData.length
    })
    console.log(filterData)
   
  }
  resetFilter()
  {
    this.setState({
    filterPlan:'',filterBranches:'',filterUserPerBranch:'',sDate:null,filterAccount:'',allPlans:this.tempAvialablePlans,
    totalPlans:this.tempAvialablePlans.length
    })
  }
  deletePlan(id,account)
  {
    if(account==0)
    {
      this.setState({
        progress:50
      })
      let token = localStorage.getItem('sessionToken')
      fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/paymentPlan/delete', {
        method: 'POST',
        body:JSON.stringify({
          
            "id": id 
        }),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
      })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.applicationStatusCode==0)
        {
          this.setState({
            progress:100
          })
          this.getPlans()
        }
        else{
          toaster.notify(responseJson.devMessage, {
            // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
            duration:5000 // This notification will not automatically close
          });
        }
       
      })
    }
    else{
      toaster.notify("This payment plan cannot be deleted as it is attached with some user", {
        duration:5000 
      });
    }
  }
  render() {
    return (
      <div className="animated fadeIn">
        <LoadingBar
        color='#2f49da'
        progress={this.state.progress}
        onLoaderFinished={() => this.setState({
          progress:0
        })}
        />
       <DefaultHeader/>





        <Row className="mb-3 hidden">
          <Col sm="4">
            <h3>
            <ButtonDropdown id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                &nbsp;
                </DropdownToggle>
                <DropdownMenu left>
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              <div className="checkbox form-check d-inline-block ml-2">
                  <input type="checkbox" className="form-check-input" onChange={()=>this.selectAll()} checked={this.state.isAll}></input>
                  <label className="form-check-label form-check-label">{this.state.totalPlans} Plans</label>
              </div>
              </h3>
          </Col>
          {this.state.isShowPlanPrev==true && <Col sm="8" className="text-right">
          
            <div className="float-right">
            <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button>
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                onClick={() => {this.setState({ isShowSortByMenu:false })}}
                >
                <button className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></button>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={()=>this.sortBy('ascending')}><input type="radio" checked={this.state.isSortBy=='ascending'} name="sort-order" onChange={()=>{}}></input> Ascending</DropdownItem>
                  <DropdownItem onClick={()=>this.sortBy('descending')}><input type="radio" checked={this.state.isSortBy=='descending'} name="sort-order" onChange={()=>{}}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            {this.state.isShowPlanEditPrev==true &&<button className="btn btn-outline-warning ml-2" onClick={()=>this.props.history.push('/newPlan')}><i className="icon2-addition-sign"></i></button>}
            </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                value={this.state.searchPlanName} onChange={(e) => this.changePlanSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.searchPlan()
                  }
                }}
                >
                  
                </input>
              <div className="input-group-append"><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>}
        </Row>

        {this.state.isShowFilters==true && <Card className="card-line" id="filter-row">
          <CardBody>
            <Row>
              
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                      <DatePickerComponent  value={this.state.sDate}  placeholder='Created On'
                        onChange={(date) => this.changeStartDate(date)}/>
                      </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                        <Input type="text" placeholder="Plan" 
                         value={this.state.filterPlan} 
                         onChange={(e) => this.changePlanFilter(e.target.value)}
                        />
                      </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="#Branches" min="0"
                      value={this.state.filterBranches} 
                      onChange={(e) => this.changeBranchesFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="#Users" min="0"
                      value={this.state.filterUserPerBranch} 
                      onChange={(e) => this.changeUserPerBranchFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="#Account" min="0"
                      value={this.state.filterAccount} 
                      onChange={(e) => this.changeAccountFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    
                    <Col sm="auto">
                    <Button color="light" className="btn-sm" onClick={()=>this.resetFilter()}>Reset</Button>
                      <Button color="primary" className="btn-sm ml-2" onClick={()=>this.applyFilter()}>Apply</Button>
                    </Col>
                    
                    </Row>
                  </CardBody>
                  </Card>}

        {this.state.isShowPlanPrev==true &&<Card className="mb-0">
              
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover" >
                  <thead>
                  <tr >
                    <th width="71" style={{minWidth:'71px'}}></th>
                    <th onClick={()=>this.sortByDates()}>Created On</th>
                    <th onClick={()=>this.sortByPlanName()} className="text-left" width="180">Plan</th>
                    <th onClick={()=>this.sortByBranchLimit()}>#Branches</th>
                    <th onClick={()=>this.sortByUserPerBranches()}>#Users Per Branch </th>
                    <th onClick={()=>this.sortByAccounts()}>#Account</th>
                    <th onClick={()=>this.sortByMontlyPrice()}>Price(Monthly)  
                      <ButtonDropdown id="card1" isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                      <DropdownToggle caret className="p-0 mb-0 text-primary" color="transparent">
                      {this.state.selectedMontlyCurrency}
                      </DropdownToggle>
                      <DropdownMenu right>
                      {this.state.allMontlyCurrency.map(currency => (
                        <DropdownItem>
                          <div onClick={this.changeMontlyCurrency}>{currency.currency}</div>
                          </DropdownItem>
                      ))}
                      </DropdownMenu>
                    </ButtonDropdown>
                    </th>
                    <th onClick={()=>this.sortByAnuallyPrice()}>Price(Annualy) 
                    <ButtonDropdown id="card2" isOpen={this.state.card2} toggle={() => { this.setState({ card2: !this.state.card2 }); }}>
                      <DropdownToggle caret className="p-0 mb-0 text-primary" color="transparent">
                      {this.state.selectedAnuallyCurrency}
                      </DropdownToggle>
                      <DropdownMenu right>
                      {this.state.allAnuallyCurrency.map(currency => (
                        <DropdownItem>
                          <div onClick={this.changeAnuallyCurrency}>{currency.currency}</div>
                          </DropdownItem>
                      ))}
                      </DropdownMenu>
                    </ButtonDropdown>

                    </th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                 
                
                  {this.state.allPlans.map(plan => (
                      <tr >
                      <td className="text-right">
                        <div className="checkbox form-check">
                            <input type="checkbox" className="form-check-input" onChange={()=>this.select(plan.paymentPlan)} checked={plan.paymentPlan.isChecked}></input>
                            <label  className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">{plan.paymentPlan.createdDate}</td>
                      {plan.isMostUsed==true && <td className="td-truncate"><span className="text-truncate d-block w-100" title={plan.paymentPlan.planName}>{plan.paymentPlan.planName}</span><p className="text-success">Most Used</p></td>}
                      {plan.isLeastUsed==true && <td className="td-truncate"><span className="text-truncate d-block w-100" title={plan.paymentPlan.planName}>{plan.paymentPlan.planName}</span><p className="text-danger">Least Used</p></td>}
                      {plan.isLeastUsed==undefined && plan.isMostUsed==undefined  && <td className="td-truncate"><span className="text-truncate d-block w-100" title={plan.paymentPlan.planName}>{plan.paymentPlan.planName}</span></td>}
                      <td className="text-center">{plan.paymentPlan.branchLimit}</td>
                      <td className="text-center">{plan.paymentPlan.noOfUserPerBranch}</td>
                      <td className="text-center">{plan.noOfSubscribers}</td>
                       {plan.paymentPlan.showPriceMonthly!=null && 
                       <td className="text-center">{this.state.selectedMontlyCurrency} {plan.paymentPlan.showPriceMonthly}<span className="text-light-grey">/m</span></td>}
                        {plan.paymentPlan.showPriceMonthly==null && 
                       <td className="text-center">Not Available</td>}
                       {plan.paymentPlan.showPriceAnnually!=null && 
                       <td className="text-center">{this.state.selectedAnuallyCurrency} {plan.paymentPlan.showPriceAnnually}<span className="text-light-grey">/m</span></td>}
                        {plan.paymentPlan.showPriceAnnually==null && 
                       <td className="text-center">Not Available</td>}
                     <td className="text-center">
                     <Link className="ml-4" onClick={()=>this.showMenu(plan.paymentPlan.paymentPlanId)}><i className="f20 icon2-menu-options text-light-grey"></i></Link>
                          {plan.isShowMenu==true && 
                          <Dropdown isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu:false }); }} >
                                  <DropdownToggle
                                    tag="span"
                                    onClick={() => {this.setState({ isShowDropDownMenu:false })}}
                                    data-toggle="dropdown"
                                    >
                                  </DropdownToggle>
                                  <DropdownMenu>
                                <DropdownItem onClick={()=>this.planDetails(plan.paymentPlan.paymentPlanId)}>Details</DropdownItem>
                                {this.state.isShowPlanEditPrev==true &&  <DropdownItem onClick={()=>this.deletePlan(plan.paymentPlan.paymentPlanId,plan.noOfSubscribers)}>Delete</DropdownItem>}
                                
                                  </DropdownMenu>
                            </Dropdown>}
                       </td>
                    </tr>
                  ))}
                     
                    
                   </tbody>
                </Table>
               
              </CardBody>
            </Card>}
      </div>
    );
  }
}

export default Branch;
