import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Alert, Col, Row, Table, Button, FormGroup, Label, Input,Modal,ModalHeader,ModalBody,ModalFooter, InputGroup,
  InputGroupAddon,
  InputGroupText, } from 'reactstrap';
import { Link } from 'react-router-dom';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';
import Steps  from 'rc-steps';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import axios from "axios";
import { TimePickerComponent } from '@syncfusion/ej2-react-calendars';
import LoadingBar from 'react-top-loading-bar';
import { Map, GoogleApiWrapper,Marker } from 'google-maps-react';
import Autocomplete from "react-google-autocomplete";
import Geocoder from 'react-native-geocoding';
Geocoder.init("AIzaSyDLN9J4HC_D04AFVx1ZK5n0MQjJaO6YUko");
const mapStyles = {
  height: 400,
  width: '100%',  
  fullscreenControl: false,
  // zoomControl: false,
  streetViewControl: false,
  scaleControl: true,
  mapTypeControl: true
};
const containerStyle = {
  position: 'relative',  
  height: 400,
  width: '100%'
}
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
class Breadcrumbs extends Component {
  id=0;
  tempAvialableAgents=[]
  totalAgents=0
  signedAgents=0
  tempAssigendCounter=[]
  unsignedAgents=0
  branchName=''
  totalCounter=0
  totalAgentCounter=0
  tempAssignedServices=[]
  tempWeekSelected=[]
  tempAvialableServiceSelected=[]
  tempAllPrivileges=[]
  selectedPrivileges=[]
  tempAvialableBranchService=[]
  tempAssignedAgent=[]
  selectedTempWeekSelected=[]
  
  weekDays=[{
    'name':'Monday',id:1
  },{
    'name':'Tuesday',id:2
  },{
    'name':'Wednesday',id:3
  },{
    'name':'Thursday',id:4
  },{
    'name':'Friday',id:5
  },{
    'name':'Saturday',id:6
  },{
    'name':'Sunday',id:7
  }]
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
    this.state = {
      progress:0,
      isShowUpdate:false,
      isDisableCounter:false,
      isDisableDeleteCounter:false,
      isDisableDeleteAgent:false,
      dropdownOpen: false,
      isAgentModal:false,
      searchName:'',
      avialableBranchCounter:[],
      allPrivileges:[],
      allAgents:[],
      selectedAvialableBranchCounterId:null,
      radioSelected: 2,
      startTime:new Date(),
      endTime:new Date(),
      isShowExisting:false,
      isShowNew:true,
      selectedWeekName:'',
      NextAgentId:'',
      AgentPIN:0,
      email: '', emailValid: false,
      agentEmail: '', agentEmailValid: false,
      serviceCenterArabicName: '',serviceCenterArabicNameValid: false,
      agentName: '', agentNameValid: false,
      phone: '', phoneValid: false,
      serviceCenterName: '',serviceCenterNameValid: false,
      isAll:false,
      isAssigend:false,
      isUnassigned:false,
      selectedServiceSectorId:0,
      serviceSector:[],
      assignedServices:[],
      assignedAgents:[],
      selectedServiceSectorObject:[],
      selectedBranchServiceId:0,
      branchServices:[],
      selectedBranchServiceObject:[],
      counterNumber:0,
      isFilter:false,
      next:0,
      weekDaysObject:[],
      selectedWeekDayId:0,
      errorMsg: {},
      formValid:false,
      formAgentValid:false,
      counterIdTracker:0,
      lat:33.68211,
      long:72.993513,
      address:'',
      currentLocation: { lat: 0, lng: 0 },
    };
    this.handleStartTimeChange = this.handleStartTimeChange.bind(this);
    this.handleEndTimeChange = this.handleEndTimeChange.bind(this);
  }
  componentDidMount() {


    navigator.geolocation.getCurrentPosition((position)=>{
      console.log("Latitude is :", position.coords.latitude);
      console.log("Longitude is :", position.coords.longitude);

      this.setState({lat: position.coords.latitude,
        long: position.coords.longitude,
        currentLocation:position
      })

        console.log(this.state.currentLocation)
        
    });


    // this.renderAutoComplete();
    if(localStorage.getItem('isShowArabicLanguage')=='true')
    {
      global.isShowArabicLanguage=true
    }
    else
    {
      global.isShowArabicLanguage=false
    }
    this.state.startTime.setHours(9)
    this.state.startTime.setMinutes(0)
    this.state.endTime.setHours(17)
    this.state.endTime.setMinutes(0)
    this.setState({
      selectedWeekName:this.weekDays[0].name,
      selectedWeekDayId:this.weekDays[0].id
    })
    this.getAllServiceSector()
    this.getAllAgents()
    
  }
  componentDidUpdate(prevProps) {
    // if (this.props !== prevProps.map) 
    // this.renderAutoComplete();
  }
  onSubmit(e) {
    e.preventDefault();
  }
  renderAutoComplete() {
    const { google, map } = this.props;

    if (!google || !map) return;

    const autocomplete = new google.maps.places.Autocomplete(this.autocomplete);
    autocomplete.bindTo('bounds', map);

    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();

      if (!place.geometry) return;

      if (place.geometry.viewport) map.fitBounds(place.geometry.viewport);
      else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }

      this.setState({ position: place.geometry.location });
    });
  }
  getAllAgents()
  {
    this.signedAgents=0
    this.unsignedAgents=0
    this.tempAvialableAgents=[]
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/agent/get/serviceCenter';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode==0)
      {
        
        for(var i=0;i<responseJson.data.agentList.length;i++)
        {
            responseJson.data.agentList[i].agent['isChecked']=false
            if(responseJson.data.agentList[i].agent.branch!=undefined)
            {
              this.signedAgents=this.signedAgents + 1
            }
            else if(responseJson.data.agentList[i].agent.branch==undefined)
            {
              this.unsignedAgents=this.unsignedAgents + 1
            }
            this.tempAvialableAgents.push(responseJson.data.agentList[i])
        }
        
        this.totalAgents=this.tempAvialableAgents.length;
        this.setState({
          allAgents:responseJson.data.agentList,
         
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  changeAgentBranch(data)
  {
    for(var i=0;i<this.state.allAgents.length;i++)
    {
      if(data.agent.agentId==this.state.allAgents[i].agent.agentId)
      {
        if(this.state.allAgents[i].agent.branch!=undefined)
        {
          toaster.notify(({ onClose }) => (
            <div className='assignedBranch'>
              <div style={{textAlign:'left'}}>
              <span>Agent cannot added to this Branch because he is already assigned to another Branch, Kindly contact Branch Admin to release agent.</span>
              </div>
              <div>
                <div style={{textAlign:'left'}}><span>Branch Details:</span></div>
                <div style={{textAlign:'left'}}><span>Name: {this.state.allAgents[i].agent.branch.branchName}</span></div>
                <div style={{textAlign:'left'}}><span>Arabic Name: {this.state.allAgents[i].agent.branch.branchNameArabic}</span></div>
                <div style={{textAlign:'left'}}><span>Email: {this.state.allAgents[i].agent.branch.emailAddress}</span></div>
                <div style={{textAlign:'left'}} ><span>Phone Number: {this.state.allAgents[i].agent.branch.phoneNumber}</span></div>
              </div>
            
              <button onClick={onClose} className='assigendBranchCross'><i className='font-lg font-weight-bold icon2-cancel'></i></button>
            </div>
          ),{
            duration:5000
          });
          this.state.allAgents[i].agent['isChecked']=false
          return
        }
        else
        {
          if(this.state.allAgents[i].agent['isChecked']==true)
          {
            this.state.allAgents[i].agent['isChecked']=false
            
          }
          else
          {
            this.state.allAgents[i].agent['isChecked']=true
          }
        }
      }
    }
    var data=this.state.allAgents
    this.setState({allAgents:data})
  }
  search(agentName)
  {
    
    if(agentName!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempAvialableAgents.length;i++)
      {
        if(this.tempAvialableAgents[i].agent.serviceCenterEmployee.user.name.toLowerCase().includes(agentName.toLowerCase()))
        {
          data.push(this.tempAvialableAgents[i])
          this.setState({
            allAgents:data
          })
          isMatch=true
          this.totalAgents=data.length
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
          
        }
      }
      if(isMatch==false)
      {
        this.totalAgents=0
        this.setState({allAgents:[]})
      }
    }
    else
    {
      this.totalAgents=this.tempAvialableAgents.length
      this.setState({
        allAgents:this.tempAvialableAgents
      })
    }
  
  }
  changeSearch(agentName)
  {
    if(agentName=='')
    { 
      this.totalAgents=this.tempAvialableAgents.length
      this.setState({
        allAgents:this.tempAvialableAgents
      })
    }
    this.search(agentName)
    this.setState({
      searchName:agentName
      })
  }
  selectAllAgents(type)
  {
    
    if(type=='All')
    {
      this.setState({allAgents:this.tempAvialableAgents})
    }
    else if(type=='Assigned'){
      var data=[]
      for(var i=0;i<this.tempAvialableAgents.length;i++)
      {
        if(this.tempAvialableAgents[i].agent.branch!=undefined)
        {
         data.push(this.tempAvialableAgents[i])
        }   
      }
      this.setState({allAgents:data})  
    }
    else if(type=='UnAssigned'){
      var data=[]
      for(var i=0;i<this.tempAvialableAgents.length;i++)
      {
        if(this.tempAvialableAgents[i].agent.branch==undefined)
        {
        data.push(this.tempAvialableAgents[i])
        }
      
      }
      this.setState({allAgents:data})    
    }
  }
  
  // selectSigendAgents()
  // {
  //   var temp=!this.state.isAssigend
  //   this.setState({isAssigend:temp})
  //   if(temp==true)
  //   {
  //     var data=[]
  //     for(var i=0;i<this.tempAvialableAgents.length;i++)
  //     {
  //       if(this.tempAvialableAgents[i].agent.branch!=undefined)
  //       {
  //        data.push(this.tempAvialableAgents[i])
  //       }   
  //     }
  //     this.setState({allAgents:data})
  //   }
  //   else if(this.state.isAll==true){
  //     this.setState({allAgents:this.tempAvialableAgents})   
  //   }
  //   else if(this.state.isUnassigned==true){
  //     var data=[]
  //     for(var i=0;i<this.tempAvialableAgents.length;i++)
  //     {
  //       if(this.tempAvialableAgents[i].agent.branch==undefined)
  //       {
  //       data.push(this.tempAvialableAgents[i])
  //       }
      
  //     }
  //     this.setState({allAgents:data})    
  //   }
  //   else
  //   {
  //     this.setState({allAgents:this.tempAvialableAgents})
  //   }
   
  // }
  // selectUnAssigendAgents()
  // {
  //   var temp=!this.state.isUnassigned
  //   this.setState({isUnassigned:temp})
  //   if(temp==true)
  //   {
  //     var data=[]
  //     for(var i=0;i<this.tempAvialableAgents.length;i++)
  //     {
  //       if(this.tempAvialableAgents[i].agent.branch==undefined)
  //       {
  //       data.push(this.tempAvialableAgents[i])
  //       }
      
  //     }
  //     this.setState({allAgents:data})
  //   }
  //   else if(this.state.isAll==true){
  //     this.setState({allAgents:this.tempAvialableAgents})   
  //   }
  //   else if(this.state.isAssigend==true){
  //     var data=[]
  //     for(var i=0;i<this.tempAvialableAgents.length;i++)
  //     {
  //       if(this.tempAvialableAgents[i].agent.branch!=undefined)
  //       {
  //        data.push(this.tempAvialableAgents[i])
  //       }   
  //     }
  //     this.setState({allAgents:data})    
  //   }
  //   else
  //   {
  //     this.setState({allAgents:this.tempAvialableAgents})
  //   }
    
  // }
  showFilter()
  {
    var temp=!this.state.isFilter
    this.setState({isFilter:temp})
  }
  updateExistingAgent(agent)
  {
    var selectedAgents=[]
    
    for(var i=0;i<this.tempAvialableAgents.length;i++)
    {
      if(this.tempAvialableAgents[i].agent['isChecked']==true)
      {
        var obj=JSON.stringify({
          "agentPk": this.tempAvialableAgents[i].agent.agentId,
          "branchId": parseInt(localStorage.getItem('branchId')),
          updateStatus:2
        })
        selectedAgents.push(obj)
      }
    }

      if(selectedAgents.length==0)
      {
        toaster.notify('Kindly select atleast one agent',{
          duration:5000
        })
        return;
      }
      this.setState({
        progress:50
      })
    for(var i=0;i<this.tempAvialableAgents.length;i++)
    {
      if(this.tempAvialableAgents[i].agent['isChecked']==true)
      {
        var obj=JSON.stringify({
          "agentPk": this.tempAvialableAgents[i].agent.agentId,
          "branchId": parseInt(localStorage.getItem('branchId')),
          updateStatus:2
        })
        let token = localStorage.getItem('sessionToken')
        fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/update', {
          method: 'POST',
          body:JSON.stringify({
            "agentPk": this.tempAvialableAgents[i].agent.agentId,
            "branchId": parseInt(localStorage.getItem('branchId')),
            'updateStatus':2
          }),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          },
        })
        .then(response => response.json())
        .then(responseJson => {
          if(responseJson.applicationStatusCode==0)
          {
            this.tempAssignedAgent.push(responseJson.values)
            this.totalAgentCounter=this.tempAssignedAgent.length
            this.setState({
                assignedAgents:this.tempAssignedAgent,progress:100,
            })
            global.isBranchAgentCounterUpdated.next(true)
            global.isNotificationUpdated.next(true)
            this.getAllAgents()
            // this.getNextCredentials()
          }
          else{
            toaster.notify(responseJson.devMessage, {
              // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
              duration:5000 // This notification will not automatically close
            });
          }
        })
      }
    }
  }
  handleStartTimeChange(time) {
    this.setState({ startTime:time });
  }
  handleEndTimeChange(time) {
    this.setState({ endTime:time });
  }
  nextStep=(next)=>{
    if(next==1)
    {
      this.addBranch();
    }
    if(next==2)
    {
      this.tempAssigendCounter=[]
      this.setState({next:next})
      this.getNextCredentials()
      this.getAvilableBranchCounter()
      this.getAgentPrevileges()
    }
    
  }
  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }
  changeWeekDays=e=>
  {
    this.setState({selectedWeekDayId:parseInt(e.target.value)})
    for(var i=0;i<this.weekDays.length;i++)
    {
      if(this.weekDays[i].id==parseInt(e.target.value))
      {
       
        this.setState({selectedWeekDayId:parseInt(e.target.value),selectedWeekName:this.weekDays[i].name})
      }
    }
  }
  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }
  onDismissAvialableService(id) {
    for(var i=0;i<this.tempAvialableServiceSelected.length;i++)
    {
      if(id==this.tempAvialableServiceSelected[i].serviceId)
      {
       
          this.tempAvialableServiceSelected.splice(i,1)

      }
    }
    this.setState({ selectedServiceSectorObject: this.tempAvialableServiceSelected,serviceSector:[] });
    this.getAllServiceSector()
  }
  onDismissAvialableBranchService(id) {
    for(var i=0;i<this.tempAvialableBranchService.length;i++)
    {
      if(id==this.tempAvialableBranchService[i].serviceId)
      {
        this.tempAvialableBranchService.splice(i,1)
      }
    }
    this.setState({ selectedBranchServiceObject: this.tempAvialableBranchService,branchServices:[] });
    this.getBranchServices()
  }
  validateForm = () => {
    const {serviceCenterNameValid, emailValid,phoneValid,serviceCenterArabicNameValid} = this.state;
    this.setState({
      formValid: serviceCenterNameValid && emailValid && phoneValid && serviceCenterArabicNameValid
    })
  }
  validateAgentForm = () => {
    const {agentEmailValid, agentNameValid} = this.state;
    this.setState({
      formAgentValid: agentEmailValid && agentNameValid
    })
  }
  updatePhoneNo = (phone) => {
    this.setState({phone}, this.validatePhoneNo)
  }
  validatePhoneNo = () => {
    const {phone} = this.state;
    let phoneValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (phone.length == 0 || phone=='') {
      phoneValid = false;
      errorMsg.phone = 'Required'
    }
     else if (phone.length <8) {
      phoneValid = false;
      errorMsg.phone = 'Phone number must be 8 characters'
    }
    else if (!/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/.test(phone)){
      phoneValid = false;
      errorMsg.phone = 'Invalid phone format'
    }
    this.setState({phoneValid, errorMsg}, this.validateForm)
  }
  updateEmail = (email) => {
    this.setState({email}, this.validateEmail)
  }
  validateEmail = () => {
    const {email} = this.state;
    let emailValid = true;
    let errorMsg = {...this.state.errorMsg}

    // checks for format _@_._
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){
      emailValid = false;
      errorMsg.email = 'Invalid email format'
    }

    this.setState({emailValid, errorMsg}, this.validateForm)
  }
  updateAgentEmail= (agentEmail) => {
    this.setState({agentEmail}, this.validateAgentEmail)
  }
  validateAgentEmail = () => {
    const {agentEmail} = this.state;
    let agentEmailValid = true;
    let errorMsg = {...this.state.errorMsg}

    // checks for format _@_._
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(agentEmail)){
      agentEmailValid = false;
      errorMsg.agentEmail = 'Invalid email format'
    }

    this.setState({agentEmailValid, errorMsg}, this.validateAgentForm)
  }
  updateAgentName = (agentName) => {
    this.setState({agentName}, this.validateAgentName)
  }
  validateAgentName = () => {
    const {agentName} = this.state;
    let agentNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (agentName.length == 0 ||agentName=='' ) {
      agentNameValid = false;
      errorMsg.agentName = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(agentName))
    // {
    //   agentNameValid = false;
    //   errorMsg.agentName =  'Only alphabets are allowed'
    // }
    this.setState({agentNameValid, errorMsg}, this.validateAgentForm)
  }
  updateServiceCenterName = (serviceCenterName) => {
    this.setState({serviceCenterName}, this.validateServiceCenterName)
  }
  validateServiceCenterName = () => {
    const {serviceCenterName} = this.state;
    let serviceCenterNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (serviceCenterName.length == 0 ||serviceCenterName=='' ) {
      serviceCenterNameValid = false;
      errorMsg.serviceCenterName = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(serviceCenterName))
    // {
    //   serviceCenterNameValid = false;
    //   errorMsg.serviceCenterName = 'Only alphabets are allowed'
    // }
    else if (serviceCenterName.length <2) {
      serviceCenterNameValid = false;
      errorMsg.serviceCenterName = 'Name should be at least 2 characters'
    }
    this.setState({serviceCenterNameValid, errorMsg}, this.validateForm)
  }
  getAllServiceSector()
  {

    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getServices';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode==0)
      {
        // responseJson.data.serviceList.unshift({serviceId:0,serviceName:'Services'})
        // this.tempAvialableServiceSelected.push(responseJson.data.serviceList[0])
        this.setState({
          serviceSector:responseJson.data.services,
          // selectedServiceSectorId:responseJson.data.serviceList[0].serviceId,
          // selectedServiceSectorObject:this.tempAvialableServiceSelected
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  changeServiceSector=e=>
  {
    console.log(e.target.value);
    this.setState({selectedServiceSectorId:parseInt(e.target.value)})
    var repeat=false;
    for(var i=0;i<this.state.serviceSector.length;i++)
    {
      console.log('inside');
      if(this.state.serviceSector[i].serviceTAB.serviceId==parseInt(e.target.value))
      {

        for(var j=0;j<this.tempAvialableServiceSelected.length;j++){
          if(this.state.serviceSector[i].serviceTAB.serviceId==this.state.selectedServiceSectorObject[j].serviceId){
            repeat=true
          }

        }
        console.log(repeat)
        if(repeat ==true){
          toaster.notify('Cannot add duplicate service',{
            duration:1000
          })
        }


        else{
          console.log('inside 2');
          this.tempAvialableServiceSelected.push(this.state.serviceSector[i].serviceTAB)
        }

        
      }
    }
    // var tempArray=this.tempAvialableServiceSelected.map(o => [o.serviceId, o]).values()
    // this.tempAvialableServiceSelected=tempArray
    this.setState({selectedServiceSectorObject:this.tempAvialableServiceSelected})
    console.log(this.tempAvialableServiceSelected);
  }
  changeBranchService=e=>
  {
    if(parseInt(e.target.value)!=0)
    {
      this.setState({selectedBranchServiceId:parseInt(e.target.value)})
      for(var i=0;i<this.state.branchServices.length;i++)
      {
        if(this.state.branchServices[i].serviceId==parseInt(e.target.value))
        {
          this.tempAvialableBranchService.push(this.state.branchServices[i])  
        }
      }
      // var tempArray=[...new Map(this.tempAvialableBranchService.map(o => [o.serviceId, o])).values()]
      // this.tempAvialableBranchService=tempArray
      this.setState({selectedBranchServiceObject:this.tempAvialableBranchService})
    }
   
  }
  addWeekDays()
  {
    this.id=this.id+1
    var requestStartTime
    var requestEndTime
    var sHours = this.state.startTime.getHours();
    var sMinutes = this.state.startTime.getMinutes();
    sHours = sHours < 10 ?'0' + sHours : sHours;
    sMinutes = sMinutes < 10 ? '0' + sMinutes : sMinutes;
    requestStartTime=sHours + ':' + sMinutes 
    var ampm = sHours >= 12 ? 'PM' : 'AM';
    sHours = sHours % 12;
    sHours = sHours ? sHours : 12; 
   
    
    var strTime = sHours + ':' + sMinutes + ' ' + ampm;
  

    var eHours = this.state.endTime.getHours();
    var eMinutes = this.state.endTime.getMinutes();
    eHours = eHours < 10 ?'0' + eHours : eHours;
    eMinutes = eMinutes < 10 ? '0' + eMinutes : eMinutes;
    requestEndTime=eHours + ':' + eMinutes 
    var ampm = eHours >= 12 ? 'PM' : 'AM';
    eHours = eHours % 12;
    eHours = eHours ? eHours : 12;
    
    
    var endTime = eHours + ':' + eMinutes + ' ' + ampm;
    for(var i=0;i<this.tempWeekSelected.length;i++)
    {
      if(this.state.selectedWeekDayId==this.tempWeekSelected[i].day)
      {
        toaster.notify('Week Day is already selected',{
          duration:5000
        })
        return;
      }
    }

    this.tempWeekSelected.push({
       
      "closingTime":requestEndTime,
      "day": this.state.selectedWeekDayId,
      "openingTime": requestStartTime

    })
    this.selectedTempWeekSelected.push({
      'id':this.id,
      "closingTime": endTime,
      "day": this.state.selectedWeekDayId,
      'name':this.state.selectedWeekName,
      "openingTime":strTime

    })
    this.setState({
        weekDaysObject:this.selectedTempWeekSelected
     })
  }
  onDismissWeekDays(id)
  {
    for(var i=0;i<this.selectedTempWeekSelected.length;i++)
    {
      if(id==this.selectedTempWeekSelected[i].day)
      {
        this.selectedTempWeekSelected.splice(i,1)
        this.tempWeekSelected.splice(i,1)
      }
    }
    this.setState({ weekDaysObject: this.selectedTempWeekSelected });
  }
  changeStartTime(time)
  {
    this.setState({startTime: time.value})
  }
  changeEndTime(time)
  {
    this.setState({endTime: time.value})
  }
  addBranch()
  {
   
    if(this.state.weekDaysObject.length==0)
    {
      toaster.notify('Kindly select working hours',{
        duration:5000
      })
      return;
    }
    if(this.state.selectedServiceSectorObject.length==0)
    {
      toaster.notify('Kindly select atleast one service',{
        duration:5000
      })
      return;
    }
    this.setState({
      formValid:false,
      progress:50
    })
    var latitude=this.state.lat
    var longitude=this.state.lng
    if (latitude==null || longitude==null){
      latitude=0
      longitude=0
    }
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/add', {
      method: 'POST',
      body:JSON.stringify({
        "address": this.state.address,
        "branchName": this.state.serviceCenterName,
        "branchNameArabic": this.state.serviceCenterArabicName,
        "city": "Rawalpindi",
        "email": this.state.email,
        "latitude": latitude,
        "longitude": longitude,
        "phoneNumber": this.state.phone,
        "serviceList": this.state.selectedServiceSectorObject,
        "workingHoursList": this.tempWeekSelected
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responsejson => {
      if(responsejson.applicationStatusCode==0)
      {
        this.branchName=responsejson.values.BranchName
        this.setState({next:1,formValid:true,selectedWeekDayId:1,progress:100})
        global.isBranchAgentCounterUpdated.next(true)
        global.isNotificationUpdated.next(true)
        localStorage.setItem('branchId',responsejson.values.BranchId)
        
        this.getNextCounterNumber()
        this.getBranchServices()
       
      }
      else{
        this.setState({formValid:true,progress:100})
        toaster.notify(responsejson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000// This notification will not automatically close
        });
      }
    })
  }
  getBranchServices()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/getServices', {
      method: 'POST',
      body:JSON.stringify({
      'id':parseInt(localStorage.getItem('branchId'))
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(branchServiceResponse => {
      if(branchServiceResponse.applicationStatusCode==0)
      {
        // branchServiceResponse.serviceList.unshift({serviceId:0,serviceName:'Services'})
        this.setState({
          branchServices:branchServiceResponse.serviceList,
          // selectedBranchServiceId:branchServiceResponse.serviceList[0].serviceId,
          // selectedBranchServiceObject:this.tempAvialableBranchService
        })
      }
    })
  }
  addBranchCounter()
  {
    var obj
    if(this.state.selectedBranchServiceObject.length==0)
    {
      toaster.notify('Kindly select atleast one service',{
        duration:5000
      })
      return;
    }
    
    if(this.tempAssignedServices.length!=0)
    {
      for(var i=0;i<this.tempAssignedServices.length;i++)
      {
         if(this.state.counterNumber==this.tempAssignedServices[i].SavedCounterNumber)
         {
           obj=JSON.stringify({
            "branchId": parseInt(localStorage.getItem('branchId')),
            "counterId": this.tempAssignedServices[i].CounterId,
            "services": this.state.selectedBranchServiceObject,
            "update": true    
           }
           
          )
          this.tempAssignedServices.splice(i,1)
          break;
        
        }
         else{
          obj=JSON.stringify({
            "branchId": parseInt(localStorage.getItem('branchId')),
            "counterId": null,
            "services": this.state.selectedBranchServiceObject,
            "update": false    
           })
         }
      }
    }
    else{
      obj=JSON.stringify({
        "branchId": parseInt(localStorage.getItem('branchId')),
        "counterId": null,
        "services": this.state.selectedBranchServiceObject,
        "update": false    
       })
    }
    this.setState({
      isDisableCounter:true,
      progress:50
    })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/addOrUpdate', {
      method: 'POST',
      body:obj,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.tempAssignedServices.push(responseJson.values)
        this.totalCounter=this.tempAssignedServices.length
        this.setState({
          progress:100,
            assignedServices:this.tempAssignedServices,
            selectedBranchServiceObject:[],
            branchServices:[],
            isDisableCounter:false
          })
          this.tempAvialableBranchService=[]
          this.getBranchServices()
          this.getNextCounterNumber()
      }
      else{
        this.setState({
          isDisableCounter:false,progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
  updateBranchCounter(data)
  {
    this.tempAvialableBranchService=[]
    for(var i=0;i<data.AssignedServices.length;i++)
    {
      this.tempAvialableBranchService.push(data.AssignedServices[i])
    }
    
    this.setState({counterNumber:data.SavedCounterNumber,
      selectedBranchServiceObject:this.tempAvialableBranchService,
      selectedBranchServiceId:this.tempAvialableBranchService[0].serviceId
    })
  }
  deleteCounter(id,index)
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/delete', {
      method: 'POST',
      body:JSON.stringify({
        'ids':[parseInt(id)]
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        if((this.tempAssignedServices.length-1)==index)
        {
          this.getNextCounterNumber()
          this.tempAssignedServices.splice(index,1)
          this.totalCounter=this.tempAssignedServices.length
        }
        else
        {
          for(var i=0;i<this.tempAssignedServices.length;i++)
          {
            if(parseInt(id)==this.tempAssignedServices[i].CounterId)
            {
              if(this.state.counterNumber==this.tempAssignedServices[i].SavedCounterNumber)
              {
                this.getNextCounterNumber()
              }
              this.tempAssignedServices.splice(i,1)
              this.totalCounter=this.tempAssignedServices.length
            }
          }
        }


        this.setState({assignedServices:this.tempAssignedServices})
      }
      else{
        toaster.notify(responseJson.devMessage, {
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
  getNextCounterNumber()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/getNextCounterNumber', {
      method: 'POST',
      body:JSON.stringify({
      'id':parseInt(localStorage.getItem('branchId'))
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(counterResponse => {
      if(counterResponse.applicationStatusCode==0)
      {
        this.setState({counterNumber:counterResponse.values.NextCounterNumber})
      }
      else{
        toaster.notify(counterResponse.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
  getNextCredentials()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getNextCredentials', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({NextAgentId:responseJson.values.NextAgentId,
          AgentPIN:responseJson.values.AgentPIN
        })
      }
    })
  }
  getAvilableBranchCounter()
  {
    this.tempAssigendCounter=[]
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/get/branch', {
      method: 'POST',
      body:JSON.stringify({
       'id':parseInt(localStorage.getItem('branchId'))
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        // for(var i=0;i<responseJson.branchCounterList.length;i++)
        // {
        //   this.tempAssigendCounter.push(responseJson.branchCounterList[i])
        // }
        // var isMatch=false
        // if(data.assignedBranchCounter!=null)
        // {
        //   for(var x=0;x<this.tempAssigendCounter.length;x++)
        //   {
        //     if(this.tempAssigendCounter[x].branchCounter.counterId==data.assignedBranchCounter.counterId)
        //     {
        //       isMatch=true
        //       break;
        //     }
        //     else{
        //         isMatch=false
        //       }
            
        //   }
        //   if(isMatch==false)
        //   {
        //     this.tempAssigendCounter.push({'branchCounter':data.assignedBranchCounter})
        //   }
        //   this.setState({
        //     selectedAvialableBranchCounterId:this.tempAssigendCounter[0].branchCounter.counterId,
        //     avialableBranchCounter:this.tempAssigendCounter
        //   })
        // }
        // else
        // {
        //   if(this.tempAssigendCounter.length!=0)
        //   {
        //     this.setState({
        //       selectedAvialableBranchCounterId:this.tempAssigendCounter[0].branchCounter.counterId,
        //       avialableBranchCounter:this.tempAssigendCounter
        //     })
        //   }
         
        // }


        for(var i=0;i<responseJson.branchCounterList.length;i++)
        {
          this.tempAssigendCounter.push(responseJson.branchCounterList[i])
        }
        if(this.tempAssigendCounter.length!=0)
        {
          this.setState({avialableBranchCounter:responseJson.branchCounterList,
            selectedAvialableBranchCounterId:this.tempAssigendCounter[0].branchCounter.counterId
          })
        }
        // for(var i=0;i<responseJson.branchCounterList.length;i++)
        // {
        //   if(responseJson.branchCounterList[i].assignedToAgent!=true)
        //   {
        //     responseJson.branchCounterList[i]['isDisabled']=false
        //     this.setState({selectedAvialableBranchCounterId:responseJson.branchCounterList[i].branchCounter.counterId})
        //   }
        //   else{
        //     responseJson.branchCounterList[i]['isDisabled']=true
        //   }
        // }
        this.setState({avialableBranchCounter:responseJson.branchCounterList})
      }
    })
  }
  changeAssigendCounter=e=>
  {
    // if(parseInt(e.target.value)!=0)
    // {
    //   for(var i=0;i<this.state.avialableBranchCounter.length;i++)
    //   {
    //     if(this.state.avialableBranchCounter[i].branchCounter.counterId==parseInt(e.target.value))
    //     {
    //       this.setState({selectedAvialableBranchCounterId:this.state.avialableBranchCounter[i].branchCounter.counterId})
    //     }
    //   }
    // }
    if(parseInt(e.target.value)!=0)
    {
      for(var i=0;i<this.state.avialableBranchCounter.length;i++)
      {
        if(this.state.avialableBranchCounter[i].branchCounter.counterId==parseInt(e.target.value))
        {
          if(this.state.avialableBranchCounter[i].assignedToAgent!=true)
          {
            this.setState({selectedAvialableBranchCounterId:this.state.avialableBranchCounter[i].branchCounter.counterId})
          }
          else{
            toaster.notify('Counter is already assigend',{
              duration:5000
            })
            return
          }
          
        }
      }
    }
  }
  getAgentPrevileges()
  {
    this.tempAllPrivileges=[]
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getPrivileges', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        for(var i=0;i<responseJson.privilegeList.length;i++)
        {
          responseJson.privilegeList[i]['isChecked']=false
            var key=responseJson.privilegeList[i].privilegeName
            var replaced = key.split('_').join(' ');
            responseJson.privilegeList[i].privilegeName=replaced
        }
        this.tempAllPrivileges.push(responseJson.privilegeList)
        this.setState({allPrivileges:responseJson.privilegeList})
       
      }
    })
  }
  changePriveleges(data)
  {
    for(var i=0;i<this.tempAllPrivileges[0].length;i++)
    {
      if(data.privilegeId==this.tempAllPrivileges[0][i].privilegeId)
      {
        if(this.tempAllPrivileges[0][i]['isChecked']==true)
        {
          this.tempAllPrivileges[0][i]['isChecked']=false
          
        }
        else
        {
          this.tempAllPrivileges[0][i]['isChecked']=true
          
          // this.selectedPrivileges.push(this.tempAllPrivileges[i])
        }

      }
    }
    this.setState({allPrivileges:this.tempAllPrivileges[0]})
  }
 addAgent()
 {
   this.selectedPrivileges=[]
   for(var i=0;i<this.tempAllPrivileges[0].length;i++)
   {
     if(this.tempAllPrivileges[0][i]['isChecked']==true)
     {
        var key=this.tempAllPrivileges[0][i].privilegeName
        var replaced = key.split(' ').join('_');
        this.tempAllPrivileges[0][i].privilegeName=replaced
       this.selectedPrivileges.push(JSON.parse(JSON.stringify(this.tempAllPrivileges[0][i])))
     }
   }
   if(this.selectedPrivileges.length==0)
   {
    toaster.notify('Kindly select atleast one privilage',{
      duration:5000
    })
      return
   }
   for(var i=0;i<this.selectedPrivileges.length;i++)
   {
       delete this.selectedPrivileges[i]['isChecked']
   }

  this.setState({
    formAgentValid:false,
    progress:50
  })
  let token = localStorage.getItem('sessionToken')
  var obj
  var tempObj
  if(this.tempAssignedAgent.length!=0)
  {

    for(var j=0;j<this.tempAssignedAgent.length;j++)
    {
       if(this.state.NextAgentId==this.tempAssignedAgent[j].SavedAgentId)
       {
         obj=JSON.stringify({

          "agentId": this.state.NextAgentId,
          "agentName": this.state.agentName,
          "agentPk": this.tempAssignedAgent[j].AgentPk,
          "agentPin": this.state.AgentPIN,
          "assignedCounterId": this.state.selectedAvialableBranchCounterId,
          "branchId": parseInt(localStorage.getItem('branchId')),
          "email": this.state.agentEmail,
          "privileges":this.selectedPrivileges,
          "updateStatus": 1
      
         }
         
        )
        tempObj={

          "agentId": this.state.NextAgentId,
          "agentName": this.state.agentName,
          "agentPk": this.tempAssignedAgent[j].AgentPk,
          "agentPin": this.state.AgentPIN,
          "assignedCounterId": this.state.selectedAvialableBranchCounterId,
          "branchId": parseInt(localStorage.getItem('branchId')),
          "email": this.state.agentEmail,
          "privileges":this.selectedPrivileges,
          "updateStatus": 1
      
         }
        this.tempAssignedAgent.splice(j,1)
        
      
      }
       else{

        obj=JSON.stringify({
          "agentId": this.state.NextAgentId,
          "agentName": this.state.agentName,
          "agentPin": this.state.AgentPIN,
          "assignedCounterId": this.state.selectedAvialableBranchCounterId,
          "branchId": parseInt(localStorage.getItem('branchId')),
          "email": this.state.agentEmail,
          "privileges":this.selectedPrivileges
         })
         tempObj={
          "agentId": this.state.NextAgentId,
          "agentName": this.state.agentName,
          "agentPin": this.state.AgentPIN,
          "assignedCounterId": this.state.selectedAvialableBranchCounterId,
          "branchId": parseInt(localStorage.getItem('branchId')),
          "email": this.state.agentEmail,
          "privileges":this.selectedPrivileges
         }
       }
      
    }
  }
  else{
    obj=JSON.stringify({
      "agentId": this.state.NextAgentId,
      "agentName": this.state.agentName,
      "agentPin": this.state.AgentPIN,
      "assignedCounterId": this.state.selectedAvialableBranchCounterId,
      "branchId": parseInt(localStorage.getItem('branchId')),
      "email": this.state.agentEmail,
      "privileges":this.selectedPrivileges
     })
     tempObj ={
      "agentId": this.state.NextAgentId,
      "agentName": this.state.agentName,
      "agentPin": this.state.AgentPIN,
      "assignedCounterId": this.state.selectedAvialableBranchCounterId,
      "branchId": parseInt(localStorage.getItem('branchId')),
      "email": this.state.agentEmail,
      "privileges":this.selectedPrivileges
     }
  }
  if(tempObj.updateStatus!=undefined && tempObj.updateStatus==1)
  {
    if(this.tempAssigendCounter.length!=0)
    {
      if(this.state.selectedAvialableBranchCounterId!=this.state.counterIdTracker)
      {
        for(var o=0;o<this.tempAssigendCounter.length;o++)
        {
          if(this.tempAssigendCounter[o].branchCounter.counterId==this.state.selectedAvialableBranchCounterId)
          {
            if(this.tempAssigendCounter[o].assignedToAgent==true)
            {
              toaster.notify('Counter is already assigned',{
                duration:5000
              })
              return
            }
          
          }
        }
      }

    }
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/update', {
      method: 'POST',
      body:obj,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.selectedPrivileges=[]
        this.setState({agentEmail:'',agentName:'',progress:100})
        this.tempAssignedAgent.push(responseJson.values)
        this.totalAgentCounter=this.tempAssignedAgent.length
        this.setState({

            assignedAgents:this.tempAssignedAgent,
            formAgentValid:false,
            isShowUpdate:false
        })
        global.isBranchAgentCounterUpdated.next(true)
        global.isNotificationUpdated.next(true)
        this.getAvilableBranchCounter()
        this.getAgentPrevileges()
        this.getNextCredentials()
      }
      else{
        this.setState({
          formAgentValid:false,progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
  else
  {
    if(this.tempAssigendCounter.length!=0)
    {
       for(var o=0;o<this.tempAssigendCounter.length;o++)
       {
         if(this.tempAssigendCounter[o].branchCounter.counterId==this.state.selectedAvialableBranchCounterId)
         {
           if(this.tempAssigendCounter[o].assignedToAgent==true)
           {
             toaster.notify('Counter is already assigned',{
               duration:5000
             })
             return
           }
         
         }
       }
    }
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/add', {
      method: 'POST',
      body:obj,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.selectedPrivileges=[]
        this.setState({agentEmail:'',agentName:''})
        this.tempAssignedAgent.push(responseJson.values)
        this.totalAgentCounter=this.tempAssignedAgent.length
        this.setState({
            assignedAgents:this.tempAssignedAgent,
            formAgentValid:false,
            isShowUpdate:false,
            progress:100
        })
        global.isNotificationUpdated.next(true)
        global.isBranchAgentCounterUpdated.next(true)
        this.getAvilableBranchCounter()
        this.getAgentPrevileges()
        this.getNextCredentials()
      }
      else{
        this.setState({
          formAgentValid:false,progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
 
 }
 updateAgent(data)
 {
  //  this.tempAvialableBranchService=[]
   for(var i=0;i<data.AssignedPrivileges.length;i++)
   {
    for(var j=0;j<this.tempAllPrivileges[0].length;j++)
    {
      if(data.AssignedPrivileges[i].privilegeId==this.tempAllPrivileges[0][j].privilegeId)
      {
        this.tempAllPrivileges[0][i]['isChecked']=true
      }
    }
   }
   
   this.branchName=data.BranchName
   this.updateAgentEmail(data.Email)
   this.updateAgentName(data.AgentName)
   this.setState({NextAgentId:data.SavedAgentId,
    isShowUpdate:true,
      AgentPIN:data.AgentPIN,
      selectedAvialableBranchCounterId:data.AssignedCounterId,
      counterIdTracker:data.AssignedCounterId,
      allPrivileges:this.tempAllPrivileges[0],
      formAgentValid:true
    //  selectedBranchServiceId:this.tempAvialableBranchService[0].serviceId
   })
 }
 deleteAgent(id)
 {
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/delete', {
    method: 'POST',
    body:JSON.stringify({
      'ids':[parseInt(id)]
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      for(var i=0;i<this.tempAssignedAgent.length;i++)
      {
        if(id==this.tempAssignedAgent[i].AgentPk)
        {
          if(this.state.NextAgentId==this.tempAssignedAgent[i].SavedAgentId)
          {
            this.getNextCredentials()
          }
          this.tempAssignedAgent.splice(i,1)
          this.totalAgentCounter=this.tempAssignedAgent.length
          this.getAllAgents()
          this.getAvilableBranchCounter()
        }
      }

      this.setState({assignedAgents:this.tempAssignedAgent})
    }
    else{
      toaster.notify(responseJson.devMessage, {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
  })
 }
 deleteAllAgent()
 {
   var data=[]
   for(var i=0;i<this.tempAssignedAgent.length;i++)
    {
      data.push(this.tempAssignedAgent[i].AgentPk)
    }
    this.setState({
      isDisableDeleteAgent:true,progress:50
    })
    let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/delete', {
    method: 'POST',
    body:JSON.stringify({
      'ids':data
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      this.getAvilableBranchCounter()
      this.getAllAgents()
      this.tempAssignedAgent=[]
      this.totalAgentCounter=0
      this.setState({assignedAgents:[],isDisableDeleteAgent:false,progress:100})
    }
    else{
      this.setState({isDisableDeleteAgent:false,progress:100})
      toaster.notify(responseJson.devMessage, {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
  })

 }
 updateArabicServiceCenterName = (serviceCenterArabicName) => {
  this.setState({serviceCenterArabicName}, this.validateArabicServiceCenterName)
}
validateArabicServiceCenterName = () => {
  const {serviceCenterArabicName} = this.state;
  let serviceCenterArabicNameValid = true;
  let errorMsg = {...this.state.errorMsg}

  if (serviceCenterArabicName.length == 0 ||serviceCenterArabicName=='' ) {
    serviceCenterArabicNameValid = false;
    errorMsg.serviceCenterArabicName = 'Required'
  }
  else if(!/^[\u0621-\u064A\040]+$/.test(serviceCenterArabicName))
  {
    serviceCenterArabicNameValid = false;
    errorMsg.serviceCenterArabicName = 'Only arabic alphabets are allowed'
  }
  else if (serviceCenterArabicName.length <2) {
    serviceCenterArabicNameValid = false;
    errorMsg.serviceCenterArabicName = 'Name should be at least 2 characters'
  }
  this.setState({serviceCenterArabicNameValid, errorMsg}, this.validateForm)
}
togglecollapse() {
  this.setState({ collapse: !this.state.collapse });
}
changeExistingAgentStatus()
{
  var temp=!this.state.isShowExisting
  this.setState({
    isShowExisting:temp
  })
}
changeNewAgentStatus()
{
  var temp=!this.state.isShowNew
  this.setState({
    isShowNew:temp
  })
}
resetExistingAgent()
{
  for(var i=0;i<this.tempAvialableAgents.length;i++)
    {
      this.tempAvialableAgents[i].agent['isChecked']=false
    }
    this.setState({allAgents:this.tempAvialableAgents})
}
resetNewAgent(){
  for(var i=0;i<this.selectedPrivileges.length;i++)
   {
      this.selectedPrivileges[i]['isChecked']=false
   }
  this.selectedPrivileges=[]
  this.tempAllPrivileges=[]
  this.setState({agentEmail:'',agentName:'',allPrivileges:[]})
  this.getNextCredentials()
  this.getAgentPrevileges()
}
resetCounter(){
  this.tempAvialableBranchService=[]
  this.setState({
    selectedBranchServiceObject:[],
    branchServices:[]
  })
  this.getNextCounterNumber()
  this.getBranchServices()
}
deleteAllCounter(){
  var data=[]
  for(var i=0;i<this.tempAssignedServices.length;i++)
  {
    
   data.push(this.tempAssignedServices[i].CounterId)
  }
  this.setState({
    isDisableDeleteCounter:true,
    progress:50
  })
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/delete', {
    method: 'POST',
    body:JSON.stringify({
      'ids':data
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  }).then(response => response.json()).then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      this.getNextCounterNumber()
      this.setState({
        isDisableDeleteCounter:false,
        progress:100
      })
      this.tempAssignedServices=[]
      this.totalCounter=this.tempAssignedServices.length
      this.setState({assignedServices:[]})
    }
    else{
      this.setState({
        isDisableDeleteCounter:false,
        progress:100
      })
      toaster.notify(responseJson.devMessage, {
        duration:5000 // This notification will not automatically close
      });
    }
  })      
}
navigateToBranchList() {
  global.isBranchAgentCounterUpdated.next(true);
  this.props.history.push('/branch-list');
}
editLocation() { 
  this.setState({ isLocationModal: true }); 

}
fetchPlaces(mapProps, map) {
  const {google} = mapProps;
  const service = new google.maps.places.PlacesService(map);
}
onMapClicked = (event) => {
  console.log(event);
  // if (this.state.showingInfoWindow) {
  //   this.setState({
  //     showingInfoWindow: false,
  //     activeMarker: null
  //   })
  // }
};
  render() {

    return (
      <div className="animated fadeIn">
        <LoadingBar
        color='#2f49da'
        progress={this.state.progress}
        onLoaderFinished={() => this.setState({
          progress:0
        })}
      />
        <DefaultHeader/>
        <Row className="mb-3 hidden">          
          <Col sm="6">
              {global.isShowArabicLanguage==false &&<h4 className="mb-0">New Branch</h4>}
              {global.isShowArabicLanguage==true &&<h4 className="mb-0">فرع جديد</h4>}
            </Col>             
          <Col sm="6" className="text-right">
          
          <Steps current={this.state.next}  direction="horizontal">
                <Steps.Step title="Details" />
                <Steps.Step title="Counters" />
                <Steps.Step title="Agents" />
              </Steps>
           
          </Col>
        </Row>


        <Card>
              
              <CardBody>
        


          {(this.state.next==0) && (
            <div className="steps-content">
                <Row>
          <Col md="7" className="pr-lg-3 pr-xl-4">
          {global.isShowArabicLanguage==false &&<h4>Basic</h4>}
          {global.isShowArabicLanguage==true &&<h4>معلومات اساسية</h4>}
                <FormGroup className="input-line">
                {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Branch Name (English)</Label>}
                {global.isShowArabicLanguage==true &&<Label className="text-light-grey">(اسم الفرع (بالإنجليزية</Label>}
                {global.isShowArabicLanguage==false &&  <Input type="text" placeholder="Branch Name" 
                value={this.state.serviceCenterName} onChange={(e) => this.updateServiceCenterName(e.target.value)} />}
                {global.isShowArabicLanguage==true &&  <Input type="text" placeholder="اسم الفرع" 
                value={this.state.serviceCenterName} onChange={(e) => this.updateServiceCenterName(e.target.value)} />}
                     < ValidationMessage valid={this.state.serviceCenterNameValid} message={this.state.errorMsg.serviceCenterName} />
                  </FormGroup>
                  <FormGroup className="input-line">
                  {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Branch Name (Arabic)</Label>}
                  {global.isShowArabicLanguage==true &&<Label className="text-light-grey">(اسم الفرع (بالعربية</Label>}

                  {global.isShowArabicLanguage==false &&<Input type="text" placeholder="Branch Name" 
                    value={this.state.serviceCenterArabicName} onChange={(e) => this.updateArabicServiceCenterName(e.target.value)} />}
                  {global.isShowArabicLanguage==true &&<Input type="text" placeholder="اسم الفرع" 
                    value={this.state.serviceCenterArabicName} onChange={(e) => this.updateArabicServiceCenterName(e.target.value)} />}
                     < ValidationMessage valid={this.state.serviceCenterArabicNameValid} message={this.state.errorMsg.serviceCenterArabicName} />
                  </FormGroup>
                  <FormGroup className="input-line">
                  {global.isShowArabicLanguage==false && <Label className="text-light-grey">Available Services</Label>}
                  {global.isShowArabicLanguage==true && <Label className="text-light-grey">الخدمات المتاحة</Label>}

                    <select className="form-control" onChange={this.changeServiceSector} >
                    {global.isShowArabicLanguage==false && <option value='0' key='0'>
                        Services
                      </option>}
                      {global.isShowArabicLanguage==true && <option value='0' key='0'>
                      اختر من القائمة
                      </option>}
                    {this.state.serviceSector.map(service => (
                      <option value={service.serviceTAB.serviceId} key={service.serviceTAB.serviceId}>
                        {service.serviceTAB.serviceName}
                      </option>
                    ))}
                    </select>
                  </FormGroup>
                  {this.state.selectedServiceSectorObject.map(service => (
                      <Alert color="light" key={service.serviceId} className="theme-alert" isOpen={this.state.visible} toggle={()=>this.onDismissAvialableService(service.serviceId)}>
                        {service.serviceName}
                      </Alert>  
                  ))}
                 

                 {global.isShowArabicLanguage==false && <h6 className="text-light-grey">Working Hours</h6>}
                 {global.isShowArabicLanguage==true && <h6 className="text-light-grey">ساعات العمل</h6>}

                  <Row>
                    <Col sm="4">
                      <FormGroup className="input-line">
                      {global.isShowArabicLanguage==false && <Label className="text-light-grey">Week Day</Label>}
                      {global.isShowArabicLanguage==true && <Label className="text-light-grey">يوم الأسبوع</Label>}
                      <select className="form-control" onChange={this.changeWeekDays} >
                      {this.weekDays.map(week => (
                      <option value={week.id} key={week.id}>
                        {week.name}
                      </option>
                    ))}
                      </select>
                    </FormGroup>
                    </Col>
                    <Col sm="6">
                      <FormGroup className="input-line">
                      {global.isShowArabicLanguage==false && <Label className="text-light-grey">Time</Label>}
                      {global.isShowArabicLanguage==true && <Label className="text-light-grey">الوقت</Label>}

                      <Row>
                        <Col sm="5">
                        
                        <TimePickerComponent id="starttimepicker" value={this.state.startTime}  max={this.state.endTime}  allowEdit={false}
                        disabled={false}
                        onChange={(time) => this.changeStartTime(time)}  />
                        </Col>
                        <Col sm="2" className="text-light-grey no-wrap">To</Col>
                        <Col sm="5">
                        <TimePickerComponent id="endtimepicker" value={this.state.endTime} min={this.state.startTime} allowEdit={false}
                        disabled={false}
                        onChange={(time) => this.changeEndTime(time)} />
                        </Col>
                      </Row>
                      
                    </FormGroup>
                    </Col>
                    
                    <Col sm="2" className="pt-3">
                      <Button outline color="warning" onClick={()=>this.addWeekDays()}>Add</Button>
                    </Col>
                  </Row>
                  
                  {this.state.weekDaysObject.map(weekDays => (
                    <Alert color="light" key={weekDays.day} className="theme-alert" isOpen={this.state.visible} toggle={()=>this.onDismissWeekDays(weekDays.day)}>
                      <Row>
                        <Col sm="6">{weekDays.name}</Col>
                        <Col sm="6">{weekDays.openingTime} - {weekDays.closingTime}</Col>
                      </Row>
                    </Alert>
                  ))}
              <Button color="warning" disabled={!this.state.formValid} onClick={()=>{this.nextStep(1)}}>Save&nbsp;and&nbsp;Continue</Button>
              </Col>
          <Col md="5" className="pl-lg-4">
          
          <FormGroup className="input-line">
                    <Label className="text-light-grey" htmlFor="exampleEmail" >Email</Label>
                    
                    <Input type="email" placeholder="Email" autoComplete="username" name="email" id="exampleEmail"
                    value={this.state.email} onChange={(e) => this.updateEmail(e.target.value)}/>
                    < ValidationMessage valid={this.state.emailValid} message={this.state.errorMsg.email} />
                  </FormGroup>
          <FormGroup className="input-line">
            <Label className="text-light-grey">Phone</Label>
            
            <Input type="text" placeholder="Phone" autoComplete="username"
            value={this.state.phone} onChange={(e) => this.updatePhoneNo(e.target.value)} />
            < ValidationMessage valid={this.state.phoneValid} message={this.state.errorMsg.phone} />
          </FormGroup>
            {global.isShowArabicLanguage==false && <Button outline block color="warning" className="mb-3" onClick={()=>this.editLocation()}><i className="icon2-location"></i> Add Location</Button>}
            {global.isShowArabicLanguage==true && <Button outline block color="warning" className="mb-3" onClick={()=>this.editLocation()}><i className="icon2-location"></i> أضف الموقع</Button>}

          </Col>
        
        </Row>
            </div>
            )}
          {(this.state.next==2) && (
              <div className="steps-content">
                <Row>
          <Col md="7" className="pr-lg-3 pr-xl-4">

          <Card className="card-box" >
              <CardBody className="pt-4">
              <Row>
                  <Col sm="8">
                  {global.isShowArabicLanguage==false &&<h4>Select From Existing List</h4>}
                  {global.isShowArabicLanguage==true &&<h4>اختر العميل المتواجد</h4>}

                  </Col>
                  <Col sm="4" className="text-right">
                      <Link className="text-warning f20" onClick={()=>this.changeExistingAgentStatus()}><i className={(this.state.isShowExisting ? 'font-lg font-weight-bold icon2-cancel' : 'icon2-addition-sign')}></i></Link>
                  </Col>
                </Row>

                {this.state.isShowExisting==true && <div className="mt-4">
                  <FormGroup className="form-inline d-flex mb-3">

                        <InputGroup className="flex-fill">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText className="bg-transparent">
                            <i className="icon2-magnifying-glass"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Search" className="b-l-0" id="inputIsValid"  
                    value={this.state.searchName} onChange={(e) => this.changeSearch(e.target.value)}
                    onKeyPress={event => {
                      if (event.key === 'Enter') {
                        this.search()
                      }
                    }}
                  />
                      </InputGroup>

                  <button className="btn btn-outline-secondary ml-2" onClick={()=>this.showFilter()}><i className="icon2-filter2"></i> Filters</button>
                </FormGroup>
                

                {this.state.isFilter==true && <div className="mb-4">
                  <Row>
                    <Col>
                    <FormGroup check>
                      <div className="radio">
                        <Input className="form-check-input" type="radio" id="existing-checkbox1" name="existing-radio" value="option1" onChange={()=>this.selectAllAgents('All')} />
                        {global.isShowArabicLanguage==false &&<Label className="form-check-label" check htmlFor="existing-checkbox1">Select All . {this.totalAgents}</Label>}
                        {global.isShowArabicLanguage==true &&<Label className="form-check-label" check htmlFor="existing-checkbox1">اختيار الكل . {this.totalAgents}</Label>}

                      </div>
                      </FormGroup>
                    </Col>
                    <Col>
                    <FormGroup check>
                      <div className="radio">
                        <Input className="form-check-input" type="radio" id="existing-checkbox2" name="existing-radio" value="option2" onChange={()=>this.selectAllAgents('Assigned')} />
                        {global.isShowArabicLanguage==false &&<Label className="form-check-label" check htmlFor="existing-checkbox2">Assigned . {this.signedAgents}</Label>}
                        {global.isShowArabicLanguage==true &&<Label className="form-check-label" check htmlFor="existing-checkbox2">تعيين. {this.signedAgents}</Label>}
                        </div>
                      </FormGroup>
                    </Col>
                    <Col>
                    <FormGroup check>
                      <div className="radio">
                        <Input className="form-check-input" type="radio" id="existing-checkbox3" name="existing-radio" value="option3" onChange={()=>this.selectAllAgents('UnAssigned')} />
                        {global.isShowArabicLanguage==false &&<Label className="form-check-label" check htmlFor="existing-checkbox3">UnAssigned . {this.unsignedAgents}</Label>}
                        {global.isShowArabicLanguage==true &&<Label className="form-check-label" check htmlFor="existing-checkbox3">غير معين . {this.unsignedAgents}</Label>}

                        </div>
                      </FormGroup>
                    </Col>
                  </Row>
                    
                      
                      
                </div>}
                  <Table className="m-0 table-md">
                    {this.state.allAgents.map((agent,index) => (
                      <tr key={index}>
                        <td width="50px" className="text-center">
                        <div className="checkbox">
                          <input type="checkbox"  checked={agent.agent.isChecked} value={agent.agent.serviceCenterEmployee.user.userId} onChange={()=>this.changeAgentBranch(agent)}></input>
                          <label  data-content={agent.agent.serviceCenterEmployee.user.userId}></label>
                        </div>
                        </td>
                        <td width="110px" className="text-primary">{agent.agent.serviceCenterEmployee.employeeNumber}</td>
                        <td className="text-light-grey">{agent.agent.serviceCenterEmployee.user.name}</td>
                      </tr>
                    ))}
                      </Table>

                      
                  <Button outline color="warning" onClick={()=>this.resetExistingAgent()}>Reset</Button>
                  {global.isShowArabicLanguage==false &&<Button color="warning" className="ml-2"  onClick={()=>{this.updateExistingAgent()}}>Add Agent</Button>}
                  {global.isShowArabicLanguage==true &&<Button color="warning" className="ml-2"  onClick={()=>{this.updateExistingAgent()}}>إضافة العميل</Button>}
                  </div>
                }
                



                </CardBody>
                </Card>

            <Card className="card-box mb-0">
              <CardBody className="pt-4">
              <Row>
                  <Col sm="8">
                  {this.state.isShowUpdate==false && global.isShowArabicLanguage==false && <h4>Create New Agent</h4>}
                {this.state.isShowUpdate==false && global.isShowArabicLanguage==true && <h4>إضافة عميل جديد</h4>}
                {this.state.isShowUpdate==true && global.isShowArabicLanguage==false && <h4>Update Agent</h4>}
                {this.state.isShowUpdate==true && global.isShowArabicLanguage==true && <h4>عامل التحديث</h4>}

                  </Col>
                  <Col sm="4" className="text-right">
                      <Link className="text-warning f20" onClick={()=>this.changeNewAgentStatus()}><i className={(this.state.isShowNew ? 'font-lg font-weight-bold icon2-cancel' : 'icon2-addition-sign')}></i></Link>
                  </Col>
                </Row>
             {this.state.isShowNew==true && <div>
              <Row className="mt-4">
                <Col sm="6">
                <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Agent ID</Label>}
                    {global.isShowArabicLanguage==true &&<Label className="text-light-grey">رمز العميل</Label>}
                    <Input type="text" placeholder="Service Center Name" className="text-primary" autoComplete="username" readOnly value={this.state.NextAgentId} />
                    {global.isShowArabicLanguage==false &&<small className="text-light-grey"><span className="text-danger">*</span> Generated by System</small>}
                    {global.isShowArabicLanguage==true &&<small className="text-light-grey"><span className="text-danger">*</span> تم إنشاؤها بواسطة النظام</small>}
                  </FormGroup>
                </Col>
                <Col sm="6">
                {this.state.isShowUpdate==false && <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false && <Label className="text-light-grey">PIN</Label>}
                    {global.isShowArabicLanguage==true && <Label className="text-light-grey">رقم التعريف الشخصي</Label>}
                    <Input type="text" placeholder="Service Center Name" className="text-primary" autoComplete="username" readOnly value={this.state.AgentPIN} />
                    {global.isShowArabicLanguage==false && <small className="text-light-grey"><span className="text-danger">*</span> Changed on first login</small>}
                    {global.isShowArabicLanguage==true && <small className="text-light-grey"><span className="text-danger">*</span> يستبدل عند أول تسجيل دخول</small>}

                  </FormGroup>}
                  {this.state.isShowUpdate==true && <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false && <Label className="text-light-grey">PIN</Label>}
                    {global.isShowArabicLanguage==true && <Label className="text-light-grey">رقم التعريف الشخصي</Label>}
                    <Input type="text" placeholder="Service Center Name" className="text-primary" autoComplete="username" readOnly value='......' />
                    {global.isShowArabicLanguage==false && <small className="text-light-grey"><span className="text-danger">*</span> Changed on first login</small>}
                    {global.isShowArabicLanguage==true && <small className="text-light-grey"><span className="text-danger">*</span> يستبدل عند أول تسجيل دخول</small>}
                  </FormGroup>}
                </Col>
              </Row>
              <FormGroup className="input-line">
                    <Label className="text-light-grey">Assigned Counters</Label>
                    <select className="form-control" onChange={this.changeAssigendCounter} value={this.state.selectedAvialableBranchCounterId}>
                    {this.state.avialableBranchCounter.map(service => (
                      <option value={service.branchCounter.counterId} key={service.branchCounter.counterId}>
                        {service.branchCounter.counterNumber}
                      </option>
                    ))}
                    </select>
                  </FormGroup>
                <FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label>
                    <Input type="text" placeholder="Agent Name" 
                     value={this.state.agentName} onChange={(e) => this.updateAgentName(e.target.value)} />
                     < ValidationMessage valid={this.state.agentNameValid} message={this.state.errorMsg.agentName} />
                  </FormGroup>
                  {this.state.isShowUpdate==false && <FormGroup className="input-line">
                    <Label className="text-light-grey">Email</Label>
                    <Input type="email" placeholder="Email" autoComplete="username" name="email" id="exampleEmail"
                    value={this.state.agentEmail} onChange={(e) => this.updateAgentEmail(e.target.value)}/>
                    < ValidationMessage valid={this.state.agentEmailValid} message={this.state.errorMsg.agentEmail} />
                  </FormGroup>}
                  {this.state.isShowUpdate==true && <FormGroup className="input-line">
                    <Label className="text-light-grey">Email</Label>
                    <Input type="email" placeholder="Email" autoComplete="username" name="email" id="exampleEmail"
                    value={this.state.agentEmail} onChange={(e) => this.updateAgentEmail(e.target.value)}/>
                    < ValidationMessage valid={this.state.agentEmailValid} message={this.state.errorMsg.agentEmail} />
                  </FormGroup>}
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Branch</Label>
                    <Input type="text" autoComplete="username" readOnly value={this.branchName} />
                  </FormGroup>
                  

                  <div className="mb-2">
                  {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Privilages</Label>}
                  {global.isShowArabicLanguage==true &&<Label className="text-light-grey">الصلاحيات</Label>}

                  {this.state.allPrivileges.map((prev,index) => (
                    <div className="checkbox" key={index}>
                    <input type="checkbox"  checked={prev.isChecked} value={prev.privilegeId} onChange={()=>this.changePriveleges(prev)}></input>
                    <label  data-content={prev.privilegeName}>{prev.privilegeName}</label>
                  </div>
                  ))}
                     
                      </div>


                  <Button outline color="warning" onClick={()=>this.resetNewAgent()}>Reset</Button>
                {this.state.isShowUpdate==false && global.isShowArabicLanguage==false && <Button color="warning" className="ml-2" disabled={!this.state.formAgentValid} onClick={()=>{this.addAgent()}}>Add Agent</Button>}
                {this.state.isShowUpdate==false && global.isShowArabicLanguage==true && <Button color="warning" className="ml-2" disabled={!this.state.formAgentValid} onClick={()=>{this.addAgent()}}>إضافة العميل</Button>}
                {this.state.isShowUpdate==true && global.isShowArabicLanguage==false && <Button color="warning" className="ml-2" disabled={!this.state.formAgentValid} onClick={()=>{this.addAgent()}}>Update Agent</Button>}
                {this.state.isShowUpdate==true && global.isShowArabicLanguage==true && <Button color="warning" className="ml-2" disabled={!this.state.formAgentValid} onClick={()=>{this.addAgent()}}>عامل التحديث</Button>}

               </div>}
             


              </CardBody>
            </Card>
            

              </Col>
          <Col md="5" className="pl-lg-4">
          


          <Card className="bg-primary">
            
            <CardBody>

            <h4 className="mb-3">Agents . {this.totalAgentCounter}</h4>

              
            {this.state.assignedAgents.map((agent,index) => (
                <Card className="card-box card-list"  key={index}>
                <CardHeader>
                <p className="text-warning mb-0">{agent.SavedAgentId}</p>
            <h5 className="text-white float-left">{agent.AgentName}</h5>
                  <div className="card-header-actions float-right">
                    <Link onClick={()=>this.updateAgent(agent)}><i className="icon2-edit-pencil"></i></Link>
                    <Link className="ml-3"><i className="font-weight-bold icon2-cancel" onClick={()=>this.deleteAgent(agent.AgentPk)} ></i></Link>
                  </div>
                </CardHeader>
                <CardBody>
                <Table responsive className="m-0 table-borderless table-sm">
                <thead>
                <tr>
                  <td width="35%">{agent.AssignedCounterNumber}</td>
                  <td></td>
                  </tr>
                  <tr>
                  {global.isShowArabicLanguage==false &&<td width="35%">Privileges {agent.AssignedPrivileges.length}</td>}
                  {global.isShowArabicLanguage==true &&<td width="35%">الصلاحيات {agent.AssignedPrivileges.length}</td>}
                  <td>{agent.BranchName}</td>
                  </tr>
                  </thead>
                </Table>
                </CardBody>
              </Card>
            ))}
              <Row className="no-wrap">
                <Col sm="5"><Button outline block color="light" 
                disabled={this.state.isDisableDeleteAgent}
                onClick={()=>this.deleteAllAgent()}>Cancel</Button></Col>
                <Col sm="7"><Button block color="light" onClick={()=>this.navigateToBranchList()}>Save&nbsp;&&nbsp;Continue</Button></Col>
              </Row>
            </CardBody>
          </Card>
          </Col>
        </Row>
        </div>
        )}
          
        {(this.state.next==1) && (
        <div className="steps-content">
        <Row>
          <Col md="7" className="pr-lg-3 pr-xl-4">
            {global.isShowArabicLanguage==false && <h4>Add New Counter</h4>}
            {global.isShowArabicLanguage==true && <h4>إضافة شباك جديد</h4>}
            <FormGroup className="input-line">
              {global.isShowArabicLanguage==false &&   <Label className="text-light-grey">Counter Number</Label>}
              {global.isShowArabicLanguage==true &&   <Label className="text-light-grey">رقم الشباك</Label>}
              <Input type="text" placeholder="Service Center Name" autoComplete="username" readOnly value={this.state.counterNumber} />
              {global.isShowArabicLanguage==false &&   <Label className="text-light-grey"><small><span className="text-danger">*</span> Generated by the system</small></Label>}
              {global.isShowArabicLanguage==true &&   <Label className="text-light-grey"><small><span className="text-danger">*</span> تم إنشاؤها بواسطة النظام</small></Label>}
            </FormGroup>

            <FormGroup className="input-line">
              {global.isShowArabicLanguage==false && <Label className="text-light-grey">Available Services</Label>}
              {global.isShowArabicLanguage==true && <Label className="text-light-grey">الخدمات المتاحة</Label>}
              <select className="form-control" onChange={this.changeBranchService} >
              {global.isShowArabicLanguage==false && <option value='0' key='0'>
                    Services
                  </option>}
                  {global.isShowArabicLanguage==true && <option value='0' key='0'>
                  اختر من القائمة
                  </option>}
                {this.state.branchServices.map(service => (
                  <option value={service.serviceId} key={service.serviceId}>
                    {service.serviceName}
                  </option>
                ))}
                </select>
            </FormGroup>
            {this.state.selectedBranchServiceObject.map((service,index) => (
              <Alert color="light"  key={index} className="theme-alert" isOpen={this.state.visible} toggle={()=>this.onDismissAvialableBranchService(service.serviceId)}>
                {service.serviceName}
              </Alert>  
            ))}
            <Button outline color="warning" onClick={()=>this.resetCounter()}>Reset</Button>
            {global.isShowArabicLanguage==false && <Button color="warning" className="ml-2" 
            disabled={this.state.isDisableCounter}
            onClick={()=>{this.addBranchCounter()}}>Add Counter</Button>}
            {global.isShowArabicLanguage==true && <Button color="warning" className="ml-2" 
            disabled={this.state.isDisableCounter}
            onClick={()=>{this.addBranchCounter()}}>إضافة شباك</Button>}
          </Col>
          <Col md="5" className="pl-lg-4"> 
            <Card className="bg-primary"> 
              <CardBody> 
              <h4 className="mb-3">Counters . {this.totalCounter}</h4> 
              {this.state.assignedServices.map((service,index) => 
                <Card className="card-box card-list" key={index}>
                  <CardHeader>
                    <h5 className="text-white float-left">{service.SavedCounterNumber}</h5>
                    <div className="card-header-actions float-right">
                      <Link  onClick={()=>this.updateBranchCounter(service)} ><i className="icon2-edit-pencil"></i></Link>
                      <Link className="ml-3"  
                      onClick={()=>this.deleteCounter(service.CounterId,index)}><i className="font-weight-bold icon2-cancel"></i></Link>
                    </div>
                  </CardHeader>
                  <CardBody>
                    <Table responsive className="m-0 table-borderless table-sm">
                      <thead>
                      <tr>
                        <td width="35%">Services {service.AssignedServices.length}</td> 
                      </tr>
                      </thead> 
                    </Table>
                  </CardBody>
                </Card>
              )}
              <Row>
                <Col sm="6"><Button outline block color="light"
                disabled={this.state.isDisableDeleteCounter}
                onClick={()=>this.deleteAllCounter()}>Cancel</Button></Col>
                <Col sm="6"><Button block color="light" disabled={this.state.assignedServices.length==0} 
                onClick={()=>{this.nextStep(2)}} >Save&nbsp;&&nbsp;Continue</Button></Col>
              </Row>
              </CardBody>
            </Card> 
          </Col> 
        </Row>
        </div>
        )}
        </CardBody>
      </Card>


      <Modal isOpen={this.state.isLocationModal} className={'modal-lg ' + this.props.className}>
        <ModalHeader>Add Address</ModalHeader>
        <ModalBody>
          <FormGroup className="input-line">
              <Label className="text-light-grey" style={{width:"100%",paddingBottom: 10,}}>Address</Label>
              <Label className="" style={{width:"100%",fontSize:15,color:'black',borderBottomWidth:1,borderBottomColor:'#ccc',borderBottomStyle:'solid',paddingBottom: 5,}}>{this.state.address}</Label>
          </FormGroup>
          <div style={{height:400}}>
            <Map google={this.props.google} onReady={this.fetchPlaces} 
              onClick={(t, map, coord)=>{
              const { latLng } = coord;
              const lat = latLng.lat();
              const lng = latLng.lng();
              // console.log(t, map, coord);
              console.log(lat);
              console.log(lng);
              Geocoder.from(lat, lng).then(json => {
                var addressComponent = json.results[0].formatted_address;
                console.log(json);
                this.setState({address:addressComponent})
              }).catch(error => console.warn(error));
              this.setState({lat:lat,long:lng})
              }}
              zoom={14} containerStyle={containerStyle} style={mapStyles} initialCenter={{ lat: this.state.lat, lng: this.state.long}}>
                <Marker position={{ lat: this.state.lat, lng: this.state.long}} />
            </Map>
          </div>
            
          </ModalBody>
          <ModalFooter>
          <Button outline color="primary" onClick={()=>{this.setState({isLocationModal:false,address:''})}}>Cancel</Button>
            <Button color="primary" disabled={this.state.address==''} onClick={()=>{this.setState({isLocationModal:false})}}>Save</Button>
          </ModalFooter>
        </Modal>
        
      </div>
    );
  }
}
// export default Breadcrumbs;
export default GoogleApiWrapper({
  apiKey: 'AIzaSyDLN9J4HC_D04AFVx1ZK5n0MQjJaO6YUko'
})(Breadcrumbs);
