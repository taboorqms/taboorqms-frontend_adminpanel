import React, { Component, Suspense } from 'react';
import { NavLink } from 'react-router-dom';
import * as router from 'react-router-dom';
import routes from './routes';
import { Nav, NavItem } from 'reactstrap';
import { AppAsideToggler, AppSidebarToggler,AppAside,AppBreadcrumb2 as AppBreadcrumb } from '@coreui/react';
const DefaultAside = React.lazy(() => import('./profileAside'));
const NotificationAside = React.lazy(() => import('./notificationAside'));
class Home extends Component {
  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
  constructor(){
    super()
    global.isShowProfile=false
    global.isShowNotification=false
    this.state = { 
      isShowNotification:false,
      isShowProfile:false,
      isShowProfileAsidePrev:false,
      isShowNotificationDot:false,
      profileDetails:'',
  };
  }
  
  componentDidMount() {
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Admin_Panel_View_Only')
        {
          this.setState({isShowProfileAsidePrev:true})
        }
      }
    }
    this.getNotifications()
  }
  onShow(data) {
    if(data=='notification'){
      // global.isShowProfile=false;
      // global.isShowNotification=true;  
      this.setState({extra:true,isShowProfile:false,isShowNotification:true,});
    }
    else{
      // global.isShowNotification=false;
      // global.isShowProfile=true;
      this.setState({extra:true,isShowNotification:false,isShowProfile:true,});
    }
  }
  getNotifications()
  {
    this.tempNotifications=[]
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/notification/user/all', {
      method: 'POST',
      body:JSON.stringify({
        "pageNo": 0,
        "limit": 10    
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).then(response => response.json()).then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        if(responseJson.userNotificationList.length!=0)
        {
          this.setState({
            isShowNotificationDot:true
          })
        }
        else{
          this.setState({
            isShowNotificationDot:false
          }) 
        }
      }
    })
  }
  render() {
    return (
        <div className="admin-header">
          <AppSidebarToggler className="d-md-down-none" display="lg" />
          <AppBreadcrumb className="page-title" appRoutes={routes} router={router}/>
          <Nav className="admin-nav" navbar>
          {this.state.isShowProfileAsidePrev==true &&(
            <AppAsideToggler className="d-md-down-none" >
              <NavItem className="d-md-down-none" onClick={()=>this.onShow('notification')}>
                <NavLink to="#" className="nav-link top-icon position-relative" >
                  <i className="icon2-bell-notify"></i>
                  {this.state.isShowNotificationDot==true &&<span className="icon2-ellipse text-danger notify-active"></span>}
                </NavLink>
              </NavItem>
            </AppAsideToggler>
          )}
          {this.state.isShowProfileAsidePrev==true &&  (
            <AppAsideToggler className="d-md-down-none profile-toggle-icon" > 
              {(!global.profileimg) && (
              <NavItem className="d-md-down-none m-0 ml-3" onClick={()=>this.onShow('profile')}>
                <NavLink to="#" className="nav-link profile-menu">A</NavLink>
              </NavItem>
              )}
              {(global.profileimg) && (
              <NavItem className="d-md-down-none m-0 ml-3" onClick={()=>this.onShow('profile')}>
                <img src={"data:image/png;base64,"+global.profileimg} style={{width:50,height:50,objectFit:'cover'}}/>
              </NavItem>
              )}
            </AppAsideToggler>
          )}
          </Nav>
         <AppAside fixed>
            <Suspense fallback={this.loading()}>
              {this.state.isShowProfile==true && <DefaultAside />}
              {this.state.isShowNotification==true && <NotificationAside/>}
            </Suspense>
          </AppAside>
        </div>

    );
  }
}

export default Home;
