import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table,ButtonDropdown,
  Dropdown, DropdownItem, DropdownMenu, DropdownToggle,Button, FormGroup,Label,Input,Alert,
  Modal, ModalBody, ModalFooter, ModalHeader, } from 'reactstrap';

import { Link, NavLink } from 'react-router-dom';
import * as router from 'react-router-dom';
import { Badge, UncontrolledDropdown, Nav, NavItem } from 'reactstrap';
import {
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import axios from "axios";
import routes from './routes';
import LoadingBar from 'react-top-loading-bar'
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
class Branch extends Component {
  tempPreveliges=[]
  tempRoles=[]
  totalRoles=0
  count=0
  constructor(props) {
    super(props);
    this.state = {
      progress:0,
      filterUser:'',filterRole:'',filterPrev:'',
      isSortBy:'ascending',
      isShowRolePrev:false,
      isShowEditRolePrev:false,
      roleId:0,
      isShowAdd:true,
      allBranches:[],
      selectedPrevObject:[],
      selectedPrevId:0,
      isShowRoleModel:false,
      isShowRoleUpdateModel:false,
      isShowDropDownMenu:false,
      searchRoleName:'',
      roleName:'',
      roleType:'',
      allRoles:[],
      allPreveliges:[],
      modal: false,
      large: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
      formValid:false,
      roleNameValid:false,
      isShowFilters:false,
      errorMsg:{},
      isAll:false
      
    };
    // this.toggleLarge = this.toggleLarge.bind(this);
  
  }

  componentDidMount()
  {
    if(localStorage.getItem('isShowArabicLanguage')=='true')
    {
      global.isShowArabicLanguage=true
    }
    else
    {
      global.isShowArabicLanguage=false
    }
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='User_View')
        {
          this.setState({isShowRolePrev:true})
          isPrev=true
        }
        else {
          if(!isPrev==true)
          {
            isPrev=false
          }
         
        }
        if(prev[i]=='User_Managment')
        {
          this.setState({isShowEditRolePrev:true})
        }
      }
    }
    if(isPrev==false)
    {
      toaster.notify("User don't have privilege", {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
    else{
      this.getUserRoles()
    }
    var type=localStorage.getItem('role')
    if(type!=undefined)
    {
      if(type.startsWith('Taboor_'))
      {
        this.setState({
          roleType:'SuperAdmin',
          roleName:'Taboor ',
          roleNameValid:true
        })
      }
      else
      {
        this.setState({
          roleType:'Admin',
          roleName:'',
          roleNameValid:false
        })
      }
    }
  }
  validateForm = () => {
    const {roleNameValid} = this.state;
    this.setState({
      formValid: roleNameValid
    })
  }
  updateRoleName = (roleName) => {
    if(this.state.roleType=='Admin')
    {
      this.setState({roleName}, this.validateRoleName)
    }
    else
    {
      
      if(!roleName.startsWith('Taboor '))
      {
        if(roleName=='Taboor')
        {
          var roleName='Taboor '
          this.setState({roleName}, this.validateRoleName)
        }
        else
        {
          var roleName='Taboor ' + roleName
          this.setState({roleName}, this.validateRoleName)
        }
        // var roleName='Taboor ' + roleName
      }
      this.setState({roleName}, this.validateRoleName)
    }
  }
  validateRoleName = () => {
    const {roleName} = this.state;
    let roleNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (roleName.length == 0 ||roleName=='' ) {
      roleNameValid = false;
      errorMsg.roleName = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(roleName))
    // {
    //   roleNameValid = false;
    //   errorMsg.roleName =  'Only alphabets are allowed'
    // }
    else if(this.state.roleType=='Admin')
    {
      if(roleName.startsWith('Taboor') ||roleName.startsWith('taboor') )
      {
        roleNameValid = false;
        errorMsg.roleName =  'Name starts with taboor is not allowed'
      }
    }
    
    
    this.setState({roleNameValid, errorMsg}, this.validateForm)
  }
  getUserRoles()
  {
    this.tempRoles=[]
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/userRole/get/all', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.totalRoles=responseJson.roleList.length
        for(var i=0;i<responseJson.roleList.length;i++)
        {
          responseJson.roleList[i].role['isChecked']=false
          if(responseJson.privilegeCount==responseJson.roleList[i].role.userPrivileges.length)
          {
            responseJson.roleList[i].role['totalCount']='All'
          }
          else{
            responseJson.roleList[i].role['totalCount']=responseJson.roleList[i].role.userPrivileges.length
          }
          responseJson.roleList[i]['isShow']=false
          this.tempRoles.push(responseJson.roleList[i])
        }
        this.setState({
          allRoles:responseJson.roleList
        })
      }
    })
  }
  searchRole(roleName)
  {
    
    if(roleName!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempRoles.length;i++)
      {
        if(this.tempRoles[i].role.roleName.toLowerCase().includes(roleName.toLowerCase()))
        {
          data.push(this.tempRoles[i])
          this.setState({
            allRoles:data
          })
          isMatch=true
          this.totalRoles=data.length
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
        }
      }
      if(isMatch==false)
      {
        this.totalRoles=0
        this.setState({allRoles:[]})
      }
    }
    else
    {
      this.setState({
        allRoles:this.tempRoles
      })
    }
  
  }
  changeRoleSearch(roleName)
  {
    if(roleName=='')
    { 
      this.totalRoles=this.tempRoles.length
      this.setState({
        allRoles:this.tempRoles
      })
    }
    this.searchRole(roleName)
    this.setState({
    searchRoleName:roleName
    })
  }
  sortBy(data)
  {
    this.sortByPrev(data)
    this.setState({
      isSortBy:data
    })
  }
  sortByPrev(data)
  {
    if(this.tempRoles.length!=0) {
      let sortedProductsAsc;
      if(data=='ascending') {
        sortedProductsAsc= this.tempRoles.sort((a,b)=>{
          return Number(a.role.userPrivileges.length)  - Number(b.role.userPrivileges.length);
        })
      }
      else {
        sortedProductsAsc= this.tempRoles.sort((a,b)=>{
          return Number(a.role.userPrivileges.length)  - Number(b.role.userPrivileges.length);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      console.log(sortedProductsAsc)
      this.setState({ allRoles:sortedProductsAsc })
    } 
  }
  sortByUser() {
    if(this.tempRoles.length!=0) {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempRoles.sort((a,b)=>{
          return Number(a.noOfUsers)  - Number(b.noOfUsers);
        })
      }
      else {
        sortedProductsAsc= this.tempRoles.sort((a,b)=>{
          return Number(a.noOfUsers)  - Number(b.noOfUsers);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      } 
      console.log(sortedProductsAsc)
      this.setState({ allRoles:sortedProductsAsc })
    }
  }
  sortByRoleName() {
    if(this.tempRoles.length!=0) {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempRoles.sort((a,b)=>a.role.roleName.localeCompare(b.role.roleName))
      }
      else
      {
        sortedProductsAsc= this.tempRoles.sort((a,b)=>b.role.roleName.localeCompare(a.role.roleName))
      }
      console.log(sortedProductsAsc)
      this.setState({
        allRoles:sortedProductsAsc
      })
    }
    
  }
  showMenu(id)
  {
      for(var i=0;i<this.state.allRoles.length;i++)
      {
        if(this.state.allRoles[i].role.roleId==id)
        {
          this.state.allRoles[i]['isShowMenu']=true
          this.setState({
            isShowDropDownMenu:true
          })
        }
        else
        {
          this.state.allRoles[i]['isShowMenu']=false
        }
      }
      var data=this.state.allRoles
      this.setState({
        allRoles:data
      })
  }
  getUserPrivileges()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/userRole/getPrivileges', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        var data=[]
        // var temp=[]
        for(var i=0;i<responseJson.privileges.length;i++)
        {
          var key=responseJson.privileges[i].privilegeName
          var replaced = key.split('_').join(' ');
          responseJson.privileges[i].privilegeName=replaced
          data.push(responseJson.privileges[i])
          if(responseJson.privileges[i].privilegeId==7)
          {
            this.setState({selectedPrevId:responseJson.privileges[i].privilegeId})
            this.tempPreveliges.push(responseJson.privileges[i])
          }
          
        }
        this.setState({
          allPreveliges:data,
          selectedPrevObject:this.tempPreveliges
        })
      }
    })
  }
  changePrivileges=e=>
  {
    if(parseInt(e.target.value)!=0)
    {
      this.setState({selectedPrevId:parseInt(e.target.value)})
      for(var i=0;i<this.state.allPreveliges.length;i++)
      {
        if(this.state.allPreveliges[i].privilegeId==parseInt(e.target.value))
        {
          this.tempPreveliges.push(this.state.allPreveliges[i])  
        }
      }
      var tempArray=[...new Map(this.tempPreveliges.map(o => [o.privilegeId, o])).values()]
      this.tempPreveliges=tempArray
      this.setState({selectedPrevObject:this.tempPreveliges})
    }
   
  }
  onDismissPrev(id) {
    if(id!=7)
    {
      // this.tempPreveliges=[]
      for(var i=0;i<this.tempPreveliges.length;i++)
      {
        if(id==this.tempPreveliges[i].privilegeId)
        {
          this.tempPreveliges.splice(i,1)
        }
      }
      this.setState({ selectedPrevObject: this.tempPreveliges,allPreveliges:[] });
      let token = localStorage.getItem('sessionToken')
      fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/userRole/getPrivileges', {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.applicationStatusCode==0)
        {
          var data=[]
         
          for(var i=0;i<responseJson.privileges.length;i++)
          {
            var key=responseJson.privileges[i].privilegeName
            var replaced = key.split('_').join(' ');
            responseJson.privileges[i].privilegeName=replaced
            this.setState({selectedPrevId:7})
            data.push(responseJson.privileges[i])
          }
          this.setState({
            allPreveliges:data,
          })
        }
      })
    }
   
  }
  addRole()
  {
    var data=[]
    if(this.state.selectedPrevObject.length==0)
    {
      toaster.notify('Kindly select atleast one privilege', {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
    this.setState({
      formValid:false,progress:50
    })
    for(var i=0;i<this.state.selectedPrevObject.length;i++)
    {
      var key=this.state.selectedPrevObject[i].privilegeName
      var replaced = key.split(' ').join('_');
      this.state.selectedPrevObject[i].privilegeName=replaced
      data.push(this.state.selectedPrevObject[i])
    }
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/userRole/add', {
      method: 'POST',
      body:JSON.stringify({
        "privileges":data,
        "roleName": this.state.roleName
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          formValid:true,progress:100
        })
        this.getUserRoles()
        this.closeModel()
      }
      else{
        this.setState({
          formValid:true,progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  deleteRole(id)
  {
    this.setState({
      progress:50
    })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/userRole/delete', {
      method: 'POST',
      body:JSON.stringify({
        'id':parseInt(id)
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          progress:100
        })
        this.getUserRoles()
      }
      else{
        this.setState({
          progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  closeModel()
  {
    this.tempPreveliges=[]
    this.updateRoleName('')
    this.setState({
      // roleName:'',
      isShowRoleModel:false,
      allPreveliges:[],
      selectedPrevObject:[],
      formValid:false
    })
  }
  resetModel()
  {
    this.tempPreveliges=[]
    this.updateRoleName('')
    this.setState({
      // roleName:'',
      allPreveliges:[],
      selectedPrevObject:[],
      formValid:false
    })
    this.getUserPrivileges()
  }
  toggleModel() {
    this.getUserPrivileges()
    this.setState({
      isShowRoleModel: true,
      isShowAdd:true
    });
  }
  updateRole(role)
  {
    this.tempPreveliges=[]
    this.setState({
      roleName:role.role.roleName,
      formValid:true
    })
  //  this.updateRoleName(role.role.roleName)
   let token = localStorage.getItem('sessionToken')
   fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/userRole/getPrivileges', {
     method: 'GET',
     headers: {
       'Authorization': `Bearer ${token}`
     },
   })
   .then(response => response.json())
   .then(responseJson => {
     if(responseJson.applicationStatusCode==0)
     {
       var data=[]
       for(var i=0;i<responseJson.privileges.length;i++)
       {
        var key=responseJson.privileges[i].privilegeName
        var replaced = key.split('_').join(' ');
        responseJson.privileges[i].privilegeName=replaced
         data.push(responseJson.privileges[i])        
       }
       for(var j=0;j<role.role.userPrivileges.length;j++)
       {
        var key=role.role.userPrivileges[j].privilegeName
        var replaced = key.split('_').join(' ');
        role.role.userPrivileges[j].privilegeName=replaced
        this.tempPreveliges.push(role.role.userPrivileges[j])
       }
       var tempArray=[...new Map(this.tempPreveliges.map(o => [o.privilegeId, o])).values()]
       this.tempPreveliges=tempArray
       this.setState({
         allPreveliges:data,
         selectedPrevId:this.tempPreveliges[0].privilegeId,
         selectedPrevObject:this.tempPreveliges
       })
     }
   })
   this.setState({
    isShowRoleUpdateModel:true,
    roleId:role.role.roleId,
   })
  }
  closeUpdateModel()
  {
    this.tempPreveliges=[]
    this.setState({
      roleName:'',
      isShowRoleUpdateModel:false,
      allPreveliges:[],
      selectedPrevObject:[],
      formValid:false
    })
  }
  resetUpdateModel()
  {
    this.tempPreveliges=[]
    this.setState({
      roleName:'',
      allPreveliges:[],
      selectedPrevObject:[],
      formValid:false
    })
    this.getUserPrivileges()
  }
  updateUserRole()
  {
    var data=[]
    if(this.state.selectedPrevObject.length==0)
    {
      toaster.notify('Kindly select atleast one privilege', {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
    this.setState({
      formValid:false,progress:50
    })
    for(var i=0;i<this.state.selectedPrevObject.length;i++)
    {
      var key=this.state.selectedPrevObject[i].privilegeName
      var replaced = key.split(' ').join('_');
      this.state.selectedPrevObject[i].privilegeName=replaced
      data.push(this.state.selectedPrevObject[i])
    }
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/userRole/update', {
      method: 'POST',
      body:JSON.stringify({
        "privileges":data,
        "roleName": this.state.roleName,
        'roleId':this.state.roleId
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          formValid:true,progress:100
        })
        this.getUserRoles()
        this.closeUpdateModel()
      }
      else{
        this.setState({
          formValid:true,progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  onDismissUpdatePrev(id)
  {
    if(id!=7)
    {
      // this.tempPreveliges=[]
      for(var i=0;i<this.tempPreveliges.length;i++)
      {
        if(id==this.tempPreveliges[i].privilegeId)
        {
          this.tempPreveliges.splice(i,1)
        }
      }
      this.setState({ selectedPrevObject: this.tempPreveliges,allPreveliges:[] });
      let token = localStorage.getItem('sessionToken')
      fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/userRole/getPrivileges', {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.applicationStatusCode==0)
        {
          var data=[]
          // var temp=[]
          for(var i=0;i<responseJson.privileges.length;i++)
          {
            var key=responseJson.privileges[i].privilegeName
            var replaced = key.split('_').join(' ');
            responseJson.privileges[i].privilegeName=replaced
            this.setState({selectedPrevId:0})
            data.push(responseJson.privileges[i])
          }
          this.setState({
            allPreveliges:data,
          })
        }
      })
    }
  }
  selectAll()
  {
    var temp=this.state.isAll
    if(this.tempRoles.length!=0)
    {
      for(var i=0;i<this.tempRoles.length;i++)
      {
        if(temp==false)
        {
          this.tempRoles[i].role['isChecked']=true
          this.count=this.tempRoles.length
        }
        else
        {
          this.count=0
          this.tempRoles[i].role['isChecked']=false
        }
        
      }
    }
    this.setState({
      isAll:!temp,
      allRoles:this.tempRoles,
    })
  }
  select(data)
  {
    for(var i=0;i<this.tempRoles.length;i++)
    {
      if(data.roleId==this.tempRoles[i].role.roleId)
      {
        if(this.tempRoles[i].role['isChecked']==true)
        {
          this.tempRoles[i].role['isChecked']=false
          this.setState({
            isAll:false
          })
          this.count=this.count-1
          break;
        }
        else
        {         
          this.count=this.count+1
          this.tempRoles[i].role['isChecked']=true
        }
  
      }
    }
    if(this.count==this.tempRoles.length)
    {
      this.setState({
        isAll:true
      })
    }
    this.setState({allRoles:this.tempRoles})
  }

  changeUserFilter(data)
  {
    this.setState({
      filterUser:data
    })
  }
  changeRoleFilter(data)
  {
    this.setState({
      filterRole:data
    })
  }
  changePrevFilter(data)
  {
    this.setState({
      filterPrev:data
    })
  }
  applyFilter()
  {
    var filterData=JSON.parse(JSON.stringify(this.tempRoles))
    if(this.state.filterUser!='')
    {
      var val=this.state.filterUser
      filterData =  filterData.filter(function(status) {
        return status.noOfUsers == parseInt(val);
      });
    }
    if(this.state.filterRole!='')
    {
      var val=this.state.filterRole
      filterData =  filterData.filter(function(status) {
        return status.role.roleName.toLowerCase() ==val.toLowerCase();
      });
    }
    if(this.state.filterPrev!='')
    {
      var val=this.state.filterPrev
      filterData =  filterData.filter(function(status) {
        return status.role.userPrivileges.length == parseInt(val);
      });
    }
    this.totalRoles=filterData.length
    this.setState({
      allRoles:filterData
    })
    console.log(filterData)
   
  }
  resetFilter()
  {
    this.totalRoles=this.tempRoles.length
    this.setState({
      filterUser:'',filterRole:'',filterPrev:'',allRoles:this.tempRoles
    })
  }
  render() {
    return (
      <div className="animated fadeIn">
        <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress:0
          })}
        />
        <DefaultHeader/>
        <Row className="mb-3 hidden">
         
          <Col sm="4">
            <h3>
            <ButtonDropdown id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                &nbsp;
                </DropdownToggle>
                <DropdownMenu left>
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              <div className="checkbox form-check d-inline-block ml-2">
                  <input type="checkbox" className="form-check-input" onChange={()=>this.selectAll()} checked={this.state.isAll}></input>
                  {global.isShowArabicLanguage==false &&  <label  className="form-check-label form-check-label">{this.totalRoles}&nbsp;Roles</label>}
                  {global.isShowArabicLanguage==true &&  <label  className="form-check-label form-check-label">{this.totalRoles}&nbsp;دور</label>}

              </div>
              </h3>
          </Col>

          {this.state.isShowRolePrev==true && <Col sm="8" className="text-right">
          
            <div className="float-right">
            <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button>
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                onClick={() => {this.setState({ isShowSortByMenu:false })}}
                >
                <button className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></button>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={()=>this.sortBy('ascending')}><input type="radio" checked={this.state.isSortBy=='ascending'} name="sort-order" onChange={()=>{}}></input> Ascending</DropdownItem>
                  <DropdownItem onClick={()=>this.sortBy('descending')}><input type="radio" checked={this.state.isSortBy=='descending'} name="sort-order" onChange={()=>{}}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            {this.state.isShowEditRolePrev==true && <button className="btn btn-outline-warning ml-2" onClick={()=>this.toggleModel()}><i className="icon2-addition-sign"></i></button>}
              </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                 value={this.state.searchRoleName} onChange={(e) => this.changeRoleSearch(e.target.value)}
                 onKeyPress={event => {
                   if (event.key === 'Enter') {
                     this.searchRole()
                   }
                 }}
                >

                  
                </input>
              <div className="input-group-append"><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>}
        </Row>

        {this.state.isShowFilters==true && <Card className="card-line" id="filter-row">
          <CardBody>
            <Row>
              
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Role" 
                       value={this.state.filterRole} 
                       onChange={(e) => this.changeRoleFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    
                   
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="Privileges"  min="0"
                       value={this.state.filterPrev} 
                       onChange={(e) => this.changePrevFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="#Users"  min="0"
                      value={this.state.filterUser} 
                      onChange={(e) => this.changeUserFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                   
                   
                    <Col sm="auto">
                    <Button color="light" className="btn-sm" onClick={()=>this.resetFilter()}>Reset</Button>
                      <Button color="primary" className="btn-sm ml-2"  onClick={()=>this.applyFilter()}> Apply</Button>
                    </Col>
                    
                    </Row>
                  </CardBody>
                  </Card>}

        {this.state.isShowRolePrev==true &&    <Card className="mb-0">
              
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                    <th width="71"></th>
                    {global.isShowArabicLanguage==false &&  <th className="text-left" onClick={()=>this.sortByRoleName()}>
                    Role
                    </th>}
                    {global.isShowArabicLanguage==true &&  <th className="text-left" onClick={()=>this.sortByRoleName()}>
                    دور
                    </th>}
                  
                    {global.isShowArabicLanguage==false &&   <th onClick={()=>this.sortByPrev()}>Privileges </th>}
                    {global.isShowArabicLanguage==true &&   <th onClick={()=>this.sortByPrev()}>الصلاحيات </th>}

                    <th onClick={()=>this. sortByUser()}>#Users</th>
                    <th width="50"></th>
                  </tr>
                  </thead>
                  <tbody>
                 
                  {this.state.allRoles.map(role => (
                    <tr>
                      <td className="text-right">
                        <div className="checkbox form-check">
                            <input type="checkbox" className="form-check-input" onChange={()=>this.select(role.role)} checked={role.role.isChecked}></input>
                            <label  className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td>{role.role.roleName}</td>
                      <td className="text-center">{role.role.totalCount}</td>
                      <td className="text-center">{role.noOfUsers}</td>
                      
                      {this.state.isShowEditRolePrev==true && <td className="text-center"><Link onClick={()=>this.showMenu(role.role.roleId)}><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>}
                      {role.isShowMenu==true && 
                     <Dropdown isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu:false }); }} >
                            <DropdownToggle
                              tag="span"
                              onClick={() => {this.setState({ isShowDropDownMenu:false })}}
                              data-toggle="dropdown"
                              >
                            </DropdownToggle>
                            <DropdownMenu>
                              <DropdownItem onClick={()=>this.updateRole(role)}>Update</DropdownItem>
                              <DropdownItem onClick={()=>this.deleteRole(role.role.roleId)}>Delete</DropdownItem>
                            </DropdownMenu>
                      </Dropdown>}
                    </tr>
                  ))}
                    
                    

                  
                  
                   </tbody>
                </Table>
               
              </CardBody>
            </Card>}
          
          


            <Modal isOpen={this.state.isShowRoleModel} 
                       className={'modal-lg ' + this.props.className}>
                 <ModalHeader>Add Role</ModalHeader>
                  <ModalBody>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label> 
                    {this.state.roleType=='Admin' && <Input type="text" placeholder="Name" autoComplete="username"
                    value={this.state.roleName} onChange={(e) => this.updateRoleName(e.target.value)}/>}
                    {this.state.roleType=='SuperAdmin' && <Input type="text" placeholder="Name" autoComplete="username"
                    value={this.state.roleName} onChange={(e) => this.updateRoleName(e.target.value)}/>}
                    < ValidationMessage valid={this.state.roleNameValid} message={this.state.errorMsg.roleName} />
                  </FormGroup>
                  


                  <FormGroup className="input-line">
                  {global.isShowArabicLanguage==false &&   <Label className="text-light-grey">Privileges</Label>}
                    {global.isShowArabicLanguage==true &&  <Label className="text-light-grey"> الصلاحيات</Label>}
                   
                    <select className="form-control" onChange={this.changePrivileges} value={this.state.selectedPrevId} >
                    {global.isShowArabicLanguage==false &&    <option value='0' key='0'>
                        Privileges
                      </option>}
                      {global.isShowArabicLanguage==true &&    <option value='0' key='0'>
                      الصلاحيات
                      </option>}
                    {this.state.allPreveliges.map(prev => (
                      <option value={prev.privilegeId} key={prev.privilegeId}>
                        {prev.privilegeName}
                      </option>
                    ))}
                    </select>
                  </FormGroup>
                  {this.state.selectedPrevObject.map(prev => (
                    <Alert color="light" className="theme-alert" isOpen={this.state.visible} toggle={()=>this.onDismissPrev(prev.privilegeId)}>
                      {prev.privilegeName}
                    </Alert>  
                  ))}
                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeModel()}>Cancel</Button>
                    <Button outline color="primary" onClick={()=>this.resetModel()}>Reset</Button>
                    <Button color="primary" disabled={!this.state.formValid} onClick={()=>this.addRole()}>Add</Button>
                  </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.isShowRoleUpdateModel} 
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader>Edit Role</ModalHeader>
                  <ModalBody>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label> 
                    {this.state.roleType=='Admin' && <Input type="text" placeholder="Name" autoComplete="username"
                    value={this.state.roleName} readOnly/>}
                    {this.state.roleType=='SuperAdmin' && <Input type="text" placeholder="Name" autoComplete="username"
                    value={this.state.roleName} readOnly/>}
                  </FormGroup>
                  


                  <FormGroup className="input-line">
                  {global.isShowArabicLanguage==false &&   <Label className="text-light-grey">Privileges</Label>}
                    {global.isShowArabicLanguage==true &&  <Label className="text-light-grey"> الصلاحيات</Label>}
                    <select className="form-control" onChange={this.changePrivileges} value={this.state.selectedPrevId} >
                    {global.isShowArabicLanguage==false &&    <option value='0' key='0'>
                        Privileges
                      </option>}
                      {global.isShowArabicLanguage==true &&    <option value='0' key='0'>
                      الصلاحيات
                      </option>}
                    {this.state.allPreveliges.map(prev => (
                      <option value={prev.privilegeId} key={prev.privilegeId}>
                        {prev.privilegeName}
                      </option>
                    ))}
                    </select>
                  </FormGroup>
                  {this.state.selectedPrevObject.map(prev => (
                    <Alert color="light" className="theme-alert" isOpen={this.state.visible} toggle={()=>this.onDismissUpdatePrev(prev.privilegeId)}>
                      {prev.privilegeName}
                    </Alert>  
                  ))}
                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeUpdateModel()}>Cancel</Button>
                    <Button outline color="primary" onClick={()=>this.resetUpdateModel()}>Reset</Button>
                     <Button color="primary" disabled={!this.state.formValid} onClick={()=>this.updateUserRole()}>Update</Button>
                  </ModalFooter>
                </Modal>


      
      


      </div>
    );
  }
}

export default Branch;
