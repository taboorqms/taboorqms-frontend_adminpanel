import {signOut} from './home'
import axios from "axios";

export default {
   items: [
    {
      name: 'My Account',
      url: '/myAccount',
      icon: 'icon2-username',
      
    },
    {
      name: 'Users',
      url: '/users',
      icon: 'icon2-username',
      badge: {
        variant: 'info',
        
      },
    },
    {
      name: 'Roles',
      url: '/roles',
      icon: 'icon2-target1',
      badge: {
        variant: 'secondary',
        
      },
    },
    {
      name: 'Notifications',
      url: '/notifications',
      icon: 'icon2-alarm-bell',
    },
    {
      name: 'Configurations',
      url: '/configurations',
      icon: 'icon2-settings-gear',
    },
        
        
      ],
}