import React, { Component } from 'react';
import { Card, CardBody, Col, Row, Table, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle, Button, FormGroup,Label,Input, 
  Modal, ModalBody, ModalFooter, ModalHeader,Dropdown } from 'reactstrap';
import axios from "axios"; 
import { Link } from 'react-router-dom'; 
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import { DatePickerComponent  } from '@syncfusion/ej2-react-calendars';
import LoadingBar from 'react-top-loading-bar'

function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}

const DefaultHeader = React.lazy(() => import('./defaultHeader'));
class Breadcrumbs extends Component {

 tempAvialableAgents=[]
 tempAllPrivileges=[]
 selectedPrivileges=[]
 totalAgents=0
 counter=0
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      progress:0,
      isDisableAgent:false,
      isSortBy:'ascending',
      isShowDropDownMenu:false,
      isShowAgentPrev:false,
      isShowAgentEditPrev:false,
      dropdownOpen: false,
      agentEmail:'',
      agentName:'',
      isAgentModal:false,
      agentNameValid:false,
      agentEmailValid:false,
      errorMsg:{},
      radioSelected: 2,
      selectedBranchId:0,
      allAgents:[],
      allPrivileges:[],
      searchQueue:'',
      allBranches:[],
      modal: false,
      large: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
      formValid:false,
      isShowFilters:false,
      isAll:false,
      sDate:null,
      filterId:'',filterName:'',filterAssignCounter:'',filterBranch:'',filterPrev:'',filterAst:''
    };
    this.toggleLarge = this.toggleLarge.bind(this);
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }
  toggleLarge() {
    this.setState({
      isAgentModal: true,
    });
    this.getAllBranches()
    this.getAgentPrevileges()
    this.getNextCredentials()
  }
  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }
  componentDidMount()
  {
    if(localStorage.getItem('isShowArabicLanguage')=='true')
    {
      global.isShowArabicLanguage=true
    }
    else
    {
      global.isShowArabicLanguage=false
    }
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Agent_View')
        {
          this.setState({isShowAgentPrev:true})
          isPrev=true
        }
        else {
          if(!isPrev==true)
          {
            isPrev=false
          }
         
        }
        if(prev[i]=='Agent_Edit')
        {
          this.setState({isShowAgentEditPrev:true})
        }
      }
    }
    if(isPrev==false)
    {
      toaster.notify("User don't have privilege", {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
    else{
      this.getAgents()
    }
  }
  sortBy(data)
  {
    this.setState({
      isSortBy:data
    })
  }
  sortById()
  {
    if(this.tempAvialableAgents.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableAgents[0].sort((a,b)=>a.agent.serviceCenterEmployee.employeeNumber.localeCompare(b.agent.serviceCenterEmployee.employeeNumber))

      }
      else
      {
        sortedProductsAsc= this.tempAvialableAgents[0].sort((a,b)=>b.agent.serviceCenterEmployee.employeeNumber.localeCompare(a.agent.serviceCenterEmployee.employeeNumber))
        // sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    console.log(sortedProductsAsc)
    this.setState({
        allAgents:sortedProductsAsc
    })
    }
    
  }
  sortByName()
  {
    if(this.tempAvialableAgents.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableAgents[0].sort((a,b)=>a.agent.serviceCenterEmployee.user.name.localeCompare(b.agent.serviceCenterEmployee.user.name))
      }
      else
      {
        sortedProductsAsc= this.tempAvialableAgents[0].sort((a,b)=>b.agent.serviceCenterEmployee.user.name.localeCompare(a.agent.serviceCenterEmployee.user.name))
      }
      console.log(sortedProductsAsc)
      this.setState({
          allAgents:sortedProductsAsc
      })
    }
    
  }
  sortByBranchName()
  {
    if(this.tempAvialableAgents.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableAgents[0].sort((a,b)=>a.agent.branch.branchName.localeCompare(b.agent.branch.branchName))
      }
      else
      {
        sortedProductsAsc= this.tempAvialableAgents[0].sort((a,b)=>b.agent.branch.branchName.localeCompare(a.agent.branch.branchName))
      }
      console.log(sortedProductsAsc)
      this.setState({
          allAgents:sortedProductsAsc
      })
    }
    
  }

  sortByDates()
  {
    if(this.tempAvialableAgents.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableAgents[0].sort((a,b)=>{
        return new Date(a.agent.serviceCenterEmployee.user.lastLoginDate)  - new Date(b.agent.serviceCenterEmployee.user.lastLoginDate);
      })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableAgents[0].sort((a,b)=>{
        return new Date(a.agent.serviceCenterEmployee.user.lastLoginDate)  - new Date(b.agent.serviceCenterEmployee.user.lastLoginDate);
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
      }
     
    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
  }
  sortByAST()
  {
    if(this.tempAvialableAgents.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableAgents[0].sort((a,b)=>{
        return Number(a.agent.AST)  - Number(b.agent.AST);
      })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableAgents[0].sort((a,b)=>{
        return Number(a.agent.AST)  - Number(b.agent.AST);
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    console.log(sortedProductsAsc)
    this.setState({
        allAgents:sortedProductsAsc
    })
    }
  }
  getAgents()
  {
    this.tempAvialableAgents=[]
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getListing';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode==0)
      {
        
        for(var i=0;i<responseJson.data.agentList.length;i++)
        {
          responseJson.data.agentList[i].agent['isChecked']=false
          var hms = responseJson.data.agentList[i].agent.averageServiceTime;  
          var a = hms.split(':'); 
          var minutes = (+a[0]) * 60 + (+a[1]); 
          console.log(minutes);
          responseJson.data.agentList[i].agent['AST']=minutes
          if(responseJson.data.agentList[i].agent.serviceCenterEmployee.user.lastLoginTime!=null)
          {
            var lastLoginDate= new Date(responseJson.data.agentList[i].agent.serviceCenterEmployee.user.lastLoginTime) 
            responseJson.data.agentList[i].agent.serviceCenterEmployee.user['lastLoginDate']=lastLoginDate
            var sHours = lastLoginDate.getHours();
            var sMinutes = lastLoginDate.getMinutes();
            var sDate = lastLoginDate.getDate();
            var sMonth = lastLoginDate.getMonth()+1;
            var sYear = lastLoginDate.getFullYear();
            var date= sDate+'.'+sMonth+'.'+sYear
            var ampm = sHours >= 12 ? 'PM' : 'AM';
            sHours = sHours % 12;
            sHours = sHours ? sHours : 12;
            sHours = sHours < 10 ?'0' + sHours : sHours;
            sMinutes = sMinutes < 10 ? '0' + sMinutes : sMinutes;
            var strTime = sHours + ':' + sMinutes + ' ' + ampm;
            responseJson.data.agentList[i].agent.serviceCenterEmployee.user['time']=strTime
            responseJson.data.agentList[i].agent.serviceCenterEmployee.user['date']=date
          }
          responseJson.data.agentList[i]['isShowMenu']=false
        }
        this.tempAvialableAgents.push(responseJson.data.agentList)
        this.totalAgents=this.tempAvialableAgents[0].length
        this.setState({
          allAgents:responseJson.data.agentList,
         
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  showMenu(id)
  {
      for(var i=0;i<this.state.allAgents.length;i++)
      {
        if(this.state.allAgents[i].agent.agentId==id)
        {
          this.state.allAgents[i]['isShowMenu']=true
          this.setState({
            isShowDropDownMenu:true
          })
        }
        else
        {
          this.state.allAgents[i]['isShowMenu']=false
        }
      }
      var data=this.state.allAgents
      this.setState({
        allAgents:data
      })
  }
  getAllBranches()
  {
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/branch/getList';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode==0)
      {
        this.setState({
          allBranches:responseJson.data.branchList,
          // selectedBranchId:responseJson.data.branchList[0].branchId
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  search(queueNumber)
  {
    
    if(queueNumber!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempAvialableAgents[0].length;i++)
      {
        if(this.tempAvialableAgents[0][i].agent.serviceCenterEmployee.employeeNumber.toLowerCase().includes(queueNumber.toLowerCase()) || 
          this.tempAvialableAgents[0][i].agent.serviceCenterEmployee.user.name.toLowerCase().includes(queueNumber.toLowerCase())
          )
        {
          data.push(this.tempAvialableAgents[0][i])
          this.setState({
            allAgents:data
          })
          isMatch=true
          this.totalAgents=data.length
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
          
        }
      }
      if(isMatch==false)
      {
        this.totalAgents=0
        this.setState({allAgents:[]})
      }
    }
    else
    {
      this.totalAgents=this.tempAvialableAgents[0].length
      this.setState({
        allAgents:this.tempAvialableAgents[0]
      })
    }
  
  }
  changeSearch(queueNumber)
  {
    if(queueNumber=='')
    { 
      this.totalAgents=this.tempAvialableAgents[0].length
      this.setState({
        allAgents:this.tempAvialableAgents[0]
      })
    }
    this.search(queueNumber)
    this.setState({
      searchQueue:queueNumber
      })
  }
  getAgentPrevileges()
  {
    this.tempAllPrivileges=[]
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getPrivileges', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        for(var i=0;i<responseJson.privilegeList.length;i++)
        {
          responseJson.privilegeList[i]['isChecked']=true
          var key=responseJson.privilegeList[i].privilegeName
            var replaced = key.split('_').join(' ');
            responseJson.privilegeList[i].privilegeName=replaced
        }
        this.tempAllPrivileges.push(responseJson.privilegeList)
        this.setState({allPrivileges:responseJson.privilegeList})
        
      }
    })
  }
  getNextCredentials()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getNextCredentials', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({NextAgentId:responseJson.values.NextAgentId,
          AgentPIN:responseJson.values.AgentPIN
        })
      }
    })
}
  changePriveleges(data)
  {
    for(var i=0;i<this.tempAllPrivileges[0].length;i++)
    {
      if(data.privilegeId==this.tempAllPrivileges[0][i].privilegeId)
      {
        if(this.tempAllPrivileges[0][i]['isChecked']==true)
        {
          this.tempAllPrivileges[0][i]['isChecked']=false
          
        }
        else
        {
          this.tempAllPrivileges[0][i]['isChecked']=true
          
          // this.selectedPrivileges.push(this.tempAllPrivileges[i])
        }

      }
    }
    this.setState({allPrivileges:this.tempAllPrivileges[0]})
  }
  resetAgent()
  {
    for(var i=0;i<this.selectedPrivileges.length;i++)
    {
      this.selectedPrivileges[i]['isChecked']=false
    }
  this.selectedPrivileges=[]
  this.tempAllPrivileges=[]
  this.setState({agentEmail:'',agentName:'',formValid:false,allPrivileges:[],allBranches:[]})
  this.getAgentPrevileges()
  this.getAllBranches()
  }
  closeAgentModel()
  {
    this.selectedPrivileges=[]
    this.tempAllPrivileges=[]
    this.setState({agentEmail:'',agentName:'',formValid:false,allPrivileges:[],isAgentModal:false,selectedBranchId:0})
    this.getAgents()
  }
  changeAgentBranch=e=>
  {
    this.setState({selectedBranchId:parseInt(e.target.value)})
  }
  addUpdateAgent()
  {
    this.selectedPrivileges=[]
    if(this.state.selectedBranchId==0)
    {
     toaster.notify('Kindly select atleast one branch',{
      duration:5000
     })
       return
    }
    for(var i=0;i<this.tempAllPrivileges[0].length;i++)
    {
      if(this.tempAllPrivileges[0][i]['isChecked']==true)
      {
        var key=this.tempAllPrivileges[0][i].privilegeName
        var replaced = key.split(' ').join('_');
        this.tempAllPrivileges[0][i].privilegeName=replaced
        this.selectedPrivileges.push(this.tempAllPrivileges[0][i])
      }
    }
    if(this.selectedPrivileges.length==0)
    {
     toaster.notify('Kindly select atleast one privilage',{
      duration:5000
     })
       return
    }
    for(var i=0;i<this.selectedPrivileges.length;i++)
    {
        delete this.selectedPrivileges[i]['isChecked']
    }
    this.setState({
      formValid:false,progress:50
    })
   let token = localStorage.getItem('sessionToken')
   
      fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/add', {
        method: 'POST',
        body:JSON.stringify({
          "agentId": this.state.NextAgentId,
          "agentName": this.state.agentName,
          "agentPin": this.state.AgentPIN,
          "assignedCounterId":-1,
          "branchId": this.branchId,
          "email": this.state.agentEmail,
          "privileges":this.selectedPrivileges,
          'branchId':this.state.selectedBranchId
         }),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
      })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.applicationStatusCode==0)
        {
          
          this.selectedPrivileges=[]
          global.isBranchAgentCounterUpdated.next(true)
          global.isNotificationUpdated.next(true)
          this.setState({agentEmail:'',agentName:''})
          this.setState({
            formValid:true,progress:100
          })
          this.closeAgentModel()
          this.getAgents()
        }
        else{
          this.setState({
            formValid:true,progress:100
          })
          toaster.notify(responseJson.devMessage, {
            // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
            duration:5000 // This notification will not automatically close
          });
        }
      })
  }
  updateAgentName = (agentName) => {
    this.setState({agentName}, this.validateAgentName)
  }
  validateAgentName = () => {
    const {agentName} = this.state;
    let agentNameValid = true;
    let errorMsg = {...this.state.errorMsg}
  
    if (agentName.length == 0 ||agentName=='' ) {
      agentNameValid = false;
      errorMsg.agentName = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(agentName))
    // {
    //   agentNameValid = false;
    //   errorMsg.agentName =  'Only alphabets are allowed'
    // }
    this.setState({agentNameValid, errorMsg}, this.validateForm)
  }
  updateAgentEmail= (agentEmail) => {
    this.setState({agentEmail}, this.validateAgentEmail)
  }
  validateAgentEmail = () => {
    const {agentEmail} = this.state;
    let agentEmailValid = true;
    let errorMsg = {...this.state.errorMsg}
  
    // checks for format _@_._
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(agentEmail)){
      agentEmailValid = false;
      errorMsg.agentEmail = 'Invalid email format'
    }
  
    this.setState({agentEmailValid, errorMsg}, this.validateForm)
  }
  validateForm = () => {
    const {agentEmailValid, agentNameValid} = this.state;
    this.setState({
      formValid: agentEmailValid && agentNameValid 
    })
  }
  agentDetails(id)
  {
    this.props.history.push(
      {
        pathname:'/agent-details',
        agentId:id
      }
    
    )
  }
  selectAll()
  {
    var temp=this.state.isAll
    if(this.tempAvialableAgents[0].length!=0)
    {
      for(var i=0;i<this.tempAvialableAgents[0].length;i++)
      {
        if(temp==false)
        {
          this.tempAvialableAgents[0][i].agent['isChecked']=true
          this.count=this.tempAvialableAgents[0].length
        }
        else
        {
          this.count=0
          this.tempAvialableAgents[0][i].agent['isChecked']=false
        }
        
      }
    }
    this.setState({
      isAll:!temp,
      allAgents:this.tempAvialableAgents[0],
    })
  }
  select(data)
  {
    for(var i=0;i<this.tempAvialableAgents[0].length;i++)
    {
      if(data.agentId==this.tempAvialableAgents[0][i].agent.agentId)
      {
        if(this.tempAvialableAgents[0][i].agent['isChecked']==true)
        {
          this.tempAvialableAgents[0][i].agent['isChecked']=false
          this.setState({
            isAll:false
          })
          this.count=this.count-1
          break;
        }
        else
        {         
          this.count=this.count+1
          this.tempAvialableAgents[0][i].agent['isChecked']=true
        }
  
      }
    }
    if(this.count==this.tempAvialableAgents[0].length)
    {
      this.setState({
        isAll:true
      })
    }
    this.setState({allBranches:this.tempAvialableAgents[0]})
  }
  changefilterId(data)
  {
    this.setState({
      filterId:data
    })
  }
  changeBranchFilter(data)
  {
    this.setState({
      filterBranch:data
    })
  }
  changeNameFilter(data)
  {
    this.setState({
      filterName:data
    })
  }
  changeAssignCounterFilter(data)
  {
    this.setState({
      filterAssignCounter:data
    })
  }
  changeASTFilter(data)
  {
    this.setState({
      filterAst:data
    })
  }
  changeStartDate(date)
  {
    this.setState({sDate:date.value})
    }
  applyFilter()
  {
    var filterData=JSON.parse(JSON.stringify(this.tempAvialableAgents[0]))
    if(this.state.filterId!='')
    {
      var val=this.state.filterId
      filterData =  filterData.filter(function(status) {
        return status.agent.serviceCenterEmployee.employeeNumber.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterBranch!='')
    {
      var val=this.state.filterBranch
      filterData =  filterData.filter(function(status) {
        return status.agent.branch.branchName.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterName!='')
    {
      var val=this.state.filterName
      filterData =  filterData.filter(function(status) {
        return status.agent.serviceCenterEmployee.user.name.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterAssignCounter!='')
    {
      var val=this.state.filterAssignCounter
      filterData =  filterData.filter(function(status) {
        return status.assignedBranchCounter.counterNumber.toLowerCase() ==val.toLowerCase();
      });
    }
    if(this.state.filterAst!='')
    {
      var val=this.state.filterAst
      filterData =  filterData.filter(function(status) {
        return status.agent.AST == parseInt(val);
      });
    }
    if(this.state.sDate!=null)
    {
      let d = this.state.sDate.getDate();
      let m = this.state.sDate.getMonth()+1;
      let y=this.state.sDate.getFullYear()
      d = d < 10 ? '0' + d : d;
      m = m < 10 && m > 0 ? '0' + m : m;
      var val=d+"."+m+"."+y 
      filterData =  filterData.filter(function(status) {
        return status.agent.serviceCenterEmployee.user.date == val;
      });
    }
    this.totalAgents=filterData.length
    this.setState({
      allAgents:filterData
    })
    console.log(filterData)
   
  }
  resetFilter()
  {
    this.totalAgents=this.tempAvialableAgents[0].length
    this.setState({
      filterId:'',filterName:'',filterAssignCounter:'',filterBranch:'',filterPrev:'',filterAst:'',allAgents:this.tempAvialableAgents[0]
    })
  }
  render() {
    return (
      <div className="animated fadeIn">
        <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress:0
          })}
        />
      <DefaultHeader/>
       <Row className="mb-3 hidden">
          <Col sm="4">
            <h3>
            {/* <ButtonDropdown id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                &nbsp;
                </DropdownToggle>
                <DropdownMenu left>
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown> */}
              <div className="checkbox form-check d-inline-block ml-2">
                  <input  type="checkbox" className="form-check-input" onChange={()=>this.selectAll()} checked={this.state.isAll}></input>
                  <label  className="form-check-label form-check-label">{this.totalAgents}&nbsp;Agents</label>
              </div>
              </h3>
          </Col>

          {this.state.isShowAgentPrev==true &&  <Col sm="8" className="text-right">
          
            <div className="float-right">
            <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button>
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                onClick={() => {this.setState({ isShowSortByMenu:false })}}
                >
                Sort By <i className="icon2-sort-descending"></i>
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem ><input type="radio" checked={this.state.isSortBy=='ascending'} name="sort-order" onChange={()=>{this.sortBy('ascending')}}/>Ascending </DropdownItem>
                  <DropdownItem ><input type="radio" checked={this.state.isSortBy=='descending'} name="sort-order" onChange={()=>{this.sortBy('descending')}}/>Descending </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            {this.state.isShowAgentEditPrev==true &&<button className="btn btn-outline-warning ml-2" onClick={this.toggleLarge}><i className="icon2-addition-sign"></i></button>}
            </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                value={this.state.searchQueue} onChange={(e) => this.changeSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.search()
                  }
                }}
                ></input>
              <div className="input-group-append"><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>}
        </Row>

        {this.state.isShowFilters==true && <Card className="card-line" id="filter-row">
          <CardBody>
            <Row>
              
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="ID" 
                      value={this.state.filterId}  onChange={(e) => this.changefilterId(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Name" 
                      onChange={(e) => this.changeNameFilter(e.target.value)}
                       value={this.state.filterName} 
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Assig. Counters" 
                      onChange={(e) => this.changeAssignCounterFilter(e.target.value)}
                       value={this.state.filterAssignCounter} 
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Branch" 
                      onChange={(e) => this.changeBranchFilter(e.target.value)}
                       value={this.state.filterBranch} 
                      /></FormGroup>
                    </Col>

                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="Avg Handle Time"  min="0"
                      onChange={(e) => this.changeASTFilter(e.target.value)}
                      value={this.state.filterAst} 
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                        <DatePickerComponent  value={this.state.sDate}  placeholder='Last Login'
                        onChange={(date) => this.changeStartDate(date)}/>
                      </FormGroup>
                    </Col>
                   
                    <Col sm="auto">
                    <Button color="light" className="btn-sm" onClick={()=>this.resetFilter()}>Reset</Button>
                      <Button color="primary" className="btn-sm ml-2" onClick={()=>this.applyFilter()}>Apply</Button>
                    </Col>
                    
                    </Row>
                  </CardBody>
                  </Card>}

        {this.state.isShowAgentPrev==true && <Card className="mb-0">
              
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                    <th width="71"></th>
                    {global.isShowArabicLanguage==false && <th className="text-center" width="100" onClick={()=>this.sortById()}>ID</th>}
                    {global.isShowArabicLanguage==true && <th className="text-center" width="100" onClick={()=>this.sortById()}>رقم التعريف</th>}

                    <th className="text-left" onClick={()=>this.sortByName()}>Name</th>
                    <th className="text-center">
                    Assig.&nbsp;Counters
                        {/* <ButtonDropdown id="card1" isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                          <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                          Assig. Counters
                          </DropdownToggle>
                          <DropdownMenu right>
                            <DropdownItem>Action</DropdownItem>
                            <DropdownItem>Another action</DropdownItem>
                            <DropdownItem disabled>Disabled action</DropdownItem>
                            <DropdownItem>Something else here</DropdownItem>
                          </DropdownMenu>
                        </ButtonDropdown> */}
                    </th>
                    <th className="text-left" onClick={()=>this.sortByBranchName()}>Branch</th>
                    <th className="text-center">
                      Rating
                    {/* <ButtonDropdown id="card2" isOpen={this.state.card2} toggle={() => { this.setState({ card2: !this.state.card2 }); }}>
                          <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                          Privilegs
                          </DropdownToggle>
                          <DropdownMenu right>
                            <DropdownItem>Action</DropdownItem>
                            <DropdownItem>Another action</DropdownItem>
                            <DropdownItem disabled>Disabled action</DropdownItem>
                            <DropdownItem>Something else here</DropdownItem>
                          </DropdownMenu>
                        </ButtonDropdown> */}
                    </th>
                    <th className="text-center" onClick={()=>this.sortByAST()}>Avg Handle Time</th>
                    <th className="text-center" onClick={()=>this.sortByDates()}>Last Login</th>
                    <th></th>
                  </tr>
                  {/* {this.state.isShowFilters==true && <tr id="filter-row">
                    <th></th>
                    
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line">
                        <select className="form-control">
                          <option value='0' key='0'>
                            Select Option
                          </option>
                          <option value='0' key='0'>
                            Option 2
                          </option>
                        </select>
                      </FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    
                    <th>
                      <Button color="primary" className="btn-sm">Apply</Button>
                    </th>
                  </tr>} */}
                  </thead>
                  <tbody>
                  {this.state.allAgents.map(agent => (
                     
                     <tr>
                       <td className="text-right" style={{verticalAlign: "middle"}}>
                         <div className="checkbox form-check">
                             <input type="checkbox" className="form-check-input" checked={agent.agent.isChecked} onChange={()=>this.select(agent.agent)}></input>
                             <label className="form-check-label form-check-label"></label>
                         </div>
                       </td>
                       
                  <td className="text-center" style={{verticalAlign: "middle"}}>{agent.agent.serviceCenterEmployee.employeeNumber}</td>
                       <td style={{verticalAlign: "middle"}}>{agent.agent.serviceCenterEmployee.user.name}</td>
                       <td className="text-center" style={{verticalAlign: "middle"}}>
                           {agent.assignedBranchCounter!=null && <span className="badge badge-success">{agent.assignedBranchCounter.counterNumber}</span>}
                           {agent.assignedBranchCounter==null && <span className="badge badge-danger">Nil</span>}
                       </td>
                       {agent.agent.branch!=null && <td>{agent.agent.branch.branchName}<p className="text-light-grey"><i className="icon2-location"></i> {agent.agent.branch.address}</p></td>}
                       {agent.agent.branch==null && <td>Nil</td>}
                       <td className="text-center" style={{verticalAlign: "middle"}}>
                       

                  {agent.rating < 2 &&<i className="icon2-normal-face text-warning f20"></i>}

                  {agent.rating >= 2 && agent.rating < 4 &&<i className="icon2-normal-face text-warning f20"></i>}

                  {agent.rating >= 4 && agent.rating <= 5 &&<i className="icon2-smile text-success f20"></i>}

                   
                    {/* {agent.assignedPrivileges.length}< */}
                    
                      </td>
                   
                       <td className="text-center" style={{verticalAlign: "middle"}}>{agent.agent.AST} <small className="text-light-grey">min</small></td>
                       <td className="text-center" style={{verticalAlign: "middle"}}>
                         {agent.agent.serviceCenterEmployee.user.lastLoginTime!=null && <span>
                          {agent.agent.serviceCenterEmployee.user.time} <p className="text-light-grey">{agent.agent.serviceCenterEmployee.user.date}</p>
                         </span>}
                         {agent.agent.serviceCenterEmployee.user.lastLoginTime==null && <span style={{ color: 'red' }}>
                          Nil
                         </span>}
                         </td>
                         
                       <td className="text-center" style={{verticalAlign: "middle"}}>
                       <Link className="ml-4" onClick={()=>this.showMenu(agent.agent.agentId)}><i className="f20 icon2-menu-options text-light-grey"></i></Link>
                     {agent.isShowMenu==true && 
                     <Dropdown isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu:false }); }} >
                            <DropdownToggle
                              tag="span"
                              onClick={() => {this.setState({ isShowDropDownMenu:false })}}
                              data-toggle="dropdown"
                              >
                            </DropdownToggle>
                            <DropdownMenu>
                           <DropdownItem onClick={()=>this.agentDetails(agent.agent.agentId)}>Details</DropdownItem>
                           
                            </DropdownMenu>
                      </Dropdown>}
                       </td>
                     </tr>
                  ))}
                  </tbody>
                </Table>
               
              </CardBody>
            </Card>}

                <Modal isOpen={this.state.isAgentModal} 
                       className={'modal-lg ' + this.props.className}>
                  {global.isShowArabicLanguage==false && <ModalHeader>Add Agent</ModalHeader>}
                  {global.isShowArabicLanguage==true && <ModalHeader>إضافة العميل</ModalHeader>}
                  
                  <ModalBody>


                  <Row className="mt-3">
                  <Col sm="6">
                <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Agent ID</Label>}
                    {global.isShowArabicLanguage==true &&<Label className="text-light-grey">رمز العميل</Label>}
                    <Input type="text" placeholder="Service Center Name" autoComplete="username" readOnly value={this.state.NextAgentId} />
                    {global.isShowArabicLanguage==false &&<small><span className="text-danger">*</span> Generated by System</small>}
                    {global.isShowArabicLanguage==true &&<small><span className="text-danger">*</span> تم إنشاؤها بواسطة النظام</small>}
                  </FormGroup>
                </Col>
               <Col sm="6">
                <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false && <Label className="text-light-grey">PIN</Label>}
                    {global.isShowArabicLanguage==true && <Label className="text-light-grey">رقم التعريف الشخصي</Label>}
                    <Input type="text" placeholder="Service Center Name" autoComplete="username" readOnly value={this.state.AgentPIN} />
                    {global.isShowArabicLanguage==false && <small ><span className="text-danger">*</span> Changed on first login</small>}
                    {global.isShowArabicLanguage==true && <small ><span className="text-danger">*</span> يستبدل عند أول تسجيل دخول</small>}
                  </FormGroup>
                </Col>
                
              </Row>
                 <FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label>
                    <Input type="text" placeholder="Agent Name" 
                     value={this.state.agentName} onChange={(e) => this.updateAgentName(e.target.value)} />
                     < ValidationMessage valid={this.state.agentNameValid} message={this.state.errorMsg.agentName} />
               </FormGroup>
              
               <FormGroup className="input-line">
                  <Label className="text-light-grey">Email</Label>
                  <Input type="email" placeholder="Email" autoComplete="username" name="email" id="exampleEmail"
                  value={this.state.agentEmail} onChange={(e) => this.updateAgentEmail(e.target.value)}/>
                  < ValidationMessage valid={this.state.agentEmailValid} message={this.state.errorMsg.agentEmail} />
                </FormGroup>
               <FormGroup className="input-line">
                  <Label className="text-light-grey">Branch</Label>
                  <select className="form-control" onChange={this.changeAgentBranch}>
                  <option value='0' key='0'>
                      Branches
                    </option>
                  {this.state.allBranches.map(branch => (
                    <option value={branch.branchId} key={branch.branchId}>
                      {branch.branchName}
                    </option>
                  ))}
                  </select>
                </FormGroup>                  
                  <div className="mb-2">
                  {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Privilages</Label>}
                  {global.isShowArabicLanguage==true &&<Label className="text-light-grey">الصلاحيات</Label>}
                  {this.state.allPrivileges.map(prev => (
                    <div className="checkbox">
                    <input type="checkbox" checked={prev.isChecked} value={prev.privilegeId} onChange={()=>this.changePriveleges(prev)}></input>
                    <label  data-content={prev.privilegeName}>{prev.privilegeName}</label>
                  </div>
                  ))}
                  </div>
                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeAgentModel()}>Cancel</Button>
                    <Button outline color="primary" onClick={()=>this.resetAgent()}>Reset</Button>
                    <Button color="primary" disabled={!this.state.formValid} onClick={()=>this.addUpdateAgent()}>Save</Button>
                  </ModalFooter>
                </Modal>
      

      


      </div>
    );
  }
}

export default Breadcrumbs;
