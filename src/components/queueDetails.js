import React, { Component } from 'react';
import { Badge, Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table, TabContent, TabPane,  DropdownItem,  DropdownMenu,  DropdownToggle,ListGroup, ListGroupItem } from 'reactstrap';
import axios from "axios";


import { Link, NavLink } from 'react-router-dom';
import * as router from 'react-router-dom';
import { UncontrolledDropdown, Nav, NavItem } from 'reactstrap';
import {
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import routes from './routes';
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css';
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
const params = {
  slidesPerView: 4,
  centeredSlides:true,
  spaceBetween: 0,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev'
  },
}
class Breadcrumbs extends Component {
  counter=0;
  tempTrackLastCounter=0
  tempTrackOtherCounter=0
  
  trackCounter=0
  tempQueues=[]
  branchId=0
  serviceId=0
  tempQueueTicket=[]
  totalQueue=0
  fastPassTypeCounter=0
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      queueNumberLength:0,
      isShowNew:false,
      queueNumberList:[],
      activeTab: new Array(4).fill('1'),
      branchName:'',
      branchAddress:'',
      totalTickets:0,
      servicesAllocated:0,
      countersAllocated:0,
      totalCounters:0,
      totalAgents:0,
      AST:0,
      AWT:0,
      allQueue:[],
      allQueueTicket:[],
      searchQueue:'',
    };
  }

 

  toggle(id) {
    for(var i=0;i<this.tempQueues.length;i++)
    {
      if(id==this.tempQueues[i].queue.queueId)
      {
        this.tempQueues[i].queue['isActive']=true
      }
      else{
        this.tempQueues[i].queue['isActive']=false
      }
    }
    this.setState({
      allQueue:this.tempQueues,
     
    })
    this.queueDetails(id)
  }

  search()
  {
    
    if(this.state.searchQueue!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempQueueTicket.length;i++)
      {
        if(this.state.searchQueue==this.tempQueueTicket[i].ticket.ticketNumber)
        {
          
          data.push(this.tempQueueTicket[i])
          this.setState({
            allQueueTicket:data
          })
          isMatch=true
          this.totalQueue=data.length
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
          
        }
      }
      if(isMatch==false)
      {
        this.totalQueue=0
        this.setState({allQueueTicket:[]})
      }
    }
    else
    {
      this.totalQueue=this.tempQueueTicket.length
      this.setState({
        allQueueTicket:this.tempQueueTicket
      })
    }
  
  }
  changeSearch(queueNumber)
  {
    if(queueNumber=='')
    { 
      this.totalQueue=this.tempQueueTicket.length
      this.setState({
        allQueueTicket:this.tempQueueTicket
      })
    }
    this.setState({
      searchQueue:queueNumber
      })
  }
queueList(id)
{
    this.counter=0;
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/queue/getListing';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
    if(responseJson.data.applicationStatusCode==0)
    {
      var data=[]
      for(var i=0;i<responseJson.data.queueList.length;i++)
      {
        if(id==responseJson.data.queueList[i].queue.queueId)
        {
          responseJson.data.queueList[i].queue['isActive']=true
        }
        else{
          responseJson.data.queueList[i].queue['isActive']=false
        }
        this.tempQueues.push(responseJson.data.queueList[i])
        if(i<3)
        {
          this.counter=this.counter+1
          
          this.trackCounter=1
          data.push(responseJson.data.queueList[i])

        }
        
      }
      
      this.setState({
        allQueue:responseJson.data.queueList,
        queueNumberList:data,
        queueNumberLength:this.counter
       
      })
    }
  }).catch(err=>{
    console.log(err)
  });

}
queueDetails(id)
{
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/queue/getDetails', {
    method: 'POST',
    body:JSON.stringify({
     "id": parseInt(id)
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responsejson => {
    if(responsejson.applicationStatusCode==0)
    {
     var hms = responsejson.queueList[0].queue.averageServiceTime;  
     var a = hms.split(':'); 
     var minutes = (+a[0]) * 60 + (+a[1]); 
   
     var hmss = responsejson.queueList[0].queue.averageWaitTime;  
     var a = hmss.split(':'); 
     var minutess = (+a[0]) * 60 + (+a[1]); 
     this.branchId=responsejson.queueList[0].queue.branch.branchId
     this.serviceId=responsejson.queueList[0].queue.service.serviceId
     this.queueTicket(this.branchId,this.serviceId)
      this.setState({
        branchName:responsejson.queueList[0].queue.branch.branchName,
        branchAddress:responsejson.queueList[0].queue.branch.address,
        totalTickets:responsejson.queueList[0].noOfTickets,
        AST:minutes,
        AWT:minutess,
        countersAllocated:responsejson.queueList[0].noOfServingCounters,
        totalCounters:responsejson.queueList[0].noOfTotalCounters,
        totalAgents:responsejson.queueList[0].noOfAgents,
        //  servicesAllocated:responsejson.queueList[0].queue.noOfTickets,
       
      })
      
    }
   

  })
}
queueTicket(branchId,serviceId)
{
  var data=[]
  data.push(branchId)
  data.push(serviceId)
  //   data.push(2)
  // data.push(4)
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/queuemanagement/queue/tickets/get', {
    method: 'POST',
    body:JSON.stringify({
     "ids": data
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responsejson => {
    if(responsejson.applicationStatusCode==0)
    {
      this.tempQueueTicket=[]
      var counter=0
      for(var i=0;i<responsejson.ticketList.length;i++)
      {
        var date=new Date(responsejson.ticketList[i].ticket.createdTime)
        let hours = date.getHours();
        let minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12;
        hours = hours < 10 ? '0' + hours : hours;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        
        responsejson.ticketList[i].ticket['time']=strTime
        if(responsejson.ticketList[i].ticket.waitingTime!=null)
        {
          var hms = responsejson.ticketList[i].ticket.waitingTime;  
          var a = hms.split(':'); 
          var h=a[0]
          var m=a[1]
          h = h % 12;
          h = h ? h : 12;
          h = h < 10 ? '0' + h : h;
          m = m < 10 && m > 0 ? '0' + m : m;
          var wTime = h + ':' + m 
          responsejson.ticketList[i].ticket['wTime']=wTime
        }
        else
        {
          responsejson.ticketList[i].ticket['wTime']=null
        }


        this.tempQueueTicket.push(responsejson.ticketList[i])
        if(responsejson.ticketList[i].ticket.ticketType==1)
        {
          counter=counter+1
        }
        
      }
      this.fastPassTypeCounter=counter
      this.totalQueue=this.tempQueueTicket.length;
      this.setState({
        allQueueTicket:responsejson.ticketList
      })
    }
   

  })
}
next()
{
  
  if(this.state.queueNumberLength<this.state.allQueue.length)
  {
    this.setState({isShowNew:true})
    var data=[]
    for(var i=0,j=this.trackCounter;i<3;i++,j++)
    {
      if(j==this.state.allQueue.length)
      {
        // this.tempTrackLastCounter=i
        break;
      }
     data.push(this.tempQueues[j])
      this.counter=j+1      
    }
    this.trackCounter=this.trackCounter+1
    this.setState({
      queueNumberList:data,
      queueNumberLength:this.counter
     
    })
  }
  else
  {
    this.setState({isShowNew:false})
  }
}
prev()
{
  
  if(this.trackCounter>1)
  {
    var data=[]
    var index=0
    for(var i=3,j=this.trackCounter;i>0;i--,j--)
    {

      data.push(this.tempQueues[j])
      this.counter=j-1
    }
    this.trackCounter=this.trackCounter-1
    this.setState({
      queueNumberList:data.reverse(),
      queueNumberLength:this.counter
     
    })
  }
  else
  {
    this.setState({isShowNew:false})
  }
}
 componentDidMount()
 {
  if(localStorage.getItem('isShowArabicLanguage')=='true')
  {
    global.isShowArabicLanguage=true
  }
  else
  {
    global.isShowArabicLanguage=false
  }
   var data=this.props.location
   this.queueList(parseInt(data.queueId))
   this.queueDetails(parseInt(data.queueId))
   
 }
  render() {
    return (
      <div className="animated fadeIn">
        <DefaultHeader/>
        <div className="tabs-line">
        <Row>
          <Col sm="1">
              <button className="btn btn-linkml-3" disabled={!this.state.isShowNew} onClick={()=>this.prev()}>
                <i className="fa fa-angle-left fa-2x" aria-hidden="true"></i>
              </button>
          </Col>
          <Col sm="10">
          <Nav tabs>
              {this.state.queueNumberList.map(queue => (
                <NavItem>
                  <NavLink className="nav-link" to="#"
                    active={queue.queue.isActive}
                    isActive={(match, location) => {
                      if (!queue.queue.isActive) {
                        return false;
                      }
                      else{
                        return true;
                      }
                    }}
                    onClick={() => { this.toggle(queue.queue.queueId); }}
                  >
                    <h5 className="mb-0">{queue.queue.queueNumber}</h5>
                    <p className="text-light-grey mb-0"><i className="icon2-calendar"></i> 08:00 PM - 12.10.19</p>
                  </NavLink>
                </NavItem>
              ))}
              </Nav>
          </Col>
          <Col sm="1">
              <button className="btn btn-link"  style={{ float: 'right' }} onClick={()=>this.next()} disabled={this.state.queueNumberLength==this.state.allQueue.length}><i className="fa fa-angle-right fa-2x" aria-hidden="true"></i></button>
          </Col>
        </Row>
       

          
            <TabContent className="pt-5" activeTab={this.state.activeTab[0]}>
            <TabPane tabId="1">
            <Row>
              <Col sm="3">
                <h4 className="mb-0">{this.state.branchName}</h4>
                <p className="text-light-grey"><i className="icon2-location text-light-grey"></i>{this.state.branchAddress} <Link>Open Map</Link></p>
              </Col>
              <Col sm="2">
                <p className="mb-1 font-sm no-wrap"><i className="icon2-recipt text-light-grey"></i> Total #Tickets {this.state.totalTickets}</p>
                <p className="font-sm no-wrap"><i className="icon2-settings-gear text-light-grey"></i> Services 1</p>
              </Col>
              <Col sm="2">
                <p className="mb-1 font-sm no-wrap"><i className="icon2-counter text-light-grey"></i> Counters {this.state.countersAllocated}<span className="text-light-grey">/{this.state.totalCounters}</span></p>
                <p className="font-sm no-wrap"><i className="icon2-username text-light-grey"></i> Agents {this.state.totalAgents}</p>
              </Col>
              <Col sm="5" className="text-right" style={{alignSelf: 'center',}}>
                <span className="badge badge-info pl-3 pr-3 mb-1 mr-2"><i className="icon2-wall-clock"></i> Avg. Service Time {this.state.AST} min</span>
                <span className="badge badge-info pl-3 pr-3"><i className="icon2-hourglass"></i> Avg. Waiting Time {this.state.AWT} min</span>
              </Col>
            </Row>

            <Card className="mb-0">
              
              <CardBody className="p-0">

              <div className="pt-3 pl-3 pr-3 pb-1">

              <Row>
                <Col sm="4">
                  {global.isShowArabicLanguage==false && <h3 className="mb-0">Tickets in Queue . {this.totalQueue}</h3>}
                  {global.isShowArabicLanguage==true && <h3 className="mb-0">تذاكر في الطابور . {this.totalQueue}</h3>}

                  <p className="text-warning mb-0">{this.fastPassTypeCounter} Fast Pass</p>
                </Col>

                <Col sm="8" className="text-right">
                
                  <div className="float-right">
                    <div className="btn-group">
                      <button className="btn btn-outline-secondary w-100 ml-2 mr-2">Sort By <i className="icon2-sort-descending"></i></button>
                      <select className="form-control">
                        <option>View All</option>
                        <option>Option 1</option>
                        <option>Option 2</option>
                      </select>
                    </div>
                  </div>
                  <div className="input-group float-right w-auto">
                      <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                      value={this.state.searchQueue} onChange={(e) => this.changeSearch(e.target.value)}
                      onKeyPress={event => {
                        if (event.key === 'Enter') {
                          this.search()
                        }
                      }}
                      
                      
                      ></input>
                    <div className="input-group-append"><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
                  </div>
                  <div className="clearfix"></div>

                </Col>
              </Row>
              </div>

                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                    <th className="text-center">#</th>
                    <th className="text-center">Ticket</th>
                    <th className="text-center">Created at</th>
                    <th className="text-center">WT</th>
                    <th className="text-center">Counter</th>
                    <th>Agent</th>
                    <th>Services Name</th>
                    <th className="text-center">Type</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.state.allQueueTicket.map((queue,i)=> (
                    
                    <tr>
                      <td className="text-center">{i+1}</td>
                      <td className="text-center">{queue.ticket.ticketNumber}</td>
                      <td className="text-center">{queue.ticket.time}</td>
                      {queue.ticket.wTime!=null && <td className="text-center">{queue.ticket.wTime} min</td>}
                      {queue.ticket.wTime==null && <td className="text-center">0 min</td>}
                      <td className="text-center">
                      {queue.counterNumber!=null &&<span>
                          {queue.ticket.counterNumber}
                        </span>}
                        {queue.counterNumber==null &&<span style={{ color: 'red' }}>
                          Nil
                        </span>}
                        </td>
                      {/* <td className="text-center">{queue.ticket.counterNumber}</td> */}
                      <td>
                        {queue.agent!=null &&<span>
                          <p className="text-light-grey">{queue.agent.serviceCenterEmployee.employeeNumber}</p><br></br>
                        <h6>{queue.agent.serviceCenterEmployee.user.name}</h6>
                        </span>}
                        {queue.agent==null &&<span style={{ color: 'red',textAlign:"center" }}>
                         -<br></br>
                         
                        </span>}
                       
                        </td>
                        {queue.ticket.serviceName!=null && <td>{queue.ticket.serviceName}</td>}
                        {queue.ticket.serviceName==null && <td>Nil</td>}
                      <td className="text-center">
                        {queue.ticket.ticketType==2 && <span className="badge badge-info">Standard</span>}
                        {queue.ticket.ticketType==1 && <span className="badge badge-warning">Fast Pass</span>}
                        {queue.ticket.ticketType==3 && <span className="badge badge-info">Stepout</span>}
                        {queue.ticket.ticketType==4 && <span className="badge badge-info">Cancel</span>}
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>
                    /* <tr>
                      <td className="text-center">2</td>
                      <td className="text-center">J-1345</td>
                      <td className="text-center">10:03 AM</td>
                      <td className="text-center">35.03 min</td>
                      <td className="text-center">2</td>
                      <td>
                        k4872<br></br>
                        <h6>Agent Name</h6>
                        </td>
                      <td>doller sit amet conset ipsum.</td>
                      <td className="text-center">
                        <span className="badge badge-warning">Fast Pass</span>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>
                    <tr>
                      <td className="text-center">3</td>
                      <td className="text-center">J-1345</td>
                      <td className="text-center">10:03 AM</td>
                      <td className="text-center">35.03 min</td>
                      <td className="text-center">2</td>
                      <td>
                        k4872<br></br>
                        <h6>Agent Name</h6>
                        </td>
                      <td>doller sit amet conset ipsum.</td>
                      <td className="text-center">
                      <span className="badge badge-warning">Fast Pass</span>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>
                    <tr>
                      <td className="text-center">4</td>
                      <td className="text-center">J-1345</td>
                      <td className="text-center">10:03 AM</td>
                      <td className="text-center">35.03 min</td>
                      <td className="text-center">2</td>
                      <td>
                        k4872<br></br>
                        <h6>Agent Name</h6>
                        </td>
                      <td>doller sit amet conset ipsum.</td>
                      <td className="text-center">
                        <span className="badge badge-info">Standard</span>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>
                    <tr>
                      <td className="text-center">5</td>
                      <td className="text-center">J-1345</td>
                      <td className="text-center">10:03 AM</td>
                      <td className="text-center">35.03 min</td>
                      <td className="text-center">2</td>
                      <td>
                        k4872<br></br>
                        <h6>Agent Name</h6>
                        </td>
                      <td>doller sit amet conset ipsum.</td>
                      <td className="text-center">
                        <span className="badge badge-info">Standard</span>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr> */
                    
                    
                    
                  ))}
</tbody>
                </Table>
               
              </CardBody>
            </Card>
          
          

              </TabPane>
              <TabPane tabId="2">
                
              <Row>
              <Col sm="3">
                <h4 className="mb-0">Branch's Name</h4>
                <p><i className="icon2-location text-light-grey"></i> Location here. <Link>Open Map</Link></p>
              </Col>
              <Col sm="2">
                <p className="mb-1"><i className="icon2-recipt text-light-grey"></i> <b>Total #Tickets 8</b></p>
                <p><i className="fa fa-gear text-light-grey"></i> <b>Services 3</b>/5</p>
              </Col>
              <Col sm="2">
                <p className="mb-1"><i className="fa fa-file-o text-light-grey"></i> <b>Counters 4</b>/5</p>
                <p><i className="icon2-username text-light-grey"></i> <b>Agents 4</b>/6</p>
              </Col>
              <Col sm="5" className="text-right">
                <span className="badge badge-info pl-3 pr-3 mb-1"><i className="icon2-username"></i> Avg. Service Time 30 min</span>
                <br></br>
                <span className="badge badge-info pl-3 pr-3"><i className="icon2-hourglass"></i> Avg. Waiting Time 22 min</span>
              </Col>
            </Row>


              </TabPane>
              
            </TabContent>
            </div>




      


      </div>
    );
  }
}

export default Breadcrumbs;
