import React, { Component } from 'react';
import { Card, CardBody, Col, Row, Table,  
  Dropdown, DropdownItem, DropdownMenu, DropdownToggle,Button,ButtonDropdown, FormGroup,Label,Input,Alert,
  Modal, ModalBody, ModalFooter, ModalHeader, } from 'reactstrap';

import { Link } from 'react-router-dom'; 
import { DatePickerComponent  } from '@syncfusion/ej2-react-calendars'; 
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import LoadingBar from 'react-top-loading-bar';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
var formData=new FormData()
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
class Branch extends Component {
  tempUsers=[]
  tempRoles=[]
  tempPrev=[]
  totalUsers=0
  counter=0
  tempBranches=[]
  roleId=0
  countries= [
    {
        "code": "+1",
        "name": "Canada"
    },
    {
        "code": "+1",
        "name": "United States"
    },
    {
        "code": "+1 242",
        "name": "Bahamas"
    },
    {
        "code": "+1 246",
        "name": "Barbados"
    },
    {
        "code": "+1 264",
        "name": "Anguilla"
    },
    {
        "code": "+1 268",
        "name": "Antigua and Barbuda"
    },
    {
        "code": "+1 268",
        "name": "Barbuda"
    },
    {
        "code": "+1 284",
        "name": "British Virgin Islands"
    },
    {
        "code": "+1 340",
        "name": "U.S. Virgin Islands"
    },
    {
        "code": "+1 441",
        "name": "Bermuda"
    },
    {
        "code": "+1 473",
        "name": "Grenada"
    },
    {
        "code": "+1 649",
        "name": "Turks and Caicos Islands"
    },
    {
        "code": "+1 670",
        "name": "Northern Mariana Islands"
    },
    {
        "code": "+1 671",
        "name": "Guam"
    },
    {
        "code": "+1 684",
        "name": "American Samoa"
    },
    {
        "code": "+1 767",
        "name": "Dominica"
    },
    {
        "code": "+1 787",
        "name": "Puerto Rico"
    },
    {
        "code": "+1 808",
        "name": "Midway Island"
    },
    {
        "code": "+1 808",
        "name": "Wake Island"
    },
    {
        "code": "+1 809",
        "name": "Dominican Republic"
    },
    {
        "code": "+1 868",
        "name": "Trinidad and Tobago"
    },
    {
        "code": "+1 869",
        "name": "Nevis"
    },
    {
        "code": "+1 876",
        "name": "Jamaica"
    },
    {
        "code": "+1664",
        "name": "Montserrat"
    },
    {
        "code": "+20",
        "name": "Egypt"
    },
    {
        "code": "+212",
        "name": "Morocco"
    },
    {
        "code": "+213",
        "name": "Algeria"
    },
    {
        "code": "+216",
        "name": "Tunisia"
    },
    {
        "code": "+218",
        "name": "Libya"
    },
    {
        "code": "+220",
        "name": "Gambia"
    },
    {
        "code": "+221",
        "name": "Senegal"
    },
    {
        "code": "+222",
        "name": "Mauritania"
    },
    {
        "code": "+223",
        "name": "Mali"
    },
    {
        "code": "+224",
        "name": "Guinea"
    },
    {
        "code": "+225",
        "name": "Ivory Coast"
    },
    {
        "code": "+226",
        "name": "Burkina Faso"
    },
    {
        "code": "+227",
        "name": "Niger"
    },
    {
        "code": "+228",
        "name": "Togo"
    },
    {
        "code": "+229",
        "name": "Benin"
    },
    {
        "code": "+230",
        "name": "Mauritius"
    },
    {
        "code": "+231",
        "name": "Liberia"
    },
    {
        "code": "+232",
        "name": "Sierra Leone"
    },
    {
        "code": "+233",
        "name": "Ghana"
    },
    {
        "code": "+234",
        "name": "Nigeria"
    },
    {
        "code": "+235",
        "name": "Chad"
    },
    {
        "code": "+236",
        "name": "Central African Republic"
    },
    {
        "code": "+237",
        "name": "Cameroon"
    },
    {
        "code": "+238",
        "name": "Cape Verde"
    },
    {
        "code": "+240",
        "name": "Equatorial Guinea"
    },
    {
        "code": "+241",
        "name": "Gabon"
    },
    {
        "code": "+242",
        "name": "Congo"
    },
    {
        "code": "+243",
        "name": "Congo, Dem. Rep. of (Zaire)"
    },
    {
        "code": "+244",
        "name": "Angola"
    },
    {
        "code": "+245",
        "name": "Guinea-Bissau"
    },
    {
        "code": "+246",
        "name": "British Indian Ocean Territory"
    },
    {
        "code": "+246",
        "name": "Diego Garcia"
    },
    {
        "code": "+247",
        "name": "Ascension"
    },
    {
        "code": "+248",
        "name": "Seychelles"
    },
    {
        "code": "+249",
        "name": "Sudan"
    },
    {
        "code": "+250",
        "name": "Rwanda"
    },
    {
        "code": "+251",
        "name": "Ethiopia"
    },
    {
        "code": "+253",
        "name": "Djibouti"
    },
    {
        "code": "+254",
        "name": "Kenya"
    },
    {
        "code": "+255",
        "name": "Tanzania"
    },
    {
        "code": "+255",
        "name": "Zanzibar"
    },
    {
        "code": "+256",
        "name": "Uganda"
    },
    {
        "code": "+257",
        "name": "Burundi"
    },
    {
        "code": "+260",
        "name": "Zambia"
    },
    {
        "code": "+261",
        "name": "Madagascar"
    },
    {
        "code": "+262",
        "name": "Mayotte"
    },
    {
        "code": "+262",
        "name": "Reunion"
    },
    {
        "code": "+263",
        "name": "Zimbabwe"
    },
    {
        "code": "+264",
        "name": "Namibia"
    },
    {
        "code": "+265",
        "name": "Malawi"
    },
    {
        "code": "+266",
        "name": "Lesotho"
    },
    {
        "code": "+267",
        "name": "Botswana"
    },
    {
        "code": "+268",
        "name": "Swaziland"
    },
    {
        "code": "+269",
        "name": "Comoros"
    },
    {
        "code": "+27",
        "name": "South Africa"
    },
    {
        "code": "+291",
        "name": "Eritrea"
    },
    {
        "code": "+297",
        "name": "Aruba"
    },
    {
        "code": "+298",
        "name": "Faroe Islands"
    },
    {
        "code": "+299",
        "name": "Greenland"
    },
    {
        "code": "+30",
        "name": "Greece"
    },
    {
        "code": "+31",
        "name": "Netherlands"
    },
    {
        "code": "+32",
        "name": "Belgium"
    },
    {
        "code": "+33",
        "name": "France"
    },
    {
        "code": "+34",
        "name": "Spain"
    },
    {
        "code": "+350",
        "name": "Gibraltar"
    },
    {
        "code": "+351",
        "name": "Portugal"
    },
    {
        "code": "+352",
        "name": "Luxembourg"
    },
    {
        "code": "+353",
        "name": "Ireland"
    },
    {
        "code": "+354",
        "name": "Iceland"
    },
    {
        "code": "+355",
        "name": "Albania"
    },
    {
        "code": "+356",
        "name": "Malta"
    },
    {
        "code": "+358",
        "name": "Finland"
    },
    {
        "code": "+359",
        "name": "Bulgaria"
    },
    {
        "code": "+36",
        "name": "Hungary"
    },
    {
        "code": "+370",
        "name": "Lithuania"
    },
    {
        "code": "+371",
        "name": "Latvia"
    },
    {
        "code": "+372",
        "name": "Estonia"
    },
    {
        "code": "+373",
        "name": "Moldova"
    },
    {
        "code": "+374",
        "name": "Armenia"
    },
    {
        "code": "+375",
        "name": "Belarus"
    },
    {
        "code": "+376",
        "name": "Andorra"
    },
    {
        "code": "+377",
        "name": "Monaco"
    },
    {
        "code": "+378",
        "name": "San Marino"
    },
    {
        "code": "+380",
        "name": "Ukraine"
    },
    {
        "code": "+381",
        "name": "Serbia"
    },
    {
        "code": "+382",
        "name": "Montenegro"
    },
    {
        "code": "+385",
        "name": "Croatia"
    },
    {
        "code": "+386",
        "name": "Slovenia"
    },
    {
        "code": "+387",
        "name": "Bosnia and Herzegovina"
    },
    {
        "code": "+389",
        "name": "Macedonia"
    },
    {
        "code": "+39",
        "name": "Italy"
    },
    {
        "code": "+40",
        "name": "Romania"
    },
    {
        "code": "+41",
        "name": "Switzerland"
    },
    {
        "code": "+420",
        "name": "Czech Republic"
    },
    {
        "code": "+421",
        "name": "Slovakia"
    },
    {
        "code": "+423",
        "name": "Liechtenstein"
    },
    {
        "code": "+43",
        "name": "Austria"
    },
    {
        "code": "+44",
        "name": "United Kingdom"
    },
    {
        "code": "+45",
        "name": "Denmark"
    },
    {
        "code": "+46",
        "name": "Sweden"
    },
    {
        "code": "+47",
        "name": "Norway"
    },
    {
        "code": "+48",
        "name": "Poland"
    },
    {
        "code": "+49",
        "name": "Germany"
    },
    {
        "code": "+500",
        "name": "Falkland Islands"
    },
    {
        "code": "+500",
        "name": "South Georgia and the South Sandwich Islands"
    },
    {
        "code": "+501",
        "name": "Belize"
    },
    {
        "code": "+502",
        "name": "Guatemala"
    },
    {
        "code": "+503",
        "name": "El Salvador"
    },
    {
        "code": "+504",
        "name": "Honduras"
    },
    {
        "code": "+505",
        "name": "Nicaragua"
    },
    {
        "code": "+506",
        "name": "Costa Rica"
    },
    {
        "code": "+507",
        "name": "Panama"
    },
    {
        "code": "+509",
        "name": "Haiti"
    },
    {
        "code": "+51",
        "name": "Peru"
    },
    {
        "code": "+52",
        "name": "Mexico"
    },
    {
        "code": "+53",
        "name": "Cuba"
    },
    {
        "code": "+537",
        "name": "Cyprus"
    },
    {
        "code": "+54",
        "name": "Argentina"
    },
    {
        "code": "+55",
        "name": "Brazil"
    },
    {
        "code": "+56",
        "name": "Chile"
    },
    {
        "code": "+56",
        "name": "Easter Island"
    },
    {
        "code": "+57",
        "name": "Colombia"
    },
    {
        "code": "+58",
        "name": "Venezuela"
    },
    {
        "code": "+590",
        "name": "Guadeloupe"
    },
    {
        "code": "+591",
        "name": "Bolivia"
    },
    {
        "code": "+593",
        "name": "Ecuador"
    },
    {
        "code": "+594",
        "name": "French Guiana"
    },
    {
        "code": "+595",
        "name": "Guyana"
    },
    {
        "code": "+595",
        "name": "Paraguay"
    },
    {
        "code": "+596",
        "name": "French Antilles"
    },
    {
        "code": "+596",
        "name": "Martinique"
    },
    {
        "code": "+597",
        "name": "Suriname"
    },
    {
        "code": "+598",
        "name": "Uruguay"
    },
    {
        "code": "+599",
        "name": "Curacao"
    },
    {
        "code": "+599",
        "name": "Netherlands Antilles"
    },
    {
        "code": "+60",
        "name": "Malaysia"
    },
    {
        "code": "+61",
        "name": "Australia"
    },
    {
        "code": "+61",
        "name": "Christmas Island"
    },
    {
        "code": "+61",
        "name": "Cocos-Keeling Islands"
    },
    {
        "code": "+62",
        "name": "Indonesia"
    },
    {
        "code": "+63",
        "name": "Philippines"
    },
    {
        "code": "+64",
        "name": "New Zealand"
    },
    {
        "code": "+65",
        "name": "Singapore"
    },
    {
        "code": "+66",
        "name": "Thailand"
    },
    {
        "code": "+670",
        "name": "East Timor"
    },
    {
        "code": "+670",
        "name": "Timor Leste"
    },
    {
        "code": "+672",
        "name": "Australian External Territories"
    },
    {
        "code": "+672",
        "name": "Norfolk Island"
    },
    {
        "code": "+673",
        "name": "Brunei"
    },
    {
        "code": "+674",
        "name": "Nauru"
    },
    {
        "code": "+675",
        "name": "Papua New Guinea"
    },
    {
        "code": "+676",
        "name": "Tonga"
    },
    {
        "code": "+677",
        "name": "Solomon Islands"
    },
    {
        "code": "+678",
        "name": "Vanuatu"
    },
    {
        "code": "+679",
        "name": "Fiji"
    },
    {
        "code": "+680",
        "name": "Palau"
    },
    {
        "code": "+681",
        "name": "Wallis and Futuna"
    },
    {
        "code": "+682",
        "name": "Cook Islands"
    },
    {
        "code": "+683",
        "name": "Niue"
    },
    {
        "code": "+685",
        "name": "Samoa"
    },
    {
        "code": "+686",
        "name": "Kiribati"
    },
    {
        "code": "+687",
        "name": "New Caledonia"
    },
    {
        "code": "+688",
        "name": "Tuvalu"
    },
    {
        "code": "+689",
        "name": "French Polynesia"
    },
    {
        "code": "+690",
        "name": "Tokelau"
    },
    {
        "code": "+691",
        "name": "Micronesia"
    },
    {
        "code": "+692",
        "name": "Marshall Islands"
    },
    {
        "code": "+7",
        "name": "Russia"
    },
    {
        "code": "+7 7",
        "name": "Kazakhstan"
    },
    {
        "code": "+7840",
        "name": "Abkhazia"
    },
    {
        "code": "+81",
        "name": "Japan"
    },
    {
        "code": "+82",
        "name": "South Korea"
    },
    {
        "code": "+84",
        "name": "Vietnam"
    },
    {
        "code": "+850",
        "name": "North Korea"
    },
    {
        "code": "+852",
        "name": "Hong Kong SAR China"
    },
    {
        "code": "+853",
        "name": "Macau SAR China"
    },
    {
        "code": "+855",
        "name": "Cambodia"
    },
    {
        "code": "+856",
        "name": "Laos"
    },
    {
        "code": "+86",
        "name": "China"
    },
    {
        "code": "+880",
        "name": "Bangladesh"
    },
    {
        "code": "+886",
        "name": "Taiwan"
    },
    {
        "code": "+90",
        "name": "Turkey"
    },
    {
        "code": "+91",
        "name": "India"
    },
    {
        "code": "+92",
        "name": "Pakistan"
    },
    {
        "code": "+93",
        "name": "Afghanistan"
    },
    {
        "code": "+94",
        "name": "Sri Lanka"
    },
    {
        "code": "+95",
        "name": "Myanmar"
    },
    {
        "code": "+960",
        "name": "Maldives"
    },
    {
        "code": "+961",
        "name": "Lebanon"
    },
    {
        "code": "+962",
        "name": "Jordan"
    },
    {
        "code": "+963",
        "name": "Syria"
    },
    {
        "code": "+964",
        "name": "Iraq"
    },
    {
        "code": "+965",
        "name": "Kuwait"
    },
    {
        "code": "+966",
        "name": "Saudi Arabia"
    },
    {
        "code": "+967",
        "name": "Yemen"
    },
    {
        "code": "+968",
        "name": "Oman"
    },
    {
        "code": "+970",
        "name": "Palestinian Territory"
    },
    {
        "code": "+971",
        "name": "United Arab Emirates"
    },
    {
        "code": "+972",
        "name": "Israel"
    },
    {
        "code": "+973",
        "name": "Bahrain"
    },
    {
        "code": "+974",
        "name": "Qatar"
    },
    {
        "code": "+975",
        "name": "Bhutan"
    },
    {
        "code": "+976",
        "name": "Mongolia"
    },
    {
        "code": "+977",
        "name": "Nepal"
    },
    {
        "code": "+98",
        "name": "Iran"
    },
    {
        "code": "+992",
        "name": "Tajikistan"
    },
    {
        "code": "+993",
        "name": "Turkmenistan"
    },
    {
        "code": "+994",
        "name": "Azerbaijan"
    },
    {
        "code": "+995",
        "name": "Georgia"
    },
    {
        "code": "+996",
        "name": "Kyrgyzstan"
    },
    {
        "code": "+998",
        "name": "Uzbekistan"
    }
]
  tempAvialableBranch=[]
  constructor(props) {
    super(props);
    this.state = {
      progress:0,
      filterId:'',filterName:'',filterRole:'',filterBranch:'',filterPrev:'',sDate:null,
      isAll:false,
      isSortBy:'ascending',
      empUserId:0,
      isShowDropDownMenu:false,
      isShowUserPrev:false,
      roleType:'',
      isShowEditUserPrev:false,
      allUsers:[],
      allRoles:[],
      empNumber:0,
      allBranhces:[],
      phoneCode:'',
      selectedBranchId:0,
      isShowUserModel:false,
      isShowUpdateUserModel:false,
      isShowRoleDropDown:false,
      isShowPrevDropDown:false,
      userRoles:[],
      selectedRoleId:0,
      selectedBranchObject:[],
      userName:'',userNameValid:false,
      email:'',userEmailValid:false,
      password:'',userPasswordValid:false,
      phone:'',phoneValid:false,
      formValid:false,
      errorMsg:{},
      userPrev:[],
      searchUserNo:'',
      modal: false,
      large: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
      isShowFilters:false,
      dropdownOpen: new Array(6).fill(false),
    };
    this.toggleLarge = this.toggleLarge.bind(this);
    this.toggle = this.toggle.bind(this);
   
    
  
  }
  componentDidMount()
  {
    if(localStorage.getItem('isShowArabicLanguage')=='true')
    {
      global.isShowArabicLanguage=true
    }
    else
    {
      global.isShowArabicLanguage=false
    }
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='User_View')
        {
          this.setState({isShowUserPrev:true})
          isPrev=true
        }
        else {
          if(!isPrev==true)
          {
            isPrev=false
          }
         
        }
        if(prev[i]=='User_Managment')
        {
          this.setState({isShowEditUserPrev:true})
        }
      }
    }
    if(isPrev==false)
    {
      toaster.notify("User don't have privilege", {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
    else{
      var type=localStorage.getItem('role')
      if(type!=undefined)
      {
        if(type.startsWith('Taboor'))
        {
          this.setState({roleType:'SuperAdmin'})
          this.getEmpUsers()
        }
        else {
          this.getServiceCenterUsers()
          this.setState({roleType:'Admin'})

        }
      }
      
    }
    
  }
  validateForm = () => {
    if(this.state.isShowUpdateUserModel==false)
    {
      const {userNameValid,userEmailValid,userPasswordValid,phoneValid} = this.state;
      this.setState({
        formValid: userNameValid && userEmailValid && userPasswordValid && phoneValid
      })
    }
    else{
      const {userNameValid,userEmailValid,phoneValid} = this.state;
      this.setState({
        formValid: userNameValid && userEmailValid  && phoneValid
      })
    }
   
  }
  updateUserName = (userName) => {
    this.setState({userName}, this.validateUserName)
  }
  validateUserName = () => {
    const {userName} = this.state;
    let userNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (userName.length == 0 ||userName=='' ) {
      userNameValid = false;
      errorMsg.userName = 'Required'
    }
    this.setState({userNameValid, errorMsg}, this.validateForm)
  }
  updateEmail = (email) => {
    this.setState({email}, this.validateEmail)
  }
  validateEmail = () => {
    const {email} = this.state;
    let userEmailValid = true;
    let errorMsg = {...this.state.errorMsg}

    // checks for format _@_._
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){
      userEmailValid = false;
      errorMsg.email = 'Invalid email format'
    }

    this.setState({userEmailValid, errorMsg}, this.validateForm)
  }
  updatePassword = (password) => {
    this.setState({password}, this.validatePassword);
  }

  validatePassword = () => {
    const {password} = this.state;
    let userPasswordValid = true;
    let errorMsg = {...this.state.errorMsg}

    // must be 6 chars
    // must contain a number
    // must contain a special character

    if (password.length < 6) {
      userPasswordValid = false;
      errorMsg.password = 'Password must be at least 6 characters long';
    } else if (!/\d/.test(password)){
      userPasswordValid = false;
      errorMsg.password = 'Password must contain a digit';
    } else if (!/[!@#$%^&*]/.test(password)){
      userPasswordValid = false;
      errorMsg.password = 'Password must contain special character: !@#$%^&*';
    }

    this.setState({userPasswordValid, errorMsg}, this.validateForm);
  }
  updatePhoneNo = (phone) => {
    this.setState({phone}, this.validatePhoneNo)
  }
  validatePhoneNo = () => {
    const {phone} = this.state;
    let phoneValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (phone.length == 0 || phone=='') {
      phoneValid = false;
      errorMsg.phone = 'Required'
    }
     else if (phone.length <8) {
      phoneValid = false;
      errorMsg.phone = 'Phone number must be 8 characters'
    }
    else if (!/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/.test(phone)){
      phoneValid = false;
      errorMsg.phone = 'Invalid phone format'
    }
    this.setState({phoneValid, errorMsg}, this.validateForm)
  }
  getServiceCenterUsers()
  {
    this.tempUsers=[]
    this.tempRoles=[]
    this.tempPrev=[]
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenterEmp/getList', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.totalUsers=responseJson.employees.length
        for(var i=0;i<responseJson.employees.length;i++)
        {
          responseJson.employees[i].employee.user['isChecked']=false
          var createdOn= new Date(responseJson.employees[i].employee.user.createdTime)
          if(responseJson.employees[i].employee.user.lastLoginTime!=null)
          {
            var lastLogin= new Date(responseJson.employees[i].employee.user.lastLoginTime)
            var sHours = lastLogin.getHours();
            var sMinutes = lastLogin.getMinutes();
            var ampm = sHours >= 12 ? 'PM' : 'AM';
            sHours = sHours % 12;
            sHours = sHours ? sHours : 12;
            sHours = sHours < 10 ?'0' + sHours : sHours;
            sMinutes = sMinutes < 10 ? '0' + sMinutes : sMinutes;
            var strTime = sHours + ':' + sMinutes + ' ' + ampm;
            var sDate = lastLogin.getDate();
            var sMonth = lastLogin.getMonth()+1;
            var sYear = lastLogin.getFullYear();
            var lastDate= sDate+'.'+sMonth+'.'+sYear
            responseJson.employees[i]['lastTime']=strTime
            responseJson.employees[i]['lastDate']=lastDate

          }
          else
          {
            responseJson.employees[i]['lastTime']=null
          }
         if(responseJson.employees[i].employee.user.createdTime!=null)
         {
            var cDate = createdOn.getDate();
            var cMonth = createdOn.getMonth()+1;
            var cYear = createdOn.getFullYear();
            cDate = cDate < 10 ? '0' + cDate : cDate;
            cMonth = cMonth < 10 && cMonth > 0 ? '0' + cMonth : cMonth;
            var createdDate= cDate+'.'+cMonth+'.'+cYear

            responseJson.employees[i]['createdDate']=createdDate
         }
         else
         {
          responseJson.employees[i]['createdDate']=null
         }



         
          if(responseJson.totalPrivilegeCount==responseJson.employees[i].privileges.length)
          {
            responseJson.employees[i]['totalPrevCount']='All'
          }
          else{
            responseJson.employees[i]['totalPrevCount']=responseJson.employees[i].privileges.length
          }
          if(responseJson.totalBranchCount==responseJson.employees[i].branchList.length)
          {
            responseJson.employees[i]['totalBraCount']='All'
          }
          else{
            responseJson.employees[i]['totalBraCount']=responseJson.employees[i].branchList.length
          }
          responseJson.employees[i]['isShowMenu']=false
          this.tempUsers.push(responseJson.employees[i])
          this.tempRoles.push(responseJson.employees[i].employee.user.roleName)
          for(var j=0;j<responseJson.employees[i].privileges.length;j++)
          {
            this.tempPrev.push(responseJson.employees[i].privileges[j])
          }

        }
        var tempArray=[...new Map(this.tempPrev.map(o => [o.privilegeId, o])).values()]
        this.tempPrev=tempArray
        this.setState({
          allUsers:responseJson.employees,
          userPrev: this.tempPrev,
          userRoles:this.tempRoles
        })
      }
    })
  }
  getEmpUsers()
  {
    this.tempUsers=[]
    this.tempRoles=[]
    this.tempPrev=[]
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/taboorEmployee/getListing', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.totalUsers=responseJson.employees.length
        for(var i=0;i<responseJson.employees.length;i++)
        {
          var createdOn= new Date(responseJson.employees[i].employee.user.createdTime)
          if(responseJson.employees[i].employee.user.lastLoginTime!=null)
          {
            var lastLogin= new Date(responseJson.employees[i].employee.user.lastLoginTime)
            var sHours = lastLogin.getHours();
            var sMinutes = lastLogin.getMinutes();
            var ampm = sHours >= 12 ? 'PM' : 'AM';
            sHours = sHours % 12;
            sHours = sHours ? sHours : 12;
            sHours = sHours < 10 ?'0' + sHours : sHours;
            sMinutes = sMinutes < 10 ? '0' + sMinutes : sMinutes;
            var strTime = sHours + ':' + sMinutes + ' ' + ampm;
            var sDate = lastLogin.getDate();
            var sMonth = lastLogin.getMonth()+1;
            var sYear = lastLogin.getFullYear();
            var lastDate= sDate+'.'+sMonth+'.'+sYear
            responseJson.employees[i]['lastTime']=strTime
            responseJson.employees[i]['lastDate']=lastDate

          }
          else
          {
            responseJson.employees[i]['lastTime']=null
          }
         if(responseJson.employees[i].employee.user.createdTime!=null)
         {
            var cDate = createdOn.getDate();
            var cMonth = createdOn.getMonth()+1;
            var cYear = createdOn.getFullYear();
            var createdDate= cDate+'.'+cMonth+'.'+cYear

            responseJson.employees[i]['createdDate']=createdDate
         }
         else
         {
          responseJson.employees[i]['createdDate']=null
         }



         
          if(responseJson.totalPrivilegeCount==responseJson.employees[i].privileges.length)
          {
            responseJson.employees[i]['totalPrevCount']='All'
          }
          else{
            responseJson.employees[i]['totalPrevCount']=responseJson.employees[i].privileges.length
          }
          // if(responseJson.totalBranchCount==responseJson.employees[i].branchList.length)
          // {
          //   responseJson.employees[i]['totalBraCount']='All'
          // }
          // else{
          //   responseJson.employees[i]['totalBraCount']=responseJson.employees[i].branchList.length
          // }
          responseJson.employees[i]['isShowMenu']=false
          this.tempUsers.push(responseJson.employees[i])
          this.tempRoles.push(responseJson.employees[i].employee.user.roleName)
          for(var j=0;j<responseJson.employees[i].privileges.length;j++)
          {
            this.tempPrev.push(responseJson.employees[i].privileges[j])
          }

        }
        var tempArray=[...new Map(this.tempPrev.map(o => [o.privilegeId, o])).values()]
        this.tempPrev=tempArray
        this.setState({
          allUsers:responseJson.employees,
          userPrev: this.tempPrev,
          userRoles:this.tempRoles
        })
      }
    })
  }
  showMenu(id)
  {
      for(var i=0;i<this.state.allUsers.length;i++)
      {
        if(this.state.allUsers[i].employee.user.userId==id)
        {
          this.state.allUsers[i]['isShowMenu']=true
          this.setState({
            isShowDropDownMenu:true
          })
        }
        else
        {
          this.state.allUsers[i]['isShowMenu']=false
        }
      }
      var data=this.state.allUsers
      this.setState({
        allUsers:data
      })
  }
  searchUser(userNo)
  {
    
    if(userNo!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempUsers.length;i++)
      {
        if(this.tempUsers[i].employee.employeeNumber.toLowerCase().includes(userNo.toLowerCase()) || 
        this.tempUsers[i].employee.user.name.toLowerCase().includes(userNo.toLowerCase()))
        {
          data.push(this.tempUsers[i])
          this.setState({
            allUsers:data
          })
          isMatch=true
          this.totalUsers=data.length
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
        }
      }
      if(isMatch==false)
      {
        this.totalUsers=0
        this.setState({allUsers:[]})
      }
    }
    else
    {
      this.setState({
        allUsers:this.tempUsers
      })
    }
  
  }
  changeUserSearch(userNo)
  {
    if(userNo=='')
    { 
      this.totalUsers=this.tempUsers.length
      this.setState({
        allUsers:this.tempUsers
      })
    }
    this.searchUser(userNo)
    this.setState({
    searchUserNo:userNo
    })
  }
  toggle(i) {
    const newArray = this.state.dropdownOpen.map((element, index) => {
      return (index === i ? !element : false);
    });
    this.setState({
      dropdownOpen: newArray,
    });
  }
  toggleLarge() {
    formData=new FormData()
    this.tempAvialableBranch=[]
    this.setState({
      isShowUserModel: true,
      phoneCode:this.countries[0].code
    });
    this.getAllRoles()
    this.getAllBranches()
    if(this.state.roleType=='Admin')
    {
     
      this.getServiceCenterEmpNumber()
    }
    else{
      this.getTaboorEmpNumber()
    }
   
  }
  updateUserModel(user)
  {
    if(this.state.roleType=='Admin')
    {
      this.tempAvialableBranch=[]
      for(var i=0;i<user.branchList.length;i++)
      {
        this.tempAvialableBranch.push(user.branchList[i])
      }
      var tempPhoneCode=user.employee.user.phoneNumber.split('-')
      this.updateUserName(user.employee.user.name)
      this.updateEmail(user.employee.user.email)
      this.updatePhoneNo(tempPhoneCode[1])
      this.roleId=user.roleId
      this.setState({
        phoneCode:tempPhoneCode[0],
        selectedRoleId:user.roleId,
        isShowUpdateUserModel: true,
        empNumber:user.employee.employeeNumber,
        selectedBranchObject:this.tempAvialableBranch,
        selectedBranchId:user.branchList[user.branchList.length-1].branchId,
        empUserId:user.employee.user.userId
        
      })
    }
    else
    {
      var tempPhoneCode=user.employee.user.phoneNumber.split('-')
      this.updateUserName(user.employee.user.name)
      this.updateEmail(user.employee.user.email)
      this.updatePhoneNo(tempPhoneCode[1])
      this.roleId=user.roleId
      this.setState({
        phoneCode:tempPhoneCode[0],
        selectedRoleId:user.roleId,
        isShowUpdateUserModel: true,
        empNumber:user.employee.employeeNumber,
        empUserId:user.employee.user.userId
        
      })
    }
    
    formData=new FormData()
    this.getAllRoles()
    this.getAllBranches()
  }
  deleteUserModel(user) {
    var id=user.employee.user.userId;
    let token = localStorage.getItem('sessionToken');
    confirmAlert({
      title: '',
      message: 'Do you want to delete user?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {

            if(this.state.roleType=='Super Admin' && user.roleId=='48'){
              toaster.notify('You dont have the right to delete this user', {
                duration:5000 // This notification will not automatically close
              });
              this.setState({
                formValid:true,progress:100
              })
            }

            else{
            this.setState({ progress:50 })
            fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenterEmp/delete?userId='+id, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
              },
            }).then(response => response.json()).then(responseJson => {
              console.log('====================================');
              console.log(responseJson);
              
              console.log(this.state.roleType);
              console.log('====================================');
              if(responseJson.applicationStatusCode!=-1){
                this.setState({
                  formValid:true,progress:100
                })
                if(this.state.roleType=='SuperAdmin'){
                  this.getEmpUsers()
                }
                else{
                  this.getServiceCenterUsers();
                }
              }
              else{
                toaster.notify(responseJson.applicationStatusResponse, {
                  duration:5000 // This notification will not automatically close
                });
              }
              
            }).catch((err)=>{
              console.log('====================================');
              console.log(err);
              console.log('====================================');
            })
          }
          }
        },
        {
          label: 'No',
          onClick: () => { console.log('no selected'); }
        }
      ]
    });
  }
  resetUpdateModel()
  {
    formData=new FormData()
    this.setState({
      userName:'',email:'',phone:'',selectedRoleId:0,selectedBranchObject:[],allBranhces:[],allRoles:[],formValid:false
    })
    this.getAllBranches()
    this.getAllRoles()
  }
  cancelUpdateModel()
  {
    formData=new FormData()
    this.setState({
      userName:'',email:'',password:'',phone:'',selectedRoleId:0,selectedBranchObject:[],
      isShowUpdateUserModel:false,allBranhces:[],allRoles:[],formValid:false
    })
    this.getAllBranches()
    this.getAllRoles()
  }
  updateUser()
  {
    var branchIds=[]
    var fullPhoneNo=this.state.phoneCode +'-'+ this.state.phone
    var tempRoleId=-1
    
    if(this.state.selectedRoleId==0)
    {
      toaster.notify('Kindly select a role', {
        duration:5000 // This notification will not automatically close
      });
      return;
    }
    if(this.roleId!=this.state.selectedRoleId)
    {
      tempRoleId=this.state.selectedRoleId

    }
    if(this.state.roleType=='Admin')
    {
      if(this.state.selectedBranchObject.length==0)
      {
        toaster.notify('Kindly select a branch', {
          duration:5000 // This notification will not automatically close
        });
        return;
      }
      for(var i=0;i<this.state.selectedBranchObject.length;i++)
      {
        branchIds.push(this.state.selectedBranchObject[i].branchId)
      }
     this.setState({
       formValid:false,progress:50
     })
      let token = localStorage.getItem('sessionToken')
        fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenterEmp/update', {
          method: 'POST',
          body:JSON.stringify({
            "branchIds":branchIds,
            "name": this.state.userName,
            "phoneNumber": fullPhoneNo,
            "roleId": tempRoleId,
            "email":this.state.email,
            "userId":this.state.empUserId
          }),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          },
        })
        .then(response => response.json())
        .then(responseJson => {
          if(responseJson.applicationStatusCode==0)
          {
            formData=new FormData()
            this.setState({
              formValid:true,progress:100
            })
            global.isNotificationUpdated.next(true)
            this.getServiceCenterUsers()
            this.cancelUpdateModel()
            
          }
          else{
            console.log('dsadsa');
            this.cancelUpdateModel()
            toaster.notify(responseJson.applicationStatusResponse, {
              duration:5000 // This notification will not automatically close
            });
            this.setState({
              formValid:true,progress:100
            })
            formData=new FormData()
          }
        
        })
    }
    else{
      this.setState({
        formValid:false,progress:50
      })
      let token = localStorage.getItem('sessionToken')
      fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/taboorEmployee/update', {
        method: 'POST',
        body:JSON.stringify({
          "branchIds":branchIds,
          "name": this.state.userName,
          "phoneNumber": fullPhoneNo,
          "roleId": tempRoleId,
          "userId":this.state.empUserId,
          "email":this.state.email
        }),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
      })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.applicationStatusCode==0)
        {
          formData=new FormData()
          this.setState({
            formValid:true,progress:100
          })
          global.isNotificationUpdated.next(true)
          this.getEmpUsers()
          this.cancelUpdateModel()
          
        }
        else{
          formData=new FormData()
          this.setState({
            formValid:true,progress:100
          })
          toaster.notify(responseJson.applicationStatusResponse, {
            // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
            duration:5000 // This notification will not automatically close
          });
        }
      
      })
    }
  }
  getServiceCenterEmpNumber()
  {
    
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenterEmp/getNextEmpNumber', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
       
        this.setState({
          empNumber:responseJson.values.NextEmpNumber
        })
      }
    })
  }
  getAllRoles()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/userRole/get/all', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
       
        this.setState({
          allRoles:responseJson.roleList
        })
      }
    })
  }
  getTaboorEmpNumber()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/taboorEmployee/getNextEmpNumber', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
       
        this.setState({
          empNumber:responseJson.values.NextEmpNumber
        })
      }
    })
  }
  changeRole=e=>
  {
    if(parseInt(e.target.value)!=0)
    {
      this.setState({selectedRoleId:parseInt(e.target.value)})
    }
   
  }
  getAllBranches()
  {
    this.tempBranches=[]
    // this.tempAvialableBranch=[]
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/getList', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
       for(var i=0;i<responseJson.branchList.length;i++)
       {
         this.tempBranches.push(responseJson.branchList[i])
       }
        this.setState({
          allBranhces:responseJson.branchList
        })
      }
    })
  }
  changeBranch=e=>
  {
    if(parseInt(e.target.value)!=0)
    {
      this.setState({selectedBranchId:parseInt(e.target.value)})
      for(var i=0;i<this.state.allBranhces.length;i++)
      {
        if(this.state.allBranhces[i].branchId==parseInt(e.target.value))
        {
          this.tempAvialableBranch.push(this.state.allBranhces[i])  
        }
      }
      var tempArray=[...new Map(this.tempAvialableBranch.map(o => [o.branchId, o])).values()]
      this.tempAvialableBranch=tempArray
      this.setState({selectedBranchObject:this.tempAvialableBranch})
    }
   
  }
  onDismissAvialableBranch(id) {
    for(var i=0;i<this.tempAvialableBranch.length;i++)
    {
      if(id==this.tempAvialableBranch[i].branchId)
      {
        this.tempAvialableBranch.splice(i,1)
      }
    }
    this.setState({ selectedBranchObject: this.tempAvialableBranch,allBranhces:[],selectedBranchId:0 });
    this.getAllBranches()
  }
  changePhoneNo=e=>
  {
    this.setState({phoneCode:parseInt(e.target.value)})
  }
  addUser()
  {
    var branchIds=[]
    var fullPhoneNo=this.state.phoneCode +'-'+ this.state.phone
    if(this.state.selectedRoleId==0)
    {
      toaster.notify('Kindly select a role', {
        duration:5000 // This notification will not automatically close
      });
      return;
    }
    if(this.state.roleType=='Admin')
    {
      if(this.state.selectedBranchObject.length==0)
      {
        toaster.notify('Kindly select a branch', {
          duration:5000 // This notification will not automatically close
        });
        return;
      }
      for(var i=0;i<this.state.selectedBranchObject.length;i++)
      {
        branchIds.push(this.state.selectedBranchObject[i].branchId)
      }
      this.setState({
        formValid:false,progress:50
      })
      formData.append('RegisterServiceCenterEmpPayload',JSON.stringify({
        "branchIds":branchIds,
        "email": this.state.email,
        "name": this.state.userName,
        "password": this.state.password,
        "phoneNumber": fullPhoneNo,
        "roleId": this.state.selectedRoleId
      
      }))
      let token = localStorage.getItem('sessionToken')
        fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenterEmp/register', {
          method: 'POST',
          body:formData,
          headers: {
            'Authorization': `Bearer ${token}`
          },
        })
        .then(response => response.json())
        .then(responseJson => {
          if(responseJson.applicationStatusCode==0)
          {
            formData=new FormData()
            this.setState({
              formValid:true,progress:100
            })
            global.isNotificationUpdated.next(true)
            this.getServiceCenterUsers()
            this.cancel()
            
          }
          else{
            formData=new FormData()
            this.setState({
              formValid:true,progress:100
            })
            toaster.notify(responseJson.devMessage, {
              // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
              duration:5000 // This notification will not automatically close
            });
          }
        
        })
    }
    else{
      this.setState({
        formValid:false,progress:50
      })
      formData.append('RegisterTaboorEmpPayload',JSON.stringify({
        "email": this.state.email,
        "name": this.state.userName,
        "password": this.state.password,
        "phoneNumber": fullPhoneNo,
        "roleId": this.state.selectedRoleId
      
      }))
      let token = localStorage.getItem('sessionToken')
      fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/taboorEmployee/register', {
        method: 'POST',
        body:formData,
        headers: {
          'Authorization': `Bearer ${token}`
        },
      })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.applicationStatusCode==0)
        {
          this.setState({
            formValid:true,progress:100
          })
          formData=new FormData()
          global.isNotificationUpdated.next(true)
          this.getEmpUsers()
          this.cancel()
          
        }
        else{
          this.setState({
            formValid:true,progress:100
          })
          formData=new FormData()
          toaster.notify(responseJson.devMessage, {
            // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
            duration:5000 // This notification will not automatically close
          });
        }
      
      })
    }
     

    
  }
  reset()
  {
    formData=new FormData()
    this.setState({
      userName:'',email:'',phone:'',selectedRoleId:0,selectedBranchObject:[],allBranhces:[],allRoles:[],formValid:false
    })
    this.getAllBranches()
    this.getAllRoles()
  }
  cancel()
  {
    formData=new FormData()
    this.setState({
      userName:'',email:'',password:'',phone:'',selectedRoleId:0,selectedBranchObject:[],isShowUserModel:false,allBranhces:[],allRoles:[],formValid:false
    })
    this.getAllBranches()
    this.getAllRoles()
  }
  sortBy(data)
  {
    this.sortById(data)
    this.setState({ isSortBy:data })
  }
  sortById(data)
  {
    if(this.tempUsers.length!=0)
    {
      let sortedProductsAsc;
      if(data=='ascending')
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>a.employee.employeeNumber.localeCompare(b.employee.employeeNumber));
      }
      else
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>b.employee.employeeNumber.localeCompare(a.employee.employeeNumber))
      }
      
    console.log(sortedProductsAsc)
    this.setState({
        allUsers:sortedProductsAsc
    })
    }
    
  }
  sortByName()
  {
    if(this.tempUsers.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>a.employee.user.name.localeCompare(b.employee.user.name))
      }
      else
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>b.employee.user.name.localeCompare(a.employee.user.name))
      }
      console.log(sortedProductsAsc)
      this.setState({
        allUsers:sortedProductsAsc
      })
    }
    
  }
  sortByRole()
  {
    if(this.tempUsers.length!=0)
    { 
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>a.employee.user.roleName.localeCompare(b.employee.user.roleName))
      }
      else
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>b.employee.user.roleName.localeCompare(a.employee.user.roleName))
      }
      console.log(sortedProductsAsc)
      this.setState({
        allUsers:sortedProductsAsc
      })
    }
    
  }
  sortByPrev()
  {
    if(this.tempUsers.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>{
        return Number(a.privileges.length)  - Number(b.privileges.length);
      })
      }
      else
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>{
        return Number(a.privileges.length)  - Number(b.privileges.length);
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    console.log(sortedProductsAsc)
    this.setState({
      allUsers:sortedProductsAsc
    })
    }
  }
  sortByBranches()
  {
    if(this.tempUsers.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>{
        return Number(a.branchList.length)  - Number(b.branchList.length);
      })
      }
      else
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>{
        return Number(a.branchList.length)  - Number(b.branchList.length);
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    console.log(sortedProductsAsc)
    this.setState({
      allUsers:sortedProductsAsc
    })
    }
  }
  sortByDates()
  {
    if(this.tempUsers.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>{
          return new Date(a.employee.user.createdTime)  - new Date(b.employee.user.createdTime);
        })
        // sortedProductsAsc= this.tempUsers.sort((a,b)=>a.createdDate.localeCompare(b.createdDate))
      }
      else
      {
        sortedProductsAsc= this.tempUsers.sort((a,b)=>{
          return new Date(a.employee.user.createdTime)  - new Date(b.employee.user.createdTime);
        })
        // sortedProductsAsc= this.tempUsers.sort((a,b)=>a.createdDate.localeCompare(b.createdDate))
         sortedProductsAsc=sortedProductsAsc.reverse()
      }
     
    console.log(sortedProductsAsc)
    this.setState({
      allUsers:sortedProductsAsc
    })
    }
  }
  selectAll()
  {
    var temp=this.state.isAll
    if(this.tempUsers.length!=0)
    {
      for(var i=0;i<this.tempUsers.length;i++)
      {
        if(temp==false)
        {
          this.tempUsers[i].employee.user['isChecked']=true
          this.count=this.tempUsers.length
        }
        else
        {
          this.count=0
          this.tempUsers[i].employee.user['isChecked']=false
        }
        
      }
    }
    this.setState({
      isAll:!temp,
      allUsers:this.tempUsers,
    })
  }
  select(data)
  {
    for(var i=0;i<this.tempUsers.length;i++)
    {
      if(data.userId==this.tempUsers[i].employee.user.userId)
      {
        if(this.tempUsers[i].employee.user['isChecked']==true)
        {
          this.tempUsers[i].employee.user['isChecked']=false
          this.setState({
            isAll:false
          })
          this.count=this.count-1
          break;
        }
        else
        {         
          this.count=this.count+1
          this.tempUsers[i].employee.user['isChecked']=true
        }
  
      }
    }
    if(this.count==this.tempUsers.length)
    {
      this.setState({
        isAll:true
      })
    }
    this.setState({allUsers:this.tempUsers})
  }
  changefilterId(data)
  {
    this.setState({
      filterId:data
    })
  }
  changeBranchFilter(data)
  {
    this.setState({
      filterBranch:data
    })
  }
  changeNameFilter(data)
  {
    this.setState({
      filterName:data
    })
  }
  changeRoleFilter(data)
  {
    this.setState({
      filterRole:data
    })
  }
  changePrevFilter(data)
  {
    this.setState({
      filterPrev:data
    })
  }
  changeStartDate(date)
  {
    this.setState({sDate:date.value})
    }
  applyFilter()
  {
    var filterData=JSON.parse(JSON.stringify(this.tempUsers))
    if(this.state.filterId!='')
    {
      var val=this.state.filterId
      filterData =  filterData.filter(function(status) {
        return status.employee.employeeNumber.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterBranch!='')
    {
      var val=this.state.filterBranch
      filterData =  filterData.filter(function(status) {
        return status.branchList.length == parseInt(val);
      });
    }
    if(this.state.filterName!='')
    {
      var val=this.state.filterName
      filterData =  filterData.filter(function(status) {
        return status.employee.user.name.toLowerCase()== val.toLowerCase();
      });
    }
    if(this.state.filterRole!='')
    {
      var val=this.state.filterRole
      filterData =  filterData.filter(function(status) {
        return status.employee.user.roleName.toLowerCase() ==val.toLowerCase();
      });
    }
    if(this.state.filterBranch!='')
    {
      var val=this.state.filterBranch
      filterData =  filterData.filter(function(status) {
        return status.branchList.length == parseInt(val);
      });
    }
    if(this.state.sDate!=null)
    {
      let d = this.state.sDate.getDate();
      let m = this.state.sDate.getMonth()+1;
      let y=this.state.sDate.getFullYear()
      d = d < 10 ? '0' + d : d;
      m = m < 10 && m > 0 ? '0' + m : m;
      var val=d+"."+m+"."+y 
      filterData =  filterData.filter(function(status) {
        return status.createdDate == val;
      });
    }
    this.totalUsers=filterData.length
    this.setState({
      allUsers:filterData
    })
    console.log(filterData)
   
  }
  resetFilter()
  {
    this.totalUsers=this.tempUsers.length
    this.setState({
      filterId:'',filterName:'',filterRole:'',filterBranch:'',filterPrev:'',sDate:null,allUsers:this.tempUsers
    })
  }
  render() {
    return (
      <div className="animated fadeIn">
         <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress:0
          })}
        />
        <DefaultHeader/>
        <Row className="mb-3 hidden">
          
          <Col sm="4">
            <h3>
            <ButtonDropdown id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                &nbsp;
                </DropdownToggle>
                <DropdownMenu left>
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              <div className="checkbox form-check d-inline-block pl-4">
                  <input  type="checkbox" className="form-check-input" onChange={()=>this.selectAll()} checked={this.state.isAll}></input>
                  <label  className="form-check-label form-check-label">{this.totalUsers}&nbsp;Users</label>
              </div>
              </h3>
          </Col>

          {this.state.isShowUserPrev==true &&  <Col sm="8" className="text-right">
          
            <div className="float-right">
            <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button>
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                onClick={() => {this.setState({ isShowSortByMenu:false })}}
                >
                <button className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></button>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={()=>this.sortBy('ascending')}><input type="radio" checked={this.state.isSortBy=='ascending'} name="sort-order" onChange={()=>{}}></input> Ascending</DropdownItem>
                  <DropdownItem onClick={()=>this.sortBy('descending')}><input type="radio" checked={this.state.isSortBy=='descending'} name="sort-order" onChange={()=>{}}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            {this.state.isShowEditUserPrev==true && <button className="btn btn-outline-warning ml-2" onClick={()=>this.toggleLarge()}><i className="icon2-addition-sign"></i></button>}
            </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                 value={this.state.searchUserNo} onChange={(e) => this.changeUserSearch(e.target.value)}
                 onKeyPress={event => {
                   if (event.key === 'Enter') {
                     this.searchUser()
                   }
                 }}
                >

                  
                </input>
              <div className="input-group-append" ><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>}
        </Row>
        {this.state.isShowFilters==true && <Card className="card-line" id="filter-row">
          <CardBody>
            <Row>
              
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="ID" 
                       value={this.state.filterId} 
                       onChange={(e) => this.changefilterId(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Name" 
                       value={this.state.filterName} 
                       onChange={(e) => this.changeNameFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line mb-0">
                        <Input type="text" placeholder="Role" 
                         value={this.state.filterRole} 
                         onChange={(e) => this.changeRoleFilter(e.target.value)}
                        />
                      </FormGroup>
                    </Col>
                   
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="Privileges"   min="0"
                      value={this.state.filterPrev} 
                      onChange={(e) => this.changePrevFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    {this.state.roleType=='Admin'&& <Col className="text-center">
                      <FormGroup className="input-line mb-0">
                        <Input type="number" placeholder="Branches"  min="0"
                         value={this.state.filterBranch} 
                         onChange={(e) => this.changeBranchFilter(e.target.value)}
                         />
                      </FormGroup>
                    </Col>}
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"> 
                      <DatePickerComponent  value={this.state.sDate} placeholder='Created On' 
                        onChange={(date) => this.changeStartDate(date)}/>
                        </FormGroup>
                    </Col>
                   
                   
                    <Col sm="auto">
                    <Button color="light" className="btn-sm" onClick={()=>this.resetFilter()}>Reset</Button>
                      <Button color="primary" className="btn-sm ml-2" onClick={()=>this.applyFilter()}>Apply</Button>
                    </Col>
                    
                    </Row>
                  </CardBody>
                  </Card>}


        {this.state.isShowUserPrev==true &&   <Card className="mb-0">
              
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                    <th width="95" style={{minWidth:'91px'}}></th>
                    {global.isShowArabicLanguage==false &&  <th className="text-center" onClick={()=>this.sortById()}>ID</th>}
                    {global.isShowArabicLanguage==true &&  <th className="text-center" onClick={()=>this.sortById()}>رقم التعريف</th>}

                    <th className="text-left" onClick={()=>this.sortByName()}>Name</th>
                    {global.isShowArabicLanguage==false &&  <th onClick={()=>this.sortByRole()}>
                    Role
                    </th>}
                    {global.isShowArabicLanguage==true &&  <th onClick={()=>this.sortByRole()}>
                    دور
                    </th>}
                    {global.isShowArabicLanguage==false &&  <th onClick={()=>this.sortByPrev()}> 
                      Privileges
                    </th>}
                    {global.isShowArabicLanguage==true &&  <th onClick={()=>this.sortByPrev()}> 
                      الصلاحيات
                    </th>}
                    {this.state.roleType=='Admin'&&<th onClick={()=>this.sortByBranches()}>Branches</th>}
                    <th>Last Login</th>
                    <th onClick={()=>this.sortByDates()} >Created On</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                 
                  {this.state.allUsers.map(user => (
                    <tr>
                      <td className="text-center">
                        <div className="checkbox form-check">
                            <input  type="checkbox" className="form-check-input" onChange={()=>this.select(user.employee.user)} checked={user.employee.user.isChecked}></input>
                            <label  className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">{user.employee.employeeNumber}</td>
                      <td>{user.employee.user.name}</td>
                      <td className="text-center">
                        <span className="badge badge-success">{user.employee.user.roleName}</span>
                      </td>
                      <td className="text-center">{user.totalPrevCount}</td>
                      {this.state.roleType=='Admin'&& <td className="text-center">{user.totalBraCount}</td>}
                      {user.lastTime!=null && <td className="text-center">{user.lastTime}<p className="text-light-grey">{user.lastDate}</p></td>}
                      {user.lastTime==null && <td className="text-center">Nil<br></br></td>}

                      {user.createdDate!=null && <td className="text-center">{user.createdDate}</td>}
                      {user.createdDate==null && <td className="text-center">Nil</td>}

                      {this.state.isShowEditUserPrev==true && <td className="text-center">
                      <Link className="ml-4" onClick={()=>this.showMenu(user.employee.user.userId)}><i className="f20 icon2-menu-options text-light-grey"></i></Link>
                      {user.isShowMenu==true && 
                        <Dropdown isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu:false }); }} >
                          <DropdownToggle tag="span" onClick={() => {this.setState({ isShowDropDownMenu:false })}} data-toggle="dropdown" >
                          </DropdownToggle>
                          <DropdownMenu>
                            <DropdownItem onClick={()=>this.updateUserModel(user)}>Update</DropdownItem>
                            <DropdownItem onClick={()=>this.deleteUserModel(user)}>Delete</DropdownItem>
                          </DropdownMenu>
                        </Dropdown>
                      }
                      </td>
                      }
                    </tr>
                  ))}
                   </tbody>
                </Table>
               
              </CardBody>
            </Card>}
          
          


            <Modal isOpen={this.state.isShowUserModel} 
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader>Add User</ModalHeader>
                  <ModalBody>


                  


                    

                  <FormGroup className="input-line">
                    <Label>Employee Number</Label>
                    <Input type="text" placeholder="Employee Number"  readOnly value={this.state.empNumber}/>
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label>Name</Label> 
                    <Input type="text" placeholder="Name" 
                     value={this.state.userName} onChange={(e) => this.updateUserName(e.target.value)} />
                     < ValidationMessage valid={this.state.userNameValid} message={this.state.errorMsg.userName} />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label>Email</Label> 
                    <Input type="email" placeholder="Email" 
                     value={this.state.email} onChange={(e) => this.updateEmail(e.target.value)} />
                     < ValidationMessage valid={this.state.userEmailValid} message={this.state.errorMsg.email} />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label>Password</Label> 
                    <Input type="password" placeholder="Password" 
                     value={this.state.password} onChange={(e) => this.updatePassword(e.target.value)} />
                     < ValidationMessage valid={this.state.userPasswordValid} message={this.state.errorMsg.password} />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label>Phone Number</Label> 
                    <Row>
                      <Col sm="3">
                        <select className="form-control" onChange={this.changePhoneNo} >
                        {this.countries.map(country => (
                          <option value={country.code}>{country.code}</option>
                        ))}
                      </select>
                      </Col>
                      <Col sm="9">
                        <Input type="text" placeholder="Phone" 
                      value={this.state.phone} onChange={(e) => this.updatePhoneNo(e.target.value)} />
                      < ValidationMessage valid={this.state.phoneValid} message={this.state.errorMsg.phone} />
                      </Col>
                    </Row>
                  </FormGroup>


                  <FormGroup className="input-line">
                    <Label>Role</Label>
                    <select className="form-control" onChange={this.changeRole}>
                    <option value='0' key='0'>Role</option>
                    {this.state.allRoles.map(role => (
                      <option value={role.role.roleId} key={role.role.roleId}>{role.role.roleName}</option>
                    ))}                      
                    </select>
                  </FormGroup>
                  {this.state.roleType=='Admin'&& <FormGroup className="input-line">
                    <Label>Branch</Label>
                    <select className="form-control" onChange={this.changeBranch}>
                      <option value='0' key='0'>Branch</option>
                    {this.state.allBranhces.map(branch => (
                      <option value={branch.branchId} key={branch.branchId}>{branch.branchName}</option>
                    ))}                      
                    </select>
                  </FormGroup>}
                  {this.state.selectedBranchObject.map(branch => (
                    <Alert color="light" className="theme-alert" isOpen={this.state.visible} toggle={()=>this.onDismissAvialableBranch(branch.branchId)}>
                      {branch.branchName}
                    </Alert>  
                  ))}


                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.cancel()}>Cancel</Button>
                    <Button outline color="primary" onClick={()=>this.reset()}>Reset</Button>
                    <Button color="primary" disabled={!this.state.formValid } onClick={()=>this.addUser()}>Add</Button>
                  </ModalFooter>
                </Modal>



                <Modal isOpen={this.state.isShowUpdateUserModel} 
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader>Update User</ModalHeader>
                  <ModalBody>
                  <FormGroup className="input-line">
                    <Label>Employee Number</Label>
                    <Input type="text" placeholder="Employee Number" readOnly value={this.state.empNumber}/>
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label>Name</Label> 
                    <Input type="text" placeholder="Name"
                     value={this.state.userName} onChange={(e) => this.updateUserName(e.target.value)}/>
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label>Email</Label> 
                    <Input type="email" placeholder="Email" value={this.state.email} onChange={(e) => this.updateEmail(e.target.value)}/>
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label>Phone Number</Label> 
                    <Row>
                      <Col sm="3">
                        <select className="form-control" onChange={this.changePhoneNo} value={this.state.phoneCode}>
                        {this.countries.map(country => (
                          <option value={country.code}>{country.code}</option>
                        ))}
                      </select>
                      </Col>
                      <Col sm="9">
                        <Input type="text" placeholder="Phone"  onChange={(e) => this.updatePhoneNo(e.target.value)} value={this.state.phone}/>
                      </Col>
                    </Row>
                  </FormGroup>


                  <FormGroup className="input-line">
                    <Label>Role</Label>
                    <select className="form-control" onChange={this.changeRole} value={this.state.selectedRoleId} >
                    <option value='0' key='0'>Role</option>
                    {this.state.allRoles.map(role => (
                      <option value={role.role.roleId} key={role.role.roleId}>{role.role.roleName}</option>
                    ))}                      
                    </select>
                  </FormGroup>
                  {this.state.roleType=='Admin'&& <FormGroup className="input-line">
                    <Label>Branch</Label>
                    <select className="form-control" onChange={this.changeBranch} value={this.state.selectedBranchId}>
                      <option value='0' key='0'>Branch</option>
                    {this.state.allBranhces.map(branch => (
                      <option value={branch.branchId} key={branch.branchId}>{branch.branchName}</option>
                    ))}                      
                    </select>
                  </FormGroup>}
                  {this.state.selectedBranchObject.map(branch => (
                    <Alert color="light" className="theme-alert" isOpen={this.state.visible} toggle={()=>this.onDismissAvialableBranch(branch.branchId)}>
                      {branch.branchName}
                    </Alert>  
                  ))}


                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.cancelUpdateModel()}>Cancel</Button>
                    {/* <Button outline color="primary" onClick={()=>this.resetUpdateModel()}>Reset</Button> */}
                    <Button color="primary" disabled={!this.state.formValid } onClick={()=>this.updateUser()}>Update</Button>
                  </ModalFooter>
                </Modal>

      
      


      </div>
    );
  }
}

export default Branch;
