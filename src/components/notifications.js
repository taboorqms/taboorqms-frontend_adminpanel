import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table,  
  DropdownItem,  DropdownMenu,  DropdownToggle,Button,ButtonDropdown, FormGroup,Label,Input,Alert,
  Modal, ModalBody, ModalFooter, ModalHeader,TabContent, TabPane,Collapse } from 'reactstrap';

import { Link, NavLink } from 'react-router-dom';
import * as router from 'react-router-dom';
import { Badge, UncontrolledDropdown, Nav, NavItem } from 'reactstrap';
import {
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler, AppSwitch } from '@coreui/react';
// import routes from '../../../routes';
import { CalendarComponent,DatePickerComponent  } from '@syncfusion/ej2-react-calendars';
import axios from "axios";
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import routes from './routes';
import {CSVLink,CSVDownload} from "react-csv";
import LoadingBar from 'react-top-loading-bar'
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
const columns = [
  {
      Header: 'Subject',
      accessor: 'Subject', // String-based value accessors!
  },
  {
      Header: 'To User/Groups',
      accessor: 'To User/Groups',

 },
 {
  Header: 'By',
  accessor: 'By',

}
,
 {
  Header: 'Date',
  accessor: 'Date',

}
,
 {
  Header: 'Time',
  accessor: 'Time',

},
{
  Header: 'Status',
  accessor: 'Status',

}]
class Branch extends Component {
  tempItems=[
    {
      'name':'New/Update User',checked:false,id:1,disabled:false,isShowChecked:false
    },
    {
      'name':'New/Update Agent',checked:false,id:2,disabled:false,isShowChecked:false
    },
    {
      'name':'New/Update Branch',checked:false,id:3,disabled:false,isShowChecked:false
    },
    {
      'name':'Update Support Ticket',checked:false,id:10,disabled:false,isShowChecked:false
    },
    {
      'name':'Customer Feedback (All Ratings)',checked:false,id:4,disabled:false,isShowChecked:false
    },
    {
      'name':'Customer Feedback 1 Star',checked:false,id:5,disabled:false,isShowChecked:false
    },
    {
      'name':'Customer Feedback 2 Stars',checked:false,id:6,disabled:false,isShowChecked:false
    },
    {
      'name':'Customer Feedback 3 Stars',checked:false,id:7,disabled:false,isShowChecked:false
    },
    {
      'name':'Customer Feedback 4 Stars',checked:false,id:8,disabled:false,isShowChecked:false
    },
    {
      'name':'Customer Feedback 5 Stars',checked:false,id:9,disabled:false,isShowChecked:false
    }
  ]
  temSuperAdmin=[
    {
      'name':'New/Update User',checked:false,id:1,disabled:false,isShowChecked:false
    },
    {
      'name':'New/Update Service Center',checked:false,id:2,disabled:false,isShowChecked:false
    },
  ]
  tempNotifications=[]
  totalUsers=0
  count=0
  allData=[]
  constructor(props) {
    super(props);
    this.state = {
      isDisableSave:false,
      isDisableReset:false,
      progress:0,
      isSortByNotification:'ascending',
      isSortByLog:'ascending',
      allNotifications:[],
      isShowNotificationPrev:false,
      items:[],
      activeTab: new Array(4).fill('1'),
      collapse: false,
      accordion: [true, false, false],
      custom: [true, false],
      status: 'Closed',
      roleType:'',
      fadeIn: true,
      timeout: 300,
      isSetting:true,
      searchUserNo:'',
      searchLogNo:'',
      isLog:false,
      modal: false,
      large: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
      isShowFilters:false,
      isAll:false,
      filterStatus:'Select Status',filterSubject:'',filterUserPerGroup:'',filterBy:'Select By',sDate:null,
    };
    this.toggleLarge = this.toggleLarge.bind(this);
  
  }
  componentDidMount()
  {
    if(localStorage.getItem('isShowArabicLanguage')=='true')
    {
      global.isShowArabicLanguage=true
    }
    else
    {
      global.isShowArabicLanguage=false
    }
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Admin_Panel_View_Only')
        {
          this.setState({isShowNotificationPrev:true})
          isPrev=true
        }
        else {
          if(!isPrev==true)
          {
            isPrev=false
          }
         
        }
      }
    }
    if(isPrev==false)
    {
      this.setState({isLog:true,isSetting:false})
    }
    else{
      
      var type=localStorage.getItem('role')
      if(type!=undefined)
      {
        if(type.startsWith('Taboor'))
        {
          this.setState({
            roleType:'Super Admin'
          })
          this.getSuperAdminUserConfiguration()
        }
        else {
          this.setState({
            roleType:'Admin'
          })
          this.getUserConfiguration()
        }
      }
      }
    
  }
  getUserConfiguration()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/setting/notificationType/get/user', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.tempItems[0].checked=responseJson.userNotificationType.newOrUpdateUser
        this.tempItems[1].checked=responseJson.userNotificationType.newOrUpdateAgent
        this.tempItems[2].checked=responseJson.userNotificationType.newOrUpdateBranch
        this.tempItems[3].checked=responseJson.userNotificationType.updateSupportTicket
        this.tempItems[5].checked=responseJson.userNotificationType.customerFeedback1Star
        this.tempItems[6].checked=responseJson.userNotificationType.customerFeedback2Star
        this.tempItems[7].checked=responseJson.userNotificationType.customerFeedback3Star
        this.tempItems[8].checked=responseJson.userNotificationType.customerFeedback4Star
        this.tempItems[9].checked=responseJson.userNotificationType.customerFeedback5Star
        if(this.tempItems[5].checked==true && this.tempItems[6].checked==true &&this.tempItems[7].checked==true &&this.tempItems[8].checked==true &&this.tempItems[9].checked==true)
        {
          this.tempItems[4].checked=true
        }
        this.totalUsers=10
        this.setState({
          items:this.tempItems
        })
      }
    })
  }
  getSuperAdminUserConfiguration()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/setting/notificationType/get/user', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.temSuperAdmin[0].checked=responseJson.userNotificationType.newOrUpdateUser
        this.temSuperAdmin[1].checked=responseJson.userNotificationType.newOrUpdateServiceCenter
    
        this.totalUsers=2
        this.setState({
          items:this.temSuperAdmin
        })
      }
    })
  }
  changeConfigration(id)
  {
    if(id==4)
    {
      if(this.tempItems[4]['checked']==true)
      {
        this.tempItems[4]['checked']=false
        this.tempItems[5]['checked']=false
        this.tempItems[6]['checked']=false
        this.tempItems[7]['checked']=false
        this.tempItems[8]['checked']=false
        this.tempItems[9]['checked']=false

        this.tempItems[0]['disabled']=false
        this.tempItems[1]['disabled']=false
        this.tempItems[2]['disabled']=false
        this.tempItems[3]['disabled']=false
        this.tempItems[4]['disabled']=false
        this.tempItems[5]['disabled']=false
        this.tempItems[6]['disabled']=false
        this.tempItems[7]['disabled']=false
        this.tempItems[8]['disabled']=false
        this.tempItems[9]['disabled']=false
                // this.tempItems[4]['checked']=false
        // this.tempItems[5]['checked']=false
        // this.tempItems[6]['checked']=false
        // this.tempItems[7]['checked']=false
        this.setState({
          disabled:false
        })
      }
      else{
        this.tempItems[4]['checked']=true
        this.tempItems[5]['checked']=true
        this.tempItems[6]['checked']=true
        this.tempItems[7]['checked']=true
        this.tempItems[8]['checked']=true
        this.tempItems[9]['checked']=true

        this.tempItems[0]['disabled']=false
        this.tempItems[1]['disabled']=false
        this.tempItems[2]['disabled']=false
        this.tempItems[3]['disabled']=false
        this.tempItems[4]['disabled']=true
        this.tempItems[5]['disabled']=true
        this.tempItems[6]['disabled']=true
        this.tempItems[7]['disabled']=true
        this.tempItems[8]['disabled']=true
        this.tempItems[9]['disabled']=true
      }
    }
    else
    {
      for(var i=0;i<this.tempItems.length;i++)
      {
        if(id==this.tempItems[i].id)
        {
          if(this.tempItems[i]['checked']==true)
          {
            this.tempItems[i]['checked']=false

          }
          else{
            this.tempItems[i]['checked']=true
          }
        }
      }
    }
    
    this.setState({
      items:this.tempItems
    })
  }
  changeSuperAdminConfigration(id)
  {
      for(var i=0;i<this.temSuperAdmin.length;i++)
      {
        if(id==this.temSuperAdmin[i].id)
        {
          if(this.temSuperAdmin[i]['checked']==true)
          {
            this.temSuperAdmin[i]['checked']=false

          }
          else{
            this.temSuperAdmin[i]['checked']=true
          }
        }
      }
    // }
    
    this.setState({
      items:this.temSuperAdmin
    })
  }
  resetConfigration()
  {
    for(var i=0;i<this.tempItems.length;i++)
    {
      this.tempItems[i]['checked']=false
      this.tempItems[i]['disabled']=false
    }
    this.setState({
      items:this.tempItems
    })
    this.updateConfigration()
  }
  resetSuperAdminConfigration()
  {
    for(var i=0;i<this.temSuperAdmin.length;i++)
    {
      this.temSuperAdmin[i]['checked']=false
      this.temSuperAdmin[i]['disabled']=false
    }
    this.setState({
      items:this.temSuperAdmin
    })
    this.updateSuperAdminConfigration()
  }
  updateConfigration()
  {
    this.setState({
      isDisableSave:true,progress:50
    })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/setting/notificationType/update', {
      method: 'POST',
      body:JSON.stringify({
        "customerFeedback1Star": this.state.items[5].checked,
        "customerFeedback2Star": this.state.items[6].checked,
        "customerFeedback3Star": this.state.items[7].checked,
        "customerFeedback4Star": this.state.items[8].checked,
        "customerFeedback5Star": this.state.items[9].checked,
        "updateSupportTicket": this.state.items[3].checked,
        "newOrUpdateAgent": this.state.items[1].checked,
        "newOrUpdateBranch": this.state.items[2].checked,
        "newOrUpdateUser": this.state.items[0].checked,
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          isDisableSave:false,progress:100
        })
        this.tempItems[3]['checked']=false
        for( var i=0;i<this.tempItems.length;i++)
        {
          this.tempItems[i]['disabled']=false
        }
        this.setState({items:this.tempItems})
        this.getUserConfiguration()
      }
    })
  }
  updateSuperAdminConfigration()
  {
    this.setState({
      isDisableSave:true,progress:50
    })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/setting/notificationType/update', {
      method: 'POST',
      body:JSON.stringify({
        "newOrUpdateServiceCenter": this.state.items[1].checked,
        "newOrUpdateUser": this.state.items[0].checked,
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          isDisableSave:false,progress:100
        })
        this.getSuperAdminUserConfiguration()
      }
    })
  }
  toggle(type) {
    if(type=='setting')
    {
      if(this.state.roleType=='Admin')
      {
        this.getUserConfiguration()
      }
      else
      {
        this.getSuperAdminUserConfiguration()
      }
      
      this.setState({isSetting:true,isLog:false})
    }
    else if(type=='log')
    {
     this.getNotifications()
      this.setState({isLog:true,isSetting:false})
    }
   
  }
  toggletAB(tabPane, tab) {
    const newArray = this.state.activeTab.slice()
    newArray[tabPane] = tab
    this.setState({
      activeTab: newArray,
    });
  }
  toggleLarge() {
    this.setState({
      large: !this.state.large,
    });
  }
  searchUser(userNo)
  {
    
    if(userNo!='')
    {
      var isMatch=false
      var data=[]
      if(this.state.roleType=='Admin')
      {
        for(var i=0;i<this.tempItems.length;i++)
        {
          if(this.tempItems[i].name.toLowerCase().includes(userNo.toLowerCase()))
          {
            data.push(this.tempItems[i])
            this.setState({
              items:data
            })
            isMatch=true
            this.totalUsers=data.length
          }
          else{
            if(isMatch!=true)
            {
              isMatch=false
            }
          }
        }
        if(isMatch==false)
        {
          this.totalUsers=0
          this.setState({items:[]})
        }
      }
      else
      {
        for(var i=0;i<this.temSuperAdmin.length;i++)
        {
          if(this.temSuperAdmin[i].name.toLowerCase().includes(userNo.toLowerCase()))
          {
            data.push(this.temSuperAdmin[i])
            this.setState({
              items:data
            })
            isMatch=true
            this.totalUsers=data.length
          }
          else{
            if(isMatch!=true)
            {
              isMatch=false
            }
          }
        }
        if(isMatch==false)
        {
          this.totalUsers=0
          this.setState({items:[]})
        }
      }

    }
    else
    {
      if(this.state.roleType=='Admin')
      {
        this.setState({
          items:this.tempItems
        })
      }
      else
      {
        this.setState({
          items:this.temSuperAdmin
        })
      }
      
    }
  
  }
  changeUserSearch(userNo)
  {
    if(userNo=='')
    { 
      if(this.state.roleType=='Admin')
      {
        this.totalUsers=this.tempItems.length
        this.setState({
          items:this.tempItems
        })
      }
      else
      {
        this.totalUsers=this.temSuperAdmin.length
        this.setState({
          items:this.temSuperAdmin
        })
      }
      
    }
    this.searchUser(userNo)
    this.setState({
    searchUserNo:userNo
    })
  }
  searchLog(userNo)
  {
    
    if(userNo!='')
    {
      var isMatch=false
      var data=[]
      
        for(var i=0;i<this.tempNotifications.length;i++)
        {
          if(this.tempNotifications[i].title.toLowerCase().includes(userNo.toLowerCase())||
          this.tempNotifications[i].user.name.toLowerCase().includes(userNo.toLowerCase()))
          {
            data.push(this.tempNotifications[i])
            this.setState({
              allNotifications:data
            })
            isMatch=true
          }
          else{
            if(isMatch!=true)
            {
              isMatch=false
            }
          }
        }
        if(isMatch==false)
        {
          this.setState({allNotifications:[]})
        }
    }
    else
    {
        this.setState({
          allNotifications:this.tempNotifications
        })
      
    }
  
  }
  changeLogSearch(userNo)
  {
    if(userNo=='')
    { 
      this.setState({
        allNotifications:this.tempNotifications
      })   
    }
    this.searchLog(userNo)
    this.setState({
      searchLogNo:userNo
    })
  }
  getNotifications()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/notification/email/user', {
      method: 'POST',
      body:JSON.stringify({
        "pageNo": 0,
        "limit": 10    
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        for(var i=0;i<responseJson.userNotificationList.length;i++)
        {
          if(responseJson.userNotificationList[i].userSentBy!=null)
          {
            responseJson.userNotificationList[i].userSentByValue='Not Scheduled'
          }
          else
          {
            responseJson.userNotificationList[i].userSentByValue='Scheduled'
          }
          if(responseJson.userNotificationList[i].createdAt!=null)
          {
            var createdDate= new Date(responseJson.userNotificationList[i].createdAt) 
            responseJson.userNotificationList[i]['createdDate']=createdDate
            var sHours = createdDate.getHours();
            var sMinutes = createdDate.getMinutes();
            var sDate = createdDate.getDate();
            var sMonth = createdDate.getMonth()+1;
            var sYear = createdDate.getFullYear();
            sDate = sDate < 10 ? '0' + sDate : sDate;
            sMonth = sMonth < 10 && sMonth > 0 ? '0' + sMonth : sMonth;
            var date= sDate+'.'+sMonth+'.'+sYear
            var ampm = sHours >= 12 ? 'PM' : 'AM';
            sHours = sHours % 12;
            sHours = sHours ? sHours : 12;
            sHours = sHours < 10 ?'0' + sHours : sHours;
            sMinutes = sMinutes < 10 ? '0' + sMinutes : sMinutes;
            var strTime = sHours + ':' + sMinutes + ' ' + ampm;
            responseJson.userNotificationList[i]['time']=strTime
            responseJson.userNotificationList[i]['date']=date
            this.allData.push({
              "Subject":responseJson.userNotificationList[i].title,
              'To User/Groups':responseJson.userNotificationList[i].user.name,
              'By':responseJson.userNotificationList[i].userSentByValue,
              'Date':responseJson.userNotificationList[i].date,
              'Time':responseJson.userNotificationList[i].time,
              'Status':responseJson.userNotificationList[i].notificationStatus
            })
           
            
            
          }
          else
          {
            responseJson.userNotificationList[i]['time']=null
          }
          this.tempNotifications.push(responseJson.userNotificationList[i])
        }
        this.setState({
          allNotifications:responseJson.userNotificationList
        })
      }
    })
  }
  sortByNotification(data)
  {
    this.sortByUser(data)
    this.setState({ isSortByNotification:data })
  }
  sortByLog(data)
  {
    this.sortBySubject(data)
    this.setState({ isSortByLog:data })
  }
  sortByUser(data)
  {
    if(this.state.roleType=='Admin')
    {
      if(this.tempItems.length!=0)
      {
        let sortedProductsAsc;
        if(data=='ascending') { 
          sortedProductsAsc= this.tempItems.sort((a,b)=>a.name.localeCompare(b.name))
        }
        else {
          sortedProductsAsc= this.tempItems.sort((a,b)=>b.name.localeCompare(a.name))
        }
        console.log(sortedProductsAsc)
        this.setState({
          allRoles:sortedProductsAsc
        })
      }
    }
    else
    {
      if(this.temSuperAdmin.length!=0)
      {
        let sortedProductsAsc;
        if(data=='ascending') {
          sortedProductsAsc= this.temSuperAdmin.sort((a,b)=>a.name.localeCompare(b.name))
        }
        else {
          sortedProductsAsc= this.temSuperAdmin.sort((a,b)=>b.name.localeCompare(a.name))
        }
        console.log(sortedProductsAsc)
        this.setState({
          allRoles:sortedProductsAsc
        })
      }
    }
  }
  sortBySubject(data)
  {
      if(this.tempNotifications.length!=0)
      {
        let sortedProductsAsc;
        if(data=='ascending')
        {
          sortedProductsAsc= this.tempNotifications.sort((a,b)=>a.title.localeCompare(b.title))
        }
        else
        {
          sortedProductsAsc= this.tempNotifications.sort((a,b)=>b.title.localeCompare(a.title))
        }
        console.log(sortedProductsAsc)
        this.setState({
          allNotifications:sortedProductsAsc
        })
      }
  }
  sortByUserGroup()
  {
      // if(this.tempNotifications.length!=0)
      // {
      //   let sortedProductsAsc;
      //   if(this.state.isSortByLog=='ascending')
      //   {
      //     sortedProductsAsc= this.tempNotifications.sort((a,b)=>a.user.name.localeCompare(b.user.name))
      //   }
      //   else
      //   {
      //     sortedProductsAsc= this.tempNotifications.sort((a,b)=>b.user.name.localeCompare(a.user.name))
      //   }
      //   console.log(sortedProductsAsc)
      //   this.setState({
      //     allNotifications:sortedProductsAsc
      //   })
      // }
  }
  sortByDates()
  {
    if(this.tempNotifications.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortByLog=='ascending')
      {
        sortedProductsAsc= this.tempNotifications.sort((a,b)=>{
        return new Date(a.createdDate)  - new Date(b.createdDate);
      })
      }
      else
      {
        sortedProductsAsc= this.tempNotifications.sort((a,b)=>{
        return new Date(a.createdDate)  - new Date(b.createdDate);
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
      }
     
    console.log(sortedProductsAsc)
    this.setState({
        allNotifications:sortedProductsAsc
    })
    }
  }
  sortByStatus()
  {
    
      if(this.tempNotifications.length!=0)
      {
        let sortedProductsAsc;
        if(this.state.isSortByLog=='ascending')
        {
          sortedProductsAsc= this.tempNotifications.sort((a,b)=>a.notificationStatus.localeCompare(b.notificationStatus))
        }
        else
        {
          sortedProductsAsc= this.tempNotifications.sort((a,b)=>b.notificationStatus.localeCompare(a.notificationStatus))
        }
        console.log(sortedProductsAsc)
        this.setState({
          allNotifications:sortedProductsAsc
        })
      }
  }
  selectAll()
  {
    var temp=this.state.isAll
    if(this.state.roleType=='Admin')
    {
      if(this.tempItems.length!=0)
      {
        for(var i=0;i<this.tempItems.length;i++)
        {
          if(temp==false)
          {
            this.tempItems[i]['isShowChecked']=true
            this.count=this.tempItems.length
          }
          else
          {
            this.count=0
            this.tempItems[i]['isShowChecked']=false
          }
          
        }
        this.setState({
          isAll:!temp,
          items:this.tempItems,
        })
      }
    }
    else{
      if(this.temSuperAdmin.length!=0)
      {
        for(var i=0;i<this.temSuperAdmin.length;i++)
        {
          if(temp==false)
          {
            this.temSuperAdmin[i]['isShowChecked']=true
            this.count=this.temSuperAdmin.length
          }
          else
          {
            this.count=0
            this.temSuperAdmin[i]['isShowChecked']=false
          }
          
        }
        this.setState({
          isAll:!temp,
          items:this.temSuperAdmin,
        })
      }
    }

    
  }
  select(data)
  {
    if(this.state.roleType=='Admin')
    {
      for(var i=0;i<this.tempItems.length;i++)
      {
        if(data.id==this.tempItems[i].id)
        {
          if(this.tempItems[i]['isShowChecked']==true)
          {
            this.tempItems[i]['isShowChecked']=false
            this.setState({
              isAll:false
            })
            this.count=this.count-1
            break;
          }
          else
          {         
            this.count=this.count+1
            this.tempItems[i]['isShowChecked']=true
          }
    
        }
      }
      if(this.count==this.tempItems.length)
      {
        this.setState({
          isAll:true
        })
      }
      this.setState({items:this.tempItems})
    }
    else
    {
      for(var i=0;i<this.temSuperAdmin.length;i++)
      {
        if(data.id==this.temSuperAdmin[i].id)
        {
          if(this.temSuperAdmin[i]['isShowChecked']==true)
          {
            this.temSuperAdmin[i]['isShowChecked']=false
            this.setState({
              isAll:false
            })
            this.count=this.count-1
            break;
          }
          else
          {         
            this.count=this.count+1
            this.temSuperAdmin[i]['isShowChecked']=true
          }
    
        }
      }
      if(this.count==this.temSuperAdmin.length)
      {
        this.setState({
          isAll:true
        })
      }
      this.setState({items:this.temSuperAdmin})
    }

  }
  changefilterStatus=e=>
  {
    this.setState({
      filterStatus:e.target.value
    })
  }
  changeByFilter=e=>
  {
    this.setState({
      filterBy:e.target.value
    })
  }
  changeSubjectFilter(data)
  {
    this.setState({
      filterSubject:data
    })
  }
  changeUserPerGroupFilter(data)
  {
    this.setState({
      filterUserPerGroup:data
    })
  }
  changeStartDate(date)
{
  this.setState({sDate:date.value})
  }
  applyFilter()
  {
    var filterData=JSON.parse(JSON.stringify(this.tempNotifications))
    if(this.state.filterStatus!='Select Status')
    {
      var val=this.state.filterStatus
      filterData =  filterData.filter(function(status) {
        return status.notificationStatus.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterSubject!='')
    {
      var val=this.state.filterSubject
      filterData =  filterData.filter(function(status) {
        return status.title.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterUserPerGroup!='')
    {
      var val=this.state.filterUserPerGroup
      filterData =  filterData.filter(function(status) {
        return status.user.name.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterBy!='Select By')
    {
      var val=this.state.filterBy
      filterData =  filterData.filter(function(status) {
        return status.userSentByValue.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.sDate!=null)
    {
      let d = this.state.sDate.getDate();
      let m = this.state.sDate.getMonth()+1;
      let y=this.state.sDate.getFullYear()
      d = d < 10 ? '0' + d : d;
      m = m < 10 && m > 0 ? '0' + m : m;
      var val=d+"."+m+"."+y 
      filterData =  filterData.filter(function(status) {
        return status.date == val;
      });
    }
    // this.tota
    this.setState({
      allNotifications:filterData
    })
    console.log(filterData)
   
  }
  resetFilter()
  {
    this.setState({
      filterStatus:'Select Status',filterSubject:'',filterUserPerGroup:'',filterBy:'Select By',sDate:null,allNotifications:this.tempNotifications
    })
  }
  render() {
    return (
      <div className="animated fadeIn">
        <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress:0
          })}
        />
       <DefaultHeader/>
        <div className="tabs-line">
      <Nav tabs>
      {this.state.isShowNotificationPrev==true && <NavItem>
                <NavLink className="nav-link" to="#1"
                  active={this.state.isSetting}
                  isActive={(match, location) => {
                    if (!this.state.isSetting) {
                      return false;
                    }
                    else{
                      return true;
                    }
                  }}
                  onClick={() => this.toggle('setting')}
                >
                  <h5 className="mb-0">{global.isShowArabicLanguage==false && <span>
                    Setting
                    </span>}
                    {global.isShowArabicLanguage==true && <span>
                      ضبط
                    </span>}</h5>
                </NavLink>
              </NavItem>}
              <NavItem>
                <NavLink className="nav-link" to="#2"
                  active={this.state.isLog}
                  isActive={(match, location) => {
                    if (!this.state.isLog) {
                      return false;
                    }
                    else{
                      return true;
                    }
                  }}
                  onClick={() => this.toggle('log')}
                >
                  <h5 className="mb-0">
                    {global.isShowArabicLanguage==false && <span>
                    Log
                    </span>}
                    {global.isShowArabicLanguage==true && <span>
                      سجل
                    </span>}
                    </h5>
                </NavLink>
              </NavItem>
    </Nav>
    <TabContent className="pt-4">
       {this.state.isSetting==true && <TabPane >
          
        <Row className="mb-3 hidden">
          
          <Col sm="3">
            <h3>
            <ButtonDropdown id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                &nbsp;
                </DropdownToggle>
                <DropdownMenu left>
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              <div className="checkbox form-check d-inline-block ml-2">
                  <input type="checkbox" className="form-check-input" onChange={()=>this.selectAll()} checked={this.state.isAll}></input>
                  {global.isShowArabicLanguage==false && <label className="form-check-label form-check-label">{this.totalUsers}&nbsp;Notifications&nbsp;Types</label>}
                  {global.isShowArabicLanguage==true && <label className="form-check-label form-check-label">{this.totalUsers}&nbsp;نوع الإشعارات</label>}

              </div>
              </h3>
          </Col>
          <Col sm="9" className="text-right">
          
            <div className="float-right">
            {/* <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button> */}
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                onClick={() => {this.setState({ isShowSortByMenu:false })}}
                >
                <button className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></button>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={()=>this.sortByNotification('ascending')}><input type="radio" checked={this.state.isSortByNotification=='ascending'} name="sort-order" onChange={()=>{}}></input> Ascending</DropdownItem>
                  <DropdownItem onClick={()=>this.sortByNotification('descending')}><input type="radio" checked={this.state.isSortByNotification=='descending'} name="sort-order" onChange={()=>{}}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            {this.state.roleType=='Admin' && <button className="btn btn-outline-warning ml-2" disabled={this.state.isDisableSave} onClick={()=>this.resetConfigration()}>Reset to Default</button>}
          {this.state.roleType=='Admin' &&  <button className="btn btn-warning ml-2" disabled={this.state.isDisableSave} onClick={()=>this.updateConfigration()}>Save</button>}
          {this.state.roleType=='Super Admin' && <button className="btn btn-outline-warning ml-2" disabled={this.state.isDisableSave} onClick={()=>this.resetSuperAdminConfigration()}>Reset to Default</button>}
          {this.state.roleType=='Super Admin' &&  <button className="btn btn-warning ml-2" disabled={this.state.isDisableSave} onClick={()=>this.updateSuperAdminConfigration()}>Save</button>}
            {/* <button className="btn btn-outline-warning ml-2" onClick={this.toggleLarge}><i className="icon2-addition-sign"></i></button> */}
            </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                value={this.state.searchBranch} onChange={(e) => this.changeUserSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.searchUser()
                  }
                }}
                >

                  
                </input>
              <div className="input-group-append" onClick={()=>this.searchUser()}><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>
        </Row>



            <Card className="mb-0">
              
              <CardBody className="p-0">

              <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                  <th width="71"></th>
                    <th className="text-left" onClick={()=>this.sortByUser()}>User</th>
                    <th className="text-right">Status</th>
                    {/* <th width="50"></th> */}
                  </tr>
                  {this.state.isShowFilters==true && <tr id="filter-row">
                    <th></th>
                    
                   
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    
                    
                    <th>
                      <Button color="primary" className="btn-sm">Apply</Button>
                    </th>
                  </tr>}
                  </thead>
                  <tbody>
                 
                  {this.state.items.map(item => (
                     <tr>
                     <td className="text-right">
                         <div className="checkbox form-check">
                             <input type="checkbox" className="form-check-input" onChange={()=>this.select(item)} 
                             checked={item.isShowChecked}></input>
                             <label className="form-check-label form-check-label"></label>
                         </div>
                       </td>
                       <td className={"text-left " + (item.checked ? '' : 'text-light-grey')}>{item.name}</td>
                       <td className="text-right">
                       {this.state.roleType=='Admin' && <AppSwitch className={'mx-1'} variant={'3d'} color={'success'} checked={item.checked} disabled={item.disabled} onChange={()=>this.changeConfigration(item.id)} />}
                       {this.state.roleType=='Super Admin' && <AppSwitch className={'mx-1'} variant={'3d'} color={'success'} checked={item.checked} disabled={item.disabled} onChange={()=>this.changeSuperAdminConfigration(item.id)} />}

                       </td>
                       {/* <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td> */}
                     </tr>
                  ))}
                   </tbody>
                </Table>
                
              </CardBody>
            </Card>
          
          

       </TabPane>}
       {this.state.isLog==true &&<TabPane >
          
        <Row className="mb-3 hidden">
          <Col sm="4">
          {global.isShowArabicLanguage==false &&  <Button color="warning"><CSVLink className="text-white" data={this.allData}
          filename={"my-file.csv"} >Export</CSVLink></Button>}
          

          {global.isShowArabicLanguage==true &&  <Button color="warning"><CSVLink data={this.allData} 
          filename={"my-file.csv"}>تصدير</CSVLink></Button>}

          </Col>

          <Col sm="8" className="text-right">
          
            <div className="float-right">
            <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button>
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                onClick={() => {this.setState({ isShowSortByMenu:false })}}
                >
                <button className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></button>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={()=>this.sortByLog('ascending')}><input type="radio" checked={this.state.isSortByLog=='ascending'} name="sort-order" onChange={()=>{}}></input> Ascending</DropdownItem>
                  <DropdownItem onClick={()=>this.sortByLog('descending')}><input type="radio" checked={this.state.isSortByLog=='descending'} name="sort-order" onChange={()=>{}}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            {/* <button className="btn btn-outline-warning ml-2" onClick={this.toggleLarge}><i className="icon2-addition-sign"></i></button> */}
            </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                value={this.state.searchBranch} onChange={(e) => this.changeLogSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.searchLog()
                  }
                }}
                >

                  
                </input>
              <div className="input-group-append" onClick={()=>this.searchLog()}><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>
        </Row>

        {this.state.isShowFilters==true && <Card className="card-line" id="filter-row">
          <CardBody>
            <Row>
              
                    
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Subject" 
                      value={this.state.filterSubject} 
                      onChange={(e) => this.changeSubjectFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Users/Groups"
                      value={this.state.filterUserPerGroup} 
                      onChange={(e) => this.changeUserPerGroupFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                      <select className="form-control" onChange={this.changeByFilter}  value={this.state.filterBy}>
                          <option value='Select By' key='Select By'>
                            Select By
                          </option>
                          <option value='Scheduled' key='Scheduled'>
                            Scheduled
                          </option>
                          <option value='Not Scheduled' key='Not Scheduled'>
                          Not Scheduled
                          </option>
                        </select>
                      </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0 h-100">
                      <DatePickerComponent  value={this.state.sDate}  placeholder='Date'
                        onChange={(date) => this.changeStartDate(date)}/>
                      
                      </FormGroup>
                    </Col>
                    {/* <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Time" /></FormGroup>
                    </Col> */}
                    <Col className="text-center">
                      <FormGroup className="input-line mb-0">
                        <select className="form-control" onChange={this.changefilterStatus} value={this.state.filterStatus}>
                          <option value='Select Status' key='Select Status'>
                           Select Status
                          </option>
                          <option value='Sent' key='Sent'>
                            Sent
                          </option>
                          <option value='Not Sent' key='Not Sent'>
                            Not Sent
                          </option>
                          <option value='Expired' key='Expired'>
                            Expired
                          </option>
                          <option value='Error' key='Error'>
                            Error
                          </option>
                        </select>
                      </FormGroup>
                    </Col>
                   
                    <Col sm="auto">
                    <Button color="light" className="btn-sm" onClick={()=>this.resetFilter()}>Reset</Button>
                      <Button color="primary" className="btn-sm ml-2" onClick={()=>this.applyFilter()}>Apply</Button>
                    </Col>
                    
                    </Row>
                  </CardBody>
                  </Card>}

            <Card className="mb-0">
              
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr >
                  {global.isShowArabicLanguage==false &&  <th className="text-left" onClick={()=>this.sortBySubject()}>Subject</th>}
                  {global.isShowArabicLanguage==true &&  <th className="text-left" onClick={()=>this.sortBySubject()}>العنوان</th>}

                    {global.isShowArabicLanguage==false &&  <th onClick={()=>this.sortByUserGroup()}>To User/Groups</th>}
                    {global.isShowArabicLanguage==true &&  <th onClick={()=>this.sortByUserGroup()}>الى المستخدم / المجموعات</th>}

                    {global.isShowArabicLanguage==false &&  <th >By </th>}
                    {global.isShowArabicLanguage==true &&  <th >من </th>}

                    <th onClick={()=>this.sortByDates()}>Date</th>
                    <th >Time</th>
                    <th onClick={()=>this.sortByStatus()}>Status</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                 
                  {this.state.allNotifications.map(noti => (
                    <tr>
                    <td>{noti.title}</td>
                    <td className="text-center">{noti.user.name}</td>
                    {noti.userSentBy==null &&<td className="text-center">Scheduled</td>}
                    {noti.userSentBy!=null &&<td className="text-center">{noti.userSentBy.name}</td>}
                    {noti.time!=null && <td className="text-center">{noti.date}</td>}
                    {noti.time==null && <td className="text-center">Nil</td>}
                    {noti.time!=null && <td className="text-center">{noti.time}</td>}
                    {noti.time==null && <td className="text-center">Nil</td>}
                    <td className="text-center">
                      <span className="badge badge-success">{noti.notificationStatus}</span>
                    </td>
                    <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                  </tr>
                  ))}
                    
                   </tbody>
                </Table>
               
              </CardBody>
            </Card>
          
          

       </TabPane>}
       </TabContent>
       </div>




            <Modal isOpen={this.state.large} toggle={this.toggleLarge}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader toggle={this.toggleLarge}>Add User</ModalHeader>
                  <ModalBody>


                  


                    

                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Employee Number</Label>
                    <Input type="text" placeholder="Employee Number" />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label> 
                    <Input type="text" placeholder="Name" />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Email</Label> 
                    <Input type="text" placeholder="Name" />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Password</Label> 
                    <Input type="text" placeholder="Name" />
                  </FormGroup>


                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Role</Label>
                    <select className="form-control">
                      <option>option 1</option>
                      <option>option 1</option>
                    </select>
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Branch</Label>
                    <select className="form-control">
                      <option>option 1</option>
                      <option>option 1</option>
                    </select>
                  </FormGroup>


                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={this.toggleLarge}>Cancel</Button>
                    <Button outline color="primary" onClick={this.toggleLarge}>Reset</Button>
                    <Button color="primary" onClick={this.toggleLarge}>Add</Button>
                  </ModalFooter>
                </Modal>

      
      


      </div>
    );
  }
}

export default Branch;
