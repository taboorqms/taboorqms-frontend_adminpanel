import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Alert, Col, Row, Table, Button, FormGroup, Label, Input, 
  ButtonGroup, ButtonDropdown, DropdownItem,  DropdownMenu,  DropdownToggle, 
  Collapse, Fade ,ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText,InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText, } from 'reactstrap';
import { Link,NavLink } from 'react-router-dom';
import {Line, Pie } from 'react-chartjs-2';
import { AppSwitch } from '@coreui/react'
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';
import Steps, { Step } from 'rc-steps';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import * as router from 'react-router-dom';
import { Badge, UncontrolledDropdown, Nav, NavItem } from 'reactstrap';
import {AppBreadcrumb2 as AppBreadcrumb,AppSidebarNav2 as AppSidebarNav,} from '@coreui/react';
import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import routes from './routes';
import axios from "axios";
import { TimePickerComponent } from '@syncfusion/ej2-react-calendars';
import LoadingBar from 'react-top-loading-bar'
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
class Breadcrumbs extends Component {
  tempMaxBranch=[]
  tempUserPerBranch=[]
  tempNoOfServices=[]
  tempInOfficeTime=[]
  tempOutOfficeTime=[]
  type=''
  constructor(props) {
    super(props);
    this.state = {
      progress:0,
      dropdownOpen: false,
      allMaxBranch:[],
      allUserPerBranch:[],
      allNoOfServices:[],
      allInOfficeTime:[],
      allOutOfficeTime:[],
      packageId:0,
      selectedMaxBranch:1,
      selectedUserPerBranch:1,
      selectedInTime:30,
      selectedOutTime:30,
      selectedNoOfServices:1,
      selectedCurrency:'AED',
      isKisok:false,
      isAdvanced:false,
      isEasy:false,
      isDedicated :false,
      isPriority:false,
      isRemote:false,
      isAllTimeSupport:false,
      selectedSupportType:'Basic',
      startTime:new Date(),
      endTime:new Date(),
      planArabicName: '',planArabicNameValid: false,
      priceMontly: '', priceMontlyValid: false,
      priceAnually: '', priceAnuallyValid: false,
      pricePerSmsValid:false,pricePerSms:'',
      pricePerUserValid:false,pricePerUser:'',
      planName: '', planNameValid: false,
      smsQuota:'',smsQuotaValid:false,
      errorMsg: {},
      formValid:false,
    };
  }
  componentDidMount()
  {
    var data=this.props.location
    if(data.type==undefined)
    {
      this.type='add'
      this.state.startTime.setHours(9)
      this.state.startTime.setMinutes(0)
      this.state.endTime.setHours(17)
      this.state.endTime.setMinutes(0)
      for(var i=0;i<100;i++)
      {
        this.tempMaxBranch.push(i+1)
        this.tempUserPerBranch.push(i+1)
      }
      for(var j=0;j<50;j++)
      {
        this.tempNoOfServices.push(j+1)
      }
      for(var x=0,j=0;x<10;x++)
      {
        j=j+30
        this.tempInOfficeTime.push(j)
      }
      for(var y=0,j=0;y<10;y++)
      {
        j=j+30
        this.tempOutOfficeTime.push(j)
      }
      this.setState({
        allMaxBranch:this.tempMaxBranch,
        allUserPerBranch:this.tempUserPerBranch,
        allNoOfServices:this.tempNoOfServices,
        allInOfficeTime:this.tempInOfficeTime,
        allOutOfficeTime:this.tempOutOfficeTime
      })
    }
    else
    {
      this.type='update'
      for(var i=0;i<100;i++)
      {
        this.tempMaxBranch.push(i+1)
        this.tempUserPerBranch.push(i+1)
      }
      for(var j=0;j<50;j++)
      {
        this.tempNoOfServices.push(j+1)
      }
       for(var x=0,j=0;x<10;x++)
      {
        j=j+30
        this.tempInOfficeTime.push(j)
      }
      for(var y=0,j=0;y<10;y++)
      {
        j=j+30
        this.tempOutOfficeTime.push(j)
      }
      var tempPlanName=data.planName
      var tempPlanArabiceName=data.planArabic
      var tempPriceMontly=data.priceMonthly
      var tempPriceAnually=data.priceAnnually
      var tempPricPerUser=data.perUserPrice
      var tempPricePerSms=data.perSMSPrice
      var tempNoOfSms=data.noOfSms
      var tempStartTime = data.startTime;  
      var a = tempStartTime.split(':'); 
      var timeIn = (+a[0]) * 60 + (+a[1]); 
      var tempEndTime = data.endTime;  
      var b = tempEndTime.split(':'); 
      var timeOut = (+b[0]) * 60 + (+b[1]);
      var tempMaxBranch=data.maxBranch
      var tempNoOfServices=data.noOfServices
      var tempNoOfUsers=data.noOfUsers
      if(tempMaxBranch==0)
      {
        tempMaxBranch=1
      }
      if(tempNoOfServices==0)
      {
        tempNoOfServices=1
      }
      if(tempNoOfUsers==0)
      {
        tempNoOfUsers=1
      }
      this.setState({
        selectedMaxBranch:tempMaxBranch,
        packageId:parseInt(data.packageId),
        selectedNoOfServices:tempNoOfServices,
        selectedUserPerBranch:tempNoOfUsers,
        selectedInTime:timeIn,
        selectedOutTime:timeOut,
        selectedCurrency:data.selectedMontlyCurrency,
        selectedSupportType:data.supportType,
        allMaxBranch:this.tempMaxBranch,
        allUserPerBranch:this.tempUserPerBranch,
        allNoOfServices:this.tempNoOfServices,
        allInOfficeTime:this.tempInOfficeTime,
        allOutOfficeTime:this.tempOutOfficeTime,
        isKisok:data.isKisok,
        isAdvanced:data.isAdvanced,
        isEasy:data.isEasy,
        isPriority:data.isPriority,
        isDedicated:data.isDedicated,
        isRemote:data.isRemote,
        isAllTimeSupport:data.isAllTimeSupport
      })
      // this.changeMaxBranchLimit(data.maxBranch)
      this.updatePlanName(tempPlanName)
      this.updateArabicPlanName(tempPlanArabiceName) 
      this.updatePriceMontly(tempPriceMontly)         
      this.updatePriceAnually(tempPriceAnually)  
      this.updatePricePerSms(tempPricePerSms) 
      this.updatePricePerUser(tempPricPerUser) 
      this.updateSmsQuota(tempNoOfSms)
    }


    
  }

  validateForm = () => {
    const {planNameValid,priceMontlyValid,planArabicNameValid,priceAnuallyValid,pricePerSmsValid,pricePerUserValid,smsQuotaValid } = this.state;
    this.setState({
      formValid: planNameValid && priceMontlyValid && planArabicNameValid && priceAnuallyValid && pricePerSmsValid && pricePerUserValid && smsQuotaValid
    })
  }
  updatePlanName = (planName) => {
    this.setState({planName}, this.validatePlanName)
  }
  validatePlanName = () => {
    const {planName} = this.state;
    let planNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (planName.length == 0 ||planName=='' ) {
      planNameValid = false;
      errorMsg.planName = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(planName))
    // {
    //   planNameValid = false;
    //   errorMsg.planName = 'Only alphabets are allowed'
    // }
    this.setState({planNameValid, errorMsg}, this.validateForm)
  }
  updateArabicPlanName = (planArabicName) => {
    this.setState({planArabicName}, this.validateArabicPlanName)
  }
  validateArabicPlanName = () => {
    const {planArabicName} = this.state;
    let planArabicNameValid = true;
    let errorMsg = {...this.state.errorMsg}
  
    if (planArabicName.length == 0 ||planArabicName=='' ) {
      planArabicNameValid = false;
      errorMsg.planArabicName = 'Required'
    }
    else if(!/^[\u0621-\u064A\040]+$/.test(planArabicName))
    {
      planArabicNameValid = false;
      errorMsg.planArabicName = 'Only arabic alphabets are allowed'
    }
    this.setState({planArabicNameValid, errorMsg}, this.validateForm)
  }
  updatePriceMontly = (priceMontly) => {
    this.setState({priceMontly}, this.validatePriceMontly)
  }
  validatePriceMontly = () => {
    const {priceMontly} = this.state;
    let priceMontlyValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (priceMontly.length == 0 || priceMontly=='') {
      priceMontlyValid = false;
      errorMsg.priceMontly = 'Required'
    }
     else if (priceMontly=='0') {
      priceMontlyValid = false;
      errorMsg.priceMontly = 'Price must be greater than 0'
    }
    else if (!/^\d+(?:\.\d+)?(?:,\d+(?:\.\d+)?)*$/.test(priceMontly)){
      priceMontlyValid = false;
      errorMsg.priceMontly = 'Invalid price format'
    }
    this.setState({priceMontlyValid, errorMsg}, this.validateForm)
  }
  updatePriceAnually = (priceAnually) => {
    this.setState({priceAnually}, this.validatePriceAnually)
  }
  validatePriceAnually = () => {
    const {priceAnually} = this.state;
    let priceAnuallyValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (priceAnually.length == 0 || priceAnually=='') {
      priceAnuallyValid = false;
      errorMsg.priceAnually = 'Required'
    }
     else if (priceAnually=='0') {
      priceAnuallyValid = false;
      errorMsg.priceAnually = 'Price must be greater than 0'
    }
    else if (!/^\d+(?:\.\d+)?(?:,\d+(?:\.\d+)?)*$/.test(priceAnually)){
      priceAnuallyValid = false;
      errorMsg.priceAnually = 'Invalid price format'
    }
    this.setState({priceAnuallyValid, errorMsg}, this.validateForm)
  }
  updatePricePerUser = (pricePerUser) => {
    this.setState({pricePerUser}, this.validatePricePerUser)
  }
  validatePricePerUser = () => {
    const {pricePerUser} = this.state;
    let pricePerUserValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (pricePerUser.length == 0 || pricePerUser=='') {
      pricePerUserValid = false;
      errorMsg.pricePerUser = 'Required'
    }
     else if (pricePerUser=='0') {
      pricePerUserValid = false;
      errorMsg.pricePerUser = 'Price must be greater than 0'
    }
    else if (!/^\d+(?:\.\d+)?(?:,\d+(?:\.\d+)?)*$/.test(pricePerUser)){
      pricePerUserValid = false;
      errorMsg.pricePerUser = 'Invalid price format'
    }
    this.setState({pricePerUserValid, errorMsg}, this.validateForm)
  }
  updatePricePerSms = (pricePerSms) => {
    this.setState({pricePerSms}, this.validatePricePerSms)
  }
  validatePricePerSms = () => {
    const {pricePerSms} = this.state;
    let pricePerSmsValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (pricePerSms.length == 0 || pricePerSms=='') {
      pricePerSmsValid = false;
      errorMsg.pricePerSms = 'Required'
    }
     else if (pricePerSms=='0') {
      pricePerSmsValid = false;
      errorMsg.pricePerSms = 'Price must be greater than 0'
    }
    else if (!/^\d+(?:\.\d+)?(?:,\d+(?:\.\d+)?)*$/.test(pricePerSms)){
      pricePerSmsValid = false;
      errorMsg.pricePerSms = 'Invalid price format'
    }
    this.setState({pricePerSmsValid, errorMsg}, this.validateForm)
  }
  updateSmsQuota = (smsQuota) => {
    this.setState({smsQuota}, this.validatesSmsQuota)
  }
  validatesSmsQuota = () => {
    const {smsQuota} = this.state;
    let smsQuotaValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (smsQuota.length == 0 || smsQuota=='') {
      smsQuotaValid = false;
      errorMsg.smsQuota = 'Required'
    }
     else if (parseInt(smsQuota)<0) {
      smsQuotaValid = false;
      errorMsg.smsQuota = 'Invalid Sms Quota'
    }
    else if (parseInt(smsQuota)>100000) {
      smsQuotaValid = false;
      errorMsg.smsQuota = 'Invalid Sms Quota'
    }
    else if (!/^\d+(?:\.\d+)?(?:,\d+(?:\.\d+)?)*$/.test(smsQuota)){
      smsQuotaValid = false;
      errorMsg.smsQuota = 'Invalid Sms Quota format'
    }
    this.setState({smsQuotaValid, errorMsg}, this.validateForm)
  }
  changeMaxBranchLimit=e=>
  {
    this.setState({selectedMaxBranch:parseInt(e.target.value)})
  }
  changeNoOfUser=e=>
  {
    this.setState({selectedUserPerBranch:parseInt(e.target.value)})
  }
  changeNoOfServices=e=>
  {
    this.setState({selectedNoOfServices:parseInt(e.target.value)})
  }
  changeCurrency=e=>
  {
    this.setState({selectedCurrency:e.target.value})
  }
  changeInTime=e=>
  {
    this.setState({selectedInTime:parseInt(e.target.value)})
  }
  changeOutTime=e=>
  {
    this.setState({selectedOutTime:parseInt(e.target.value)})
  }
  changeKisok()
  {
    var temp=!this.state.isKisok
    this.setState({isKisok:temp})
  }
  changeAdvanced()
  {
    var temp=!this.state.isAdvanced
    this.setState({isAdvanced:temp})
  }
  changeEasy()
  {
    var temp=!this.state.isEasy
    this.setState({isEasy:temp})
  }
  changeDedicated()
  {
    var temp=!this.state.isDedicated
    this.setState({isDedicated:temp})
  }
  changePirority()
  {
    var temp=!this.state.isPriority
    this.setState({isPriority:temp})
  }
  changeRemote()
  {
    var temp=!this.state.isRemote
    this.setState({isRemote:temp})
  }
  changeAllTime()
  {
    var temp=!this.state.isAllTimeSupport
    this.setState({isAllTimeSupport:temp})
  }
  changeSupportType=e=>
  {
    this.setState({selectedSupportType:e.target.value})
  }
  cancelPlan()
  {
    this.props.history.push('/packagePlan')
  }
  addPlan()
  {
    // var num = n;
    var shours = (this.state.selectedInTime / 60);
    var srhours = Math.floor(shours);
    var sminutes = (shours - srhours) * 60;
    var srminutes = Math.round(sminutes);
    srhours=srhours<10?'0' + srhours : srhours
    srminutes=srminutes<10?'0' + srminutes : srminutes
    var sTime=srhours+":"+srminutes

    var ehours = (this.state.selectedOutTime / 60);
    var erhours = Math.floor(ehours);
    var eminutes = (ehours - erhours) * 60;
    var erminutes = Math.round(eminutes);
    erhours=erhours<10?'0' + erhours : erhours
    erminutes=erminutes<10?'0' + erminutes : erminutes
    var eTime=erhours+":"+erminutes
    let token = localStorage.getItem('sessionToken')
    this.setState({
      progress:50,formValid:false
    })
    if(this.type=='add')
    {
        fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/paymentPlan/addOrUpdate', {
        method: 'POST',
        body:JSON.stringify({
          "advanceReportingEnabled": this.state.isAdvanced,
          "branchLimit": this.state.selectedMaxBranch,
          "createdTime": new Date() ,
          "dedicatedAccountManager": this.state.isDedicated,
          "easyPaymentEnabled": this.state.isEasy,
          "fullDaySupport": this.state.isAllTimeSupport,
          "kioskEnabled": this.state.isKisok,
          "noOfServices": this.state.selectedNoOfServices,
          "paymentPlanId": null,
          "planName": this.state.planName,
          "planNameArabic": this.state.planArabicName,
          "prices": [
            {
              "currency": this.state.selectedCurrency,
              "perSMSPrice": this.state.pricePerSms,
              "perUserPrice": this.state.pricePerUser,
              "priceAnnually": this.state.priceAnually,
              "priceMonthly": this.state.priceMontly
            }
          ],
          "prioritySupport": this.state.isPriority,
          "remoteDedicatedSupport": this.state.isRemote,
          "responseTimeInOffice": sTime,
          "responseTimeOutOffice": eTime,
          "noOfSMS": parseInt(this.state.smsQuota),
          "supportType": this.state.selectedSupportType,
          "noOfUserPerBranch": this.state.selectedUserPerBranch


        }),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
      })
      .then(response => response.json())
      .then(responsejson => {
        if(responsejson.applicationStatusCode==0)
        {
          this.setState({
            progress:100,formValid:true
          })
          global.isCenterUpdated.next(true)
          this.props.history.push('/packagePlan')
        
        }
        else{
          this.setState({
            progress:100,formValid:true
          })
          toaster.notify(responsejson.devMessage, {
            duration:5000 // This notification will not automatically close
          });
        }
      
      })
    }
    else if(this.type=='update')
    {
      fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/paymentPlan/addOrUpdate', {
      method: 'POST',
      body:JSON.stringify({
        "advanceReportingEnabled": this.state.isAdvanced,
        "branchLimit": this.state.selectedMaxBranch,
        "createdTime": new Date() ,
        "dedicatedAccountManager": this.state.isDedicated,
        "easyPaymentEnabled": this.state.isEasy,
        "fullDaySupport": this.state.isAllTimeSupport,
        "kioskEnabled": this.state.isKisok,
        "noOfServices": this.state.selectedNoOfServices,
        "paymentPlanId": this.state.packageId,
        "planName": this.state.planName,
        "planNameArabic": this.state.planArabicName,
        "prices": [
          {
            "currency": this.state.selectedCurrency,
            "perSMSPrice": this.state.pricePerSms,
            "perUserPrice": this.state.pricePerUser,
            "priceAnnually": this.state.priceAnually,
            "priceMonthly": this.state.priceMontly
          }
        ],
        "prioritySupport": this.state.isPriority,
        "remoteDedicatedSupport": this.state.isRemote,
        "responseTimeInOffice": sTime,
        "responseTimeOutOffice": eTime,
        "noOfSMS": parseInt(this.state.smsQuota),
        "supportType": this.state.selectedSupportType,
        "noOfUserPerBranch": this.state.selectedUserPerBranch


      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responsejson => {
      if(responsejson.applicationStatusCode==0)
      {
        this.setState({
          progress:100,formValid:true
        })
        global.isCenterUpdated.next(true)
        this.props.history.push('/packagePlan')
       
      }
      else{
        this.setState({
          progress:100,formValid:true
        })
        toaster.notify(responsejson.devMessage, {
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
    }
    
  }
  render() {
    return (
      <div className="animated fadeIn">
        <LoadingBar
        color='#2f49da'
        progress={this.state.progress}
        onLoaderFinished={() => this.setState({
          progress:0
        })}
        />
        <DefaultHeader/>
        


      <Card>
        <CardBody className="p-5">

  
            <h5><i className="icon2-align-left"></i> Core Features</h5>
                <Row>
                  <Col sm="4">
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Plan Name (English)</Label>
                    <Input type="text" placeholder="Plan Name (English)" 
                     value={this.state.planName} onChange={(e) => this.updatePlanName(e.target.value)} />
                     < ValidationMessage valid={this.state.planNameValid} message={this.state.errorMsg.planName} />
                  </FormGroup>
                  </Col>
                  <Col sm="4">
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Plan Name (Arabic)</Label>
                    <Input type="text" placeholder="Plan Name (Arabic)" 
                     value={this.state.planArabicName} onChange={(e) => this.updateArabicPlanName(e.target.value)} />
                     < ValidationMessage valid={this.state.planArabicNameValid} message={this.state.errorMsg.planArabicName} />
                  </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col sm="4">
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Max Branches Limit</Label> 
                    <select className="form-control" onChange={this.changeMaxBranchLimit} value={this.state.selectedMaxBranch}>
                    {/* <option value='0' key='0'>
                      Max Branches Limit
                    </option> */}
                    {this.state.allMaxBranch.map(branch => (
                      <option value={branch} key={branch} >{branch}</option>
                    ))}
                    </select>
                  </FormGroup>
                  </Col>
                  <Col sm="4">
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">No. of Users per Branch</Label> 
                    <select className="form-control" onChange={this.changeNoOfUser} value={this.state.selectedUserPerBranch}>
                    {/* <option value='0' key='0'>
                      No. of Users per Branch
                    </option> */}
                    {this.state.allUserPerBranch.map(branch => (
                      <option value={branch} key={branch} >{branch}</option>
                    ))}
                    </select>
                  </FormGroup>
                  </Col>
                  <Col sm="4">
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">No. of Services</Label> 
                    <select className="form-control" onChange={this.changeNoOfServices} value={this.state.selectedNoOfServices}>
                    {/* <option value='0' key='0'>
                      No. of Services
                    </option> */}
                    {this.state.allNoOfServices.map(branch => (
                      <option value={branch} key={branch} >{branch}</option>
                    ))}
                    </select>
                  </FormGroup>
                  </Col>
                </Row>




                <Row>
                  <Col sm="8">
                      
                      <Row>
                        <Col sm="6">
                          <FormGroup className="input-line">
                          <Label className="text-light-grey" htmlFor="exampleEmail" >Price (Monthly)</Label>
                          <Input type="text" placeholder="Price" 
                          value={this.state.priceMontly} onChange={(e) => this.updatePriceMontly(e.target.value)} />
                          < ValidationMessage valid={this.state.priceMontlyValid} message={this.state.errorMsg.priceMontly} />
                            </FormGroup>
                          </Col>
                          <Col sm="6">
                          <FormGroup className="input-line">
                          <Label className="text-light-grey" htmlFor="exampleEmail" >Price (Annualy)</Label>
                          <Input type="text" placeholder="Price" 
                          value={this.state.priceAnually} onChange={(e) => this.updatePriceAnually(e.target.value)} />
                          < ValidationMessage valid={this.state.priceAnuallyValid} message={this.state.errorMsg.priceAnually} />
                            </FormGroup>
                          </Col>
                          <Col sm="6">
                          <FormGroup className="input-line">
                          <Label className="text-light-grey" htmlFor="exampleEmail" >Price per User</Label>
                          <Input type="text" placeholder="Price" 
                          value={this.state.pricePerUser} onChange={(e) => this.updatePricePerUser(e.target.value)} />
                          < ValidationMessage valid={this.state.pricePerUserValid} message={this.state.errorMsg.pricePerUser} />
                            </FormGroup>
                          </Col>
                          <Col sm="6">
                          <FormGroup className="input-line">
                          <Label className="text-light-grey" htmlFor="exampleEmail" >Price per SMS</Label>
                          <Input type="text" placeholder="Price" 
                          value={this.state.pricePerSms} onChange={(e) => this.updatePricePerSms(e.target.value)} />
                          < ValidationMessage valid={this.state.pricePerSmsValid} message={this.state.errorMsg.pricePerSms} />
                            </FormGroup>
                          </Col>
                      </Row>
                      
                      
                  </Col>
                  <Col sm="4">
                      <FormGroup className="input-line">
                      <Label className="text-light-grey" htmlFor="exampleEmail" >&nbsp;</Label>
                      <select className="form-control text-warning" onChange={this.changeCurrency} value={this.state.selectedCurrency}>
                      <option value='AED' key='AED'>
                        Emirati dirham (AED)
                      </option>
                      <option value='USD' key='USD'>
                        United State Dollar (USD)
                      </option>
                      </select>
                      </FormGroup>
                      
                  </Col>
                  
                </Row>


                
                      


        <hr className="mb-5"></hr>
        <h5 className="mb-3"><i className="icon2-payments-card"></i> Additional Features</h5>

        <Row>
          <Col sm="3">
          <div className="checkbox mb-2">
            <input type="checkbox"  checked={this.state.isKisok} onChange={()=>this.changeKisok()}></input>
            <label >Kiosk Enabled</label>
          </div>
          <div className="checkbox mb-2">
            <input type="checkbox" checked={this.state.isAdvanced} onChange={()=>this.changeAdvanced()}></input>
            <label>Advanced Reporting</label>
          </div>
          </Col>
          <Col sm="3">
          <div className="checkbox mb-2">
            <input type="checkbox"  checked={this.state.isEasy} onChange={()=>this.changeEasy()}></input>
            <label>Easy Payments</label>
          </div>
          <p> 
          <FormGroup className="input-line">
          <Label className="text-light-grey" htmlFor="exampleEmail" >Free Sms Quota</Label>
            <Input type="text" placeholder="SMS Quota" 
            value={this.state.smsQuota} onChange={(e) => this.updateSmsQuota(e.target.value)} />
            < ValidationMessage valid={this.state.smsQuotaValid} message={this.state.errorMsg.smsQuota} />
            </FormGroup>
          </p>
          </Col>
        </Row>
                  
                  

                    
        <hr className="mb-5"></hr>
        <h5 className="mb-3"><i className="icon2-tools-gear"></i> Support</h5>
                  
                  <Row>
                    <Col sm="3">
                      <FormGroup className="input-line">
                      <Label className="text-primary">Support Type</Label>
                      <select className="form-control" onChange={this.changeSupportType} value={this.state.selectedSupportType}>
                        <option value='Basic' key='Basic'>
                          Basic
                        </option>
                        <option value='Standard' key='Standard'>
                          Standard
                        </option>
                        <option value='Professional' key='Professional'>
                          Professional
                        </option>
                    </select>
                    </FormGroup>
                    </Col>
                    
                  </Row>
                  <Row>
                    <Col sm="3">
                      <FormGroup className="input-line">
                      <Label className="text-primary">Response Time (mins)</Label>
                      {/* <TimePickerComponent id="starttimepicker" value={this.state.startTime}  max={this.state.endTime} allowEdit={false}
                        onChange={(time) => this.changeStartTime(time)} /> */}
                     <select className="form-control" value={this.state.selectedInTime}
                     onChange={this.changeInTime} 
                     value={this.state.selectedInTime}>
                      {/* <option value='0' key='0'>
                        No. of Services
                      </option> */}
                      {this.state.allInOfficeTime.map(time => (
                        <option value={time} key={time} >{time} </option>
                      ))}
                    </select>
                    </FormGroup>
                    </Col>
                    <Col sm="3">
                      <FormGroup className="input-line">
                      <Label className="text-primary">Out Office Response Time (mins)</Label>
                      {/* <TimePickerComponent id="endtimepicker" value={this.state.endTime} min={this.state.startTime} allowEdit={false}
                        onChange={(time) => this.changeEndTime(time)} /> */}
                    <select className="form-control" value={this.state.selectedOutTime}
                    onChange={this.changeOutTime} 
                     value={this.state.selectedOutTime}>
                      {/* <option value='0' key='0'>
                        No. of Services
                      </option> */}
                      {this.state.allOutOfficeTime.map(time => (
                        <option value={time} key={time} >{time} </option>
                      ))}
                    </select>
                    </FormGroup>
                    </Col>
                  </Row>
                  
                  <h5 className="mb-3 mt-5">Additional Support Term</h5>
                  <div className="checkbox mb-2">
                    <input type="checkbox"  checked={this.state.isDedicated}
                    onClick={()=>this.changeDedicated()}
                    ></input>
                    <label>Dedicated Account Manager</label>
                  </div>
                  <div className="checkbox mb-2">
                    <input type="checkbox"checked={this.state.isPriority}
                    onClick={()=>this.changePirority()}></input>
                    <label >Priority Support</label>
                  </div>
                  <div className="checkbox mb-2">
                    <input type="checkbox"  checked={this.state.isRemote}
                    onClick={()=>this.changeRemote()}></input>
                    <label >Remote Dedicated Support</label>
                  </div>
                  <div className="checkbox mb-2">
                    <input type="checkbox"  checked={this.state.isAllTimeSupport}
                    onClick={()=>this.changeAllTime()}></input>
                    <label >24Hr Support</label>
                  </div>
                    


                <div className="text-right">
                    <Button outline color="primary" className="mr-2" onClick={()=>this.cancelPlan()}>Cancel</Button>
                    {/* <Button outline color="primary" className="mr-2">Save Draft</Button> */}
                  <Button color="primary" disabled={!this.state.formValid} onClick={()=>this.addPlan()}>Save</Button>
              </div>
            
        </CardBody>
</Card>
      


      </div>
    );
  }
}

export default Breadcrumbs;
