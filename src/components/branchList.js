import React, { Component } from 'react';
import { Card, CardBody, Col, Row, Table,FormGroup, ButtonDropdown,  Dropdown, DropdownItem, DropdownMenu, DropdownToggle,Input, Button } from 'reactstrap'; 
import { Link } from 'react-router-dom'; 
import { DatePickerComponent  } from '@syncfusion/ej2-react-calendars';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import axios from "axios"; 
const DefaultHeader = React.lazy(() => import('./defaultHeader'));

class Branch extends Component {
  tempAvialableBranches=[]
  totalBranches=0
  count=0
  constructor(props) {
    super(props);
    this.state = {
      allBranches:[],
      searchBranch:'',
      isShowBranchPrev:false,
      isShowBranchEditPrev:false,
      isShowDropDownMenu:false,
      isShowSortByMenu:false,
      isSortBy:'ascending',
      isShowFilters:false,
      filterStatus:'',
      filterBranch:'',
      filterSevice:'',
      filterCounter:'',
      filterAgents:'',
      filterAst:'',
      sDate:null,
      isAll:false,
      load:false
    };
  
  }
  componentDidMount()
  {

    // setTimeout(() => {
    //   this.setState({lo})
    // }, 3000);
    if(localStorage.getItem('isShowArabicLanguage')=='true')
    {
      global.isShowArabicLanguage=true
    }
    else
    {
      global.isShowArabicLanguage=false
    }
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Branch_View')
        {
          this.setState({isShowBranchPrev:true})
          isPrev=true
        }
        else {
          if(!isPrev==true)
          {
            isPrev=false
          }
         
        }
        if(prev[i]=='Branch_Edit')
        {
          this.setState({isShowBranchEditPrev:true})
        }
      }
    }
    if(isPrev==false)
    {
      toaster.notify("User don't have privilege", {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
    else{
      this.getBranchList()
    }
  }
  sortBy(data)
  {
    this.setState({
      isSortBy:data
    })
  }

  checkBranch(){
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/check', {
      method: 'POST',
      body:JSON.stringify({

      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responsejson => {
      if(responsejson.applicationStatusCode==0)
      {
        console.log(responsejson);
        // this.branchName=responsejson.values.BranchName
        // this.setState({next:1,formValid:true,selectedWeekDayId:1,progress:100})
        // global.isBranchAgentCounterUpdated.next(true)
        // global.isNotificationUpdated.next(true)

        this.props.history.push('/add-branch')
      }
      else{
        this.setState({formValid:true,progress:100})
        toaster.notify(responsejson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000// This notification will not automatically close
        });
      }
    })
  
  }
  getBranchList()
  {
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/branch/getListing';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      console.log('==================responseJson==================');
      console.log(responseJson);
      console.log('===================responseJson=================');
      if(responseJson.data.applicationStatusCode==0) {
        responseJson.data.branchList.map((item,i)=>{
          item.branch['isChecked']=false;
          fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/getCompleteDetails/id', {
            method: 'POST',
            body:JSON.stringify({
              "id": parseInt(responseJson.data.branchList[i].branch.branchId
                )
            }),
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            },
          }).then(response => response.json()).then(responsejson => {
            console.log("Ye hai mera response", responseJson)
            if(responsejson.applicationStatusCode==0){
              if(responsejson.branch.branchStatus==0|| responsejson.branch.branchStatus==2){
                console.log(i);
                item.branch.workingStatusValue='Closed'
                item.branch.branchStatus=2
                console.log(item.branchStatus=2)
              }
              else
              {
                item.workingStatusValue='Open'
                item.branchStatus=1
              }
              this.setState({extra:true})
              setTimeout(() => {
                this.setState({load:true})
              }, 1000);
            }
            
        
          })

          var oldDate=new Date(item.branch['createdTime'])
          let d = oldDate.getDate();
          let m = oldDate.getMonth()+1;
          let y=oldDate.getFullYear()
          d = d < 10 ? '0' + d : d;
          m = m < 10 && m > 0 ? '0' + m : m;
          var getDate=d+"."+m+"."+y 
          item.branch['date']=oldDate
          var hms = item.branch.averageServiceTime;  
          var a = hms.split(':'); 
          var minutes = (+a[0]) * 60 + (+a[1]); 
          console.log(minutes);
          item.branch['AST']=minutes
          item.branch['AddedOn']=getDate 
          item['isShowMenu']=false
        })
        this.tempAvialableBranches.push(responseJson.data.branchList)
        this.totalBranches=this.tempAvialableBranches[0].length
        this.setState({
          allBranches:responseJson.data.branchList,
         
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }

  branchDetails(id)
  {

 
  }
  showMenu(id)
  {
      for(var i=0;i<this.state.allBranches.length;i++)
      {
        if(this.state.allBranches[i].branch.branchId==id)
        {
          this.state.allBranches[i]['isShowMenu']=true
          this.setState({
            isShowDropDownMenu:true
          })
        }
        else
        {
          this.state.allBranches[i]['isShowMenu']=false
        }
      }
      var data=this.state.allBranches
      this.setState({
        allBranches:data
      })
  }
  search(branchName)
  {
    
    if(branchName!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempAvialableBranches[0].length;i++)
      {
        if(this.tempAvialableBranches[0][i].branch.branchName.toLowerCase().includes(branchName.toLowerCase()))
        {
          data.push(this.tempAvialableBranches[0][i])
          this.setState({
            allBranches:data
          })
          isMatch=true
          this.totalBranches=data.length
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
        }
      }
      if(isMatch==false)
      {
        this.totalBranches=0
        this.setState({allBranches:[]})
      }
    }
    else
    {
      this.totalBranches=this.tempAvialableBranches[0].length
      this.setState({
        allBranches:this.tempAvialableBranches[0]
      })
    }
  
  }
  changeSearch(branchName)
  {
    if(branchName=='')
    { 
      this.totalBranches=this.tempAvialableBranches[0].length
      this.setState({
        allBranches:this.tempAvialableBranches[0]
      })
    }
    this.search(branchName)
    this.setState({
    searchBranch:branchName
    })
  }
  sortByStatus()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
          return Number(a.branch.workingStatus)  - Number(b.branch.workingStatus);
        })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
          return Number(a.branch.workingStatus)  - Number(b.branch.workingStatus);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
    
  }
  sortByName()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>a.branch.branchName.localeCompare(b.branch.branchName))
      }
      else
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>b.branch.branchName.localeCompare(a.branch.branchName))
      }
      console.log(sortedProductsAsc)
      this.setState({
          allBranches:sortedProductsAsc
      })
    }
    
  }
  sortByCounters()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
         return Number(a.noOfTotalCounters)  - Number(b.noOfTotalCounters);
        })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
          return Number(a.noOfTotalCounters)  - Number(b.noOfTotalCounters);
         })
         sortedProductsAsc=sortedProductsAsc.reverse()
      }
     

    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
    
  }
  sortByAgents()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
        return Number(a.noOfAgents)  - Number(b.noOfAgents);
        })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
        return Number(a.noOfAgents)  - Number(b.noOfAgents);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    
    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
  }
  sortByDates()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
        return new Date (a.branch.date)  - new Date(b.branch.date);
      })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
        return new Date(a.branch.date)  - new Date(b.branch.date);
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
      }
     
    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
  }
  sortByAST()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
        return Number(a.branch.AST)  - Number(b.branch.AST);
      })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
        return Number(a.branch.AST)  - Number(b.branch.AST);
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
  }
  changefilterStatus=e=>
  {
    this.setState({
      filterStatus:e.target.value
    })
  }
  changeBranchFilter(data)
  {
    this.setState({
      filterBranch:data
    })
  }
  changeServiceFilter(data)
  {
    this.setState({
      filterSevice:data
    })
  }
  changeAgentFilter(data)
  {
    this.setState({
      filterAgents:data
    })
  }
  changeCounterFilter(data)
  {
    this.setState({
      filterCounter:data
    })
  }
  changeASTFilter(data)
  {
    this.setState({
      filterAst:data
    })
  }
  changeStartDate(date)
{
  this.setState({sDate:date.value})
  }
  applyFilter()
  {
    var filterData=JSON.parse(JSON.stringify(this.tempAvialableBranches[0]))
    if(this.state.filterStatus!='Select Status')
    {
      var val=this.state.filterStatus
      filterData =  filterData.filter(function(status) {
        return status.branch.workingStatusValue.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterBranch!='')
    {
      var val=this.state.filterBranch
      filterData =  filterData.filter(function(status) {
        return status.branch.branchName.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterCounter!='')
    {
      var val=this.state.filterCounter
      filterData =  filterData.filter(function(status) {
        return status.noOfTotalCounters == parseInt(val);
      });
    }
    if(this.state.filterAgents!='')
    {
      var val=this.state.filterAgents
      filterData =  filterData.filter(function(status) {
        return status.noOfAgents == parseInt(val);
      });
    }
    if(this.state.filterAst!='')
    {
      var val=this.state.filterAst
      filterData =  filterData.filter(function(status) {
        return status.branch.AST == parseInt(val);
      });
    }
    if(this.state.sDate!=null)
    {
      let d = this.state.sDate.getDate();
      let m = this.state.sDate.getMonth()+1;
      let y=this.state.sDate.getFullYear()
      d = d < 10 ? '0' + d : d;
      m = m < 10 && m > 0 ? '0' + m : m;
      var val=d+"."+m+"."+y 
      filterData =  filterData.filter(function(status) {
        return status.branch.AddedOn == val;
      });
    }
    this.totalBranches=filterData.length
    this.setState({
      allBranches:filterData
    })
    console.log(filterData)
   
  }
  resetFilter()
  {
    this.totalBranches=this.tempAvialableBranches[0].length
    this.setState({
      filterStatus:'Select Status',filterBranch:'',filterAgents:'',filterCounter:'',filterAst:'',sDate:null,allBranches:this.tempAvialableBranches[0]
    })
  }
  branchDetails(id)
  {
    this.props.history.push(
      {
        pathname:'/branch-details',
        branchId:id
      }
    
    )
  }
  selectAll()
  {
    var temp=this.state.isAll
    if(this.tempAvialableBranches[0].length!=0)
    {
      for(var i=0;i<this.tempAvialableBranches[0].length;i++)
      {
        if(temp==false)
        {
          this.tempAvialableBranches[0][i].branch['isChecked']=true
          this.count=this.tempAvialableBranches[0].length
        }
        else
        {
          this.count=0
          this.tempAvialableBranches[0][i].branch['isChecked']=false
        }
        
      }
    }
    this.setState({
      isAll:!temp,
      allBranches:this.tempAvialableBranches[0],
    })
  }
  select(data)
  {
    for(var i=0;i<this.tempAvialableBranches[0].length;i++)
    {
      if(data.branchId==this.tempAvialableBranches[0][i].branch.branchId)
      {
        if(this.tempAvialableBranches[0][i].branch['isChecked']==true)
        {
          this.tempAvialableBranches[0][i].branch['isChecked']=false
          this.setState({
            isAll:false
          })
          this.count=this.count-1
          break;
        }
        else
        {         
          this.count=this.count+1
          this.tempAvialableBranches[0][i].branch['isChecked']=true
        }
  
      }
    }
    if(this.count==this.tempAvialableBranches[0].length)
    {
      this.setState({
        isAll:true
      })
    }
    this.setState({allBranches:this.tempAvialableBranches[0]})
  }
  render() {
    return (
      <div className="animated fadeIn">
      <DefaultHeader/>
        <Row className="mb-3 hidden">
          <Col sm="4">
            <h3>
            
              <div className="checkbox form-check d-inline-block ml-2">
                  <input type="checkbox" className="form-check-input" onChange={()=>this.selectAll()} checked={this.state.isAll} ></input>
                  <label  className="form-check-label form-check-label">{this.totalBranches}&nbsp;Branches</label>
              </div>
              </h3>
          </Col>

          {this.state.isShowBranchPrev==true &&  <Col sm="8" className="text-right">
          
            <div className="float-right">
             <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button>
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent" onClick={() => {this.setState({ isShowSortByMenu:false })}}>
                <div className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></div>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={()=>this.sortBy('ascending')}><input type="radio" checked={this.state.isSortBy=='ascending'} name="sort-order" onChange={()=>{}}></input> Ascending</DropdownItem>
                  <DropdownItem onClick={()=>this.sortBy('descending')}><input type="radio" checked={this.state.isSortBy=='descending'} name="sort-order" onChange={()=>{}}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>

              
              
            {this.state.isShowBranchEditPrev==true && <button className="btn btn-outline-warning ml-2" onClick={()=>this.checkBranch()}><i className="icon2-addition-sign"></i></button>}
            </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                value={this.state.searchBranch} onChange={(e) => this.changeSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.search()
                  }
                }}
                >
                  
                </input>
              <div className="input-group-append" onClick={()=>this.search()}><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>}
        </Row>

        {this.state.isShowFilters==true && <Card className="card-line" id="filter-row">
          <CardBody>
            <Row>
              
                    <Col className="text-center">
                      <FormGroup className="input-line mb-0">
                        <select className="form-control" onChange={this.changefilterStatus} value={this.state.filterStatus}>
                          <option value='Select Status' key='Select Status'>
                            Select Status
                          </option>
                          <option value='Open' key='Open'>
                            Open
                          </option>
                          <option value='Closed' key='Closed'>
                            Closed
                          </option>
                        </select>
                      </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                        <Input type="text" placeholder="Branch"   value={this.state.filterBranch} 
                        onChange={(e) => this.changeBranchFilter(e.target.value)}/>
                        </FormGroup>
                    </Col>
                    <Col className="text-center">
                    <FormGroup className="input-line  mb-0">
                        <Input type="text" placeholder="Service"   value={this.state.filterSevice} 
                        onChange={(e) => this.changeServiceFilter(e.target.value)}/>
                        </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                        <Input type="number" placeholder="Counter" min="0"
                        value={this.state.filterCounter} 
                        onChange={(e) => this.changeCounterFilter(e.target.value)}
                        />
                        </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="Agents" min="0"
                       value={this.state.filterAgents} 
                       onChange={(e) => this.changeAgentFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="AST" min="0"
                       value={this.state.filterAst} 
                       onChange={(e) => this.changeASTFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                        <DatePickerComponent  value={this.state.sDate}  placeholder='Added On'
                        onChange={(date) => this.changeStartDate(date)}/>
                        </FormGroup> 
                    </Col>
                   
                    <Col sm="auto">
                    <Button color="light" className="btn-sm" onClick={()=>this.resetFilter()}>Reset</Button>
                      <Button color="primary" className="btn-sm ml-2" onClick={()=>this.applyFilter()}>Apply</Button>
                    </Col>
                    
                    </Row>
                  </CardBody>
                  </Card>}



        {this.state.isShowBranchPrev==true && <Card className="mb-0">
              
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                    <th width="71"></th>
                    <th className="text-center" onClick={()=>this.sortByStatus()}>Status</th>
                    <th onClick={()=>this.sortByName()} className="text-left">Branch</th>
                    <th>Services</th>
                    <th onClick={()=>this.sortByCounters()}>Counters </th>
                    <th onClick={()=>this.sortByAgents()}>Agents</th>
                    <th onClick={()=>this.sortByAST()}>AST</th>
                    <th onClick={()=>this.sortByDates()} >Added On</th>
                    <th>Rating</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.state.allBranches.map((branch,index) => (
                    <tr key={index}>
                      <td className="text-right" >
                        <div className="checkbox form-check">
                            <input  type="checkbox" className="form-check-input" checked={branch.branch.isChecked} onChange={()=>this.select(branch.branch)}></input>
                            <label className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">
                      {(this.state.load)&&(
                        <>
                        {branch.branch.branchStatus==1 && global.isShowArabicLanguage==false && <span  className="badge badge-success">Open</span>}
                        {branch.branch.branchStatus==1 && global.isShowArabicLanguage==true && <span  className="badge badge-success">مفتوح</span>}
                        {branch.branch.branchStatus==2 && global.isShowArabicLanguage==false &&<span className="badge badge-danger">Closed</span>}
                        {branch.branch.branchStatus==2 && global.isShowArabicLanguage==true &&<span className="badge badge-danger">تم/أغلقت</span>}
                      
                        </>
                      )}
                      </td>
                      <td>{branch.branch.branchName}
                      <p className="text-light-grey"><i className="icon2-location"></i> {branch.branch.address}</p>
                      </td>
                      <td>
                      <FormGroup className="input-line">
                        <select className="form-control" onChange={this.changeServiceSector} >
                          {branch.providedServices.map(service => (
                            <option value={service.serviceId} key={service.serviceId}>
                              {service.serviceName}
                            </option>
                          ))}
                        </select>
                        </FormGroup>

                      </td>
                      <td className="text-center" style={{paddingTop:23}}>{branch.noOfTotalCounters}</td>
                      <td className="text-center" style={{paddingTop:23}}>{branch.noOfAgents}</td>
                      <td className="text-center" style={{paddingTop:23}}>{branch.branch.AST} <small className="text-light-grey">min</small></td>
                      <td className="text-center" style={{paddingTop:23}}>{branch.branch.AddedOn}</td>
                      <td className="text-center"  style={{paddingTop:20}}>
                        {(branch.branch.rating < 2 && branch.branch.ratingCount==0) &&<i className="icon2-normal-face text-warning f20"></i>}
                        {(branch.branch.rating < 2 && branch.branch.ratingCount!=0) &&<i className="icon2-angry-face text-danger f20"></i>}
                        {branch.branch.rating >= 2 && branch.branch.rating < 4 &&<i className="icon2-normal-face text-warning f20"></i>}
                        {branch.branch.rating >= 4 && branch.branch.rating <= 5 &&<i className="icon2-smile text-success f20"></i>}
                      </td>
                      <td className="text-center"  style={{paddingTop:20}}>
                        <div className="ml-4" onClick={()=>this.showMenu(branch.branch.branchId)}>
                          <i className="f20 icon2-menu-options text-light-grey"></i>
                        </div>
                        {branch.isShowMenu==true && 
                          <Dropdown isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu:false }); }} >
                            <DropdownToggle tag="span" onClick={() => {this.setState({ isShowDropDownMenu:false })}}
                              data-toggle="dropdown" >
                            </DropdownToggle>
                            <DropdownMenu>
                              <DropdownItem onClick={()=>this.branchDetails(branch.branch.branchId)}>Details</DropdownItem>
                            </DropdownMenu>
                          </Dropdown>
                        }
                      </td>
                    </tr>
                   ))}
                   </tbody>
                </Table>
               
              </CardBody>
            </Card>}
          
          


      


      </div>
    );
  }
}

export default Branch;
