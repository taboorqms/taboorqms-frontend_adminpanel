import React from 'react';
const profileAside = React.lazy(() =>  import ('./profileAside'));
const BranchList = React.lazy(() => import('./branchList'));
const BranchDetails = React.lazy(() => import('./branchDetails'));
const centerProfile = React.lazy(() => import('./centerProfile'));
const AddBranch = React.lazy(() => import('./addBranch'));
const AgentList = React.lazy(() => import('./agentList'));
const AgentDetails = React.lazy(() => import('./agentDetails'));
const QueueList = React.lazy(() => import('./queueList'));
const QueueDetails = React.lazy(() => import('./queueDetails'));
const Dashboard = React.lazy(() => import('./dashboard'));
const support = React.lazy(() => import('./support'));
const myAccount = React.lazy(() => import('./myAccount'));
const users = React.lazy(() => import('./users'));
const roles = React.lazy(() => import('./roles'));
const notifications = React.lazy(() => import('./notifications'));
const configurations = React.lazy(() => import('./configurations'));
const centerList = React.lazy(() => import('./centerList'));
const invoices = React.lazy(() => import('./invoices'));
const editinvoices = React.lazy(() => import('./editinvoice.js'));
const packagePlan = React.lazy(() => import('./packagePlan'));
const payment = React.lazy(() => import('./payment'));
const newInvoice = React.lazy(() => import('./newInvoice'));
const packageDetails = React.lazy(() => import('./packageDetails'));
const newPlan = React.lazy(() => import('./newPlan'));
const paymentSummary = React.lazy(() => import('./paymentSummary'));
const paymentSummary2 = React.lazy(() => import('./paymentSummary2'));
const register2 = React.lazy(() => import('./register2'));
const routes = [
    { path: '/', exact: true, name: 'Home' },
    { path: '/dashboard', name: 'Dashboard', component: Dashboard },
    { path: '/branch-list', name: 'Branches', component: BranchList },
    { path: '/branch-details', name: 'Branch Detail', component: BranchDetails },
    { path: '/add-branch', name: 'New Branch', component: AddBranch },
    { path: '/agent-list', name: 'Agents', component: AgentList },
    { path: '/agent-details', name: 'Agent Detail', component: AgentDetails },
    { path: '/queue-list', name: 'Queues', component: QueueList },
    { path: '/queue-details', name: 'Queue Detail', component: QueueDetails },
    { path: '/centerProfile', name: 'Service Center Profile', component: centerProfile },
    { path: '/support', name: 'Support', component: support },
    { path: '/myAccount', name: 'My Account', component: myAccount },
    { path: '/users', name: 'Users', component: users },
    { path: '/roles', name: 'Roles', component: roles },
    { path: '/notifications', name: 'Notifications', component: notifications },
    { path: '/configurations', name: 'Configurations', component: configurations },
    { path: '/centerList', name: 'Service Center', component: centerList },
    { path: '/invoices', name: 'Invoices', component: invoices },
    { path: '/packagePlan', name: 'Plans/Packages', component: packagePlan },
    { path: '/payment', name: 'Payment', component: payment },   
    { path: '/newInvoice', name: 'New Invoice', component: newInvoice },
    { path: '/editinvoices', name: 'Edit Invoice', component: editinvoices },
    { path: '/packageDetails', name: 'Plans/Package Details', component: packageDetails },
    { path: '/newPlan', name: 'New Plans/Package', component: newPlan },
    { path: '/paymentSummary', name: 'Payment Summary', component: paymentSummary },
    { path: '/paymentSummary2', name: 'Payment Summary 2', component: paymentSummary2 },
    { path: '/profileaside', name: 'Profile Aside', component: profileAside },
    // { path: '/register2', name: 'Register 2', component: register2 },
  ];
  export default routes;