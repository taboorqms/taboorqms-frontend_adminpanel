
export default {
   items: [
        {
          name: 'Home',
          url: '/dashboard',
          icon: 'icon2-home-menu',
          
        },
        // {
        //   title: true,
        //   name: 'Theme',
        //   wrapper: {            // optional wrapper object
        //     element: '',        // required valid HTML5 element tag
        //     attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
        //   },
        //   class: ''             // optional class names space delimited list for title item ex: "text-center"
        // },   
        // {
        //   title: true,
        //   name: 'Components',
        //   wrapper: {
        //     element: '',
        //     attributes: {},
        //   },
        // },
        {
          name: 'Branches',
          url: '/branch-list',
          icon: 'icon2-branches',
          badge: {
            variant: 'primary',
            text: '0',
          },
          // children: [
          //   {
          //     name: 'Branches',
          //     url: '/branch-list',
          //     icon: 'icon-puzzle',
          //   },
          //   {
          //     name: 'Branch Details',
          //     url: '/branch-details',
          //     icon: 'icon-puzzle',
          //   },
          //   {
          //     name: 'New Branch',
          //     url: '/add-branch',
          //     icon: 'icon-puzzle',
          //   },
            
          // ],
        },
        {
          name: 'Agents',
          url: '/agent-list',
          icon: 'icon2-username',
          badge: {
            variant: 'primary',
            text: '0',
          },
          // children: [
          //   {
          //     name: 'Agents',
          //     url: '/agent-list',
          //     icon: 'icon-cursor',
          //   },
          //   {
          //     name: 'Agent Details',
          //     url: '/agent-details',
          //     icon: 'icon-cursor',
          //   },
            
          // ],
        },
        {
          name: 'Queues',
          url: '/queue-list',
          icon: 'icon2-queues',
          badge: {
            variant: 'warning',
            text: '0',
          },

        },
        // {
        //   name: 'Centers',
        //   url: '/centerList',
        //   icon: 'icon2-username',
        //   meta: 'forSuperAdmin',
        //   badge: {
        //     variant: 'primary',
        //     text: '0',
        //   },
        // },
        // {
        //   name: 'Invoices',
        //   url: '/invoices',
        //   icon: 'icon-pie-chart',
        //   meta: 'forSuperAdmin',
        // },
        // {
        //   name: 'Packages/Plans',
        //   url: '/packagePlan',
        //   icon: 'icon-pie-chart',
        //   meta: 'forSuperAdmin',
        // },
        // {
        //   name: 'Reports',
        //   url: '/landing',
        //   icon: 'icon-pie-chart',
        // },
        // {
        //   divider: true,
        // },
        {
          divider: true,
        },
        {
          divider: true,
        },
        {
          name: 'Center Profile',
          url: '/centerProfile',
          icon: 'icon2-counter',
        },
        {
          name: 'Settings',
          url: '/register',
          icon: 'icon2-settings-bars',
          children: [
            {
              name: 'My Account',
              url: '/myAccount',
              icon: 'icon2-username',
              attributes: { disabled: true },
            },
            {
              name: 'Users',
              url: '/users',
              icon: 'icon2-username',
              badge: {
                variant: 'info',
                
              },
            },
            {
              name: 'Roles',
              url: '/roles',
              icon: 'icon2-target1',
              badge: {
                variant: 'secondary',
                
              },
            },
            {
              name: 'Notifications',
              url: '/notifications',
              icon: 'icon2-alarm-bell',
            },
            {
              name: 'Configurations',
              url: '/configurations',
              icon: 'icon2-settings-gear',
            },
          ],
        },
        {
          name: 'Support',
          url: '/support',
          icon: 'icon2-tools-gear',
          
        },
        {
          name: 'Logout',
          url: '/login',
          icon: 'icon2-logout-menu',
        },
        
        
        {
          divider: true,
        },
        {
          title: true,
          name: 'Extras',
        },
      ],
}