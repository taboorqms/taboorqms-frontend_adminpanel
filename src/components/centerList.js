import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table, ButtonDropdown, Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from "axios";
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import LoadingBar from 'react-top-loading-bar'
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
class Breadcrumbs extends Component {
  tempCenters = []
  counter = 0
  constructor(props) {
    super();
    this.toggle = this.toggle.bind(this);
    this.state = {
      isAll: false,
      isShowServiceCenterMenuPrev: false,
      isSortBy: 'ascending',
      isShowDropDownMenu: false,
      activeTab: new Array(4).fill('1'),
      searchCenterName: '',
      collapse: false,
      accordion: [true, false, false],
      allCenters: [],
      isShowServiceCenterPrev: false,
      isShowServiceCenterEditPrev: false,
      totalCenters: 0,
      custom: [true, false],
      status: 'Closed',
      fadeIn: true,
      timeout: 300,
      isManual: true,
      isFAQ: false,
      isTicket: false,
      modal: false,
      large: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,      
      progress:0,
      info: false,
      currentPage: 1,
      centersPerPage: 3,

    };
    this.toggleLarge = this.toggleLarge.bind(this);
    this.addfaqmodal = this.addfaqmodal.bind(this);
    this.addmanualmodal = this.addmanualmodal.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }
  componentDidMount() {
    var prev = JSON.parse(localStorage.getItem('privileges'))
    var isPrev = false
    if (prev != null) {
      for (var i = 0; i < prev.length; i++) {
        if (prev[i] == 'Service_Center_View') {
          this.setState({ isShowServiceCenterPrev: true })
          isPrev = true
        }
        else {
          if (!isPrev == true) {
            isPrev = false
          }
        }
        if (prev[i] == 'Service_Center_Edit') {
          this.setState({ isShowServiceCenterEditPrev: true })
        }
        if (prev[i] == 'Service_Center_Profile_View') {
          this.setState({ isShowServiceCenterMenuPrev: true })
        }
      }
    }
    if (isPrev == false) {
      toaster.notify("User don't have privilege", {
        duration: 5000
      });
    }
    else {
      this.getCenterList()
    }
  }
  getCenterList() {
    this.tempCenters = []
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getListing';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    axios.get(url, header).then(responseJson => {
      if (responseJson.data.applicationStatusCode == 0) {
        this.setState({
          progress:100,isDisableSave:false
        })
        for (var i = 0; i < responseJson.data.serviceCenterList.length; i++) {
          responseJson.data.serviceCenterList[i].serviceCenter['isChecked'] = false
          if (responseJson.data.serviceCenterList[i].serviceCenter.createdTime != null) {
            var createdDate = new Date(responseJson.data.serviceCenterList[i].serviceCenter.createdTime)
            var createdOn = createdDate.getDate() + '.' + (createdDate.getMonth() + 1) + '.' + createdDate.getFullYear()
            responseJson.data.serviceCenterList[i].serviceCenter['createdDate'] = createdOn
          }
          else {
            responseJson.data.serviceCenterList[i]['createdDate'] = null
          }
          if (responseJson.data.serviceCenterList[i].nextPaymentDueDate != null) {
            var nextDate = new Date(responseJson.data.serviceCenterList[i].nextPaymentDueDate)
            var nextDueOn = nextDate.getDate() + '.' + (nextDate.getMonth() + 1) + '.' + nextDate.getFullYear()
            responseJson.data.serviceCenterList[i]['nextDueOn'] = nextDueOn
          }
          else {
            responseJson.data.serviceCenterList[i]['nextDueOn'] = null
          }
          responseJson.data.serviceCenterList[i]['isShowMenu'] = false

          this.tempCenters.push(responseJson.data.serviceCenterList[i])
        }
        this.setState({
          allCenters: this.tempCenters,
          totalCenters: this.tempCenters.length
        })
        this.setState({
          allCenters: this.tempCenters,
          totalCenters: this.tempCenters.length
        })
      }
      else{
        this.setState({
          progress:50,isDisableSave:false
        })
      }
    }).catch(err => {
      console.log(err)
    });
  }
  showMenu(id) {
    for (var i = 0; i < this.state.allCenters.length; i++) {
      if (this.state.allCenters[i].serviceCenter.serviceCenterId == id) {
        this.state.allCenters[i]['isShowMenu'] = true
        this.setState({
          isShowDropDownMenu: true
        })
      }
      else {
        this.state.allCenters[i]['isShowMenu'] = false
      }
    }
    var data = this.state.allCenters
    this.setState({
      allCenters: data
    })
  }
  toggleLarge() {
    this.setState({
      large: !this.state.large,
    });
  }

  addfaqmodal() {
    this.setState({
      large: !this.state.large,
    });
  }
  addmanualmodal() {
    this.setState({
      large: !this.state.large,
    });
  }
  toggletAB(tabPane, tab) {
    const newArray = this.state.activeTab.slice()
    newArray[tabPane] = tab
    this.setState({
      activeTab: newArray,
    });
  }

  onEntering() {
    this.setState({ status: 'Opening...' });
  }

  onEntered() {
    this.setState({ status: 'Opened' });
  }

  onExiting() {
    this.setState({ status: 'Closing...' });
  }

  onExited() {
    this.setState({ status: 'Closed' });
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleAccordion(data) {
    for (var i = 0; i < this.tmp.length; i++) {
      if (this.tmp[i].id == data.id) {
        if (this.tmp[i].isChecked == true) {
          this.tmp[i].isChecked = false
        }
        else {
          this.tmp[i].isChecked = true
        }
      }
    }
    this.setState({
      allFAQs: this.tmp
    })
  }

  toggleCustom(tab) {
    const prevState = this.state.custom;
    const state = prevState.map((x, index) => tab === index ? !x : false);
    this.setState({
      custom: state,
    });
  }

  toggleFade() {
    this.setState({ fadeIn: !this.state.fadeIn });
  }
  searchCenter(centerName) {
    if (centerName != '') {
      var isMatch = false
      var data = []
      for (var i = 0; i < this.tempCenters.length; i++) {
        if (this.tempCenters[i].serviceCenter.serviceCenterName.toLowerCase().includes(centerName.toLowerCase())) {
          data.push(this.tempCenters[i])
          this.setState({
            allCenters: data,
            totalCenters: data.length
          })
          isMatch = true
        }
        else {
          if (isMatch != true) {
            isMatch = false
          }
        }
      }
      if (isMatch == false) {
        this.setState({
          allCenters: [], totalCenters: 0
        })
      }
    }
    else {
      this.setState({
        allCenters: this.tempCenters
      })
    }
  }
  changeCenterSearch(centerName) {
    if (centerName == '') {
      this.setState({
        allCenters: this.tempCenters,
        totalCenters: this.tempCenters.length
      })
    }
    this.searchCenter(centerName)
    this.setState({
      searchCenterName: centerName
    })
  }
  sortBy(data) {
    this.sortByCenterName(data)
    this.setState({
      isSortBy: data
    })
  }
  sortByCenterName(data) {
    if (this.tempCenters.length != 0) {
      let sortedProductsAsc;
      if (data == 'ascending') {
        sortedProductsAsc = this.tempCenters.sort((a, b) => a.serviceCenter.serviceCenterName.localeCompare(b.serviceCenter.serviceCenterName))
      }
      else {
        sortedProductsAsc = this.tempCenters.sort((a, b) => b.serviceCenter.serviceCenterName.localeCompare(a.serviceCenter.serviceCenterName))
      }
      console.log(sortedProductsAsc)
      this.setState({
        allCenters: sortedProductsAsc
      })
    }
  }
  selectAll() {
    var temp = this.state.isAll
    if (this.tempCenters.length != 0) {
      for (var i = 0; i < this.tempCenters.length; i++) {
        if (temp == false) {
          this.tempCenters[i].serviceCenter['isChecked'] = true
          this.count = this.tempCenters.length
        }
        else {
          this.count = 0
          this.tempCenters[i].serviceCenter['isChecked'] = false
        }
      }
    }
    this.setState({
      isAll: !temp,
      allCenters: this.tempCenters,
    })
  }
  select(data) {
    for (var i = 0; i < this.tempCenters.length; i++) {
      if (data.serviceCenterId == this.tempCenters[i].serviceCenter.serviceCenterId) {
        if (this.tempCenters[i].serviceCenter['isChecked'] == true) {
          this.tempCenters[i].serviceCenter['isChecked'] = false
          this.setState({
            isAll: false
          })
          this.count = this.count - 1
          break;
        }
        else {
          this.count = this.count + 1
          this.tempCenters[i].serviceCenter['isChecked'] = true
        }
      }
    }
    if (this.count == this.tempCenters.length) {
      this.setState({
        isAll: true
      })
    }
    this.setState({ allCenters: this.tempCenters })
  }
  deleteServiceCenter(id) {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/delete', {
      method: 'POST',
      body: JSON.stringify({

        "id": id
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.applicationStatusCode == 0) {
          global.isCenterUpdated.next(true)
          this.getCenterList()
        }
        else {
          toaster.notify(responseJson.devMessage, {
            duration: 5000 // This notification will not automatically close
          });
        }
      })
  }
  render() {

    // const { allCenters, currentPage, centersPerPage } = this.state;

    // // Logic for displaying allCenters
    // const indexOfLastCenter = currentPage * centersPerPage;
    // const indexOfFirstCenter = indexOfLastCenter - centersPerPage;
    // const currentCenters = allCenters.slice(indexOfFirstCenter, indexOfLastCenter);

    // const renderCenters = currentCenters.map((center, index) => {
    //   return <li key={index}>{center}</li>;
    // });

    // // Logic for displaying page numbers
    // const pageNumbers = [];
    // for (let i = 1; i <= Math.ceil(allCenters.length / centersPerPage); i++) {
    //   pageNumbers.push(i);
    // }

    // const renderPageNumbers = pageNumbers.map(number => {
    //   return (
    //     <li
    //       key={number}
    //       id={number}
    //       onClick={this.handleClick}
    //     >
    //       {number}
    //     </li>
    //   );
    // });


    return (
      <div className="animated fadeIn">
        <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress: 0
          })}
        />
        <DefaultHeader />
        <Row className="mb-3 hidden">
          <Col sm="4">
            <h3>
              <ButtonDropdown id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                  &nbsp;
                </DropdownToggle>
                <DropdownMenu left="true">
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              <div className="checkbox form-check d-inline-block pl-0">
                <input type="checkbox" className="form-check-input" onChange={() => this.selectAll()} checked={this.state.isAll}></input>
                <label className="form-check-label form-check-label">{this.state.totalCenters}&nbsp;Centers/Accounts</label>
              </div>
            </h3>
          </Col>
          {this.state.isShowServiceCenterPrev == true && <Col sm="8" className="text-right">
            <div className="float-right">
              <button className="btn btn-outline-secondary ml-2" onClick={() => { this.setState({ isShowFilters: !this.state.isShowFilters }) }}>Filters <i className="icon2-filter2"></i></button>
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() => { this.setState({ isShowSortByMenu: !this.state.isShowSortByMenu }) }} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                  onClick={() => { this.setState({ isShowSortByMenu: false }) }}
                >
                  <div className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></div>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={() => this.sortBy('ascending')}><input type="radio" checked={this.state.isSortBy == 'ascending'} name="sort-order" onChange={() => { }} ></input> Ascending</DropdownItem>
                  <DropdownItem onClick={() => this.sortBy('descending')}><input type="radio" checked={this.state.isSortBy == 'descending'} name="sort-order" onChange={() => { }}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              {this.state.isShowServiceCenterEditPrev == true && <button className="btn btn-outline-warning ml-2" onClick={() => this.props.history.push('/register2')}><i className="icon2-addition-sign"></i></button>}
            </div>
            <div className="input-group float-right w-auto">
              <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                value={this.state.searchCenterName} onChange={(e) => this.changeCenterSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.searchCenter()
                  }
                }}
              >
              </input>
              <div className="input-group-append"><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>
          </Col>}
        </Row>
        {this.state.isShowServiceCenterPrev == true && <Table className="table-borderless table-sm">
          {this.state.allCenters.map((center, index) => (
            <tr key={index}>
              <td width="39" className="text-right pt-5">
                <div className="checkbox form-check mt-4">
                  <input type="checkbox" className="form-check-input" onChange={() => this.select(center.serviceCenter)} checked={center.serviceCenter.isChecked}></input>
                  <label className="form-check-label form-check-label"></label>
                </div>
              </td>
              <td>
                <Card className="card-simple">
                  <CardHeader>
                  </CardHeader>
                  <CardBody>
                    <Table className="table-borderless table-sm mb-0">
                      <tr >
                        <td className="pt-3" rowSpan="2" width="10%">
                          {center.serviceCenter.logoDataString != '' && <img className="img-fluid" src={"data:image/png;base64," + center.serviceCenter.logoDataString} />}
                          {center.serviceCenter.logoDataString == '' && <span className="badge badge-light font-2xl p-4 w-100 mt-1">&nbsp;</span>}
                        </td>
                        <td rowSpan="2" className="pt-4" width="27%">
                          <h4 className="mt-2">{center.serviceCenter.serviceCenterName}</h4>
                          <p><i className="icon2-location text-light-grey"></i> {center.serviceCenter.country}</p>
                        </td>
                        {center.serviceCenter.createdDate != null && <td width="20%">
                          <small className="text-light-grey">Created On</small><br></br>
                          {center.serviceCenter.createdDate}
                        </td>}
                        {center.serviceCenter.createdDate == null && <td width="20%">
                          <small className="text-light-grey">Created On</small><br></br>
                        Nil
                      </td>}
                        <td width="20%">
                          <small className="text-light-grey">Branches</small><br></br>
                          {center.branchCount} ({center.userCount} Users)
                      </td>
                        {center.issueCount != 0 && <td width="20%">
                          <small className="text-light-grey">Status</small><br></br>
                          <span className="text-danger">{center.issueCount} Issues</span> <i className="fa fa-exclamation-circle text-light-grey"></i>
                        </td>}
                        {center.issueCount == 0 && <td width="20%">
                          <small className="text-light-grey">Status</small><br></br>
                          <span className="text-success">No Issues</span>
                        </td>}
                        <td rowSpan="2" width="50" className="text-right">
                          <Link to="#" onClick={() => this.showMenu(center.serviceCenter.serviceCenterId)}><i className="f20 icon2-menu-options text-light-grey"></i></Link>
                          {center.isShowMenu == true &&
                            <Dropdown isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu: false }); }} >
                              <DropdownToggle
                                tag="span"
                                onClick={() => { this.setState({ isShowDropDownMenu: false }) }}
                                data-toggle="dropdown"
                              >
                              </DropdownToggle>
                              <DropdownMenu>
                                {this.state.isShowServiceCenterMenuPrev == true && <DropdownItem onClick={() => { global.centerid = center.serviceCenter.serviceCenterId; this.props.history.push('/centerProfile') }}>Details</DropdownItem>}
                                {this.state.isShowServiceCenterEditPrev == true && <DropdownItem onClick={() => this.deleteServiceCenter(center.serviceCenter.serviceCenterId)}>Delete</DropdownItem>}

                              </DropdownMenu>
                            </Dropdown>}
                        </td>
                      </tr>
                      <tr>
                        {center.planName == null && <td>
                          <small className="text-light-grey">Plan/Package</small><br></br>
                          Nil
                        </td>}
                        {center.planName != null && <td>
                          <small className="text-light-grey">Plan/Package</small><br></br>
                          {center.planName}
                        </td>}
                        {center.nextDueOn != null && <td>
                          <small className="text-light-grey">Next Payment Due On</small><br></br>
                          {center.nextDueOn}
                        </td>}
                        {center.nextDueOn == null && <td>
                          <small className="text-light-grey">Next Payment Due On</small><br></br>
                         Nil
                        </td>}
                        <td></td>

                      </tr>
                    </Table>
                  </CardBody>
                </Card>
              </td>
            </tr>
          ))}
        </Table>

        }
        {/* <div>
          <ul>
            {renderCenters}
          </ul>
          <ul id="page-numbers">
            {renderPageNumbers}
          </ul>
        </div> */}
        {/* <ul id="page-numbers">
            {renderPageNumbers}
          </ul>
          <ul>
            {renderCenters}
          </ul> */}
      </div>


    );

  }
}

export default Breadcrumbs;
