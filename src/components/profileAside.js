import React, { Component } from 'react';
import { Nav, NavItem, NavLink, Progress, TabContent, TabPane,Alert,Button,Label,Row,Col,Input,FormGroup, ListGroup, ListGroupItem,Modal, ModalBody, ModalFooter, ModalHeader, } from 'reactstrap';
import { AppAsideToggler, AppSidebarToggler } from '@coreui/react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { AppSwitch } from '@coreui/react'
import axios from "axios";
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import LoadingBar from 'react-top-loading-bar'

const NotificationAside = React.lazy(() => import('./notificationAside'));
const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
class ProfileAside extends Component {
  countries= [
    {
        "code": "+1",
        "name": "Canada"
    },
    {
        "code": "+1",
        "name": "United States"
    },
    {
        "code": "+1 242",
        "name": "Bahamas"
    },
    {
        "code": "+1 246",
        "name": "Barbados"
    },
    {
        "code": "+1 264",
        "name": "Anguilla"
    },
    {
        "code": "+1 268",
        "name": "Antigua and Barbuda"
    },
    {
        "code": "+1 268",
        "name": "Barbuda"
    },
    {
        "code": "+1 284",
        "name": "British Virgin Islands"
    },
    {
        "code": "+1 340",
        "name": "U.S. Virgin Islands"
    },
    {
        "code": "+1 441",
        "name": "Bermuda"
    },
    {
        "code": "+1 473",
        "name": "Grenada"
    },
    {
        "code": "+1 649",
        "name": "Turks and Caicos Islands"
    },
    {
        "code": "+1 670",
        "name": "Northern Mariana Islands"
    },
    {
        "code": "+1 671",
        "name": "Guam"
    },
    {
        "code": "+1 684",
        "name": "American Samoa"
    },
    {
        "code": "+1 767",
        "name": "Dominica"
    },
    {
        "code": "+1 787",
        "name": "Puerto Rico"
    },
    {
        "code": "+1 808",
        "name": "Midway Island"
    },
    {
        "code": "+1 808",
        "name": "Wake Island"
    },
    {
        "code": "+1 809",
        "name": "Dominican Republic"
    },
    {
        "code": "+1 868",
        "name": "Trinidad and Tobago"
    },
    {
        "code": "+1 869",
        "name": "Nevis"
    },
    {
        "code": "+1 876",
        "name": "Jamaica"
    },
    {
        "code": "+1664",
        "name": "Montserrat"
    },
    {
        "code": "+20",
        "name": "Egypt"
    },
    {
        "code": "+212",
        "name": "Morocco"
    },
    {
        "code": "+213",
        "name": "Algeria"
    },
    {
        "code": "+216",
        "name": "Tunisia"
    },
    {
        "code": "+218",
        "name": "Libya"
    },
    {
        "code": "+220",
        "name": "Gambia"
    },
    {
        "code": "+221",
        "name": "Senegal"
    },
    {
        "code": "+222",
        "name": "Mauritania"
    },
    {
        "code": "+223",
        "name": "Mali"
    },
    {
        "code": "+224",
        "name": "Guinea"
    },
    {
        "code": "+225",
        "name": "Ivory Coast"
    },
    {
        "code": "+226",
        "name": "Burkina Faso"
    },
    {
        "code": "+227",
        "name": "Niger"
    },
    {
        "code": "+228",
        "name": "Togo"
    },
    {
        "code": "+229",
        "name": "Benin"
    },
    {
        "code": "+230",
        "name": "Mauritius"
    },
    {
        "code": "+231",
        "name": "Liberia"
    },
    {
        "code": "+232",
        "name": "Sierra Leone"
    },
    {
        "code": "+233",
        "name": "Ghana"
    },
    {
        "code": "+234",
        "name": "Nigeria"
    },
    {
        "code": "+235",
        "name": "Chad"
    },
    {
        "code": "+236",
        "name": "Central African Republic"
    },
    {
        "code": "+237",
        "name": "Cameroon"
    },
    {
        "code": "+238",
        "name": "Cape Verde"
    },
    {
        "code": "+240",
        "name": "Equatorial Guinea"
    },
    {
        "code": "+241",
        "name": "Gabon"
    },
    {
        "code": "+242",
        "name": "Congo"
    },
    {
        "code": "+243",
        "name": "Congo, Dem. Rep. of (Zaire)"
    },
    {
        "code": "+244",
        "name": "Angola"
    },
    {
        "code": "+245",
        "name": "Guinea-Bissau"
    },
    {
        "code": "+246",
        "name": "British Indian Ocean Territory"
    },
    {
        "code": "+246",
        "name": "Diego Garcia"
    },
    {
        "code": "+247",
        "name": "Ascension"
    },
    {
        "code": "+248",
        "name": "Seychelles"
    },
    {
        "code": "+249",
        "name": "Sudan"
    },
    {
        "code": "+250",
        "name": "Rwanda"
    },
    {
        "code": "+251",
        "name": "Ethiopia"
    },
    {
        "code": "+253",
        "name": "Djibouti"
    },
    {
        "code": "+254",
        "name": "Kenya"
    },
    {
        "code": "+255",
        "name": "Tanzania"
    },
    {
        "code": "+255",
        "name": "Zanzibar"
    },
    {
        "code": "+256",
        "name": "Uganda"
    },
    {
        "code": "+257",
        "name": "Burundi"
    },
    {
        "code": "+260",
        "name": "Zambia"
    },
    {
        "code": "+261",
        "name": "Madagascar"
    },
    {
        "code": "+262",
        "name": "Mayotte"
    },
    {
        "code": "+262",
        "name": "Reunion"
    },
    {
        "code": "+263",
        "name": "Zimbabwe"
    },
    {
        "code": "+264",
        "name": "Namibia"
    },
    {
        "code": "+265",
        "name": "Malawi"
    },
    {
        "code": "+266",
        "name": "Lesotho"
    },
    {
        "code": "+267",
        "name": "Botswana"
    },
    {
        "code": "+268",
        "name": "Swaziland"
    },
    {
        "code": "+269",
        "name": "Comoros"
    },
    {
        "code": "+27",
        "name": "South Africa"
    },
    {
        "code": "+291",
        "name": "Eritrea"
    },
    {
        "code": "+297",
        "name": "Aruba"
    },
    {
        "code": "+298",
        "name": "Faroe Islands"
    },
    {
        "code": "+299",
        "name": "Greenland"
    },
    {
        "code": "+30",
        "name": "Greece"
    },
    {
        "code": "+31",
        "name": "Netherlands"
    },
    {
        "code": "+32",
        "name": "Belgium"
    },
    {
        "code": "+33",
        "name": "France"
    },
    {
        "code": "+34",
        "name": "Spain"
    },
    {
        "code": "+350",
        "name": "Gibraltar"
    },
    {
        "code": "+351",
        "name": "Portugal"
    },
    {
        "code": "+352",
        "name": "Luxembourg"
    },
    {
        "code": "+353",
        "name": "Ireland"
    },
    {
        "code": "+354",
        "name": "Iceland"
    },
    {
        "code": "+355",
        "name": "Albania"
    },
    {
        "code": "+356",
        "name": "Malta"
    },
    {
        "code": "+358",
        "name": "Finland"
    },
    {
        "code": "+359",
        "name": "Bulgaria"
    },
    {
        "code": "+36",
        "name": "Hungary"
    },
    {
        "code": "+370",
        "name": "Lithuania"
    },
    {
        "code": "+371",
        "name": "Latvia"
    },
    {
        "code": "+372",
        "name": "Estonia"
    },
    {
        "code": "+373",
        "name": "Moldova"
    },
    {
        "code": "+374",
        "name": "Armenia"
    },
    {
        "code": "+375",
        "name": "Belarus"
    },
    {
        "code": "+376",
        "name": "Andorra"
    },
    {
        "code": "+377",
        "name": "Monaco"
    },
    {
        "code": "+378",
        "name": "San Marino"
    },
    {
        "code": "+380",
        "name": "Ukraine"
    },
    {
        "code": "+381",
        "name": "Serbia"
    },
    {
        "code": "+382",
        "name": "Montenegro"
    },
    {
        "code": "+385",
        "name": "Croatia"
    },
    {
        "code": "+386",
        "name": "Slovenia"
    },
    {
        "code": "+387",
        "name": "Bosnia and Herzegovina"
    },
    {
        "code": "+389",
        "name": "Macedonia"
    },
    {
        "code": "+39",
        "name": "Italy"
    },
    {
        "code": "+40",
        "name": "Romania"
    },
    {
        "code": "+41",
        "name": "Switzerland"
    },
    {
        "code": "+420",
        "name": "Czech Republic"
    },
    {
        "code": "+421",
        "name": "Slovakia"
    },
    {
        "code": "+423",
        "name": "Liechtenstein"
    },
    {
        "code": "+43",
        "name": "Austria"
    },
    {
        "code": "+44",
        "name": "United Kingdom"
    },
    {
        "code": "+45",
        "name": "Denmark"
    },
    {
        "code": "+46",
        "name": "Sweden"
    },
    {
        "code": "+47",
        "name": "Norway"
    },
    {
        "code": "+48",
        "name": "Poland"
    },
    {
        "code": "+49",
        "name": "Germany"
    },
    {
        "code": "+500",
        "name": "Falkland Islands"
    },
    {
        "code": "+500",
        "name": "South Georgia and the South Sandwich Islands"
    },
    {
        "code": "+501",
        "name": "Belize"
    },
    {
        "code": "+502",
        "name": "Guatemala"
    },
    {
        "code": "+503",
        "name": "El Salvador"
    },
    {
        "code": "+504",
        "name": "Honduras"
    },
    {
        "code": "+505",
        "name": "Nicaragua"
    },
    {
        "code": "+506",
        "name": "Costa Rica"
    },
    {
        "code": "+507",
        "name": "Panama"
    },
    {
        "code": "+509",
        "name": "Haiti"
    },
    {
        "code": "+51",
        "name": "Peru"
    },
    {
        "code": "+52",
        "name": "Mexico"
    },
    {
        "code": "+53",
        "name": "Cuba"
    },
    {
        "code": "+537",
        "name": "Cyprus"
    },
    {
        "code": "+54",
        "name": "Argentina"
    },
    {
        "code": "+55",
        "name": "Brazil"
    },
    {
        "code": "+56",
        "name": "Chile"
    },
    {
        "code": "+56",
        "name": "Easter Island"
    },
    {
        "code": "+57",
        "name": "Colombia"
    },
    {
        "code": "+58",
        "name": "Venezuela"
    },
    {
        "code": "+590",
        "name": "Guadeloupe"
    },
    {
        "code": "+591",
        "name": "Bolivia"
    },
    {
        "code": "+593",
        "name": "Ecuador"
    },
    {
        "code": "+594",
        "name": "French Guiana"
    },
    {
        "code": "+595",
        "name": "Guyana"
    },
    {
        "code": "+595",
        "name": "Paraguay"
    },
    {
        "code": "+596",
        "name": "French Antilles"
    },
    {
        "code": "+596",
        "name": "Martinique"
    },
    {
        "code": "+597",
        "name": "Suriname"
    },
    {
        "code": "+598",
        "name": "Uruguay"
    },
    {
        "code": "+599",
        "name": "Curacao"
    },
    {
        "code": "+599",
        "name": "Netherlands Antilles"
    },
    {
        "code": "+60",
        "name": "Malaysia"
    },
    {
        "code": "+61",
        "name": "Australia"
    },
    {
        "code": "+61",
        "name": "Christmas Island"
    },
    {
        "code": "+61",
        "name": "Cocos-Keeling Islands"
    },
    {
        "code": "+62",
        "name": "Indonesia"
    },
    {
        "code": "+63",
        "name": "Philippines"
    },
    {
        "code": "+64",
        "name": "New Zealand"
    },
    {
        "code": "+65",
        "name": "Singapore"
    },
    {
        "code": "+66",
        "name": "Thailand"
    },
    {
        "code": "+670",
        "name": "East Timor"
    },
    {
        "code": "+670",
        "name": "Timor Leste"
    },
    {
        "code": "+672",
        "name": "Australian External Territories"
    },
    {
        "code": "+672",
        "name": "Norfolk Island"
    },
    {
        "code": "+673",
        "name": "Brunei"
    },
    {
        "code": "+674",
        "name": "Nauru"
    },
    {
        "code": "+675",
        "name": "Papua New Guinea"
    },
    {
        "code": "+676",
        "name": "Tonga"
    },
    {
        "code": "+677",
        "name": "Solomon Islands"
    },
    {
        "code": "+678",
        "name": "Vanuatu"
    },
    {
        "code": "+679",
        "name": "Fiji"
    },
    {
        "code": "+680",
        "name": "Palau"
    },
    {
        "code": "+681",
        "name": "Wallis and Futuna"
    },
    {
        "code": "+682",
        "name": "Cook Islands"
    },
    {
        "code": "+683",
        "name": "Niue"
    },
    {
        "code": "+685",
        "name": "Samoa"
    },
    {
        "code": "+686",
        "name": "Kiribati"
    },
    {
        "code": "+687",
        "name": "New Caledonia"
    },
    {
        "code": "+688",
        "name": "Tuvalu"
    },
    {
        "code": "+689",
        "name": "French Polynesia"
    },
    {
        "code": "+690",
        "name": "Tokelau"
    },
    {
        "code": "+691",
        "name": "Micronesia"
    },
    {
        "code": "+692",
        "name": "Marshall Islands"
    },
    {
        "code": "+7",
        "name": "Russia"
    },
    {
        "code": "+7 7",
        "name": "Kazakhstan"
    },
    {
        "code": "+7840",
        "name": "Abkhazia"
    },
    {
        "code": "+81",
        "name": "Japan"
    },
    {
        "code": "+82",
        "name": "South Korea"
    },
    {
        "code": "+84",
        "name": "Vietnam"
    },
    {
        "code": "+850",
        "name": "North Korea"
    },
    {
        "code": "+852",
        "name": "Hong Kong SAR China"
    },
    {
        "code": "+853",
        "name": "Macau SAR China"
    },
    {
        "code": "+855",
        "name": "Cambodia"
    },
    {
        "code": "+856",
        "name": "Laos"
    },
    {
        "code": "+86",
        "name": "China"
    },
    {
        "code": "+880",
        "name": "Bangladesh"
    },
    {
        "code": "+886",
        "name": "Taiwan"
    },
    {
        "code": "+90",
        "name": "Turkey"
    },
    {
        "code": "+91",
        "name": "India"
    },
    {
        "code": "+92",
        "name": "Pakistan"
    },
    {
        "code": "+93",
        "name": "Afghanistan"
    },
    {
        "code": "+94",
        "name": "Sri Lanka"
    },
    {
        "code": "+95",
        "name": "Myanmar"
    },
    {
        "code": "+960",
        "name": "Maldives"
    },
    {
        "code": "+961",
        "name": "Lebanon"
    },
    {
        "code": "+962",
        "name": "Jordan"
    },
    {
        "code": "+963",
        "name": "Syria"
    },
    {
        "code": "+964",
        "name": "Iraq"
    },
    {
        "code": "+965",
        "name": "Kuwait"
    },
    {
        "code": "+966",
        "name": "Saudi Arabia"
    },
    {
        "code": "+967",
        "name": "Yemen"
    },
    {
        "code": "+968",
        "name": "Oman"
    },
    {
        "code": "+970",
        "name": "Palestinian Territory"
    },
    {
        "code": "+971",
        "name": "United Arab Emirates"
    },
    {
        "code": "+972",
        "name": "Israel"
    },
    {
        "code": "+973",
        "name": "Bahrain"
    },
    {
        "code": "+974",
        "name": "Qatar"
    },
    {
        "code": "+975",
        "name": "Bhutan"
    },
    {
        "code": "+976",
        "name": "Mongolia"
    },
    {
        "code": "+977",
        "name": "Nepal"
    },
    {
        "code": "+98",
        "name": "Iran"
    },
    {
        "code": "+992",
        "name": "Tajikistan"
    },
    {
        "code": "+993",
        "name": "Turkmenistan"
    },
    {
        "code": "+994",
        "name": "Azerbaijan"
    },
    {
        "code": "+995",
        "name": "Georgia"
    },
    {
        "code": "+996",
        "name": "Kyrgyzstan"
    },
    {
        "code": "+998",
        "name": "Uzbekistan"
    }
]
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
    progress:0,
      profileDetails:{},
      isShow:true,
      activeTab: '1',
      roleType:'',
      userName:'',
      userImage:'',
      userRole:'',
      userPhoneNo:'',
      userEmail:'',
      userAddress:'',
      branchCount:0,
      agentCount:0,
      privilegeInfo:[],
      formValid:false,
      errorMsg:{},
      userName:'',userNameValid:false,
      email:'',userEmailValid:false,
      password:'',userPasswordValid:false,
      phone:'',phoneValid:false,
      isShowUpdateUserModel:false,
      phoneCode:'',
      showPasswordModel:false,
      oldpswd:'',
      newpswd:''
    };
  }

  toggle(tab) {
    var temp=!this.state.isShow
      this.setState({
        isShow:temp
      });
  }
  getServiceCenterProfile()
  {
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenterEmp/getProfile';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode==0)
      {
        var data=responseJson.data.user.roleName
        for(var i=0;i<responseJson.data.privileges.length;i++)
        {
            var key=responseJson.data.privileges[i].privilegeName
            var replaced = key.split('_').join(' ');
            responseJson.data.privileges[i].privilegeName=replaced
        }
        var newRole=data.split('_')
        this.setState({
          profileDetails:responseJson.data.user,
          userName:responseJson.data.user.name, 
          userRole:responseJson.data.user.roleName,
          userPhoneNo:responseJson.data.user.phoneNumber,
          userEmail:responseJson.data.user.email,
          userAddress:responseJson.data.address,
          branchCount:responseJson.data.branchCount,
          agentCount:responseJson.data.agentCount,
          privilegeInfo:responseJson.data.privileges})
          if(responseJson.data.user.profileImageUrl!='')
          {
            fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/fileData/get', {
                method: 'POST',
                body:responseJson.data.user.profileImageUrl,
                headers: {
                    'Authorization': `Bearer ${token}`
                },
              }).then(response => response.json()).then(responsejson => {
                if(responsejson.applicationStatusCode==0)
                {   global.profileimg=responsejson.applicationStatusResponse
                  this.setState({
                    userImage:responsejson.applicationStatusResponse
                  });
                  
                }
                else
                {
                  toaster.notify(responsejson.devMessage, {
                    duration:5000 // This notification will not automatically close
                  });
                }
              })
          }
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  getEmpProfile()
  {
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/taboorEmployee/getProfile';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode==0)
      {
        for(var i=0;i<responseJson.data.privileges.length;i++)
        {
            var key=responseJson.data.privileges[i].privilegeName
            var replaced = key.split('_').join(' ');
            responseJson.data.privileges[i].privilegeName=replaced
        }
        var data=responseJson.data.user.roleName
        var newRole=data.split('_')
        localStorage.setItem('userName',responseJson.data.user.name)
        this.setState({
          profileDetails:responseJson.data.user,
          userName:responseJson.data.user.name,
          userRole:responseJson.data.user.roleName,
          userPhoneNo:responseJson.data.user.phoneNumber,
          userEmail:responseJson.data.user.email,
          userAddress:responseJson.data.address,
          branchCount:responseJson.data.centerCount,
          agentCount:responseJson.data.userCount,
          privilegeInfo:responseJson.data.privileges})
          if(responseJson.data.user.profileImageUrl!='')
          {
            fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/fileData/get', {
                method: 'POST',
                body:responseJson.data.user.profileImageUrl,
                headers: {
                    'Authorization': `Bearer ${token}`
                },
              })
              .then(response => response.json())
              .then(responsejson => {
                if(responsejson.applicationStatusCode==0)
                {   global.profileimg=responsejson.applicationStatusResponse;
                    this.setState({
                        userImage:responsejson.applicationStatusResponse
                      })
                }
                else
                {
                  
                  toaster.notify(responsejson.devMessage, {
                    // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
                    duration:5000 // This notification will not automatically close
                  });
                  // NotificationManager.error(responsejson.devMessage, 'Error');
                }
              })
            
          }
         
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  componentDidMount()
  {
    var type=localStorage.getItem('role')
    if(type!=undefined) {
      if(type.startsWith('Taboor')) {
        this.setState({ roleType:'Super Admin' })
        this.getEmpProfile()
      }
      else {
        this.setState({ roleType:'Admin' })
        this.getServiceCenterProfile()
      }
    }
  }
  validateForm = () => {
    const {userNameValid,userEmailValid,phoneValid} = this.state;
    this.setState({
      formValid: userNameValid && userEmailValid && phoneValid
    })
  }
  updateUserName = (userName) => {
    this.setState({userName}, this.validateUserName)
  }
  validateUserName = () => {
    const {userName} = this.state;
    let userNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (userName.length == 0 ||userName=='' ) {
      userNameValid = false;
      errorMsg.userName = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(userName))
    // {
    //   userNameValid = false;
    //   errorMsg.userName =  'Only alphabets are allowed'
    // }
    this.setState({userNameValid, errorMsg}, this.validateForm)
  }
  updateEmail = (email) => {
    this.setState({email}, this.validateEmail)
  }
  validateEmail = () => {
    const {email} = this.state;
    let userEmailValid = true;
    let errorMsg = {...this.state.errorMsg}

    // checks for format _@_._
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){
      userEmailValid = false;
      errorMsg.email = 'Invalid email format'
    }

    this.setState({userEmailValid, errorMsg}, this.validateForm)
  }
  updatePhoneNo = (phone) => {
    this.setState({phone}, this.validatePhoneNo)
  }
  validatePhoneNo = () => {
    const {phone} = this.state;
    let phoneValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (phone.length == 0 || phone=='') {
      phoneValid = false;
      errorMsg.phone = 'Required'
    }
    else if (phone.length <8) {
      phoneValid = false;
      errorMsg.phone = 'Phone number must be 8 characters'
    }
    else if (!/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/.test(phone)){
      phoneValid = false;
      errorMsg.phone = 'Invalid phone format'
    }
    this.setState({phoneValid, errorMsg}, this.validateForm)
  }
  changePhoneNo=e=>
  {
    this.setState({phoneCode:parseInt(e.target.value)})
  }
  updateUserModel()
  {
    var tempPhoneCode=this.state.profileDetails.phoneNumber.split('-')
    this.updateUserName(this.state.profileDetails.name)
    this.updateEmail(this.state.profileDetails.email)
    this.updatePhoneNo(tempPhoneCode[1])
    this.setState({
      phoneCode:tempPhoneCode[0],
      isShowUpdateUserModel: true,
         
    });
  }
  resetUpdateModel()
  {
    this.setState({
      userName:'',email:'',phone:'',formValid:false
    })
    this.getAllBranches()
    this.getAllRoles()
  }
  cancelUpdateModel()
  {
    this.setState({
      userName:'',email:'',password:'',phone:'',
      isShowUpdateUserModel:false,formValid:false
    })
  }
  updateUser()
  {
      this.setState({
          progress:50,formValid:false
      })
    var fullPhoneNo=this.state.phoneCode +'-'+ this.state.phone
    if(this.state.roleType=='Admin')
    {
      let token = localStorage.getItem('sessionToken')
        fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenterEmp/update', {
          method: 'POST',
          body:JSON.stringify({
            'branchIds':[],
            "name": this.state.userName,
            "phoneNumber": fullPhoneNo,
            "roleId":-1,
            "userId":-1
          }),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          },
        })
        .then(response => response.json())
        .then(responseJson => {
          if(responseJson.applicationStatusCode==0)
          {
            this.setState({
                progress:100,formValid:true
            })
            this.getServiceCenterProfile()
            this.cancelUpdateModel()
            
          }
          else{
            this.setState({
                progress:100,formValid:true
            })
            toaster.notify(responseJson.devMessage, {
              // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
              duration:5000 // This notification will not automatically close
            });
          }
        
        })
    }
    else{
      
      let token = localStorage.getItem('sessionToken')
      fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/taboorEmployee/update', {
        method: 'POST',
        body:JSON.stringify({
          'branchIds':[],
          "name": this.state.userName,
          "phoneNumber": fullPhoneNo,
          "roleId":-1,
          "userId":-1
        }),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
      })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.applicationStatusCode==0)
        {
            this.setState({
                progress:100,formValid:true
            })
          this.getEmpProfile()
          this.cancelUpdateModel()
          
        }
        else{
            this.setState({
                progress:100,formValid:true
            })
          toaster.notify(responseJson.devMessage, {
            // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
            duration:5000 // This notification will not automatically close
          });
        }
      })
    }
  }
  onImageChange = event => {
    var  formData=new FormData();
    formData.append("file", event.target.files[0]);
    let token = localStorage.getItem('sessionToken')
    if (event.target.files && event.target.files[0]) {
      if(event.target.files[0].type!='image/png' && event.target.files[0].type!='image/jpeg' && event.target.files[0].type!='image/jpg') {
        toaster.notify('Upload image  only',{
          duration:5000
        })
      }
      else {
        if(event.target.files[0].size<=262144) {
            var myHeaders = new Headers();
            myHeaders.append("Authorization", "Bearer "+token);

            var formdata = new FormData();
            formdata.append("profileImage", event.target.files[0]);

            var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: formdata,
            redirect: 'follow'
            };

            fetch("https://apicall.taboor.ae/taboor-qms/usermanagement/user/updateProfilePicture", requestOptions)
            .then(response => response.json()).then(result => {
                if(result.applicationStatusCode==0) {
                    console.log(result);
                    if(this.state.roleType=='Admin'){
                    this.getServiceCenterProfile()
                    }
                    else{
                    this.getEmpProfile()
                    }
                    window.location.reload(false);
                }
                else{
                    toaster.notify(result.devMessage,{ duration:5000 });
                }
            })
            .catch(error => console.log('error', error));
        }
        else {
          toaster.notify('Size must be less than 256KB',{
            duration:5000
          })
        }
      }
     
    }
  }
  updatePassword=()=>{
    let token = localStorage.getItem('sessionToken');
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer "+token);
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
    "oldPassword": this.state.oldpswd,
    "newPassword": this.state.newpswd
    });

    var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
    };

    fetch("https://apicall.taboor.ae/taboor-qms/usermanagement/password/change", requestOptions).then(response => response.text()).then(result => {
        console.log(result);
        if(result.applicationStatusCode==-1) {
            toaster.notify(result.devMessage,{ duration:5000 });
        }
        else{
            window.location.reload(false);
        }
    })
    .catch(error => console.log('error', error));
  }
  render() {
    const { children, ...attributes } = this.props;
    return (
      <React.Fragment>
        <LoadingBar color='#2f49da' progress={this.state.progress} onLoaderFinished={() => this.setState({ progress:0 })} />
       <div className="profile-sidebar">

        
         <Nav tabs>
          <NavItem>
            <NavLink to="#" className={classNames({ active: this.state.isShow == true })}>
                <div className="text-light-grey">
                    Account 
                    <a className="float-right" onClick={()=>this.setState({showPasswordModel:true,oldpswd:'',newpswd:''})}>
                        <i className="icon2-lock_outline p-0" style={{fontSize:18,marginLeft:6}}></i>
                    </a>
                    <a className="float-right" onClick={()=>this.updateUserModel()}>
                        <i className="icon2-edit-pencil p-0"></i>
                    </a>
                <div className="clearfix"></div>
              </div>
            </NavLink>
          </NavItem>
        </Nav>
         <TabContent >
          <TabPane  className="p-3">
            <div className="text-center mb-2">
              <div className="profile-logo" style={{position:'relative'}}>
                 {global.profileimg!='' && <img className="img-fluid" src={"data:image/png;base64," +global.profileimg}/>} 
                 <label htmlFor='imgupload'>
                    <div style={{backgroundColor:"#2f49da",padding: 3,borderRadius:30,height:40,width:40,position:'absolute',bottom:0,right:0,cursor:'pointer'}} onClick={()=>{}}>
                        <input type="file" id="imgupload" style={{display:'none'}} onChange={this.onImageChange} accept="image/x-png,image/jpg,image/jpeg"/>
                        <img  src={require('../assets/img/cameraicon.png')} style={{height:20,width:20,objectFit:'contain'}}/>
                    </div>
                 </label>
              </div>
              <h5>{this.state.userName}</h5>
              <a href="#">{this.state.userRole}</a>
            </div>
            <div className="text-center mb-2">
            <p className="mb-1"><i className="icon2-phone-call text-warning"></i> {this.state.userPhoneNo}</p>
              <p className="mb-1"><i className="icon2-email-envelop text-warning"></i> {this.state.userEmail}</p>
              {this.state.roleType=='Admin' && <p><i className="icon2-location text-warning"></i> {this.state.userAddress}</p>}
            </div>

            <div className="card mb-2 card-line">
            <div className="card-body text-center p-2">   
            <div className="row">
              {this.state.roleType=='Admin' &&<div className="col-6">
                    <h2 className="text-warning font-weight-normal">{this.state.branchCount}</h2>
                    <p className="text-light-grey font-weight-normal"><i className="icon2-branches"></i> Branches</p>
              </div>}
              {this.state.roleType=='Super Admin' &&<div className="col-6">
                    <h2 className="text-warning font-weight-normal">{this.state.branchCount}</h2>
                    <p className="text-light-grey font-weight-normal"><i className="icon2-branches"></i> Center</p>
              </div>}
              {this.state.roleType=='Admin' && <div className="col-6">
                  <h2 className="text-warning font-weight-normal">{this.state.agentCount}</h2>
                  <p className="text-light-grey font-weight-normal"><i className="icon2-username"></i> Agents</p>
              </div>}
              {this.state.roleType=='Super Admin' && <div className="col-6">
                  <h2 className="text-warning font-weight-normal">{this.state.agentCount}</h2>
                  <p className="text-light-grey font-weight-normal"><i className="icon2-username"></i> User</p>
              </div>}
            </div>
            </div>
          </div>
            {global.isShowArabicLanguage==false && <h5>Privilages</h5>}
            {global.isShowArabicLanguage==true &&   <h5>الصلاحيات</h5>}
            <ul className="list-dots ml-2">
                {this.state.privilegeInfo.map((prev,index) => (
                    <li key={index}>
                        <a>{prev.privilegeName}</a>
                    </li>
                ))}
            </ul>
          </TabPane>
        </TabContent>
        
        
        <Modal isOpen={this.state.isShowUpdateUserModel} className={'modal-lg ' + this.props.className}>
            <ModalHeader>Update User</ModalHeader>
            <ModalBody>
                <FormGroup className="input-line">
                    <Label>Name</Label> 
                    <Input type="text" placeholder="Name"  value={this.state.userName} onChange={(e) => this.updateUserName(e.target.value)} />
                    <ValidationMessage valid={this.state.userNameValid} message={this.state.errorMsg.userName} />
                </FormGroup>
                <FormGroup className="input-line">
                    <Label>Email</Label> 
                    <Input type="email" placeholder="Email"  value={this.state.email} disabled />
                </FormGroup>
                <FormGroup className="input-line">
                    <Label>Phone Number</Label> 
                    <Row>
                        <Col sm="3">
                        <select className="form-control" onChange={this.changePhoneNo} value={this.state.phoneCode}>
                        {this.countries.map((country,index) => (
                            <option key={index} value={country.code}>{country.code}</option>
                        ))}
                        </select>
                        </Col>
                        <Col sm="9">
                        <Input type="text" placeholder="Phone" 
                        value={this.state.phone} onChange={(e) => this.updatePhoneNo(e.target.value)} />
                        < ValidationMessage valid={this.state.phoneValid} message={this.state.errorMsg.phone} />
                        </Col>
                    </Row>
                </FormGroup>



                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.cancelUpdateModel()}>Cancel</Button>
                    <Button color="primary"  disabled={!this.state.formValid} onClick={()=>this.updateUser()}>Add</Button>
                  </ModalFooter>
                </Modal>
        

        <Modal isOpen={this.state.showPasswordModel} className={'modal-lg ' + this.props.className}>
        <ModalHeader>Change Password</ModalHeader>
        <ModalBody>
            <FormGroup className="input-line">
                <Label>Old Password</Label> 
                <Input type="password" placeholder="Old Password" value={this.state.oldpswd} onChange={(e) => this.setState({oldpswd:e.target.value})} />
            </FormGroup>
            <FormGroup className="input-line">
                <Label>New Password</Label> 
                <Input type="password" placeholder="New Password" value={this.state.newpswd}  onChange={(e) => this.setState({newpswd:e.target.value})} />
            </FormGroup>
        </ModalBody>
        <ModalFooter>
            <Button outline color="primary" onClick={()=>{this.setState({showPasswordModel:false,oldpswd:'',newpswd:''})}}>Cancel</Button>
            <Button color="primary" onClick={()=>{this.updatePassword();}}>Change</Button>
        </ModalFooter>
        </Modal>
        
        </div>
        
      </React.Fragment>
    );
  }
}

ProfileAside.propTypes = propTypes;
ProfileAside.defaultProps = defaultProps;

export default ProfileAside;
