import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { Button, Card, CardBody, CardGroup, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, 
  Row, Table, Label, FormGroup, Nav, NavItem, NavLink, TabContent, TabPane,ButtonDropdown, DropdownItem,  DropdownMenu,  DropdownToggle, Alert } from 'reactstrap';
import { AppNavbarBrand } from '@coreui/react';
import logo from '../assets/img/brand/logo.png';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';
import Steps, { Step } from 'rc-steps';
import { AppSwitch } from '@coreui/react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Route, Redirect } from 'react-router-dom';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
var formData=new FormData()
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
class Register extends Component {
  userServiceCenterId=0
  tempPayment=[]
  paymentType=[{
    'name':'Annual',id:1
  },{
    'name':'Montly',id:2
  }]
  constructor(props){
    super(props);
    this.state={
      isError:false,
      serviceCenterName: '',serviceCenterNameValid: false,
      serviceCenterArabicName: '',serviceCenterArabicNameValid: false,
      selectedServiceSectorId:0,
      selectedPlanId:0,
      paymentAmount:0,
      isSuccess:false,
      selectedPlanObject:{},
      selectedServiceObject:{},
      selectedPlanBool:false,
      email: '', emailValid: false,
      password: '', passwordValid: false,
      country: '', countryValid: false,
      phone: '', phoneValid: false,
      cardValid:false,cardNo:'',
      cvvNoValid:false,cvvNo:'',
      dateValue:new Date(),
      cardNameValid:false,cardName:'',
      passwordConfirm: '', passwordConfirmValid: false,
      formValid: false,
      paymentTypeId:0,
      paymentFormValid: false,
      errorMsg: {},
      paymentErrorMsg: {},
      serviceSector:[],
      paymentPlanList:[],
      next:0,
      image: null
    }
  }
  setStartDate(date)
  {
     this.setState({dateValue:date})
  }
  validateForm = () => {
    // const {serviceCenterNameValid, emailValid, passwordValid, passwordConfirmValid,countryValid,phoneValid} = this.state;
    // this.setState({
    //   formValid: serviceCenterNameValid && emailValid && passwordValid && passwordConfirmValid && phoneValid && countryValid
    // })
    const {serviceCenterNameValid, emailValid,countryValid,phoneValid,serviceCenterArabicNameValid,passwordValid,passwordConfirmValid} = this.state;
    this.setState({
      formValid: serviceCenterNameValid && emailValid && phoneValid && countryValid && serviceCenterArabicNameValid && passwordValid && passwordConfirmValid
    })
  }

  validatePaymentForm = () => {
    const {cardNo,cvvNo,cardName} = this.state;
    this.setState({
      paymentFormValid: cardNo  && cvvNo && cardName
    })
  }
  updateServiceCenterName = (serviceCenterName) => {
    this.setState({serviceCenterName}, this.validateServiceCenterName)
  }

  validateServiceCenterName = () => {
    const {serviceCenterName} = this.state;
    let serviceCenterNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (serviceCenterName.length == 0 ||serviceCenterName=='' ) {
      serviceCenterNameValid = false;
      errorMsg.serviceCenterName = 'Required'
    }
    else if(!/^[a-zA-Z\s]*$/.test(serviceCenterName))
    {
      serviceCenterNameValid = false;
      errorMsg.serviceCenterName = 'Only alphabets are allowed'
    }
    this.setState({serviceCenterNameValid, errorMsg}, this.validateForm)
  }
  updateArabicServiceCenterName = (serviceCenterArabicName) => {
    this.setState({serviceCenterArabicName}, this.validateArabicServiceCenterName)
  }

  validateArabicServiceCenterName = () => {
    const {serviceCenterArabicName} = this.state;
    let serviceCenterArabicNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (serviceCenterArabicName.length == 0 ||serviceCenterArabicName=='' ) {
      serviceCenterArabicNameValid = false;
      errorMsg.serviceCenterArabicName = 'Required'
    }
    else if(!/^[\u0621-\u064A\040]+$/.test(serviceCenterArabicName))
    {
      serviceCenterArabicNameValid = false;
      errorMsg.serviceCenterArabicName = 'Only arabic alphabets are allowed'
    }
    this.setState({serviceCenterArabicNameValid, errorMsg}, this.validateForm)
  }
  updateCardName = (cardName) => {
    this.setState({cardName}, this.validateCardName)
  }

  validateCardName = () => {
    const {cardName} = this.state;
    let cardNameValid = true;
    let paymentErrorMsg = {...this.state.paymentErrorMsg}

    if (cardName.length == 0 ||cardName=='' ) {
      cardNameValid = false;
      paymentErrorMsg.cardName = 'Required'
    }
    else if(!/^[a-zA-Z\s]*$/.test(cardName))
    {
      cardNameValid = false;
      paymentErrorMsg.cardName = 'Only alphabets are allowed'
    }
    this.setState({cardNameValid, paymentErrorMsg}, this.validatePaymentForm)
  }
  updateCountryName = (country) => {
    this.setState({country}, this.validateCountryName)
  }

  validateCountryName = () => {
    const {country} = this.state;
    let countryValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (country.length == 0 ||country=='') {
      countryValid = false;
      errorMsg.country = 'Required'
    }
    else if(!/^[a-zA-Z\s]*$/.test(country))
    {
      countryValid = false;
      errorMsg.country = 'Only alphabets are allowed'
    }
    this.setState({countryValid, errorMsg}, this.validateForm)
  }

  updatePhoneNo = (phone) => {
    this.setState({phone}, this.validatePhoneNo)
  }

  validatePhoneNo = () => {
    const {phone} = this.state;
    let phoneValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (phone.length == 0 || phone=='') {
      phoneValid = false;
      errorMsg.phone = 'Required'
    }
     else if (phone.length <8) {
      phoneValid = false;
      errorMsg.phone = 'Phone number must be 8 characters'
    }
    else if (!/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/.test(phone)){
      phoneValid = false;
      errorMsg.phone = 'Invalid phone format'
    }
    this.setState({phoneValid, errorMsg}, this.validateForm)
  }
  updateCardNo = (cardNo) => {
    this.setState({cardNo}, this.validateCardNo)
  }
  validateCardNo = () => {
    const {cardNo} = this.state;
    let cardValid = true;
    let paymentErrorMsg = {...this.state.paymentErrorMsg}

    if (cardNo.length == 0 || cardNo=='') {
      cardValid = false;
      paymentErrorMsg.cardNo = 'Required'
    }
     else if (cardNo.length <16) {
      cardValid = false;
      paymentErrorMsg.cardNo = 'Card number must be 16 characters'
    }
    else if (!/[0-9]/.test(cardNo)){
      cardValid = false;
      paymentErrorMsg.cardNo = 'Must be a number'
    }
    this.setState({cardValid, paymentErrorMsg}, this.validatePaymentForm)
  }
  updateCvvCardNo = (cvvNo) => {
    this.setState({cvvNo}, this.validateCvvCardNo)
  }
  validateCvvCardNo = () => {
    const {cvvNo} = this.state;
    let cvvNoValid = true;
    let paymentErrorMsg = {...this.state.paymentErrorMsg}

    if (cvvNo.length == 0 || cvvNo=='') {
      cvvNoValid = false;
      paymentErrorMsg.cvvNo = 'Required'
    }
     else if (cvvNo.length < 3) {
      cvvNoValid = false;
      paymentErrorMsg.cvvNo = 'CVV number must be 3 characters'
    }
    else if (!/[0-9]/.test(cvvNo)){
      cvvNoValid = false;
      paymentErrorMsg.cvvNo = 'Must be a number'
    }
    this.setState({cvvNoValid, paymentErrorMsg}, this.validatePaymentForm)
  }
  updateEmail = (email) => {
    this.setState({email}, this.validateEmail)
  }

  validateEmail = () => {
    const {email} = this.state;
    let emailValid = true;
    let errorMsg = {...this.state.errorMsg}

    // checks for format _@_._
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){
      emailValid = false;
      errorMsg.email = 'Invalid email format'
    }

    this.setState({emailValid, errorMsg}, this.validateForm)
  }

  updatePassword = (password) => {
    this.setState({password}, this.validatePassword);
  }

  validatePassword = () => {
    const {password} = this.state;
    let passwordValid = true;
    let errorMsg = {...this.state.errorMsg}

    // must be 6 chars
    // must contain a number
    // must contain a special character

    if (password.length < 6) {
      passwordValid = false;
      errorMsg.password = 'Password must be at least 6 characters long';
    } else if (!/\d/.test(password)){
      passwordValid = false;
      errorMsg.password = 'Password must contain a digit';
    } else if (!/[!@#$%^&*]/.test(password)){
      passwordValid = false;
      errorMsg.password = 'Password must contain special character: !@#$%^&*';
    }

    this.setState({passwordValid, errorMsg}, this.validateForm);
  }

  updatePasswordConfirm = (passwordConfirm) => {
    this.setState({passwordConfirm}, this.validatePasswordConfirm)
  }
  changePaymentType=e=>
  {
     this.setState({paymentTypeId:parseInt(e.target.value)})
     if(parseInt(e.target.value)==1)
     {
       this.setState({paymentAmount:this.state.selectedPlanObject.priceYearly})
     }
     else if(parseInt(e.target.value)==2)
     {
      this.setState({paymentAmount:this.state.selectedPlanObject.totalPriceMonthly})
     }
  }
  validatePasswordConfirm = () => {
    const {passwordConfirm, password} = this.state;
    let passwordConfirmValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (password !== passwordConfirm) {
      passwordConfirmValid = false;
      errorMsg.passwordConfirm = 'Passwords do not match'
    }

    this.setState({passwordConfirmValid, errorMsg}, this.validateForm);
  }
  nextStep=(next)=>{
    if(this.state.formValid)
    {
      if(next==0)
      {
        formData=new FormData()
      }
      if(next==1)
      {
        this.getAllPaymentPlan()
        this.setState({next:next});
      }
      if(next==2)
      {
        this.registerServiceCenter()
      }
     
      
    }
    if(this.state.paymentFormValid)
    {
      if(next==3)
      {
        this.addPayment()
      }
    }
    
  }
  addPayment()
  {
    var expiryMonth=this.state.dateValue.getMonth()+1
    expiryMonth=expiryMonth<10 ?'0' + expiryMonth : expiryMonth;
    var expiryYear=this.state.dateValue.getFullYear()
    var expiryDate=expiryMonth+'/'+expiryYear
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/bankCard/add', {
      method: 'POST',
      body:JSON.stringify({
        "cardCvv": this.state.cvvNo,
        "cardNumber": this.state.cardNo,
        "cardTitle": this.state.cardName,
        "cardType": "Debit",
        "expiryDate": expiryDate,    
        "paymentMethodId": 2,
        "serviceCenterId": this.userServiceCenterId

      }),
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(response => response.json())
    .then(responsejson => {
      if(responsejson.applicationStatusCode==0)
      {
        fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/subscription/pay', {
          method: 'POST',
          body:JSON.stringify({
          
            "paymentType": this.state.paymentTypeId,
            "serviceCenterId": this.userServiceCenterId
        
          }),
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(response => response.json())
        .then(responsejson => {
          if(responsejson.applicationStatusCode==0)
          {

            this.setState({next:4,isSuccess:true})
           
          }
        })
       
      }
      else{
        toaster.notify(responsejson.devMessage, {
          position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration: null // This notification will not automatically close
        });
      }
     
    })
  }
  changeServiceSector= e =>{
    for(var i=0;i<this.state.serviceSector.length;i++)
    {
      if(this.state.serviceSector[i].serviceSectorId==parseInt(e.target.value))
      {
        this.setState({selectedServiceObject:this.state.serviceSector[i]})
      }
    }
    this.setState({selectedServiceSectorId:parseInt(e.target.value)})
  }
  componentDidMount() {
    formData=new FormData()
    this.getAllServiceSector()

 }
 getAllServiceSector()
 {
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceSector/get/all', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      this.setState({selectedServiceObject:responseJson.serviceSectors[0],serviceSector:responseJson.serviceSectors,selectedServiceSectorId:responseJson.serviceSectors[0].serviceSectorId})
    }
  })
 }
 getAllPaymentPlan()
 {
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/paymentPlan/get/all', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      for(var i=0;i<responseJson.paymentPlanList.length;i++)
      {
        responseJson.paymentPlanList[i]['isSelected']=false
      }
      this.tempPayment=responseJson.paymentPlanList
      this.setState({paymentPlanList:responseJson.paymentPlanList})
    }
  })
 }
 registerServiceCenter()
 {
  formData.append('RegisterServiceCenterPayload',JSON.stringify({
    "arabicName": this.state.serviceCenterArabicName,
    "country": this.state.country,
    "email": this.state.email,
    'name':this.state.serviceCenterName,
    "password": this.state.password,
    "paymentPlanId": this.state.selectedPlanId,
    "phoneNumber": this.state.phone,
    "serviceSector": {
        "serviceSectorId": this.state.selectedServiceSectorId,
        "serviceSectorName": this.state.selectedServiceObject.serviceSectorName
    }
  }))
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/register', {
    method: 'POST',
    body:formData,
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      this.setState({next:2});
      this.userServiceCenterId=parseInt(responseJson.values.ServiceCenterId)
      fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getPaymentPlan', {
        method: 'POST',
        body:JSON.stringify({
          'id':parseInt(responseJson.values.ServiceCenterId)
        }),
        headers: {
          'Content-Type': 'application/json'
        },
      })
      .then(response => response.json())
      .then(responsejson => {
        if(responsejson.applicationStatusCode==0)
        {
          this.setState({selectedPlanObject:responsejson.paymentPlanList[0],paymentAmount:responsejson.paymentPlanList[0].priceYearly});
         
        }
        else{
          toaster.notify(responsejson.devMessage, {
            position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
            duration: null // This notification will not automatically close
          });
        }
      })
    }
    else{
      toaster.notify(responseJson.devMessage, {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration: null // This notification will not automatically close
      });
    }
   
  })
 }
 onImageChange = event => {
  if (event.target.files && event.target.files[0]) {
    let img = event.target.files[0];
    formData.append("ServiceCenterProfileImage", event.target.files[0]);
    this.setState({
      image: URL.createObjectURL(img)
    });
  }
}
resetImage()
{
  this.setState({
    image: ''
  });
}
selectedPlan=(data)=>{
 
  
  for(var i=0;i<this.tempPayment.length;i++)
  {
    if(this.tempPayment[i].paymentPlanId==data.paymentPlanId)
    {
      if(this.tempPayment[i]['isSelected']==false)
      {}
      this.tempPayment[i]['isSelected']=true
      
    }
    else{
      this.tempPayment[i]['isSelected']=false
    }

  }
  this.setState({selectedPlanId:data.paymentPlanId,selectedPlanBool:true,paymentPlanList:this.tempPayment})
}
  render() {
    return (
      <div className={"app align-items-center register-page "+ (this.state.isSuccess ? 'success-page' : '')}>
        <Container fluid className="p-0">
        <Row>
          
          <Col sm="9">
          {(this.state.next==0) && (
            <Card className="register-card m-0">
              <CardBody className="p-5"> <Form>
                <Row className="mb-3">
                  <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                  <Col sm="6" className="text-right">  
                    <h6 className="text-light-grey">Already have an account? <Link to="/login">Login Here</Link></h6> 
                  </Col>
                </Row> 


                
                <Row>
                   
                  <Col sm="6" className="text-center">
                    
                    <Card className="mr-5">
                      <CardBody>
                      <div className="plan-info">
                        <h2><i className="icon2-diamond-shape text-warning"></i></h2>
                          <h4 className="mb-4 font-weight-bold"> {this.state.selectedPlanObject.paymentPlanName}</h4>
                          <h2 className="font-5xl font-weight-normal text-primary">{this.state.selectedPlanObject.priceYearly}K $</h2>
                          <h6 className="text-warning mb-4">Per Year</h6>
                          <div className="text-light-grey">
                            <p>1 month free Demo</p>
                            <p>{this.state.selectedPlanObject.supportedNoOfUsers} #Users</p>
                            <p>{this.state.selectedPlanObject.supportedNoOfBranches} #Branches</p>
                            <p>Easy Payment</p>
                            <p>{this.state.selectedPlanObject.supoortDurationInMonths} month Support</p>
                          </div>
                      </div>
                      </CardBody>
                    </Card>

                  </Col>
                  <Col sm="6"> 


                  <Card  className="ml-5">
      
                  <CardBody>
                    <h5 className="mb-3"><i className="icon2-order-summary"></i> Order Summary</h5>
                    <h6>Payment Method</h6>
                    <Table responsive className="table table-borderless table-sm font-sm">
                      <thead className="b-b-1">
                        <tr>
                          <th colSpan="2" className="pb-3">
                            <small className="text-success">Recently Used</small><br></br>
                            Visa XXXX 4250
                          </th>
                          <th className="text-right pb-3"><Link className="text-warning">Change</Link></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td width="35%">
                            <h6 className="mb-0">Branches</h6>
                            <FormGroup className="input-line">
                              <input className="form-control" type="number"></input>
                            </FormGroup>
                          </td>
                          <td>X AED 29</td>
                          <td>AED 29.00</td>
                        </tr>
                        <tr>
                          <td>
                            <h6 className="mb-0">Users</h6>
                            <FormGroup className="input-line">
                              <input className="form-control" type="number"></input>
                            </FormGroup>
                          </td>
                          <td>X AED 29</td>
                          <td>AED 29.00</td>
                        </tr>
                        <tr>
                          <td>
                            <h6 className="mb-0">SMSs</h6>
                            <FormGroup className="input-line">
                              <input className="form-control" type="number"></input>
                            </FormGroup>
                          </td>
                          <td>X AED 0.07</td>
                          <td>AED 70</td>
                        </tr>
                      </tbody>
                      <tfoot className="b-t-1">
                        <tr>
                          <td className="pt-3" colSpan="2"><b>Order Total</b></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td colSpan="2">Total before VAT (5%)</td>
                          <td>AED 640</td>
                        </tr>
                        <tr>
                          <td colSpan="2"><b>Total</b></td>
                          <td><b>AED 768</b></td>
                        </tr>
                      </tfoot>
                    </Table>
                    <Button block color="warning">Checkout</Button>
                  </CardBody>
                </Card>
                

                    
                  
                   </Col>
                </Row> 

                
                <p className="text-light-grey">I confirm I have read and accepted the <Link>Terms and Conditions of Use</Link> By clicking on "<span className="text-warning">Pay</span>" you agree to immediately access the service and to waive any right of withdrawal. You may terminate your subscription at any time by contact Taboor Service Center. The termination will be applied at the end of the current subscription period.</p>

              </Form> </CardBody>
              <CardFooter className="p-4 text-right b-a-0">
                <Button color="primary"  disabled={!this.state.formValid} onClick={()=>{this.nextStep(1)}}>Pay</Button>
                {/* <Button color="primary" type='submit' disabled={!this.state.formValid}>Next</Button> */}
              </CardFooter>
            </Card>
             )}
          

          {(this.state.next==1) && (
            
              <Card className="register-card m-0">
              <CardBody className="p-5"> <Form>
                <Row className="mb-5">
                  <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                  <Col sm="6" className="text-right">  
                    <h6 className="text-light-grey">Already have an account? <Link to="/login">Login Here</Link></h6> 
                  </Col>
                </Row> 
                
                
               { (this.state.paymentPlanList!=undefined) && (
                  this.state.paymentPlanList.map(paymentPlan => (
                    <div onClick={() => this.selectedPlan(paymentPlan)} >
                        <Card className={paymentPlan.isSelected? "active-plan" : 'card'}>
                          <CardBody className="p-4">
                            <Table className="m-0 table-borderless table-sm">
                              <tr>
                                <td rowSpan="4">
                                  <h4 className="font-weight-bold mb-4"><i className="icon2-diamond-shape text-warning"></i> {paymentPlan.paymentPlanName}</h4>
                                  <h2 className="font-weight-normal">{paymentPlan.priceYearly}K $ <sup className="badge badge-danger badge-pill bg-pink font-sm pb-3 pt-3 text-white">-37%</sup>  <small className="text-primary font-lg">/month /branch</small></h2>
                                  <h6 className="text-warning"><span>Annualy</span>  <AppSwitch className={'mx-1 plan-switch'} variant={'3d'} color={'success'} defaultChecked />  <span>Monthly</span></h6>
                                </td>
                                
                                <td>#Users</td>
                                <td>#Branches</td>
                              </tr>
                              <tr>
                                
                                <th>{paymentPlan.supportedNoOfUsers} Users / Branch</th>
                                <th>Up to {paymentPlan.supportedNoOfBranches} Branches</th>
                              </tr>
                              <tr> 
                                <td>
                                  Support & Reporting
                                </td>
                                <td>Free SMS Quota</td>
                              </tr>
                              <tr>
                                <th>Basic . <Link>Detail</Link></th>
                                <th>100 SMSs</th>
                              </tr>
                            
                            </Table>
                          </CardBody>
                        </Card>
                    </div>
                    
                  )))}
              </Form>
              </CardBody>
              <CardFooter className="p-4 text-right b-a-0">
                <Button outline color="primary" onClick={()=>{this.nextStep(0)}}>Back</Button>
                <Button color="primary" className="ml-2" disabled={!this.state.selectedPlanBool} onClick={()=>{this.nextStep(2)}}>Next</Button>
              </CardFooter>
            </Card>
          )}
          

          {(this.state.next==2) && (
            <Card className="register-card m-0">
              <CardBody className="p-5"> <Form>
                <Row className="mb-3">
                  <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                  <Col sm="6" className="text-right">  
                    <h6 className="text-light-grey">Already have an account? <Link to="/login">Login Here</Link></h6> 
                  </Col>
                </Row> 
                
                <Card className="mb-5 mt-3">
                  <CardBody>
                      <Table responsive className="table-borderless">
                        <tr>
                          <th></th>
                          <th width="25%" className="text-primary">Due Date</th>
                          <th className="text-primary">Amount <span className="text-warning">%</span></th>
                          <th className="text-primary">Grace Period <span className="text-warning">Days</span></th>
                          <th className="text-primary">Amount <ButtonDropdown id="card1" isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                            <DropdownToggle caret className="p-0 mb-0 text-warning" color="transparent">
                            AED
                            </DropdownToggle>
                            <DropdownMenu right>
                              <DropdownItem>Action</DropdownItem>
                              <DropdownItem>Another action</DropdownItem>
                              <DropdownItem disabled>Disabled action</DropdownItem>
                              <DropdownItem>Something else here</DropdownItem>
                            </DropdownMenu>
                          </ButtonDropdown>
                          </th>
                        </tr>
                        <tr>
                          <td className="text-warning">#1</td>
                          <td>
                          <FormGroup className="input-line">
                            <select className="form-control">
                              <option>Set Date</option>
                            </select>
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                            <Input type="text" autoComplete="username"/>
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                            <Input type="text" autoComplete="username"/>
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                            <Input type="text" autoComplete="username"/>
                          </FormGroup>
                          </td>
                        </tr>
                        <tr>
                          <td className="text-warning">#2</td>
                          <td>
                          <FormGroup className="input-line">
                            <select className="form-control">
                              <option>Set Date</option>
                            </select>
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                            <Input type="text" autoComplete="username"/>
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                            <Input type="text" autoComplete="username"/>
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                            <Input type="text" autoComplete="username"/>
                          </FormGroup>
                          </td>
                        </tr>
                        <tr>
                          <td className="text-warning">#3</td>
                          <td>
                          <FormGroup className="input-line">
                            <select className="form-control">
                              <option>Set Date</option>
                            </select>
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                            <Input type="text" autoComplete="username"/>
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                            <Input type="text" autoComplete="username"/>
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                            <Input type="text" autoComplete="username"/>
                          </FormGroup>
                          </td>
                        </tr>
                       
                      </Table>
                  </CardBody>
                </Card>

                <Row>
                    <Col sm="4">
                      <FormGroup className="input-line">
                        <Label className="text-light-grey">Payment</Label>
                        <select className="form-control" onChange={this.changePaymentType}>
                        {this.paymentType.map(payment => (
                      <option value={payment.id} key={payment.id}>
                        {payment.name}
                      </option>
                    ))}
                        </select>
                      </FormGroup>
                  </Col>
                  <Col sm="8" className="text-right">
                      <h6 className="text-success">Amount {this.state.paymentAmount} $</h6>
                    </Col>
                  </Row>


              </Form> </CardBody>
              <CardFooter className="p-4 text-right b-a-0">
              <Button outline color="primary" onClick={()=>{this.nextStep(1)}}>Back</Button>
                <Button color="primary" className="ml-2" disabled={!this.state.paymentFormValid} onClick={()=>{this.nextStep(3)}}>Create</Button>
              </CardFooter>
            </Card>
          )}
          {(this.state.next==4) && (
              <Card className="register-card m-0"  style={{height:'100vh'}}>
                <CardBody className="p-5"> <Form>
  
                  <div className="text-center">
                    <h2 className="text-primary mb-5">Registration Completed</h2>
  
                    <div className="icon-success">
                      <i className="fa-5x icon-check"></i>
                    </div>
                    <div className="mb-5">
                      <h1>Great!</h1>
                    </div>
  
                    <h3 className="mb-5 mt-5">You're now ready to use Taboor</h3>
                    <p className="mb-5">A message with your username and password & copy of your invoice <br></br>
                    was sent to your registered email<br></br>
                    Follow the instructions and start using Taboor</p>
                    <Link to="/login">
                        <Button color="primary">Back To Login</Button>
                    </Link>
                    
  
                    </div> 
  
  
                
                </Form> 
                </CardBody>
                
              </Card>
          )}

            </Col>

          <Col sm="3">
            <div className="p-5">
              <div className="login-logo mb-lg-4 mb-md-2 mb-sm-1 mb-xl-5">
                <AppNavbarBrand className="img-fluid"  full={{ src: logo, width: 250, alt: 'Taboor Logo' }} />
              </div>
              <Steps current={this.state.next}  direction="vertical">
                <Steps.Step title="Basic" description="Lorem ipsum dolor sit amet consectetur adipiscing elit, cupidatat non proident." />
                <Steps.Step title="Plan" description="Lorem ipsum dolor sit amet consectetur adipiscing elit, cupidatat non proident." />
                <Steps.Step title="Payment" description="Lorem ipsum dolor sit amet consectetur adipiscing elit, cupidatat non proident." />
              </Steps>
            </div>
          </Col>

        </Row>
        </Container>
      </div>
    );
  }
}

export default Register;

