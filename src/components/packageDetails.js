import React, { Component } from 'react';
import { Badge, Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table, ButtonGroup, ButtonDropdown, 
  DropdownItem,  DropdownMenu,  DropdownToggle,ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText,Formgroup, Input, Label, FormGroup } from 'reactstrap';

import { Link, NavLink } from 'react-router-dom';
import * as router from 'react-router-dom';
import { UncontrolledDropdown, Nav, NavItem } from 'reactstrap';
import {
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import {Line, Pie } from 'react-chartjs-2';
import { AppSwitch } from '@coreui/react'
import routes from './routes';
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
const line = {
  labels: ['May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
  datasets: [
    {
      label: 'Performance',
      Color: [
        'rgb(108, 135, 254)'
      ],
      labels: {
        fontColor: "rgb(255, 99, 132)"
      },
      line: {
        borderColor: '#F85F73',
       },
      fill: true,
      lineTension: 0.1,
      backgroundColor: 'rgba(255, 255, 255, 0.1)',
      borderColor: 'rgb(108, 135, 254)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgb(108, 135, 254)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(255, 255, 255, 0.8)',
      pointHoverBorderColor: 'rgba(255, 255, 255, 0.8)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [10, 20, 30, 25, 40, 45],
    },
  ],
};


class Breadcrumbs extends Component {
  tempAvialablePlans=[]
  tempMontlyCurrency=[]
  tempAnuallyCurrency=[]
  packageId=0
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
    this.state = {
      isShowPlanEditPrev:false,
      planDetails:[],
      planName:'',
      createdOn:'',
      maxBranch:0,
      noOfUsers:0,
      planNameArabic:'',
      noOfServices:0,
      montlyPrice:0,
      anuallyPrice:0,
      noOfSms:0,
      timeInTime:'',
      timeOutTime:'',
      perUserPrice:0,
      perSMSPrice:0,
      supportType:'',
      allMontlyCurrency:[],
      allAnuallyCurrency:[],
      selectedMontlyCurrency:'',
      selectedAnuallyCurrency:'',
      userEmail:'',
      userPhoneNo:'',
      isKisok:false,
      isAdvanced:false,
      isEasy:false,
      isDedicated :false,
      isPriority:false,
      isRemote:false,
      isAllTimeSupport:false,
      card1:false,
      card2:false,
      dropdownOpen: false,
      radioSelected: 2,
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }
  componentDidMount()
 {
  var prev=JSON.parse(localStorage.getItem('privileges'))
  var isPrev=false
  if(prev!=null)
  {
    for(var i=0;i<prev.length;i++)
    {
      if(prev[i]=='Subscription_Plan_Edit')
      {
        this.setState({isShowPlanEditPrev:true})
      }
    }
  }
  var data=this.props.location
  this.paymentId=parseInt(data.paymentId)
  this.paymentDetails(parseInt(data.paymentId))
   
 }
 paymentDetails(id)
 {
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/paymentPlan/getDetails', {
    method: 'POST',
    body:JSON.stringify({
     "id": parseInt(id)
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      for(var i=0;i<responseJson.paymentPlanList.length;i++)
      {
        for(var x=0;x<responseJson.paymentPlanList[i].paymentPlan.prices.length;x++)
        {
          this.tempMontlyCurrency.push(responseJson.paymentPlanList[i].paymentPlan.prices[x])
          this.tempAnuallyCurrency.push(responseJson.paymentPlanList[i].paymentPlan.prices[x])
        }
        var timeInOffice = responseJson.paymentPlanList[i].paymentPlan.responseTimeInOffice;  
        var a = timeInOffice.split(':'); 
        var timeIn = (+a[0]) * 60 + (+a[1]); 
        var timeOutOffice = responseJson.paymentPlanList[i].paymentPlan.responseTimeOutOffice;  
        var a = timeOutOffice.split(':'); 
        var timeOut = (+a[0]) * 60 + (+a[1]);
        this.setState({
          timeInTime:timeIn,
          timeOutTime:timeOut
        })
        var createdDate=new Date(responseJson.paymentPlanList[i].paymentPlan.createdTime)
        var uDate = createdDate.getDate();
        var uMonth = createdDate.getMonth()+1;
        var uYear = createdDate.getFullYear();

        var date= uDate+'.'+uMonth+'.'+uYear
        responseJson.paymentPlanList[i].paymentPlan['createdDate']=date
        this.tempAvialablePlans.push(responseJson.paymentPlanList[i])
      }
      var tempMC=this.tempMontlyCurrency[0].currency
      var tempAC=this.tempAnuallyCurrency[0].currency
      for(var k=0;k<this.tempAvialablePlans.length;k++)
      { 
        for(var o=0;o<this.tempAvialablePlans[k].paymentPlan.prices.length;o++)
        {
          if(tempMC==this.tempAvialablePlans[k].paymentPlan.prices[o].currency)
          {
            this.setState({
              priceMonthly:this.tempAvialablePlans[k].paymentPlan.prices[o].priceMonthly,
              perSMSPrice:this.tempAvialablePlans[k].paymentPlan.prices[o].perSMSPrice,
              perUserPrice:this.tempAvialablePlans[k].paymentPlan.prices[o].perUserPrice,
            })
          }
        }
      }
      for(var y=0;y<this.tempAvialablePlans.length;y++)
      { 
        for(var m=0;m<this.tempAvialablePlans[y].paymentPlan.prices.length;m++)
        {
          if(tempAC==this.tempAvialablePlans[y].paymentPlan.prices[m].currency)
          {
            this.setState({
              priceAnnually:this.tempAvialablePlans[y].paymentPlan.prices[m].priceAnnually,
              perSMSPrice:this.tempAvialablePlans[y].paymentPlan.prices[m].perSMSPrice,
              perUserPrice:this.tempAvialablePlans[y].paymentPlan.prices[m].perUserPrice,
            })
            // this.tempAvialablePlans[y].paymentPlan['showPriceAnnually']=this.tempAvialablePlans[y].paymentPlan.prices[m].priceAnnually
          }
        }
      }

      var tempArray=[...new Map(this.tempMontlyCurrency.map(o => [o.currency, o])).values()]
      this.tempMontlyCurrency=tempArray
      var tempArray1=[...new Map(this.tempAnuallyCurrency.map(o => [o.currency, o])).values()]
      this.tempAnuallyCurrency=tempArray1
      this.packageId=this.tempAvialablePlans[0].paymentPlan.paymentPlanId
      this.setState({
        
        isKisok:this.tempAvialablePlans[0].paymentPlan.kioskEnabled,
        isAdvanced:this.tempAvialablePlans[0].paymentPlan.advanceReportingEnabled,
        isEasy:this.tempAvialablePlans[0].paymentPlan.easyPaymentEnabled,
        isDedicated:this.tempAvialablePlans[0].paymentPlan.dedicatedAccountManager,
        isPriority:this.tempAvialablePlans[0].paymentPlan.prioritySupport,
        isRemote:this.tempAvialablePlans[0].paymentPlan.remoteDedicatedSupport,
        isAllTimeSupport:this.tempAvialablePlans[0].paymentPlan.fullDaySupport,
        noOfSms:this.tempAvialablePlans[0].paymentPlan.noOfSMS,
        supportType:this.tempAvialablePlans[0].paymentPlan.supportType,
        planName:this.tempAvialablePlans[0].paymentPlan.planName,
        planNameArabic:this.tempAvialablePlans[0].paymentPlan.planNameArabic,
        createdOn:this.tempAvialablePlans[0].paymentPlan.createdDate,
        maxBranch:this.tempAvialablePlans[0].paymentPlan.branchLimit,
        noOfUsers:this.tempAvialablePlans[0].paymentPlan.noOfUserPerBranch,
        noOfServices:this.tempAvialablePlans[0].paymentPlan.noOfServices,
        allMontlyCurrency:this.tempMontlyCurrency,
        allAnuallyCurrency:this.tempAnuallyCurrency,
        selectedMontlyCurrency:this.tempMontlyCurrency[0].currency,
        selectedAnuallyCurrency:this.tempAnuallyCurrency[0].currency
       
      })
    }
  })
 }
 changeMontlyCurrency=e=>{
  this.setState({selectedMontlyCurrency: e.currentTarget.textContent})
  for(var j=0;j<this.tempAvialablePlans.length;j++)
  { 
    for(var i=0;i<this.tempAvialablePlans[j].paymentPlan.prices.length;i++)
    {
      if( e.currentTarget.textContent==this.tempAvialablePlans[j].paymentPlan.prices[i].currency)
      {
        this.setState({
          priceMonthly:this.tempAvialablePlans[j].paymentPlan.prices[i].priceMonthly,
          perSMSPrice:this.tempAvialablePlans[j].paymentPlan.prices[i].perSMSPrice,
          perUserPrice:this.tempAvialablePlans[j].paymentPlan.prices[i].perUserPrice,
        })
        // this.tempAvialablePlans[j].paymentPlan['showPriceMonthly']=this.tempAvialablePlans[j].paymentPlan.prices[i].priceMonthly
     
      }
    }
  }
  this.setState({
    planDetails: this.tempAvialablePlans,
  })
}
changeAnuallyCurrency=e=>{
  this.setState({selectedAnuallyCurrency: e.currentTarget.textContent})
  for(var j=0;j<this.tempAvialablePlans.length;j++)
  { 
    for(var i=0;i<this.tempAvialablePlans[j].paymentPlan.prices.length;i++)
    {
      if( e.currentTarget.textContent==this.tempAvialablePlans[j].paymentPlan.prices[i].currency)
      {
        this.setState({
          priceAnnually:this.tempAvialablePlans[j].paymentPlan.prices[i].priceAnnually
        })
        // this.tempAvialablePlans[j].paymentPlan['showPriceAnnually']=this.tempAvialablePlans[j].paymentPlan.prices[i].priceAnnually
        // this.tempAvialablePlans[j].paymentPlan['showPriceMonthly']=this.tempAvialablePlans[j].paymentPlan.prices[i].priceMonthly
     
      }
    }
  }
  this.setState({
    planDetails: this.tempAvialablePlans,
  })
}
editPackage()
{
  this.props.history.push({ 
    pathname:'/newPlan',
    planName:this.state.planName,
    planArabic:this.state.planNameArabic,
    isKisok:this.state.isKisok,
    isAdvanced:this.state.isAdvanced,
    isEasy:this.state.isEasy,
    isDedicated:this.state.isDedicated,
    isPriority:this.state.isPriority,
    isRemote:this.state.isRemote,
    isAllTimeSupport:this.state.isAllTimeSupport,
    noOfSms:this.state.noOfSms,
    supportType:this.state.supportType,
    packageId:this.packageId,
    // createdOn:this.tempAvialablePlans[0].paymentPlan.createdDate,
    maxBranch:this.state.maxBranch,
    noOfUsers:this.state.noOfUsers,
    noOfServices:this.state.noOfServices,
    priceMonthly:this.state.priceMonthly,
    priceAnnually:this.state.priceAnnually,
    selectedMontlyCurrency:this.state.selectedMontlyCurrency,
    selectedAnuallyCurrency:this.state.selectedAnuallyCurrency,
    perUserPrice:this.state.perUserPrice,
    perSMSPrice:this.state.perSMSPrice,
    startTime:this.tempAvialablePlans[0].paymentPlan.responseTimeInOffice,
    endTime:this.tempAvialablePlans[0].paymentPlan.responseTimeOutOffice,
    type:'edit'
})
}
  render() {
    return (
      <div className="animated fadeIn">
        <DefaultHeader/>
          <Row className="mb-3 hidden">
          <Col sm="7">
            <Row>
            <Col className="text-center"  style={{maxWidth:'60px'}}>
              <i className="icon2-diamond-shape text-warning font-3xl"></i>
            </Col>
            <Col className="pl-0">
                <h4 className="mb-1 text-truncate" title={this.state.planName}><span className="text-overflow-hide text-truncate">{ this.state.planName}</span> <sup className="up-dot"><i className="icon2-ellipse"></i> </sup></h4>
                <p className="text-light-grey">Created on {this.state.createdOn}</p>
            </Col>
            </Row>
          </Col>

          <Col sm="5" className="text-right">
          
          {this.state.isShowPlanEditPrev==true && <button className="btn btn-outline-secondary ml-2" onClick={()=>this.editPackage()}>Edit <i className="icon2-edit-pencil"></i></button>}
          <button className="btn btn-outline-secondary ml-2">More <i className="icon2-menu-options font-sm"></i></button>
            
          </Col>
        </Row>


        <Row className="mb-3">

          <Col className="">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-3">
                    <div className="widget-icon">
                        <i className="icon2-branches text-warning font-2xl"></i>
                      </div>
                  </div>
                  <div className="col-9">
                    <div className="widget-title">
                      <p className="mb-1 text-light-grey">Max Branch Limit &nbsp; &nbsp; </p>
                       <h5>{this.state.maxBranch}&nbsp;</h5>
                    </div>
                  </div>
                    </div>

              </CardBody>
            </Card>
          </Col>

          <Col className="">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-3">
                    <div className="widget-icon">
                        <i className="icon2-username text-warning font-2xl"></i>
                      </div>
                  </div>
                  <div className="col-9">
                    <div className="widget-title">
                      <p className="mb-1 text-light-grey">No. of Users/Branch</p>
                     <h5>{this.state.noOfUsers}&nbsp;</h5>
                    </div>
                  </div>
                    </div>

              </CardBody>
            </Card>
          </Col>

          <Col className="">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-3">
                    <div className="widget-icon">
                        <i className="icon2-recipt text-warning font-2xl"></i>
                      </div>
                  </div>
                  <div className="col-9">
                    <div className="widget-title">
                      <p className="mb-1 text-light-grey">No. of Services &nbsp; &nbsp; &nbsp; &nbsp;</p>
                      <h5>{this.state.noOfServices}&nbsp;</h5>
                    </div>
                  </div>
                    </div>

              </CardBody>
            </Card>
          </Col>

          <Col className="">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-3">
                    <div className="widget-icon">
                        <i className="icon2-payments-card text-warning font-2xl"></i>
                      </div>
                  </div>
                  <div className="col-9 pr-0">
                    <div className="widget-title">
                      <p className="mb-1 text-light-grey">Price (Monthly) 
                      <ButtonDropdown id="card1" isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                      <DropdownToggle caret className="p-0 mb-0 text-warning" color="transparent">
                      {this.state.selectedMontlyCurrency}
                      </DropdownToggle>
                      <DropdownMenu right>
                      {this.state.allMontlyCurrency.map(currency => (
                        <DropdownItem>
                          <div onClick={this.changeMontlyCurrency}>{currency.currency}</div>
                          </DropdownItem>
                      ))}
                      </DropdownMenu>
                    </ButtonDropdown></p>
                      <h5>{this.state.priceMonthly} {this.state.selectedMontlyCurrency}&nbsp;</h5>
                    </div>
                  </div>
                    </div>

              </CardBody>
            </Card>
          </Col>

          <Col className="">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-3">
                    <div className="widget-icon">
                        <i className="icon2-payments-card text-warning font-2xl"></i>
                      </div>
                  </div>
                  <div className="col-9 pr-0">
                    <div className="widget-title">
                    <p className="mb-1 text-light-grey">Price (Annualy) 
                    <ButtonDropdown id="card2" isOpen={this.state.card2} toggle={() => { this.setState({ card2: !this.state.card2 }); }}>
                      <DropdownToggle caret className="p-0 mb-0 text-warning" color="transparent">
                      {this.state.selectedAnuallyCurrency}
                      </DropdownToggle>
                      <DropdownMenu right>
                      {this.state.allAnuallyCurrency.map(currency => (
                        <DropdownItem>
                          <div onClick={this.changeAnuallyCurrency}>{currency.currency}</div>
                          </DropdownItem>
                      ))}
                      </DropdownMenu>
                    </ButtonDropdown>
                    </p>
                    <h5>{this.state.priceAnnually} {this.state.selectedAnuallyCurrency}&nbsp;</h5>
                    </div>
                  </div>
                    </div>

              </CardBody>
            </Card>
          </Col>

        </Row>
        <Card className="mb-0">
            
            <CardBody>
              <Row>
                <Col sm="6">
                  <h5 className="mb-3"><i className="icon2-align-left"></i> Additional Features</h5>

                      

                    <div className="checkbox mb-2">
                    <input type="checkbox" id="additional1" name="Additional-Features" disabled checked={this.state.isKisok}></input>
                    <label htmlFor="additional1">Kiosk Enabled</label>
                  </div>
                  <div className="checkbox mb-2">
                    <input type="checkbox" id="additional2" name="Additional-Features" disabled checked={this.state.isAdvanced}></input>
                    <label htmlFor="additional2">Advanced Reporting</label>
                  </div>
                  <div className="checkbox mb-2">
                    <input type="checkbox" id="additional3" name="Additional-Features" disabled checked={this.state.isEasy}></input>
                    <label htmlFor="additional3">Easy Payments</label>
                  </div>

                    <p>Free SMS Quota <b>{this.state.noOfSms} SMS</b></p>
                    <ul className="list-dots blue-dots">
                      <li><a>{this.state.perUserPrice} {this.state.selectedMontlyCurrency} Price per User</a></li>
                      <li><a>{this.state.perSMSPrice} {this.state.selectedMontlyCurrency} Price per SMS</a></li>
                    </ul>

                </Col>
                <Col sm="6">
                <h5 className="mb-3"><i className="icon2-tools-gear"></i> Support/Maintenance</h5>
                <div className="badge badge-info mb-2">{this.state.supportType} Support</div>
                <ul className="list-dots blue-dots mb-5">
                      <li><a>{this.state.timeInTime} Mints Response Time</a></li>
                      <li><a>{this.state.timeOutTime} Mints Out of Office Response Time</a></li>
                    </ul>

                    <h6 className="text-primary">Additional Support Terms</h6>
                    <ul className="list-dots blue-dots">
                      {this.state.isDedicated==true && <li><a>Dedicated Account Manager</a></li>}
                      {this.state.isPriority==true &&  <li><a>Priority Support</a></li>}
                       {this.state.isRemote==true && <li><a>Remote Dedicated Support</a></li>}
                        {this.state.isAllTimeSupport==true && <li><a>24Hr Support</a></li>}
                    </ul>
                </Col>
              </Row>
            </CardBody>
          </Card>


      


      </div>
    );
  }
}

export default Breadcrumbs;
