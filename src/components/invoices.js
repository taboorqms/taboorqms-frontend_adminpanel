import React, { Component } from 'react';
import { Dropdown, Card, CardBody, Col, Row, Table, FormGroup,ButtonDropdown,  DropdownItem,  DropdownMenu,  DropdownToggle,Input,Button } from 'reactstrap';
import { Pie } from 'react-chartjs-2';
import { DatePickerComponent  } from '@syncfusion/ej2-react-calendars';
import axios from "axios";
import moment from 'moment'; 
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import LoadingBar from 'react-top-loading-bar'
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
class Invoice extends Component {
  tempInvoiceList=[]
  counter=0
  constructor(props) {
    super(props);
    this.state = {
      progress:0,
      isShowSortByMenu:false,
      isSortBy:'ascending',
      isShowInvoicePrev:false,
      isShowEditInvoicePrev:false,
      allBranches:[],
      searchInvoiceNumber:'',
      invoicelist:[],
      draftedinvoice:0,
      paymentpie:{
        label: ['Paid','Unpaid'],
        datasets: [
          {
            labels: '',
            data: [0,0],
            backgroundColor: [
              '#06cf99',
              '#f72c39',
            ],
            hoverBackgroundColor: [
              '#06cf99',
              '#f72c39',
            ],
          }],
      },
      statuspie:{
        label: ["Paid","Unpaid","Overdue","Cancel"],
        datasets: [
          {
            labels: '',
            data: [0,0,0,0],
            backgroundColor: [
              '#07cf98',
              '#f2ac15',
              '#f86c6b',
              '#a6b7f4'
            ],
            hoverBackgroundColor: [
              '#07cf98',
              '#f2ac15',
              '#f86c6b',
              '#a6b7f4'
            ],
          }],
      },
      dropdowninvoice:null,
      paidamount:0,
      unpaidamount:0,
      paidamountaed:0,
      unpaidamountaed:0,
      paidstaus:0,
      unpaidstatus:0,
      draftstatus:0,
      overdue:0,
      extra:false,
      isAll:false,
      totalinvoices:0,
      cancelledstatus:0,
      filterStatus:'Select Status',filterType:'Select Type',
      filterInoviceNumber:'',filterServiceCenter:'',filterAmount:'',sDate:null,eDate:null,dDate:null,nDate:null,
    };
    
  }
  componentDidMount()
  {
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Invoice_View')
        {
          this.setState({isShowInvoicePrev:true})
          isPrev=true
        }
        else {
          if(!isPrev==true)
          {
            isPrev=false
          }
         
        }
        if(prev[i]=='Invoice_Edit')
        {
          this.setState({isShowEditInvoicePrev:true})
        }
      }
    }
    if(isPrev==false)
    {
      toaster.notify("User don't have privilege", {
        duration:5000 // This notification will not automatically close
      });
    }
    else{
      this.getinvoices();
    }
  }
  branchDetails(id)
  {
    this.props.history.push(
      {
        pathname:'/branch-details',
        branchId:id
      }
    )
  }
  getinvoices=()=>{
    this.tempInvoiceList=[]
    this.setState({cancelledstatus:0});
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/invoice/getListing';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    this.setState({
      progress:50,isDisableSave:false
    })
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode===0)
      {
        this.setState({
          progress:100,isDisableSave:false
        })
        console.log(responseJson.data);
        this.setState({invoicelist:responseJson.data.invoices,totalinvoices:responseJson.data.invoices.length});
        for(var i=0;i<responseJson.data.invoices.length;i++){
          responseJson.data.invoices[i]['isChecked']=false
          if(responseJson.data.invoices[i].createdOn!=null)
          {
            var createdDate= new Date(responseJson.data.invoices[i].createdOn) 
            var sDate = createdDate.getDate();
            var sMonth = createdDate.getMonth()+1;
            var sYear = createdDate.getFullYear();
            sDate = sDate < 10 ? '0' + sDate : sDate;
            sMonth = sMonth < 10 && sMonth > 0 ? '0' + sMonth : sMonth;
            var date= sDate+'.'+sMonth+'.'+sYear
            responseJson.data.invoices[i]['sDate']=date
          }
          else
          {
            responseJson.data.invoices[i]['sDate']=null
          }
          if(responseJson.data.invoices[i].expiredOn!=null)
          {
            var expiredDate= new Date(responseJson.data.invoices[i].expiredOn) 
            var sDate = expiredDate.getDate();
            var sMonth = expiredDate.getMonth()+1;
            var sYear = expiredDate.getFullYear();
            sDate = sDate < 10 ? '0' + sDate : sDate;
            sMonth = sMonth < 10 && sMonth > 0 ? '0' + sMonth : sMonth;
            var date= sDate+'.'+sMonth+'.'+sYear
            responseJson.data.invoices[i]['eDate']=date
          }
          else
          {
            responseJson.data.invoices[i]['eDate']=null
          }
          if(responseJson.data.invoices[i].dueDate!=null)
          {
            var dueDate= new Date(responseJson.data.invoices[i].dueDate) 
            var sDate = dueDate.getDate();
            var sMonth = dueDate.getMonth()+1;
            var sYear = dueDate.getFullYear();
            sDate = sDate < 10 ? '0' + sDate : sDate;
            sMonth = sMonth < 10 && sMonth > 0 ? '0' + sMonth : sMonth;
            var date= sDate+'.'+sMonth+'.'+sYear
            responseJson.data.invoices[i]['dDate']=date
          }
          else
          {
            responseJson.data.invoices[i]['dDate']=null
          }
          if(responseJson.data.invoices[i].issuedOn!=null)
          {
            var issueDate= new Date(responseJson.data.invoices[i].issuedOn) 
            var sDate = issueDate.getDate();
            var sMonth = issueDate.getMonth()+1;
            var sYear = issueDate.getFullYear();
            sDate = sDate < 10 ? '0' + sDate : sDate;
            sMonth = sMonth < 10 && sMonth > 0 ? '0' + sMonth : sMonth;
            var date= sDate+'.'+sMonth+'.'+sYear
            responseJson.data.invoices[i]['nDate']=date
          }
          else
          {
            responseJson.data.invoices[i]['nDate']=null
          }
          if(responseJson.data.invoices[i].invoiceStatus=='DRAFT'){
            this.setState({draftedinvoice:this.state.draftedinvoice+1});
            this.setState({draftstatus:this.state.draftstatus+1});
          }
          if(responseJson.data.invoices[i].invoiceStatus=='OVERDUE'){
            this.setState({overdue:this.state.overdue+1});
          }
          if(responseJson.data.invoices[i].invoiceStatus=="PAID"){
            this.setState({paidstaus:this.state.paidstaus+1});
            this.setState({paidamount:responseJson.data.invoices[i].currency=='USD'?this.state.paidamount+responseJson.data.invoices[i].amount:this.state.paidamount});
            this.setState({paidamountaed:responseJson.data.invoices[i].currency=='AED'?this.state.paidamountaed+responseJson.data.invoices[i].amount:this.state.paidamountaed});

          }
          if(responseJson.data.invoices[i].invoiceStatus=="UNPAID"){
            this.setState({unpaidamount:responseJson.data.invoices[i].currency=='USD'?this.state.unpaidamount+responseJson.data.invoices[i].amount:this.state.unpaidamount});
            this.setState({unpaidamountaed:responseJson.data.invoices[i].currency=='AED'?this.state.unpaidamountaed+responseJson.data.invoices[i].amount:this.state.unpaidamountaed});


            this.setState({unpaidstatus:this.state.unpaidstatus+1});
          }
          if(responseJson.data.invoices[i].invoiceStatus=="CANCELLED"){
            this.setState({cancelledstatus:this.state.cancelledstatus+1});
          }
          this.tempInvoiceList.push(responseJson.data.invoices[i])
        }
        setTimeout(() => {
          var paymentchart={
            label: ['Paid','Unpaid'],
            datasets: [
              {
                labels: '',
                data: [this.state.paidamount+this.state.paidamountaed,this.state.unpaidamount+this.state.unpaidamountaed],
                backgroundColor: [
                  '#06cf99',
                  '#f72c39',
                ],
                hoverBackgroundColor: [
                  '#06cf99',
                  '#f72c39',
                ],
              }],
          }
          var statuschart={
            label: ["Paid","Unpaid","Overdue","Cancel"],
            datasets: [
              {
                labels: '',
                data: [this.state.paidstaus,this.state.unpaidstatus,this.state.overdue,this.state.cancelledstatus],
                backgroundColor: [
                  '#07cf98',
                  '#f2ac15',
                  '#f86c6b',
                  '#a6b7f4'
                ],
                hoverBackgroundColor: [
                  '#07cf98',
                  '#f2ac15',
                  '#f86c6b',
                  '#a6b7f4'
                ],
              }],
          }
          this.setState({paymentpie:paymentchart});
          this.setState({statuspie:statuschart});
        }, 1000);
      }
      else{
        this.setState({
          progress:50,isDisableSave:false
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  updateinvoice=(status,invoiceid,email,centerName)=>{
    this.setState({
      progress:50
    })
    let token = localStorage.getItem('sessionToken');
    var body = {
      "invoiceId": invoiceid,
      "invoiceStatus": status,
      "updateStatus": 2,
      "email": email,
      "draft" : false,
      "centerName": centerName

    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/invoice/addOrUpdate';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    console.log(body)
    axios.post(url,body,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode===0)
      {
        this.setState({invoicelist:[],progress:100});
        this.setState({draftedinvoice:0});
        this.setState({draftstatus:0});
        this.setState({paymentpie:{}});
        this.setState({statuspie:{}});
        this.setState({paidamount:0});
        this.setState({unpaidamount:0});
        this.setState({paidamountaed:0});
        this.setState({unpaidamountaed:0});
        this.setState({paidstaus:0});
        this.setState({unpaidstatus:0});
        this.setState({draftstatus:0});
        this.setState({overdue:0});
        this.getinvoices();
      }
      else{
        this.setState({
          progress:50
        })
        toaster.notify(responseJson.data.applicationStatusResponse, {
          duration: 1000 
        });
      }
    }).catch(err=>{
      console.log(err);
      toaster.notify('Network Error', {
        duration: 1000 
      });
    });
  }
  deleteinvoice=(invoiceid)=>{
    this.setState({
      progress:50
    })
    let token = localStorage.getItem('sessionToken');
    var body = {
      "id": invoiceid
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/invoice/delete';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.post(url,body,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode===0)
      {
        this.setState({invoicelist:[],progress:100});
        this.setState({draftedinvoice:0});
        this.setState({draftstatus:0});
        this.setState({paymentpie:{}});
        this.setState({statuspie:{}});
        this.setState({paidamount:0});
        this.setState({unpaidamount:0});
        this.setState({paidamountaed:0});
        this.setState({unpaidamountaed:0});
        this.setState({paidstaus:0});
        this.setState({unpaidstatus:0});
        this.setState({draftstatus:0});
        this.setState({overdue:0});
        this.getinvoices();
      }
      else{
        this.setState({
          progress:100
        })
        toaster.notify(responseJson.data.applicationStatusResponse, {
          duration: 1000 
        });
      }
    }).catch(err=>{
      console.log(err);
      toaster.notify('Network Error', {
        duration: 1000 
      });
    });
  }
  search(invoiceNumber)
  {
    
    if(invoiceNumber!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempInvoiceList.length;i++)
      {
        if(this.tempInvoiceList[i].invoiceNumber.toLowerCase().includes(invoiceNumber.toLowerCase())
        ||this.tempInvoiceList[i].invoiceStatus.toLowerCase().includes(invoiceNumber.toLowerCase())
        ||this.tempInvoiceList[i].invoiceType.toLowerCase().includes(invoiceNumber.toLowerCase())
        || this.tempInvoiceList[i].serviceCenterSubscription.serviceCenter.serviceCenterName.toLowerCase().includes(invoiceNumber.toLowerCase()))
        {
          
          data.push(this.tempInvoiceList[i])
          this.setState({
            invoicelist:data
          })
          isMatch=true
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
        }
      }
      if(isMatch==false)
      {
        this.setState({invoicelist:[]})
      }
    }
    else
    {
      this.setState({
        invoicelist:this.tempInvoiceList
      })
    }
  
  }
  changeSearch(invoiceNumber)
  {
    if(invoiceNumber=='')
    { 
      this.setState({
        invoicelist:this.tempInvoiceList
      })
    }
    this.search(invoiceNumber)
    this.setState({
      searchInvoiceNumber:invoiceNumber
      })
  }
  sortBy(data)
  {
    this.sortByStatus(data)
    this.setState({
      isSortBy:data
    })
  }
  sortByStatus(data)
  {
    if(this.tempInvoiceList.length!=0) {
      let sortedProductsAsc;
      if(data=='ascending')
      { sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>a.invoiceStatus.localeCompare(b.invoiceStatus)) }
      else
      { sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>b.invoiceStatus.localeCompare(a.invoiceStatus)) }
      console.log(sortedProductsAsc)
      this.setState({ invoicelist:sortedProductsAsc })
    }
  }
  sortByType()
  {
    if(this.tempInvoiceList.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>a.invoiceType.localeCompare(b.invoiceType))
      }
      else
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>b.invoiceType.localeCompare(a.invoiceType))
      }
      console.log(sortedProductsAsc)
      this.setState({
        invoicelist:sortedProductsAsc
      })
    }
    
  }
  sortByInoviceNumber()
  {
    if(this.tempInvoiceList.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>a.invoiceNumber.localeCompare(b.invoiceNumber))
      }
      else
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>b.invoiceNumber.localeCompare(a.invoiceNumber))
      }
      console.log(sortedProductsAsc)
      this.setState({
        invoicelist:sortedProductsAsc
      })
    }
    
  }
  sortByCreatedOn()
  {
    if(this.tempInvoiceList.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>{
          return new Date(a.createdOn)  - new Date (b.createdOn);
        })
      }
      else
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>{
          return new Date(a.createdOn)  - new Date(b.createdOn);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      console.log(sortedProductsAsc)
      this.setState({
        invoicelist:sortedProductsAsc
      })
    }
    
  }
  sortByExpireOn()
  {
    if(this.tempInvoiceList.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>{
          return new Date(a.expiredOn)  - new Date(b.expiredOn);
        })
      }
      else
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>{
          return new Date(a.expiredOn)  - new Date(b.expiredOn);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      console.log(sortedProductsAsc)
      this.setState({
        invoicelist:sortedProductsAsc
      })
    }
  }
  sortByServiceCenter()
  {
    if(this.tempInvoiceList.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>a.serviceCenterSubscription.serviceCenter.serviceCenterName.localeCompare(b.serviceCenterSubscription.serviceCenter.serviceCenterName))
      }
      else
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>b.serviceCenterSubscription.serviceCenter.serviceCenterName.localeCompare(a.serviceCenterSubscription.serviceCenter.serviceCenterName))
      }
      console.log(sortedProductsAsc)
      this.setState({
        invoicelist:sortedProductsAsc
      })
    }
    
  }
  sortByAmount()
  {
    if(this.tempInvoiceList.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>{
          return (a.amount)  - (b.amount);
        })
      }
      else
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>{
          return (a.amount)  - (b.amount);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      console.log(sortedProductsAsc)
      this.setState({
        invoicelist:sortedProductsAsc
      })
    }
    
  }
  sortByDueDate()
  {
    if(this.tempInvoiceList.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>{
          return new Date(a.dueDate)  - new Date(b.dueDate);
        })
      }
      else
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>{
          return new Date(a.dueDate)  - new Date(b.dueDate);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      console.log(sortedProductsAsc)
      this.setState({
        invoicelist:sortedProductsAsc
      })
    }
    
  }
  sortByNotifyDate()
  {
    if(this.tempInvoiceList.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>{
          return new Date(a.issuedOn)  - new Date(b.issuedOn);
        })
      }
      else
      {
        sortedProductsAsc= this.tempInvoiceList.sort((a,b)=>{
          return (a.issuedOn)  - (b.issuedOn);
        })
      }
      console.log(sortedProductsAsc)
      this.setState({
        invoicelist:sortedProductsAsc
      })
      sortedProductsAsc=sortedProductsAsc.reverse()
    }
    
  }
  selectAll()
  {
    var temp=this.state.isAll
    if(this.tempInvoiceList.length!=0)
    {
      for(var i=0;i<this.tempInvoiceList.length;i++)
      {
        if(temp==false)
        {
          this.tempInvoiceList[i]['isChecked']=true
          this.count=this.tempInvoiceList.length
        }
        else
        {
          this.count=0
          this.tempInvoiceList[i]['isChecked']=false
        }
        
      }
    }
    this.setState({
      isAll:!temp,
      invoicelist:this.tempInvoiceList,
    })
  }
  select(data)
  {
    for(var i=0;i<this.tempInvoiceList.length;i++)
    {
      if(data.invoiceId==this.tempInvoiceList[i].invoiceId)
      {
        if(this.tempInvoiceList[i]['isChecked']==true)
        {
          this.tempInvoiceList[i]['isChecked']=false
          this.setState({
            isAll:false
          })
          this.count=this.count-1
          break;
        }
        else
        {         
          this.count=this.count+1
          this.tempInvoiceList[i]['isChecked']=true
        }
  
      }
    }
    if(this.count==this.tempInvoiceList.length)
    {
      this.setState({
        isAll:true
      })
    }
    this.setState({invoicelist:this.tempInvoiceList})
  }
  changefilterStatus=e=>
  {
    this.setState({
      filterStatus:e.target.value
    })
  }
  changeFilterType=e=>
  {
    this.setState({
      filterType:e.target.value
    })
  }
  changeSubjectFilter(data)
  {
    this.setState({
      filterSubject:data
    })
  }
  changeInoviceNumberFilter(data)
  {
    this.setState({
      filterInoviceNumber:data
    })
  }
  changeServiceCenterFilter(data)
  {
    this.setState({
      filterServiceCenter:data
    })
  }
  changeAmountFilter(data)
  {
    this.setState({
      filterAmount:data
    })
  }
  changeStartDate(date)
  {
    this.setState({sDate:date.value})
  }
  changeExpireDate(date)
  {
    this.setState({eDate:date.value})
  }
  changeDueDate(date)
  {
    this.setState({dDate:date.value})
  }
  changeNotifyDate(date)
  {
    this.setState({nDate:date.value})
  }
  applyFilter()
  {
    var filterData=JSON.parse(JSON.stringify(this.tempInvoiceList))
    if(this.state.filterStatus!='Select Status')
    {
      var val=this.state.filterStatus
      filterData =  filterData.filter(function(status) {
        return status.invoiceStatus.toLowerCase() == val.toLowerCase() ;
      });
    }
    if(this.state.filterType!='Select Type')
    {
      var val=this.state.filterType
      filterData =  filterData.filter(function(status) {
        return status.invoiceType.toLowerCase()  == val.toLowerCase() ;
      });
    }
    if(this.state.filterInoviceNumber!='')
    {
      var val=this.state.filterInoviceNumber
      filterData =  filterData.filter(function(status) {
        return status.invoiceNumber == val;
      });
    }
    if(this.state.filterServiceCenter!='')
    {
      var val=this.state.filterServiceCenter
      filterData =  filterData.filter(function(status) {
        return status.serviceCenterSubscription.serviceCenter.serviceCenterName.toLowerCase() == val.toLowerCase() ;
      });
    }
    if(this.state.filterAmount!='')
    {
      var val=this.state.filterAmount
      filterData =  filterData.filter(function(status) {
        return status.amount== parseInt(val);
      });
    }
    if(this.state.sDate!=null)
    {
      let d = this.state.sDate.getDate();
      let m = this.state.sDate.getMonth()+1;
      let y=this.state.sDate.getFullYear()
      d = d < 10 ? '0' + d : d;
      m = m < 10 && m > 0 ? '0' + m : m;
      var val=d+"."+m+"."+y 
      filterData =  filterData.filter(function(status) {
        return status.sDate == val;
      });
    }
    if(this.state.nDate!=null)
    {
      let d = this.state.nDate.getDate();
      let m = this.state.nDate.getMonth()+1;
      let y=this.state.nDate.getFullYear()
      d = d < 10 ? '0' + d : d;
      m = m < 10 && m > 0 ? '0' + m : m;
      var val=d+"."+m+"."+y 
      filterData =  filterData.filter(function(status) {
        return status.nDate == val;
      });
    }
    if(this.state.eDate!=null)
    {
      let d = this.state.eDate.getDate();
      let m = this.state.eDate.getMonth()+1;
      let y=this.state.eDate.getFullYear()
      d = d < 10 ? '0' + d : d;
      m = m < 10 && m > 0 ? '0' + m : m;
      var val=d+"."+m+"."+y 
      filterData =  filterData.filter(function(status) {
        return status.eDate == val;
      });
    }
    if(this.state.dDate!=null)
    {
      let d = this.state.dDate.getDate();
      let m = this.state.dDate.getMonth()+1;
      let y=this.state.dDate.getFullYear()
      d = d < 10 ? '0' + d : d;
      m = m < 10 && m > 0 ? '0' + m : m;
      var val=d+"."+m+"."+y 
      filterData =  filterData.filter(function(status) {
        return status.dDate == val;
      });
    }
    this.setState({
      invoicelist:filterData
    })
    console.log(filterData)
   
  }
  resetFilter()
  {
    this.setState({
      filterStatus:'Select Status',filterType:'Select Type',
      filterInoviceNumber:'',filterServiceCenter:'',filterAmount:'',sDate:null,eDate:null,dDate:null,nDate:null,
      invoicelist:this.tempInvoiceList
    })
  }
  render() {
    return (
      <div className="animated fadeIn">
        <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress:0
        })}
        />
      <DefaultHeader/>
     {this.state.isShowInvoicePrev==true &&  <Row className="mb-4">
     {(this.state.paymentpie!={}) && (
        <Col sm="6">
          <Card className="card-line">
            <CardBody>
            <h4>Payments</h4>
            <Row>
              <Col sm="4"></Col>
              <Col sm="4">
              <div className="chart-wrapper pie-graph" style={{height: '110px', width: '100%'}}>
                <Pie data={this.state.paymentpie} options={{tooltips:{enabled:false}}}/>
              </div>
              </Col>
              <Col sm="4">
                <ul className="graph-legends">
                <li><h5 className="mb-0"><i className="icon2-ellipse text-success"></i> $ {this.state.paidamount} , AED {this.state.paidamountaed}</h5> <h6 className="text-light-grey ml-3 font-weight-normal">Received</h6></li>
                <li><h5 className="mb-0"><i className="icon2-ellipse text-danger"></i> $ {this.state.unpaidamount} , AED {this.state.unpaidamountaed}</h5> <h6 className="text-light-grey ml-3 font-weight-normal">Outstanding</h6></li>
              </ul>
              </Col>
            </Row>
          
            </CardBody>
          </Card>
        </Col>
      )}
        {(this.state.statuspie!={}) && (
          <Col sm="6">
            <Card className="card-line">
                <CardBody>
                <h4>Status</h4>
              <Row>
              <Col sm="4" className="pt-3"><h2 className="line-h-normal">{this.state.totalinvoices} <small className="font-lg">Total</small></h2></Col>
              <Col sm="4">
              <div className="chart-wrapper pie-graph" style={{height: '110px', width: '100%'}}>
                <Pie data={this.state.statuspie} options={{tooltips:{enabled:false}}}/>
              </div>
              </Col>
              <Col sm="4">
                <ul className="graph-legends">
                  <li><i className="icon2-ellipse text-success"></i> <b>{this.state.paidstaus}</b> Paid</li>
                  <li><i className="icon2-ellipse text-warning"></i> <b>{this.state.unpaidstatus}</b> Unpaid</li>
                  {/* <li><i className="icon2-ellipse text-info"></i> </li> */}
                  <li><i className="icon2-ellipse text-danger"></i> <b>{this.state.overdue}</b> Overdue</li>
                  <li><i className="icon2-ellipse text-info"></i> <b>{this.state.cancelledstatus}</b> Cancel</li>
                </ul>
              </Col>
              </Row>
                </CardBody>
              </Card>          
          </Col>
        )}
        
      </Row>}


      {this.state.isShowInvoicePrev==true && <Row className="mb-3 hidden">
          
          <Col sm="4"> 
            <h3>
            <ButtonDropdown className="float-left" id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                &nbsp;
                </DropdownToggle>
                <DropdownMenu left="true">
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              <div className="checkbox form-check d-inline-block ml-2 float-left">
                  <input  type="checkbox" className="form-check-input" onChange={()=>this.selectAll()} checked={this.state.isAll}></input>
                  <label  className="form-check-label form-check-label">{this.state.invoicelist.length}&nbsp;Invoices</label>
              </div>
              {/* {(this.state.draftedinvoice!=0) && ( */}
                <span className="badge badge-primary float-left mt-1">{this.state.draftedinvoice} Drafts</span>
              {/* )} */}
              <div className="clearfix"></div>
              </h3>
          </Col>
          <Col sm="8" className="text-right">
          
            <div className="float-right">
            <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button>
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                onClick={() => {this.setState({ isShowSortByMenu:false })}}
                >
                <div className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></div>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={()=>this.sortBy('ascending')}><input type="radio" checked={this.state.isSortBy=='ascending'} name="sort-order" onChange={()=>{}}></input> Ascending</DropdownItem>
                  <DropdownItem onClick={()=>this.sortBy('descending')}><input type="radio" checked={this.state.isSortBy=='descending'} name="sort-order" onChange={()=>{}}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            {this.state.isShowEditInvoicePrev==true && <button className="btn btn-outline-warning ml-2" onClick={()=>this.props.history.push('/newInvoice')}><i className="icon2-addition-sign"></i></button>}
            </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                value={this.state.searchBranch} onChange={(e) => this.changeSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.search()
                  }
                }}
                >
                  
                </input>
              <div className="input-group-append" onClick={()=>this.search()}><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>
        </Row>}

        {this.state.isShowFilters==true && <Card className="card-line" id="filter-row">
          <CardBody>
            <Row>
              
                    <Col className="text-center">
                      <FormGroup className="input-line mb-0">
                        <select className="form-control" value={this.state.filterStatus} onChange={this.changefilterStatus}>
                          <option value='Select Status' key='Select Status'>
                            Select Status
                          </option>
                          <option value='Paid' key='Paid'>
                            Paid
                          </option>
                          <option value='UnPaid' key='UnPaid'>
                            UnPaid
                          </option>
                          <option value='OverDue' key='OverDue'>
                            OverDue
                          </option>
                          <option value='Draft' key='Draft'>
                            Draft
                          </option>
                          <option value='Cancelled' key='Cancelled'>
                            Cancel
                          </option>
                        </select>
                      </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line mb-0">
                        <select className="form-control" value={this.state.filterType} onChange={this.changeFilterType}>
                          <option value='Select Type' key='Select Type'>
                            Select Type
                          </option>
                          <option value='Auto' key='Auto'>
                            Auto
                          </option>
                          <option value='Manual' key='Manual'>
                            Manual
                          </option>
                        </select>
                      </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="invoice#"
                        value={this.state.filterInoviceNumber} 
                        onChange={(e) => this.changeInoviceNumberFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                      <DatePickerComponent  value={this.state.sDate}  placeholder='Created'
                        onChange={(date) => this.changeStartDate(date)}/>
                      </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                      <DatePickerComponent  value={this.state.eDate}  placeholder='Expires'
                        onChange={(date) => this.changeExpireDate(date)}/>
                      </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Service Center"
                        value={this.state.filterServiceCenter} 
                        onChange={(e) => this.changeServiceCenterFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Amount" 
                       value={this.state.filterAmount} 
                       onChange={(e) => this.changeAmountFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                      <DatePickerComponent  value={this.state.dDate}  placeholder='Due Date'
                        onChange={(date) => this.changeDueDate(date)}/>
                      </FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0">
                      <DatePickerComponent  value={this.state.nDate}  placeholder='Notify'
                        onChange={(date) => this.changeNotifyDate(date)}/>
                      </FormGroup>
                    </Col>
                    
                   
                    <Col sm="auto">
                      <Button color="light" className="btn-sm" onClick={()=>this.resetFilter()} >Reset</Button>
                      <Button color="primary" className="btn-sm ml-2" onClick={()=>this.applyFilter()}>Apply</Button>
                    </Col>
                    
                    </Row>
                  </CardBody>
                  </Card>}
        {this.state.isShowInvoicePrev==true &&  <Card className="mb-0">
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                  <th width="71" style={{minWidth:'71px'}}></th>
                    <th onClick={()=>this.sortByStatus()}>Status</th>
                    <th onClick={()=>this.sortByType()}>Type</th>
                    <th onClick={()=>this.sortByInoviceNumber()}>Invoice#</th>
                    <th onClick={()=>this.sortByCreatedOn()}>Created </th>
                    <th onClick={()=>this.sortByExpireOn()}>Expires</th>
                    <th className="text-left" onClick={()=>this.sortByServiceCenter()}>Service Center</th>
                    <th onClick={()=>this.sortByAmount()}>Amount</th>
                    <th onClick={()=>this.sortByDueDate()}>Due Date</th>
                    <th onClick={()=>this.sortByNotifyDate()}>Notify</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                    {this.state.invoicelist.map((item,index)=>(
                      <tr key={index}>
                      <td className="text-right">
                        <div className="checkbox form-check">
                            <input type="checkbox" className="form-check-input" onChange={()=>this.select(item)} checked={item.isChecked}></input>
                            <label  className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">
                        <span className="badge badge-success capifirstltr d-block" style={{color: item.invoiceStatus=='UNPAID'?'#f72c39':item.invoiceStatus=='PAID'?'#06cf99':'#20a8d8',backgroundColor: item.invoiceStatus=='UNPAID'?'#ffe1e3':item.invoiceStatus=='PAID'?'#06cf9924':'#20a8d833'}}>{item.invoiceStatus=="CANCELLED"?'Cancel':item.invoiceStatus}</span>
                      </td>
                      
                      <td className="text-center capifirstltr">{item.invoiceType}</td>
                      <td className="text-center">{item.invoiceNumber}</td>
                      <td className="text-center no-wrap">{moment(item.createdOn).format('DD-MM-YYYY')}</td>
                      <td className="text-center no-wrap">{moment(item.expiredOn).format('DD-MM-YYYY')}</td>
                      <td>{item.serviceCenterSubscription.serviceCenter.serviceCenterName}<p className="text-light-grey"><i className="icon2-location"></i> {item.serviceCenterSubscription.serviceCenter.country}</p></td>
                      <td className="text-center">{item.currency=='USD'?'$':'AED'} {item.amount}</td>
                      <td className="text-center no-wrap">{moment(item.dueDate).format('DD-MM-YYYY')}</td>
                      <td className="text-center no-wrap">{moment(item.issuedOn).format('DD-MM-YYYY')}</td>
                      <td className="text-center">
                        {(this.state.isShowEditInvoicePrev==true && item.invoiceType=="MANUAL" && item.invoiceStatus!="CANCELLED") && (
                          <Dropdown id={index} isOpen={this.state.dropdowninvoice==index} toggle={() => { 
                            if(this.state.dropdowninvoice!=index){
                              this.setState({dropdowninvoice:index})
                            }
                            else{
                              this.setState({dropdowninvoice:null})
                            }
                            }}>
                            <DropdownToggle color="transparent">
                            <i className="f20 icon2-menu-options text-light-grey"></i>
                            </DropdownToggle>
                            <DropdownMenu right>
                              {(item.invoiceStatus=="UNPAID") && (
                                <DropdownItem onClick={()=>{this.updateinvoice("PAID",item.invoiceId,item.serviceCenterSubscription.serviceCenter.email,item.serviceCenterSubscription.serviceCenter.serviceCenterName)}}>Paid</DropdownItem>
                              )}
                              {(item.invoiceStatus=="DRAFT") && (
                                <DropdownItem onClick={()=>{this.deleteinvoice(item.invoiceId)}}>Delete</DropdownItem>
                              )}
                              {(item.invoiceStatus=="PAID") && (
                                <DropdownItem onClick={()=>{this.updateinvoice("CANCELLED",item.invoiceId,item.serviceCenterSubscription.serviceCenter.email,item.serviceCenterSubscription.serviceCenter.serviceCenterName)}}>Cancel</DropdownItem>
                              )}
                              {(item.invoiceStatus!="PAID") && (
                                <DropdownItem onClick={()=>{localStorage.setItem('editinvoice', item); global.editinvoice=item;this.props.history.push('/editinvoices')}}>Edit</DropdownItem>
                              )}
                            </DropdownMenu>
                          </Dropdown>
                        )}
                        
                      </td>
                    </tr>
                    ))}
                  
                   </tbody>
                </Table>
               
              </CardBody>
            </Card>}
      </div>
    );
  }
}

export default Invoice;
