export default {
    items: [
        //  {
        //    name: 'Home',
        //    url: '/dashboard',
        //    icon: 'icon-home',
           
        //  },
         {
           name: 'Centers',
           url: '/centerList',
           icon: 'icon2-branches',
           badge: {
             variant: 'primary',
             text: '0',
           },
         },
         {
           name: 'Invoices',
           url: '/invoices',
           icon: 'icon2-money-card',
         },
         {
           name: 'Packages/Plans',
           url: '/packagePlan',
           icon: 'icon2-cube-shape',
         },
         // {
         //   name: 'Reports',
         //   url: '/landing',
         //   icon: 'icon-pie-chart',
         // },
         // {
         //   divider: true,
         // },
         {
           divider: true,
         },
         {
           divider: true,
         },
        //  {
        //    name: 'Center Profile',
        //    url: '/centerProfile',
        //    icon: 'icon2-username',
        //  },
         {
           name: 'Settings',
           url: '/register',
           icon: 'icon2-settings-bars',
           children: [
             {
               name: 'My Account',
               url: '/myAccount',
               icon: 'icon2-username',
               attributes: { disabled: true },
               
             },
             {
               name: 'Users',
               url: '/users',
               icon: 'icon2-username',
               badge: {
                 variant: 'info',
                 
               },
             },
             {
               name: 'Roles',
               url: '/roles',
               icon: 'icon2-target1',
               badge: {
                 variant: 'secondary',
                 
               },
             },
             {
               name: 'Notifications',
               url: '/notifications',
               icon: 'icon2-alarm-bell',
             },
             {
               name: 'Configurations',
               url: '/configurations',
               icon: 'icon2-settings-gear',
             },
           ],
         },
         {
           name: 'Support',
           url: '/support',
           icon: 'icon2-tools-gear',
           
         },
         {
           name: 'Logout',
           url: '/login',
           icon: 'icon2-logout-menu',
         },
         
         
         {
           divider: true,
         },
         {
           title: true,
           name: 'Extras',
         },
       ],
 }