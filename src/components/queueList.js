import React, { Component } from 'react';
import { Badge, Breadcrumb, BreadcrumbItem, Card,Dropdown, CardBody, CardHeader, Col, Row, Table, 
  ButtonDropdown, DropdownItem,  DropdownMenu,  DropdownToggle,FormGroup,Button,Input  } from 'reactstrap';
import axios from "axios";


import { Link, NavLink } from 'react-router-dom';
import * as router from 'react-router-dom';
import { UncontrolledDropdown, Nav, NavItem } from 'reactstrap';
import {
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import routes from './routes';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
class Breadcrumbs extends Component {
  tempAvialableQueues=[]
  totalQueues=0
  counter=0
  constructor(props) {
    super(props);
    this.state = {
      isSortBy:'ascending',
      isShowDropDownMenu:false,
      allQueue:[],
      searchQueue:'',
      isShowQueuePrev:false,
      isShowQueueEditPrev:false,
      isShowFilters:false,
      isAll:false,
      filterId:'',filterInQueue:'',filterTicket:'',filterBranch:'',filterFastPass:'',filterCounter:'',filterAgents:'',
      filterAst:'',filterAwt:''
    };
  
  }
  componentDidMount()
  {
    if(localStorage.getItem('isShowArabicLanguage')=='true')
    {
      global.isShowArabicLanguage=true
    }
    else
    {
      global.isShowArabicLanguage=false
    }
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Queue_View')
        {
          this.setState({isShowQueuePrev:true})
          isPrev=true
        }
        else {
          if(!isPrev==true)
          {
            isPrev=false
          }
         
        }
        if(prev[i]=='Queue_Edit')
        {
          this.setState({isShowQueueEditPrev:true})
        }
      }
    }
    if(isPrev==false)
    {
      toaster.notify("User don't have privilege", {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
    else{
      this.getQueueList()
    }
  }
  getQueueList()
  {
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/queue/getListing';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode===0)
      {
        
        for(var i=0;i<responseJson.data.queueList.length;i++)
        {

          var hms = responseJson.data.queueList[i].queue.averageServiceTime;  
          var a = hms.split(':'); 
          var minutes = (+a[0]) * 60 + (+a[1]); 
          console.log(minutes);
          responseJson.data.queueList[i].queue['AST']=minutes
          var hmss = responseJson.data.queueList[i].queue.averageWaitTime;  
          var a = hmss.split(':'); 
          var minutess = (+a[0]) * 60 + (+a[1]); 
          console.log(minutess);
          responseJson.data.queueList[i].queue['AWT']=minutess
          responseJson.data.queueList[i]['isShowMenu']=false
        }
        this.tempAvialableQueues.push(responseJson.data.queueList)
        this.totalQueues=this.tempAvialableQueues[0].length
        this.setState({
          allQueue:responseJson.data.queueList,
         
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  showMenu(id)
  {
      for(var i=0;i<this.state.allQueue.length;i++)
      {
        if(this.state.allQueue[i].queue.queueId==id)
        {
          this.state.allQueue[i]['isShowMenu']=true
          this.setState({
            isShowDropDownMenu:true
          })
        }
        else
        {
          this.state.allQueue[i]['isShowMenu']=false
        }
      }
      var data=this.state.allQueue
      this.setState({
        allQueue:data
      })
  }
  search(queueNumber)
  {
    
    if(queueNumber!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempAvialableQueues[0].length;i++)
      {
        if(this.tempAvialableQueues[0][i].queue.queueNumber.toLowerCase().includes(queueNumber.toLowerCase()) || 
          this.tempAvialableQueues[0][i].queue.branch.branchName.toLowerCase().includes(queueNumber.toLowerCase()) )
        {
          
          data.push(this.tempAvialableQueues[0][i])
          this.setState({
            allQueue:data
          })
          isMatch=true
          this.totalQueues=data.length
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
          
        }
      }
      if(isMatch==false)
      {
        this.totalQueues=0
        this.setState({allQueue:[]})
      }
    }
    else
    {
      this.totalQueues=this.tempAvialableQueues[0].length
      this.setState({
        allQueue:this.tempAvialableQueues[0]
      })
    }
  
  }
  changeSearch(queueNumber)
  {
    if(queueNumber=='')
    { 
      this.totalQueues=this.tempAvialableQueues[0].length
      this.setState({
        allQueue:this.tempAvialableQueues[0]
      })
    }
    this.search(queueNumber)
    this.setState({
      searchQueue:queueNumber
      })
  }
  sortBy(data)
  {
    this.setState({
      isSortBy:data
    })
  }
  sortByName()
  {
    if(this.tempAvialableQueues.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>a.queue.branch.branchName.localeCompare(b.queue.branch.branchName))

      }
      else
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>b.queue.branch.branchName.localeCompare(a.queue.branch.branchName))

      }
      console.log(sortedProductsAsc)
      this.setState({
          allQueue:sortedProductsAsc
      })
    }
    
  }
  sortByQueueId()
  {
    if(this.tempAvialableQueues.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>a.queue.queueNumber.localeCompare(b.queue.queueNumber))
      }
      else
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>a.queue.queueNumber.localeCompare(b.queue.queueNumber))
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      console.log(sortedProductsAsc)
      this.setState({
          allQueue:sortedProductsAsc
      })
    }
    
  }
  sortByCounters()
  {
    if(this.tempAvialableQueues.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.noOfServingCounters)  - Number(b.noOfServingCounters);
        })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.noOfServingCounters)  - Number(b.noOfServingCounters);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
     
    console.log(sortedProductsAsc)
    this.setState({
        allQueue:sortedProductsAsc
    })
    }
    
  }
  sortByFastPass()
  {
    if(this.tempAvialableQueues.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.noOfFastpassTicket)  - Number(b.noOfFastpassTicket);
        })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.noOfFastpassTicket)  - Number(b.noOfFastpassTicket);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      
    console.log(sortedProductsAsc)
    this.setState({
        allQueue:sortedProductsAsc
    })
    }
    
  }
  sortByTicket()
  {
    if(this.tempAvialableQueues.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.noOfTickets)  - Number(b.noOfTickets);
        })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.noOfTickets)  - Number(b.noOfTickets);
        })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
      console.log(sortedProductsAsc)
      this.setState({
          allQueue:sortedProductsAsc
      })
    }
  }
  sortByAgents()
  {
    if(this.tempAvialableQueues.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.noOfAgents)  - Number(b.noOfAgents);
       })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.noOfAgents)  - Number(b.noOfAgents);
       })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
    console.log(sortedProductsAsc)
    this.setState({
        allQueue:sortedProductsAsc
    })
    }
  }
  sortByInQueue()
  {
    if(this.tempAvialableQueues.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.noOfInQueueTickets)  - Number(b.noOfInQueueTickets);
       })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.noOfInQueueTickets)  - Number(b.noOfInQueueTickets);
       })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
    
    console.log(sortedProductsAsc)
    this.setState({
        allQueue:sortedProductsAsc
    })
    }
  }
  sortByAST()
  {
    if(this.tempAvialableQueues.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.queue.AST)  - Number(b.queue.AST);
       })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.queue.AST)  - Number(b.queue.AST);
       })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
   
    console.log(sortedProductsAsc)
    this.setState({
        allQueue:sortedProductsAsc
    })
    }
  }
  sortByAWT()
  {
    if(this.tempAvialableQueues.length!=0)
    {
      let sortedProductsAsc;
      if(this.state.isSortBy=='ascending')
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.queue.AWT)  - Number(b.queue.AWT);
       })
      }
      else
      {
        sortedProductsAsc= this.tempAvialableQueues[0].sort((a,b)=>{
          return Number(a.queue.AWT)  - Number(b.queue.AWT);
       })
        sortedProductsAsc=sortedProductsAsc.reverse()
      }
    
    console.log(sortedProductsAsc)
    this.setState({
      allQueue:sortedProductsAsc
    })
    }
  }
  queueDetails(id)
  {
    this.props.history.push(
      {
        pathname:'/queue-details',
        queueId:id
      }
    
    )
  }
  selectAll()
  {
    var temp=this.state.isAll
    if(this.tempAvialableQueues[0].length!=0)
    {
      for(var i=0;i<this.tempAvialableQueues[0].length;i++)
      {
        if(temp==false)
        {
          this.tempAvialableQueues[0][i].queue['isChecked']=true
          this.count=this.tempAvialableQueues[0].length
        }
        else
        {
          this.count=0
          this.tempAvialableQueues[0][i].queue['isChecked']=false
        }
        
      }
    }
    this.setState({
      isAll:!temp,
      allQueue:this.tempAvialableQueues[0],
    })
  }
  select(data)
  {
    for(var i=0;i<this.tempAvialableQueues[0].length;i++)
    {
      if(data.queueId==this.tempAvialableQueues[0][i].queue.queueId)
      {
        if(this.tempAvialableQueues[0][i].queue['isChecked']==true)
        {
          this.tempAvialableQueues[0][i].queue['isChecked']=false
          this.setState({
            isAll:false
          })
          this.count=this.count-1
          break;
        }
        else
        {         
          this.count=this.count+1
          this.tempAvialableQueues[0][i].queue['isChecked']=true
        }
  
      }
    }
    if(this.count==this.tempAvialableQueues[0].length)
    {
      this.setState({
        isAll:true
      })
    }
    this.setState({allQueue:this.tempAvialableQueues[0]})
  }
  changefilterId(data)
  {
    this.setState({
      filterId:data
    })
  }
  changeBranchFilter(data)
  {
    this.setState({
      filterBranch:data
    })
  }
  changeInQueueFilter(data)
  {
    this.setState({
      filterInQueue:data
    })
  }
  changeTicketFilter(data)
  {
    this.setState({
      filterTicket:data
    })
  }
  changeFastPassFilter(data)
  {
    this.setState({
      filterFastPass:data
    })
  }
  changeAgentFilter(data)
  {
    this.setState({
      filterAgents:data
    })
  }
  changeCounterFilter(data)
  {
    this.setState({
      filterCounter:data
    })
  }
  changeASTFilter(data)
  {
    this.setState({
      filterAst:data
    })
  }
  changeAWTFilter(data)
  {
    this.setState({
      filterAwt:data
    })
  }
  applyFilter()
  {
    var filterData=JSON.parse(JSON.stringify(this.tempAvialableQueues[0]))
    if(this.state.filterId!='')
    {
      var val=this.state.filterId
      filterData =  filterData.filter(function(status) {
        return status.queue.queueNumber.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterInQueue!='')
    {
      var val=this.state.filterInQueue
      filterData =  filterData.filter(function(status) {
        return status.noOfInQueueTickets == parseInt(val);
      });
    }
    if(this.state.filterBranch!='')
    {
      var val=this.state.filterBranch
      filterData =  filterData.filter(function(status) {
        return status.queue.branch.branchName.toLowerCase() == val.toLowerCase();
      });
    }
    if(this.state.filterTicket!='')
    {
      var val=this.state.filterName
      filterData =  filterData.filter(function(status) {
        return status.noOfTickets == parseInt(val);
      });
    }
    if(this.state.filterFastPass!='')
    {
      var val=this.state.filterFastPass
      filterData =  filterData.filter(function(status) {
        return status.noOfFastpassTicket ==parseInt(val);
      });
    }
    if(this.state.filterCounter!='')
    {
      var val=this.state.filterCounter
      filterData =  filterData.filter(function(status) {
        return status.noOfTotalCounters ==parseInt(val);
      });
    }
    if(this.state.filterAgents!='')
    {
      var val=this.state.filterAgents
      filterData =  filterData.filter(function(status) {
        return status.noOfAgents ==parseInt(val);
      });
    }
    if(this.state.filterAst!='')
    {
      var val=this.state.filterAst
      filterData =  filterData.filter(function(status) {
        return status.queue.AST == parseInt(val);
      });
    }
    if(this.state.filterAwt!='')
    {
      var val=this.state.filterAwt
      filterData =  filterData.filter(function(status) {
        return status.queue.AST == parseInt(val);
      });
    }
   this.totalQueues=filterData.length
    this.setState({
      allQueue:filterData
    })
    console.log(filterData)
   
  }
  resetFilter()
  {
    this.totalQueues=this.tempAvialableQueues[0].length
    this.setState({
      filterId:'',filterInQueue:'',filterTicket:'',filterBranch:'',filterFastPass:'',filterCounter:'',filterAgents:'',
      filterAst:'',filterAwt:'',allQueue:this.tempAvialableQueues[0]
    })
  }
  render() {
    return (
      <div className="animated fadeIn">
        <DefaultHeader/>
        <Row className="mb-3 hidden">
          <Col sm="4">
            <h3>
            <ButtonDropdown id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                &nbsp;
                </DropdownToggle>
                <DropdownMenu left>
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              <div className="checkbox form-check d-inline-block ml-2">
                  <input  type="checkbox" className="form-check-input" onChange={()=>this.selectAll()} checked={this.state.isAll}></input>
                  <label className="form-check-label form-check-label">{this.totalQueues}&nbsp;Queues</label>
              </div>
              </h3>
          </Col>
          {this.state.isShowQueuePrev==true && <Col sm="8" className="text-right">
          
            <div className="float-right">
            <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button>
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                onClick={() => {this.setState({ isShowSortByMenu:false })}}
                >
                <button className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></button>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={()=>this.sortBy('ascending')}><input type="radio" checked={this.state.isSortBy=='ascending'} name="sort-order" onChange={()=>{}}></input> Ascending</DropdownItem>
                  <DropdownItem onClick={()=>this.sortBy('descending')}><input type="radio" checked={this.state.isSortBy=='descending'} name="sort-order" onChange={()=>{}}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
             {/* {this.state.isShowQueueEditPrev==true &&    <button className="btn btn-outline-warning ml-2"><i className="icon2-addition-sign"></i></button>} */}
            </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                value={this.state.searchQueue} onChange={(e) => this.changeSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.search()
                  }
                }}
                >
                


                </input>
              <div className="input-group-append"><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>}
        </Row>

        {this.state.isShowFilters==true && <Card className="card-line" id="filter-row">
          <CardBody>
            <Row>
              
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Q.ID" 
                       value={this.state.filterId} 
                       onChange={(e) => this.changefilterId(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="In Queue" min="0"
                       value={this.state.filterInQueue} 
                       onChange={(e) => this.changeInQueueFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="text" placeholder="Branch" 
                      value={this.state.filterBranch} 
                      onChange={(e) => this.changeBranchFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="#Tickets" min="0"
                       value={this.state.filterTicket} 
                       onChange={(e) => this.changeTicketFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="Fast Pass" min="0"
                       value={this.state.filterFastPass} 
                       onChange={(e) => this.changeFastPassFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="Counters" min="0"
                       value={this.state.filterCounter} 
                       onChange={(e) => this.changeCounterFilter(e.target.value)}
                       /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="Agents" min="0"
                      value={this.state.filterAgents} 
                      onChange={(e) => this.changeAgentFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="AST" min="0"
                      value={this.state.filterAst} 
                      onChange={(e) => this.changeASTFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                    <Col className="text-center">
                      <FormGroup className="input-line  mb-0"><Input type="number" placeholder="AWT" min="0"
                      value={this.state.filterAwt} 
                      onChange={(e) => this.changeAWTFilter(e.target.value)}
                      /></FormGroup>
                    </Col>
                   
                    <Col sm="auto">
                    <Button color="light" className="btn-sm" onClick={()=>this.resetFilter()}>Reset</Button>
                      <Button color="primary" className="btn-sm ml-2" onClick={()=>this.applyFilter()}>Apply</Button>
                    </Col>
                    
                    </Row>
                  </CardBody>
                  </Card>}

        {this.state.isShowQueuePrev==true &&  <Card className="mb-0">
              
              <CardBody className="p-0">
              <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr>
                    <th width="71" style={{minWidth:'71px'}}></th>
                    {global.isShowArabicLanguage==false &&<th className="text-center" onClick={()=>this.sortByQueueId()}>Q.ID</th>}
                    {global.isShowArabicLanguage==true &&<th className="text-center" onClick={()=>this.sortByQueueId()}>رمز الطابور</th>}
                    <th className="bg-warning text-center" onClick={()=>this.sortByInQueue()} >In Queue</th>
                    <th onClick={()=>this.sortByName()} className="text-left">Branch</th>
                    <th onClick={()=>this.sortByTicket()}>Total #Tickets</th>
                    <th onClick={()=>this.sortByFastPass()}>Fast Pass</th>
                    <th onClick={()=>this.sortByCounters()} >Counters</th>
                    <th onClick={()=>this.sortByAgents()}>Agents</th>
                    <th  onClick={()=>this.sortByAST()}>AST</th>
                    <th  onClick={()=>this.sortByAWT()}>AWT</th>
                    <th></th>
                  </tr>
                  
                  </thead>
                  <tbody>
                  {this.state.allQueue.map(queue => (
                      
                       <tr >
                         <td className="text-right">
                           <div className="checkbox form-check">
                               <input type="checkbox" className="form-check-input" checked={queue.queue.isChecked} onChange={()=>this.select(queue.queue)}></input>
                               <label  className="form-check-label form-check-label"></label>
                           </div>
                         </td>
                        <td className="text-center">{queue.queue.queueNumber}</td>
                         <td className="bg-warning text-center">{queue.noOfInQueueTickets} </td>
                         <td>{queue.queue.branch.branchName}
                             <p className="text-light-grey"><i className="icon2-location"></i> {queue.queue.branch.address}</p>
                         </td>
                         <td className="text-center">{queue.noOfTickets}</td>
                         <td className="text-center">{queue.noOfFastpassTicket}</td>
                         <td className="text-center">
                           <span style={{color:'#1ac38d'}}>{queue.noOfServingCounters}</span><span className="text-light-grey">/{queue.noOfTotalCounters}</span></td>
                         <td className="text-center">{queue.noOfAgents}</td>
                         <td className="text-center">{queue.queue.AST} <small className="text-light-grey">min</small></td>
                         <td className="text-center">{queue.queue.AWT} <small className="text-light-grey">min</small></td>
                         <td className="text-center">
                         <Link className="ml-4" onClick={()=>this.showMenu(queue.queue.queueId)}><i className="f20 icon2-menu-options text-light-grey"></i></Link>
                          {queue.isShowMenu==true && 
                          <Dropdown isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu:false }); }} >
                                  <DropdownToggle
                                    tag="span"
                                    onClick={() => {this.setState({ isShowDropDownMenu:false })}}
                                    data-toggle="dropdown"
                                    >
                                  </DropdownToggle>
                                  <DropdownMenu>
                                <DropdownItem onClick={()=>this.queueDetails(queue.queue.queueId)}>Details</DropdownItem>
                                
                                  </DropdownMenu>
                            </Dropdown>}
                         </td>
                       </tr>
                  ))}
               </tbody>
                </Table>
               
              </CardBody>
            </Card>}
          
          


      


      </div>
    );
  }
}

export default Breadcrumbs;
