import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { AppNavbarBrand } from '@coreui/react';
import logo from '../assets/img/brand/logo.png';



class Landing extends Component {
  render() {
    return (
      <div className="app flex-row landing-page">
        <Container className="text-center">
        <div className="login-logo mb-5 pb-5">
          <AppNavbarBrand
            full={{ src: logo, width: 250, alt: 'Taboor Logo' }}
          />
        </div>

          <Row className="justify-content-center">
            <Col md="6">
              <div className="landing-button">
                  <Link to="/register">
                    <Button>Service Centers</Button>
                  </Link>
              </div>
            </Col>
            <Col md="6">
              <div className="landing-button">
                  <Link to="/login">
                    <Button>Super Admins</Button>
                  </Link>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Landing;
