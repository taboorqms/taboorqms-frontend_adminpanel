import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table,FormGroup,
  ButtonDropdown,  DropdownItem,  DropdownMenu,  DropdownToggle,Input,Button } from 'reactstrap';

import { Link, NavLink } from 'react-router-dom';
import * as router from 'react-router-dom';
import { Badge, UncontrolledDropdown, Nav, NavItem } from 'reactstrap';
import {
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
// import routes from '../../../routes';

import axios from "axios";

import routes from './routes';
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
class Branch extends Component {
  tempAvialableBranches=[]
  totalBranches=0
  componentDidMount()
  {
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/branch/getListing';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode==0)
      {
        
        for(var i=0;i<responseJson.data.branchList.length;i++)
        {
          var oldDate=new Date(responseJson.data.branchList[i].branch['createdTime'])
          responseJson.data.branchList[i].branch['date']=oldDate
          var hms = responseJson.data.branchList[i].branch.averageServiceTime;  
          var a = hms.split(':'); 
          var minutes = (+a[0]) * 60 + (+a[1]); 
          console.log(minutes);
          var getDate=oldDate.getDate()+"."+(oldDate.getMonth()+1)+"."+oldDate.getFullYear()
          responseJson.data.branchList[i].branch['AST']=minutes
          responseJson.data.branchList[i].branch['AddedOn']=getDate
        }
        this.tempAvialableBranches.push(responseJson.data.branchList)
        this.totalBranches=this.tempAvialableBranches[0].length
        this.setState({
          allBranches:responseJson.data.branchList,
         
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  search(branchName)
  {
    
    if(branchName!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempAvialableBranches[0].length;i++)
      {
        if(this.tempAvialableBranches[0][i].branch.branchName.includes(branchName))
        {
          data.push(this.tempAvialableBranches[0][i])
          this.setState({
            allBranches:data
          })
          isMatch=true
          this.totalBranches=data.length
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
        }
      }
      if(isMatch==false)
      {
        this.totalBranches=0
        this.setState({allBranches:[]})
      }
    }
    else
    {
      this.totalBranches=this.tempAvialableBranches[0].length
      this.setState({
        allBranches:this.tempAvialableBranches[0]
      })
    }
  
  }
  changeSearch(branchName)
  {
    if(branchName=='')
    { 
      this.totalBranches=this.tempAvialableBranches[0].length
      this.setState({
        allBranches:this.tempAvialableBranches[0]
      })
    }
    this.search(branchName)
    this.setState({
    searchBranch:branchName
    })
  }
  sortByStatus()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
    sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
       return Number(a.branch.workingStatus)  - Number(b.branch.workingStatus);
    })
    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
    
  }
  sortByName()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
      sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>a.branch.branchName.localeCompare(b.branch.branchName))
      console.log(sortedProductsAsc)
      this.setState({
          allBranches:sortedProductsAsc
      })
    }
    
  }
  sortByCounters()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
    sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
       return Number(a.noOfTotalCounters)  - Number(b.noOfTotalCounters);
    })
    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
    
  }
  sortByAgents()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
    sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
       return Number(a.noOfAgents)  - Number(b.noOfAgents);
    })
    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
  }
  sortByDates()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
    sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
       return (a.branch.date)  - (b.branch.date);
    })
    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
  }
  sortByAST()
  {
    if(this.tempAvialableBranches.length!=0)
    {
      let sortedProductsAsc;
    sortedProductsAsc= this.tempAvialableBranches[0].sort((a,b)=>{
       return Number(a.branch.AST)  - Number(b.branch.AST);
    })
    console.log(sortedProductsAsc)
    this.setState({
        allBranches:sortedProductsAsc
    })
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      allBranches:[],
      searchBranch:'',
      isShowFilters:false
    };
  
  }
  branchDetails(id)
  {
    this.props.history.push(
      {
        pathname:'/branch-details',
        branchId:id
      }
    
    )
  }
  render() {
    return (
      <div className="animated fadeIn">
        <DefaultHeader/>
        <Row className="mb-3 hidden">
          
          <Col sm="4">
            <h3>
            <ButtonDropdown id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                &nbsp;
                </DropdownToggle>
                <DropdownMenu left>
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              <div className="checkbox form-check d-inline-block ml-2">
                  <input id="checkbox1" name="checkbox1" type="checkbox" className="form-check-input"></input>
                  <label htmlFor="checkbox1" className="form-check-label form-check-label">309&nbsp;Users</label>
              </div>
              </h3>
          </Col>

          <Col sm="8" className="text-right">
          
            <div className="float-right">
            <button className="btn btn-outline-secondary ml-2" onClick={()=>{this.setState({isShowFilters:!this.state.isShowFilters})}}>Filters <i className="icon2-filter2"></i></button>
              <ButtonDropdown className="ml-2" id="pagedrop" isOpen={this.state.isShowSortByMenu} toggle={() =>{this.setState({isShowSortByMenu:!this.state.isShowSortByMenu})}} >
                <DropdownToggle className="p-0 mb-0 text-light-grey" color="transparent"
                onClick={() => {this.setState({ isShowSortByMenu:false })}}
                >
                <button className="btn btn-outline-secondary">Sort By <i className="icon2-sort-descending"></i></button>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem onClick={()=>this.sortBy('ascending')}><input type="radio" checked={this.state.isSortBy=='ascending'} name="sort-order" onChange={()=>{}}></input> Ascending</DropdownItem>
                  <DropdownItem onClick={()=>this.sortBy('descending')}><input type="radio" checked={this.state.isSortBy=='descending'} name="sort-order" onChange={()=>{}}></input> Descending</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            <button className="btn btn-outline-warning ml-2"><i className="icon2-addition-sign" onClick={()=>this.props.history.push('/base/cards')}></i></button>
            </div>
            <div className="input-group float-right w-auto">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                value={this.state.searchBranch} onChange={(e) => this.changeSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.search()
                  }
                }}
                >

                  
                </input>
              <div className="input-group-append" onClick={()=>this.search()}><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="clearfix"></div>

          </Col>
        </Row>



            <Card className="mb-0">
              
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                  <tr >
                    <th></th>
                    <th className="text-center" onClick={()=>this.sortByStatus()}>ID</th>
                    <th onClick={()=>this.sortByName()} className="text-left">Name</th>
                    <th>Role</th>
                    <th onClick={()=>this.sortByCounters()}>Privileges </th>
                    <th onClick={()=>this.sortByAgents()}>Branches</th>
                    <th onClick={()=>this.sortByAST()}>Last Login</th>
                    <th onClick={()=>this.sortByDates()} >Created On</th>
                    <th></th>
                  </tr>
                  {this.state.isShowFilters==true && <tr id="filter-row">
                    <th></th>
                    
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line">
                        <select className="form-control">
                          <option value='0' key='0'>
                            Select Option
                          </option>
                          <option value='0' key='0'>
                            Option 2
                          </option>
                        </select>
                      </FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line">
                        <select className="form-control">
                          <option value='0' key='0'>
                            Select Option
                          </option>
                          <option value='0' key='0'>
                            Option 2
                          </option>
                        </select>
                      </FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    <th className="text-center">
                      <FormGroup className="input-line"><Input type="text" /></FormGroup>
                    </th>
                    
                    <th>
                      <Button color="primary" className="btn-sm">Apply</Button>
                    </th>
                  </tr>}
                  </thead>
                  <tbody>
                  {this.state.allBranches.map(branch => (
                 
                
                      <tr onClick={()=>this.branchDetails(branch.branch.branchId)}>
                      <td className="text-center">
                        <div className="checkbox form-check">
                            <input id="checkbox1" name="checkbox1" type="checkbox" className="form-check-input"></input>
                            <label htmlFor="checkbox1" className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">
                      {branch.branch.workingStatus==true && <span  className="badge badge-success">Open</span>}
                      {branch.branch.workingStatus==false && <span className="badge badge-danger">Closed</span>}
                      </td>
                      <td>{branch.branch.branchName}</td>
                      <td>
                      <FormGroup className="input-line">
                        <select className="form-control" onChange={this.changeServiceSector} >
                          {branch.providedServices.map(service => (
                            <option value={service.serviceId} key={service.serviceId}>
                              {service.serviceName}
                            </option>
                          ))}
                        </select>
                        </FormGroup>

                      </td>
                          <td className="text-center">{branch.noOfTotalCounters}</td>
                      <td className="text-center">{branch.noOfAgents}</td>
                      <td className="text-center">{branch.branch.AST} min</td>
                      <td className="text-center">{branch.branch.AddedOn}</td>
                      <td className="text-center">
                        <i className="icon2-smile text-success f20"></i>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>

                    /* <tr>
                      <td className="text-center">
                        <div className="checkbox form-check">
                            <input id="checkbox1" name="checkbox1" type="checkbox" className="form-check-input"></input>
                            <label htmlFor="checkbox1" className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">
                      <span className="badge badge-danger">Closed</span>
                      </td>
                      <td>Branch's Name</td>
                      <td>Lorem ipsum doller sit amet</td>
                      <td className="text-center">8</td>
                      <td className="text-center">2</td>
                      <td className="text-center">35 min</td>
                      <td className="text-center">12.4.19</td>
                      <td className="text-center">
                        <i className="icon2-smile text-warning f20"></i>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>
                    <tr>
                      <td className="text-center">
                        <div className="checkbox form-check">
                            <input id="checkbox1" name="checkbox1" type="checkbox" className="form-check-input"></input>
                            <label htmlFor="checkbox1" className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">
                      <span className="badge badge-success">Open</span>
                      </td>
                      <td>Branch's Name</td>
                      <td>Lorem ipsum doller sit amet</td>
                      <td className="text-center">8</td>
                      <td className="text-center">2</td>
                      <td className="text-center">35 min</td>
                      <td className="text-center">12.4.19</td>
                      <td className="text-center">
                        <i className="icon2-smile text-success f20"></i>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>
                    <tr>
                      <td className="text-center">
                        <div className="checkbox form-check">
                            <input id="checkbox1" name="checkbox1" type="checkbox" className="form-check-input"></input>
                            <label htmlFor="checkbox1" className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">
                      <span className="badge badge-success">Open</span>
                      </td>
                      <td>Branch's Name</td>
                      <td>Lorem ipsum doller sit amet</td>
                      <td className="text-center">8</td>
                      <td className="text-center">2</td>
                      <td className="text-center">35 min</td>
                      <td className="text-center">12.4.19</td>
                      <td className="text-center">
                        <i className="icon2-smile text-success f20"></i>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>
                    <tr>
                      <td className="text-center">
                        <div className="checkbox form-check">
                            <input id="checkbox1" name="checkbox1" type="checkbox" className="form-check-input"></input>
                            <label htmlFor="checkbox1" className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">
                      <span className="badge badge-success">Open</span>
                      </td>
                      <td>Branch's Name</td>
                      <td>Lorem ipsum doller sit amet</td>
                      <td className="text-center">8</td>
                      <td className="text-center">2</td>
                      <td className="text-center">35 min</td>
                      <td className="text-center">12.4.19</td>
                      <td className="text-center">
                        <i className="icon2-smile text-warning f20"></i>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>
                    <tr>
                      <td className="text-center">
                        <div className="checkbox form-check">
                            <input id="checkbox1" name="checkbox1" type="checkbox" className="form-check-input"></input>
                            <label htmlFor="checkbox1" className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">
                      <span className="badge badge-danger">Closed</span>
                      </td>
                      <td>Branch's Name</td>
                      <td>Lorem ipsum doller sit amet</td>
                      <td className="text-center">8</td>
                      <td className="text-center">2</td>
                      <td className="text-center">35 min</td>
                      <td className="text-center">12.4.19</td>
                      <td className="text-center">
                        <i className="icon2-smile text-success f20"></i>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>
                    <tr>
                      <td className="text-center">
                        <div className="checkbox form-check">
                            <input id="checkbox1" name="checkbox1" type="checkbox" className="form-check-input"></input>
                            <label htmlFor="checkbox1" className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">
                      <span className="badge badge-success">Open</span>
                      </td>
                      <td>Branch's Name</td>
                      <td>Lorem ipsum doller sit amet</td>
                      <td className="text-center">8</td>
                      <td className="text-center">2</td>
                      <td className="text-center">35 min</td>
                      <td className="text-center">12.4.19</td>
                      <td className="text-center">
                        <i className="icon2-smile text-warning f20"></i>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr>
                    <tr>
                      <td className="text-center">
                        <div className="checkbox form-check">
                            <input id="checkbox1" name="checkbox1" type="checkbox" className="form-check-input"></input>
                            <label htmlFor="checkbox1" className="form-check-label form-check-label"></label>
                        </div>
                      </td>
                      <td className="text-center">
                      <span className="badge badge-success">Open</span>
                      </td>
                      <td>Branch's Name</td>
                      <td>Lorem ipsum doller sit amet</td>
                      <td className="text-center">8</td>
                      <td className="text-center">2</td>
                      <td className="text-center">35 min</td>
                      <td className="text-center">12.4.19</td>
                      <td className="text-center">
                        <i className="icon2-smile text-success f20"></i>
                      </td>
                      <td className="text-center"><Link><i className="f20 icon2-menu-options text-light-grey"></i></Link></td>
                    </tr> */
                 

                  
                  
                   ))}
                   </tbody>
                </Table>
               
              </CardBody>
            </Card>
          
          


      


      </div>
    );
  }
}

export default Branch;
