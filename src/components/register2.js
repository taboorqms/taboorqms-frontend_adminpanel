import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { Button, Card, CardBody, CardGroup, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, 
  Row, Table, Label, FormGroup, Nav, NavItem, NavLink, TabContent, TabPane,ButtonDropdown, 
  DropdownItem,  DropdownMenu,  DropdownToggle, Alert, Modal, ModalBody, ModalFooter, ModalHeader, } from 'reactstrap';
import { AppNavbarBrand } from '@coreui/react';
import logo from '../assets/img/brand/logo.png';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';
import Steps, { Step } from 'rc-steps';
import { AppSwitch } from '@coreui/react';
import { Route, Redirect } from 'react-router-dom';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import LoadingBar from 'react-top-loading-bar'
var formData=new FormData()
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
class Register extends Component {
  userServiceCenterId=0
  tempPayment=[]
  tempInstallments=[]
  tempEasyPayments=[]
  actualTotalAmount=0
  paymentType=[{
    'name':'Annual',id:1
  },{
    'name':'Montly',id:2
  }]
  amount='' 
  userServiceCenterId=0
  tempPayment=[]
  tempCurrency=[]
  tempSinglePlanCurrency=[]
  paymentType=[{
    'name':'Annual',id:1
  },{
    'name':'Montly',id:2
  }]
  counteries=[
    {"name":"Afghanistan"},
    {"name":"Albania"},
    {"name":"Algeria"},
   {"name":"Andorra"},
   {"name":"Angola"},
   {"name":"Anguilla"},
   {"name":"Antigua &amp; Barbuda"},
   {"name":"Argentina"},
   {"name": "Armenia"},
   {"name": "Aruba"},
   {"name": "Australia"},
   {"name": "Austria"},
   {"name": "Azerbaijan"},
   {"name":  "Bahamas"},
   {"name":  "Bahrain"},
   {"name":  "Bangladesh"},
   {"name":  "Barbados"},
   {"name": "Belarus"},
   {"name": "Belgium"},
   {"name": "Belize"},
   {"name": "Benin"},
   {"name": "Bermuda"},
   {"name": "Bhutan"},
   {"name":"Bolivia"},
   {"name": "Bosnia &amp; Herzegovina"},
   {"name": "Botswana"},
   {"name": "Brazil"},
   {"name": "British Virgin Islands"},
   {"name": "Brunei"},
   {"name":"Bulgaria"},
   {"name": "Burkina Faso"},
   {"name": "Burundi"},
   {"name": "Cambodia"},
   {"name": "Cameroon"},
   {"name": "Cape Verde"},
   {"name": "Cayman Islands"},
   {"name": "Chad"},
   {"name": "Chile"},
   {"name": "China"},
   {"name": "Colombia"},
   {"name": "Congo"},
   {"name": "Cook Islands"},
   {"name": "Costa Rica"},
   {"name": "Cote D Ivoire"},
   {"name":"Croatia"},
   {"name":"Cruise Ship"},
   {"name": "Cuba"},
   {"name": "Cyprus"},
   {"name": "Czech Republic"},
   {"name": "Denmark"},
   {"name": "Djibouti"},
   {"name":"Dominica"},
   {"name": "Dominican Republic"},
   {"name": "Ecuador"},
   {"name": "Egypt"},
   {"name":"El Salvador"},
   {"name":"Equatorial Guinea"},
   {"name": "Estonia"},
   {"name": "Ethiopia"},
   {"name": "Falkland Islands"},
   {"name": "Faroe Islands"},
   {"name": "Fiji"},
   {"name":"Finland"},
   {"name": "France"},
   {"name": "French Polynesia"},
   {"name": "French West Indies"},
   {"name":  "Gabon"},
   {"name": "Gambia"},
   {"name": "Georgia"},
   {"name": "Germany"},
   {"name": "Ghana"},
   {"name": "Gibraltar"},
   {"name": "Greece"},
   {"name": "Greenland"},
   {"name": "Grenada"},
   {"name": "Guam"},
   {"name":  "Guatemala"},
   {"name":  "Guernsey"},
   {"name": "Guinea"},
   {"name": "Guinea Bissau"},
   {"name": "Guyana"},
   {"name":"Haiti"},
   {"name": "Honduras"},
   {"name": "Hong Kong"},
   {"name": "Hungary"},
   {"name": "Iceland"},
   {"name": "India"},
   {"name": "Indonesia"},
   {"name":  "Iran"},
   {"name":  "Iraq"},
   {"name": "Ireland"},
   {"name": "Isle of Man"},
   {"name": "Israel"},
   {"name": "Italy"},
   {"name": "Jamaica"},
   {"name": "Japan"},
   {"name": "Jersey"},
   {"name": "Jordan"},
   {"name": "Kazakhstan"},
   {"name": "Kenya"},
   {"name": "Kuwait"},
   {"name": "Kyrgyz Republic"},
   {"name": "Laos"},
   {"name":"Latvia"},
   {"name":"Lebanon"},
   {"name": "Lesotho"},
   {"name": "Liberia"},
   {"name":"Libya"},
   {"name":"Liechtenstein"},
   {"name":"Lithuania"},
   {"name":"Luxembourg"},
   {"name":"Macau"},
   {"name": "Macedonia"},
   {"name": "Madagascar"},
   {"name": "Malawi"},
   {"name": "Malaysia"},
   {"name": "Maldives"},
   {"name": "Mali"},
   {"name": "Malta"},
   {"name": "Mauritania"},
   {"name": "Mauritius"},
   {"name": "Mexico"},
   {"name":"Moldova"},
   {"name": "Monaco"},
   {"name":"Mongolia"},
   {"name":"Montenegro"},
   {"name":"Montserrat"},
   {"name":"Morocco"},
   {"name":"Mozambique"},
   {"name":"Namibia"},
   {"name":"Nepal"},
   {"name":"Netherlands"},
   {"name":"Netherlands Antilles"},
   {"name":"New Caledonia"},
   {"name": "New Zealand"},
   {"name": "Nicaragua"},
   {"name": "Niger"},
   {"name":  "Nigeria"},
   {"name": "Norway"},
   {"name": "Oman"},
   {"name": "Pakistan"},
   {"name": "Palestine"},
   {"name": "Panama"},
   {"name":  "Papua New Guinea"},
   {"name":  "Paraguay"},
   {"name": "Peru"},
   {"name": "Philippines"},
   {"name": "Poland"},
   {"name": "Portugal"},
   {"name": "Puerto Rico"},
   {"name": "Qatar"},
   {"name": "Reunion"},
   {"name": "Romania"},
   {"name": "Russia"},
   {"name": "Rwanda"},
   {"name": "Saint Pierre &amp; Miquelon"},
   {"name":"Samoa"},
   {"name":"San Marino"},
   {"name": "Satellite"},
   {"name": "Saudi Arabia"},
   {"name": "Senegal"},
   {"name": "Serbia"},
   {"name":"Seychelles"},
   {"name":"Sierra Leone"},
   {"name":"Singapore"},
   {"name":"Slovakia"},
   {"name":"Slovenia"},
   {"name": "South Africa"},
   {"name": "South Korea"},
   {"name":"Spain"},
   {"name":"Sri Lanka"},
   {"name": "St Kitts &amp; Nevis"},
   {"name": "St Lucia"},
   {"name": "St Vincent"},
   {"name": "St. Lucia"},
   {"name":"Sudan"},
   {"name": "Suriname"},
   {"name": "Swaziland"},
   {"name":"Sweden"},
   {"name":"Switzerland"},
   {"name":"Syria"},
   {"name":"Taiwan"},
   {"name":"Tajikistan"},
   {"name":"Tanzania"},
   {"name":"Thailand"},
   {"name": "Timor L'Este"},
   {"name": "Togo"},
   {"name":"Tonga"},
   {"name":"Trinidad &amp; Tobago"},
   {"name":"Tunisia"},
   {"name":"Turkey"},
   {"name": "Turkmenistan"},
   {"name": "Turks &amp; Caicos"},
   {"name": "Uganda"},
   {"name": "Ukraine"},
   {"name":"United Arab Emirates"},
   {"name":"United Kingdom"},
   {"name":"Uruguay"},
   {"name":"Uzbekistan"},
   {"name":"Venezuela"},
   {"name":"Vietnam"},
   {"name":"Virgin Islands (US)"},
   {"name":"Yemen"},
   {"name":"Zambia"},
   {"name":"Zimbabwe"}

  ]
    phoneCodes=[
      {
          "code": "+1",
          "name": "Canada"
      },
      {
          "code": "+1",
          "name": "United States"
      },
      {
          "code": "+1 242",
          "name": "Bahamas"
      },
      {
          "code": "+1 246",
          "name": "Barbados"
      },
      {
          "code": "+1 264",
          "name": "Anguilla"
      },
      {
          "code": "+1 268",
          "name": "Antigua and Barbuda"
      },
      {
          "code": "+1 268",
          "name": "Barbuda"
      },
      {
          "code": "+1 284",
          "name": "British Virgin Islands"
      },
      {
          "code": "+1 340",
          "name": "U.S. Virgin Islands"
      },
      {
          "code": "+1 441",
          "name": "Bermuda"
      },
      {
          "code": "+1 473",
          "name": "Grenada"
      },
      {
          "code": "+1 649",
          "name": "Turks and Caicos Islands"
      },
      {
          "code": "+1 670",
          "name": "Northern Mariana Islands"
      },
      {
          "code": "+1 671",
          "name": "Guam"
      },
      {
          "code": "+1 684",
          "name": "American Samoa"
      },
      {
          "code": "+1 767",
          "name": "Dominica"
      },
      {
          "code": "+1 787",
          "name": "Puerto Rico"
      },
      {
          "code": "+1 808",
          "name": "Midway Island"
      },
      {
          "code": "+1 808",
          "name": "Wake Island"
      },
      {
          "code": "+1 809",
          "name": "Dominican Republic"
      },
      {
          "code": "+1 868",
          "name": "Trinidad and Tobago"
      },
      {
          "code": "+1 869",
          "name": "Nevis"
      },
      {
          "code": "+1 876",
          "name": "Jamaica"
      },
      {
          "code": "+1664",
          "name": "Montserrat"
      },
      {
          "code": "+20",
          "name": "Egypt"
      },
      {
          "code": "+212",
          "name": "Morocco"
      },
      {
          "code": "+213",
          "name": "Algeria"
      },
      {
          "code": "+216",
          "name": "Tunisia"
      },
      {
          "code": "+218",
          "name": "Libya"
      },
      {
          "code": "+220",
          "name": "Gambia"
      },
      {
          "code": "+221",
          "name": "Senegal"
      },
      {
          "code": "+222",
          "name": "Mauritania"
      },
      {
          "code": "+223",
          "name": "Mali"
      },
      {
          "code": "+224",
          "name": "Guinea"
      },
      {
          "code": "+225",
          "name": "Ivory Coast"
      },
      {
          "code": "+226",
          "name": "Burkina Faso"
      },
      {
          "code": "+227",
          "name": "Niger"
      },
      {
          "code": "+228",
          "name": "Togo"
      },
      {
          "code": "+229",
          "name": "Benin"
      },
      {
          "code": "+230",
          "name": "Mauritius"
      },
      {
          "code": "+231",
          "name": "Liberia"
      },
      {
          "code": "+232",
          "name": "Sierra Leone"
      },
      {
          "code": "+233",
          "name": "Ghana"
      },
      {
          "code": "+234",
          "name": "Nigeria"
      },
      {
          "code": "+235",
          "name": "Chad"
      },
      {
          "code": "+236",
          "name": "Central African Republic"
      },
      {
          "code": "+237",
          "name": "Cameroon"
      },
      {
          "code": "+238",
          "name": "Cape Verde"
      },
      {
          "code": "+240",
          "name": "Equatorial Guinea"
      },
      {
          "code": "+241",
          "name": "Gabon"
      },
      {
          "code": "+242",
          "name": "Congo"
      },
      {
          "code": "+243",
          "name": "Congo, Dem. Rep. of (Zaire)"
      },
      {
          "code": "+244",
          "name": "Angola"
      },
      {
          "code": "+245",
          "name": "Guinea-Bissau"
      },
      {
          "code": "+246",
          "name": "British Indian Ocean Territory"
      },
      {
          "code": "+246",
          "name": "Diego Garcia"
      },
      {
          "code": "+247",
          "name": "Ascension"
      },
      {
          "code": "+248",
          "name": "Seychelles"
      },
      {
          "code": "+249",
          "name": "Sudan"
      },
      {
          "code": "+250",
          "name": "Rwanda"
      },
      {
          "code": "+251",
          "name": "Ethiopia"
      },
      {
          "code": "+253",
          "name": "Djibouti"
      },
      {
          "code": "+254",
          "name": "Kenya"
      },
      {
          "code": "+255",
          "name": "Tanzania"
      },
      {
          "code": "+255",
          "name": "Zanzibar"
      },
      {
          "code": "+256",
          "name": "Uganda"
      },
      {
          "code": "+257",
          "name": "Burundi"
      },
      {
          "code": "+260",
          "name": "Zambia"
      },
      {
          "code": "+261",
          "name": "Madagascar"
      },
      {
          "code": "+262",
          "name": "Mayotte"
      },
      {
          "code": "+262",
          "name": "Reunion"
      },
      {
          "code": "+263",
          "name": "Zimbabwe"
      },
      {
          "code": "+264",
          "name": "Namibia"
      },
      {
          "code": "+265",
          "name": "Malawi"
      },
      {
          "code": "+266",
          "name": "Lesotho"
      },
      {
          "code": "+267",
          "name": "Botswana"
      },
      {
          "code": "+268",
          "name": "Swaziland"
      },
      {
          "code": "+269",
          "name": "Comoros"
      },
      {
          "code": "+27",
          "name": "South Africa"
      },
      {
          "code": "+291",
          "name": "Eritrea"
      },
      {
          "code": "+297",
          "name": "Aruba"
      },
      {
          "code": "+298",
          "name": "Faroe Islands"
      },
      {
          "code": "+299",
          "name": "Greenland"
      },
      {
          "code": "+30",
          "name": "Greece"
      },
      {
          "code": "+31",
          "name": "Netherlands"
      },
      {
          "code": "+32",
          "name": "Belgium"
      },
      {
          "code": "+33",
          "name": "France"
      },
      {
          "code": "+34",
          "name": "Spain"
      },
      {
          "code": "+350",
          "name": "Gibraltar"
      },
      {
          "code": "+351",
          "name": "Portugal"
      },
      {
          "code": "+352",
          "name": "Luxembourg"
      },
      {
          "code": "+353",
          "name": "Ireland"
      },
      {
          "code": "+354",
          "name": "Iceland"
      },
      {
          "code": "+355",
          "name": "Albania"
      },
      {
          "code": "+356",
          "name": "Malta"
      },
      {
          "code": "+358",
          "name": "Finland"
      },
      {
          "code": "+359",
          "name": "Bulgaria"
      },
      {
          "code": "+36",
          "name": "Hungary"
      },
      {
          "code": "+370",
          "name": "Lithuania"
      },
      {
          "code": "+371",
          "name": "Latvia"
      },
      {
          "code": "+372",
          "name": "Estonia"
      },
      {
          "code": "+373",
          "name": "Moldova"
      },
      {
          "code": "+374",
          "name": "Armenia"
      },
      {
          "code": "+375",
          "name": "Belarus"
      },
      {
          "code": "+376",
          "name": "Andorra"
      },
      {
          "code": "+377",
          "name": "Monaco"
      },
      {
          "code": "+378",
          "name": "San Marino"
      },
      {
          "code": "+380",
          "name": "Ukraine"
      },
      {
          "code": "+381",
          "name": "Serbia"
      },
      {
          "code": "+382",
          "name": "Montenegro"
      },
      {
          "code": "+385",
          "name": "Croatia"
      },
      {
          "code": "+386",
          "name": "Slovenia"
      },
      {
          "code": "+387",
          "name": "Bosnia and Herzegovina"
      },
      {
          "code": "+389",
          "name": "Macedonia"
      },
      {
          "code": "+39",
          "name": "Italy"
      },
      {
          "code": "+40",
          "name": "Romania"
      },
      {
          "code": "+41",
          "name": "Switzerland"
      },
      {
          "code": "+420",
          "name": "Czech Republic"
      },
      {
          "code": "+421",
          "name": "Slovakia"
      },
      {
          "code": "+423",
          "name": "Liechtenstein"
      },
      {
          "code": "+43",
          "name": "Austria"
      },
      {
          "code": "+44",
          "name": "United Kingdom"
      },
      {
          "code": "+45",
          "name": "Denmark"
      },
      {
          "code": "+46",
          "name": "Sweden"
      },
      {
          "code": "+47",
          "name": "Norway"
      },
      {
          "code": "+48",
          "name": "Poland"
      },
      {
          "code": "+49",
          "name": "Germany"
      },
      {
          "code": "+500",
          "name": "Falkland Islands"
      },
      {
          "code": "+500",
          "name": "South Georgia and the South Sandwich Islands"
      },
      {
          "code": "+501",
          "name": "Belize"
      },
      {
          "code": "+502",
          "name": "Guatemala"
      },
      {
          "code": "+503",
          "name": "El Salvador"
      },
      {
          "code": "+504",
          "name": "Honduras"
      },
      {
          "code": "+505",
          "name": "Nicaragua"
      },
      {
          "code": "+506",
          "name": "Costa Rica"
      },
      {
          "code": "+507",
          "name": "Panama"
      },
      {
          "code": "+509",
          "name": "Haiti"
      },
      {
          "code": "+51",
          "name": "Peru"
      },
      {
          "code": "+52",
          "name": "Mexico"
      },
      {
          "code": "+53",
          "name": "Cuba"
      },
      {
          "code": "+537",
          "name": "Cyprus"
      },
      {
          "code": "+54",
          "name": "Argentina"
      },
      {
          "code": "+55",
          "name": "Brazil"
      },
      {
          "code": "+56",
          "name": "Chile"
      },
      {
          "code": "+56",
          "name": "Easter Island"
      },
      {
          "code": "+57",
          "name": "Colombia"
      },
      {
          "code": "+58",
          "name": "Venezuela"
      },
      {
          "code": "+590",
          "name": "Guadeloupe"
      },
      {
          "code": "+591",
          "name": "Bolivia"
      },
      {
          "code": "+593",
          "name": "Ecuador"
      },
      {
          "code": "+594",
          "name": "French Guiana"
      },
      {
          "code": "+595",
          "name": "Guyana"
      },
      {
          "code": "+595",
          "name": "Paraguay"
      },
      {
          "code": "+596",
          "name": "French Antilles"
      },
      {
          "code": "+596",
          "name": "Martinique"
      },
      {
          "code": "+597",
          "name": "Suriname"
      },
      {
          "code": "+598",
          "name": "Uruguay"
      },
      {
          "code": "+599",
          "name": "Curacao"
      },
      {
          "code": "+599",
          "name": "Netherlands Antilles"
      },
      {
          "code": "+60",
          "name": "Malaysia"
      },
      {
          "code": "+61",
          "name": "Australia"
      },
      {
          "code": "+61",
          "name": "Christmas Island"
      },
      {
          "code": "+61",
          "name": "Cocos-Keeling Islands"
      },
      {
          "code": "+62",
          "name": "Indonesia"
      },
      {
          "code": "+63",
          "name": "Philippines"
      },
      {
          "code": "+64",
          "name": "New Zealand"
      },
      {
          "code": "+65",
          "name": "Singapore"
      },
      {
          "code": "+66",
          "name": "Thailand"
      },
      {
          "code": "+670",
          "name": "East Timor"
      },
      {
          "code": "+670",
          "name": "Timor Leste"
      },
      {
          "code": "+672",
          "name": "Australian External Territories"
      },
      {
          "code": "+672",
          "name": "Norfolk Island"
      },
      {
          "code": "+673",
          "name": "Brunei"
      },
      {
          "code": "+674",
          "name": "Nauru"
      },
      {
          "code": "+675",
          "name": "Papua New Guinea"
      },
      {
          "code": "+676",
          "name": "Tonga"
      },
      {
          "code": "+677",
          "name": "Solomon Islands"
      },
      {
          "code": "+678",
          "name": "Vanuatu"
      },
      {
          "code": "+679",
          "name": "Fiji"
      },
      {
          "code": "+680",
          "name": "Palau"
      },
      {
          "code": "+681",
          "name": "Wallis and Futuna"
      },
      {
          "code": "+682",
          "name": "Cook Islands"
      },
      {
          "code": "+683",
          "name": "Niue"
      },
      {
          "code": "+685",
          "name": "Samoa"
      },
      {
          "code": "+686",
          "name": "Kiribati"
      },
      {
          "code": "+687",
          "name": "New Caledonia"
      },
      {
          "code": "+688",
          "name": "Tuvalu"
      },
      {
          "code": "+689",
          "name": "French Polynesia"
      },
      {
          "code": "+690",
          "name": "Tokelau"
      },
      {
          "code": "+691",
          "name": "Micronesia"
      },
      {
          "code": "+692",
          "name": "Marshall Islands"
      },
      {
          "code": "+7",
          "name": "Russia"
      },
      {
          "code": "+7 7",
          "name": "Kazakhstan"
      },
      {
          "code": "+7840",
          "name": "Abkhazia"
      },
      {
          "code": "+81",
          "name": "Japan"
      },
      {
          "code": "+82",
          "name": "South Korea"
      },
      {
          "code": "+84",
          "name": "Vietnam"
      },
      {
          "code": "+850",
          "name": "North Korea"
      },
      {
          "code": "+852",
          "name": "Hong Kong SAR China"
      },
      {
          "code": "+853",
          "name": "Macau SAR China"
      },
      {
          "code": "+855",
          "name": "Cambodia"
      },
      {
          "code": "+856",
          "name": "Laos"
      },
      {
          "code": "+86",
          "name": "China"
      },
      {
          "code": "+880",
          "name": "Bangladesh"
      },
      {
          "code": "+886",
          "name": "Taiwan"
      },
      {
          "code": "+90",
          "name": "Turkey"
      },
      {
          "code": "+91",
          "name": "India"
      },
      {
          "code": "+92",
          "name": "Pakistan"
      },
      {
          "code": "+93",
          "name": "Afghanistan"
      },
      {
          "code": "+94",
          "name": "Sri Lanka"
      },
      {
          "code": "+95",
          "name": "Myanmar"
      },
      {
          "code": "+960",
          "name": "Maldives"
      },
      {
          "code": "+961",
          "name": "Lebanon"
      },
      {
          "code": "+962",
          "name": "Jordan"
      },
      {
          "code": "+963",
          "name": "Syria"
      },
      {
          "code": "+964",
          "name": "Iraq"
      },
      {
          "code": "+965",
          "name": "Kuwait"
      },
      {
          "code": "+966",
          "name": "Saudi Arabia"
      },
      {
          "code": "+967",
          "name": "Yemen"
      },
      {
          "code": "+968",
          "name": "Oman"
      },
      {
          "code": "+970",
          "name": "Palestinian Territory"
      },
      {
          "code": "+971",
          "name": "United Arab Emirates"
      },
      {
          "code": "+972",
          "name": "Israel"
      },
      {
          "code": "+973",
          "name": "Bahrain"
      },
      {
          "code": "+974",
          "name": "Qatar"
      },
      {
          "code": "+975",
          "name": "Bhutan"
      },
      {
          "code": "+976",
          "name": "Mongolia"
      },
      {
          "code": "+977",
          "name": "Nepal"
      },
      {
          "code": "+98",
          "name": "Iran"
      },
      {
          "code": "+992",
          "name": "Tajikistan"
      },
      {
          "code": "+993",
          "name": "Turkmenistan"
      },
      {
          "code": "+994",
          "name": "Azerbaijan"
      },
      {
          "code": "+995",
          "name": "Georgia"
      },
      {
          "code": "+996",
          "name": "Kyrgyzstan"
      },
      {
          "code": "+998",
          "name": "Uzbekistan"
      }
]
  noOfBranches=[]
  planDetailObject={}
    constructor(props){
    super(props);
    this.state={
      progress:0,
      isEasyPaymentVal:false,
      isDisableCreate:false,
      uptoNoOfService:0,
      uptoNoOFSms:0,
      planPaymentDetail:{},
      userName:'',
      role:'',
      isError:false,
      branches:0,
      isEasyPayment:false,
      selectedInstallments:1,
      sDate:new Date(),
      selectedCurrency:'',
      serviceCenterName: '',serviceCenterNameValid: false,
      serviceCenterArabicName: '',serviceCenterArabicNameValid: false,
      selectedServiceSectorId:0,
      selectedPlanId:0,
      planSubscriptionType:2,
      allCurrency:[],
      singlePlanCurrency:[],
      selectedSinglePlanCurrency:'',
      planEnglishName:'',
      years : [
        { val: '01/2020',  name: '01/2020' },
        { val: '02/2020',  name: '02/2020' },
        { val: '03/2020',  name: '03/2020' },
        { val: '04/2020',  name: '04/2020' },
        { val: '05/2020',  name: '05/2020' },
        { val: '06/2020',  name: '06/2020' },
        { val: '07/2020',  name: '07/2020' },
        { val: '08/2020',  name: '08/2020' },
        { val: '09/2020',  name: '09/2020' },
        { val: '10/2020',  name: '10/2020' },
        { val: '11/2020',  name: '11/2020' },
        { val: '12/2020',  name: '12/2020' },
    
        { val: '01/2021',  name: '01/2021' },
        { val: '02/2021',  name: '02/2021' },
        { val: '03/2021',  name: '03/2021' },
        { val: '04/2021',  name: '04/2021' },
        { val: '05/2021',  name: '05/2021' },
        { val: '06/2021',  name: '06/2021' },
        { val: '07/2021',  name: '07/2021' },
        { val: '08/2021',  name: '08/2021' },
        { val: '09/2021',  name: '09/2021' },
        { val: '10/2021',  name: '10/2021' },
        { val: '11/2021',  name: '11/2021' },
        { val: '12/2021',  name: '12/2021' },
    
        { val: '01/2022',  name: '01/2022' },
        { val: '02/2022',  name: '02/2022' },
        { val: '03/2022',  name: '03/2022' },
        { val: '04/2022',  name: '04/2022' },
        { val: '05/2022',  name: '05/2022' },
        { val: '06/2022',  name: '06/2022' },
        { val: '07/2022',  name: '07/2022' },
        { val: '08/2022',  name: '08/2022' },
        { val: '09/2022',  name: '09/2022' },
        { val: '10/2022',  name: '10/2022' },
        { val: '11/2022',  name: '11/2022' },
        { val: '12/2022',  name: '12/2022' },
    
        { val: '01/2023',  name: '01/2023' },
        { val: '02/2023',  name: '02/2023' },
        { val: '03/2023',  name: '03/2023' },
        { val: '04/2023',  name: '04/2023' },
        { val: '05/2023',  name: '05/2023' },
        { val: '06/2023',  name: '06/2023' },
        { val: '07/2023',  name: '07/2023' },
        { val: '08/2023',  name: '08/2023' },
        { val: '09/2023',  name: '09/2023' },
        { val: '10/2023',  name: '10/2023' },
        { val: '11/2023',  name: '11/2023' },
        { val: '12/2023',  name: '12/2023' },
    
        { val: '01/2024',  name: '01/2024' },
        { val: '02/2024',  name: '02/2024' },
        { val: '03/2024',  name: '03/2024' },
        { val: '04/2024',  name: '04/2024' },
        { val: '05/2024',  name: '05/2024' },
        { val: '06/2024',  name: '06/2024' },
        { val: '07/2024',  name: '07/2024' },
        { val: '08/2024',  name: '08/2024' },
        { val: '09/2024',  name: '09/2024' },
        { val: '10/2024',  name: '10/2024' },
        { val: '11/2024',  name: '11/2024' },
        { val: '12/2024',  name: '12/2024' },
    
        { val: '01/2025',  name: '01/2025' },
        { val: '02/2025',  name: '02/2025' },
        { val: '03/2025',  name: '03/2025' },
        { val: '04/2025',  name: '04/2025' },
        { val: '05/2025',  name: '05/2025' },
        { val: '06/2025',  name: '06/2025' },
        { val: '07/2025',  name: '07/2025' },
        { val: '08/2025',  name: '08/2025' },
        { val: '09/2025',  name: '09/2025' },
        { val: '10/2025',  name: '10/2025' },
        { val: '11/2025',  name: '11/2025' },
        { val: '12/2025',  name: '12/2025' },
    
        { val: '01/2026',  name: '01/2026' },
        { val: '02/2026',  name: '02/2026' },
        { val: '03/2026',  name: '03/2026' },
        { val: '04/2026',  name: '04/2026' },
        { val: '05/2026',  name: '05/2026' },
        { val: '06/2026',  name: '06/2026' },
        { val: '07/2026',  name: '07/2026' },
        { val: '08/2026',  name: '08/2026' },
        { val: '09/2026',  name: '09/2026' },
        { val: '10/2026',  name: '10/2026' },
        { val: '11/2026',  name: '11/2026' },
        { val: '12/2026',  name: '12/2026' },
    
        { val: '01/2027',  name: '01/2027' },
        { val: '02/2027',  name: '02/2027' },
        { val: '03/2027',  name: '03/2027' },
        { val: '04/2027',  name: '04/2027' },
        { val: '05/2027',  name: '05/2027' },
        { val: '06/2027',  name: '06/2027' },
        { val: '07/2027',  name: '07/2027' },
        { val: '08/2027',  name: '08/2027' },
        { val: '09/2027',  name: '09/2027' },
        { val: '10/2027',  name: '10/2027' },
        { val: '11/2027',  name: '11/2027' },
        { val: '12/2027',  name: '12/2027' },
    
        { val: '01/2028',  name: '01/2028' },
        { val: '02/2028',  name: '02/2028' },
        { val: '03/2028',  name: '03/2028' },
        { val: '04/2028',  name: '04/2028' },
        { val: '05/2028',  name: '05/2028' },
        { val: '06/2028',  name: '06/2028' },
        { val: '07/2028',  name: '07/2028' },
        { val: '08/2028',  name: '08/2028' },
        { val: '09/2028',  name: '09/2028' },
        { val: '10/2028',  name: '10/2028' },
        { val: '11/2028',  name: '11/2028' },
        { val: '12/2028',  name: '12/2028' },
    
        { val: '01/2029',  name: '01/2029' },
        { val: '02/2029',  name: '02/2029' },
        { val: '03/2029',  name: '03/2029' },
        { val: '04/2029',  name: '04/2029' },
        { val: '05/2029',  name: '05/2029' },
        { val: '06/2029',  name: '06/2029' },
        { val: '07/2029',  name: '07/2029' },
        { val: '08/2029',  name: '08/2029' },
        { val: '09/2029',  name: '09/2029' },
        { val: '10/2029',  name: '10/2029' },
        { val: '11/2029',  name: '11/2029' },
        { val: '12/2029',  name: '12/2029' },
    
        { val: '01/2030',  name: '01/2030' },
        { val: '02/2030',  name: '02/2030' },
        { val: '03/2030',  name: '03/2030' },
        { val: '04/2030',  name: '04/2030' },
        { val: '05/2030',  name: '05/2030' },
        { val: '06/2030',  name: '06/2030' },
        { val: '07/2030',  name: '07/2030' },
        { val: '08/2030',  name: '08/2030' },
        { val: '09/2030',  name: '09/2030' },
        { val: '10/2030',  name: '10/2030' },
        { val: '11/2030',  name: '11/2030' },
        { val: '12/2030',  name: '12/2030' },
      ],
      tax:0,
      planArabicName:'',
      paymentAmount:0,
      isPaymentDetailModel:false,
      isSuccess:false,
      selectedPlanObject:{},
      selectedServiceObject:{},
      isShowUpload:true,
      priceAnnually:'',
      priceMonthly:'',
      isVisa:true,
      allEasyPayments:[],
      isMaster:false,
      isPaypal:false,
      selectedPlanBool:false,
      email: '', emailValid: false,
      password: '', passwordValid: false,
      country: '', countryValid: false,
      phone: '', phoneValid: false,
      discountValid:false,realDiscount:'0',
      realAmountValid:false,realAmount:'',
      graceDaysValid:false,graceDays:'',
      cardValid:false,cardNo:'',
      cvvNoValid:false,cvvNo:'',
      dateValue:new Date(),
      cardNameValid:false,cardName:'',
      passwordConfirm: '', passwordConfirmValid: false,
      formValid: false,
      isAnnualy:false,
      isMontly:true,
      paymentTypeId:4,
      totalAmount:0,
      selectedDate:'',
      paymentFormValid: false,
      errorMsg: {},
      phoneCode:'',
      paymentErrorMsg: {},
      serviceSector:[],
      paymentPlanList:[],
      next:0,
      image: null
    }
    
  }
  componentDidMount() {
    formData=new FormData()    
    this.setState({phoneCode:this.phoneCodes[0].code,country:this.counteries[0].name})
    // for(var i=2;i<12;i++)
    // {
      this.setState({
        userName:localStorage.getItem('userName'),
        role:localStorage.getItem('role')

      })
      this.tempInstallments.push(1)
      this.tempInstallments.push(2)
      this.tempInstallments.push(3)
      this.tempInstallments.push(4)
      this.tempInstallments.push(5)
      this.tempInstallments.push(6)
      this.tempInstallments.push(7)
      this.tempInstallments.push(8)
      this.tempInstallments.push(9)
      this.tempInstallments.push(10)
      this.tempInstallments.push(11)
      this.tempInstallments.push(12)     
    // }
    
    this.getAllServiceSector()

 }
  setStartDate(date)
  {
     this.setState({dateValue:date})
  }
  validateForm = () => {
    // const {serviceCenterNameValid, emailValid, passwordValid, passwordConfirmValid,countryValid,phoneValid} = this.state;
    // this.setState({
    //   formValid: serviceCenterNameValid && emailValid && passwordValid && passwordConfirmValid && phoneValid && countryValid
    // })
    const {serviceCenterNameValid, emailValid,phoneValid,serviceCenterArabicNameValid,passwordValid,passwordConfirmValid} = this.state;
    this.setState({
      formValid: serviceCenterNameValid && emailValid && phoneValid  && serviceCenterArabicNameValid && passwordValid && passwordConfirmValid
    })
  }

  validatePaymentForm = () => {
    const {discountValid} = this.state;
    this.setState({
      paymentFormValid: discountValid
    })
  }
  updateServiceCenterName = (serviceCenterName) => {
    this.setState({serviceCenterName}, this.validateServiceCenterName)
  }

  validateServiceCenterName = () => {
    const {serviceCenterName} = this.state;
    let serviceCenterNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (serviceCenterName.length == 0 ||serviceCenterName=='' ) {
      serviceCenterNameValid = false;
      errorMsg.serviceCenterName = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(serviceCenterName))
    // {
    //   serviceCenterNameValid = false;
    //   errorMsg.serviceCenterName = 'Only alphabets are allowed'
    // }
    this.setState({serviceCenterNameValid, errorMsg}, this.validateForm)
  }
  updateArabicServiceCenterName = (serviceCenterArabicName) => {
    this.setState({serviceCenterArabicName}, this.validateArabicServiceCenterName)
  }

  validateArabicServiceCenterName = () => {
    const {serviceCenterArabicName} = this.state;
    let serviceCenterArabicNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (serviceCenterArabicName.length == 0 ||serviceCenterArabicName=='' ) {
      serviceCenterArabicNameValid = false;
      errorMsg.serviceCenterArabicName = 'Required'
    }
    else if(!/^[\u0621-\u064A\040]+$/.test(serviceCenterArabicName))
    {
      serviceCenterArabicNameValid = false;
      errorMsg.serviceCenterArabicName = 'Only arabic alphabets are allowed'
    }
    this.setState({serviceCenterArabicNameValid, errorMsg}, this.validateForm)
  }
  // updateCountryName = (country) => {
  //   this.setState({country}, this.validateCountryName)
  // }

  // validateCountryName = () => {
  //   const {country} = this.state;
  //   let countryValid = true;
  //   let errorMsg = {...this.state.errorMsg}

  //   if (country.length == 0 ||country=='') {
  //     countryValid = false;
  //     errorMsg.country = 'Required'
  //   }
  //   // else if(!/^[a-zA-Z\s]*$/.test(country))
  //   // {
  //   //   countryValid = false;
  //   //   errorMsg.country = 'Only alphabets are allowed'
  //   // }
  //   this.setState({countryValid, errorMsg}, this.validateForm)
  // }

  updatePhoneNo = (phone) => {
    this.setState({phone}, this.validatePhoneNo)
  }

  validatePhoneNo = () => {
    const {phone} = this.state;
    let phoneValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (phone.length == 0 || phone=='') {
      phoneValid = false;
      errorMsg.phone = 'Required'
    }
     else if (phone.length <8) {
      phoneValid = false;
      errorMsg.phone = 'Phone number must be 8 characters'
    }
    else if (!/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/.test(phone)){
      phoneValid = false;
      errorMsg.phone = 'Invalid phone format'
    }
    this.setState({phoneValid, errorMsg}, this.validateForm)
  }


  updateEmail = (email) => {
    this.setState({email}, this.validateEmail)
  }

  validateEmail = () => {
    const {email} = this.state;
    let emailValid = true;
    let errorMsg = {...this.state.errorMsg}

    // checks for format _@_._
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){
      emailValid = false;
      errorMsg.email = 'Invalid email format'
    }

    this.setState({emailValid, errorMsg}, this.validateForm)
  }

  updatePassword = (password) => {
    this.setState({password}, this.validatePassword);
  }

  validatePassword = () => {
    const {password} = this.state;
    let passwordValid = true;
    let errorMsg = {...this.state.errorMsg}

    // must be 6 chars
    // must contain a number
    // must contain a special character

    if (password.length < 6) {
      passwordValid = false;
      errorMsg.password = 'Password must be at least 6 characters long';
    } else if (!/\d/.test(password)){
      passwordValid = false;
      errorMsg.password = 'Password must contain a digit';
    } else if (!/[!@#$%^&*]/.test(password)){
      passwordValid = false;
      errorMsg.password = 'Password must contain special character: !@#$%^&*';
    }

    this.setState({passwordValid, errorMsg}, this.validateForm);
  }

  updatePasswordConfirm = (passwordConfirm) => {
    this.setState({passwordConfirm}, this.validatePasswordConfirm)
  }

  validatePasswordConfirm = () => {
    const {passwordConfirm, password} = this.state;
    let passwordConfirmValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (password !== passwordConfirm) {
      passwordConfirmValid = false;
      errorMsg.passwordConfirm = 'Passwords do not match'
    }

    this.setState({passwordConfirmValid, errorMsg}, this.validateForm);
  }
  updateDiscount = (realDiscount) => {
    this.setState({realDiscount}, this.validateDiscount)
  }

  validateDiscount = () => {
    var totalTempAmount=0
    const {realDiscount} = this.state;
    let discountValid = true;
    let paymentErrorMsg = {...this.state.paymentErrorMsg}

    if (realDiscount.length == 0 || realDiscount=='') {
      discountValid = false;
      paymentErrorMsg.realDiscount = 'Required'
    }
    else if (!/(^100(\.0{1,2})?$)|(^([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/i.test(realDiscount)){
      discountValid = false;
      paymentErrorMsg.realDiscount = 'Discount out of range or too much decimal'
    }
    if(discountValid==true)
    {
      this.tempEasyPayments=[]
      var discount=(this.actualTotalAmount/100)*realDiscount
      totalTempAmount=this.actualTotalAmount-discount
      totalTempAmount=Math.round((totalTempAmount + Number.EPSILON) * 100) / 100
      this.setState({
        totalAmount:totalTempAmount
      })
      var percentage=100/(parseInt(this.state.selectedInstallments))
      percentage=Math.round((percentage + Number.EPSILON) * 100) / 100
      for(var j=0;j<parseInt(this.state.selectedInstallments);j++)
      {
        var totalAmount=(totalTempAmount/100)*percentage
        totalAmount=Math.round((totalAmount + Number.EPSILON) * 100) / 100
        if(j==0)
        {
    
          this.tempEasyPayments.push({
            "amountPercentage": percentage,
            "amountValue": totalAmount,
            "currency": this.state.selectedSinglePlanCurrency,
            "dueDate": new Date,
            "graceDays": 0
          })
        }
        else{
            var today = new Date();
            today.setDate(today.getDate() + j)      
            var tomorrow = new Date(today);
            this.tempEasyPayments.push({
            "amountPercentage": percentage,
            "amountValue": totalAmount,
            "currency": this.state.selectedSinglePlanCurrency,
            "dueDate": tomorrow,
            "graceDays": 0
          })
        }
      }
      this.setState({
        allEasyPayments:this.tempEasyPayments,
      })

    }
    
    //  else if (realDiscount.length > 3) {
    //   cvvNoValid = false;
    //   paymentErrorMsg.cvvNo = 'Discount must be less than '
    // }
    
    this.setState({discountValid, paymentErrorMsg}, this.validatePaymentForm)
  }
  changePaymentType=e=>
  {
     this.setState({paymentTypeId:parseInt(e.target.value)})
     if(parseInt(e.target.value)==1)
     {
       this.setState({paymentAmount:this.state.selectedPlanObject.priceYearly})
     }
     else if(parseInt(e.target.value)==2)
     {
      this.setState({paymentAmount:this.state.selectedPlanObject.totalPriceMonthly})
     }
  }
  validateEmailAddress()
  {
    this.setState({
      formValid:false,
      progress:50
    })
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/user/exists', {
      method: 'POST',
      body:this.state.email,
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(response => response.json())
    .then(responsejson => {
      if(responsejson.applicationStatusCode!=0)
      {
        this.setState({
          formValid:true,progress:100
        })
        this.getAllPaymentPlan()
        this.setState({next:1});

      }
      else
      {
        this.setState({
          formValid:true,progress:100
        })
        toaster.notify(responsejson.applicationStatusResponse, {
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
  nextStep=(next)=>{
    if(this.state.formValid)
    {
      if(next==0)
      {
        formData=new FormData()
      }
      if(next==1)
      {
        // if(this.state.image=='' || this.state.image==null)
        // {
        //   toaster.notify('Kindly choose image', {
        //     duration:5000 // This notification will not automatically close
        //   });
        //   return;
        // }
        this.validateEmailAddress()
      }
      if(next==2)
      {
        this.tempSinglePlanCurrency=[]
        this.setState({
          realDiscount:0
        })
        var actualAnualPrice=0
        for(var i=0;i<this.planDetailObject.paymentPlan.branchLimit;i++)
        {
          this.noOfBranches.push({'name':(i+1),'value':i+1})
        }
        for(var x=0;x<this.planDetailObject.paymentPlan.prices.length;x++)
        {
          this.tempSinglePlanCurrency.push(this.planDetailObject.paymentPlan.prices[x])
        }
        for(var j=0;j<this.planDetailObject.paymentPlan.prices.length;j++)
        {
          if(this.tempSinglePlanCurrency[0].currency==this.planDetailObject.paymentPlan.prices[j].currency)
          {
            actualAnualPrice=this.planDetailObject.paymentPlan.prices[j].priceAnnuallyReal
            this.planDetailObject.paymentPlan.showPriceAnnuallyReal=actualAnualPrice
            this.planDetailObject.paymentPlan.showPriceMonthly=this.planDetailObject.paymentPlan.prices[j].priceMonthly
            this.planDetailObject.paymentPlan.showPriceAnnually=this.planDetailObject.paymentPlan.prices[j].priceAnnually
          }
        }
        this.tempEasyPayments=[]
        if(this.state.isAnnualy!=true)
        {        
          this.discount=(parseInt(this.planDetailObject.paymentPlan.showPriceMonthly)*12)-(actualAnualPrice)
          this.amount=this.planDetailObject.paymentPlan.showPriceMonthly
          var tax1=(((parseInt(this.amount))* (parseInt(this.noOfBranches[0].value))/100)*5)
          tax1=Math.round((tax1 + Number.EPSILON) * 100) / 100
          var tAmount=((parseInt(this.amount))* (parseInt(this.noOfBranches[0].value))+tax1)
          tAmount=Math.round((tAmount + Number.EPSILON) * 100) / 100
          this.actualTotalAmount=tAmount
          this.setState({next:2,branches:this.noOfBranches[0].value,tax:tax1,totalAmount:tAmount,
            singlePlanCurrency: this.tempSinglePlanCurrency,
            selectedSinglePlanCurrency:this.tempSinglePlanCurrency[0].currency,
            selectedInstallments:1
          });
        }
        else
        {
         this.selectedPlanParticularType('annualy')
         this.setState({next:2,branches:this.noOfBranches[0].value,
          singlePlanCurrency: this.tempSinglePlanCurrency,
          selectedSinglePlanCurrency:this.tempSinglePlanCurrency[0].currency,
          selectedInstallments:1
        });
        }
      }
      if(next==3)
      {
        this.setState({
          isEasyPayment:false
        })
        for(var j=0;j<this.state.selectedInstallments;j++)
        {
          var totalAmount=(this.state.totalAmount)/100*100
          if(j==0)
          {
            this.tempEasyPayments.push({
              "amountPercentage": 100,
              "amountValue": totalAmount,
              "currency": this.state.selectedSinglePlanCurrency,
              "dueDate": new Date,
              "graceDays": 0
            })
          }
          else{
            var today = new Date();
            today.setDate(today.getDate() + j)      
            var tomorrow = new Date(today);
            this.tempEasyPayments.push({
              "amountPercentage": 100,
              "amountValue": totalAmount,
              "currency": this.state.selectedSinglePlanCurrency,
              "dueDate": tomorrow,
              "graceDays": 0
            })
          }
         
         
        }
        this.setState({
          allEasyPayments:this.tempEasyPayments
        })
         this.setState({next:4});
        this.setState({
          selectedDate:this.state.years[0].name
        })
      }
    }
    if(this.state.isEasyPayment)
    {
        if(next==5)
        {
          this.createEasyPayments()
          // this.setState({paymentTypeId:4})
        }
    }
    else
    {
      if(next==5)
        {
          this.registerServiceCenter()
        }
    }

    
  }
  createEasyPayments()
  {
    
    let token = localStorage.getItem('sessionToken')
    var counter=0
    for(var i=0;i<this.tempEasyPayments.length;i++)
    {
      counter=counter+this.tempEasyPayments[i].amountPercentage
      if(!/(^100(\.0{1,2})?$)|(^([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/i.test(this.tempEasyPayments[i].amountPercentage))
      {
        toaster.notify('Amount percentage out of range or too much decimal', {
          duration:5000 // This notification will not automatically close
        });
        return;
      }
    }
    counter=Math.ceil(counter)
    if(counter!=100)
    {
      toaster.notify('Total amount percentage must be 100', {
        duration:5000 // This notification will not automatically close
      });
      return;
    }
    this.setState({
      isDisableCreate:true,
      progress:50
    })
    var fullPhoneNo=this.state.phoneCode +'-'+ this.state.phone
   formData.append('RegisterServiceCenterPayload',JSON.stringify({
    "arabicName": this.state.serviceCenterArabicName,
    "country": this.state.country,
    'currency':this.planDetailObject.paymentPlan.prices[0].currency,
    "email": this.state.email,
    "easyPayments":this.tempEasyPayments,
    "noOfBranch": this.state.branches,
    'name':this.state.serviceCenterName,
    "password": this.state.password,
    'planSubscriptionType':this.state.planSubscriptionType,
    "paymentPlanId": this.state.selectedPlanId,
    "phoneNumber": fullPhoneNo,
    "serviceSector": {
        "serviceSectorId": this.state.selectedServiceSectorId,
        "serviceSectorName": this.state.selectedServiceObject.serviceSectorName
    },
    "totalAmount": this.state.totalAmount
  }))
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/registerByAdmin', {
    method: 'POST',
    body:formData,
    headers: {
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      formData=new FormData()
      this.setState({
        isDisableCreate:false,progress:100
      })
      global.isNotificationUpdated.next(true)
      this.props.history.push('/centerList')
      // this.userServiceCenterId=parseInt(responseJson.values.ServiceCenterId)
      
    }
    else{
      formData=new FormData()
      this.setState({
        isDisableCreate:false,progress:100
      })
      toaster.notify(responseJson.devMessage, {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
   
  })
  }
  changeServiceSector= e =>{
    for(var i=0;i<this.state.serviceSector.length;i++)
    {
      if(this.state.serviceSector[i].serviceSectorId==parseInt(e.target.value))
      {
        this.setState({selectedServiceObject:this.state.serviceSector[i]})
      }
    }
    this.setState({selectedServiceSectorId:parseInt(e.target.value)})
  }

 getAllServiceSector()
 {
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceSector/get/all', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      this.setState({selectedServiceObject:responseJson.serviceSectors[0],serviceSector:responseJson.serviceSectors,selectedServiceSectorId:responseJson.serviceSectors[0].serviceSectorId})
    }
  })
 }
 getAllPaymentPlan()
 {
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/paymentPlan/get/all', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      
      for(var j=0;j<responseJson.paymentPlanList.length;j++)
      {
        responseJson.paymentPlanList[j].paymentPlan['showPriceAnnually']=''
        responseJson.paymentPlanList[j].paymentPlan['showPriceMonthly']=''
        responseJson.paymentPlanList[j]['isSelected']=false
        responseJson.paymentPlanList[j]['isMontly']=false
        responseJson.paymentPlanList[j]['isAnnualy']=true

        for(var x=0;x<responseJson.paymentPlanList[j].paymentPlan.prices.length;x++)
        {
          responseJson.paymentPlanList[j].paymentPlan.prices[x].priceAnnuallyReal=responseJson.paymentPlanList[j].paymentPlan.prices[x].priceAnnually
          var pAnnually=parseInt(responseJson.paymentPlanList[j].paymentPlan.prices[x].priceAnnually)/12
          pAnnually=Math.ceil(pAnnually)
          responseJson.paymentPlanList[j].paymentPlan.prices[x].priceAnnually=pAnnually
          this.tempCurrency.push(responseJson.paymentPlanList[j].paymentPlan.prices[x])
        }
      }
      this.tempPayment=responseJson.paymentPlanList
      var tempArray=[...new Map(this.tempCurrency.map(o => [o.currency, o])).values()]
      this.tempCurrency=tempArray
      var tempC=this.tempCurrency[0].currency
     

      for(var k=0;k<this.tempPayment.length;k++)
      { 
        for(var o=0;o<this.tempPayment[k].paymentPlan.prices.length;o++)
        {
          if(tempC==this.tempPayment[k].paymentPlan.prices[o].currency)
          {
            this.tempPayment[k].paymentPlan['isShowPlan']=true
            this.tempPayment[k].paymentPlan['showPriceAnnually']=this.tempPayment[k].paymentPlan.prices[o].priceAnnually
            this.tempPayment[k].paymentPlan['showPriceMonthly']=this.tempPayment[k].paymentPlan.prices[o].priceMonthly
          }
        }
      }
      if(this.planDetailObject.length!=0)
      {
        for(var m=0;m<this.tempPayment.length;m++)
        {
         if(this.tempPayment[m].paymentPlan.paymentPlanId==this.state.selectedPlanId) 
         {
          this.tempPayment[m].isSelected=true
         }
        }
      }
      this.setState({
        allCurrency: this.tempCurrency,
        paymentPlanList: this.tempPayment,
        selectedCurrency:this.tempCurrency[0].currency,
      })


    }
  })
 }
 changePlanType(type,data)
 {
   for(var i=0;i<this.tempPayment.length;i++)
   {
     if(this.tempPayment[i].paymentPlan.paymentPlanId==data.paymentPlan.paymentPlanId)
     {
       if(type=='annualy')
       {
         data.isAnnualy=true
         this.tempPayment[i].isAnnualy=true
         data.isMontly=false
         this.tempPayment[i].isMontly=false
       }
       else if(type=='montly')
       {
         data.isMontly=true
         this.tempPayment[i].isMontly=true
         data.isAnnualy=false
         this.tempPayment[i].isAnnualy=false
       }
     }
   }
   this.setState({
     paymentPlanList:this.tempPayment
   })
 }
 registerServiceCenter()
 {
  this.setState({
    isDisableCreate:true,
    progress:50
  })
  let token = localStorage.getItem('sessionToken')
   var fullPhoneNo=this.state.phoneCode +'-'+ this.state.phone
   formData.append('RegisterServiceCenterPayload',JSON.stringify({
    "arabicName": this.state.serviceCenterArabicName,
    "country": this.state.country,
    'currency':this.planDetailObject.paymentPlan.prices[0].currency,
    "email": this.state.email,
    "easyPayments":[],
    "noOfBranch": this.state.branches,
    'name':this.state.serviceCenterName,
    "password": this.state.password,
    'planSubscriptionType':this.state.planSubscriptionType,
    "paymentPlanId": this.state.selectedPlanId,
    "phoneNumber": fullPhoneNo,
    "serviceSector": {
        "serviceSectorId": this.state.selectedServiceSectorId,
        "serviceSectorName": this.state.selectedServiceObject.serviceSectorName
    },
    "totalAmount": this.state.totalAmount
  }))
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/registerByAdmin', {
    method: 'POST',
    body:formData,
    headers: {
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      formData=new FormData()
      this.setState({
        isDisableCreate:false,progress:100
      })
      global.isNotificationUpdated.next(true)
       this.props.history.push('/centerList')
      // this.userServiceCenterId=parseInt(responseJson.values.ServiceCenterId)
      
    }
    else{
      formData=new FormData()
      this.setState({
        isDisableCreate:false,progress:100
      })
      toaster.notify(responseJson.devMessage, {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
   
  })
 }
 onImageChange = event => {
  if (event.target.files && event.target.files[0]) {
    if(event.target.files[0].type!='image/png' && event.target.files[0].type!='image/gif' 
    && event.target.files[0].type!='image/jpeg' && event.target.files[0].type!='image/jpg')
    {
      toaster.notify('Upload image  only',{
        duration:5000
      })
    }
    else
    {
      if( event.target.files[0].size<=262144)
      {
        let img = event.target.files[0];
        formData.append("ServiceCenterProfileImage", event.target.files[0]);
        this.setState({
          image: URL.createObjectURL(img),
          isShowUpload:false
        });
      }
      else
      {
        toaster.notify('Size must be less than 256KB',{
          duration:5000
        })
      }
      
    }
   
  }
}
resetImage()
{
  formData.delete('ServiceCenterProfileImage')
  this.setState({
    image: '',
    isShowUpload:true
  });
}
selectedPlan=(data)=>{
 
  
  for(var i=0;i<this.tempPayment.length;i++)
  {
     if(this.tempPayment[i].paymentPlan.paymentPlanId==data.paymentPlan.paymentPlanId)
    {
      if(this.tempPayment[i].paymentPlan.easyPaymentEnabled==true)
      {
        this.setState({
          isEasyPaymentVal:true
        })
      }
      else{
        this.setState({
          isEasyPaymentVal:false
        })
      }
      if(this.tempPayment[i]['isSelected']==false)
      {
        this.tempPayment[i]['isSelected']=true
      }
      
        this.planDetailObject=this.tempPayment[i]
    }
    else{
      this.tempPayment[i]['isSelected']=false
    }

  }
  this.setState({selectedPlanId:data.paymentPlan.paymentPlanId,selectedPlanBool:true,paymentPlanList:this.tempPayment})
}
selectedPlanParticularType(type)
{
  var actualAnualPrice=0
  for(var i=0;i<this.planDetailObject.paymentPlan.prices.length;i++)
  {
    if( this.state.selectedSinglePlanCurrency==this.planDetailObject.paymentPlan.prices[i].currency)
    {
      actualAnualPrice=this.planDetailObject.paymentPlan.prices[i].priceAnnuallyReal
      this.planDetailObject.paymentPlan['showPriceAnnuallyReal']=actualAnualPrice
      this.planDetailObject.paymentPlan['showPriceAnnually']=this.planDetailObject.paymentPlan.prices[i].priceAnnually
      this.planDetailObject.paymentPlan['showPriceMonthly']=this.planDetailObject.paymentPlan.prices[i].priceMonthly
      
    }
  }
  this.discount=(parseInt(this.planDetailObject.paymentPlan.showPriceMonthly)*12)-(actualAnualPrice)
 if(type=='montly')
 {
  this.setState({isMontly:true,isAnnualy:false,planSubscriptionType:2})
  this.amount=this.planDetailObject.paymentPlan.showPriceMonthly
 
 }
 else if(type=='annualy')
 {
  this.setState({isMontly:false,isAnnualy:true,planSubscriptionType:1})
  this.amount=this.planDetailObject.paymentPlan.showPriceAnnuallyReal
 }

  var tax1=(((parseInt(this.amount))* (parseInt(this.state.branches))/100)*5)
  tax1=Math.round((tax1 + Number.EPSILON) * 100) / 100

  var tAmount=((parseInt(this.amount))* (parseInt(this.state.branches))+tax1)
  tAmount=Math.round((tAmount + Number.EPSILON) * 100) / 100
  this.actualTotalAmount=tAmount
  this.setState({tax:tax1,totalAmount:tAmount});
  
}
paymentDetailModel(payment)
{
  // if(payment.paymentPlan.kioskEnabled==true)
  // {
  //   this.setState({
  //     planPaymentDetail:payment.paymentPlan
  //   })
  // }
  this.setState({
    planPaymentDetail:payment.paymentPlan,
    isPaymentDetailModel:true,
    uptoNoOFSms:payment.paymentPlan.noOfSMS,
    uptoNoOfService:payment.paymentPlan.noOfServices,
  })
}
changeBranches=e=>
{
   var tax1=(((parseInt(this.amount))* (parseInt(e.target.value))/100)*5)
   tax1=Math.round((tax1 + Number.EPSILON) * 100) / 100

   var tAmount=((parseInt(this.amount))* (parseInt(e.target.value))+tax1)
   tAmount=Math.round((tAmount + Number.EPSILON) * 100) / 100
   this.actualTotalAmount=tAmount
   this.setState({tax:tax1,totalAmount:tAmount,branches:parseInt(e.target.value)});
}
changePhoneNo=e=>
{
  this.setState({phoneCode:parseInt(e.target.value)})
}
changeDate=e=>
{
  this.setState({selectedDate:e.target.value})
}
changeAllCurrency=e=>{
  this.setState({selectedCurrency: e.currentTarget.textContent})
  for(var x=0;x<this.tempPayment.length;x++)
  {
    this.tempPayment[x].paymentPlan['isShowPlan']=false
    this.tempPayment[x].paymentPlan['showPriceAnnually']=''
    this.tempPayment[x].paymentPlan['showPriceMonthly']=''
  }
  for(var j=0;j<this.tempPayment.length;j++)
  { 
    for(var i=0;i<this.tempPayment[j].paymentPlan.prices.length;i++)
    {
      if( e.currentTarget.textContent==this.tempPayment[j].paymentPlan.prices[i].currency)
      {
        this.tempPayment[j].paymentPlan['isShowPlan']=true
        this.tempPayment[j].paymentPlan['showPriceAnnually']=this.tempPayment[j].paymentPlan.prices[i].priceAnnually
        this.tempPayment[j].paymentPlan['showPriceMonthly']=this.tempPayment[j].paymentPlan.prices[i].priceMonthly
      }
    }
  }

  this.setState({
    paymentPlanList: this.tempPayment,
  })
}
changeCurrency=e=>{
  var actualAnualPrice=0
  this.setState({selectedSinglePlanCurrency: e.currentTarget.textContent})
    for(var i=0;i<this.planDetailObject.paymentPlan.prices.length;i++)
    {
      if( e.currentTarget.textContent==this.planDetailObject.paymentPlan.prices[i].currency)
      {
        actualAnualPrice=this.planDetailObject.paymentPlan.prices[i].priceAnnuallyReal
        this.planDetailObject.paymentPlan['showPriceAnnuallyReal']=actualAnualPrice
        this.planDetailObject.paymentPlan['showPriceAnnually']=this.planDetailObject.paymentPlan.prices[i].priceAnnually
        this.planDetailObject.paymentPlan['showPriceMonthly']=this.planDetailObject.paymentPlan.prices[i].priceMonthly
        
      }
    }
    this.discount=(parseInt(this.planDetailObject.paymentPlan.showPriceMonthly)*12)-(actualAnualPrice)
    if(this.state.planSubscriptionType==2)
    {
      this.amount=this.planDetailObject.paymentPlan.showPriceMonthly
    }
    else if(this.state.planSubscriptionType==1)
    {
      this.amount=this.planDetailObject.paymentPlan.showPriceAnnuallyReal
    }
    var tax1=(((parseInt(this.amount))* (parseInt(this.state.branches))/100)*5)
    tax1=Math.round((tax1 + Number.EPSILON) * 100) / 100
    var tAmount=((parseInt(this.amount))* (parseInt(this.state.branches))+tax1)
    tAmount=Math.round((tAmount + Number.EPSILON) * 100) / 100
    this.actualTotalAmount=tAmount
    this.setState({tax:tax1,totalAmount:tAmount});

  // this.setState({
  //   paymentPlanList: this.tempPayment,
  // })
}
paymentTypeVal(id)
{
  if(id=='4')
  {
    this.setState({
      paymentTypeId:4,
      isVisa:true,
      isMaster:false,
      isPaypal:false
    })
  }
  else if(id=='3')
  {
    this.setState({
      paymentTypeId:3,
      isVisa:false,
      isMaster:true,
      isPaypal:false
    })
  }
  else if(id=='2')
  {
    this.setState({
      paymentTypeId:2,
      isVisa:false,
      isMaster:false,
      isPaypal:true
    })
  }
  
}
changePayment(type)
{
  if(type=='easy')
  {
    if(this.state.planSubscriptionType==2)
    {
      toaster.notify('Easy payment is only for annually', {
        duration:5000 // This notification will not automatically close
      });
      return;
    }
    if(this.state.isEasyPaymentVal==false)
    {
      toaster.notify('Easy payment is not avialable for the selected plan', {
        duration:5000 // This notification will not automatically close
      });
      return;
    }
    this.setState({
      isEasyPayment:true
    })
  }
  else if(type=='full')
  {
    this.setState({
      isEasyPayment:false
    })
  }
}
changeInstallments=e=>{
  this.tempEasyPayments=[]
  var percentage=100/(parseInt(e.target.value))
  percentage=Math.round((percentage + Number.EPSILON) * 100) / 100
  for(var j=0;j<parseInt(e.target.value);j++)
  {
    var totalAmount=(this.state.totalAmount/100)*percentage
    totalAmount=Math.round((totalAmount + Number.EPSILON) * 100) / 100
    if(j==0)
    {

      this.tempEasyPayments.push({
        "amountPercentage": percentage,
        "amountValue": totalAmount,
        "currency": this.state.selectedSinglePlanCurrency,
        "dueDate": new Date,
        "graceDays": 0
      })
    }
    else{
        var today = new Date();
        today.setDate(today.getDate() + j)      
        var tomorrow = new Date(today);
        this.tempEasyPayments.push({
        "amountPercentage": percentage,
        "amountValue": totalAmount,
        "currency": this.state.selectedSinglePlanCurrency,
        "dueDate": tomorrow,
        "graceDays": 0
      })
    }
  }
  this.setState({
    allEasyPayments:this.tempEasyPayments,
    selectedInstallments:parseInt(e.target.value)
  })
  // this.setState({
    
  // })
}
changeStartDate(date,index)
{
  this.tempEasyPayments[index].dueDate=date.value
  
    for(var i=0;i<this.tempEasyPayments.length;i++)
    {
      if(i>index)
      {
        var today = new Date(this.tempEasyPayments[i-1].dueDate);
        today.setDate(today.getDate() + 1)      
        var tomorrow = new Date(today);
        this.tempEasyPayments[i].dueDate=tomorrow
        // this.tempEasyPayments[0].dueDate=date.value
      }
      // else
      // {
      //   var today = new Date();
      //   today.setDate(today.getDate() + i)      
      //   var tomorrow = new Date(today);
      //   this.tempEasyPayments[i].dueDate=tomorrow
      // }
  }
  
  this.setState({allEasyPayments:this.tempEasyPayments})
}
changeAmountPercentage(value,index)
{
  if(value!='')
  {
    var totalAmount=(this.state.totalAmount/100)*parseInt(value)
    this.tempEasyPayments[index].amountPercentage=parseInt(value)
    this.tempEasyPayments[index].amountValue=totalAmount
    this.setState({allEasyPayments:this.tempEasyPayments})
  }
  else
  {
    this.tempEasyPayments[index].amountPercentage=value
    this.tempEasyPayments[index].amountValue=value
    this.setState({allEasyPayments:this.tempEasyPayments})
  }
}
changeGraceDays(value,index)
{
  if(value!='')
  {
    this.tempEasyPayments[index].graceDays=parseInt(value)
    this.setState({allEasyPayments:this.tempEasyPayments})
  }
  else
  {
    this.tempEasyPayments[index].graceDays=value
    this.setState({allEasyPayments:this.tempEasyPayments})
  }
}
cancelModel()
{
  this.setState({
    isPaymentDetailModel:false
  })
}
changeCountry=e=>
{
  this.setState({country:e.target.value})
}
  render() {
    return (
      <div className={"app align-items-center register-page "+ (this.state.isSuccess ? 'success-page' : '')}>
        <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress:0
          })}
        />
        <Container fluid className="p-0">
        <Row>
          
          <Col sm="9" className="pr-4">
          {(this.state.next==0) && (
            <Card className="register-card m-0">
              <CardBody className="p-5 pb-0"> <Form>
                <Row className="mb-3">
                  <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                  <Col sm="6" className="text-right">  
                    <h6 className="mt-2">{this.state.userName}</h6> 
                  </Col>
                </Row> 


                <h4 className="text-dark mb-3">Basic Information</h4>
                <Row> <Col sm="7"> 
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Center Name (English)</Label>
                    
                    <Input type="text" placeholder="Service Center Name English" 
                     value={this.state.serviceCenterName} onChange={(e) => this.updateServiceCenterName(e.target.value)} />
                     < ValidationMessage valid={this.state.serviceCenterNameValid} message={this.state.errorMsg.serviceCenterName} />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Center Name (Arabic)</Label>
                    
                    <Input type="text" placeholder="Service Center Name Arabic" 
                     value={this.state.serviceCenterArabicName} onChange={(e) => this.updateArabicServiceCenterName(e.target.value)} />
                     < ValidationMessage valid={this.state.serviceCenterArabicNameValid} message={this.state.errorMsg.serviceCenterArabicName} />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Service Type / Sector</Label>
                    <select className="form-control" onChange={this.changeServiceSector} >
                    {this.state.serviceSector.map(service => (
                      <option value={service.serviceSectorId} key={service.serviceSectorId}>
                        {service.serviceSectorName}
                      </option>
                    ))}
                    </select>
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Country</Label>
                    <select className="form-control" onChange={this.changeCountry}>
                        {this.counteries.map(country => (
                          <option value={country.name}>{country.name}</option>
                        ))}
                    </select>
                    {/* <Input type="text" placeholder="Country" autoComplete="username" 
                    value={this.state.country} onChange={(e) => this.updateCountryName(e.target.value)}/>
                     < ValidationMessage valid={this.state.countryValid} message={this.state.errorMsg.country} /> */}
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Phone</Label>
                    <Row>
                      <Col sm="3">
                        <select className="form-control" onChange={this.changePhoneNo} >
                        {this.phoneCodes.map(country => (
                          <option value={country.code}>{country.code}</option>
                        ))}
                      </select>
                      </Col>
                      <Col sm="9">
                        <Input type="text" placeholder="Phone" autoComplete="username"
                      value={this.state.phone} onChange={(e) => this.updatePhoneNo(e.target.value)} />
                      < ValidationMessage valid={this.state.phoneValid} message={this.state.errorMsg.phone} />
                      </Col>
                    </Row>
                    
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey" htmlFor="exampleEmail" >Email</Label>
                    
                    <Input type="email" placeholder="Email" autoComplete="username" name="email" id="exampleEmail"
                    value={this.state.email} onChange={(e) => this.updateEmail(e.target.value)}/>
                    < ValidationMessage valid={this.state.emailValid} message={this.state.errorMsg.email} />
                  </FormGroup> 
                  <FormGroup className="input-line">
                    <Label className="text-light-grey" htmlFor="exampleEmail" >Password</Label>
                    
                    <Input type="password" placeholder="Password" autoComplete="username" name="password" id="exampleEmail"
                    value={this.state.password} onChange={(e) => this.updatePassword(e.target.value)}/>
                    < ValidationMessage valid={this.state.passwordValid} message={this.state.errorMsg.password} />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey" htmlFor="exampleEmail" >Retype Password</Label>
                    
                    <Input type="password" placeholder="Password" autoComplete="username" name="password" id="exampleEmail"
                    value={this.state.passwordConfirm} onChange={(e) => this.updatePasswordConfirm(e.target.value)}/>
                    < ValidationMessage valid={this.state.passwordConfirmValid} message={this.state.errorMsg.passwordConfirm} />
                  </FormGroup>
                  </Col>
                  <Col sm="5" className="text-right">
                    <div className="mt-sm-5 pt-sm-5 upload-image">
                     {this.state.isShowUpload==true &&  <div className="custom-file-upload mb-4">
                          <label htmlFor="file-upload" className="custom-file-content text-center"> <i className="icon2-download-outline font-lg"></i> <br></br>Upload Logo </label>
                          <input id="file-upload" type="file" onChange={this.onImageChange} accept="image/x-png,image/jpg,image/jpeg"/>
                      </div>}
                      {this.state.isShowUpload==false &&   <div className="uploaded-image">
                        <a className="remove-image" title="Delete" onClick={()=>this.resetImage()}><i className="font-sm font-weight-bold icon2-cancel"></i></a>
                        <img className="img-fluid" src={this.state.image}/>
                      </div>}
                    </div>
                  </Col>
                </Row> 
              
              
              </Form> </CardBody>
              <CardFooter className="p-5 text-right b-a-0 pt-0">
                <Button color="primary" 
                 disabled={!this.state.formValid}
                  onClick={()=>{this.nextStep(1)}}>Next</Button>
                {/* <Button color="primary" type='submit' disabled={!this.state.formValid}>Next</Button> */}
              </CardFooter>
            </Card>
             )}
          

          {(this.state.next==1) && (
            
            <Card className="register-card m-0">
            <CardBody className="p-5 pb-0"> <Form>
              <Row className="mb-5">
                <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                <Col sm="6" className="text-right">  
                <h6 className="mt-2">{this.state.userName}</h6> 
                </Col>
              </Row> 
              <Row className="mb-4">
                <Col><h4 className="text-dark mb-3">Choose A Subscription Plan</h4></Col>
                <Col className="text-right">
                  <ButtonDropdown id="card1" isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                    <DropdownToggle caret className="p-0 mb-0 text-primary" color="transparent">
                   {this.state.selectedCurrency}
                    </DropdownToggle>
                    <DropdownMenu right>
                    {this.state.allCurrency.map(currency => (
                      <DropdownItem onClick={this.changeAllCurrency}>
                        {currency.currency}
                        </DropdownItem>
                    ))}
                    </DropdownMenu>
                  </ButtonDropdown>
                </Col>
              </Row>

              {/* <Alert color="warning" className="theme-alert mb-5 bg-warning" isOpen={this.state.visible} toggle={this.onDismiss}>
                  <Row>
                    <Col sm="3"><h3 className="mb-0 mt-1 text-white">FREE DEMO</h3></Col>
                    <Col sm="6"><h5 className="mb-0 mt-2 text-dark">Try Taboor Platform now for 1 month for free</h5></Col>
                    <Col sm="3" className="text-right"><Button color="light">Get Demo</Button></Col>
                  </Row>
              </Alert> */}





              




            <Row>
            {this.state.paymentPlanList.map((payment,index) => {
               if(payment.paymentPlan.isShowPlan==true)
               {
                 return(
                  <Col sm="4">
                  {payment.paymentPlan.isShowPlan==true &&<Card className={"text-center "+ (payment.isSelected?"active-plan" : '')}>
                        <CardBody className="pb-5 pl-4 pr-4 pt-5">
                        <div className="plan-info">
                          <h2 className="mb-4 text-truncate" title={payment.paymentPlan.planName}>{payment.paymentPlan.planName}</h2>
                          <div className="tabs-line tabs-small">
    
                  <Nav tabs>
                  <NavItem className="w-50">
                          <NavLink className="nav-link" to="#1"
                            active={payment.isAnnualy}
                            isActive={(match, location) => {
                              if (!payment.isAnnualy) {
                                return false;
                              }
                              else{
                                return true;
                              }
                            }}
                            onClick={() => this.changePlanType('annualy',payment)}
                          >
                            <h5 className="mb-0">Annually</h5>
                          </NavLink>
                        </NavItem>
                        <NavItem className="w-50">
                          <NavLink className="nav-link" to="#2"
                            active={payment.isMontly}
                            isActive={(match, location) => {
                              if (!payment.isMontly) {
                                return false;
                              }
                              else{
                                return true;
                              }
                            }}
                            onClick={() => this.changePlanType('montly',payment)}
                          >
                            <h5 className="mb-0">Monthly</h5>
                          </NavLink>
                        </NavItem>
                        
                      </Nav>
                      <TabContent className="pt-4">
                        {payment.isAnnualy==true && <TabPane >
                            {payment.paymentPlan.showPriceAnnually!=''&& <h2 className="font-4xl font-weight-normal">{payment.paymentPlan.showPriceAnnually} {this.state.selectedCurrency}</h2>}
                            {payment.paymentPlan.showPriceAnnually==''&& <h2 className="font-2xl font-weight-normal mt-2">Not Available</h2>}
                            {/* <h2 className="font-5xl font-weight-normal">{payment.paymentPlan.showPriceAnnually} {this.state.selectedCurrency}</h2> */}
                            <p className="mb-4 text-light-grey">Per month/branch</p>
                            <div className="text-light-grey mb-5">
                              <p>Up to {payment.paymentPlan.branchLimit} branches</p>
                              <p>Up to {payment.paymentPlan.noOfUserPerBranch} Users/Annualy</p>
                              <p>{payment.paymentPlan.supportType} Support. <Link onClick={()=>this.paymentDetailModel(payment)} >Details</Link></p>
                            </div>
                            
                          
    
                          </TabPane>}
                          {payment.isMontly==true && <TabPane>
                            {payment.paymentPlan.showPriceMonthly!=''&&  <h2 className="font-4xl font-weight-normal">{payment.paymentPlan.showPriceMonthly} {this.state.selectedCurrency}</h2>}
                              {payment.paymentPlan.showPriceMonthly==''&& <h2 className="font-2xl font-weight-normal mt-2">Not Available</h2>}
    
                          {/* <h2 className="font-5xl font-weight-normal">{payment.paymentPlan.showPriceMonthly} {this.state.selectedCurrency}</h2> */}
                            <p className="mb-4 text-light-grey">Per month/branch</p>
                            <div className="text-light-grey mb-5">
                            <p>Up to {payment.paymentPlan.branchLimit} branches</p>
                              <p>Up to {payment.paymentPlan.noOfUserPerBranch} Users/Annualy</p>
                              <p>{payment.paymentPlan.supportType} Support. <Link onClick={()=>this.paymentDetailModel(payment)} >Details</Link></p>
                            </div>
                            
                      
    
                        </TabPane>}
                        <Button color="warning" onClick={() => this.selectedPlan(payment)} >Select</Button>
                        
                      </TabContent>
                      </div>
                            
                        </div>
                        </CardBody>
                      </Card>}
                  </Col>
                 )
               }

              })}
            </Row>





            <Modal isOpen={this.state.isPaymentDetailModel}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader>Plan Details</ModalHeader>
                  <ModalBody>

                  
                  <Card className="card-line">
                    <CardBody>
                    <p>Up to {this.state.uptoNoOfService} Services</p>
                    <p>Up to {this.state.uptoNoOFSms} SMS</p>
                    {this.state.planPaymentDetail.kioskEnabled==true && <p>Kiosk Enabled</p>}
                    {this.state.planPaymentDetail.easyPaymentEnabled==true && <p>Easy Payment Enabled</p>}
                    {this.state.planPaymentDetail.advanceReportingEnabled==true && <p>Advance Reporting Enabled</p>}
                    {this.state.planPaymentDetail.dedicatedAccountManager==true && <p>Dedicated Account Manager Enabled</p>}
                    {this.state.planPaymentDetail.prioritySupport==true && <p>Priority Support Enabled</p>}
                    {this.state.planPaymentDetail.remoteDedicatedSupport==true && <p>Remote Dedicated Support Enabled</p>}
                    {this.state.planPaymentDetail.fullDaySupport==true && <p>Full Day Support Enabled</p>}
                    </CardBody>
                  </Card>


                  </ModalBody>
                  <ModalFooter>
                  {/* <Button outline color="primary" onClick={()=>this.cancelModel()}>Cancel</Button> */}
                    <Button color="primary"onClick={()=>this.cancelModel()}>Ok</Button>
                  </ModalFooter>
                </Modal>
            </Form>
            </CardBody>
            <CardFooter className="p-5 text-right b-a-0 pt-0">
              <Button outline color="primary" onClick={()=>{this.setState({next:0})}}>Back</Button>
              <Button color="primary" className="ml-2" disabled={!this.state.selectedPlanBool} onClick={()=>{this.nextStep(2)}}>Next</Button>
            </CardFooter>
          </Card>
        )}
         {(this.state.next==2) && (
            <Card className="register-card m-0">
              <CardBody className="p-5 pb-0"> <Form>
                <Row className="mb-3">
                  <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                  <Col sm="6" className="text-right">  
                  <h6 className="mt-2">{this.state.userName}</h6> 
                  </Col>
                </Row> 
                <Row className="mb-4">
                  <Col><h4 className="text-dark mb-3">Set Your Plan - Basic Plan</h4></Col>
                  <Col className="text-right">
                  <ButtonDropdown id="card1" isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                      <DropdownToggle caret className="p-0 mb-0 text-primary" color="transparent">
                     {this.state.selectedSinglePlanCurrency}
                      </DropdownToggle>
                      <DropdownMenu right>
                      {this.state.singlePlanCurrency.map(currency => (
                        <DropdownItem onClick={this.changeCurrency}>
                          {currency.currency}
                          </DropdownItem>
                      ))}
                      </DropdownMenu>
                    </ButtonDropdown>
                  </Col>
                </Row>
                <Row>
                   <Col sm="7"> 
                        
                      <Card className="card-line">
                     <CardBody>
                       <div className="float-left">
                         <p className="mb-0">No. of Branches</p>
                       </div>
                       <div className="float-right">
                       <select className="text-primary bg-transparent" onChange={this.changeBranches}> 
                          {this.noOfBranches.map(branch => (
                            <option value={branch.value} key={branch.value} >{branch.name}</option>
                          ))}
                        </select>
                       </div>
                       <div className="clearfix"></div>
                     </CardBody>
                      </Card>
                   


                  <Row>
                      <Col sm="6" onClick={()=>this.selectedPlanParticularType('montly')}>
                          <Card className={"text-center pt-3 "+(this.state.isMontly==true?'active-plan':'')}>
                            <CardBody className="p-4">
                              <h4 className="font-weight-normal mb-4">Monthly</h4>
                              <h1 className={"font-weight-bold mb-0 "+(this.state.isMontly==true?'text-success':'')}>{this.planDetailObject.paymentPlan.showPriceMonthly} {this.state.selectedSinglePlanCurrency}</h1>
                              <p className="text-light-grey mb-4">monthly cost</p>
                              <p>Billed monthly</p>
                            </CardBody>
                          </Card>
                      </Col>
                      <Col sm="6" className="position-relative" onClick={()=>this.selectedPlanParticularType('annualy')}>
                        <Card className={"text-center pt-3 "+(this.state.isAnnualy==true?'active-plan':'')}>
                          <CardBody className="p-4">
                            <h4 className="font-weight-normal mb-4">Annual</h4>
                            <h1 className={"font-weight-bold mb-0 "+(this.state.isAnnualy==true?'text-success':'')}>{this.planDetailObject.paymentPlan.showPriceAnnuallyReal} {this.state.selectedSinglePlanCurrency}</h1>
                            <p className="text-light-grey mb-4">anum cost</p>
                            <p>Billed annually</p>
                          </CardBody>
                        </Card>
                        <Button color="warning"  
                        onClick={()=>console.log()}
                        style={{left: '0',right:'0',margin:'auto',bottom: '7px'}} className="position-absolute">Save {this.discount} {this.state.selectedSinglePlanCurrency}</Button>
                      </Col>
                 
                  </Row>


                   </Col>
                  <Col sm="5" className="text-center">
                    
                    <Card className="ml-5 card-line">
                      <CardBody>
                      <div className="plan-info">
                        <h4 className="text-center mb-3 font-weight-normal">Order Details</h4>
                        <Row>
                        <Col sm="7" className="text-left"><h5 className="font-lg font-weight-bold text-dark">{this.planDetailObject.paymentPlan.planName}</h5></Col>
                        {this.state.isMontly==true &&  <Col sm="5" className="text-right"><h3 className="text-dark mb-0">{this.amount} {this.state.selectedSinglePlanCurrency}</h3><p className="text-light-grey">per month</p></Col>}
                        {this.state.isAnnualy==true &&  <Col sm="5" className="text-right"><h3 className="text-dark mb-0">{this.amount} {this.state.selectedSinglePlanCurrency}</h3><p className="text-light-grey">per month</p></Col>}
                          </Row>
                          <div className="text-light-grey text-left">
                            <p>{this.state.branches} Branches</p>
                            <p>{this.planDetailObject.paymentPlan.noOfUserPerBranch} Users/Branch</p>
                            <p>{this.planDetailObject.paymentPlan.supportType} Support</p>
                          </div>
                          <hr></hr>
                          <Row>
                          {this.state.isMontly==true && <Col sm="5" className="text-left"><h4 className="mb-0 text-dark">Tax (5%)</h4><p className="text-light-grey">per month</p></Col>}
                          {this.state.isAnnualy==true && <Col sm="5" className="text-left"><h4 className="mb-0 text-dark">Tax (5%)</h4><p className="text-light-grey">per month</p></Col>}
                          
                            <Col sm="7" className="text-right pl-lg-0"><h3 className="font-3xl text-dark">{this.state.tax} {this.state.selectedSinglePlanCurrency}</h3></Col>
                          </Row>
                          <hr></hr>
                          <Row>
                            <Col sm="5" className="text-left"><h3 className="text-primary">Total </h3></Col>
                            {this.state.isMontly==true &&  <Col sm="7" className="text-right pl-lg-0"><h3 className="text-primary mb-0">{this.state.totalAmount} {this.state.selectedSinglePlanCurrency}</h3><p className="mb-0">per month</p></Col>}
                            {this.state.isAnnualy==true && <Col sm="7" className="text-right pl-lg-0"><h3 className="text-primary mb-0">{this.state.totalAmount} {this.state.selectedSinglePlanCurrency}</h3><p className="mb-0">per month</p></Col>}
                          </Row>
                      </div>
                      </CardBody>
                    </Card>

                  </Col>
                </Row> 


              </Form> </CardBody>
              <CardFooter className="p-5 text-right b-a-0 pt-0">
              <Button outline color="primary" onClick={()=>{this.nextStep(1)}}>Back</Button>
                <Button color="primary" className="ml-2"
                 onClick={()=>{this.nextStep(3)}}>Continue</Button>
              </CardFooter>
            </Card>
          )}
          

          {(this.state.next==4) && (
            <Card className="register-card m-0">
              <CardBody className="p-5 pb-0"> <Form>
                <Row className="mb-3">
                  <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                  <Col sm="6" className="text-right">  
                  <h6 className="mt-2">{this.state.userName}</h6> 
                  </Col>
                </Row> 
                <h4 className="text-dark mb-5">Payments Method</h4>
                <Row>
                   <Col sm="6"> 
                   <div className="mb-5 pb-5">
                   <Button outline color="secondary" onClick={()=>console.log('Hello')}>Manual Payment</Button>
                   </div>

                   <div className="checkbox mb-4">
                    <input type="radio" id="additional1" name="Subscription" checked={!this.state.isEasyPayment} onChange={()=>this.changePayment('full')}></input>
                    <label htmlFor="additional1">Full Payment (On Subscription)</label>
                  </div>
                  <div className="checkbox mb-4">
                    <input type="radio" id="additional2" name="Subscription" checked={this.state.isEasyPayment} onChange={()=>this.changePayment('easy')}></input>
                    <label htmlFor="additional2">Easy Payment</label>
                  </div>


                  {this.state.isEasyPayment==true && <Row>
                    <Col sm="6">
                      <FormGroup className="input-line">
                        <Label className="text-light-grey">No. of Installments</Label>
                        <select className="form-control" onChange={this.changeInstallments}>
                        {this.tempInstallments.map(installments => (
                          <option value={installments} key={installments}>{installments}</option>
                        ))}
                          
                        </select>
                      </FormGroup>
                  </Col>
                  <Col sm="6">
                  <FormGroup className="input-line">
                        <Label className="text-light-grey">Discount %</Label>
                        <Input type="text" placeholder="Discount" autoComplete="username"
                        value={this.state.realDiscount} onChange={(e) => this.updateDiscount(e.target.value)} />
                        < ValidationMessage valid={this.state.discountValid} message={this.state.paymentErrorMsg.realDiscount} />
                      </FormGroup>
                    </Col>
                  </Row>}


                   </Col>
                  <Col sm="6" className="text-center">
                    
                  <Card className="ml-5 card-line">
                      <CardBody>
                      <div className="plan-info">
                        <h4 className="text-center mb-3 font-weight-normal">Order Details</h4>
                        <Row>
                          <Col sm="7" className="text-left"><h5 className="font-lg font-weight-bold text-dark">{this.planDetailObject.paymentPlan.planName}</h5></Col>
                          {this.state.isEasyPayment==true && <Col sm="5" className="text-right"><h3 className="text-dark mb-0">{this.amount} {this.state.selectedSinglePlanCurrency}</h3><p className="text-light-grey">per anum</p></Col>}
                          {this.state.isEasyPayment==false && <Col sm="5" className="text-right"><h3 className="text-dark mb-0">{this.amount} {this.state.selectedSinglePlanCurrency}</h3><p className="text-light-grey">per month</p></Col>}

                          </Row>
                          <div className="text-light-grey text-left">
                            <p>{this.state.branches} Branches</p>
                            <p>{this.planDetailObject.paymentPlan.noOfUserPerBranch} Users/Branch</p>
                            <p>{this.planDetailObject.paymentPlan.supportType} Support</p>
                          </div>
                          <hr></hr>
                          <Row>
                          {this.state.isEasyPayment==true &&  <Col sm="5" className="text-left"><h4 className="mb-0 text-dark">Tax (5%)</h4><p className="text-light-grey">per anum</p></Col>}
                          {this.state.isEasyPayment==false &&  <Col sm="5" className="text-left"><h4 className="mb-0 text-dark">Tax (5%)</h4><p className="text-light-grey">per month</p></Col>}
                            <Col sm="7" className="text-right pl-lg-0"><h3 className="font-3xl text-dark">{this.state.tax} {this.state.selectedSinglePlanCurrency}</h3></Col>
                          </Row>
                          <hr></hr>
                          <Row>
                            <Col sm="5" className="text-left"><h3 className="text-primary">Total</h3></Col>
                            {this.state.isEasyPayment==false &&  <Col sm="7" className="text-right pl-lg-0"><h3 className="text-primary mb-0">{this.state.totalAmount} {this.state.selectedSinglePlanCurrency}</h3><p className="mb-0">per month</p></Col>}
                            {this.state.isEasyPayment==true &&  <Col sm="7" className="text-right pl-lg-0"><h3 className="text-primary mb-0">{this.state.totalAmount} {this.state.selectedSinglePlanCurrency}</h3><p className="mb-0">per anum</p></Col>}
                          </Row>
                      </div>
                      </CardBody>
                    </Card>

                  </Col>
                </Row> 

                {this.state.isEasyPayment==true && <Card className="mb-5 mt-3 card-line bg-white">
               
                
                <CardBody>
                      <Table responsive className="table-borderless">
                        <tr>
                          <th></th>
                          <th width="25%" className="text-primary text-center">Due Date</th>
                          <th className="text-primary text-center">Amount <span className="text-warning">%</span></th>
                          <th className="text-primary text-center">Grace Period <span className="text-warning">Days</span></th>
                          <th className="text-primary text-center">Amount <span className="text-warning">{this.state.selectedSinglePlanCurrency}</span>
                          {/* <ButtonDropdown id="card1" isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                            <DropdownToggle caret className="p-0 mb-0 text-warning" color="transparent">
                            AED
                            </DropdownToggle>
                            <DropdownMenu right>
                              <DropdownItem>Action</DropdownItem>
                              <DropdownItem>Another action</DropdownItem>
                              <DropdownItem disabled>Disabled action</DropdownItem>
                              <DropdownItem>Something else here</DropdownItem>
                            </DropdownMenu>
                          </ButtonDropdown> */}
                          </th>
                        </tr>
                        {this.state.allEasyPayments.map((installments,index)=>
                          <tr>
                          <td className="text-warning">#{index+1}</td>
                          <td>
                          <FormGroup className="input-line">
                          <div>
                            {/* if (index==0) {
                              <DatePickerComponent id="calendar" value={installments.dueDate}  allowEdit={false} 
                              onChange={(date) => this.changeStartDate(date,index)}/>
                            }
                            else{ */}
                               <DatePickerComponent id="calendar" value={installments.dueDate}  allowEdit={false} min={this.tempEasyPayments[index].dueDate}
                               onChange={(date) => this.changeStartDate(date,index)}/>
                            {/* } */}
                            
                          </div>
                            {/* <select className="form-control">
                              <option>Set Date</option>
                            </select> */}
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                          <Input type="text"  autoComplete="username"
                          value={installments.amountPercentage} onChange={(e) => this.changeAmountPercentage(e.target.value,index)} />
                          {/* < ValidationMessage valid={this.state.realAmountValid} message={this.state.paymentErrorMsg.realAmount} /> */}
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                          <Input type="text"  autoComplete="username"
                          value={installments.graceDays} onChange={(e) => this.changeGraceDays(e.target.value,index)} />
                          {/* < ValidationMessage valid={this.state.graceDaysValid} message={this.state.paymentErrorMsg.graceDays} /> */}
                          </FormGroup>
                          </td>
                          <td>
                          <FormGroup className="input-line">
                            <Input type="text" autoComplete="username" readOnly value={installments.amountValue}/>
                          </FormGroup>
                          </td>
                        </tr>
                        )}                   
                      </Table>
                  </CardBody>
                </Card>}

              </Form> </CardBody>
              <CardFooter className="p-5 text-right b-a-0 pt-0">
              <Button outline color="primary" onClick={()=>{this.nextStep(2)}}>Back</Button>
                {this.state.isEasyPayment==true && <Button color="primary" className="ml-2" 
                disabled={this.state.isDisableCreate}
                onClick={()=>{this.nextStep(5)}}>Create</Button>}
                {this.state.isEasyPayment==false && 
                <Button color="primary" className="ml-2" 
                disabled={this.state.isDisableCreate}
                onClick={()=>{this.nextStep(5)}}>Create</Button>}
              </CardFooter>
            </Card>
          )}
          {/* {(this.state.next==6) && (
              <Card className="register-card m-0"  style={{height:'100vh'}}>
                <CardBody className="p-5"> <Form>
  
                  <div className="text-center">
                    <h2 className="text-primary mb-5">Registration Completed</h2>
  
                    <div className="icon-success">
                      <i className="fa-5x icon-check"></i>
                    </div>
                    <div className="mb-5">
                      <h1>Great!</h1>
                    </div>
  
                    <h3 className="mb-5 mt-5">You're now ready to use Taboor</h3>
                    <p className="mb-5">A message with your username and password & copy of your invoice <br></br>
                    was sent to your registered email<br></br>
                    Follow the instructions and start using Taboor</p>
                    <Link to="/login">
                        <Button color="primary">Back To Login</Button>
                    </Link>
                    
  
                    </div> 
  
  
                
                </Form> 
                </CardBody>
                
              </Card>
          )} */}

            </Col>

          <Col sm="3" style={{position:'fixed',right:0}}>
            <div className="pl-lg-3 pl-md-2 pl-sm-0 pl-xl-4 pr-4 pt-lg-5 pt-md-4 pt-sm-2">
              <div className="login-logo mb-lg-4 mb-md-2 mb-sm-1 mb-xl-5">
                <AppNavbarBrand className="img-fluid"  full={{ src: logo, width: 250, alt: 'Taboor Logo' }} />
              </div>
              <Steps current={this.state.next}  direction="vertical">
                <Steps.Step title="Basic" description="Lorem ipsum dolor sit amet consectetur adipiscing elit, cupidatat non proident." />
                <Steps.Step title="Plan" description="Lorem ipsum dolor sit amet consectetur adipiscing elit, cupidatat non proident." />
                <Steps.Step title="Payment" description="Lorem ipsum dolor sit amet consectetur adipiscing elit, cupidatat non proident." />
              </Steps>
            </div>
          </Col>

        </Row>
        </Container>
      </div>
    );
  }
}

export default Register;

