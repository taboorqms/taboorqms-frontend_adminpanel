import React, { Component,useState,useEffect } from 'react';
import Geocoder from 'react-native-geocoding';
import { Card, CardBody, CardHeader, Col, Row, Table , 
  DropdownItem,  DropdownMenu,  DropdownToggle, 
  Button, FormGroup,Label,Input,Alert,Dropdown,
  Modal, ModalBody, ModalFooter, ModalHeader,  } from 'reactstrap'; 
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import StarRatingComponent from 'react-star-rating-component';
import { TimePickerComponent } from '@syncfusion/ej2-react-calendars';
import toaster from "toasted-notes";
import axios from "axios";
import { Map, GoogleApiWrapper,Marker } from 'google-maps-react';
import LoadingBar from 'react-top-loading-bar';
const DefaultHeader = React.lazy(() => import('./defaultHeader'));

Geocoder.init("AIzaSyDLN9J4HC_D04AFVx1ZK5n0MQjJaO6YUko");
const mapStyles = {
  height: 300,
  width: '100%'
};
const containerStyle = {
  position: 'relative',  
  height: 300,
  width: '100%'
}
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
export class Breadcrumbs extends Component {
  id=0
  branchId=0
  tempBranchDetailServices=[]
  tempBranchDetailCounters=[]
  tempBranchDetailAgents=[]
  showLessAgents=[]
  showLessAvialableBranches=[]
  showLessCounters=[]
  branchCounterId=0
  modalHeader=''
  modalCounterHeader=''
  modalAgentHeader=''
  totalServices=0
  totalAgents=0
  tempAvialableBranchService=[]
  tempAvialableBranchCounterService=[]
  tempAllPrivileges=[]
  tempAssigendCounter=[]
  totalCounters=0
  selectedPrivileges=[]
  selectedTempWeekSelected=[]
  tempWeekSelected=[]
  weekDays=[{
    'name':'Monday',id:1
  },{
    'name':'Tuesday',id:2
  },{
    'name':'Wednesday',id:3
  },{
    'name':'Thursday',id:4
  },{
    'name':'Friday',id:5
  },{
    'name':'Saturday',id:6
  },{
    'name':'Sunday',id:7
  }]
  constructor(props) {
    super(props);
    this.state = {
      lat:0,
      long:0,
      progress:0,
      workingStatus:false,
      isDisableService:false,
      isDisableCounter:false,
      isDisableWorkingHours:false,
      isShowDropDownMenu:false,
      isShowAllCounter:false,
      isShowAllAgents:false,
      isShowAllServices:false,
      isShowBranchEditPrev:false,
      formValid: false,
      serviceCenterName: '',serviceCenterNameValid: false,
      serviceCenterArabicName: '',serviceCenterArabicNameValid: false,
      email: '', emailValid: false,
      phone: '', phoneValid: false,
      serviceSector:[],
      selectedServiceSectorId:0,
      branchServices:[],
      selectedBranchServiceId:0,
      selectedBranchCounterServiceObject:[],
      avialableBranchCounter:[],
      selectedAvialableBranchCounterId:null,
      allPrivileges:[],
      counterNumber:'',
      agentPK:0,
      selectedWeekName:'',
      selectedWeekDayId:0,
      errorMsg: {},
      dropdownOpen: false,
      startTime:new Date(),
      endTime:new Date(),
      weekDaysObject:[],
      branchName:'',
      branchAddress:'',
      branchNameArabic:'',
      branchEmail:'',
      branchPhoneNo:'',
      totalTickets:0,
      avgTickets:0,
      createdOn:'',
      rating:0,
      serviceCounterSector:[],
      AST:0,
      AWT:0,
      allServices:[],
      allCounter:[],
      allAgents:[],
      radioSelected: 2,
      allWorkingHours:[],
      isEditCloneModal:false,
      isBranchServiceModal:false,
      isBranchCounterModal:false,    
      isWorkingHoursModal:false,
      isAgentModal:false,  
      modal: false,
      large: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
      NextAgentId:'',
      AgentPIN:'',
      agentName: '', agentNameValid: false,
      agentEmail: '', agentEmailValid: false,
      formAgentValid:false

    };
    this.menudropdown = this.menudropdown.bind(this);
  }
  componentDidMount()
{
  if(localStorage.getItem('isShowArabicLanguage')=='true')
  {
    global.isShowArabicLanguage=true
  }
  else
  {
    global.isShowArabicLanguage=false
  }
  var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Branch_Edit')
        {
          this.setState({isShowBranchEditPrev:true})
        }
      }
    }
    this.state.startTime.setHours(9)
    this.state.startTime.setMinutes(0)
    this.state.endTime.setHours(17)
    this.state.endTime.setMinutes(0)
    this.setState({
      selectedWeekName:this.weekDays[0].name,
      selectedWeekDayId:this.weekDays[0].id
    })
    var data=this.props.location
    this.branchId=parseInt(data.branchId)
    this.branchDetails(parseInt(data.branchId))
}
  branchDetails(id)
  {
    this.showLessAgents=[]
    this.showLessCounters=[]
    this.tempBranchDetailAgents=[]
    this.tempBranchDetailCounters=[]
    this.tempBranchDetailServices=[]
    this.selectedTempWeekSelected=[]
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/getCompleteDetails/id', {
    method: 'POST',
    body:JSON.stringify({
      "id": parseInt(id)
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  }).then(response => response.json()).then(responsejson => {
    if(responsejson.applicationStatusCode==0){
      if(responsejson.branch.branchStatus==0|| responsejson.branch.branchStatus==2){
        this.setState({
          workingStatus:false
        })
      }
      else
      {
        this.setState({
          workingStatus:true
        })
      }
      var date=new Date(responsejson.branch.createdTime)
      let d = date.getDate();
      let m = date.toLocaleString('default', { month: 'short' });
      let y=date.getFullYear()
      d = d < 10 ? '0' + d : d;
      // m = m < 10 && m > 0 ? '0' + m : m;
      var creadtedDate=d+" "+m+" "+y 
      var hms = responsejson.branch.averageServiceTime;  
      var a = hms.split(':'); 
      var minutes = (+a[0]) * 60 + (+a[1]); 
    
      var hmss = responsejson.branch.averageWaitingTime;  
      var a = hmss.split(':'); 
      var minutess = (+a[0]) * 60 + (+a[1]); 
      // if(responsejson.branch.averageTicketPerDay)
      this.totalServices=responsejson.branchServicesCompleteObjs.length
      this.totalCounters=responsejson.counterList.length
      this.totalAgents=responsejson.agentList.length
      var data=[]
      for(var g=0;g<responsejson.agentList.length;g++)
      {
        var name=responsejson.agentList[g].agent.serviceCenterEmployee.user.name
        name=name.charAt(0)
        responsejson.agentList[g].agent.serviceCenterEmployee.user['logo']=name
        if(g<2)
        {
          this.showLessAgents.push(responsejson.agentList[g])
        }
        this.tempBranchDetailAgents.push(responsejson.agentList[g])
      }
      for(var j=0;j<responsejson.branchServicesCompleteObjs.length;j++)
      {
        responsejson.branchServicesCompleteObjs[j]['counters']=[]
        responsejson.branchServicesCompleteObjs[j]['isChecked']=true
        for(var x=0;x<responsejson.counterList.length;x++)
        {
          for(var b=0;b<responsejson.counterList[x].providedServices.length;b++)
          {
            if(responsejson.branchServicesCompleteObjs[j].serviceTAB.serviceId==responsejson.counterList[x].providedServices[b].serviceId)
            {
              responsejson.branchServicesCompleteObjs[j]['counters'].push(responsejson.counterList[x].branchCounter.counterNumber)
            }
          }
          
        }
        // if(j<3)
        // {
        //   this.showLessAvialableBranches.push(responsejson.branchServicesCompleteObjs[j])
        // }
        // this.tempBranchDetailServices.push(responsejson.branchServicesCompleteObjs[j])
      }
      for(var i=0;i<responsejson.counterList.length;i++)
      {
        responsejson.counterList[i]['isShowMenu']=false
        responsejson.counterList[i]['noOfCounterServices']= responsejson.counterList[i].providedServices.length
        if(responsejson.counterList[i].assignedToAgent==true)
        {
          responsejson.counterList[i]['agent']=1
        }
        else
        {
          responsejson.counterList[i]['agent']=0
        }
        if(i<2)
        {
          this.showLessCounters.push(responsejson.counterList[i])
        }
        this.tempBranchDetailCounters.push(responsejson.counterList[i])
      }
      for(var c=0;c<responsejson.branchWorkingHourList.length;c++)
      {
        responsejson.branchWorkingHourList[c]['id']=responsejson.branchWorkingHourList[c].day   
        for(var k=0;k<this.weekDays.length;k++)
        {
          if(responsejson.branchWorkingHourList[c].day==this.weekDays[k].id)
          {
            responsejson.branchWorkingHourList[c]['name']=this.weekDays[k].name
            var beforeConversion=JSON.parse(JSON.stringify(responsejson.branchWorkingHourList[c]))
            var data=responsejson.branchWorkingHourList[c].openingTime
            var requestStartTime
            var requestEndTime
            var strTime=data.split(':')
            var sHours=strTime[0]
            var sMinutes=strTime[1]
            sHours = sHours < 10 ?'0' + sHours : sHours;
            // sMinutes = sMinutes < 10 ? '0' + sMinutes : sMinutes;
            requestStartTime=sHours + ':' + sMinutes 
            var ampm = sHours >= 12 ? 'PM' : 'AM';
            sHours = sHours % 12;
            sHours = sHours ? sHours : 12; 
            var sTime=sHours+":"+sMinutes+" "+ampm
            responsejson.branchWorkingHourList[c]['startTime']=sTime
            responsejson.branchWorkingHourList[c]['openingTime']=sTime
            var data1=responsejson.branchWorkingHourList[c].closingTime

            var etrTime=data1.split(':')

            var eHours=etrTime[0]
            var eMinutes=etrTime[1]
            eHours = eHours < 10 ?'0' + eHours : eHours;
            // eMinutes = eMinutes < 10 ? '0' + eMinutes : eMinutes;
            requestEndTime=eHours + ':' + eMinutes 
            var ampm = eHours >= 12 ? 'PM' : 'AM';
            eHours = eHours % 12;
            eHours = eHours ? eHours : 12; 
            var eTime=eHours+":"+eMinutes+" "+ampm
            responsejson.branchWorkingHourList[c]['endTime']=eTime
            responsejson.branchWorkingHourList[c]['closingTime']=eTime
            var afterCoversion=responsejson.branchWorkingHourList[c]
            this.tempWeekSelected.push(beforeConversion)
            this.selectedTempWeekSelected.push(afterCoversion)
          }
        }
      }
        this.setState({
          // lat:responsejson.branch.latitude,
          // long:responsejson.branch.longitude,
          weekDaysObject:this.selectedTempWeekSelected,
          allWorkingHours:responsejson.branchWorkingHourList,
          allServices:responsejson.branchServicesCompleteObjs,
          allAgents:this.showLessAgents,
          allCounter:this.showLessCounters,
          branchName:responsejson.branch.branchName,
          branchNameArabic:responsejson.branch.branchNameArabic,
          branchAddress:responsejson.branch.address,
          branchEmail:responsejson.branch.emailAddress,
          branchPhoneNo:responsejson.branch.phoneNumber,
          totalTickets:responsejson.totalTicketCount,
          avgTickets:responsejson.branch.averageTicketPerDay,
          createdOn:creadtedDate,
          rating:responsejson.branch.rating,
          AST:minutes,
          AWT:minutess,
              
        })
        
    }
    

  })
  }
  showMenu(id)
  {
      for(var i=0;i<this.state.allCounter.length;i++)
      {
        if(this.state.allCounter[i].branchCounter.counterId==id)
        {
          this.state.allCounter[i]['isShowMenu']=true
          this.setState({
            isShowDropDownMenu:true
          })
        }
        else
        {
          this.state.allCounter[i]['isShowMenu']=false
        }
      }
      var data=this.state.allCounter
      this.setState({
        allCounter:data
      })
  }
  viewAllCounters()
  {
    this.setState({
      allCounter:this.tempBranchDetailCounters,
      isShowAllCounter:true
    })
  }
  viewLessCounters()
  {
    this.setState({
      allCounter:this.showLessCounters,
      isShowAllCounter:false
    })
  }
  viewAllAgents()
  {
    this.setState({
      allAgents:this.tempBranchDetailAgents,
      isShowAllAgents:true
    })
  }
  viewLessAgents()
  {
    this.setState({
      allAgents:this.showLessAgents,
      isShowAllAgents:false
    })
  }
  menudropdown(i) {
    const newArray = this.state.dropdownOpen.map((element, index) => {
      return (index === i ? !element : false);
    });
    this.setState({
      dropdownOpen: newArray,
    });
  }
  updateServiceCenterName = (serviceCenterName) => {
    this.setState({serviceCenterName}, this.validateServiceCenterName)
  }
  
  validateServiceCenterName = () => {
    const {serviceCenterName} = this.state;
    let serviceCenterNameValid = true;
    let errorMsg = {...this.state.errorMsg}
  
    if (serviceCenterName.length == 0 || serviceCenterName=='' ) {
      serviceCenterNameValid = false;
      errorMsg.serviceCenterName = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(serviceCenterName))
    // {
    //   serviceCenterNameValid = false;
    //   errorMsg.serviceCenterName = 'Only alphabets are allowed'
    // }
    this.setState({serviceCenterNameValid, errorMsg}, this.validateForm)
  }
  updateArabicServiceCenterName = (serviceCenterArabicName) => {
    this.setState({serviceCenterArabicName}, this.validateArabicServiceCenterName)
  }
  
  validateArabicServiceCenterName = () => {
    const {serviceCenterArabicName} = this.state;
    let serviceCenterArabicNameValid = true;
    let errorMsg = {...this.state.errorMsg}
  
    if (serviceCenterArabicName.length == 0 ||serviceCenterArabicName=='' ) {
      serviceCenterArabicNameValid = false;
      errorMsg.serviceCenterArabicName = 'Required'
    }
    else if(!/^[\u0621-\u064A\040]+$/.test(serviceCenterArabicName))
    {
      serviceCenterArabicNameValid = false;
      errorMsg.serviceCenterArabicName = 'Only arabic alphabets are allowed'
    }
    this.setState({serviceCenterArabicNameValid, errorMsg}, this.validateForm)
  }
  updatePhoneNo = (phone) => {
    this.setState({phone}, this.validatePhoneNo)
  }
  
  validatePhoneNo = () => {
    const {phone} = this.state;
    let phoneValid = true;
    let errorMsg = {...this.state.errorMsg}
  
    if (phone.length == 0 || phone=='') {
      phoneValid = false;
      errorMsg.phone = 'Required'
    }
     else if (phone.length <8) {
      phoneValid = false;
      errorMsg.phone = 'Phone number must be 8 characters'
    }
    else if (!/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/.test(phone)){
      phoneValid = false;
      errorMsg.phone = 'Invalid phone format'
    }
    this.setState({phoneValid, errorMsg}, this.validateForm)
  }
  updateEmail = (email) => {
    this.setState({email}, this.validateEmail)
  }
  
  validateEmail = () => {
    const {email} = this.state;
    let emailValid = true;
    let errorMsg = {...this.state.errorMsg}
  
    if (email.length == 0 || email=='') {
      emailValid = false;
      errorMsg.email = 'Required'
    }
    else if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){
      emailValid = false;
      errorMsg.email = 'Invalid email format'
    }
  
    this.setState({emailValid, errorMsg}, this.validateForm)
  }
  updateAgentName = (agentName) => {
    this.setState({agentName}, this.validateAgentName)
  }
  validateAgentName = () => {
    const {agentName} = this.state;
    let agentNameValid = true;
    let errorMsg = {...this.state.errorMsg}
  
    if (agentName.length == 0 ||agentName=='' ) {
      agentNameValid = false;
      errorMsg.agentName = 'Required'
    }
    // else if(!/^[a-zA-Z\s]*$/.test(agentName))
    // {
    //   agentNameValid = false;
    //   errorMsg.agentName =  'Only alphabets are allowed'
    // }
    this.setState({agentNameValid, errorMsg}, this.validateAgentForm)
  }
  updateAgentEmail= (agentEmail) => {
    this.setState({agentEmail}, this.validateAgentEmail)
  }
  validateAgentEmail = () => {
    const {agentEmail} = this.state;
    let agentEmailValid = true;
    let errorMsg = {...this.state.errorMsg}
  
    // checks for format _@_._
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(agentEmail)){
      agentEmailValid = false;
      errorMsg.agentEmail = 'Invalid email format'
    }
  
    this.setState({agentEmailValid, errorMsg}, this.validateAgentForm)
  }
  validateForm = () => {
    const {serviceCenterNameValid, emailValid,phoneValid,serviceCenterArabicNameValid} = this.state;
    this.setState({
      formValid: serviceCenterNameValid && emailValid && phoneValid  && serviceCenterArabicNameValid
    })
  }
  validateAgentForm = () => {
    const {agentEmailValid, agentNameValid} = this.state;
    this.setState({
      formAgentValid: agentEmailValid && agentNameValid
    })
  }

  toggleLarge(type) {
    if(type=='clone')
    {
      this.setState({
        isEditCloneModal:true
      });
      this.modalHeader='clone'
    }
    else if(type=='edit')
    {
      this.getAddress(this.state.branchAddress)
      this.setState({
        serviceCenterName:this.state.branchName,
        serviceCenterArabicName:this.state.branchNameArabic,
        phone:this.state.branchPhoneNo,
        email:this.state.branchEmail,
        isEditCloneModal:true,
      });
      this.updateServiceCenterName(this.state.branchName)
      this.updateArabicServiceCenterName(this.state.branchNameArabic)
      this.updateEmail(this.state.branchEmail)
      this.updatePhoneNo(this.state.branchPhoneNo)

      this.modalHeader='edit'
    }
    
  }
  cloneBranch()
  {
    this.setState({
      progress:50
    })
    if(this.modalHeader=='clone')
    {
      let token = localStorage.getItem('sessionToken')
      fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/clone', {
      method: 'POST',
      body:JSON.stringify({
        "address": "Chaklala Cantt., Rawalpindi",
        "branchName": this.state.serviceCenterName,
        "branchNameArabic": this.state.serviceCenterArabicName,
        "city": "Rawalpindi",
        "email": this.state.email,
        "latitude": 27.2038,
        "longitude": 77.5011,
        "phoneNumber": this.state.phone,
        "sampleBranchId": this.branchId
    
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responsejson => {
      if(responsejson.applicationStatusCode==0)
      {
        this.setState({
          progress:100
        })
        this.branchId=responsejson.values.BranchId
        global.isBranchAgentCounterUpdated.next(true)
        global.isNotificationUpdated.next(true)
        this.branchDetails(responsejson.values.BranchId)
        this.closeEditCloneBranchModel()
      
      }
      else{
        this.setState({
          progress:100
        })
        toaster.notify(responsejson.devMessage, {
          duration:5000
        });
      }
    })
    }
    else if(this.modalHeader=='edit')
    {
      let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/update', {
      method: 'POST',
      body:JSON.stringify({
        "branchId": this.branchId,
        "address": this.state.branchAddress,
        "branchName": this.state.serviceCenterName,
        "branchNameArabic": this.state.serviceCenterArabicName,
        "city": "Rawalpindi",
        "email": this.state.email,
        "latitude": this.state.lat,
        "longitude": this.state.long,
        "phoneNumber": this.state.phone
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responsejson => {
      if(responsejson.applicationStatusCode==0)
      {
        this.setState({
          progress:100
        })
        this.branchId=responsejson.values.BranchId
        global.isNotificationUpdated.next(true)
        this.branchDetails(responsejson.values.BranchId)
        this.closeEditCloneBranchModel()
        // this.setState({
        //   isEditCloneModal:false
        // })
      
      }
      else{

        this.setState({
          progress:100
        })
        toaster.notify(responsejson.devMessage, {
          duration:5000
        });
      }
    })
    }
    
  }
  resetEditCloneBranch()
  {
    this.setState({
      serviceCenterName:'',
      serviceCenterArabicName:'',
      phone:'',
      email:'',
      formValid:false
    })

  }
  closeEditCloneBranchModel()
  {
    this.setState({
      isEditCloneModal:false,
      serviceCenterName:'',
      serviceCenterArabicName:'',
      phone:'',
      email:'',
      formValid:false
    })
  }

branchservicemodal() {
  this.setState({
    isBranchServiceModal: true,
  });
  
  this.getBranchServices()
}
getBranchServices()
{
  let token = localStorage.getItem('sessionToken')
  const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getServices';
  const header={headers: {
    'Authorization': `Bearer ${token}`
  }} 
  axios.get(url,header).then(responseJson => {
    if(responseJson.data.applicationStatusCode===0)
    {
      let data = responseJson.data.services.filter(o1 => !this.state.allServices.find(o2 => o1.serviceTAB.serviceId === o2.serviceTAB.serviceId));
      // data.unshift({serviceId:0,serviceName:'Services'})
      this.setState({
        branchServices:data,
      })
    }
  }).catch(err=>{
    console.log(err)
  });
}
addBranchService()

{
  if(this.state.selectedBranchServiceId==0)
  {
    toaster.notify('Kindly select atleast one service',{
      duration:5000
    })
      return
  }
  this.setState({
    isDisableService:true,
    progress:50
  })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/service/add', {
    method: 'POST',
    body:JSON.stringify({
      "branchId": this.branchId,
     'serviceList':[
       {
        "serviceId": this.state.selectedBranchServiceId
       }
     ]

    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responsejson => {
    if(responsejson.applicationStatusCode==0)
    {
      this.branchDetails(this.branchId)
      this.setState({
        progress:100,
        isBranchServiceModal:false,
        selectedBranchServiceId:0,
        isDisableService:false
      })
    
    }
    else{
      this.setState({
        isDisableService:false,
        progress:100,
      })
      toaster.notify(responsejson.devMessage, {
        duration:5000
      });
    }
  })
}
deleteBranchService(id) {
  var token = localStorage.getItem('sessionToken');
  var data=[];
  data.push(this.branchId);
  data.push(id);

  confirmAlert({
    title: '',
    message: 'Do you want to delete service from branch?',
    buttons: [
      {
        label: 'Yes',
        onClick: () => {
          fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/service/delete', {
          method: 'POST',
          body:JSON.stringify({
            'ids':data
          }),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          },
          }).then(response => response.json()).then(responsejson => {
            if(responsejson.applicationStatusCode==0)
            {
              this.branchDetails(this.branchId)
            }
            else{
              toaster.notify(responsejson.devMessage, {
                duration:5000
              });
            }
          })
        }
      },
      {
        label: 'No',
        onClick: () => { console.log('no selected'); }
      }
    ]
  });
  
}
closeBranchServiceModel()
{
  this.setState({
    selectedBranchServiceId:0,
  branchServices:[],
  isBranchServiceModal:false
  })
}
resetBranchService()
{
  this.setState({
    selectedBranchServiceId:0,
    branchServices:[]
  })
  this.getBranchServices()
}
changeBranchService=e=>
{
  this.setState({selectedBranchServiceId:parseInt(e.target.value)})
}


branchcountermodal(type,data) {
  this.setState({
    isBranchCounterModal: true,
  });
  if(type=='add')
  {
    this.tempAvialableBranchCounterService=[]
    this.modalCounterHeader='add'
    this.getNextCounterNumber()
    this.getBranchCounterServices()
  }
  else if(type=='edit')
  {
    this.tempAvialableBranchCounterService=[]
    this.getBranchCounterServices()
    for(var i=0;i<data.providedServices.length;i++)
    {
      this.tempAvialableBranchCounterService.push(data.providedServices[i])
    }
    this.branchCounterId=data.branchCounter.counterId

    this.setState({
      counterNumber:data.branchCounter.counterNumber,
      selectedBranchCounterServiceObject:data.providedServices
    })
    this.modalCounterHeader='edit'
  }
 
}
getBranchCounterServices()
{
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/getServices', {
    method: 'POST',
    body:JSON.stringify({
    'id':parseInt(this.branchId)
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(branchServiceResponse => {
    console.log('=============branchServiceResponse=======================');
    console.log(branchServiceResponse);
    console.log('=============branchServiceResponse=======================');
    if(branchServiceResponse.applicationStatusCode==0)
    {
      // branchServiceResponse.unshift({serviceId:0,serviceName:'Services'})
      this.setState({
        serviceCounterSector:branchServiceResponse.serviceList,
      })
    }
  })
}
getNextCounterNumber()
{
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/getNextCounterNumber', {
    method: 'POST',
    body:JSON.stringify({
    'id':parseInt(this.branchId)
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(counterResponse => {
    if(counterResponse.applicationStatusCode==0)
    {
      this.setState({counterNumber:counterResponse.values.NextCounterNumber})
    }
    else{
      toaster.notify(counterResponse.devMessage, {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
  })
}
changeServiceSector=e=>
{
  if(parseInt(e.target.value)!=0)
  {
    var repeat= false
    for(var i=0;i<this.state.serviceCounterSector.length;i++)
    {
      if(this.state.serviceCounterSector[i].serviceId==parseInt(e.target.value))
      {

        for(var j=0;j<this.state.selectedBranchCounterServiceObject.length;j++){
          if(this.state.serviceCounterSector[i].serviceId===this.state.selectedBranchCounterServiceObject[j].serviceId){
            repeat=true
          }

        }
        console.log(repeat)
        if(repeat ===true){
          toaster.notify('Cannot add duplicate service',{
            duration:1000
          })
        }

        else{
          this.tempAvialableBranchCounterService.push(this.state.serviceCounterSector[i]) 

        } 
      }
    }
    // var tempArray=[...new Map(this.tempAvialableBranchCounterService.map(o => [o.serviceId, o])).values()]
    // this.tempAvialableBranchCounterService=tempArray
    this.setState({selectedBranchCounterServiceObject:this.tempAvialableBranchCounterService})
  }
}
onDismissAvialableBranchService(id) {
  for(var i=0;i<this.tempAvialableBranchCounterService.length;i++)
  {
    if(id==this.tempAvialableBranchCounterService[i].serviceId)
    {
      this.tempAvialableBranchCounterService.splice(i,1)
    }
  }
  this.setState({ selectedBranchCounterServiceObject: this.tempAvialableBranchCounterService,serviceCounterSector:[] });
  this.getBranchCounterServices()
}
addUpdateCounter()
{
  if(this.state.selectedBranchCounterServiceObject.length==0)
  {
    toaster.notify('Kindly select atleast one service',{
      duration:5000
    })
    return;
  }
  this.setState({
    isDisableCounter:true,
    progress:50,
  })
  if(this.modalCounterHeader=='add')
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/addOrUpdate', {
      method: 'POST',
      body:JSON.stringify({
        "branchId": this.branchId,
        "counterId": null,
        "services": this.state.selectedBranchCounterServiceObject,
        "update": false    
       }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.branchDetails(this.branchId)
        this.setState({isShowAllCounter:false,
          
          progress:100,isBranchCounterModal:false,isDisableCounter:false,selectedBranchCounterServiceObject:[]})
      }
      else{
        this.setState({
          isShowAllCounter:false,
          isDisableCounter:false,progress:100,
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
  else if(this.modalCounterHeader=='edit')
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/addOrUpdate', {
      method: 'POST',
      body:JSON.stringify({
        "branchId": this.branchId,
        "counterId": this.branchCounterId,
        "services": this.state.selectedBranchCounterServiceObject,
        "update": true    
       }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
       
        this.branchDetails(this.branchId)
        this.setState({isShowAllCounter:false,progress:100,isBranchCounterModal:false,isDisableCounter:false,selectedBranchCounterServiceObject:[]})
      }
      else{
        this.setState({
          progress:100,isDisableCounter:false,isShowAllCounter:false,
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
 
}
deleteCounter(id)
{
  let token = localStorage.getItem('sessionToken')
  confirmAlert({
    title: '',
    message: 'Do you want to delete counter from branch?',
    buttons: [
      {
        label: 'Yes',
        onClick: () => {
          fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/delete', {
            method: 'POST',
            body:JSON.stringify({
              'ids':[parseInt(id)]
            }),
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            },
          }).then(response => response.json()).then(responseJson => {
            if(responseJson.applicationStatusCode==0){this.branchDetails(this.branchId);}
            else{toaster.notify(responseJson.devMessage, {duration:5000 });}
          })
        }
      },
      {
        label: 'No',
        onClick: () => { console.log('no selected'); }
      }
    ]
  });
  
}
resetCounter()
{
  this.tempAvialableBranchCounterService=[]
  this.setState({
    selectedBranchCounterServiceObject:[],
    serviceCounterSector:[]
    
  })
  this.getBranchCounterServices()
}
closeCounterModel()
{
  this.tempAvialableBranchCounterService=[]
  this.setState({
    selectedBranchCounterServiceObject:[],
    serviceCounterSector:[],
    isBranchCounterModal:false
  })
}

workinghoursmodal() {
  this.setState({
    isWorkingHoursModal: true,
    weekDaysObject:[]
  });
  this.tempWeekSelected=[]
  this.selectedTempWeekSelected=[]
  this.branchDetails(this.branchId)
}
addWeekDays()
{
  this.id=this.id+1
  var requestStartTime
  var requestEndTime
  var sHours = this.state.startTime.getHours();
  var sMinutes = this.state.startTime.getMinutes();
  sHours = sHours < 10 ?'0' + sHours : sHours;
  sMinutes = sMinutes < 10 ? '0' + sMinutes : sMinutes;
  requestStartTime=sHours + ':' + sMinutes 
  var ampm = sHours >= 12 ? 'PM' : 'AM';
  sHours = sHours % 12;
  sHours = sHours ? sHours : 12;
 
  var strTime = sHours + ':' + sMinutes + ' ' + ampm;
 

  var eHours = this.state.endTime.getHours();
  var eMinutes = this.state.endTime.getMinutes();
  eHours = eHours < 10 ?'0' + eHours : eHours;
  eMinutes = eMinutes < 10 ? '0' + eMinutes : eMinutes;
  requestEndTime=eHours + ':' + eMinutes 
  var ampm = eHours >= 12 ? 'PM' : 'AM';
  eHours = eHours % 12;
  eHours = eHours ? eHours : 12;
  
 
  var endTime = eHours + ':' + eMinutes + ' ' + ampm;

  for(var i=0;i<this.tempWeekSelected.length;i++)
  {
    if(this.state.selectedWeekDayId==this.tempWeekSelected[i].day)
    {
      toaster.notify('Week Day is already selected',{
        duration:5000
      })
      return;
    }
  }
  this.tempWeekSelected.push({
     
    "closingTime":requestEndTime,
    "day": this.state.selectedWeekDayId,
    "openingTime": requestStartTime

  })
  this.selectedTempWeekSelected.push({
    'id':this.id,
    "closingTime": endTime,
    "day": this.state.selectedWeekDayId,
    'name':this.state.selectedWeekName,
    "openingTime":strTime

  })
  this.setState({
      weekDaysObject:this.selectedTempWeekSelected
   })
}
onDismissWeekDays(id)
{
  for(var i=0;i<this.selectedTempWeekSelected.length;i++)
  {
    if(id==this.selectedTempWeekSelected[i].day)
    {
      this.selectedTempWeekSelected.splice(i,1)
      this.tempWeekSelected.splice(i,1)
    }
  }
  this.setState({ weekDaysObject: this.selectedTempWeekSelected });
}
changeStartTime(time)
{
  this.setState({startTime: time.value})
}
changeEndTime(time)
{
  this.setState({endTime: time.value})
}
changeWeekDays=e=>
  {
    this.setState({selectedWeekDayId:parseInt(e.target.value)})
    for(var i=0;i<this.weekDays.length;i++)
    {
      if(this.weekDays[i].id==parseInt(e.target.value))
      {
       
        this.setState({selectedWeekDayId:parseInt(e.target.value),selectedWeekName:this.weekDays[i].name})
      }
    }
}
updateWorkingHours()
{
  if(this.tempWeekSelected.length==0)
  {
    toaster.notify('Kindly select working hours',{
      duration:5000
    })
    return;
  }
  this.setState({
    isDisableWorkingHours:true,
    progress:50,
  })
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branch/workingHours/update', {
      method: 'POST',
      body:JSON.stringify({
        "branchId": this.branchId,
        "workingHoursList":this.tempWeekSelected
       }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.tempWeekSelected=[]
        this.selectedTempWeekSelected=[]
        this.setState(
          {
            progress:100,
            isDisableWorkingHours:false,
            weekDaysObject:[],

          }
        )
        this.closeWorkingHour()
       this.branchDetails(this.branchId)
      }
      else{
        this.setState({
          progress:100,
          isDisableWorkingHours:false
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
}
resetWorkingHour()
{
  this.tempWeekSelected=[]
  this.selectedTempWeekSelected=[]
  this.setState(
    {
      selectedWeekDayId:1,selectedWeekName:'Monday',
      weekDaysObject:[],

    }
  )
}
closeWorkingHour()
{
  this.tempWeekSelected=[]
  this.selectedTempWeekSelected=[]
  this.setState(
    {
      selectedWeekDayId:1,selectedWeekName:'Monday',
      weekDaysObject:[],
      isWorkingHoursModal:false

    }
  )
}


agentModal(type,data)
{
  this.setState({
    isAgentModal: true,
  });
  if(type=='add')
  {
    this.tempAssigendCounter=[]
    this.getNextCredentials()
    this.getAvilableBranchCounter()
    this.getAgentPrevileges()
    this.modalAgentHeader='add'
  }
  else if(type=='edit')
  {
    this.modalAgentHeader='edit'
    this.tempAssigendCounter=[]
    this.tempAllPrivileges=[]
    // this.getNextCredentials()
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/get/branch', {
    method: 'POST',
    body:JSON.stringify({
      'id':this.branchId
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      for(var i=0;i<responseJson.branchCounterList.length;i++)
      {
        this.tempAssigendCounter.push(responseJson.branchCounterList[i])
      }
      var isMatch=false
      if(data.assignedBranchCounter!=null)
      {
        for(var x=0;x<this.tempAssigendCounter.length;x++)
        {
          if(this.tempAssigendCounter[x].branchCounter.counterId==data.assignedBranchCounter.counterId)
          {
            isMatch=true
            break;
          }
          else{
              isMatch=false
            }
          
        }
        if(isMatch==false)
        {
          this.tempAssigendCounter.push({'branchCounter':data.assignedBranchCounter})
          // this.tempAssigendCounter[this.tempAssigendCounter.length-1]['isDisabled']=false
        }
        this.setState({ 
          selectedAvialableBranchCounterId:data.assignedBranchCounter.counterId,
          avialableBranchCounter:this.tempAssigendCounter
        })
      }
      else
      {
        if(this.tempAssigendCounter.length!=0)
        {
          this.setState({
            selectedAvialableBranchCounterId:this.tempAssigendCounter[0].branchCounter.counterId,
            avialableBranchCounter:this.tempAssigendCounter
          })
        }
       
      }
      
      for(var j=0;j<data.assignedPrivileges.length;j++)
      {
        data.assignedPrivileges[j]['isChecked']=true
      }
      this.tempAllPrivileges.push(data.assignedPrivileges)
    
      this.setState({
        NextAgentId:data.agent.serviceCenterEmployee.employeeNumber,
        agentPk:data.agent.agentId,
        agentName:data.agent.serviceCenterEmployee.user.name,
        agentEmail:data.agent.serviceCenterEmployee.user.email,
        allPrivileges:data.assignedPrivileges,
        AgentPIN:data.agentPin
        
      
      })

      this.updateAgentName(data.agent.serviceCenterEmployee.user.name)
      this.updateAgentEmail(data.agent.serviceCenterEmployee.user.email)
      }
    })
  }
}
getNextCredentials()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getNextCredentials', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({NextAgentId:responseJson.values.NextAgentId,
          AgentPIN:responseJson.values.AgentPIN
        })
      }
    })
}
getAvilableBranchCounter()
{
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/get/branch', {
    method: 'POST',
    body:JSON.stringify({
      'id':this.branchId
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      
      for(var i=0;i<responseJson.branchCounterList.length;i++)
      {
        this.tempAssigendCounter.push(responseJson.branchCounterList[i])
      }
      if(this.tempAssigendCounter.length!=0)
      {
        this.setState({avialableBranchCounter:responseJson.branchCounterList,
          selectedAvialableBranchCounterId:this.tempAssigendCounter[0].branchCounter.counterId
        })
      }
     
    }
  })
}

changeAssigendCounter=e=>
{
  if(parseInt(e.target.value)!=0)
  {
    for(var i=0;i<this.state.avialableBranchCounter.length;i++)
    {
      if(this.state.avialableBranchCounter[i].branchCounter.counterId==parseInt(e.target.value))
      {
        if(this.state.avialableBranchCounter[i].assignedToAgent!=true)
        {
          this.setState({selectedAvialableBranchCounterId:this.state.avialableBranchCounter[i].branchCounter.counterId})
        }
        else{
          toaster.notify('Counter is already assigend',{
            duration:5000
          })
          return
        }
        
      }
    }
  }
}
getAgentPrevileges()
{
  this.tempAllPrivileges=[]
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getPrivileges', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      for(var i=0;i<responseJson.privilegeList.length;i++)
      {
        var key=responseJson.privilegeList[i].privilegeName
            var replaced = key.split('_').join(' ');
            responseJson.privilegeList[i].privilegeName=replaced
        responseJson.privilegeList[i]['isChecked']=true
      }
      this.tempAllPrivileges.push(responseJson.privilegeList)
      this.setState({allPrivileges:responseJson.privilegeList})
      
    }
  })
}
changePriveleges(data)
{
  for(var i=0;i<this.tempAllPrivileges[0].length;i++)
  {
    if(data.privilegeId==this.tempAllPrivileges[0][i].privilegeId)
    {
      if(this.tempAllPrivileges[0][i]['isChecked']==true)
      {
        this.tempAllPrivileges[0][i]['isChecked']=false
        
      }
      else
      {
        this.tempAllPrivileges[0][i]['isChecked']=true
        
        // this.selectedPrivileges.push(this.tempAllPrivileges[i])
      }

    }
  }
  this.setState({allPrivileges:this.tempAllPrivileges[0]})
}
addUpdateAgent()
{
  for(var o=0;o<this.tempAssigendCounter.length;o++)
  {
    if(this.tempAssigendCounter[o].branchCounter.counterId==this.state.selectedAvialableBranchCounterId)
    {
      if(this.tempAssigendCounter[o].assignedToAgent==true && this.modalAgentHeader=='add')
      {
        toaster.notify('Counter is already assigned',{
          duration:5000
        })
        return
      }
      else{
        this.tempAssigendCounter[o].assignedToAgent=false
      }
     
    }
  }
  for(var i=0;i<this.tempAllPrivileges[0].length;i++)
  {
    if(this.tempAllPrivileges[0][i]['isChecked']==true)
    {
      var key=this.tempAllPrivileges[0][i].privilegeName
      var replaced = key.split(' ').join('_');
      this.tempAllPrivileges[0][i].privilegeName=replaced
      this.selectedPrivileges.push(this.tempAllPrivileges[0][i])
    }
  }
  if(this.selectedPrivileges.length==0)
  {
   toaster.notify('Kindly select atleast one privilage',{
    duration:5000
   })
     return
  }
  for(var i=0;i<this.selectedPrivileges.length;i++)
  {
      delete this.selectedPrivileges[i]['isChecked']
  }
  this.setState({
    formAgentValid:false,progress:50
  })
 let token = localStorage.getItem('sessionToken')
  if(this.modalAgentHeader=='add')
  {
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/add', {
      method: 'POST',
      body:JSON.stringify({
        "agentId": this.state.NextAgentId,
        "agentName": this.state.agentName,
        "agentPin": this.state.AgentPIN,
        "assignedCounterId": this.state.selectedAvialableBranchCounterId,
        "branchId": this.branchId,
        "email": this.state.agentEmail,
        "privileges":this.selectedPrivileges
       }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.selectedPrivileges=[]
        global.isBranchAgentCounterUpdated.next(true)
        global.isNotificationUpdated.next(true)
        this.setState({agentEmail:'',agentName:'',formAgentValid:true,progress:100,})
        this.closeAgentModel()
       this.branchDetails(this.branchId)
      }
      else{
        this.setState({formAgentValid:true,progress:100,})
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
  else if(this.modalAgentHeader=='edit')
  {
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/update', {
      method: 'POST',
      body:JSON.stringify({
        "agentId": this.state.NextAgentId,
        "agentName": this.state.agentName,
        "agentPk": this.state.agentPk,
        "assignedCounterId": this.state.selectedAvialableBranchCounterId,
        "branchId": parseInt(this.branchId),
        "email": this.state.agentEmail,
        "privileges":this.selectedPrivileges,
        "updateStatus": 1
       }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.selectedPrivileges=[]
        global.isNotificationUpdated.next(true)
        this.setState({agentEmail:'',agentName:'',progress:100,formAgentValid:true,isShowAllAgents:false,})
        this.closeAgentModel()
       this.branchDetails(this.branchId)
      }
      else{
        this.setState({formAgentValid:true,progress:100,isShowAllAgents:false,})
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
}
deleteAgent(id)
 {
  let token = localStorage.getItem('sessionToken')
  confirmAlert({
    title: '',
    message: 'Do you want to delete agent from branch?',
    buttons: [
      {
        label: 'Yes',
        onClick: () => {
          fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/update', {
            method: 'POST',
            body:JSON.stringify({
              'agentPk':parseInt(id),
              'branchId':-1
            }),
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            },
          }).then(response => response.json()).then(responseJson => {
            if(responseJson.applicationStatusCode==0){
              global.isBranchAgentCounterUpdated.next(true)
              global.isNotificationUpdated.next(true)
              this.branchDetails(this.branchId)
              this.getAvilableBranchCounter()
            }
            else{
              toaster.notify(responseJson.devMessage, {duration:5000});
            }
          })
        }
      },
      {
        label: 'No',
        onClick: () => { console.log('no selected'); }
      }
    ]
  });
  
 }
resetAgent()
{
  for(var i=0;i<this.selectedPrivileges.length;i++)
  {
     this.selectedPrivileges[i]['isChecked']=false
  }
 this.selectedPrivileges=[]
 this.tempAllPrivileges=[]
 this.setState({agentEmail:'',agentName:'',formAgentValid:false,allPrivileges:[]})
 this.getAgentPrevileges()
}
closeAgentModel()
{
  this.selectedPrivileges=[]
  this.tempAllPrivileges=[]
  this.setState({agentEmail:'',agentName:'',formAgentValid:false,allPrivileges:[],isAgentModal:false,
isShowAllAgents:false})
}
changeCounterStatus(id)
{
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/status/change', {
    method: 'POST',
    body:JSON.stringify({
      'id':parseInt(id),
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      this.branchDetails(this.branchId)
      this.getAvilableBranchCounter()
    }
    else{
      toaster.notify(responseJson.devMessage, {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
  })
}
getAddress(data)
{
  console.log(data);
  Geocoder.from(data).then(json => {
    var location = json.results[0].geometry.location;
    console.log(location);
    this.setState({ lat:location.lat, long:location.lng })
  }).catch(error => console.warn(error));
}
  render() {
    return (
      <div className="animated fadeIn">
        <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress:0
          })}
        />
        <DefaultHeader/>
        <Row className="mb-3 hidden">
          <Col sm="3">
            <h4 className="mb-0">{this.state.branchName}</h4>
            <p className="text-light-grey"><i className="icon2-location"></i> {this.state.branchAddress} <span>Open Map</span></p>
          </Col>
          <Col sm="5" className="text-right" style={{alignSelf:'center'}}>
            <span className="badge badge-info pl-3 pr-3 mb-1 mr-2"><i className="icon2-wall-clock"></i> Avg. Service Time {this.state.AST} min</span>
            <span className="badge badge-info pl-3 pr-3"><i className="icon2-hourglass"></i> Avg. Waiting Time {this.state.AWT} min</span>
          </Col>
          <Col sm="4" className="text-right">
            {this.state.isShowBranchEditPrev==true && <button className="btn btn-outline-secondary ml-2" onClick={()=>this.toggleLarge('clone')}>Clone <i className="icon2-clone-copy"></i></button>}
            {this.state.isShowBranchEditPrev==true &&  <button className="btn btn-outline-secondary ml-2" onClick={()=>this.toggleLarge('edit')}>Edit <i className="icon2-edit-pencil"></i></button>}
            {/* {this.state.isShowBranchEditPrev==true &&  <button className="btn btn-outline-secondary ml-2">More <i className="icon2-menu-options font-sm"></i></button>} */}
          </Col>
        </Row>
        
        

        <Row className="mb-3">

          <Col xs="12" sm="6" lg="3">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                        <i className="font-3xl icon2-calendar-dots text-light-grey"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p className="mb-2 text-light-grey">Created on</p>
                      <h5 className="mb-0">{this.state.createdOn} &nbsp;</h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>


          <Col xs="12" sm="6" lg="3">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                        <i className="font-3xl icon2-wall-clock text-light-grey"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p className="mb-2 text-light-grey">Avg. Service Time</p>
                      <h5 className="mb-0">{this.state.AST} min</h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                    <i className="font-3xl icon2-empty-star text-light-grey"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      {global.isShowArabicLanguage==false && <p className="mb-2 text-light-grey">Customer Rating</p>}
                      {global.isShowArabicLanguage==true && <p className="mb-2 text-light-grey">تقييم المتعاملين</p>}

                      <h5 className="mb-0">
                        <span className="float-left">
                        <StarRatingComponent className="mb-0" 
                        name="rate2" 
                        editing={false}
                        // renderStarIcon={() => <span className={"icon2-star-full " + (this.state.rating==0 ? 'text-light-grey' : 'text-warning')}></span>}
                        starCount={5}
                        value={this.state.rating}
                      />
                      <span className="float-right" style={{color:'black !important'}}>{this.state.rating}</span>
                        </span>
                     
                          </h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                    <i className="font-3xl icon2-recipt text-light-grey"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p className="mb-2 text-light-grey">Avg. #Tickets</p>
                      <h5 className="mb-0">{this.state.avgTickets} Ticket/Day</h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>

          
        </Row>
        


        <Row>
          <Col sm="8">
          <Card>
              
              <CardBody>
                
                <Row className="mb-3">
                  <Col sm="7">
                  {global.isShowArabicLanguage==false && <h4>Available Services . {this.totalServices}</h4>}
                  {global.isShowArabicLanguage==true && <h4>الخدمات المتاحة . {this.totalServices}</h4>}

                  </Col>
                  <Col sm="5" className="text-right">
                  {this.state.isShowBranchEditPrev==true &&  <button className="btn btn-outline-warning ml-2 btn-sm" onClick={()=>this.branchservicemodal()}><i className="icon2-addition-sign"></i> Add</button>}
                  </Col>
                </Row>
                
                {this.state.allServices.map((service,index) => (
                  <Card className="card-box" key={index}>
                  <CardHeader>
                <h5 className="text-primary float-left">{service.serviceTAB.serviceName}</h5>
                    <div className="card-header-actions float-right">
                    {this.state.isShowBranchEditPrev==true && <div className="text-light-grey ml-3" onClick={()=>this.deleteBranchService(service.serviceTAB.serviceId)}><i className="font-sm font-weight-bold icon2-cancel" ></i></div>}
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm">
                      <tr>
                        {/* <td width="35%">Average Waiting Time</td> */}
                        <td>Counters {service.counters.length}</td>
                      </tr>
                      <tr>
                        {/* <th>0 minutes</th> */}
                        <th>
                        {service.counters.map((count,index) => (
                          <th key={index}>{count}</th>
                        ))}
                        </th>
                       
                        
                      </tr>
                  </Table>
                  </CardBody>
                </Card>
                ))}
                


              </CardBody>
            </Card>
          
            <Card className="mb-0">
              
              <CardBody>
                
                <Row className="mb-3">
                  <Col sm="7">
                      <h4>Agents . {this.totalAgents}</h4>
                  </Col>
                  <Col sm="5" className="text-right">
                  {this.state.isShowBranchEditPrev==true &&  <button className="btn btn-outline-warning ml-2 btn-sm" onClick={()=>this.agentModal('add','data')}><i className="icon2-addition-sign"></i> Add</button>}
                  </Col>
                </Row>
                
                {this.state.allAgents.map((agent,index) => (
                    <Card className="card-box" key={index}>
                    <CardHeader>
                      <div className="float-left mr-3">
                        <span className="badge bg-box bg-gray-200 font-2xl p-4">{agent.agent.serviceCenterEmployee.user.logo}</span>
                      </div>
                      <div className="float-left">
                        <p className="text-warning mb-2">{agent.agent.serviceCenterEmployee.employeeNumber}</p>
                        <h5 className="float-left">{agent.agent.serviceCenterEmployee.user.name}
                        {agent.agent.loginStatus==true && <span  className="text-primary float-right" style={{border:'1px solid green',borderRadius:'5px',height:'8px',width:'8px',background:'green',marginTop:'6px',marginLeft:'5px'}}></span>}
                        {agent.agent.loginStatus==false && <span  className="text-primary float-right" style={{border:'1px solid red',borderRadius:'5px',height:'8px',width:'8px',background:'red',marginTop:'6px',marginLeft:'5px'}}></span>}
                        </h5>
                      </div>
                      
                      <div className="card-header-actions float-right">
                      {agent.assignedBranchCounter!=null && <span className="badge badge-success">Counter {agent.assignedBranchCounter.counterNumber}</span>}
                      {this.state.isShowBranchEditPrev==true &&   <div className="text-light-grey ml-3" onClick={()=>this.agentModal('edit',agent)} ><i className="icon2-edit-pencil"></i></div>}
                        {this.state.isShowBranchEditPrev==true &&  <div className="text-light-grey ml-3" onClick={()=>this.deleteAgent(agent.agent.agentId)}><i className="font-sm font-weight-bold icon2-cancel"></i></div>}
                      </div>
                    </CardHeader>
                    <CardBody>
                    <Table responsive className="m-0 table-borderless table-sm">
                        {/* <tr>
                        {agent.assignedBranchCounter!=null && <td>Counters {agent.assignedBranchCounter.counterNumber}</td>}
                        {agent.assignedBranchCounter==null && <td>Counters </td>}
                      </tr> */}
                      <tr>
                      {global.isShowArabicLanguage==false && <td>Privileges {agent.assignedPrivileges.length}</td>}
                      {global.isShowArabicLanguage==true && <td>الصلاحيات {agent.assignedPrivileges.length}</td>}

                      </tr>
                    </Table>
  
                    {agent.assignedPrivileges.map((priv,index) => (
                      <span  key={index} className="badge badge-tag p-2 mr-2">{priv.privilegeName}</span>
                    ))}
                      {/* <span className="badge badge-tag p-2 mr-2">Fusce sodales quis metus eget diagnissim</span>
                      <span className="badge badge-tag p-2 mr-2">Fusce sodales quis metus eget diagnissim</span>
                      <span className="badge badge-tag p-2">Fusce sodales quis metus eget diagnissim</span>
   */}
                    </CardBody>
                  </Card>
                ))}
                <div className="text-center">
                {this.state.isShowAllAgents==false &&  global.isShowArabicLanguage==false && <div onClick={()=>this.viewAllAgents()}>View All</div>}
                {this.state.isShowAllAgents==false &&  global.isShowArabicLanguage==true && <div onClick={()=>this.viewAllAgents()}>عرض الكل</div>}

                {this.state.isShowAllAgents==true &&  global.isShowArabicLanguage==false && <div onClick={()=>this.viewLessAgents()}>View Less</div>}
                {this.state.isShowAllAgents==true &&  global.isShowArabicLanguage==true && <div onClick={()=>this.viewAllAgents()}>عرض أقل</div>}

              </div>



              </CardBody>
            </Card>
          

          </Col>
          <Col sm="4">
          


          <Card>
              
              <CardBody>
                
                <Row className="mb-3">
                  <Col sm="6">
                      <h4>Counters . {this.totalCounters}</h4>
                  </Col>
                  <Col sm="6" className="text-right">
                  {this.state.isShowBranchEditPrev==true && <button className="btn btn-outline-warning btn-sm" onClick={()=>this.branchcountermodal('add','data')}><i className="icon2-addition-sign"></i> Add</button>}
                  </Col>
                </Row>
                
                {this.state.allCounter.map((counter,index) => (
                  <Card className="card-box" key={index}>
                  <CardHeader>
                    <div className="float-left">
                      <h5 className="text-primary">{counter.branchCounter.counterNumber}</h5>
                    </div>
                    <div className="card-header-actions float-right">
                      <div className="float-left">
                    {this.state.isShowBranchEditPrev==true &&  <div className="text-light-grey" onClick={()=>this.branchcountermodal('edit',counter)} ><i className="icon2-edit-pencil"></i></div>}
                      
                        </div>
                        <div className="float-left ml-4">
                        {this.state.isShowBranchEditPrev==true && <div onClick={()=>this.showMenu(counter.branchCounter.counterId)}><i className="f20 icon2-menu-options text-light-grey"></i></div>}
                          {counter.isShowMenu==true && 
                          <Dropdown isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu:false }); }} >
                                  <DropdownToggle
                                  className="p-0 mb-0 text-light-grey" color="transparent"
                                    tag="span"
                                    onClick={() => {this.setState({ isShowDropDownMenu:false })}}
                                    data-toggle="dropdown"
                                    >
                                  </DropdownToggle>
                                  <DropdownMenu right>
                                 {counter.branchCounter.status==0 && global.isShowArabicLanguage==false && <DropdownItem onClick={()=>this.changeCounterStatus(counter.branchCounter.counterId)}><i className="icon2-switch-off font-xs"></i>Freeze</DropdownItem>}
                                 {counter.branchCounter.status==0 && global.isShowArabicLanguage==true && <DropdownItem onClick={()=>this.changeCounterStatus(counter.branchCounter.counterId)}><i className="icon2-switch-off font-xs"></i>تجميد</DropdownItem>}

                                 {counter.branchCounter.status==1 && global.isShowArabicLanguage==false &&<DropdownItem onClick={()=>this.changeCounterStatus(counter.branchCounter.counterId)}><i className="icon2-switch-off font-xs"></i>UnFreeze</DropdownItem>}
                                 {counter.branchCounter.status==1 && global.isShowArabicLanguage==true &&<DropdownItem onClick={()=>this.changeCounterStatus(counter.branchCounter.counterId)}><i className="icon2-switch-off font-xs"></i>تحرير</DropdownItem>}

                                  {global.isShowArabicLanguage==false &&<DropdownItem onClick={()=>this.deleteCounter(counter.branchCounter.counterId)}><i className="icon2-cancel font-xs"></i>Remove</DropdownItem>}
                                  {global.isShowArabicLanguage==true &&<DropdownItem onClick={()=>this.deleteCounter(counter.branchCounter.counterId)}><i className="icon2-cancel font-xs"></i>إزالة</DropdownItem>}

                                  </DropdownMenu>
                            </Dropdown>}
                            </div>
                    </div>
                  </CardHeader>
                  <CardBody>
                  <Table responsive className="m-0 table-borderless table-sm text-light-grey">
                  <thead>
                  <tr>
                        <td width="35%">Services {counter.noOfCounterServices}</td>
                        <td>Agents {counter.agent}</td>
                    </tr>
                    </thead>
                    
                  </Table>
                  </CardBody>
                </Card>
                ))}
              <div className="text-center">
                {this.state.isShowAllCounter==false && global.isShowArabicLanguage==false && <div onClick={()=>this.viewAllCounters()}>View All</div>}
                {this.state.isShowAllCounter==false && global.isShowArabicLanguage==true && <div onClick={()=>this.viewAllCounters()}>عرض الكل</div>}
                {this.state.isShowAllCounter==true && global.isShowArabicLanguage==true && <div onClick={()=>this.viewAllCounters()}>عرض أقل</div>}

                {this.state.isShowAllCounter==true && global.isShowArabicLanguage==false && <div onClick={()=>this.viewLessCounters()}>View Less</div>}
              </div>

              </CardBody>
            </Card>
          
            <Card>
              
              <CardBody>
                
               
                

                <Row className="mb-3">
                  <Col sm="6 pr-0">
                  {global.isShowArabicLanguage==false && <h4 className="mb-0">Working Hours</h4>}
                  {global.isShowArabicLanguage==true && <h4 className="mb-0">ساعات العمل</h4>}
                {this.state.workingStatus==true && <p className="text-success">Open Now</p>}
                {this.state.workingStatus==false && <p className="text-danger">Closed</p>}

                  </Col>
                  <Col sm="6" className="text-right">
                  {this.state.isShowBranchEditPrev==true &&   <button className="btn btn-outline-warning btn-sm" onClick={()=>this.workinghoursmodal()}><i className="icon2-edit-pencil"></i> Edit</button>}
                  </Col>
                </Row>

                <Table responsive className="m-0 table-borderless table-sm text-light-grey">
                  
                    <tbody>
                    {this.state.allWorkingHours.map((workingHour,index) => (
                      <tr key={index}>
                      <td width="35%">{workingHour.name}</td>
                      <td>{workingHour.startTime} - {workingHour.endTime}</td>
                    </tr>
                    ))}
                      

                      </tbody>
                  
                  </Table>
              </CardBody>
            </Card>
          

          <Card className="mb-0 card-primary">
            
            <CardBody className="text-left">
                <p className="text-white"><i className="icon2-email-envelop text-warning mr-2"></i> {this.state.branchEmail}</p>
                <p className="text-white mb-0"><i className="icon2-phone-call text-warning mr-2"></i> {this.state.branchPhoneNo}</p>
            </CardBody>
          </Card>
          
          </Col>
        </Row>
            
          




        <Modal isOpen={this.state.isEditCloneModal} 
                       className={'modal-lg ' + this.props.className}>
                  {this.modalHeader=='edit' && global.isShowArabicLanguage==false && <ModalHeader >Edit Branch</ModalHeader>}
                  {this.modalHeader=='edit' && global.isShowArabicLanguage==true && <ModalHeader >تحرير الفرع</ModalHeader>}

                  {this.modalHeader=='clone' && global.isShowArabicLanguage==false && <ModalHeader >Clone Branch</ModalHeader>}
                  {this.modalHeader=='clone' && global.isShowArabicLanguage==true && <ModalHeader >فرع استنساخ</ModalHeader>}
                  <ModalBody>
                  <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Branch Name (English)</Label>}
                    {global.isShowArabicLanguage==true &&<Label className="text-light-grey">(اسم الفرع (بالإنجليزية</Label>}
                    {global.isShowArabicLanguage==false &&  <Input type="text" placeholder="Branch Name" 
                    value={this.state.serviceCenterName} onChange={(e) => this.updateServiceCenterName(e.target.value)} />}
                    {global.isShowArabicLanguage==true &&  <Input type="text" placeholder="اسم الفرع" 
                    value={this.state.serviceCenterName} onChange={(e) => this.updateServiceCenterName(e.target.value)} />}
                     < ValidationMessage valid={this.state.serviceCenterNameValid} message={this.state.errorMsg.serviceCenterName} />
                  </FormGroup>
                  <FormGroup className="input-line">
                  {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Branch Name (Arabic)</Label>}
                  {global.isShowArabicLanguage==true &&<Label className="text-light-grey">(اسم الفرع (بالعربية</Label>}
                    {global.isShowArabicLanguage==false &&<Input type="text" placeholder="Branch Name" 
                    value={this.state.serviceCenterArabicName} onChange={(e) => this.updateArabicServiceCenterName(e.target.value)} />}
                  {global.isShowArabicLanguage==true &&<Input type="text" placeholder="اسم الفرع" 
                    value={this.state.serviceCenterArabicName} onChange={(e) => this.updateArabicServiceCenterName(e.target.value)} />}
                     < ValidationMessage valid={this.state.serviceCenterArabicNameValid} message={this.state.errorMsg.serviceCenterArabicName} />
                  </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Email</Label> 
                    <Input type="email" placeholder="Email" autoComplete="username" name="email" id="exampleEmail"
                    value={this.state.email} onChange={(e) => this.updateEmail(e.target.value)}/>
                    < ValidationMessage valid={this.state.emailValid} message={this.state.errorMsg.email} />
                  </FormGroup>

                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Phone</Label> 
                    <Input type="text" placeholder="Phone" autoComplete="username"
                    value={this.state.phone} onChange={(e) => this.updatePhoneNo(e.target.value)} />
                    < ValidationMessage valid={this.state.phoneValid} message={this.state.errorMsg.phone} />
                  </FormGroup>

                  <FormGroup className="input-line">
                    <Label className="text-light-grey" style={{paddingBottom:10}}>Location</Label> 
                    <Label className="" style={{width:"100%",fontSize:15,color:'black',borderBottomWidth:1,borderBottomColor:'#ccc',borderBottomStyle:'solid',paddingBottom: 5,}}>{this.state.branchAddress}</Label>
                    <div style={{height: '300px', width: '100%'}}>
                    <Map
                      google={this.props.google}
                      zoom={10}
                      containerStyle={containerStyle} 
                      style={mapStyles}
                      center={{
                      lat: this.state.lat,
                      lng: this.state.long
                      }}
                      onClick={(t, map, coord)=>{
                        const { latLng } = coord;
                        const lat = latLng.lat();
                        const lng = latLng.lng();
                        // console.log(t, map, coord);
                        console.log(lat);
                        console.log(lng);
                        Geocoder.from(lat, lng).then(json => {
                          var addressComponent = json.results[0].formatted_address;
                          console.log(json);
                          this.setState({branchAddress:addressComponent})
                        }).catch(error => console.warn(error));
                        this.setState({lat:lat,long:lng})
                      }}
                    >
                      <Marker position={{ lat: this.state.lat, lng: this.state.long}} />
                    </Map>
                    </div>
                  </FormGroup>


                  

                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeEditCloneBranchModel()}>Cancel</Button>
                    <Button outline color="primary" onClick={()=>this.resetEditCloneBranch()}>Reset</Button>
                    <Button color="primary" disabled={!this.state.formValid} onClick={()=>this.cloneBranch()} >Save</Button>
                  </ModalFooter>
                </Modal>

      

                <Modal isOpen={this.state.isBranchServiceModal} className={'modal-lg ' + this.props.className}>
                  {global.isShowArabicLanguage==false && <ModalHeader >Add Branch Service</ModalHeader>}
                  {global.isShowArabicLanguage==true && <ModalHeader >إضافة خدمة الفرع</ModalHeader>}

                  <ModalBody>
                  <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false && <Label className="text-light-grey">Service Name</Label>}
                    {global.isShowArabicLanguage==true && <Label className="text-light-grey">اسم الخدمة</Label>}

                    <select className="form-control" onChange={this.changeBranchService} >
                    {global.isShowArabicLanguage==false && <option value='0' key='0'>
                        Services
                      </option>}
                      {global.isShowArabicLanguage==true && <option value='0' key='0'>
                      اختر من القائمة
                      </option>}
                    {this.state.branchServices.map((service,index) => (
                      <option value={service.serviceTAB.serviceId} key={service.serviceTAB.serviceId}>
                        {service.serviceTAB.serviceName}
                      </option>
                    ))}
                    </select>
                  </FormGroup>                  
                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeBranchServiceModel()}>Cancel</Button>
                    <Button outline color="primary" onClick={()=>this.resetBranchService()}>Reset</Button>
                    <Button color="primary"  
                    disabled={this.state.isDisableService}
                    onClick={()=>this.addBranchService()} >Save</Button>
                  </ModalFooter>
                </Modal>


                <Modal isOpen={this.state.showDeleteService} className={'modal-lg ' + this.props.className}>
                  <ModalBody>
                    <Label className="text-light-grey">Do you want to delete service</Label>     
                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>{this.setState({showDeleteService:false})}}>Cancel</Button>
                    <Button color="primary" onClick={()=>this.addBranchService()} >Save</Button>
                  </ModalFooter>
                </Modal>
                
                <Modal isOpen={this.state.isBranchCounterModal} 
                       className={'modal-lg ' + this.props.className}>
                  {this.modalCounterHeader=='add' && global.isShowArabicLanguage==false &&<ModalHeader >Add Branch Counter</ModalHeader>}
                  {this.modalCounterHeader=='add' && global.isShowArabicLanguage==true &&<ModalHeader >إضافة عداد فرع</ModalHeader>}

                  {this.modalCounterHeader=='edit' && global.isShowArabicLanguage==false &&<ModalHeader >Edit Branch Counter</ModalHeader>}
                  {this.modalCounterHeader=='edit' && global.isShowArabicLanguage==true &&<ModalHeader >تحرير عداد الفرع</ModalHeader>}

                  <ModalBody>


                  <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false &&   <Label className="text-light-grey">Counter No.</Label>}
                    {global.isShowArabicLanguage==true &&   <Label className="text-light-grey">رقم الشباك</Label>}

                    {global.isShowArabicLanguage==false &&   
                     <Input type="text" placeholder="Counter No." autoComplete="username" readOnly value={this.state.counterNumber} />}
                    {global.isShowArabicLanguage==true &&   
                     <Input type="text" placeholder="رقم الشباك" autoComplete="username" readOnly value={this.state.counterNumber} />}
                  </FormGroup>
                  <FormGroup className="input-line">
                   {global.isShowArabicLanguage==false && <Label className="text-light-grey">Available Services</Label>} 
                   {global.isShowArabicLanguage==true && <Label className="text-light-grey">الخدمات المتاحة</Label>} 

                    <select className="form-control" onChange={this.changeServiceSector} >
                    {global.isShowArabicLanguage==false && <option value='0' key='0'>
                        Services
                      </option>}
                      {global.isShowArabicLanguage==true && <option value='0' key='0'>
                      اختر من القائمة
                      </option>}
                    {this.state.serviceCounterSector.map((service,index) => (
                      <option value={service.serviceId} key={service.serviceId}>
                        {service.serviceName}
                      </option>
                    ))}
                    </select>
                  </FormGroup>
                  {this.state.selectedBranchCounterServiceObject.map((service,index) => (
                    <Alert color="light" className="theme-alert"  key={index} isOpen={this.state.visible} toggle={()=>this.onDismissAvialableBranchService(service.serviceId)}>
                      {service.serviceName}
                    </Alert>  
                  ))}     
                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeCounterModel()}>Cancel</Button>
                    <Button outline color="primary" onClick={()=>this.resetCounter()}>Reset</Button>
                    <Button color="primary"
                     disabled={this.state.isDisableCounter}
                    onClick={()=>this.addUpdateCounter()} >Save</Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.isWorkingHoursModal} 
                
                className={'modal-lg ' + this.props.className}>
                   {global.isShowArabicLanguage==false && <ModalHeader >Working Hours</ModalHeader>}
                   {global.isShowArabicLanguage==true && <ModalHeader >ساعات العمل</ModalHeader>}
                    <ModalBody>



                    <Row>
                    <Col sm="4">
                      <FormGroup className="input-line">
                      {global.isShowArabicLanguage==false && <Label className="text-light-grey">Week Day</Label>}
                      {global.isShowArabicLanguage==true && <Label className="text-light-grey">يوم الأسبوع</Label>}
                      <select className="form-control" onChange={this.changeWeekDays} >
                      {this.weekDays.map((week,index) => (
                      <option value={week.id} key={week.id}>
                        {week.name}
                      </option>
                    ))}
                      </select>
                    </FormGroup>
                    </Col>
                    <Col sm="6">
                      <FormGroup className="input-line">
                      {global.isShowArabicLanguage==false && <Label className="text-light-grey">Time</Label>}
                      {global.isShowArabicLanguage==true && <Label className="text-light-grey">الوقت</Label>}
                      <Row>
                        <Col sm="5">
                        
                        <TimePickerComponent id="starttimepicker" value={this.state.startTime}  max={this.state.endTime} allowEdit={false}
                        onChange={(time) => this.changeStartTime(time)} />
                        </Col>
                        <Col sm="2">To</Col>
                        <Col sm="5">
                        <TimePickerComponent id="endtimepicker" value={this.state.endTime} min={this.state.startTime} allowEdit={false}
                        onChange={(time) => this.changeEndTime(time)} />
                        </Col>
                      </Row>
                      
                    </FormGroup>
                    </Col>
                    
                    <Col sm="2" className="pt-3">
                      <Button outline color="warning" onClick={()=>this.addWeekDays()}>Add</Button>
                    </Col>
                  </Row>
                  
                  {this.state.weekDaysObject.map((weekDays,index) => (
                    <Alert color="light"  key={index} className="theme-alert" isOpen={this.state.visible} toggle={()=>this.onDismissWeekDays(weekDays.day)}>
                    <Row>
                      <Col sm="6">{weekDays.name}</Col>
                      <Col sm="6">{weekDays.openingTime} - {weekDays.closingTime}</Col>
                    </Row>
                </Alert>
                  ))}
                    {/* <Alert color="light" className="theme-alert" isOpen={this.state.visible} toggle={this.onDismiss}>
                        <Row>
                          <Col sm="6">Monday</Col>
                          <Col sm="6">10:00 AM - 03:00 PM</Col>
                        </Row>
                    </Alert>
            */}
           </ModalBody>
           <ModalFooter>
           <Button outline color="primary" onClick={()=>this.closeWorkingHour()}>Cancel</Button>
             <Button outline color="primary" onClick={()=>this.resetWorkingHour()}>Reset</Button>
             <Button color="primary" 
             disabled={this.state.isDisableWorkingHours}
             onClick={()=>this.updateWorkingHours()}>Save</Button>
           </ModalFooter>
         </Modal>



                
                <Modal isOpen={this.state.isAgentModal}
                       className={'modal-lg ' + this.props.className}>
                  {this.modalAgentHeader=='add' && global.isShowArabicLanguage==false && <ModalHeader>Add Agent</ModalHeader>}
                  {this.modalAgentHeader=='add' && global.isShowArabicLanguage==true && <ModalHeader>إضافة العميل</ModalHeader>}

                  {this.modalAgentHeader=='edit' && global.isShowArabicLanguage==false &&  <ModalHeader>Edit Agent</ModalHeader>}
                  {this.modalAgentHeader=='edit' && global.isShowArabicLanguage==true &&  <ModalHeader>تحرير عامل</ModalHeader>}

                  <ModalBody>


                  <Row className="mt-3">
                  <Col sm="6">
                    <FormGroup className="input-line">
                      {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Agent ID</Label>}
                      {global.isShowArabicLanguage==true &&<Label className="text-light-grey">رمز العميل</Label>}
                      <Input type="text" placeholder="Service Center Name" autoComplete="username" readOnly value={this.state.NextAgentId} />
                      {global.isShowArabicLanguage==false &&<small><span className="text-danger">*</span> Generated by System</small>}
                      {global.isShowArabicLanguage==true &&<small><span className="text-danger">*</span> تم إنشاؤها بواسطة النظام</small>}                    
                    </FormGroup>
                  </Col>
                {this.modalAgentHeader=='add' && <Col sm="6">
                <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false && <Label className="text-light-grey">PIN</Label>}
                    {global.isShowArabicLanguage==true && <Label className="text-light-grey">رقم التعريف الشخصي</Label>}
                    <Input type="text" placeholder="Service Center Name" autoComplete="username" value={this.state.AgentPIN} />
                    {global.isShowArabicLanguage==false && <small ><span className="text-danger">*</span> Changed on first login</small>}
                    {global.isShowArabicLanguage==true && <small ><span className="text-danger">*</span> يستبدل عند أول تسجيل دخول</small>}
                  </FormGroup>
                </Col>}
                {this.modalAgentHeader!='add' && <Col sm="6">
                <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false && <Label className="text-light-grey">PIN</Label>}
                    {global.isShowArabicLanguage==true && <Label className="text-light-grey">رقم التعريف الشخصي</Label>}
                    <Input type="text" placeholder="Service Center Name" autoComplete="username"  value={this.state.AgentPIN} />
                    {global.isShowArabicLanguage==false && <small ><span className="text-danger">*</span> Changed on first login</small>}
                    {global.isShowArabicLanguage==true && <small ><span className="text-danger">*</span> يستبدل عند أول تسجيل دخول</small>}
                  </FormGroup>
                </Col>}
              </Row>
              <FormGroup className="input-line">
                    <Label className="text-light-grey">Assigned Counters</Label>
                    <select className="form-control" onChange={this.changeAssigendCounter} value={this.state.selectedAvialableBranchCounterId}>
                    {this.state.avialableBranchCounter.map((service,index) => (
                      <option value={service.branchCounter.counterId} key={service.branchCounter.counterId}>
                        {service.branchCounter.counterNumber}
                      </option>
                    ))}
                    </select>
                  </FormGroup>
                  {this.modalAgentHeader!='edit' &&<FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label>
                    <Input type="text" placeholder="Agent Name" 
                     value={this.state.agentName} onChange={(e) => this.updateAgentName(e.target.value)} />
                     < ValidationMessage valid={this.state.agentNameValid} message={this.state.errorMsg.agentName} />
               </FormGroup>}
               {this.modalAgentHeader=='edit' &&<FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label>
                    <Input type="text" placeholder="Agent Name"   readOnly
                     value={this.state.agentName}/>
               </FormGroup>}
               {this.modalAgentHeader!='edit' && <FormGroup className="input-line">
                    <Label className="text-light-grey">Email</Label>
                    <Input type="email" placeholder="Email" autoComplete="username" name="email" id="exampleEmail"
                    value={this.state.agentEmail} onChange={(e) => this.updateAgentEmail(e.target.value)}/>
                    < ValidationMessage valid={this.state.agentEmailValid} message={this.state.errorMsg.agentEmail} />
                  </FormGroup>}
                {this.modalAgentHeader=='edit' && <FormGroup className="input-line">
                  <Label className="text-light-grey">Email</Label>
                  <Input type="email" placeholder="Email" autoComplete="username" name="email" id="exampleEmail"
                    value={this.state.agentEmail} onChange={(e) => this.updateAgentEmail(e.target.value)}/>
                    < ValidationMessage valid={this.state.agentEmailValid} message={this.state.errorMsg.agentEmail} />
                  
                </FormGroup>}
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Branch</Label>
                    <Input type="text" autoComplete="username" readOnly value={this.state.branchName} />
                  </FormGroup>
                  
                  {this.modalAgentHeader!='edit' && <div className="mb-2">
                  {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Privilages</Label>}
                  {global.isShowArabicLanguage==true &&<Label className="text-light-grey">الصلاحيات</Label>}
                  {this.state.allPrivileges.map((prev,index) => (
                    <div className="checkbox" key={index}>
                    <input type="checkbox"  checked={prev.isChecked} value={prev.privilegeId} onChange={()=>this.changePriveleges(prev)}></input>
                    <label  data-content={prev.privilegeName}>{prev.privilegeName}</label>
                  </div>
                  ))}
                  </div>}
                  {this.modalAgentHeader=='edit' && <div className="mb-2">
                  {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Privilages</Label>}
                  {global.isShowArabicLanguage==true &&<Label className="text-light-grey">الصلاحيات</Label>}
                  {this.state.allPrivileges.map((prev,index) => (
                    <div className="checkbox" key={index}>
                    <input type="checkbox"  checked={prev.isChecked} value={prev.privilegeId} disabled></input>
                    <label  data-content={prev.privilegeName}>{prev.privilegeName}</label>
                  </div>
                  ))}
                  </div>}
                  



                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeAgentModel()}>Cancel</Button>
                  {this.modalAgentHeader!='edit' && <Button outline color="primary" onClick={()=>this.resetAgent()}>Reset</Button>}
                    <Button color="primary" disabled={!this.state.formAgentValid} onClick={()=>this.addUpdateAgent()}>Save</Button>
                  </ModalFooter>
                </Modal>


      </div>
    );
  }
}
// export default Breadcrumbs
export default GoogleApiWrapper({
  apiKey: 'AIzaSyDLN9J4HC_D04AFVx1ZK5n0MQjJaO6YUko'
})(Breadcrumbs);
