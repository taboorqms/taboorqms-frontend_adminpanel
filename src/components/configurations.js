import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Col, Row, Table,  
  DropdownItem,  DropdownMenu,  DropdownToggle,Button,ButtonDropdown, FormGroup,Label,Input,Alert,
  Modal, ModalBody, ModalFooter, ModalHeader, } from 'reactstrap';
  

import { Link, NavLink } from 'react-router-dom';
import * as router from 'react-router-dom';
import { Badge, UncontrolledDropdown, Nav, NavItem } from 'reactstrap';
import {
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler, AppSwitch } from '@coreui/react';
// import routes from '../../../routes';

import axios from "axios";
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import routes from './routes';
import LoadingBar from 'react-top-loading-bar'
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
class Branch extends Component {
  tempItems=[
    {
      'name':'In App Notifications Allowed',checked:false,id:1
    },
    {
      'name':'SMS Allowed',checked:false,id:2
    },
    {
      'name':'Email Allowed',checked:false,id:3
    }
  ]
  totalUsers=0
  constructor(props) {
    super(props);
    this.state = {
      progress:0,
      isDisableSave:false,
      isShowConfigPrev:false,
      items:[
        {
          'name':'In App Notifications Allowed',checked:false,id:1
        },
        {
          'name':'SMS Allowed',checked:false,id:2
        },
        {
          'name':'Email Allowed',checked:false,id:3
        }
      ],
      modal: false,
      large: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
    };
    this.toggleLarge = this.toggleLarge.bind(this);
  
  }
  componentDidMount()
  {
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Admin_Panel_View_Only')
        {
          this.setState({isShowConfigPrev:true})
          isPrev=true
        }
        else {
          if(!isPrev==true)
          {
            isPrev=false
          }
         
        }
      }
    }
    if(isPrev==false)
    {
      toaster.notify("User don't have privilege", {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
    else{
      this.getUserConfiguration()
    }
  }
  getUserConfiguration()
  {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/setting/configuration/get/user', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.tempItems[0].checked=responseJson.userConfiguration.inAppNotificationAllowed
        this.tempItems[1].checked=responseJson.userConfiguration.smsAllowed
        this.tempItems[2].checked=responseJson.userConfiguration.emailAllowed
        this.totalUsers=3
        this.setState({
          items:this.tempItems
        })
      }
    })
  }
  changeConfigration(id)
  {
    for(var i=0;i<this.tempItems.length;i++)
    {
      if(id==this.tempItems[i].id)
      {
        if(this.tempItems[i]['checked']==true)
        {
          this.tempItems[i]['checked']=false

        }
        else{
          this.tempItems[i]['checked']=true
        }
      }
    }
    this.setState({
      items:this.tempItems
    })
  }
  resetConfigration()
  {
    for(var i=0;i<this.tempItems.length;i++)
    {
      this.tempItems[i]['checked']=false
    }
    this.setState({
      items:this.tempItems
    })
    this.updateConfigration()
  }
  updateConfigration()
  {
    this.setState({
      isDisableSave:true,progress:50
    })
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/setting/configuration/update', {
      method: 'POST',
      body:JSON.stringify({
        "emailAllowed": this.state.items[2].checked,
        "inAppNotificationAllowed": this.state.items[0].checked,
        "smsAllowed": this.state.items[1].checked

      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        this.setState({
          isDisableSave:false,progress:100
        })
        this.getUserConfiguration()
      }
    })
  }
  toggleLarge() {
    this.setState({
      large: !this.state.large,
    });
  }
  searchUser(userNo)
  {
    
    if(userNo!='')
    {
      var isMatch=false
      var data=[]
      for(var i=0;i<this.tempItems.length;i++)
      {
        if(this.tempItems[i].name.toLowerCase().includes(userNo.toLowerCase()))
        {
          data.push(this.tempItems[i])
          this.setState({
            items:data
          })
          isMatch=true
          this.totalUsers=data.length
        }
        else{
          if(isMatch!=true)
          {
            isMatch=false
          }
        }
      }
      if(isMatch==false)
      {
        this.totalUsers=0
        this.setState({items:[]})
      }
    }
    else
    {
      this.setState({
        items:this.tempItems
      })
    }
  
  }
  changeUserSearch(userNo)
  {
    if(userNo=='')
    { 
      this.totalUsers=this.tempItems.length
      this.setState({
        items:this.tempItems
      })
    }
    this.searchUser(userNo)
    this.setState({
    searchUserNo:userNo
    })
  }
  render() {
    return (
      <div className="animated fadeIn">
         <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress:0
          })}
        />
        <DefaultHeader/>
        <Row className="mb-3 hidden">
          
          <Col sm="4">
            <h3 className="section-title">
            <ButtonDropdown className="mr-3" id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                &nbsp;
                </DropdownToggle>
                <DropdownMenu left>
                  <DropdownItem>Action</DropdownItem>
                  <DropdownItem>Another action</DropdownItem>
                  <DropdownItem>Something else here</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              {this.totalUsers}&nbsp;Configurations
              </h3>
          </Col>

          {this.state.isShowConfigPrev==true && <Col sm="8" className="text-right">
          
            
            <div className="input-group float-right w-auto ml-2">
                <input id="input2-group1" name="input2-group1" placeholder="Search" type="text" className="form-control b-r-0 bg-transparent"
                value={this.state.searchUserNo} onChange={(e) => this.changeUserSearch(e.target.value)}
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.searchUser()
                  }
                }}
                >
                </input>
              <div className="input-group-append" ><span className="input-group-text bg-transparent b-l-0"><i className="icon2-magnifying-glass"></i></span></div>
            </div>
            <div className="float-right">
              <button className="btn btn-outline-warning" disabled={this.state.isDisableSave} onClick={()=>this.resetConfigration()}>Reset to Default</button>
              <button className="btn btn-warning ml-2" disabled={this.state.isDisableSave} onClick={()=>this.updateConfigration()}>Save</button>
            </div>
            <div className="clearfix"></div>

          </Col>}
        </Row>



        {this.state.isShowConfigPrev==true && <Card className="mb-0">
              
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover table-tbody">
                  {/* <thead>
                  <tr>
                    <th className="text-left">Config Item</th>
                    <th className="text-right">Status</th>
                  </tr>
                  </thead> */}
                  <tbody>
                 
                  {this.state.items.map(item => (
                    <tr>
                      <td className={"text-left " + (item.checked ? '' : 'text-light-grey')}>{item.name}</td>
                      {/* <td className="text-left">{item.name}</td> */}
                      <td className="text-right" width="20%">
                        {/* <div className="input-line">
                        <input type="number" className="form-control"></input>
                        </div> */}
                      <AppSwitch className={'mx-1'} variant={'3d'} color={'success'} checked={item.checked} onChange={()=>this.changeConfigration(item.id)} />
                      </td>
                    </tr>
                  ))}       
                   </tbody>
                </Table>
               
              </CardBody>
            </Card>}
          
          


            <Modal isOpen={this.state.large} toggle={this.toggleLarge}
                       className={'modal-lg ' + this.props.className}>
                  <ModalHeader toggle={this.toggleLarge}>Add Role</ModalHeader>
                  <ModalBody>


                  


                    

                  
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label> 
                    <Input type="text" placeholder="Name" />
                  </FormGroup>
                  


                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Privileges</Label>
                    <select className="form-control">
                      <option>option 1</option>
                      <option>option 1</option>
                    </select>
                  </FormGroup>
                  


                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={this.toggleLarge}>Cancel</Button>
                    <Button outline color="primary" onClick={this.toggleLarge}>Reset</Button>
                    <Button color="primary" onClick={this.toggleLarge}>Add</Button>
                  </ModalFooter>
                </Modal>

      
      


      </div>
    );
  }
}

export default Branch;
