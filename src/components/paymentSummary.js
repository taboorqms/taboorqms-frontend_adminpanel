import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { Button, Card, CardBody, CardGroup, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, 
  Row, Table, Label, FormGroup, Nav, NavItem, NavLink, TabContent, TabPane,ButtonDropdown, DropdownItem,  DropdownMenu,  DropdownToggle, Alert } from 'reactstrap';
import { AppNavbarBrand } from '@coreui/react';
import logo from '../assets/img/brand/logo.png';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';
import Steps, { Step } from 'rc-steps';
import { AppSwitch } from '@coreui/react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Route, Redirect } from 'react-router-dom';
import toaster from "toasted-notes";
import MaskedInput from 'react-maskedinput';
import "toasted-notes/src/styles.css";
var formData=new FormData()
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
class Register extends Component {
  userServiceCenterId=0
  serviceCenterSubId=0
  tempPayment=[]
  paymentType=[{
    'name':'Annual',id:1
  },{
    'name':'Montly',id:2
  }]
  constructor(props){
    super(props);
    console.log(this.props);
    this.state={
      isError:false,
      serviceCenterName: '',serviceCenterNameValid: false,
      serviceCenterArabicName: '',serviceCenterArabicNameValid: false,
      selectedServiceSectorId:0,
      selectedPlanId:0,
      paymentAmount:0,
      isSuccess:false,
      selectedPlanObject:{},
      selectedServiceObject:{},
      selectedPlanBool:false,
      email: this.props.location.state.email, emailValid: false,
      password: '', passwordValid: false,
      country: '', countryValid: false,
      phone: '', phoneValid: false,
      cardValid:false,cardNo:'',
      cvvNoValid:false,cvvNo:'',
      dateValue:new Date(),
      cardNameValid:false,cardName:'',
      passwordConfirm: '', passwordConfirmValid: false,
      formValid: false,
      paymentTypeId:0,
      paymentFormValid: false,
      errorMsg: {},
      paymentErrorMsg: {},
      serviceSector:[],
      paymentPlanList:[],
      next:0,
      image: null
    }
    this.getServiceCenterId()
      }
  setStartDate(date)
  {
     this.setState({dateValue:date})
  }
  validateForm = () => {
    // const {serviceCenterNameValid, emailValid, passwordValid, passwordConfirmValid,countryValid,phoneValid} = this.state;
    // this.setState({
    //   formValid: serviceCenterNameValid && emailValid && passwordValid && passwordConfirmValid && phoneValid && countryValid
    // })
    const {serviceCenterNameValid, emailValid,countryValid,phoneValid,serviceCenterArabicNameValid,passwordValid,passwordConfirmValid} = this.state;
    this.setState({
      formValid: serviceCenterNameValid && emailValid && phoneValid && countryValid && serviceCenterArabicNameValid && passwordValid && passwordConfirmValid
    })
  }

  validatePaymentForm = () => {
    const {cardNo,cvvNo,cardName} = this.state;
    this.setState({
      paymentFormValid: cardNo  && cvvNo && cardName
    })
  }
  updateServiceCenterName = (serviceCenterName) => {
    this.setState({serviceCenterName}, this.validateServiceCenterName)
  }

  validateServiceCenterName = () => {
    const {serviceCenterName} = this.state;
    let serviceCenterNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (serviceCenterName.length == 0 ||serviceCenterName=='' ) {
      serviceCenterNameValid = false;
      errorMsg.serviceCenterName = 'Required'
    }
    else if(!/^[a-zA-Z\s]*$/.test(serviceCenterName))
    {
      serviceCenterNameValid = false;
      errorMsg.serviceCenterName = 'Only alphabets are allowed'
    }
    this.setState({serviceCenterNameValid, errorMsg}, this.validateForm)
  }
  updateArabicServiceCenterName = (serviceCenterArabicName) => {
    this.setState({serviceCenterArabicName}, this.validateArabicServiceCenterName)
  }

  validateArabicServiceCenterName = () => {
    const {serviceCenterArabicName} = this.state;
    let serviceCenterArabicNameValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (serviceCenterArabicName.length == 0 ||serviceCenterArabicName=='' ) {
      serviceCenterArabicNameValid = false;
      errorMsg.serviceCenterArabicName = 'Required'
    }
    else if(!/^[\u0621-\u064A\040]+$/.test(serviceCenterArabicName))
    {
      serviceCenterArabicNameValid = false;
      errorMsg.serviceCenterArabicName = 'Only arabic alphabets are allowed'
    }
    this.setState({serviceCenterArabicNameValid, errorMsg}, this.validateForm)
  }
  updateCardName = (cardName) => {
    this.setState({cardName}, this.validateCardName)
  }

  validateCardName = () => {
    const {cardName} = this.state;
    let cardNameValid = true;
    let paymentErrorMsg = {...this.state.paymentErrorMsg}

    if (cardName.length == 0 ||cardName=='' ) {
      cardNameValid = false;
      paymentErrorMsg.cardName = 'Required'
    }
    else if(!/^[a-zA-Z\s]*$/.test(cardName))
    {
      cardNameValid = false;
      paymentErrorMsg.cardName = 'Only alphabets are allowed'
    }
    this.setState({cardNameValid, paymentErrorMsg}, this.validatePaymentForm)
  }
  updateCountryName = (country) => {
    this.setState({country}, this.validateCountryName)
  }

  validateCountryName = () => {
    const {country} = this.state;
    let countryValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (country.length == 0 ||country=='') {
      countryValid = false;
      errorMsg.country = 'Required'
    }
    else if(!/^[a-zA-Z\s]*$/.test(country))
    {
      countryValid = false;
      errorMsg.country = 'Only alphabets are allowed'
    }
    this.setState({countryValid, errorMsg}, this.validateForm)
  }
  updatePhoneNo = (phone) => {
    this.setState({phone}, this.validatePhoneNo)
  }
  validatePhoneNo = () => {
    const {phone} = this.state;
    let phoneValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (phone.length == 0 || phone=='') {
      phoneValid = false;
      errorMsg.phone = 'Required'
    }
     else if (phone.length <8) {
      phoneValid = false;
      errorMsg.phone = 'Phone number must be 8 characters'
    }
    else if (!/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/.test(phone)){
      phoneValid = false;
      errorMsg.phone = 'Invalid phone format'
    }
    this.setState({phoneValid, errorMsg}, this.validateForm)
  }
  updateCardNo = (cardNo) => {
    this.setState({cardNo}, this.validateCardNo)
  }
  validateCardNo = () => {
    const {cardNo} = this.state;
    let cardValid = true;
    let paymentErrorMsg = {...this.state.paymentErrorMsg}

    if (cardNo.length == 0 || cardNo=='') {
      cardValid = false;
      paymentErrorMsg.cardNo = 'Required'
    }
     else if (cardNo.length <16) {
      cardValid = false;
      paymentErrorMsg.cardNo = 'Card number must be 16 characters'
    }
    else if (!/[0-9]/.test(cardNo)){
      cardValid = false;
      paymentErrorMsg.cardNo = 'Must be a number'
    }
    this.setState({cardValid, paymentErrorMsg}, this.validatePaymentForm)
  }
  updateCvvCardNo = (cvvNo) => {
    this.setState({cvvNo}, this.validateCvvCardNo)
  }
  validateCvvCardNo = () => {
    const {cvvNo} = this.state;
    let cvvNoValid = true;
    let paymentErrorMsg = {...this.state.paymentErrorMsg}

    if (cvvNo.length == 0 || cvvNo=='') {
      cvvNoValid = false;
      paymentErrorMsg.cvvNo = 'Required'
    }
     else if (cvvNo.length < 3) {
      cvvNoValid = false;
      paymentErrorMsg.cvvNo = 'CVV number must be 3 characters'
    }
    else if (!/[0-9]/.test(cvvNo)){
      cvvNoValid = false;
      paymentErrorMsg.cvvNo = 'Must be a number'
    }
    this.setState({cvvNoValid, paymentErrorMsg}, this.validatePaymentForm)
  }
  updateEmail = (email) => {
    this.setState({email}, this.validateEmail)
  }
  validateEmail = () => {
    const {email} = this.state;
    let emailValid = true;
    let errorMsg = {...this.state.errorMsg}

    // checks for format _@_._
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){
      emailValid = false;
      errorMsg.email = 'Invalid email format'
    }

    this.setState({emailValid, errorMsg}, this.validateForm)
  }
  updatePassword = (password) => {
    this.setState({password}, this.validatePassword);
  }
  validatePassword = () => {
    const {password} = this.state;
    let passwordValid = true;
    let errorMsg = {...this.state.errorMsg}

    // must be 6 chars
    // must contain a number
    // must contain a special character

    if (password.length < 6) {
      passwordValid = false;
      errorMsg.password = 'Password must be at least 6 characters long';
    } else if (!/\d/.test(password)){
      passwordValid = false;
      errorMsg.password = 'Password must contain a digit';
    } else if (!/[!@#$%^&*]/.test(password)){
      passwordValid = false;
      errorMsg.password = 'Password must contain special character: !@#$%^&*';
    }

    this.setState({passwordValid, errorMsg}, this.validateForm);
  }

  updatePasswordConfirm = (passwordConfirm) => {
    this.setState({passwordConfirm}, this.validatePasswordConfirm)
  }
  changePaymentType=e=>
  {
     this.setState({paymentTypeId:parseInt(e.target.value)})
     if(parseInt(e.target.value)==1)
     {
       this.setState({paymentAmount:this.state.selectedPlanObject.priceYearly})
     }
     else if(parseInt(e.target.value)==2)
     {
      this.setState({paymentAmount:this.state.selectedPlanObject.totalPriceMonthly})
     }
  }
  validatePasswordConfirm = () => {
    const {passwordConfirm, password} = this.state;
    let passwordConfirmValid = true;
    let errorMsg = {...this.state.errorMsg}

    if (password !== passwordConfirm) {
      passwordConfirmValid = false;
      errorMsg.passwordConfirm = 'Passwords do not match'
    }

    this.setState({passwordConfirmValid, errorMsg}, this.validateForm);
  }
  // nextStep=(next)=>{
  //   if(this.state.formValid)
  //   {
  //     if(next==0)
  //     {
  //       formData=new FormData()
  //     }
  //     if(next==1)
  //     {
  //       this.getAllPaymentPlan()
  //       this.setState({next:next});
  //     }
  //     if(next==2)
  //     {
  //       this.registerServiceCenter()
  //     }
     
      
  //   }
  //   if(this.state.paymentFormValid)
  //   {
  //     if(next==3)
  //     {
  //       this.addPayment()
  //     }
  //   }
    
  // }
  // addPayment()
  // {
  //   var expiryMonth=this.state.dateValue.getMonth()+1
  //   expiryMonth=expiryMonth<10 ?'0' + expiryMonth : expiryMonth;
  //   var expiryYear=this.state.dateValue.getFullYear()
  //   var expiryDate=expiryMonth+'/'+expiryYear
  //   fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/bankCard/add', {
  //     method: 'POST',
  //     body:JSON.stringify({
  //       "cardCvv": this.state.cvvNo,
  //       "cardNumber": this.state.cardNo,
  //       "cardTitle": this.state.cardName,
  //       "cardType": "Debit",
  //       "expiryDate": expiryDate,    
  //       "paymentMethodId": 2,
  //       "serviceCenterId": this.userServiceCenterId

  //     }),
  //     headers: {
  //       'Content-Type': 'application/json'
  //     },
  //   })
  //   .then(response => response.json())
  //   .then(responsejson => {
  //     if(responsejson.applicationStatusCode==0)
  //     {
  //       fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/subscription/pay', {
  //         method: 'POST',
  //         body:JSON.stringify({
          
  //           "paymentType": this.state.paymentTypeId,
  //           "serviceCenterId": this.userServiceCenterId
        
  //         }),
  //         headers: {
  //           'Content-Type': 'application/json'
  //         },
  //       })
  //       .then(response => response.json())
  //       .then(responsejson => {
  //         if(responsejson.applicationStatusCode==0)
  //         {

  //           this.setState({next:4,isSuccess:true})
           
  //         }
  //       })
       
  //     }
  //     else{
  //       toaster.notify(responsejson.devMessage, {
  //         position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
  //         duration: null // This notification will not automatically close
  //       });
  //     }
     
  //   })
  // }
  changeServiceSector= e =>{
    for(var i=0;i<this.state.serviceSector.length;i++)
    {
      if(this.state.serviceSector[i].serviceSectorId==parseInt(e.target.value))
      {
        this.setState({selectedServiceObject:this.state.serviceSector[i]})
      }
    }
    this.setState({selectedServiceSectorId:parseInt(e.target.value)})
  }
  componentDidMount() {
    formData=new FormData()
    this.getAllServiceSector()

 }
 getAllServiceSector()
 {
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceSector/get/all', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      this.setState({selectedServiceObject:responseJson.serviceSectors[0],serviceSector:responseJson.serviceSectors,selectedServiceSectorId:responseJson.serviceSectors[0].serviceSectorId})
    }
  })
 }
 getAllPaymentPlan()
 {
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/paymentPlan/get/all', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      for(var i=0;i<responseJson.paymentPlanList.length;i++)
      {
        responseJson.paymentPlanList[i]['isSelected']=false
      }
      this.tempPayment=responseJson.paymentPlanList
      this.setState({paymentPlanList:responseJson.paymentPlanList})
    }
  })
 }
 registerServiceCenter()
 {
  formData.append('RegisterServiceCenterPayload',JSON.stringify({
    "arabicName": this.state.serviceCenterArabicName,
    "country": this.state.country,
    "email": this.state.email,
    'name':this.state.serviceCenterName,
    "password": this.state.password,
    "paymentPlanId": this.state.selectedPlanId,
    "phoneNumber": this.state.phone,
    "serviceSector": {
        "serviceSectorId": this.state.selectedServiceSectorId,
        "serviceSectorName": this.state.selectedServiceObject.serviceSectorName
    }
  }))
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/register', {
    method: 'POST',
    body:formData,
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      this.setState({next:2});
      this.userServiceCenterId=parseInt(responseJson.values.ServiceCenterId)
      fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getPaymentPlan', {
        method: 'POST',
        body:JSON.stringify({
          'id':parseInt(responseJson.values.ServiceCenterId)
        }),
        headers: {
          'Content-Type': 'application/json'
        },
      })
      .then(response => response.json())
      .then(responsejson => {
        if(responsejson.applicationStatusCode==0)
        {
          this.setState({selectedPlanObject:responsejson.paymentPlanList[0],paymentAmount:responsejson.paymentPlanList[0].priceYearly});
         
        }
        else{
          toaster.notify(responsejson.devMessage, {
            position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
            duration: null // This notification will not automatically close
          });
        }
      })
    }
    else{
      toaster.notify(responseJson.devMessage, {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration: null // This notification will not automatically close
      });
    }
   
  })
 }
 onImageChange = event => {
  if (event.target.files && event.target.files[0]) {
    let img = event.target.files[0];
    formData.append("ServiceCenterProfileImage", event.target.files[0]);
    this.setState({
      image: URL.createObjectURL(img)
    });
  }
}
resetImage()
{
  this.setState({
    image: ''
  });
}
selectedPlan=(data)=>{
 
  
  for(var i=0;i<this.tempPayment.length;i++)
  {
    if(this.tempPayment[i].paymentPlanId==data.paymentPlanId)
    {
      if(this.tempPayment[i]['isSelected']==false)
      {}
      this.tempPayment[i]['isSelected']=true
      
    }
    else{
      this.tempPayment[i]['isSelected']=false
    }

  }
  this.setState({selectedPlanId:data.paymentPlanId,selectedPlanBool:true,paymentPlanList:this.tempPayment})
}

paymentTypeVal(id)
{
  if(id=='48')
  {
    this.setState({
      paymentTypeId:48,
      isVisa:true,
      isMaster:false,
      isPaypal:false
    })
  }
  else if(id=='47')
  {
    this.setState({
      paymentTypeId:47,
      isVisa:false,
      isMaster:true,
      isPaypal:false
    })
  }
  else if(id=='46')
  {
    this.setState({
      paymentTypeId:46,
      isVisa:false,
      isMaster:false,
      isPaypal:true
    })
  }
  
}

changeDate = (e) => {
  this.setState({selectedDate: e.target.value})
} 
getServiceCenterId(){
  fetch('https://apicall.taboor.ae/taboor-qms/dbconnector/tab/servicecenters/get/email?emailId='+this.state.email, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  }).then(response => response.json())
  .then(responseJson => {
    this.userServiceCenterId=parseInt(responseJson.serviceCenterId)
    console.log("hers the rsponse"+responseJson +this.userServiceCenterId)

    this.getServiceCenterSubscriptionId(this.userServiceCenterId)
  }).catch(
    error=>{
      console.log(error)
    }
  )
}
getServiceCenterSubscriptionId(serviceCenterId){
  fetch('https://apicall.taboor.ae/taboor-qms/dbconnector/tab/servicecentersubscriptions/get/serviceCenterId?serviceCenterId='+serviceCenterId, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  }).then(response => response.json())
  .then(responseJson => {
    this.serviceCenterSubId=parseInt(responseJson.subscriptionId)
    console.log("heres the response"+responseJson +this.serviceCenterSubId)
  }).catch(
    error=>{
      console.log(error)
    }
  )
}
addPayment()
  {
    this.setState({
      paymentTypeId:47,
      paymentFormValid:false,
      progress:50
    })
    
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/bankCard/add', {
      method: 'POST',
      body:JSON.stringify({
        "cardCvv": this.state.cvvNo,
        "cardNumber": this.state.cardNo,
        "cardTitle": this.state.cardName,
        "cardType": "Debit",
        "expiryDate": this.state.selectedDate,   
        // "expiryMonth": this.state.selectedDate[0]+this.state.selectedDate[1],
        // "expiryYear": '20'+this.state.selectedDate[3]+this.state.selectedDate[4], 
        "paymentMethodId": this.state.paymentTypeId,
        "serviceCenterId": this.userServiceCenterId

      }),
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(response => response.json())
    .then(responsejson => {
      if(responsejson.applicationStatusCode==0)
      {
        console.log(this.state.paymentTypeId)
        console.log(responsejson)
        fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/subscription/pay', {
          method: 'POST',
          body:JSON.stringify({
          
            "bankCardId": parseInt(responsejson.values.UserBankCardId),
            "serviceCenterSubscriptionId": this.serviceCenterSubId
        
          }),
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(response => response.json())
        .then(responsejson => {
          if(responsejson.applicationStatusCode==0)
          {
            console.log(responsejson)
            this.setState({
              paymentFormValid:true,progress:100
            })
            this.resetRegForm()
            this.resetPayForm()
            this.setState({next:6,isSuccess:true})
            this.props.history.push('/login');

          }
          else{
            toaster.notify(responsejson.applicationStatusResponse, {
              duration: 5000 
            });
          }
        })
       
      }
      else{
        this.setState({
          paymentFormValid:true,progress:100
        })
        toaster.notify(responsejson.devMessage, {
          duration:5000 // This notification will not automatically close
        });
      }
     
    })
  }
  resetRegForm()
  {
    this.setState({
      serviceCenterArabicName:'',
      serviceCenterName:'',
      email:'',
      phone:"",
      password:'',passwordConfirm:'',
      image:'',
      country:'',
      formValid:false,
    })
  }
  resetPayForm()
  {
    this.setState({
      cardNo:'',
      cvvNo:'',
      cardName:'',
      paymentFormValid:false,
    })
  }
  render() {
    return (
    <Container fluid className="p-0">
          <Row>
            <Col sm="12">
              <Card className="register-card m-0">
                <CardBody className="p-5"> <Form>
                  <Row className="mb-3">
                  <Col sm="12" style={{paddingBottom: 20,}}><i onClick={()=>{this.backalert()}} className="icon-arrow-left" style={{fontSize:20,fontWeight:'1000',color:'#3358dd',cursor:'pointer'}}></i></Col>
                    <Col sm="6"> <h2 className="text-primary">Registration</h2> </Col>
                    <Col sm="6" className="text-right">  
                      <h6>Already have an account. <Link to="/login">Login Here</Link></h6> 
                    </Col>
                  </Row> 

                  <h4 className="text-dark mb-5">Payments Method</h4>
                  
                  <Row>
                      
                    <Col sm="6">

                      <div className="payment-cards mb-5">
                        <ul>
                          <li className={(this.state.isVisa?"active":'')}><a title="Visa" onClick={()=>this.paymentTypeVal('48')} >
                            <img src={require('../assets/img/visa.png')}/>
                            {/* <AppNavbarBrand className="img-fluid"  full={{ src: master, width: 20, alt: 'Taboor Logo' }} /> */}
                          </a></li>
                          <li className={(this.state.isMaster?"active":'')}><a title="Master Card" onClick={()=>this.paymentTypeVal('47')} >
                          <img src={require('../assets/img/master.png')}/>
                            </a></li>
                          <li className={(this.state.isPaypal?"active":'')}><a title="PayPal" onClick={()=>this.paymentTypeVal('46')} >
                          <img src={require('../assets/img/paypal.png')}/>
                            </a></li>
                        </ul>
                      </div>
                      
                    <FormGroup className="input-line">
                      <Label className="text-light-grey">Card Number</Label>
                      <Input type="text" placeholder="XXXX XXXX XXXX XXXX"  
                      value={this.state.cardNo} onChange={(e) => this.updateCardNo(e.target.value)}/>
                      <ValidationMessage valid={this.state.cardValid} message={this.state.paymentErrorMsg.cardNo} />
                    </FormGroup>
                    <Row>
                      <Col sm="6">
                        <FormGroup className="input-line">
                        <Label className="text-light-grey d-block">Expiry Date</Label>
                        <MaskedInput className="form-control" mask="11/11" name="expiry" placeholder="MM/YY"  style={{backgroundColor:'transparent', width:'100%',paddingTop:6,paddingBottom:6,borderBottomStyle:'solid', borderBottomWidth:1,borderBottomColor:'#e4e7ea',color: '#5c6873'}}
                        value={this.state.selectedDate}
                        onChange={this.changeDate}/>
                    </FormGroup>
                      </Col>
                      <Col sm="6">
                        <FormGroup className="input-line">
                        <Label className="text-light-grey">CVV</Label>
                        <Input type="text" placeholder="CVV" 
                        value={this.state.cvvNo} onChange={(e) => this.updateCvvCardNo(e.target.value)}/>
                        <ValidationMessage valid={this.state.cvvNoValid} message={this.state.paymentErrorMsg.cvvNo} />
                      </FormGroup>
                      </Col>
                    </Row>
                    <FormGroup className="input-line">
                      <Label className="text-light-grey">Cardholder's Name</Label>
                      <Input type="text" placeholder="Name"   
                      value={this.state.cardName} onChange={(e) => this.updateCardName(e.target.value)}/>
                      <ValidationMessage valid={this.state.cardNameValid} message={this.state.paymentErrorMsg.cardName} />
                      
                    </FormGroup>
                      <br></br>
                  <p className="text-light-grey">I confirm I have read and accepted the <Link>Terms and Conditions of Use.</Link> By clicking on "<span className="text-warning">Pay</span>" you agree to immediately access the service and to waive any right of withdrawal. You may terminate your subscription at any time by contact Taboor Service Center. The termination will be applied at the end of the current subscription period.</p>


                    </Col>
                    <Col sm="6"> 

                    {/* <Card className="ml-5 card-line">
                        <CardBody>
                        <div className="plan-info">
                          <h4 className="text-center mb-3 font-weight-normal">Order Details</h4>
                          <Row>
                            <Col sm="7" className="text-left"><h5 className="font-lg font-weight-bold text-dark">{this.planDetailObject.paymentPlan.planName}</h5></Col>
                              <Col sm="5" className="text-right"><h3 className="text-dark mb-0">{this.amount} {this.planDetailObject.paymentPlan.prices[0].currency}</h3><p className="text-light-grey">per month</p></Col>
                            </Row>
                            <div className="text-light-grey text-left">
                              <p>{this.state.branches} Branches</p>
                              <p>{this.planDetailObject.paymentPlan.noOfUserPerBranch} Users/Branch</p>
                              <p>{this.planDetailObject.paymentPlan.supportType} Support</p>
                            </div>
                            <hr></hr>
                            <Row>
                            <Col sm="5" className="text-left"><h4 className="mb-0 text-dark">Tax (5%)</h4><p className="text-light-grey">per month</p></Col>
                              <Col sm="7" className="text-right pl-lg-0"><h3 className="font-3xl text-dark">{this.state.tax} {this.planDetailObject.paymentPlan.prices[0].currency} </h3></Col>
                            </Row>
                            <hr></hr>
                            <Row>
                              <Col sm="5" className="text-left"><h3 className="text-primary">Total</h3></Col>
                              <Col sm="7" className="text-right pl-lg-0"><h3 className="text-primary mb-0">{this.state.totalAmount} {this.planDetailObject.paymentPlan.prices[0].currency}</h3><p className="mb-0">per month</p></Col>
                            </Row>
                        </div>
                        </CardBody>
                      </Card> */}

                      
                    
                      </Col>
                  </Row> 

                  
                </Form> </CardBody>
                <CardFooter className="p-5 text-right b-a-0 pt-0">
                  <Button color="primary" className="ml-2" disabled={!this.state.paymentFormValid} onClick={()=>{this.addPayment()}}>Pay</Button>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
    );
  }
}

export default Register;

