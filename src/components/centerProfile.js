import React, { Component } from 'react';
import {
  Dropdown, Card, CardBody, CardHeader, Col, Row, Table, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle, Alert, Button, FormGroup, Label, Input, InputGroup, InputGroupText, InputGroupAddon,
  Modal, ModalBody, ModalFooter, ModalHeader, TabContent, TabPane
} from 'reactstrap';
import { Line } from 'react-chartjs-2';
import { AppSwitch } from '@coreui/react';
import { Link, NavLink } from 'react-router-dom';
import { Badge, Nav, NavItem } from 'reactstrap';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import axios from "axios";
import moment from 'moment';
import MaskedInput from 'react-maskedinput'
import LoadingBar from 'react-top-loading-bar'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
function ValidationMessage(props) {
  if (!props.valid) {
    return (
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
var canvas = document.createElement('canvas');
const ctx = canvas.getContext("2d")
const gradient = ctx.createLinearGradient(0, 0, 250, 0);
gradient.addColorStop(0, 'orange');
gradient.addColorStop(0.5, 'red');
gradient.addColorStop(1, 'green');
ctx.fillStyle = gradient;
ctx.fillRect(20, 20, 150, 100);
class Breadcrumbs extends Component {
  prev = []
  tempFastPass = ''
  configId = 0
  serviceCenterId = 0
  serviceCenter = {}
  tempTaboorAmount = ''
  tempFastPassCount = ''
  tempFastPassTime = ''
  tempNormalPassCount = ''
  tempNormalPassTime = ''
  countries = [
    {
      "code": "+1",
      "name": "Canada"
    },
    {
      "code": "+1",
      "name": "United States"
    },
    {
      "code": "+1 242",
      "name": "Bahamas"
    },
    {
      "code": "+1 246",
      "name": "Barbados"
    },
    {
      "code": "+1 264",
      "name": "Anguilla"
    },
    {
      "code": "+1 268",
      "name": "Antigua and Barbuda"
    },
    {
      "code": "+1 268",
      "name": "Barbuda"
    },
    {
      "code": "+1 284",
      "name": "British Virgin Islands"
    },
    {
      "code": "+1 340",
      "name": "U.S. Virgin Islands"
    },
    {
      "code": "+1 441",
      "name": "Bermuda"
    },
    {
      "code": "+1 473",
      "name": "Grenada"
    },
    {
      "code": "+1 649",
      "name": "Turks and Caicos Islands"
    },
    {
      "code": "+1 670",
      "name": "Northern Mariana Islands"
    },
    {
      "code": "+1 671",
      "name": "Guam"
    },
    {
      "code": "+1 684",
      "name": "American Samoa"
    },
    {
      "code": "+1 767",
      "name": "Dominica"
    },
    {
      "code": "+1 787",
      "name": "Puerto Rico"
    },
    {
      "code": "+1 808",
      "name": "Midway Island"
    },
    {
      "code": "+1 808",
      "name": "Wake Island"
    },
    {
      "code": "+1 809",
      "name": "Dominican Republic"
    },
    {
      "code": "+1 868",
      "name": "Trinidad and Tobago"
    },
    {
      "code": "+1 869",
      "name": "Nevis"
    },
    {
      "code": "+1 876",
      "name": "Jamaica"
    },
    {
      "code": "+1664",
      "name": "Montserrat"
    },
    {
      "code": "+20",
      "name": "Egypt"
    },
    {
      "code": "+212",
      "name": "Morocco"
    },
    {
      "code": "+213",
      "name": "Algeria"
    },
    {
      "code": "+216",
      "name": "Tunisia"
    },
    {
      "code": "+218",
      "name": "Libya"
    },
    {
      "code": "+220",
      "name": "Gambia"
    },
    {
      "code": "+221",
      "name": "Senegal"
    },
    {
      "code": "+222",
      "name": "Mauritania"
    },
    {
      "code": "+223",
      "name": "Mali"
    },
    {
      "code": "+224",
      "name": "Guinea"
    },
    {
      "code": "+225",
      "name": "Ivory Coast"
    },
    {
      "code": "+226",
      "name": "Burkina Faso"
    },
    {
      "code": "+227",
      "name": "Niger"
    },
    {
      "code": "+228",
      "name": "Togo"
    },
    {
      "code": "+229",
      "name": "Benin"
    },
    {
      "code": "+230",
      "name": "Mauritius"
    },
    {
      "code": "+231",
      "name": "Liberia"
    },
    {
      "code": "+232",
      "name": "Sierra Leone"
    },
    {
      "code": "+233",
      "name": "Ghana"
    },
    {
      "code": "+234",
      "name": "Nigeria"
    },
    {
      "code": "+235",
      "name": "Chad"
    },
    {
      "code": "+236",
      "name": "Central African Republic"
    },
    {
      "code": "+237",
      "name": "Cameroon"
    },
    {
      "code": "+238",
      "name": "Cape Verde"
    },
    {
      "code": "+240",
      "name": "Equatorial Guinea"
    },
    {
      "code": "+241",
      "name": "Gabon"
    },
    {
      "code": "+242",
      "name": "Congo"
    },
    {
      "code": "+243",
      "name": "Congo, Dem. Rep. of (Zaire)"
    },
    {
      "code": "+244",
      "name": "Angola"
    },
    {
      "code": "+245",
      "name": "Guinea-Bissau"
    },
    {
      "code": "+246",
      "name": "British Indian Ocean Territory"
    },
    {
      "code": "+246",
      "name": "Diego Garcia"
    },
    {
      "code": "+247",
      "name": "Ascension"
    },
    {
      "code": "+248",
      "name": "Seychelles"
    },
    {
      "code": "+249",
      "name": "Sudan"
    },
    {
      "code": "+250",
      "name": "Rwanda"
    },
    {
      "code": "+251",
      "name": "Ethiopia"
    },
    {
      "code": "+253",
      "name": "Djibouti"
    },
    {
      "code": "+254",
      "name": "Kenya"
    },
    {
      "code": "+255",
      "name": "Tanzania"
    },
    {
      "code": "+255",
      "name": "Zanzibar"
    },
    {
      "code": "+256",
      "name": "Uganda"
    },
    {
      "code": "+257",
      "name": "Burundi"
    },
    {
      "code": "+260",
      "name": "Zambia"
    },
    {
      "code": "+261",
      "name": "Madagascar"
    },
    {
      "code": "+262",
      "name": "Mayotte"
    },
    {
      "code": "+262",
      "name": "Reunion"
    },
    {
      "code": "+263",
      "name": "Zimbabwe"
    },
    {
      "code": "+264",
      "name": "Namibia"
    },
    {
      "code": "+265",
      "name": "Malawi"
    },
    {
      "code": "+266",
      "name": "Lesotho"
    },
    {
      "code": "+267",
      "name": "Botswana"
    },
    {
      "code": "+268",
      "name": "Swaziland"
    },
    {
      "code": "+269",
      "name": "Comoros"
    },
    {
      "code": "+27",
      "name": "South Africa"
    },
    {
      "code": "+291",
      "name": "Eritrea"
    },
    {
      "code": "+297",
      "name": "Aruba"
    },
    {
      "code": "+298",
      "name": "Faroe Islands"
    },
    {
      "code": "+299",
      "name": "Greenland"
    },
    {
      "code": "+30",
      "name": "Greece"
    },
    {
      "code": "+31",
      "name": "Netherlands"
    },
    {
      "code": "+32",
      "name": "Belgium"
    },
    {
      "code": "+33",
      "name": "France"
    },
    {
      "code": "+34",
      "name": "Spain"
    },
    {
      "code": "+350",
      "name": "Gibraltar"
    },
    {
      "code": "+351",
      "name": "Portugal"
    },
    {
      "code": "+352",
      "name": "Luxembourg"
    },
    {
      "code": "+353",
      "name": "Ireland"
    },
    {
      "code": "+354",
      "name": "Iceland"
    },
    {
      "code": "+355",
      "name": "Albania"
    },
    {
      "code": "+356",
      "name": "Malta"
    },
    {
      "code": "+358",
      "name": "Finland"
    },
    {
      "code": "+359",
      "name": "Bulgaria"
    },
    {
      "code": "+36",
      "name": "Hungary"
    },
    {
      "code": "+370",
      "name": "Lithuania"
    },
    {
      "code": "+371",
      "name": "Latvia"
    },
    {
      "code": "+372",
      "name": "Estonia"
    },
    {
      "code": "+373",
      "name": "Moldova"
    },
    {
      "code": "+374",
      "name": "Armenia"
    },
    {
      "code": "+375",
      "name": "Belarus"
    },
    {
      "code": "+376",
      "name": "Andorra"
    },
    {
      "code": "+377",
      "name": "Monaco"
    },
    {
      "code": "+378",
      "name": "San Marino"
    },
    {
      "code": "+380",
      "name": "Ukraine"
    },
    {
      "code": "+381",
      "name": "Serbia"
    },
    {
      "code": "+382",
      "name": "Montenegro"
    },
    {
      "code": "+385",
      "name": "Croatia"
    },
    {
      "code": "+386",
      "name": "Slovenia"
    },
    {
      "code": "+387",
      "name": "Bosnia and Herzegovina"
    },
    {
      "code": "+389",
      "name": "Macedonia"
    },
    {
      "code": "+39",
      "name": "Italy"
    },
    {
      "code": "+40",
      "name": "Romania"
    },
    {
      "code": "+41",
      "name": "Switzerland"
    },
    {
      "code": "+420",
      "name": "Czech Republic"
    },
    {
      "code": "+421",
      "name": "Slovakia"
    },
    {
      "code": "+423",
      "name": "Liechtenstein"
    },
    {
      "code": "+43",
      "name": "Austria"
    },
    {
      "code": "+44",
      "name": "United Kingdom"
    },
    {
      "code": "+45",
      "name": "Denmark"
    },
    {
      "code": "+46",
      "name": "Sweden"
    },
    {
      "code": "+47",
      "name": "Norway"
    },
    {
      "code": "+48",
      "name": "Poland"
    },
    {
      "code": "+49",
      "name": "Germany"
    },
    {
      "code": "+500",
      "name": "Falkland Islands"
    },
    {
      "code": "+500",
      "name": "South Georgia and the South Sandwich Islands"
    },
    {
      "code": "+501",
      "name": "Belize"
    },
    {
      "code": "+502",
      "name": "Guatemala"
    },
    {
      "code": "+503",
      "name": "El Salvador"
    },
    {
      "code": "+504",
      "name": "Honduras"
    },
    {
      "code": "+505",
      "name": "Nicaragua"
    },
    {
      "code": "+506",
      "name": "Costa Rica"
    },
    {
      "code": "+507",
      "name": "Panama"
    },
    {
      "code": "+509",
      "name": "Haiti"
    },
    {
      "code": "+51",
      "name": "Peru"
    },
    {
      "code": "+52",
      "name": "Mexico"
    },
    {
      "code": "+53",
      "name": "Cuba"
    },
    {
      "code": "+537",
      "name": "Cyprus"
    },
    {
      "code": "+54",
      "name": "Argentina"
    },
    {
      "code": "+55",
      "name": "Brazil"
    },
    {
      "code": "+56",
      "name": "Chile"
    },
    {
      "code": "+56",
      "name": "Easter Island"
    },
    {
      "code": "+57",
      "name": "Colombia"
    },
    {
      "code": "+58",
      "name": "Venezuela"
    },
    {
      "code": "+590",
      "name": "Guadeloupe"
    },
    {
      "code": "+591",
      "name": "Bolivia"
    },
    {
      "code": "+593",
      "name": "Ecuador"
    },
    {
      "code": "+594",
      "name": "French Guiana"
    },
    {
      "code": "+595",
      "name": "Guyana"
    },
    {
      "code": "+595",
      "name": "Paraguay"
    },
    {
      "code": "+596",
      "name": "French Antilles"
    },
    {
      "code": "+596",
      "name": "Martinique"
    },
    {
      "code": "+597",
      "name": "Suriname"
    },
    {
      "code": "+598",
      "name": "Uruguay"
    },
    {
      "code": "+599",
      "name": "Curacao"
    },
    {
      "code": "+599",
      "name": "Netherlands Antilles"
    },
    {
      "code": "+60",
      "name": "Malaysia"
    },
    {
      "code": "+61",
      "name": "Australia"
    },
    {
      "code": "+61",
      "name": "Christmas Island"
    },
    {
      "code": "+61",
      "name": "Cocos-Keeling Islands"
    },
    {
      "code": "+62",
      "name": "Indonesia"
    },
    {
      "code": "+63",
      "name": "Philippines"
    },
    {
      "code": "+64",
      "name": "New Zealand"
    },
    {
      "code": "+65",
      "name": "Singapore"
    },
    {
      "code": "+66",
      "name": "Thailand"
    },
    {
      "code": "+670",
      "name": "East Timor"
    },
    {
      "code": "+670",
      "name": "Timor Leste"
    },
    {
      "code": "+672",
      "name": "Australian External Territories"
    },
    {
      "code": "+672",
      "name": "Norfolk Island"
    },
    {
      "code": "+673",
      "name": "Brunei"
    },
    {
      "code": "+674",
      "name": "Nauru"
    },
    {
      "code": "+675",
      "name": "Papua New Guinea"
    },
    {
      "code": "+676",
      "name": "Tonga"
    },
    {
      "code": "+677",
      "name": "Solomon Islands"
    },
    {
      "code": "+678",
      "name": "Vanuatu"
    },
    {
      "code": "+679",
      "name": "Fiji"
    },
    {
      "code": "+680",
      "name": "Palau"
    },
    {
      "code": "+681",
      "name": "Wallis and Futuna"
    },
    {
      "code": "+682",
      "name": "Cook Islands"
    },
    {
      "code": "+683",
      "name": "Niue"
    },
    {
      "code": "+685",
      "name": "Samoa"
    },
    {
      "code": "+686",
      "name": "Kiribati"
    },
    {
      "code": "+687",
      "name": "New Caledonia"
    },
    {
      "code": "+688",
      "name": "Tuvalu"
    },
    {
      "code": "+689",
      "name": "French Polynesia"
    },
    {
      "code": "+690",
      "name": "Tokelau"
    },
    {
      "code": "+691",
      "name": "Micronesia"
    },
    {
      "code": "+692",
      "name": "Marshall Islands"
    },
    {
      "code": "+7",
      "name": "Russia"
    },
    {
      "code": "+7 7",
      "name": "Kazakhstan"
    },
    {
      "code": "+7840",
      "name": "Abkhazia"
    },
    {
      "code": "+81",
      "name": "Japan"
    },
    {
      "code": "+82",
      "name": "South Korea"
    },
    {
      "code": "+84",
      "name": "Vietnam"
    },
    {
      "code": "+850",
      "name": "North Korea"
    },
    {
      "code": "+852",
      "name": "Hong Kong SAR China"
    },
    {
      "code": "+853",
      "name": "Macau SAR China"
    },
    {
      "code": "+855",
      "name": "Cambodia"
    },
    {
      "code": "+856",
      "name": "Laos"
    },
    {
      "code": "+86",
      "name": "China"
    },
    {
      "code": "+880",
      "name": "Bangladesh"
    },
    {
      "code": "+886",
      "name": "Taiwan"
    },
    {
      "code": "+90",
      "name": "Turkey"
    },
    {
      "code": "+91",
      "name": "India"
    },
    {
      "code": "+92",
      "name": "Pakistan"
    },
    {
      "code": "+93",
      "name": "Afghanistan"
    },
    {
      "code": "+94",
      "name": "Sri Lanka"
    },
    {
      "code": "+95",
      "name": "Myanmar"
    },
    {
      "code": "+960",
      "name": "Maldives"
    },
    {
      "code": "+961",
      "name": "Lebanon"
    },
    {
      "code": "+962",
      "name": "Jordan"
    },
    {
      "code": "+963",
      "name": "Syria"
    },
    {
      "code": "+964",
      "name": "Iraq"
    },
    {
      "code": "+965",
      "name": "Kuwait"
    },
    {
      "code": "+966",
      "name": "Saudi Arabia"
    },
    {
      "code": "+967",
      "name": "Yemen"
    },
    {
      "code": "+968",
      "name": "Oman"
    },
    {
      "code": "+970",
      "name": "Palestinian Territory"
    },
    {
      "code": "+971",
      "name": "United Arab Emirates"
    },
    {
      "code": "+972",
      "name": "Israel"
    },
    {
      "code": "+973",
      "name": "Bahrain"
    },
    {
      "code": "+974",
      "name": "Qatar"
    },
    {
      "code": "+975",
      "name": "Bhutan"
    },
    {
      "code": "+976",
      "name": "Mongolia"
    },
    {
      "code": "+977",
      "name": "Nepal"
    },
    {
      "code": "+98",
      "name": "Iran"
    },
    {
      "code": "+992",
      "name": "Tajikistan"
    },
    {
      "code": "+993",
      "name": "Turkmenistan"
    },
    {
      "code": "+994",
      "name": "Azerbaijan"
    },
    {
      "code": "+995",
      "name": "Georgia"
    },
    {
      "code": "+996",
      "name": "Kyrgyzstan"
    },
    {
      "code": "+998",
      "name": "Uzbekistan"
    }
  ]
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
    this.state = {
      role: localStorage.getItem('role'),
      isDisableResetConfig: false,
      isDisableSaveConfig: false,
      progress: 0,
      selectedWeek: '1 week',
      isShowCard1: false,
      isShowCard2: false,
      isShowCard3: false,
      isShowEditModel: false,
      selectedCurrency: 'AED',
      isShowServiceCenterProfilePrev: false,
      isShowServiceCenterProfileEditPrev: false,
      fastPass: '', fastPassValid: false,
      serviceCenterPercentage: '', serviceCenterPercentageValid: false,
      taboorPercentage: '', taboorPercentageValid: false,
      taboorAmount: 0, serviceCenterAmount: 0,
      fastPassCount: '', fastPassCountValid: false,
      fastPassTime: '', fastPassTimeValid: false,
      normalPassCount: '', normalPassCountValid: false,
      normalPassTime: '', normalPassTimeValid: false,
      activeTab: new Array(4).fill('1'),
      dropdownOpen: false,
      radioSelected: 2,
      modal: false,
      large: false,
      largeedit: false,
      addlarge: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
      //hamza variables
      getprofilelist: [],
      getconfiglist: [],
      getdefaultconfiglist: [],
      getinvoicelist: [],
      getpaymentmethodlist: [],
      getsubslist: [],
      reqlist: [],
      servicenameeng: '',
      servicenamearab: '',
      reqdoceng: '',
      reqdocarab: '',
      showeditservice: false,
      editservice: false,
      editdata: '',
      isManual: true,
      fastPassErrorMsg: {},
      fastPassFormValid: false,
      configErrorMsg: {},
      configFormValid: false,
      centerid: '',
      centername: '',
      centernamearab: '',
      centercountry: '',
      centerphone: '',
      centeremail: '',
      profileimg: '',
      largeeditpay: false,
      cardid: '',
      noofbranches: 0,
      noofusersp: 0,
      noofsms: 0,
      votes: '',
      servicesCount: 0,
      paymentmethodid: 45,
      expiryDateerror: '',
      servicecode: '',
      kioskenabled: false,
      modalPay: false,
      name: "",
      modalInputName: "",
      selectedOption :0,
      line: {
        labels: [],
        datasets: [
          {
            borderColor: gradient,
            pointRadius: 0,
            fill: false,
            borderWidth: 2,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: []
          }
        ]
      },
      //hamza variables
    };
    this.toggle = this.toggle.bind(this);
    this.toggleLarge = this.toggleLarge.bind(this);
    this.toggleedit = this.toggleedit.bind(this);
    this.addpaymentmethod = this.addpaymentmethod.bind(this);
    this.editpaymentmethod = this.editpaymentmethod.bind(this);
    this.toggleSmall = this.toggleSmall.bind(this);
    this.togglePrimary = this.togglePrimary.bind(this);
    this.toggleSuccess = this.toggleSuccess.bind(this);
    this.toggleWarning = this.toggleWarning.bind(this);
    this.toggleDanger = this.toggleDanger.bind(this);
    this.toggleInfo = this.toggleInfo.bind(this);
    this.closeModel = this.closeModel.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
    function randomArray(length, max) {
      return Array.apply(null, Array(length)).map(function () {
        return Math.round(Math.random() * max);
      });
    }
    this.datagrad = (canvas) => {
      const ctx = canvas.getContext("2d")
      const gradient = ctx.createLinearGradient(0, 0, 100, 0);
      gradient.addColorStop(0.4, 'orange');
      gradient.addColorStop(0.5, 'green');
      gradient.addColorStop(1, 'red');
      return {
        labels: ['May', 'June', 'July', 'Aug', 'Sep', 'Oct'],
        datasets: [
          {
            label: 'Votes',
            backgroundColor: gradient,
            borderColor: gradient,
            pointRadius: 0,
            // pointBorderColor: gradient,
            pointBackgroundColor: gradient,
            fill: false,
            borderWidth: 0,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: [50, 80, 90, 69, 98, 55]
          }
        ]
      }
    };
  }
  componentDidMount() {
    if (localStorage.getItem('isShowArabicLanguage') == 'true') {
      global.isShowArabicLanguage = true
    }
    else {
      global.isShowArabicLanguage = false
    }
    this.setState({ phoneCode: this.countries[0].code })
    this.getprofile()
    var prev = JSON.parse(localStorage.getItem('privileges'))
    var isPrev = false
    if (prev != null) {
      for (var i = 0; i < prev.length; i++) {
        if (prev[i] == 'Service_Center_Profile_View') {
          this.setState({ isShowServiceCenterProfilePrev: true })
          isPrev = true
        }
        else {
          if (!isPrev == true) {
            isPrev = false
          }

        }
        if (prev[i] == 'Service_Center_Profile_Edit') {
          this.setState({ isShowServiceCenterProfileEditPrev: true })
        }
      }
    }
    if (isPrev == false) {
      toaster.notify("User don't have privilege", {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration: 5000 // This notification will not automatically close
      });
    }


  }
  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  toggleLarge() {
    this.setState({
      addlarge: !this.state.addlarge,
    });
    if (this.state.addlarge == false) {
      this.setState({
        servicenameeng: '',
        servicenamearab: '',
        reqdoceng: '',
        reqdocarab: '',
        reqlist: []
      });
    }
  }
  addpaymentmethod() {
    this.setState({
      large: !this.state.large,
    });
    if (this.state.large == false) {
      this.setState({
        cardnumber: '',
        carddate: '',
        cardcvv: '',
        cardholdername: '',
      })
    }
  }
  editpaymentmethod() {
    this.setState({
      largeeditpay: !this.state.largeeditpay,
    });
  }
  toggleedit = (data) => {
    this.setState({
      largeedit: !this.state.largeedit,
    });
    if (this.state.largeedit == false) {
      this.setState({ reqlist: data.serviceRequirements, editservice: this.state.editservice });
      this.setState({ servicecode: data.serviceCode, kioskenabled: data.kiosikEnabled, servicenameeng: data.serviceTAB.serviceName, servicenamearab: data.serviceTAB.serviceNameArabic, serviceid: data.serviceTAB.serviceId });
    }
    else {
      this.setState({ reqlist: [], editservice: this.state.editservice });
      this.setState({ servicenameeng: '', servicenamearab: '', serviceid: 0, servicecode: '', kioskenabled: false, });
    }
  }
  toggleSmall() {
    this.setState({
      small: !this.state.small,
    });
  }

  togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }

  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }

  toggleWarning() {
    this.setState({
      warning: !this.state.warning,
    });
  }

  toggleDanger() {
    this.setState({
      danger: !this.state.danger,
    });
  }

  toggleInfo() {
    this.setState({
      info: !this.state.info,
    });
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }
  // hamza code 
  getprofile = () => {
    console.log('===============global.centerid=====================');
    console.log(localStorage.getItem('role'));
    console.log('===============global.centerid=====================');
    let token = localStorage.getItem('sessionToken');
    var body = {
      "id": global.centerid ? global.centerid : null
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getProfile';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    axios.post(url, body, header).then(responseJson => {
      console.log('==================getprofile==================');
      console.log(responseJson.data);
      console.log('=====================getprofile===============');
      this.serviceCenterId = responseJson.data.serviceCenter.serviceCenterId
      this.setState({ getprofilelist: responseJson.data });
      this.setState({ centerid: responseJson.data.serviceCenter.serviceCenterId });
      this.setState({ centername: responseJson.data.serviceCenter.serviceCenterName });
      this.setState({ centernamearab: responseJson.data.serviceCenter.serviceCenterArabicName });
      this.setState({ centercountry: responseJson.data.serviceCenter.country });
      this.setState({ centerphone: responseJson.data.serviceCenter.phoneNumber });
      this.setState({
        centeremail: responseJson.data.serviceCenter.email,
        profileimg: responseJson.data.serviceCenter.logoDataString
      });
      // this.getprofileimg(responseJson.data.serviceCenter.imageUrl)
      this.getconfig(responseJson.data.serviceCenter.serviceCenterId);
      var date = new Date();
      var last = new Date(date.getTime() - (6 * 24 * 60 * 60 * 1000))
      this.votes(last, new Date());
      this.getinvoice(responseJson.data.serviceCenter.serviceCenterId);
      this.getpaymentmethod(responseJson.data.serviceCenter.serviceCenterId);
      this.getsubs(responseJson.data.serviceCenter.serviceCenterId);
    }).catch(err => {
      console.log(err)
    });
  }
  getprofileimg = (file) => {
    let token = localStorage.getItem('sessionToken')
    var body = file
    const url = 'https://apicall.taboor.ae/taboor-qms/usermanagement/fileData/get';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    axios.post(url, body, header).then(responseJson => {
      console.log('==================getprofileimg==================');
      console.log(responseJson.data);
      console.log('=====================getprofileimg===============');
      // this.setState({profileimg:responseJson.data.applicationStatusResponse})
    }).catch(err => {
      console.log(err);
      alert('Network Error')
    });
  }
  getconfig = (id) => {
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/config/get';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    var body = {
      'id': id
    }
    axios.post(url, body, header).then(responseJson => {
      if (responseJson.data.applicationStatusCode == 0) {
        this.setState({ getconfiglist: responseJson.data })
        this.configId = responseJson.data.serviceCenterConfiguration.configurationId
        this.serviceCenter = responseJson.data.serviceCenterConfiguration.serviceCenter
        this.tempFastPass = responseJson.data.serviceCenterConfiguration.fastpassAmount
        this.tempTaboorAmount = (10 / 100) * responseJson.data.serviceCenterConfiguration.fastpassAmount
        this.tempFastPassCount = responseJson.data.serviceCenterConfiguration.fastpassStepoutAllowedCount
        this.tempNormalPassCount = responseJson.data.serviceCenterConfiguration.normalStepoutAllowedCount
        this.updateFastAmount(responseJson.data.serviceCenterConfiguration.fastpassAmount)
        this.updateFastPassCount(responseJson.data.serviceCenterConfiguration.fastpassStepoutAllowedCount)
        this.updateNormalPassCount(responseJson.data.serviceCenterConfiguration.normalStepoutAllowedCount)
        var timeInOffice = responseJson.data.serviceCenterConfiguration.fastpassStepoutTime;
        var timeOutOffice = responseJson.data.serviceCenterConfiguration.normalStepoutTime;
        var a = timeInOffice.split(':');
        var timeIn = (+a[0]) * 60 + (+a[1]);
        var b = timeOutOffice.split(':');
        var timeOut = (+b[0]) * 60 + (+b[1]);
        this.tempFastPassTime = responseJson.data.serviceCenterConfiguration.fastpassStepoutTime
        this.tempNormalPassTime = responseJson.data.serviceCenterConfiguration.normalStepoutTime
        this.updateFastPassTime(timeIn)
        this.updateNormalPassTime(timeOut)
        this.setState({
          serviceCenterPercentage: 90,
          taboorPercentage: 10,
        })
      }
    }).catch(err => {
      console.log(err);
      // alert('Network Error')
    });
  }
  validateFastPassForm = () => {
    const { fastPassValid } = this.state;
    this.setState({
      fastPassFormValid: fastPassValid
    })
  }
  validateConfigForm = () => {
    const { fastPassCountValid, fastPassTimeValid, normalPassTimeValid, normalPassCountValid } = this.state;
    this.setState({
      configFormValid: fastPassCountValid && fastPassTimeValid && normalPassTimeValid && normalPassCountValid
    })
  }
  updateFastAmount = (fastPass) => {
    this.setState({ fastPass }, this.validateFastAmount)
  }
  updateServicePercentages = (serviceCenterPercent) => {
    this.setState({
      serviceCenterPercentage: serviceCenterPercent,
      taboorPercentage: 100 - serviceCenterPercent
    }, this.validateFastAmount)
  }
  updateTaboorPercentages = (taboorPercentage) => {
    this.setState({
      taboorPercentage: taboorPercentage,
      serviceCenterPercentage: 100 - taboorPercentage
    }, this.validateFastAmount)

  }
  validateFastAmount = () => {
    const { fastPass } = this.state;
    let fastPassValid = true;
    let fastPassErrorMsg = { ...this.state.fastPassErrorMsg }

    if (fastPass.length == 0 || fastPass == '') {
      fastPassValid = false;
      fastPassErrorMsg.fastPass = 'Required'
      this.setState({
        serviceCenterAmount: '',
        taboorAmount: ''
      })
    }
    else if (!/^\d+$/.test(fastPass)) {
      fastPassValid = false;
      fastPassErrorMsg.fastPass = 'Must be a number'
    }
    if (fastPassValid == true) {
      var tempServiceCenterAmount = (this.state.serviceCenterPercentage / 100) * fastPass
      tempServiceCenterAmount = Math.round((tempServiceCenterAmount + Number.EPSILON) * 100) / 100
      var tempTaboorAmount = (this.state.taboorPercentage / 100) * fastPass
      tempTaboorAmount = Math.round((tempTaboorAmount + Number.EPSILON) * 100) / 100
      this.setState({
        serviceCenterAmount: tempServiceCenterAmount,
        taboorAmount: tempTaboorAmount
      })
    }

    this.setState({ fastPassValid, fastPassErrorMsg }, this.validateFastPassForm)
  }
  updateFastPassCount = (fastPassCount) => {
    this.setState({ fastPassCount }, this.validateFastPassCount)
  }
  validateFastPassCount = () => {
    const { fastPassCount } = this.state;
    let fastPassCountValid = true;
    let configErrorMsg = { ...this.state.configErrorMsg }

    if (fastPassCount.length == 0 || fastPassCount == '') {
      fastPassCountValid = false;
      configErrorMsg.fastPassCount = 'Required'
    }
    else if (!/^\d+$/.test(fastPassCount)) {
      fastPassCountValid = false;
      configErrorMsg.fastPassCount = 'Must be a number'
    }
    this.setState({ fastPassCountValid, configErrorMsg }, this.validateConfigForm)
  }
  updateFastPassTime = (fastPassTime) => {
    this.setState({ fastPassTime }, this.validateFastPassTime)
  }
  validateFastPassTime = () => {
    const { fastPassTime } = this.state;
    let fastPassTimeValid = true;
    let configErrorMsg = { ...this.state.configErrorMsg }

    if (fastPassTime.length == 0 || fastPassTime == '') {
      fastPassTimeValid = false;
      configErrorMsg.fastPassTime = 'Required'
    }
    else if (!/^\d+$/.test(fastPassTime)) {
      fastPassTimeValid = false;
      configErrorMsg.fastPassTime = 'Must be a number'
    }

    this.setState({ fastPassTimeValid, configErrorMsg }, this.validateConfigForm)
  }
  updateNormalPassCount = (normalPassCount) => {
    this.setState({ normalPassCount }, this.validateNormalPassCount)
  }
  validateNormalPassCount = () => {
    const { normalPassCount } = this.state;
    let normalPassCountValid = true;
    let configErrorMsg = { ...this.state.configErrorMsg }

    if (normalPassCount.length == 0 || normalPassCount == '') {
      normalPassCountValid = false;
      configErrorMsg.normalPassCount = 'Required'
    }
    else if (!/^\d+$/.test(normalPassCount)) {
      normalPassCountValid = false;
      configErrorMsg.normalPassCount = 'Must be a number'
    }
    this.setState({ normalPassCountValid, configErrorMsg }, this.validateConfigForm)
  }
  updateNormalPassTime = (normalPassTime) => {
    this.setState({ normalPassTime }, this.validateNormalPassTime)
  }
  validateNormalPassTime = () => {
    const { normalPassTime } = this.state;
    let normalPassTimeValid = true;
    let configErrorMsg = { ...this.state.configErrorMsg }

    if (normalPassTime.length == 0 || normalPassTime == '') {
      normalPassTimeValid = false;
      configErrorMsg.normalPassTime = 'Required'
    }
    else if (!/^\d+$/.test(normalPassTime)) {
      normalPassTimeValid = false;
      configErrorMsg.normalPassTime = 'Must be a number'
    }
    this.setState({ normalPassTimeValid, configErrorMsg }, this.validateConfigForm)
  }
  assignDefaultConfig(type) {
    this.setState({
      progress: 50, isDisableResetConfig: true
    })
    var data = []
    data.push(type)
    data.push(this.serviceCenterId)
    let token = localStorage.getItem('sessionToken')
    var body = {
      "ids": data
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/config/default/assign';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    axios.post(url, body, header).then(responseJson => {
      if (responseJson.data.applicationStatusCode == 0) {
        this.setState({
          progress: 100, isDisableResetConfig: false
        })
        this.configId = responseJson.data.serviceCenterConfiguration.configurationId
        this.serviceCenter = responseJson.data.serviceCenterConfiguration.serviceCenter
        if (type == 0) {
          this.tempFastPass = responseJson.data.serviceCenterConfiguration.fastpassAmount
          this.tempTaboorAmount = (10 / 100) * responseJson.data.serviceCenterConfiguration.fastpassAmount
          this.updateFastAmount(responseJson.data.serviceCenterConfiguration.fastpassAmount)
          this.setState({
            serviceCenterPercentage: 90,
            taboorPercentage: 10,
          })
        }
        else if (type == 1) {
          this.tempFastPassCount = responseJson.data.serviceCenterConfiguration.fastpassStepoutAllowedCount
          this.tempNormalPassCount = responseJson.data.serviceCenterConfiguration.normalStepoutAllowedCount
          this.tempFastPassTime = responseJson.data.serviceCenterConfiguration.fastpassStepoutTime
          this.tempNormalPassTime = responseJson.data.serviceCenterConfiguration.normalStepoutTime
          this.updateFastPassCount(responseJson.data.serviceCenterConfiguration.fastpassStepoutAllowedCount)
          this.updateNormalPassCount(responseJson.data.serviceCenterConfiguration.normalStepoutAllowedCount)
          var timeInOffice = responseJson.data.serviceCenterConfiguration.fastpassStepoutTime;
          var timeOutOffice = responseJson.data.serviceCenterConfiguration.normalStepoutTime;
          var a = timeInOffice.split(':');
          var timeIn = (+a[0]) * 60 + (+a[1]);
          var b = timeOutOffice.split(':');
          var timeOut = (+b[0]) * 60 + (+b[1]);
          this.updateFastPassTime(timeIn)
          this.updateNormalPassTime(timeOut)
        }
        this.setState({ getconfiglist: responseJson.data })


      }
    }).catch(err => {
      console.log(err)
    });
  }
  updateConfig(type) {
    this.setState({
      progress: 50, isDisableSaveConfig: true
    })
    let token = localStorage.getItem('sessionToken')
    var obj;
    if (type == 0) {

      obj = JSON.stringify({
        "currency": this.state.selectedCurrency,
        "fastpassAmount": parseInt(this.state.fastPass),
        "fastpassStepoutAllowedCount": this.tempFastPassCount,
        "fastpassStepoutTime": this.tempFastPassTime,
        "normalStepoutAllowedCount": this.tempNormalPassCount,
        "normalStepoutTime": this.tempNormalPassTime,
        "taboorShareAmount": parseInt(this.state.taboorAmount),
        "configurationId": this.configId,
        "serviceCenter": this.serviceCenter
      })
    }
    else if (type == 1) {
      var fhours = Math.floor(this.state.fastPassTime / 60);
      fhours = fhours < 10 ? '0' + fhours : fhours
      var fminutes = this.state.fastPassTime % 60;
      fminutes = fminutes < 10 ? '0' + fminutes : fminutes
      var tempFTime = fhours + ":" + fminutes
      var nhours = Math.floor(this.state.fastPassTime / 60);
      nhours = nhours < 10 ? '0' + nhours : nhours
      var nminutes = this.state.normalPassTime % 60;
      nminutes = nminutes < 10 ? '0' + nminutes : nminutes
      var tempNTime = nhours + ":" + nminutes
      obj = JSON.stringify({
        "currency": this.state.selectedCurrency,
        "fastpassAmount": parseInt(this.tempFastPass),
        "fastpassStepoutAllowedCount": this.state.fastPassCount,
        "fastpassStepoutTime": tempFTime,
        "normalStepoutAllowedCount": this.state.normalPassCount,
        "normalStepoutTime": tempNTime,
        "taboorShareAmount": parseInt(this.tempTaboorAmount),
        "configurationId": this.configId,
        "serviceCenter": this.serviceCenter
      })
    }
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/config/update', {
      method: 'POST',
      body: obj,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.applicationStatusCode == 0) {
          this.setState({
            progress: 100, isDisableSaveConfig: false
          })
          this.setState({ getconfiglist: responseJson.serviceCenterConfiguration })
          this.serviceCenter = responseJson.serviceCenterConfiguration.serviceCenter
          this.configId = responseJson.serviceCenterConfiguration.configurationId
          this.tempFastPass = responseJson.serviceCenterConfiguration.fastpassAmount
          this.tempTaboorAmount = (10 / 100) * responseJson.serviceCenterConfiguration.fastpassAmount
          this.tempFastPassCount = responseJson.serviceCenterConfiguration.fastpassStepoutAllowedCount
          this.tempNormalPassCount = responseJson.serviceCenterConfiguration.normalStepoutAllowedCount
          this.updateFastAmount(responseJson.serviceCenterConfiguration.fastpassAmount)
          this.updateFastPassCount(responseJson.serviceCenterConfiguration.fastpassStepoutAllowedCount)
          this.updateNormalPassCount(responseJson.serviceCenterConfiguration.normalStepoutAllowedCount)
          var timeInOffice = responseJson.serviceCenterConfiguration.fastpassStepoutTime;
          var timeOutOffice = responseJson.serviceCenterConfiguration.normalStepoutTime;
          var a = timeInOffice.split(':');
          var timeIn = (+a[0]) * 60 + (+a[1]);
          var b = timeOutOffice.split(':');
          var timeOut = (+b[0]) * 60 + (+b[1]);
          this.tempFastPassTime = responseJson.serviceCenterConfiguration.fastpassStepoutTime
          this.tempNormalPassTime = responseJson.serviceCenterConfiguration.normalStepoutTime
          this.updateFastPassTime(timeIn)
          this.updateNormalPassTime(timeOut)
          this.setState({
            serviceCenterPercentage: 90,
            taboorPercentage: 10,
          })
        }
        else {
          toaster.notify(responseJson.devMessage, {
            duration: 5000 // This notification will not automatically close
          });
        }

      })
  }
  getinvoice = (id) => {
    let token = localStorage.getItem('sessionToken')
    var body = {
      "id": id
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getInvoices';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    axios.post(url, body, header).then(responseJson => {
      console.log('==================getinvoice==================');
      console.log(responseJson.data);
      console.log('=====================getinvoice===============');
      this.setState({ getinvoicelist: responseJson.data })
    }).catch(err => {
      console.log(err);
      alert('Network Error')
    });
  }
  getpaymentmethod = (id) => {
    let token = localStorage.getItem('sessionToken')
    var body = {
      "id": id
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getPaymentMethods';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    axios.post(url, body, header).then(responseJson => {
      console.log('==================getpaymentmethod==================');
      console.log(responseJson.data);
      console.log('=====================getpaymentmethod===============');
      if (responseJson.data.applicationStatusCode == -1) {
        this.setState({ getpaymentmethodlist: [] })
      }
      else {
        this.setState({ getpaymentmethodlist: responseJson.data })
      }
    }).catch(err => {
      console.log(err);
      alert('Network Error')
    });
  }
  changeDate(value) {
    if (value.length == 2 && this.state.carddate == 1) {
      value += '/'
    } else if (value.length == 2 && this.state.carddate.length == 3) {
      value = value.substring(0, value.length - 1)
    }
    this.setState({ carddate: value })
    // this.setState({selectedDate:e.target.value})
  }
  getsubs = (id) => {
    let token = localStorage.getItem('sessionToken')
    var body = {
      "id": id
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getSubscription';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    axios.post(url, body, header).then(responseJson => {
      console.log('============getSubscription========================');
      console.log(responseJson.data);
      console.log('==============getSubscription======================');
      this.setState({ getsubslist: responseJson.data.subscriptions, servicesCount: responseJson.data.servicesCount })
    }).catch(err => {
      console.log(err);
      alert('Network Error')
    });
  }
  addservice = (id) => {

    let token = localStorage.getItem('sessionToken')
    var body = {
      "service": {
        "serviceId": id,
        "serviceName": this.state.servicenameeng,
        "serviceNameArabic": this.state.servicenamearab,
        "serviceDescription": ""
      },
      "serviceCode": this.state.servicecode,
      "kiosikEnabled": this.state.kioskenabled,
      "requirementList": this.state.reqlist
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/service/add';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    if (this.state.servicenameeng == '') {
      toaster.notify('Service Name English is Required', {
        duration: 1000
      });
    }
    else if (this.state.servicenamearab == '') {
      toaster.notify('Service Name Arabic is Required', {
        duration: 1000
      });
    }
    else if (!/^[\u0621-\u064A\040]+$/.test(this.state.servicenamearab)) {
      toaster.notify('Only arabic alphabets are allowed', {
        duration: 1000
      });
    }
    else {
      this.setState({
        progress: 50
      })
      axios.post(url, body, header).then(responseJson => {
        console.log(responseJson.data);
        if (responseJson.data.applicationStatusCode == 0) {
          this.setState({
            progress: 100
          })
          this.setState({ getprofilelist: [] })
          this.getprofile()
        }
        else {
          this.setState({
            progress: 100
          })
        }
      }).catch(err => {
        console.log(err);
        alert('Network Error')
      });
    }
  }
  deleteservice = (id) => {
    let token = localStorage.getItem('sessionToken')
    var body = {
      "id": id
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/service/delete';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    confirmAlert({
      title: '',
      message: 'Do you want to delete service?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            this.setState({ progress: 50 })
            axios.post(url, body, header).then(responseJson => {
              if (responseJson.data.applicationStatusCode == 0) {
                this.setState({ progress: 100 })
                this.setState({ getprofilelist: [] });
                this.getprofile();
              }
              else {
                this.setState({ progress: 100 })
              }
            }).catch(err => {
              alert('Network Error')
            });
          }
        },
        {
          label: 'No',
          onClick: () => { console.log('no selected'); }
        }
      ]
    });

  }
  addreqdoc = () => {
    if (!/^[\u0621-\u064A\040]+$/.test(this.state.reqdocarab)) {
      toaster.notify('Only arabic alphabets are allowed', {
        duration: 1000
      });
    }
    else {
      var tempitem = {
        "requirementId": 0,
        "requirementName": this.state.reqdoceng,
        "requirementNameArabic": this.state.reqdocarab
      }
      var itemdataset = this.state.reqlist;
      itemdataset.push(tempitem);
      this.setState({ reqlist: itemdataset });
      this.setState({ reqdoceng: '', reqdocarab: '' });
    }

  }
  deletereqdoc = (i) => {
    var array = [...this.state.reqlist]; // make a separate copy of the array
    var index = i
    if (index !== -1) {
      array.splice(index, 1);
      this.setState({ reqlist: array });
    }
  }
  resetserviceadd = () => {
    this.setState({ reqdoceng: '', reqdocarab: '', servicenameeng: '', servicenamearab: '', reqlist: [] });
  }
  closeModel() { this.setState({ isShowEditModel: !this.state.isShowEditModel }); }
  updatecenter = () => {

    let token = localStorage.getItem('sessionToken')
    var body = {
      "serviceCenterId": this.state.centerid,
      "serviceCenterName": this.state.centername,
      "serviceCenterArabicName": this.state.centernamearab,
      "country": this.state.centercountry,
      "email": this.state.centeremail,
      "phoneNumber": this.state.centerphone,
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/edit';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    if (this.state.centername == '') {

      toaster.notify('Service Center Name English is Required', {
        duration: 1000
      });
    }
    else if (this.state.centernamearab == '') {
      toaster.notify('Service Center Arabic is Required', {
        duration: 1000
      });
    }
    else if (!/^[\u0621-\u064A\040]+$/.test(this.state.centernamearab)) {
      toaster.notify('Only arabic alphabets are allowed', {
        duration: 1000
      });
    }
    else if (this.state.centercountry == '') {
      toaster.notify('Service Center Country is Required', {
        duration: 1000
      });
    }
    else if (this.state.centeremail == '') {
      toaster.notify('Service Center Email is Required', {
        duration: 1000
      });
    }
    else if (this.state.centerphone == '') {
      toaster.notify('Service Center Phone is Required', {
        duration: 1000
      });
    }
    else {
      this.setState({
        progress: 50
      })
      axios.post(url, body, header).then(responseJson => {
        console.log(responseJson.data);
        if (responseJson.data.applicationStatusCode == 0) {
          this.setState({
            progress: 100
          })
          this.setState({ getprofilelist: [] })
          this.getprofile()
        }
        else {
          this.setState({
            progress: 100
          })
        }
      }).catch(err => {
        console.log(err);
        alert('Network Error')
      });
    }
  }
  addpaymentinservice = () => {

    let token = localStorage.getItem('sessionToken');
    var body = {
      "serviceCenterId": this.state.centerid,
      "paymentMethodId": this.state.paymentmethodid,
      "cardType": "Credit",
      "cardNumber": this.state.cardnumber,
      "cardTitle": this.state.cardholdername,
      "expiryMonth": this.state.carddate[0] + this.state.carddate[1],
      "expiryYear": '20' + this.state.carddate[3] + this.state.carddate[4],
      "cardCvv": this.state.cardcvv,
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/paymentMethod/add';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    if (this.state.cardnumber == '') {
      toaster.notify('Card Number is Required', {
        duration: 1000
      });
    }
    else if (this.state.cardnumber.length < 16) {
      toaster.notify('Card Number must be 16 digit', {
        duration: 1000
      });
    }
    else if (this.state.cardnumber.length > 16) {
      toaster.notify('Card Number must be 16 digit', {
        duration: 1000
      });
    }
    else if (this.state.cardholdername == '') {
      toaster.notify('Card Holder is Required', {
        duration: 1000
      });
    }
    else if (this.state.cardcvv == '') {
      toaster.notify('CVV is Required', {
        duration: 1000
      });
    }
    else if (this.state.cardcvv.length > 3) {
      toaster.notify('CVV number must be 3 characters', {
        duration: 1000
      });
    }
    else if (this.state.cardcvv.length < 3) {
      toaster.notify('CVV number must be 3 characters', {
        duration: 1000
      });
    }
    else if (!/^\d+$/.test(this.state.cardcvv)) {

      toaster.notify('Must be a number', {
        duration: 1000
      });
    }
    else if (this.state.carddate == '') {
      toaster.notify('Card Date is Required', {
        duration: 1000
      });
    }
    else {
      this.setState({
        progress: 50
      })
      axios.post(url, body, header).then(responseJson => {
        console.log(body)
        console.log(responseJson.data);

        if (responseJson.data.applicationStatusCode == 0) {
          this.setState({
            progress: 100
          })
          this.setState({ getpaymentmethodlist: [] })
          this.getpaymentmethod(this.state.centerid)
        }
        else {
          toaster.notify(responseJson.data.applicationStatusResponse, {
            duration: 5000
          });
          this.setState({
            progress: 100
          })
        }
      }).catch(err => {
        console.log(err);
        // alert('Network Error')
      });
    }
  }
  updatepaymentmethod = () => {
    let token = localStorage.getItem('sessionToken');
    var body =
    {
      "bankCardId": this.state.cardid,
      "cardType": "Credit",
      "cardNumber": this.state.cardnumber,
      "cardTitle": this.state.cardholdername,
      "expiryMonth": this.state.carddate[0] + this.state.carddate[1],
      "expiryYear": '20' + this.state.carddate[3] + this.state.carddate[4],
      "cardCvv": this.state.cardcvv,
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/updatePayment';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    if (this.state.cardnumber == '') {
      toaster.notify('Card Number is Required', {
        duration: 1000
      });
    }
    else if (this.state.cardnumber.length < 16) {
      toaster.notify('Card Number must be 16 digit', {
        duration: 1000
      });
    }
    else if (this.state.cardnumber.length > 16) {
      toaster.notify('Card Number must be 16 digit', {
        duration: 1000
      });
    }
    else if (this.state.cardholdername == '') {
      toaster.notify('Card Holder is Required', {
        duration: 1000
      });
    }
    else if (this.state.cardcvv == '') {
      toaster.notify('CVV is Required', {
        duration: 1000
      });
    }
    else if (this.state.cardcvv.length > 3) {
      toaster.notify('CVV number must be 3 characters', {
        duration: 1000
      });
    }
    else if (this.state.cardcvv.length < 3) {
      toaster.notify('CVV number must be 3 characters', {
        duration: 1000
      });
    }
    else if (!/^\d+$/.test(this.state.cardcvv)) {

      toaster.notify('Must be a number', {
        duration: 1000
      });
    }
    else if (this.state.carddate == '') {
      toaster.notify('Card Date is Required', {
        duration: 1000
      });
    }
    else if (this.state.carddate == '') {
      toaster.notify('Card Date is Required', {
        duration: 1000
      });
    }
    else {
      this.setState({
        progress: 50
      })
      axios.post(url, body, header).then(responseJson => {
        this.setState({ progress: 100 });
        if (responseJson.data.applicationStatusCode == -1) {
          console.log(responseJson.data);
          toaster.notify(responseJson.data.applicationStatusResponse, {
            duration: 5000
          });
        }
        else {
          console.log(responseJson.data);
          this.setState({ getpaymentmethodlist: [] })
          this.getpaymentmethod(this.state.centerid)
        }
      }).catch(err => {
        console.log(err);
        // alert('Network Error')
      });
    }
  }
  deletepayment = (id) => {
    let token = localStorage.getItem('sessionToken');

    
    var body =
    {
      "id": id
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/paymentMethod/delete';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    confirmAlert({
      title: '',
      message: 'Do you want to delete card?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            axios.post(url, body, header).then(responseJson => {
              console.log(responseJson.data);
              this.getpaymentmethod(this.state.centerid)
            }).catch(err => {
              console.log(err);
            });
          }
        },
        {
          label: 'No',
          onClick: () => { console.log('no selected'); }
        }
      ]
    });

  }
  changePaymentMethod  = () => {
    this.setState({
      progress: 50
    })
  
    if (this.state.selectedOption==null){
      toaster.notify("Please Select a valid payment type", {
        duration: 5000
      });

    }

    var bankCardId=this.state.getsubslist.bankCard?.bankCardId
    // if (this.state.selectedOption!=0){
    //   console.log("selected option "+this.state.selectedOption)
    //   bankCardId=this.state.selectedOption
    // }

    var total = this.total((this.state.getsubslist.planSubscriptionType == 'ANNUALY' ? this.state.getsubslist.paymentPlan.prices[0].priceAnnually : this.state.getsubslist.paymentPlan.prices[0].priceMonthly) * this.state.noofbranches, this.state.getsubslist.paymentPlan.prices[0].perUserPrice * this.state.noofusersp, this.state.getsubslist.paymentPlan.prices[0].perSMSPrice * this.state.noofsms);
    var totaltax = this.tatalvtax((this.state.getsubslist.planSubscriptionType == 'ANNUALY' ? this.state.getsubslist.paymentPlan.prices[0].priceAnnually : this.state.getsubslist.paymentPlan.prices[0].priceMonthly) * this.state.noofbranches, this.state.getsubslist.paymentPlan.prices[0].perUserPrice * this.state.noofusersp, this.state.getsubslist.paymentPlan.prices[0].perSMSPrice * this.state.noofsms);
    let token = localStorage.getItem('sessionToken');
    var body = {
      "bankCardId": bankCardId,
      "serviceCenterId": this.state.getsubslist.serviceCenter.serviceCenterId,
      "kioskEnabled": this.state.getsubslist.paymentPlan.kioskEnabled,
      "EasyPaymentEnabled": this.state.getsubslist.paymentPlan.easyPaymentEnabled,
      "advanceReportingEnabled": this.state.getsubslist.paymentPlan.advanceReportingEnabled,
      "noOfBranches": this.state.getsubslist.noOfBranches + this.state.noofbranches,
      "noOfUsersPerBranch": this.state.getsubslist.noOfUsersPerBranch + this.state.noofusersp,
      "noOfSMS": this.state.getsubslist.noOfSMS + this.state.noofsms,
      "chargesRemaining": this.state.getsubslist.chargesTotal - this.state.getsubslist.chargesPaid,
      "totalChargesBeforeTax": total + (this.state.getsubslist.chargesTotal - this.state.getsubslist.chargesPaid),
      "totalCharges": totaltax + (this.state.getsubslist.chargesTotal - this.state.getsubslist.chargesPaid),
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/changePaymentMethod';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    axios.post(url, body, header).then(responseJson => {
      console.log(responseJson.data);
      this.setState({
        progress: 100
      })
      console.log(responseJson.data.applicationStatusCode)
      if(responseJson.data.applicationStatusCode==0){
        console.log(responseJson.data.applicationStatusCode)
      this.setState({ noofbranches: 0, noofusersp: 0, noofsms: 0 })
      this.getsubs(this.state.getsubslist.serviceCenter.serviceCenterId);
      }
      else{
        toaster.notify(responseJson.data.devMessage, {
          duration: 5000
        });
      }
    }).catch(err => {
      console.log(err);
    });
    this.modalClose();
  }

  updatesubscription = () => {
    this.setState({
      progress: 50
    })
  
    if (this.state.selectedOption==null){
      toaster.notify("Please Select a valid payment type", {
        duration: 5000
      });

    }

    var bankCardId=this.state.getsubslist.bankCard?.bankCardId
    if (this.state.selectedOption!=0){
      console.log("selected option "+this.state.selectedOption)
      bankCardId=this.state.selectedOption
    }

    var total = this.total((this.state.getsubslist.planSubscriptionType == 'ANNUALY' ? this.state.getsubslist.paymentPlan.prices[0].priceAnnually : this.state.getsubslist.paymentPlan.prices[0].priceMonthly) * this.state.noofbranches, this.state.getsubslist.paymentPlan.prices[0].perUserPrice * this.state.noofusersp, this.state.getsubslist.paymentPlan.prices[0].perSMSPrice * this.state.noofsms);
    var totaltax = this.tatalvtax((this.state.getsubslist.planSubscriptionType == 'ANNUALY' ? this.state.getsubslist.paymentPlan.prices[0].priceAnnually : this.state.getsubslist.paymentPlan.prices[0].priceMonthly) * this.state.noofbranches, this.state.getsubslist.paymentPlan.prices[0].perUserPrice * this.state.noofusersp, this.state.getsubslist.paymentPlan.prices[0].perSMSPrice * this.state.noofsms);
    let token = localStorage.getItem('sessionToken');
    var body = {
      "bankCardId": bankCardId,
      "serviceCenterId": this.state.getsubslist.serviceCenter.serviceCenterId,
      "kioskEnabled": this.state.getsubslist.paymentPlan.kioskEnabled,
      "EasyPaymentEnabled": this.state.getsubslist.paymentPlan.easyPaymentEnabled,
      "advanceReportingEnabled": this.state.getsubslist.paymentPlan.advanceReportingEnabled,
      "noOfBranches": this.state.getsubslist.noOfBranches + this.state.noofbranches,
      "noOfUsersPerBranch": this.state.getsubslist.noOfUsersPerBranch + this.state.noofusersp,
      "noOfSMS": this.state.getsubslist.noOfSMS + this.state.noofsms,
      "chargesRemaining": this.state.getsubslist.chargesTotal - this.state.getsubslist.chargesPaid,
      "totalChargesBeforeTax": total + (this.state.getsubslist.chargesTotal - this.state.getsubslist.chargesPaid),
      "totalCharges": totaltax + (this.state.getsubslist.chargesTotal - this.state.getsubslist.chargesPaid),
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/upgradeSubscription';
    const header = {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }
    axios.post(url, body, header).then(responseJson => {
      console.log(responseJson.data);
      this.setState({
        progress: 100
      })
      console.log(responseJson.data.applicationStatusCode)
      if(responseJson.data.applicationStatusCode==0){
        console.log(responseJson.data.applicationStatusCode)
      this.setState({ noofbranches: 0, noofusersp: 0, noofsms: 0 })
      this.getsubs(this.state.getsubslist.serviceCenter.serviceCenterId);
      }
      else{
        toaster.notify(responseJson.data.devMessage, {
          duration: 5000
        });
      }
    }).catch(err => {
      console.log(err);
    });
    this.modalClose();
  }
  total = (b, u, s) => {

    return b + u + s;
  }
  tatalvtax = (b, u, s) => {
    return (b + u + s) + ((b + u + s) * 5 / 100);
  }
  votes(startDate, endDate) {
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getVotes', {
      method: 'POST',
      body: JSON.stringify({
        "idList": [this.serviceCenterId],
        "endDate": endDate,
        "startDate": startDate
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
      .then(response => response.json())
      .then(responsejson => {
        if (responsejson.applicationStatusCode == 0) {
          var localLine = {
            labels: [],
            datasets: [
              {
                borderColor: gradient,
                pointRadius: 0,
                fill: false,
                borderWidth: 2,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: []
              }
            ]
          }
          if (responsejson.values != null) {
            var tempSatisfied = 0
            var tempAvg = 0
            var tempPoor = 0
            for (var i = 0; i < responsejson.values.length; i++) {
              if (responsejson.values[i].key == 'SATISFIED' || responsejson.values[i].key == 'AVERAGE' || responsejson.values[i].key == 'POOR') {
                if (responsejson.values[i].key == 'SATISFIED') {
                  tempSatisfied = parseInt(responsejson.values[i].value)
                  // this.state.pie.datasets[0].data[0]=parseInt(responsejson.values[i].value);
                  this.setState({
                    staisfied: parseInt(responsejson.values[i].value)
                  })
                }
                if (responsejson.values[i].key == 'AVERAGE') {
                  tempAvg = parseInt(responsejson.values[i].value);
                  // this.state.pie.datasets[0].data[1]=parseInt(responsejson.values[i].value);
                  this.setState({
                    average: parseInt(responsejson.values[i].value)
                  })
                }
                if (responsejson.values[i].key == 'POOR') {
                  tempPoor = parseInt(responsejson.values[i].value);
                  // this.state.pie.datasets[0].data[2]=parseInt(responsejson.values[i].value)
                  this.setState({
                    poor: parseInt(responsejson.values[i].value)
                  })
                }
              }
              else {
                localLine.labels.push(responsejson.values[i].key)
                localLine.datasets[0].data.push(responsejson.values[i].value)
              }
            }
            var t = 0
            for (var j = 0; j < localLine.datasets[0].data.length; j++) {

              t = t + parseInt(localLine.datasets[0].data[j])
            }
            this.setState({
              line: localLine,
              totalTempTickets: t,
            })
            var total = tempAvg + tempSatisfied + tempPoor
            this.setState({
              totalTempRatings: total
            })
          }

        }
      })
    // let token = localStorage.getItem('sessionToken');
    // const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getVotes';
    // const header={headers: {
    //   'Authorization': `Bearer ${token}`
    // }}
    // var body
    // axios.post(url,header).then(responseJson => {
    //   console.log(responseJson.data);
    //   this.setState({votes:responseJson.data})
    // }).catch(err=>{
    //   console.log(err);
    // });
  }
  changeDays = e => {
    this.setState({ selectedWeek: e.currentTarget.textContent })
    {
      if (e.currentTarget.textContent == '1 week') {
        var date = new Date();
        var last = new Date(date.getTime() - (6 * 24 * 60 * 60 * 1000))
        this.votes(last, new Date())
      }
      else if (e.currentTarget.textContent == '2 week') {
        var date = new Date();
        var last = new Date(date.getTime() - (13 * 24 * 60 * 60 * 1000))
        this.votes(last, new Date())
      }
      else if (e.currentTarget.textContent == '1 month') {
        var oneMonthAgo = new Date(
          new Date().getFullYear(),
          new Date().getMonth() - 1,
          new Date().getDate()
        );
        this.votes(oneMonthAgo, new Date())
      }
      else if (e.currentTarget.textContent == '6 month') {
        var oneMonthAgo = new Date(
          new Date().getFullYear(),
          new Date().getMonth() - 6,
          new Date().getDate()
        );
        this.votes(oneMonthAgo, new Date())
      }
    }

  }
  updateEnglishServiceCenterName = (servicenameeng) => {
    if (servicenameeng.length == 0 || servicenameeng == '') {
      this.setState({ servicenameengerror: 'Required' });
    }
    else if (!/^[A-Za-z][A-Za-z0-9]*$/.test(servicenameeng)) {
      this.setState({ servicenameengerror: 'Only english alphabets are allowed' });
    }
    else if (servicenameeng.length < 2) {
      this.setState({ servicenameengerror: 'Name should be at least 2 characters' });
    }
    else {
      this.setState({ servicenameengerror: '' });
    }
    this.setState({ servicenameeng: servicenameeng });
  }
  reqdocengcheck = (reqdoceng) => {
    if (reqdoceng.length == 0 || reqdoceng == '') {
      this.setState({ reqdocengerror: 'Required' });
    }
    else if (!/^[A-Za-z][A-Za-z0-9]*$/.test(reqdoceng)) {
      this.setState({ reqdocengerror: 'Only english alphabets are allowed' });
    }
    else if (reqdoceng.length < 2) {
      this.setState({ reqdocengerror: 'Name should be at least 2 characters' });
    }
    else {
      this.setState({ reqdocengerror: '' });
    }
    this.setState({ reqdoceng: reqdoceng });
  }
  updateArabicServiceCenterName = (servicenamearab) => {
    if (servicenamearab.length == 0 || servicenamearab == '') {
      this.setState({ servicenamearaberror: 'Required' });
    }
    else if (!/^[\u0621-\u064A\040]+$/.test(servicenamearab)) {
      this.setState({ servicenamearaberror: 'يسمح فقط الحروف الهجائية العربية' });
    }
    else if (servicenamearab.length < 2) {
      this.setState({ servicenamearaberror: 'يجب ألا يقل الاسم عن حرفين' });
    }
    else {
      this.setState({ servicenamearaberror: '' });
    }
    this.setState({ servicenamearab: servicenamearab });
  }
  reqdocarabcheck = (reqdocarab) => {
    if (reqdocarab.length == 0 || reqdocarab == '') {
      this.setState({ reqdocaraberror: 'Required' });
    }
    else if (!/^[\u0621-\u064A\040]+$/.test(reqdocarab)) {
      this.setState({ reqdocaraberror: 'يسمح فقط الحروف الهجائية العربية' });
    }
    else if (reqdocarab.length < 2) {
      this.setState({ reqdocaraberror: 'يجب ألا يقل الاسم عن حرفين' });
    }
    else {
      this.setState({ reqdocaraberror: '' });
    }
    this.setState({ reqdocarab: reqdocarab });
  }
  checkexpire = (data) => {
    console.log(data.length);
    if (data.length == 2) {
      data = data + "/";
    }
    if (data.length > 7) {
      this.setState({ expiryDateerror: "Required (MM/YYYY)" });
    }
    if (data.length <= 7) {
      this.setState({ expiryDateerror: "" });
    }
    this.setState({ carddate: data });
  };
  priceperuser = (currency) => {
    for (let i = 0; i < this.state.getsubslist.paymentPlan.prices.length; i++) {
      if (currency == this.state.getsubslist.paymentPlan.prices[i].currency) {
        return this.state.getsubslist.paymentPlan.prices[i].perUserPrice + ' ' + currency + ' ';
      }
    }
  }
  pricepersms = (currency) => {
    for (let i = 0; i < this.state.getsubslist.paymentPlan.prices.length; i++) {
      if (currency == this.state.getsubslist.paymentPlan.prices[i].currency) {
        return this.state.getsubslist.paymentPlan.prices[i].perSMSPrice + ' ' + currency + ' ';
      }
    }
  }
  // getStatictics(startDate,endDate){
  //   let token = localStorage.getItem('sessionToken')
  //   var sDate=startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();
  //   var eDate=endDate.getFullYear()+'-'+(endDate.getMonth()+1)+'-'+endDate.getDate();
  //   fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/dashboard/statistics', {
  //     method: 'POST',
  //     body:JSON.stringify({
  //       "branchIdList": [],
  //       "endDate": endDate,
  //       "startDate": startDate
  //     }),
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'Authorization': `Bearer ${token}`
  //     },
  //   })
  //   .then(response => response.json())
  //   .then(responsejson => {
  //     if(responsejson.applicationStatusCode==0)
  //     {
  //       var localLine={
  //         labels: [],
  //           datasets: [
  //             {
  //               borderColor: gradient,
  //               pointRadius: 0,
  //               fill: false,
  //               borderWidth: 2,
  //               hoverBackgroundColor: 'rgba(255,99,132,0.4)',
  //               hoverBorderColor: 'rgba(255,99,132,1)',
  //               data: []
  //             }
  //           ]
  //       }
  //       if(responsejson.values!=null)
  //       {
  //         for(var i=0;i<responsejson.values.length;i++)
  //         {
  //           localLine.labels.push(responsejson.values[i].key)
  //           localLine.datasets[0].data.push(responsejson.values[i].value)
  //         } 
  //         var t=0
  //         for(var j=0;j<localLine.datasets[0].data.length;j++)
  //         { 

  //           t=t + parseInt(localLine.datasets[0].data[j])
  //         }
  //         this.setState({
  //           line:localLine,
  //           totalTempTickets:t
  //         })  
  //       }

  //     }


  //   })
  //   // let token = localStorage.getItem('sessionToken')
  //   // fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getStatistics', {
  //   //   method: 'POST',
  //   //   body:JSON.stringify({
  //   //     "idList": [this.id],
  //   //     "endDate": endDate,
  //   //     "startDate": startDate
  //   //   }),
  //   //   headers: {
  //   //     'Content-Type': 'application/json',
  //   //     'Authorization': `Bearer ${token}`
  //   //   },
  //   // })
  //   // .then(response => response.json())
  //   // .then(responsejson => {
  //   //   if(responsejson.applicationStatusCode==0)
  //   //   {
  //   //     var localLine={ 
  //   //         labels: [],
  //   //         datasets: [
  //   //           {
  //   //             borderColor: gradient,
  //   //             pointRadius: 0,
  //   //             fill: false,
  //   //             borderWidth: 2,
  //   //             hoverBackgroundColor: 'rgba(255,99,132,0.4)',
  //   //             hoverBorderColor: 'rgba(255,99,132,1)',
  //   //             data: []
  //   //           }
  //   //         ]
  //   //     }
  //   //     if(responsejson.values!=null)
  //   //     {
  //   //       var tempSatisfied=0
  //   //       var tempAvg=0
  //   //       var tempPoor=0
  //   //       for(var i=0;i<responsejson.values.length;i++)
  //   //       {
  //   //         if(responsejson.values[i].key=='SATISFIED' || responsejson.values[i].key=='AVERAGE'|| responsejson.values[i].key=='POOR' )
  //   //         {
  //   //           if(responsejson.values[i].key=='SATISFIED')
  //   //           {
  //   //             tempSatisfied=parseInt(responsejson.values[i].value)
  //   //             this.state.pie.datasets[0].data[0]=parseInt(responsejson.values[i].value);
  //   //             this.setState({
  //   //               staisfied:parseInt(responsejson.values[i].value)
  //   //             })
  //   //           }
  //   //           if(responsejson.values[i].key=='AVERAGE')
  //   //           {
  //   //             tempAvg=parseInt(responsejson.values[i].value);
  //   //             this.state.pie.datasets[0].data[1]=parseInt(responsejson.values[i].value);
  //   //             this.setState({
  //   //               average:parseInt(responsejson.values[i].value)
  //   //             })
  //   //           }
  //   //           if(responsejson.values[i].key=='POOR')
  //   //           {
  //   //             tempPoor=parseInt(responsejson.values[i].value);
  //   //             this.state.pie.datasets[0].data[2]=parseInt(responsejson.values[i].value)
  //   //             this.setState({
  //   //               poor:parseInt(responsejson.values[i].value)
  //   //             })
  //   //           }
  //   //         }
  //   //         else
  //   //         {
  //   //           localLine.labels.push(responsejson.values[i].key)
  //   //           localLine.datasets[0].data.push(responsejson.values[i].value)
  //   //         }
  //   //       } 
  //   //       var t=0
  //   //       for(var j=0;j<localLine.datasets[0].data.length;j++)
  //   //       { 

  //   //         t=t + parseInt(localLine.datasets[0].data[j])
  //   //       }
  //   //       this.setState({
  //   //         line:localLine,
  //   //         totalTempTickets:t,
  //   //         pie:this.state.pie
  //   //       }) 
  //   //       var total=tempAvg+tempSatisfied+tempPoor
  //   //       this.setState({
  //   //         totalTempRatings:total
  //   //       }) 
  //   //     }

  //   //   }


  //   // })
  //   }
  // hamza code 


  handleChange(e) {
    const target = e.target;
    const name = target.name;
    const value = target.value;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(e) {
    this.setState({ name: this.state.modalInputName });
    this.modalClose();
  }

  modalOpen() {
    this.setState({ modalPay: true });
  }

  modalClose() {
    this.setState({
      modalInputName: "",
      modalPay: false
    });
  }

  onValueChange(event) {
    console.log(event.target.value)
    this.setState({
      selectedOption: event.target.value
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress: 0
          })}
        />
        <DefaultHeader />
        {(this.state.isShowServiceCenterProfilePrev == true && this.state.getprofilelist.length != 0) &&
          <Row className="mb-3 hidden">
            <Col sm="7">
              <div className="float-left mr-md-3">
                <span className="badge bg-white text-primary font-2xl">
                  {this.state.profileimg != '' && <img style={{ width: 50, height: 50, objectFit: 'contain' }}
                    src={"data:image/png;base64," + this.state.profileimg} />}
                  {this.state.profileimg == '' && <span className="badge badge-light font-2xl p-4 w-100 mt-1">&nbsp;</span>}
                </span>
              </div>
              <div className="float-left mr-md-5">
                <h4 className="mb-1">{this.state.getprofilelist.serviceCenter.serviceCenterName}</h4>
                <p className="text-light-grey"><i className="icon2-location text-light-grey"></i> Location here. <Link to="#">Open Map</Link></p>
              </div>
              <div className="float-left mr-md-5">
                <p className="text-light-grey mb-1">Created on</p><p>{moment(this.state.getprofilelist.serviceCenter.createdTime).format('DD-MM-YYYY')}</p>
              </div>
              <div className="clearfix"></div>
            </Col>
            {(!global.centerid) && (
              <Col sm="5" className="text-right">
                {/* {this.state.isShowServiceCenterProfileEditPrev==true &&  <button className="btn btn-outline-secondary ml-2">Delete <i className="icon2-trash-remove"></i></button>} */}
                {this.state.isShowServiceCenterProfileEditPrev == true && <button className="btn btn-outline-secondary ml-2" onClick={() => this.closeModel()}>Edit <i className="icon2-edit-pencil"></i></button>}
              </Col>
            )}

          </Row>}


        {(this.state.isShowServiceCenterProfilePrev == true && this.state.getprofilelist.length != 0) &&
          <Row className="mb-3">
            <Col xs="12" sm="6" lg="3">
              <Card className="card-line">
                <CardBody>
                  <div className="widget-title">
                    <h2 className="text-warning ml-4 mb-0 font-weight-normal">{this.state.getprofilelist.noOfBranches}</h2>
                    <h5 className="font-weight-normal text-light-grey"><i className="icon2-branches"></i> Branches</h5>
                  </div>

                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="3">
              <Card className="card-line">
                <CardBody>
                  <div className="widget-title">
                    <h2 className="text-warning ml-4 mb-0 font-weight-normal">{this.state.getprofilelist.noOfCities}</h2>
                    <h5 className="font-weight-normal text-light-grey"><i className="icon2-branches"></i> Cities</h5>
                  </div>

                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="3">
              <Card className="card-line">
                <CardBody>
                  <div className="widget-title">
                    <h2 className="text-warning ml-4 mb-0 font-weight-normal">{this.state.getprofilelist.noOfAgents}</h2>
                    <h5 className="font-weight-normal text-light-grey"><i className="nav-icon icon2-username"></i> Agents</h5>
                  </div>

                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="3">
              <Card className="card-line">
                <CardBody>
                  <div className="widget-title">
                    <h2 className="text-warning ml-4 mb-0 font-weight-normal">{this.state.getprofilelist.noOfUsers}</h2>
                    <h5 className="font-weight-normal text-light-grey"><i className="nav-icon icon2-username"></i> Users</h5>
                  </div>

                </CardBody>
              </Card>
            </Col>
          </Row>
        }




        <Modal isOpen={this.state.addlarge} toggle={this.toggleLarge} className={'modal-lg ' + this.props.className}>
          {global.isShowArabicLanguage == false && <ModalHeader toggle={this.toggleLarge}>New Service
                  <div className="checkbox">
              <input type="checkbox" checked={this.state.kioskenabled} onChange={() => { this.setState({ kioskenabled: !this.state.kioskenabled }) }}></input>
              <label>&nbsp;KIOSK Only</label>
            </div>
          </ModalHeader>}
          {global.isShowArabicLanguage == true && <ModalHeader toggle={this.toggleLarge}>إضافة خدمة جديدة
                  <div className="checkbox">
              <input type="checkbox" checked={this.state.kioskenabled} onChange={() => { this.setState({ kioskenabled: !this.state.kioskenabled }) }}></input>
              <label>&nbsp;KIOSK Only</label>
            </div>
          </ModalHeader>}

          <ModalBody>

            <Row>
              <Col sm="2">
                <h6 className="font-weight-normal pt-3 text-light-grey">Service Code</h6>
              </Col>
              <Col sm="2">
                <FormGroup className="input-line">
                  <Input type="text" placeholder="R" value={this.state.servicecode} onChange={(e) => { this.setState({ servicecode: e.target.value }) }} />
                </FormGroup>
              </Col>
            </Row>

            <FormGroup className="input-line">
              {global.isShowArabicLanguage == false && <Label>Service Name (English)</Label>}
              {global.isShowArabicLanguage == true && <Label>(اسم الخدمة (بالإنجليزية</Label>}
              {global.isShowArabicLanguage == false && <Input type="text" placeholder="Service Name" value={this.state.servicenameeng} onChange={(e) => { this.updateEnglishServiceCenterName(e.target.value) }} />}
              {global.isShowArabicLanguage == true && <Input type="text" placeholder='اسم الخدمة' value={this.state.servicenameeng} onChange={(e) => { this.updateEnglishServiceCenterName(e.target.value) }} />}
              <p style={{ color: 'red' }}>{this.state.servicenameengerror}</p>


            </FormGroup>
            <FormGroup className="input-line">
              {global.isShowArabicLanguage == false && <Label>Service Name (Arabic)</Label>}
              {global.isShowArabicLanguage == true && <Label>(اسم الخدمة (بالعربية</Label>}
              {global.isShowArabicLanguage == false && <Input type="text" placeholder="Service Name" value={this.state.servicenamearab} onChange={(e) => this.updateArabicServiceCenterName(e.target.value)} />}
              {global.isShowArabicLanguage == true && <Input type="text" placeholder='اسم الخدمة' value={this.state.servicenamearab} onChange={(e) => this.updateArabicServiceCenterName(e.target.value)} />}
              <p style={{ color: 'red' }}>{this.state.servicenamearaberror}</p>
            </FormGroup>



            <Row>
              <Col sm="5">
                <FormGroup className="input-line">
                  {global.isShowArabicLanguage == false && <Label>Requirements (English)</Label>}
                  {global.isShowArabicLanguage == true && <Label>(المتطلبات (بالإنجليزية</Label>}
                  {global.isShowArabicLanguage == false && <Input type="text" placeholder="Requirements" value={this.state.reqdoceng} onChange={(e) => { this.reqdocengcheck(e.target.value) }} />}
                  {global.isShowArabicLanguage == true && <Input type="text" placeholder="ملفات مطلوبه" value={this.state.reqdoceng} onChange={(e) => { this.reqdocengcheck(e.target.value) }} />}
                  <p style={{ color: 'red' }}>{this.state.reqdocengerror}</p>

                </FormGroup>
              </Col>
              <Col sm="5">
                <FormGroup className="input-line">
                  {global.isShowArabicLanguage == false && <Label>Requirements (Arabic)</Label>}
                  {global.isShowArabicLanguage == true && <Label>(المتطلبات (بالعربية</Label>}
                  {global.isShowArabicLanguage == false && <Input type="text" placeholder="Requirements" value={this.state.reqdocarab} onChange={(e) => this.reqdocarabcheck(e.target.value)} />}
                  {global.isShowArabicLanguage == true && <Input type="text" placeholder="ملفات مطلوبه" value={this.state.reqdocarab} onChange={(e) => this.reqdocarabcheck(e.target.value)} />}
                  <p style={{ color: 'red' }}>{this.state.reqdocaraberror}</p>
                </FormGroup>
              </Col>
              <Col sm="2" className="pt-3">
                <Button outline color="warning" disabled={(this.state.reqdoceng != '' && this.state.reqdoceng != '') ? false : true} onClick={this.addreqdoc}><i className="icon2-check-tick"></i> Add</Button>
              </Col>
            </Row>

            {this.state.reqlist.map((item, index) => (
              <Alert color="light" className="theme-alert" isOpen={this.state.visible} toggle={this.onDismiss}>

                <div className="w-100">{item.requirementName} (English) - {item.requirementNameArabic} (Arabic) <Link className="float-right text-light-grey" onClick={() => { this.deletereqdoc(index) }}>x</Link></div>

              </Alert>
            ))}


          </ModalBody>
          <ModalFooter>
            <Button outline color="primary" onClick={this.toggleLarge}>Cancel</Button>
            <Button outline color="primary" onClick={this.resetserviceadd}>Reset</Button>
            <Button color="primary" onClick={() => { this.toggleLarge(); this.addservice(0) }}>Save</Button>
          </ModalFooter>
        </Modal>

        {this.state.isShowServiceCenterProfilePrev == true &&
          <div className="tabs-line">
            <Nav tabs>
              <NavItem>
                <NavLink className="nav-link" to="#1"
                  active={this.state.isManual ? true : false}
                  isActive={(match, location) => {
                    if (!this.state.isManual) {
                      return false;
                    }
                    else {
                      return true;
                    }
                  }}
                  onClick={() => this.setState({ isManual: true, isFAQ: false, isTicket: false, isConfig: false })}
                >
                  <h5 className="mb-0">Overview</h5>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="nav-link" to="#2"
                  active={this.state.isFAQ ? true : false}
                  isActive={(match, location) => {
                    if (!this.state.isFAQ) {
                      return false;
                    }
                    else {
                      return true;
                    }
                  }}
                  onClick={() => this.setState({ isFAQ: true, isTicket: false, isManual: false, isConfig: false })}
                >
                  <h5 className="mb-0">Subscription/Plan</h5>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="nav-link" to="#3"
                  active={this.state.isTicket}
                  isActive={() => {
                    if (!this.state.isTicket) {
                      return false;
                    }
                    else {
                      return true;
                    }
                  }}
                  onClick={() => this.setState({ isTicket: true, isFAQ: false, isManual: false, isConfig: false })}
                >
                  <h5 className="mb-0">Billing
          {/* <span className="badge badge-success ml-2">Pending</span> */}
                  </h5>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="nav-link" to="#4"
                  active={this.state.isConfig ? true : false}
                  isActive={(match, location) => {
                    if (!this.state.isConfig) {
                      return false;
                    }
                    else {
                      return true;
                    }
                  }}
                  onClick={() => this.setState({ isTicket: false, isFAQ: false, isManual: false, isConfig: true })}
                >
                  <h5 className="mb-0">Configurations</h5>
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent className="pt-4">
              {this.state.isManual == true &&
                <TabPane >
                  {(this.state.getprofilelist.length != 0) && (
                    <Row>
                      <Col sm="8">
                        <Card>
                          <CardBody>
                            <Row className="mb-3">
                              <Col sm="8">
                                <h4>Services . {this.state.getprofilelist.servicesList.length}</h4>
                              </Col>
                              <Col sm="4" className="text-right">
                                {this.state.isShowServiceCenterProfileEditPrev == true && <button className="btn btn-outline-warning ml-2 btn-sm" onClick={this.toggleLarge}><i className="icon2-addition-sign"></i> Add</button>}
                              </Col>
                            </Row>
                            {this.state.getprofilelist.servicesList.map((item, index) => (
                              <Card className="card-box" key={index}>
                                <CardHeader>
                                  {/* <p className="float-left text-warning">{item.serviceTAB.serviceName}</p> */}
                                  {(!global.centerid) && (
                                    <div className="card-header-actions float-right">
                                      {this.state.isShowServiceCenterProfileEditPrev == true && <Link to="#" className="text-light-grey" onClick={() => { this.toggleedit(item) }}><i className="icon2-edit-pencil"></i></Link>}
                                      {this.state.isShowServiceCenterProfileEditPrev == true && <Link to="#" className="text-light-grey ml-3" onClick={() => { this.deleteservice(item.serviceCenterServiceId) }}><i className="font-weight-bold icon2-cancel"></i></Link>}
                                    </div>
                                  )}
                                </CardHeader>
                                <CardBody>
                                  <h5 className="text-primary mb-3">{item.serviceTAB.serviceName}</h5>
                                  {(this.state.getprofilelist.averageServiceTimeList.length != 0) && (
                                    <p className="text-light-grey"><i className="icon2-wall-clock"></i> Average Service Time  {parseInt(this.state.getprofilelist.averageServiceTimeList[index].ast)} minutes</p>
                                  )}
                                  {global.isShowArabicLanguage == false && <p className="text-light-grey"><i className="icon2-file-text1"></i> Requirements  {item.serviceRequirements.length}</p>}
                                  {global.isShowArabicLanguage == true && <p className="text-light-grey"><i className="icon2-file-text1"></i> ملفات مطلوبه  {item.serviceRequirements.length}</p>}
                                  <ul className="list-dots blue-dots ml-3">
                                    {item.serviceRequirements.map((item, index) => (
                                      <li key={index}><a>{item.requirementName}</a></li>
                                    ))}
                                  </ul>
                                </CardBody>
                              </Card>
                            ))}
                          </CardBody>
                        </Card>
                      </Col>

                      <Col sm="4">
                        <Card>
                          <CardBody className="text-left">
                            <Row>
                              <Col sm="12">
                                <h3 className="text-light-grey font-weight-normal">{this.state.votes.totalNoOfVotes} <sub>Votes</sub></h3>
                                <ButtonDropdown style={{ float: 'right' }} id="card1" isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                                  <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                                    {this.state.selectedWeek}
                                  </DropdownToggle>
                                  <DropdownMenu right>

                                    <DropdownItem onClick={this.changeDays} >1 week</DropdownItem>
                                    <DropdownItem onClick={this.changeDays}>2 week</DropdownItem>
                                    <DropdownItem onClick={this.changeDays}>1 month</DropdownItem>
                                    <DropdownItem onClick={this.changeDays}>6 month</DropdownItem>

                                  </DropdownMenu>
                                </ButtonDropdown>
                                <h5>Overall Performance</h5>
                              </Col>
                              <Col sm="12">
                                <div className="chart-wrapper line-graph" >
                                  <Line data={this.state.line}
                                    options={{
                                      legend: {
                                        display: false
                                      },
                                      scales: {
                                        yAxes: [{
                                          ticks: {
                                            fontColor: 'white'
                                          },
                                          gridLines: {
                                            display: false
                                          },
                                        }],
                                        xAxes: [{
                                          ticks: {
                                            fontColor: 'grey'
                                          },
                                          gridLines: {
                                            display: false
                                          },
                                        }]
                                      }
                                    }}
                                  />
                                </div>
                              </Col>
                            </Row>


                            <Row>
                              <Col sm="4" className="text-center f20"><i className="icon2-smile text-success"></i> {this.state.staisfied}</Col>
                              <Col sm="4" className="text-center f20"><i className="icon2-smile text-warning"></i> {this.state.average}</Col>
                              <Col sm="4" className="text-center f20"><i className="icon2-smile text-danger"></i> {this.state.poor}</Col>
                            </Row>
                          </CardBody>
                        </Card>
                        <Card className="mb-0 card-primary">
                          <CardBody className="text-left">
                            <p className="text-white"><i className="icon2-email-envelop text-warning mr-2"></i> {this.state.getprofilelist.serviceCenter.email}</p>
                            <p className="text-white mb-0"><i className="icon2-phone-call text-warning mr-2"></i> {this.state.getprofilelist.serviceCenter.phoneNumber}</p>
                          </CardBody>
                        </Card>
                      </Col>


                    </Row>
                  )}
                </TabPane>}
              {this.state.isFAQ == true &&
                <TabPane>
                  {(this.state.getsubslist.length != 0) && (
                    <Card className="mb-0">
                      <CardBody>
                        <Row className="mb-3 hidden">
                          <Col sm="6">
                            <div className="float-left mr-3 mt-3">
                              <i className="icon2-diamond-shape text-warning font-3xl"></i>
                            </div>
                            <div className="float-left mr-3">
                              <p className="text-light-grey m-0">Current Plan</p>
                              <h4 className="mb-1">{this.state.getsubslist.paymentPlan.planName} &nbsp;
                  {(this.state.getsubslist.subscriptionStatus == 'ACTIVE') && (
                                  <sup className="up-dot"><i className="icon2-ellipse"></i> </sup>
                                )}
                                {(this.state.getsubslist.subscriptionStatus != 'ACTIVE') && (
                                  <sup className="up-dot"><i className="icon2-ellipse red"></i> </sup>
                                )}
                              </h4>
                              <p className="text-light-grey m-0">Added on {moment(this.state.getsubslist.createdOn).format('DD-MM-YYYY')}</p>
                            </div>
                            <div className="float-left">
                              <p className="text-warning m-0 mt-3 font-weight-normal">Expires on {moment(this.state.getsubslist.expiredOn).format('DD-MM-YYYY')}
                                {(this.state.getsubslist.subscriptionStatus != 'ACTIVE') && (
                                  <Badge color="danger" className="ml-2">Expired</Badge>
                                )}
                              </p>
                            </div>
                            <div className="clearfix"></div>
                          </Col>
                          {(!global.centerid) && (
                            <Col sm="6" className="text-right">
                              {this.state.isShowServiceCenterProfileEditPrev == true && <Button color="warning" onClick={() => { this.updatesubscription() }}>Upgrade</Button>}
                            </Col>
                          )}

                        </Row>
                        <Row>
                          <Col sm="8">
                            <Row className="mb-3">
                              <Col className="pr-1">
                                <Card className="card-line height-card">
                                  <CardBody>
                                    <Row>
                                      <Col sm="2">
                                        <div className="widget-icon">
                                          <i className="icon2-branches text-warning font-2xl"></i>
                                        </div>
                                      </Col>
                                      <Col sm="12 pl-0 pr-0">
                                        <div className="widget-title">
                                          <p className="mb-1 text-light-grey no-wrap">Branches</p>
                                          <h6 className="mb-0">{this.state.getprofilelist.noOfBranches} <small className="font-xss text-light-grey">Used</small> / {this.state.getsubslist.noOfBranches - this.state.getprofilelist.noOfBranches} <small className="font-xss text-light-grey">Remaining</small></h6>
                                        </div>
                                      </Col>
                                    </Row>
                                  </CardBody>
                                </Card>
                              </Col>
                              <Col className="pr-1 pl-0 ">
                                <Card className="card-line height-card">
                                  <CardBody>
                                    <div className="row">
                                      <div className="col-2">
                                        <div className="widget-icon">
                                          <i className="icon2-username text-warning font-2xl"></i>
                                        </div>
                                      </div>
                                      <div className="col-12 pl-0 pr-0">
                                        <div className="widget-title">
                                          <p className="mb-1 text-light-grey text-truncate">No. of Users/Branch</p>
                                          <h6 className="mb-0">{this.state.getsubslist.noOfUsersPerBranch}</h6>
                                        </div>
                                      </div>
                                    </div>
                                  </CardBody>
                                </Card>
                              </Col>
                              <Col className="pl-0 pr-1 ">
                                <Card className="card-line  height-card">
                                  <CardBody>

                                    <div className="row">
                                      <div className="col-2">
                                        <div className="widget-icon">
                                          <i className="icon2-recipt text-warning font-2xl"></i>
                                        </div>
                                      </div>
                                      <div className="col-12 pl-0 pr-0">
                                        <div className="widget-title">
                                          <p className="mb-1 text-light-grey no-wrap">SMS Quota </p>
                                          <h6 className="mb-0">{this.state.getsubslist.noOfSMS}</h6>
                                        </div>
                                      </div>
                                    </div>

                                  </CardBody>
                                </Card>
                              </Col>
                              <Col className="pl-0 pr-1">
                                <Card className="card-line height-card">
                                  <CardBody>

                                    <div className="row">
                                      <div className="col-2">
                                        <div className="widget-icon">
                                          <i className="icon2-payments-card text-warning font-2xl"></i>
                                        </div>
                                      </div>
                                      <div className="col-12 pl-0 pr-0">
                                        <div className="widget-title">
                                          <p className="mb-1 text-light-grey no-wrap">No. of Services </p>
                                          <h6 className="mb-0">{this.state.servicesCount} <small className="font-xss text-light-grey">Used</small> / {this.state.getsubslist.paymentPlan.noOfServices - this.state.servicesCount} <small className="font-xss text-light-grey">Remaining</small></h6>
                                        </div>
                                      </div>
                                    </div>

                                  </CardBody>
                                </Card>
                              </Col>
                            </Row>
                            <Row>
                              <Col sm="6">
                                <h5 className="mb-3"><i className="icon2-align-left"></i> Additional Features</h5>
                                <div className="checkbox mb-2">
                                  <input type="checkbox" id="additional1" name="Additional-Features" value="additional1" checked={this.state.getsubslist.paymentPlan.kioskEnabled}></input>
                                  <label htmlFor="additional1">Kiosk Enabled</label>
                                </div>
                                <div className="checkbox mb-2">
                                  <input type="checkbox" id="additional2" name="Additional-Features" value="additional2" checked={this.state.getsubslist.paymentPlan.advanceReportingEnabled}></input>
                                  <label htmlFor="additional2">Advanced Reporting</label>
                                </div>
                                <div className="checkbox mb-2">
                                  <input type="checkbox" id="additional3" name="Additional-Features" value="additional3" checked={this.state.getsubslist.paymentPlan.easyPaymentEnabled}></input>
                                  <label htmlFor="additional3">Easy Payments</label>
                                </div>
                                <p>Free SMS Quota <b>{this.state.getsubslist.noOfSMS} SMS</b></p>
                                <ul className="list-dots blue-dots">
                                  <li>
                                    <a>
                                      {this.priceperuser(this.state.getsubslist.currency)}
                        Price per User
                      </a>
                                  </li>
                                  <li>
                                    <a>
                                      {this.pricepersms(this.state.getsubslist.currency)}
                        Price per SMS
                      </a>
                                  </li>
                                </ul>
                              </Col>
                              <Col sm="6">
                                <h5 className="mb-3"><i className="icon2-tools-gear"></i> Support/Maintenance</h5>
                                <div className="badge badge-info mb-2">Basic Support</div>
                                <ul className="list-dots blue-dots mb-5">
                                  <li><a>
                                    {this.state.getsubslist.paymentPlan.responseTimeInOffice[0]}{this.state.getsubslist.paymentPlan.responseTimeInOffice[1]} Hours Response Time In Office</a></li>
                                  <li><a>
                                    {this.state.getsubslist.paymentPlan.responseTimeOutOffice[0]}{this.state.getsubslist.paymentPlan.responseTimeOutOffice[1]} Hours Out of Office Response Time</a></li>
                                </ul>
                                <h6 className="text-primary">Additional Support Terms</h6>
                                <ul className="list-dots blue-dots">
                                  {(this.state.getsubslist.paymentPlan.dedicatedAccountManager) && (
                                    <li><a>Dedicated Account Manager</a></li>
                                  )}
                                  {(this.state.getsubslist.paymentPlan.prioritySupport) && (
                                    <li><a>Priority Support</a></li>
                                  )}
                                  {(this.state.getsubslist.paymentPlan.remoteDedicatedSupport) && (
                                    <li><a>Remote Dedicated Support</a></li>
                                  )}
                                  {(this.state.getsubslist.paymentPlan.fullDaySupport) && (
                                    <li><a>24Hr Support</a></li>
                                  )}
                                </ul>
                              </Col>
                            </Row>
                          </Col>
                          <Col sm="4">
                            <Card>
                              <CardBody>
                                <h5 className="mb-3 d-flex"><i className="icon2-order-summary font-3xl"></i><span className="pt-1">Add Ons</span></h5>
                                <h6>Payment Method</h6>
                                <Table responsive className="table table-borderless table-sm font-sm">
                                  <thead className="b-b-1">
                                    <tr>
                                      <th colSpan="2" className="pb-3">
                                        <small className="text-success">Recently Used</small><br></br>
                                        {this.state.getsubslist.bankCard?.paymentMethod?.paymentMethodName == "VISA" && (
                                          <img src={require('../assets/img/visa.png')} style={{ objectFit: 'contain' }} />
                                        )}
                                        {this.state.getsubslist.bankCard?.paymentMethod?.paymentMethodName == "MasterCard" && (
                                          <img src={require('../assets/img/master.png')} style={{ objectFit: 'contain' }} />
                                        )}
                                        {this.state.getsubslist.bankCard?.paymentMethod?.paymentMethodName == "Paypal" && (
                                          <img src={require('../assets/img/paypal.png')} style={{ objectFit: 'contain' }} />
                                        )} XXXX {this.state.getsubslist.bankCard?.cardNumber?.substr(this.state.getsubslist.bankCard?.cardNumber?.length - 4)}
                                      </th>
                                      <th className="text-right pb-3">
                                        {this.state.isShowServiceCenterProfileEditPrev == true && <Button color="warning" onClick={e => this.modalOpen(e)} >Change</Button>}
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td colSpan="3">
                                        <h6 className="mb-0">Branches</h6>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td width="80">

                                        <FormGroup className="input-line">
                                          <InputGroup className="input-line input-group-sm">
                                            {(!global.centerid) && (
                                              <InputGroupAddon addonType="prepend" onClick={() => {
                                                if (this.state.noofbranches != 0) {
                                                  this.setState({ noofbranches: this.state.noofbranches - 1 })
                                                }
                                              }} >
                                                <Link><i className="icon2-remove-circle-outline text-dark"></i></Link>
                                              </InputGroupAddon>
                                            )}
                                            <input className="form-control text-center" disabled type="text" min="0" value={this.state.noofbranches} onChange={(e) => {
                                              this.setState({ noofbranches: e.target.value })
                                            }}></input>
                                            {(!global.centerid) && (
                                              <InputGroupAddon addonType="prepend" onClick={() => { this.setState({ noofbranches: this.state.noofbranches + 1 }) }}>
                                                <Link><i className="icon2-add_circle_outline1 text-dark"></i></Link>
                                              </InputGroupAddon>
                                            )}
                                          </InputGroup>
                                        </FormGroup>
                                      </td>
                                      <td>X AED {this.state.getsubslist.planSubscriptionType == 'ANNUALY' ? this.state.getsubslist.paymentPlan.prices[0].priceAnnually : this.state.getsubslist.paymentPlan.prices[0].priceMonthly}</td>
                                      <td>AED {(this.state.getsubslist.planSubscriptionType == 'ANNUALY' ? this.state.getsubslist.paymentPlan.prices[0].priceAnnually : this.state.getsubslist.paymentPlan.prices[0].priceMonthly) * this.state.noofbranches}</td>
                                    </tr>
                                    <tr>
                                      <td colSpan="3">
                                        <h6 className="mb-0">Users</h6>
                                      </td>
                                    </tr>
                                    <tr></tr>
                                    <tr>
                                      <td>
                                        <FormGroup className="input-line">
                                          <InputGroup className="input-line input-group-sm">
                                            {(!global.centerid) && (
                                              <InputGroupAddon addonType="prepend" onClick={() => {
                                                if (this.state.noofusersp != 0) {
                                                  this.setState({ noofusersp: this.state.noofusersp - 1 })
                                                }
                                              }}>
                                                <Link><i className="icon2-remove-circle-outline text-dark"></i></Link>
                                              </InputGroupAddon>
                                            )}
                                            <input className="form-control text-center" type="text" disabled min="0" value={this.state.noofusersp} onChange={(e) => {
                                              this.setState({ noofusersp: e.target.value })
                                            }}></input>
                                            {(!global.centerid) && (
                                              <InputGroupAddon addonType="prepend" onClick={() => {
                                                this.setState({ noofusersp: this.state.noofusersp + 1 })
                                              }}>
                                                <Link><i className="icon2-add_circle_outline1 text-dark"></i></Link>
                                              </InputGroupAddon>
                                            )}

                                          </InputGroup>
                                        </FormGroup>
                                      </td>
                                      <td>X AED {this.state.getsubslist.paymentPlan.prices[0].perUserPrice}</td>
                                      <td>AED {this.state.getsubslist.paymentPlan.prices[0].perUserPrice * this.state.noofusersp}</td>
                                    </tr>
                                    <tr>
                                      <td colSpan="3">
                                        <h6 className="mb-0">SMSs</h6>
                                      </td>
                                    </tr>
                                    <tr></tr>
                                    <tr>
                                      <td>

                                        <FormGroup className="input-line">
                                          <InputGroup className="input-line input-group-sm">
                                            {(!global.centerid) && (
                                              <InputGroupAddon addonType="prepend" onClick={() => {
                                                if (this.state.noofsms != 0) {
                                                  this.setState({ noofsms: this.state.noofsms - 1 })
                                                }
                                              }}>
                                                <Link><i className="icon2-remove-circle-outline text-dark"></i></Link>
                                              </InputGroupAddon>
                                            )}
                                            <input className="form-control text-center" type="text" disabled min="0" value={this.state.noofsms}
                                              onChange={(e) => {
                                                this.setState({ noofsms: e.target.value })
                                              }}>
                                            </input>
                                            {(!global.centerid) && (
                                              <InputGroupAddon addonType="prepend" onClick={() => { this.setState({ noofsms: this.state.noofsms + 1 }) }}>
                                                <Link><i className="icon2-add_circle_outline1 text-dark"></i></Link>
                                              </InputGroupAddon>
                                            )}

                                          </InputGroup>
                                        </FormGroup>
                                      </td>
                                      <td>X AED {this.state.getsubslist.paymentPlan.prices[0].perSMSPrice}</td>
                                      <td>AED {this.state.getsubslist.paymentPlan.prices[0].perSMSPrice * this.state.noofsms}</td>
                                    </tr>
                                  </tbody>
                                  <tfoot className="b-t-1">
                                    <tr>
                                      <td className="pt-3" colSpan="2"><b>Order Total</b></td>
                                      <td></td>
                                    </tr>
                                    <tr>
                                      <td colSpan="2">Total before VAT (5%)</td>
                                      <td>AED {this.total((this.state.getsubslist.planSubscriptionType == 'ANNUALY' ? this.state.getsubslist.paymentPlan.prices[0].priceAnnually : this.state.getsubslist.paymentPlan.prices[0].priceMonthly) * this.state.noofbranches, this.state.getsubslist.paymentPlan.prices[0].perUserPrice * this.state.noofusersp, this.state.getsubslist.paymentPlan.prices[0].perSMSPrice * this.state.noofsms)}</td>
                                    </tr>
                                    <tr>
                                      <td colSpan="2"><b>Total</b></td>
                                      <td > <b> AED {this.tatalvtax((this.state.getsubslist.planSubscriptionType == 'ANNUALY' ? this.state.getsubslist.paymentPlan.prices[0].priceAnnually : this.state.getsubslist.paymentPlan.prices[0].priceMonthly) * this.state.noofbranches, this.state.getsubslist.paymentPlan.prices[0].perUserPrice * this.state.noofusersp, this.state.getsubslist.paymentPlan.prices[0].perSMSPrice * this.state.noofsms)}</b></td>
                                    </tr>
                                  </tfoot>
                                </Table>
                                {(!global.centerid) && (this.state.isShowServiceCenterProfileEditPrev == true) && (
                                  <Button block color="warning" onClick={() => { this.updatesubscription() }}>Checkout</Button>
                                )}
                              </CardBody>
                            </Card>
                          </Col>
                        </Row>
                      </CardBody>
                    </Card>
                  )}
                </TabPane>
              }
              {this.state.isTicket == true &&
                <TabPane>
                  <Row className="mb-3 hidden">
                    <Col sm="12">
                      <h3>Payment Methods
            {(!global.centerid) && (this.state.isShowServiceCenterProfileEditPrev == true) && (
                          <Link to="#" className="btn btn-warning btn-sm ml-2" onClick={() => { this.addpaymentmethod() }}><i className="icon2-addition-sign"></i> Add New</Link>
                        )}
                      </h3>
                    </Col>
                  </Row>
                  <Card>
                    <CardBody className="p-0">
                      <Table responsive className="bg-white m-0 table-light table-card table-hover">
                        <thead>
                          <tr >
                            <th width="30"></th>
                            <th className="text-left" width="27%">Card</th>
                            <th width="30%">Card Type</th>
                            <th>Network</th>
                            <th width="50"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.getpaymentmethodlist.map((item, index) => (
                            <tr key={index}>
                              <td></td>
                              <td>
                                {item.cardNumber}<br></br>
                                <small className="text-success">Valid</small>
                              </td>
                              <td className="text-center">{item.cardType} Card</td>
                              <td className="text-center">
                                {item.paymentMethod.paymentMethodName == "VISA" && (
                                  <img src={require('../assets/img/visa.png')} style={{ objectFit: 'contain' }} />
                                )}
                                {item.paymentMethod.paymentMethodName == "MasterCard" && (
                                  <img src={require('../assets/img/master.png')} style={{ objectFit: 'contain' }} />
                                )}
                                {item.paymentMethod.paymentMethodName == "Paypal" && (
                                  <img src={require('../assets/img/paypal.png')} style={{ objectFit: 'contain' }} />
                                )}
                              </td>
                              {this.state.isShowServiceCenterProfileEditPrev == true && <td className="text-center">
                                {(!global.centerid) && (
                                  <Dropdown id={index} isOpen={this.state.dropdowninvoice == index} toggle={() => {
                                    if (this.state.dropdowninvoice != index) {
                                      this.setState({ dropdowninvoice: index });
                                    }
                                    else {
                                      this.setState({ dropdowninvoice: null });
                                    }
                                  }}>
                                    <DropdownToggle color="transparent">
                                      <i className="f20 icon2-menu-options text-light-grey"></i>
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                      <DropdownItem onClick={() => {
                                        item.expiryYear = '' + item.expiryYear;
                                        this.setState({
                                          cardnumber: item.cardNumber,
                                          carddate: item.expiryMonth < 10 ? '0' + item.expiryMonth + '/' + item.expiryYear[2] + item.expiryYear[3] : item.expiryMonth + '/' + item.expiryYear[2] + item.expiryYear[3],
                                          cardcvv: item.cardCvv,
                                          cardholdername: item.cardTitle,
                                          cardid: item.bankCardId,
                                        });
                                        this.editpaymentmethod()
                                      }}>
                                        Edit
                        </DropdownItem>
                                      <DropdownItem onClick={() => {
                                        item.expiryYear = '' + item.expiryYear;
                                        this.setState({
                                          cardnumber: item.cardNumber,
                                          carddate: item.expiryMonth < 10 ? '0' + item.expiryMonth + '/' + item.expiryYear[2] + item.expiryYear[3] : item.expiryMonth + '/' + item.expiryYear[2] + item.expiryYear[3],
                                          cardcvv: item.cardCvv,
                                          cardholdername: item.cardTitle,
                                          cardid: item.bankCardId,
                                        });
                                        this.deletepayment(item.bankCardId)
                                      }}>
                                        Delete
                        </DropdownItem>
                                    </DropdownMenu>
                                  </Dropdown>
                                )}

                              </td>}
                            </tr>
                          ))}
                        </tbody>
                      </Table>
                    </CardBody>
                  </Card>

                  <Row className="mb-3 hidden">
                    <Col sm="6">
                      <h3>Billing History</h3>
                    </Col>
                  </Row>
                  <Card className="mb-0">
                    <CardBody className="p-0">
                      <Table responsive className="bg-white m-0 table-light table-card table-hover">
                        <thead>
                          <tr >
                            <th>Status</th>
                            <th>Type</th>
                            <th>Invoice#</th>
                            <th>Created </th>
                            <th>Expires</th>
                            <th className="text-left">Service Center</th>
                            <th>Amount</th>
                            <th>Due Date</th>
                            <th>Notify</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.getinvoicelist.map((item, index) => (
                            <tr key={index}>
                              <td className="text-center">
                                <span className="badge badge-success" style={{ color: item.invoiceStatus == 'UNPAID' ? '#f72c39' : item.invoiceStatus == 'PAID' ? '#06cf99' : '#20a8d8', backgroundColor: item.invoiceStatus == 'UNPAID' ? '#ffe1e3' : item.invoiceStatus == 'PAID' ? '#06cf9924' : '#20a8d833' }}>{item.invoiceStatus}</span>
                              </td>
                              <td className="text-center">{item.invoiceType}</td>
                              <td className="text-center">{item.invoiceNumber}</td>
                              <td className="text-center">{moment(item.createdOn).format('DD-MM-YYYY')}</td>
                              <td className="text-center">{moment(item.expiredOn).format('DD-MM-YYYY')}</td>
                              <td>{item.serviceCenterSubscription.serviceCenter.serviceCenterName}<br></br><small className="text-light-grey"><i className="icon2-location"></i> {item.serviceCenterSubscription.serviceCenter.country}</small></td>
                              <td className="text-center">${item.amount}</td>
                              <td className="text-center">{moment(item.dueDate).format('DD-MM-YYYY')}</td>
                              <td className="text-center">{moment(item.issuedOn).format('DD-MM-YYYY')}</td>
                            </tr>
                          ))}

                        </tbody>
                      </Table>
                    </CardBody>
                  </Card>

                </TabPane>
              }



              {this.state.isConfig == true &&
                <TabPane>
                  <Row className="mb-3 hidden">
                    <Col sm="6">
                      <h3> Fast Past Management
            <AppSwitch className={'mx-1'} variant={'3d'} color={'success'} defaultChecked disabled={this.state.isShowServiceCenterProfileEditPrev} /></h3>
                    </Col>
                    <Col sm="6" className="text-right">
                      <div className="float-right">
                        {this.state.isShowServiceCenterProfileEditPrev != true && <button className="btn btn-outline-warning"
                          disabled={this.state.isDisableResetConfig}
                          onClick={() => this.assignDefaultConfig(0)}>Reset to Default</button>}
                        {this.state.isShowServiceCenterProfileEditPrev != true && <button className="btn btn-warning ml-2"
                          disabled={this.state.isDisableSaveConfig}
                          onClick={() => this.updateConfig(0)}>Save</button>}
                      </div>
                      <div className="clearfix"></div>
                    </Col>
                  </Row>
                  <Card>
                    <CardBody>
                      <Row>
                        <Col sm="4">
                          <FormGroup className="input-line">
                            <Label className="text-light-grey" htmlFor="exampleEmail" >Fees</Label>
                            <Row>
                              <Col sm="6">
                                <FormGroup className="input-line">
                                  <Input type="text" placeholder="Amount"
                                    value={this.state.fastPass} onChange={(e) => this.updateFastAmount(e.target.value)} />
                                  <ValidationMessage valid={this.state.fastPassValid} message={this.state.fastPassErrorMsg.fastPass} />
                                </FormGroup>
                              </Col>
                              <Col sm="6" className="pl-0">
                                {(global.centerid) && (
                                  <ButtonDropdown id="card1" isOpen={this.state.isShowCard1} toggle={() => { this.setState({ isShowCard1: !this.state.isShowCard1 }); }}>
                                    <DropdownToggle caret className="p-0 mb-0 text-warning font-xs wrap-initial line-h-normal" color="transparent">
                                      Emirati dirham (AED)
                              </DropdownToggle>
                                    <DropdownMenu right>
                                      <DropdownItem>Emirati dirham (AED)</DropdownItem>
                                    </DropdownMenu>
                                  </ButtonDropdown>
                                )}

                              </Col>
                            </Row>
                          </FormGroup>
                        </Col>
                        <Col sm="4">
                          <FormGroup className="input-line">
                            <Label className="text-light-grey" htmlFor="exampleEmail" >Service Center Percentage</Label>
                            <Row>
                              <Col sm="6">
                                <FormGroup className="input-line">
                                  <Input type="text" placeholder="Percentage"
                                    value={this.state.serviceCenterPercentage} onChange={(e) => this.updateServicePercentages(e.target.value)}
                                  />

                                </FormGroup>
                              </Col>
                              <Col sm="6">
                                %
                        </Col>
                            </Row>
                          </FormGroup>
                          <FormGroup className="input-line">
                            <Label className="text-light-grey" htmlFor="exampleEmail" >Service Center Amount</Label>
                            <Row>
                              <Col sm="6">
                                <Input type="text" disabled={!global.centerid} placeholder="Amount"
                                  value={this.state.serviceCenterAmount} readOnly />
                              </Col>
                              <Col sm="6">
                                {(global.centerid) && (
                                  <ButtonDropdown id="card1" isOpen={this.state.isShowCard2} toggle={() => { this.setState({ isShowCard2: !this.state.isShowCard2 }); }}>
                                    <DropdownToggle caret className="p-0 mb-0 text-warning font-xs wrap-initial line-h-normal" color="transparent">
                                      Emirati dirham (AED)
                                  </DropdownToggle>
                                    <DropdownMenu right>
                                      <DropdownItem>Emirati dirham (AED)</DropdownItem>
                                    </DropdownMenu>
                                  </ButtonDropdown>
                                )}
                              </Col>
                            </Row>
                          </FormGroup>
                        </Col>
                        <Col sm="4">
                          <FormGroup className="input-line">
                            <Label className="text-light-grey" htmlFor="exampleEmail" >Taboor Percentage</Label>
                            <Row>
                              <Col sm="6">
                                <FormGroup className="input-line">
                                  <Input type="text" placeholder="Percentage"
                                    value={this.state.taboorPercentage} onChange={(e) => this.updateTaboorPercentages(e.target.value)} />


                                </FormGroup>
                              </Col>
                              <Col sm="6">
                                %
                        </Col>
                            </Row>
                          </FormGroup>
                          <FormGroup className="input-line">
                            <Label className="text-light-grey" htmlFor="exampleEmail" >Taboor Amount</Label>
                            <Row>
                              <Col sm="6">
                                <Input type="text" disabled={!global.centerid} placeholder="Amount"
                                  value={this.state.taboorAmount} readOnly />
                              </Col>
                              <Col sm="6" className="pl-0">
                                {(global.centerid) && (
                                  <ButtonDropdown id="card1" isOpen={this.state.isShowCard3} toggle={() => { this.setState({ isShowCard3: !this.state.isShowCard3 }); }}>
                                    <DropdownToggle caret className="p-0 mb-0 text-warning font-xs wrap-initial line-h-normal" color="transparent">
                                      Emirati dirham (AED)
                            </DropdownToggle>
                                    <DropdownMenu right>
                                      <DropdownItem>Emirati dirham (AED)</DropdownItem>
                                    </DropdownMenu>
                                  </ButtonDropdown>
                                )}
                              </Col>
                            </Row>
                          </FormGroup>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                  <Row className="mb-3 hidden">
                    <Col sm="6">
                      <h3> Configurations</h3>
                    </Col>
                    <Col sm="6" className="text-right">
                      <div className="input-group float-right w-auto ml-2"></div>
                      <div className="float-right">
                        {this.state.isShowServiceCenterProfileEditPrev != true && <button
                          className="btn btn-outline-warning"
                          disabled={this.state.isDisableResetConfig}
                          onClick={() => this.assignDefaultConfig(1)}>Reset to Default</button>}
                        {this.state.isShowServiceCenterProfileEditPrev != true && <button
                          className="btn btn-warning ml-2"
                          disabled={this.state.isDisableSaveConfig}
                          onClick={() => this.updateConfig(1)} >Save</button>}
                      </div>
                      <div className="clearfix"></div>
                    </Col>
                  </Row>
                  <Card className="mb-0">
                    <CardBody className="p-0">
                      <Table responsive className="bg-white m-0 table-light table-card table-hover table-tbody table-v-middle">
                        {/* <thead>
                  <tr>
                    <th className="text-left">Config Item</th>
                    <th className="text-left" width="30%">Status</th>
                  </tr>
                  </thead> */}
                        <tbody>
                          <tr>
                            <td className="text-left">Fast Pass Stepout Allowed Count</td>
                            <td width="25%" className="text-right">
                              <FormGroup className="input-line input-group-sm">
                                <Input disabled={!global.centerid} type="text" placeholder="Time"
                                  value={this.state.fastPassCount} onChange={(e) => this.updateFastPassCount(e.target.value)} />
                                <ValidationMessage valid={this.state.fastPassCountValid} message={this.state.configErrorMsg.fastPassCount} />
                              </FormGroup>
                            </td>
                          </tr>
                          <tr>
                            <td className="text-left">Fast Pass Stepout Allowed Time</td>
                            <td className="text-right">
                              <FormGroup className="input-line input-group-sm">
                                <Input disabled={!global.centerid} type="text" placeholder="Time"
                                  value={this.state.fastPassTime} onChange={(e) => this.updateFastPassTime(e.target.value)} />
                                <ValidationMessage valid={this.state.fastPassTimeValid} message={this.state.configErrorMsg.fastPassTime} />
                              </FormGroup>
                            </td>
                          </tr>
                          <tr>
                            <td className="text-left">Normal Stepout Allowed Count</td>
                            <td className="text-right">
                              <FormGroup className="input-line input-group-sm">
                                <Input disabled={!global.centerid} type="text" placeholder="Count"
                                  value={this.state.normalPassCount} onChange={(e) => this.updateNormalPassCount(e.target.value)} />
                                <ValidationMessage valid={this.state.normalPassCountValid} message={this.state.configErrorMsg.normalPassCount} />
                              </FormGroup>
                            </td>
                          </tr>
                          <tr>
                            <td className="text-left">Normal Stepout Allowed Time</td>
                            <td className="text-right">
                              <FormGroup className="input-line input-group-sm">
                                <Input disabled={!global.centerid} type="text" placeholder="Time"
                                  value={this.state.normalPassTime} onChange={(e) => this.updateNormalPassTime(e.target.value)} />
                                <ValidationMessage valid={this.state.normalPassTimeValid} message={this.state.configErrorMsg.normalPassTime} />
                              </FormGroup>
                            </td>
                          </tr>
                        </tbody>
                      </Table>
                    </CardBody>
                  </Card>
                </TabPane>
              }

            </TabContent>
          </div>
        }

        <Modal isOpen={this.state.largeedit} toggle={this.toggleedit} className={'modal-lg ' + this.props.className}>
          {global.isShowArabicLanguage == false && <ModalHeader toggle={this.toggleedit}>New Service
          <div className="checkbox">
              <input type="checkbox" checked={this.state.kioskenabled} onChange={() => { this.setState({ kioskenabled: !this.state.kioskenabled }) }}></input>
              <label>&nbsp;KIOSK Only</label>
            </div>
          </ModalHeader>}
          {global.isShowArabicLanguage == true && <ModalHeader toggle={this.toggleLarge}>إضافة خدمة جديدة
          <div className="checkbox">
              <input type="checkbox" checked={this.state.kioskenabled} onChange={() => { this.setState({ kioskenabled: !this.state.kioskenabled }) }}></input>
              <label>&nbsp;KIOSK Only</label>
            </div>
          </ModalHeader>}


          <ModalBody>
            <Row>
              <Col sm="2">
                <h6 className="font-weight-normal pt-3 text-light-grey">Service Code</h6>
              </Col>
              <Col sm="2">
                <FormGroup className="input-line">
                  <Input type="text" placeholder="R" value={this.state.servicecode} onChange={(e) => { this.setState({ servicecode: e.target.value }) }} />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup className="input-line">
              {global.isShowArabicLanguage == false && <Label>Service Name (English)</Label>}
              {global.isShowArabicLanguage == true && <Label>(اسم الخدمة (بالإنجليزية</Label>}
              {global.isShowArabicLanguage == false && <Input type="text" placeholder="Service Name" value={this.state.servicenameeng} onChange={(e) => { this.updateEnglishServiceCenterName(e.target.value) }} />}
              {global.isShowArabicLanguage == true && <Input type="text" placeholder='اسم الخدمة' value={this.state.servicenameeng} onChange={(e) => { this.updateEnglishServiceCenterName(e.target.value) }} />}
              <p style={{ color: 'red' }}>{this.state.servicenameengerror}</p>
            </FormGroup>
            <FormGroup className="input-line">
              {global.isShowArabicLanguage == false && <Label>Service Name (Arabic)</Label>}
              {global.isShowArabicLanguage == true && <Label>(اسم الخدمة (بالعربية</Label>}
              {global.isShowArabicLanguage == false && <Input type="text" placeholder="Service Name" value={this.state.servicenamearab} onChange={(e) => this.updateArabicServiceCenterName(e.target.value)} />}
              {global.isShowArabicLanguage == true && <Input type="text" placeholder='اسم الخدمة' value={this.state.servicenamearab} onChange={(e) => this.updateArabicServiceCenterName(e.target.value)} />}
              <p style={{ color: 'red' }}>{this.state.servicenamearaberror}</p>
            </FormGroup>
            <Row>
              <Col sm="5">
                <FormGroup className="input-line">
                  {global.isShowArabicLanguage == false && <Label>Requirements (English)</Label>}
                  {global.isShowArabicLanguage == true && <Label>(المتطلبات (بالإنجليزية</Label>}
                  {global.isShowArabicLanguage == false && <Input type="text" placeholder="Requirements" value={this.state.reqdoceng} onChange={(e) => { this.reqdocengcheck(e.target.value) }} />}
                  {global.isShowArabicLanguage == true && <Input type="text" placeholder="ملفات مطلوبه" value={this.state.reqdoceng} onChange={(e) => { this.reqdocengcheck(e.target.value) }} />}
                  <p style={{ color: 'red' }}>{this.state.reqdocengerror}</p>
                </FormGroup>
              </Col>
              <Col sm="5">
                <FormGroup className="input-line">
                  {global.isShowArabicLanguage == false && <Label>Requirements (Arabic)</Label>}
                  {global.isShowArabicLanguage == true && <Label>(المتطلبات (بالعربية</Label>}
                  {global.isShowArabicLanguage == false && <Input type="text" placeholder="Requirements" value={this.state.reqdocarab} onChange={(e) => this.reqdocarabcheck(e.target.value)} />}
                  {global.isShowArabicLanguage == true && <Input type="text" placeholder="ملفات مطلوبه" value={this.state.reqdocarab} onChange={(e) => this.reqdocarabcheck(e.target.value)} />}
                  <p style={{ color: 'red' }}>{this.state.reqdocaraberror}</p>
                </FormGroup>
              </Col>
              {(!global.centerid) && (
                <Col sm="2" className="pt-3">
                  <Button outline color="warning" disabled={(this.state.reqdoceng != '' && this.state.reqdoceng != '') ? false : true} onClick={this.addreqdoc}><i className="icon2-check-tick"></i> Add</Button>
                </Col>
              )}

              {this.state.reqlist.map((item, index) => (
                <Alert color="light" className="theme-alert w-100" isOpen={this.state.visible} toggle={this.onDismiss}>

                  <div className="w-100">{item.requirementName} (English) - {item.requirementNameArabic} (Arabic) <Link className="float-right text-light-grey" onClick={() => { this.deletereqdoc(index) }}>x</Link></div>

                </Alert>
              ))}
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button outline color="primary" onClick={this.toggleedit}>Cancel</Button>
            <Button color="primary" onClick={() => { this.toggleedit(); this.addservice(this.state.serviceid) }}>Save</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.large} toggle={this.addpaymentmethod} className={'modal-lg ' + this.props.className}>
          <ModalHeader>Add Payments Method</ModalHeader>
          <ModalBody>
            <div className="payment-cards mb-5">
              <ul>
                <li className={this.state.paymentmethodid == 48 ? 'active' : ''} onClick={() => { this.setState({ paymentmethodid: 48 }) }}>
                  <a title="Visa" >
                    <img src={require('../assets/img/visa.png')} />
                  </a>
                </li>
                <li className={this.state.paymentmethodid == 47 ? 'active' : ''} onClick={() => { this.setState({ paymentmethodid: 47 }) }}>
                  <a title="Master Card">
                    <img src={require('../assets/img/master.png')} />
                  </a>
                </li>
                <li className={this.state.paymentmethodid == 46 ? 'active' : ''} onClick={() => { this.setState({ paymentmethodid: 46 }) }}>
                  <a title="PayPal" >
                    <img src={require('../assets/img/paypal.png')} />
                  </a>
                </li>
              </ul>
            </div>
            <FormGroup className="input-line">
              <Label className="text-light-grey">Card Number</Label>
              <Input type="number" placeholder="XXXX XXXX XXXX XXXX" value={this.state.cardnumber} onChange={(e) => { this.setState({ cardnumber: e.target.value }) }} />
            </FormGroup>
            <Row>
              <Col sm="6">
                <FormGroup className="input-line">
                  <Label className="text-light-grey">Expiry Date</Label>
                  <MaskedInput mask="11/11" name="expiry" placeholder="MM/YY"
                    value={this.state.carddate} style={{ width: '100%', paddingTop: 6, paddingBottom: 6, borderBottomStyle: 'solid', borderBottomWidth: 1, borderBottomColor: '#e4e7ea', color: '#5c6873' }}
                    onChange={(e) => { this.setState({ carddate: e.target.value }) }} />
                  <p style={{ color: 'red' }}>{this.state.expiryDateerror}</p>
                </FormGroup>
              </Col>
              <Col sm="6">
                <FormGroup className="input-line">
                  <Label className="text-light-grey">CVV</Label>
                  <Input type="number" placeholder="CVV" value={this.state.cardcvv} onChange={(e) => { this.setState({ cardcvv: e.target.value }) }} />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup className="input-line">
              <Label className="text-light-grey">Cardholder's Name</Label>
              <Input type="text" placeholder="Name" value={this.state.cardholdername} onChange={(e) => { this.setState({ cardholdername: e.target.value }) }} />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button outline color="primary" onClick={() => this.addpaymentmethod()}>Cancel</Button>
            <Button color="primary" onClick={() => { this.addpaymentmethod(); this.addpaymentinservice() }}>Add</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.largeeditpay} toggle={this.editpaymentmethod} className={'modal-lg ' + this.props.className}>
          <ModalHeader>Edit Payments Method</ModalHeader>
          <ModalBody>
            <div className="payment-cards mb-5">
              <ul>
                <li className="active">
                  <a title="Visa" >
                    <img src={require('../assets/img/visa.png')} />
                  </a>
                </li>
                <li>
                  <a title="Master Card">
                    <img src={require('../assets/img/master.png')} />
                  </a>
                </li>
                <li>
                  <a title="PayPal" >
                    <img src={require('../assets/img/paypal.png')} />
                  </a>
                </li>
              </ul>
            </div>
            <FormGroup className="input-line">
              <Label className="text-light-grey">Card Number</Label>
              <Input type="number" placeholder="XXXX XXXX XXXX XXXX" value={this.state.cardnumber} onChange={(e) => { this.setState({ cardnumber: e.target.value }) }} />
            </FormGroup>
            <Row>
              <Col sm="6">
                <FormGroup className="input-line">
                  <Label className="text-light-grey">Expiry Date</Label>
                  <MaskedInput mask="11/11" name="expiry" placeholder="MM/YY" style={{ width: '100%', paddingTop: 6, paddingBottom: 6, borderBottomStyle: 'solid', borderBottomWidth: 1, borderBottomColor: '#e4e7ea', color: '#5c6873' }}
                    value={this.state.carddate}
                    onChange={(e) => { this.setState({ carddate: e.target.value }) }} />
                </FormGroup>
              </Col>
              <Col sm="6">
                <FormGroup className="input-line">
                  <Label className="text-light-grey">CVV</Label>
                  <Input type="number" placeholder="CVV" value={this.state.cardcvv} onChange={(e) => { this.setState({ cardcvv: e.target.value }) }} />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup className="input-line">
              <Label className="text-light-grey">Cardholder's Name</Label>
              <Input type="text" placeholder="Name" value={this.state.cardholdername} onChange={(e) => { this.setState({ cardholdername: e.target.value }) }} />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button outline color="primary" onClick={() => this.editpaymentmethod()}>Cancel</Button>
            <Button color="primary" onClick={() => { this.editpaymentmethod(); this.updatepaymentmethod() }}>Save</Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.isShowEditModel} toggle={this.closeModel} className={'modal-lg ' + this.props.className}>
          <ModalHeader >Edit</ModalHeader>
          <ModalBody>
            <FormGroup className="input-line">
              <Label className="text-light-grey">Center Name (English)</Label>
              <Input type="text" placeholder="Service Center Name English"
                value={this.state.centername} onChange={(e) => this.setState({ centername: e.target.value })}
              />
            </FormGroup>
            <FormGroup className="input-line">
              <Label className="text-light-grey">Center Name (Arabic)</Label>
              <Input type="text" placeholder="Service Center Name Arabic"
                value={this.state.centernamearab} onChange={(e) => this.setState({ centernamearab: e.target.value })}
              />
            </FormGroup>
            <FormGroup className="input-line">
              <Label className="text-light-grey">Country</Label>
              <Input type="text" placeholder="Country"
                value={this.state.centercountry} onChange={(e) => this.setState({ centercountry: e.target.value })}
              />
            </FormGroup>
            <FormGroup className="input-line">
              <Label className="text-light-grey">Phone</Label>
              {/* <Row>
                  <Col sm="3">
                    <select className="form-control" onChange={this.changePhoneNo} >
                      {this.countries.map(country => (
                        <option value={country.code}>{country.code}</option>
                      ))}
                    </select>
                  </Col>
                  <Col sm="9"> */}
              <Input type="text" placeholder="Phone" autoComplete="username"
                value={this.state.centerphone} onChange={(e) => this.setState({ centerphone: e.target.value })}
              />
              {/* </Col>
                </Row> */}

            </FormGroup>
            <FormGroup className="input-line">
              <Label className="text-light-grey" htmlFor="exampleEmail" >Email</Label>

              <Input type="email" placeholder="Email" autoComplete="username" name="email" id="exampleEmail"
                value={this.state.centeremail} onChange={(e) => this.setState({ centeremail: e.target.value })} disabled
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button outline color="primary" onClick={() => this.closeModel()}>Cancel</Button>
            <Button color="primary" onClick={() => { this.closeModel(); this.updatecenter() }}>Save</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.modalPay} toggle={e => { this.setState({ modalPay: !this.state.modalPay }) }}>
          <h2>Payment Methods</h2>
          <div className="animated fadeIn">
            <Card>
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card table-hover">
                  <thead>
                    <tr >
                      <th width="30"></th>
                      <th className="text-left" width="27%">Card</th>
                      <th width="30%">Card Type</th>
                      <th>Network</th>
                      <th width="50"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.getpaymentmethodlist.map((item, index) => (
                      <tr key={index}>
                        <td>
                          <input type="radio"
                            value={item.bankCardId}
                            checked={this.state.selectedOption == item.bankCardId}
                            onChange={this.onValueChange} />
                        </td>
                        <td>
                          {item.cardNumber}<br></br>
                          <small className="text-success">Valid</small>
                        </td>
                        <td className="text-center">{item.cardType} Card</td>
                        <td className="text-center">
                          {item.paymentMethod.paymentMethodName == "VISA" && (
                            <img src={require('../assets/img/visa.png')} style={{ objectFit: 'contain' }} />
                          )}
                          {item.paymentMethod.paymentMethodName == "MasterCard" && (
                            <img src={require('../assets/img/master.png')} style={{ objectFit: 'contain' }} />
                          )}
                          {item.paymentMethod.paymentMethodName == "Paypal" && (
                            <img src={require('../assets/img/paypal.png')} style={{ objectFit: 'contain' }} />
                          )}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>

                {(!global.centerid) && (this.state.isShowServiceCenterProfileEditPrev == true) && (
                  <Button block color="warning" onClick={() => { this.changePaymentMethod() }}>Save</Button>
                )}
              </CardBody>
            </Card>

          </div>
        </Modal>



      </div>
    );
  }
}

export default Breadcrumbs;
