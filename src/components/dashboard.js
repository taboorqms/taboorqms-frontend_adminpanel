import React, { Component,  Suspense } from 'react';
import {Line, Pie } from 'react-chartjs-2'; 
import { Link } from 'react-router-dom';
import {
  Badge,
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Progress,
  Row,
  Table,ButtonDropdown, Dropdown,
  DropdownItem,  DropdownMenu,  DropdownToggle,
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'
import axios from "axios";

import { CalendarComponent } from '@syncfusion/ej2-react-calendars'; 
import routes from './routes';
const Widget03 = React.lazy(() => import('./widget'));
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')



// Card Chart 2
const cardChartData2 = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June'],
  datasets: [
    {
      label: 'Number of Tickets',
      backgroundColor: 'rgba(242, 172, 21, 0.35)',
      borderColor: brandWarning,
      data: [1, 10, 9, 11, 20, 18],
    },
  ],
};

const cardChartOpts2 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent',
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        },

      }],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, cardChartData2.datasets[0].data) - 5,
          max: Math.max.apply(Math, cardChartData2.datasets[0].data) + 5,
        },
      }],
  },
  elements: {
    line: {
      tension: 0.00001,
      borderWidth: 1,
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

// Social Box Chart
const socialBoxData = [
  { data: [65, 59, 84, 84, 51, 55, 40], label: 'facebook' },
  { data: [1, 13, 9, 17, 34, 41, 38], label: 'twitter' },
  { data: [78, 81, 80, 45, 34, 12, 40], label: 'linkedin' },
  { data: [35, 23, 56, 22, 97, 23, 64], label: 'google' },
];

const makeSocialBoxData = (dataSetNo) => {
  const dataset = socialBoxData[dataSetNo];
  const data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        backgroundColor: 'rgba(255,255,255,.1)',
        borderColor: 'rgba(255,255,255,.55)',
        pointHoverBackgroundColor: '#fff',
        borderWidth: 2,
        data: dataset.data,
        label: dataset.label,
      },
    ],
  };
  return () => data;
};

const socialChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: false,
      }],
    yAxes: [
      {
        display: false,
      }],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
};

// sparkline charts
const sparkLineChartData = [
  {
    data: [35, 23, 56, 22, 97, 23, 64],
    label: 'New Clients',
  },
  {
    data: [65, 59, 84, 84, 51, 55, 40],
    label: 'Recurring Clients',
  },
  {
    data: [35, 23, 56, 22, 97, 23, 64],
    label: 'Pageviews',
  },
  {
    data: [65, 59, 84, 84, 51, 55, 40],
    label: 'Organic',
  },
  {
    data: [78, 81, 80, 45, 34, 12, 40],
    label: 'CTR',
  },
  {
    data: [1, 13, 9, 17, 34, 41, 38],
    label: 'Bounce Rate',
  },
];

const makeSparkLineData = (dataSetNo, variant) => {
  const dataset = sparkLineChartData[dataSetNo];
  const data = {
    labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
    datasets: [
      {
        backgroundColor: 'transparent',
        borderColor: variant ? variant : '#c2cfd6',
        data: dataset.data,
        label: dataset.label,
      },
    ],
  };
  return () => data;
};

const sparklineChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  responsive: true,
  maintainAspectRatio: true,
  scales: {
    xAxes: [
      {
        display: false,
      }],
    yAxes: [
      {
        display: false,
      }],
  },
  elements: {
    line: {
      borderWidth: 2,
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
  legend: {
    display: false,
  },
};

// Main Chart

//Random Numbers
function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

var elements = 27;
var data1 = [];
var data2 = [];
var data3 = [];

for (var i = 0; i <= elements; i++) {
  data1.push(random(50, 200));
  data2.push(random(80, 100));
  data3.push(65);
}

const mainChart = {
  labels: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: hexToRgba(brandInfo, 10),
      borderColor: brandInfo,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data1,
    },
    {
      label: 'My Second dataset',
      backgroundColor: 'transparent',
      borderColor: brandSuccess,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data2,
    },
    {
      label: 'My Third dataset',
      backgroundColor: 'transparent',
      borderColor: brandDanger,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5],
      data: data3,
    },
  ],
};

const mainChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: 'index',
    position: 'nearest',
    callbacks: {
      labelColor: function(tooltipItem, chart) {
        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor }
      }
    }
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: false,
        },
      }],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(250 / 5),
          max: 250,
        },
      }],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
};

class Dashboardd extends Component {
  tempAvialableQueues=[]
  showLessQueues=[]
  totalQueues=0
  startDate=new Date()
  endDate=new Date()
  constructor(props) {
    super(props);
    
    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
    this.state = {
      isShowDropDownMenu:false,
      isShowAllQueue:false,
      isCalendar:false,
      isToday:true,
      isWeek:false,
      isTwoWeek:false,
      isOneMonth:false,
      isThreeMonth:false,
      isSixMonth:false,
      isOneYear:false,
      isBranchDetails:false,
      dropdownOpen: false,
      totalTempTickets:0,
      profileDetails:new Date(),
      sDate:new Date(),
      totalTempRatings:0,
      allQueue:[],
      radioSelected: 2,
      totalTickets:0,
      fastpassTickets:0,
      highestDemandAST:0,
      highestDemandTotalTickets:0,
      highestDemandServiceName:'',
      lowestPerformanceServiceName:'',
      highestPerformanceServiceName:'',
      highestPerformanceAST:0,
      highestPerformanceTotalTickets:0,
      lowestPerformanceAST:0,
      lowestPerformanceTotalTickets:0,
      staisfied:0,
      average:0,
      poor:0,
      pie:{
        labels: ["Satisfied", "Average", "Poor"],
        datasets: [
          {
            label: "",
            data: [10,20,30],
            backgroundColor: [
              "#07cf98",
              "#f7b62b",
              "#f72c39",
            ],
            hoverBackgroundColor: [
              "#07cf98",
              "#f7b62b",
              "#f72c39",
            ],
            hoverBorderColor:[
              "#07cf98",
              "#f7b62b",
              "#f72c39",
            ]
          }],
      },
      line:{
        labels: ["S", "M", "T", "W", "T", "F", "S"],
				datasets: [
				  {
					label: "",
          pointRadius: [0, 0, 0, 0, 0, 0, 0],
					pointBackgroundColor: "#ffffff38",
					pointBorderWidth: [0, 3, 0, 0, 0, 0, 5],
					fill: true,
					lineTension: 0.5,
					backgroundColor: "#ffffff38",
					borderWidth: 1,
					borderColor: "#fff",
          data: [50, 70, 90, 60, 90, 120, 100]
				  }
				]
      },
      line2:{
        labels: ["S", "M", "T", "W", "T", "F", "S","U"],
				datasets: [
				  {
					label: "",
          pointRadius: [0, 0, 0, 0, 0, 0, 2,0],
					pointBackgroundColor: "#f2af1f38",
					pointBorderWidth: 1,
					fill: true,
					lineTension: 0.5,
					backgroundColor: "#f2af1f38",
					borderWidth: 1,
					borderColor: "#f2af1f",
          data: [50, 70, 90, 60, 90, 120,100]
				  }
				]
			},
    };
    
  }
componentDidMount()
{
  if(localStorage.getItem('isShowArabicLanguage')=='true'){
    global.isShowArabicLanguage=true
  }
  else{
    global.isShowArabicLanguage=false
  }
  this.getServiceCenterProfile();
  this.getStatictics(this.startDate,this.endDate)
  this.getActivities(this.startDate,this.endDate)
  this.getRatings(this.startDate,this.endDate)
  this.getQueueList()

}
getServiceCenterProfile(){
  let token = localStorage.getItem('sessionToken')
  const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenterEmp/getProfile';
  const header={headers: { 'Authorization': `Bearer ${token}` }} 
  axios.get(url,header).then(responseJson => {
    if(responseJson.data.applicationStatusCode==0) {
      this.setState({ profileDetails:responseJson.data.user.createdTime});
    }
  }).catch(err=>{
    console.log(err)
  });
}
getStatictics(startDate,endDate){
  let token = localStorage.getItem('sessionToken')
  var sDate=startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();
  var eDate=endDate.getFullYear()+'-'+(endDate.getMonth()+1)+'-'+endDate.getDate();
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/dashboard/statistics', {
    method: 'POST',
    body:JSON.stringify({
      "branchIdList": [],
      "endDate": endDate,
      "startDate": startDate
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responsejson => {
    if(responsejson.applicationStatusCode==0)
    {
      var localLine={
        labels: [],
        datasets: [
          {
            label: "",
            pointRadius: 0,
            pointBackgroundColor: "#ffffff38",
            pointBorderWidth: [0, 3, 0, 0, 0, 0, 5],
            fill: true,
            lineTension: 0.5,
            backgroundColor: "#ffffff38",
            borderWidth: 1,
            borderColor: "#fff",
            data: [],
          },
        ],

      }
      if(responsejson.values!=null)
      {
        for(var i=0;i<responsejson.values.length;i++)
        {
          localLine.labels.push(responsejson.values[i].key)
          localLine.datasets[0].data.push(responsejson.values[i].value)
        } 
        var t=0
        for(var j=0;j<localLine.datasets[0].data.length;j++)
        { 
    
          t=t + parseInt(localLine.datasets[0].data[j])
        }
        this.setState({
          line:localLine,
          totalTempTickets:t
        })  
      }
     
    }
   

  })
}
getActivities(startDate,endDate)
{
  let token = localStorage.getItem('sessionToken')
  var sDate=startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate()
  var eDate=endDate.getFullYear()+'-'+(endDate.getMonth()+1)+'-'+endDate.getDate()

  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/dashboard/activities', {
    method: 'POST',
    body:JSON.stringify({
      "branchIdList": [],
      "endDate": endDate,
      "startDate": startDate
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      if(responseJson.values!=null)
      {
        
        if(responseJson.values.BranchDetails!=undefined)
        {
          routes[1].name=responseJson.values.BranchDetails.BranchName
        var lhms = responseJson.values.LowestPerformanceService.AST;  
        var a = lhms.split(':'); 
        var lPA = (+a[0]) * 60 + (+a[1]); 
  
        var hhms = responseJson.values.HighestPerformanceService.AST;  
        var b = hhms.split(':'); 
        var hPA = (+b[0]) * 60 + (+b[1]); 
  
        var hdhms = responseJson.values.HighestDemandService.AST;  
        var c = hdhms.split(':'); 
        var hDA = (+c[0]) * 60 + (+c[1]); 
        
          this.setState({
            isBranchDetails:true,
            lowestPerformanceAST:lPA,
            highestPerformanceAST:hPA,
            highestDemandAST:hDA,
            highestDemandServiceName:responseJson.values.HighestDemandService.Service.serviceName,
            lowestPerformanceServiceName:responseJson.values.LowestPerformanceService.Service.serviceName,
            highestPerformanceServiceName:responseJson.values.HighestPerformanceService.Service.serviceName,
            totalTickets:responseJson.values.NoOfTickets.TotalTickets,
            fastpassTickets:responseJson.values.NoOfTickets.FastpassTickets,
            lowestPerformanceTotalTickets:responseJson.values.LowestPerformanceService.TotalTickets,
            highestDemandTotalTickets:responseJson.values.HighestDemandService.TotalTickets,
            highestPerformanceTotalTickets:responseJson.values.HighestPerformanceService.TotalTickets,
          })
        }
        else
        {
          routes[1].name='Dashboard'
          var lhms = responseJson.values.LowestPerformanceBranch.AST;  
          var a = lhms.split(':'); 
          var lPA = (+a[0]) * 60 + (+a[1]); 
    
          var hhms = responseJson.values.MostVisitedBranch.AST;  
          var b = hhms.split(':'); 
          var hPA = (+b[0]) * 60 + (+b[1]); 
    
          var hdhms = responseJson.values.HighestPerformanceBranch.AST;  
          var c = hdhms.split(':'); 
          var hDA = (+c[0]) * 60 + (+c[1]); 
          this.setState({
            lowestPerformanceAST:lPA,
            highestPerformanceAST:hDA,
            highestDemandAST:hPA,
            highestDemandServiceName:responseJson.values.MostVisitedBranch.BranchName,
            lowestPerformanceServiceName:responseJson.values.LowestPerformanceBranch.BranchName,
            highestPerformanceServiceName:responseJson.values.HighestPerformanceBranch.BranchName,
            totalTickets:responseJson.values.NoOfTickets.TotalTickets,
            fastpassTickets:responseJson.values.NoOfTickets.FastpassTickets,
            lowestPerformanceTotalTickets:responseJson.values.LowestPerformanceBranch.TotalTickets,
            highestDemandTotalTickets:responseJson.values.MostVisitedBranch.TotalTickets,
            highestPerformanceTotalTickets:responseJson.values.HighestPerformanceBranch.TotalTickets,
    
          })
        }
        
      }
    
     
    }
    
  })
}
getRatings(startDate,endDate)
{
  let token = localStorage.getItem('sessionToken')
  var sDate=startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate()
  var eDate=endDate.getFullYear()+'-'+(endDate.getMonth()+1)+'-'+endDate.getDate()
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/dashboard/ratings', {
    method: 'POST',
    body:JSON.stringify({
      "branchIdList": [],
      "endDate": endDate,
      "startDate": startDate
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      if(responseJson.values!=null)
      {
       var temppie={
          labels: ["Satisfied", "Average", "Poor"],
          datasets: [
            {
              label: "",
              data: [],
              backgroundColor: [
                "#07cf98",
                "#f7b62b",
                "#f72c39",
              ],
              hoverBackgroundColor: [
                "#07cf98",
                "#f7b62b",
                "#f72c39",
              ],
              hoverBorderColor:[
                "#07cf98",
                "#f7b62b",
                "#f72c39",
              ]
            }
          ]
        }
        temppie.datasets[0].data[0]=parseInt(responseJson.values['1.0'])
        temppie.datasets[0].data[1]=parseInt(responseJson.values['2.0'])
        temppie.datasets[0].data[2]=parseInt(responseJson.values['3.0'])
        this.setState({
          pie:temppie,
          staisfied:responseJson.values['1.0'],
          average:responseJson.values['2.0'],
          poor:responseJson.values['3.0'],
          totalTempRatings:parseInt(responseJson.values['1.0'])+parseInt(responseJson.values['2.0'])+parseInt(responseJson.values['3.0']),
          
        })
      }
       
     
    }
    
  })
}
getQueueList(){
  this.showLessQueues=[]
  this.tempAvialableQueues=[]
  var prev=JSON.parse(localStorage.getItem('privileges'))
  var isPrev=false
  if(prev!=null)
  {
    for(var i=0;i<prev.length;i++)
    {
      if(prev[i]=='Queue_View')
      {
        isPrev=true
        break;
      }
      else {
        isPrev=false
      }
    }
  }
  if(isPrev!=false)
  {
    let token = localStorage.getItem('sessionToken')
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/queue/getListing';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.get(url,header).then(responseJson => {
      if(responseJson.data.applicationStatusCode==0)
      {
  
        
        for(var i=0;i<responseJson.data.queueList.length;i++)
        {
          responseJson.data.queueList[i]['isShowMenu']=false
          var hms = responseJson.data.queueList[i].queue.averageServiceTime;  
          var a = hms.split(':'); 
          var minutes = (+a[0]) * 60 + (+a[1]); 
          console.log(minutes);
          responseJson.data.queueList[i].queue['AST']=minutes
          var hmss = responseJson.data.queueList[i].queue.averageWaitTime;  
          var a = hmss.split(':'); 
          var minutess = (+a[0]) * 60 + (+a[1]); 
          console.log(minutess);
          responseJson.data.queueList[i].queue['AWT']=minutess
          if(i<=3)
          {
            this.showLessQueues.push(responseJson.data.queueList[i])
          }
        }
        this.tempAvialableQueues.push(responseJson.data.queueList)
        this.totalQueues=this.tempAvialableQueues[0].length
        this.setState({
          allQueue:this.showLessQueues,
         
        })
      }
    }).catch(err=>{
      console.log(err)
    });
  }
  
}
showMenu(id)
{
  if(this.state.isShowAllQueue!=false)
  {
    for(var i=0;i<this.tempAvialableQueues[0].length;i++)
    {
      if(this.tempAvialableQueues[0][i].queue.queueId==id)
      {
        this.tempAvialableQueues[0][i]['isShowMenu']=true
        this.setState({
          isShowDropDownMenu:true
        })
      }
      else
      {
        this.tempAvialableQueues[0][i]['isShowMenu']=false
      }
    }
    this.setState({
      allQueue:this.tempAvialableQueues[0]
    })
  }
  else
  {
    for(var i=0;i<this.showLessQueues.length;i++)
    {
      if(this.showLessQueues[i].queue.queueId==id)
      {
        this.showLessQueues[i]['isShowMenu']=true
        this.setState({
          isShowDropDownMenu:true
        })
      }
      else
      {
        this.showLessQueues[i]['isShowMenu']=false
      }
    }
    this.setState({
      allQueue:this.showLessQueues
    })
  }
    
}
viewAllQueues()
{
  if(this.tempAvialableQueues.length!=0)
  {
    this.setState({
      allQueue:this.tempAvialableQueues[0],
      isShowAllQueue:true
    })
  }
 
}
viewLessQueues()
{
  this.setState({
    allQueue:this.showLessQueues,
    isShowAllQueue:false
  })
}
toggle() {
  this.setState({
    dropdownOpen: !this.state.dropdownOpen,
  });
}

onRadioBtnClick(radioSelected) {
  this.setState({
    radioSelected: radioSelected,
  });
}

loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
onDateChange(value)
{
  if(value=='today')
  {
    var isShow=!this.state.isCalendar
    this.startDate=new Date()
    this.endDate=new Date()
    this.getStatictics(this.startDate,this.endDate)
    this.getActivities(this.startDate,this.endDate)
    this.getRatings(this.startDate,this.endDate)
    this.setState({
      isCalendar:false,
      isToday:true,
      isWeek:false,
      isTwoWeek:false,
      isOneMonth:false,
      isThreeMonth:false,
      isSixMonth:false,
      isOneYear:false,
    })
  }
  else if(value=='7d')
  {
    var date = new Date();
    var isShow=!this.state.isCalendar
    var last = new Date(date.getTime() - (6 * 24 * 60 * 60 * 1000))
    this.getStatictics(last,new Date())
    this.getActivities(last,new Date())
    this.getRatings(last,new Date())
    this.setState({
      isCalendar:false,
      isToday:false,
      isWeek:true,
      isTwoWeek:false,
      isOneMonth:false,
      isThreeMonth:false,
      isSixMonth:false,
      isOneYear:false,
    })
  }
  else if(value=='2w')
  {
    var isShow=!this.state.isCalendar
    var date = new Date();
    var last = new Date(date.getTime() - (13 * 24 * 60 * 60 * 1000))
    this.getStatictics(last,new Date())
    this.getActivities(last,new Date())
    this.getRatings(last,new Date())
    this.setState({
      isCalendar:false,
      isToday:false,
      isWeek:false,
      isTwoWeek:true,
      isOneMonth:false,
      isThreeMonth:false,
      isSixMonth:false,
      isOneYear:false,
    })
  }
  else if(value=='1m')
  {
    var isShow=!this.state.isCalendar
    var oneMonthAgo = new Date(
      new Date().getFullYear(),
      new Date().getMonth() - 1, 
      new Date().getDate()
  );
    this.getStatictics(oneMonthAgo,new Date())
    this.getActivities(oneMonthAgo,new Date())
    this.getRatings(oneMonthAgo,new Date())
    this.setState({
      isCalendar:false,
      isToday:false,
      isWeek:false,
      isTwoWeek:false,
      isOneMonth:true,
      isThreeMonth:false,
      isSixMonth:false,
      isOneYear:false,
    })
  }
  else if(value=='3m')
  {
    var isShow=!this.state.isCalendar
    var oneMonthAgo = new Date(
      new Date().getFullYear(),
      new Date().getMonth() - 3, 
      new Date().getDate()
  );
    this.getStatictics(oneMonthAgo,new Date())
    this.getActivities(oneMonthAgo,new Date())
    this.getRatings(oneMonthAgo,new Date())
    this.setState({
      isCalendar:false,
      startDate:oneMonthAgo,
      endDate:new Date,
      isToday:false,
      isWeek:false,
      isTwoWeek:false,
      isOneMonth:false,
      isThreeMonth:true,
      isSixMonth:false,
      isOneYear:false,
    })
  }
  else if(value=='6m')
  {
    var isShow=!this.state.isCalendar
    var oneMonthAgo = new Date(
      new Date().getFullYear(),
      new Date().getMonth() - 6, 
      new Date().getDate()
  );
    this.getStatictics(oneMonthAgo,new Date())
    this.getActivities(oneMonthAgo,new Date())
    this.getRatings(oneMonthAgo,new Date())
    this.setState({
      isCalendar:false,
      startDate:oneMonthAgo,
      endDate:new Date,
      isToday:false,
      isWeek:false,
      isTwoWeek:false,
      isOneMonth:false,
      isThreeMonth:false,
      isSixMonth:true,
      isOneYear:false,
    })
  }
  else if(value=='1y')
  {
    var isShow=!this.state.isCalendar
    var oneMonthAgo = new Date(
      new Date().getFullYear()-1,
      new Date().getMonth(), 
      new Date().getDate()
  );
    this.getStatictics(oneMonthAgo,new Date())
    this.getActivities(oneMonthAgo,new Date())
    this.getRatings(oneMonthAgo,new Date())
    this.setState({
      isCalendar:false,
      startDate:oneMonthAgo,
      endDate:new Date,
      isToday:false,
      isWeek:false,
      isTwoWeek:false,
      isOneMonth:false,
      isThreeMonth:false,
      isSixMonth:false,
      isOneYear:true,
    })
  }
  else if(value=='calendar')
  {
    var isShow=!this.state.isCalendar
    this.setState({
      isCalendar:isShow,
      isToday:false,
      isWeek:false,
      isTwoWeek:false,
      isOneMonth:false,
      isThreeMonth:false,
      isSixMonth:false,
      isOneYear:false,
    })
  }
 
}
changeStartDate(date)
{
  var isShow=!this.state.isCalendar
  this.getStatictics(date.value,new Date())
  this.getActivities(date.value,new Date())
  this.getRatings(date.value,new Date())
  this.setState({sDate:date.value,startDate:date.value,endDate:new Date(),isCalendar:isShow})
}
queueDetails(id)
{
  this.props.history.push(
    {
      pathname:'/queue-details',
      queueId:id
    }
  
  )
}
  render() {

    return (
      <div className="animated fadeIn"> 
        <DefaultHeader/>  
        <Row className="mb-3 hidden">
          <Col sm="3">
            {global.isShowArabicLanguage==false && <h3>Activities</h3>}
            {global.isShowArabicLanguage==true && <h3>النشاطات</h3>}
          </Col>
          <Col sm="9" className="text-right">
            <ul className="nav d-inline-flex grey-list">
              <li className={"nav-item " + (this.state.isToday ? 'active' : '')}><a className="nav-link" onClick={()=>{this.onDateChange('today')}}> Today</a> </li>
              <li className={"nav-item " + (this.state.isWeek ? 'active' : '')}><a className="nav-link" onClick={()=>{this.onDateChange('7d')}}> 7d</a> </li>
              <li className={"nav-item " + (this.state.isTwoWeek ? 'active' : '')}><a className="nav-link" onClick={()=>{this.onDateChange('2w')}}> 2w</a> </li>
              <li className={"nav-item " + (this.state.isOneMonth ? 'active' : '')}><a className="nav-link" onClick={()=>{this.onDateChange('1m')}}> 1m</a> </li>
              <li className={"nav-item " + (this.state.isThreeMonth ? 'active' : '')}><a className="nav-link" onClick={()=>{this.onDateChange('3m')}}> 3m</a> </li>
              <li className={"nav-item " + (this.state.isSixMonth ? 'active' : '')}><a className="nav-link" onClick={()=>{this.onDateChange('6m')}}> 6m</a> </li>
              <li className={"nav-item " + (this.state.isOneYear ? 'active' : '')}><a className="nav-link" onClick={()=>{this.onDateChange('1y')}}> 1y</a> </li>
              <li className={"nav-item " + (this.state.isCalendar ? 'active' : '')}><a className="nav-link"  onClick={()=>{this.onDateChange('calendar')}}>
              <i className="icon2-calendar-dots font-lg"></i></a>
              {this.state.isCalendar==true && <div><CalendarComponent id="calendar" value={this.state.sDate} min={this.state.profileDetails}  max={new Date()} 
                onChange={(date) => this.changeStartDate(date)} /></div>}
              </li> 
            </ul>
          </Col>
        </Row>
        

        <Row className="mb-3">
          <Col xs="12" sm="6" lg="3">
            <Card className="theme-widget">
              <CardBody> 
                <div className="row">
                  <div className="col-3">
                    <div className="bg-box widget-icon">
                        <i className="icon2-recipt"></i>
                      </div>
                  </div>
                  <div className="col-9">
                    <div className="widget-title">
                    { global.isShowArabicLanguage==false && <p>No. of</p>}
                    { global.isShowArabicLanguage==false &&  <h5>Tickets</h5>}
                    { global.isShowArabicLanguage==true && <p>اجمالي </p>}
                    { global.isShowArabicLanguage==true &&  <h5> التذاكر</h5>}
                    </div>
                  </div>
                  <div className="col-7">
                    <div className="chart-wrapper" style={{height: '48px', overflow:'hidden'}}>
                      <Line data={this.state.line2} options={{
                        legend:{
                          display:false
                        },
                        scales: {
                        yAxes: [{
                            ticks: {
                              beginAtZero:true,
                              display:false
                            },
                            gridLines: {
                              display: false
                            },
                        }],
                        xAxes: [{
                            ticks: {
                                beginAtZero:true,
                                display:false
                            },
                            gridLines: {
                              display: false
                            },
                        }]
                        }
                      }} />
                    </div>
                  </div>
                  <div className="col-5">
                    <div className="widget-values">
                      <h6 className="p-0 text-warning font-2xl mb-0" color="transparent">{this.state.totalTickets}</h6>
                        <h6 className="mb-0 text-truncate" title="fast pass" >{this.state.fastpassTickets} fast pass</h6>
                    </div>
                  </div>
                </div> 
              </CardBody> 
            </Card> 
          </Col>

          <Col xs="12" sm="6" lg="3">
          <Card className="theme-widget">
              <CardBody> 
                <div className="row">
                  <div className="col-3">
                    <div className="bg-box widget-icon">
                        <i className="icon2-branches"></i>
                      </div>
                  </div>
                  <div className="col-9">
                    <div className="widget-title">
                      {this.state.isBranchDetails==true && global.isShowArabicLanguage==false && <p>Highest Demand Service </p>}
                      {this.state.isBranchDetails==true && global.isShowArabicLanguage==true && <p>خدمة الطلب الأعلى</p>}
                      {this.state.isBranchDetails==false && global.isShowArabicLanguage==false && <p>Most Visited</p>}
                      {this.state.isBranchDetails==false && global.isShowArabicLanguage==true && <p>الأكثر زيارة</p>}
                      <h5 className="no-wrap"><span className="d-inline-block text-truncate w-90">{this.state.highestDemandServiceName}</span> <sup className="up-dot"><i className="icon2-ellipse"></i> </sup></h5>
                    </div>
                  </div>
                  <div className="col-4">
                    <h3 className="text-warning">{this.state.highestDemandTotalTickets}</h3>
                    <h6 className="no-wrap">Tickets</h6>
                  </div>
                  <div className="col-8">
                    <h3 className="text-warning">{this.state.highestDemandAST} <sub>min</sub></h3>
                    <h6 className="text-truncate" title="Average service time">Average service time</h6>
                  </div>
                </div> 
              </CardBody>
              
            </Card>
            
          </Col>

          <Col xs="12" sm="6" lg="3">
          <Card className="theme-widget">
              <CardBody> 
                <div className="row">
                  <div className="col-3">
                    <div className="bg-box widget-icon">
                        <i className="icon2-branches"></i>
                      </div>
                  </div>
                  <div className="col-9">
                    <div className="widget-title">
                      {this.state.isBranchDetails==true && global.isShowArabicLanguage==false && <p>Highest Performance Service </p>}
                      {this.state.isBranchDetails==true && global.isShowArabicLanguage==true && <p>خدمة عالية الأداء</p>}
                      {this.state.isBranchDetails==false && global.isShowArabicLanguage==false && <p>Highest Performance</p>}
                      {this.state.isBranchDetails==false && global.isShowArabicLanguage==true && <p>أعلى أداء</p>}
                      <h5 className="no-wrap"><span className="d-inline-block text-truncate w-90">{this.state.highestPerformanceServiceName}</span> <sup className="up-dot"><i className="icon2-ellipse"></i> </sup></h5>
                    </div>
                  </div>
                  <div className="col-4">
                  <h3 className="text-success">{this.state.highestPerformanceTotalTickets}</h3>
                    <h6 className="no-wrap">Tickets</h6>
                  </div>
                  <div className="col-8">
                  <h3 className="text-success">{this.state.highestPerformanceAST} <sub>min</sub></h3>
                    <h6 className="text-truncate" title="Average service time">Average service time</h6>
                  </div>
                </div> 
              </CardBody> 
            </Card>
            
          </Col>

          <Col xs="12" sm="6" lg="3">
          <Card className="theme-widget">
              <CardBody> 
                <div className="row">
                  <div className="col-3">
                    <div className="bg-box widget-icon">
                        <i className="icon2-branches"></i>
                      </div>
                  </div>
                  <div className="col-9">
                    <div className="widget-title">
                      {this.state.isBranchDetails==true && global.isShowArabicLanguage==false &&  <p>Lowest Performance Service </p>}
                      {this.state.isBranchDetails==true && global.isShowArabicLanguage==true &&  <p>خدمة منخفضة الأداء</p>}
                      {this.state.isBranchDetails==false && global.isShowArabicLanguage==false &&  <p>Lowest Performance</p>}
                      {this.state.isBranchDetails==false && global.isShowArabicLanguage==true &&  <p>أقل أداء</p>}
                      <h5 className="no-wrap"><span className="d-inline-block text-truncate w-90">{this.state.lowestPerformanceServiceName}</span> <sup className="up-dot"><i className="icon2-ellipse"></i> </sup></h5>
                    </div>
                  </div>
                  <div className="col-4">
                  <h3 className="text-danger">{this.state.lowestPerformanceTotalTickets}</h3>
                    <h6 className="no-wrap">Tickets</h6>
                  </div>
                  <div className="col-8">
                    <h3 className="text-danger">{this.state.lowestPerformanceAST} <sub>min</sub></h3>
                    <h6 className="text-truncate" title="Average service time">Average service time</h6>
                  </div>
                </div> 
              </CardBody> 
            </Card> 
          </Col>
        </Row>
        
        <Row>
          <Col sm="8">
            <h3 className="mb-4">Current Queues <span className="badge badge-warning-dark">{this.totalQueues}</span></h3> 
            <Card className="mb-0"> 
              <CardBody className="p-0">
                <Table responsive className="bg-white m-0 table-light table-card">
                  <thead>
                  <tr>
                    <th className="bg-warning text-center">In Queue</th>
                    <th className="text-left">Branch</th>
                    <th>#Tickets</th>
                    <th>Fast Pass</th>
                    <th>Counters</th>
                    <th>AST</th>
                    <th>Agents</th>
                    <th>Rating</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  {this.state.allQueue.map((queue,index) => ( 
                     <tr key={index}>
                       <td className="bg-warning text-center">{queue.noOfInQueueTickets}</td>
                       <td>{queue.queue.branch.branchName}
                           <p className="text-light-grey"><i className="icon2-location"></i> {queue.queue.branch.address}</p>
                       </td>
                       <td className="text-center">{queue.noOfTickets}</td>
                       <td className="text-center">{queue.noOfFastpassTicket}</td>
                       <td className="text-center"><span style={{color:'#1ac38d'}}>{queue.noOfServingCounters}</span><span className="text-light-grey">/{queue.noOfTotalCounters}</span></td>
                       <td className="text-center">{queue.queue.AST} <small className="text-light-grey">min</small></td>
                       <td className="text-center">{queue.noOfAgents}</td>
                       <td className="text-center">
                       {queue.queue.branch.rating < 2 &&<i className="icon2-angry-face text-danger f20"></i>}

                      {queue.queue.branch.rating >= 2 && queue.queue.branch.rating < 4 &&<i className="icon2-normal-face text-warning f20"></i>}

                      {queue.queue.branch.rating >= 4 && queue.queue.branch.rating <= 5 &&<i className="icon2-smile text-success f20"></i>}
                                            </td>
                       <td className="text-center"><Link className="ml-4" onClick={()=>this.showMenu(queue.queue.queueId)}><i className="f20 icon2-menu-options text-light-grey"></i></Link>
                     {queue.isShowMenu==true && 
                     <Dropdown isOpen={this.state.isShowDropDownMenu} toggle={() => { this.setState({ isShowDropDownMenu:false }); }} >
                            <DropdownToggle
                              tag="span"
                              onClick={() => {this.setState({ isShowDropDownMenu:false })}}
                              data-toggle="dropdown"
                              >
                            </DropdownToggle>
                            <DropdownMenu>
                           <DropdownItem onClick={()=>this.queueDetails(queue.queue.queueId)}>Details</DropdownItem>
                           
                            </DropdownMenu>
                      </Dropdown>}</td>
                     </tr>                     
                     
                  ))}
                  <tr>
                       <td className="bg-warning text-center"></td>
                       
                       <td colSpan="8" className="text-center">
                         {this.state.isShowAllQueue==false && global.isShowArabicLanguage==false && <Link onClick={()=>this.viewAllQueues()}>View All</Link>}
                         {this.state.isShowAllQueue==false && global.isShowArabicLanguage==true &&  <Link onClick={()=>this.viewAllQueues()}>عرض الكل</Link>}
                        {this.state.isShowAllQueue==true && global.isShowArabicLanguage==false && <Link onClick={()=>this.viewLessQueues()}>View Less</Link>}
                        {this.state.isShowAllQueue==true && global.isShowArabicLanguage==true && <Link onClick={()=>this.viewLessQueues()}>عرض أقل</Link>}
                         </td>
                       
                     </tr>  
                  </tbody>
                 
                </Table>
               
              </CardBody>
            </Card>
          
          


          </Col>
          <Col sm="4" className="text-right">

            <div className="mb-3">
              <div className="float-left">
              {global.isShowArabicLanguage==false && <h3>Statistics</h3>}
              {global.isShowArabicLanguage==true && <h3>الإحصائيات</h3>} 
              </div>
              <div className="clearfix"></div>
            </div>
            <Card className="card-primary">
            
            <CardBody className="text-left">
              <h4 className="sky-blue">{this.state.totalTempTickets} <sub>Tickets</sub></h4>
              <h5 className="text-white">Service Peak Time</h5>
              <div className="chart-wrapper line-graph" >
              
                <Line  data={this.state.line}  
                options={{
                  legend:{
                    display:false
                  },
                  scales: {
                  yAxes: [{
                      ticks: {
                        beginAtZero:true,
                        fontColor: 'white'
                      },
                      gridLines: {
                        display: false
                      },
                  }],
                  xAxes: [{
                      ticks: {
                          beginAtZero:true,
                          fontColor: 'white'
                      },
                      gridLines: {
                        display: false
                      },
                  }]
                  }
                }}
                />
              </div>
            </CardBody>
          </Card>

          <Card className="mb-0">
            
            <CardBody className="text-left">
              <Row>
              <Col sm="8">
                <h3 className="text-light-grey font-weight-normal">{this.state.totalTempRatings} <sub>Votes</sub></h3>
                {global.isShowArabicLanguage==false &&<h5 className="mb-3">Service Ratings</h5>}
                {global.isShowArabicLanguage==true &&<h5 className="mb-3">اجمالي تقييم الفروع</h5>}
              </Col>
              <Col sm="4" className="p-0">
                  <Pie data={this.state.pie} options={{ legend:{ display:false },tooltips:{enabled:false} }}/>
              </Col>
              </Row>

              
              <Row>
                <Col sm="4" className="text-center f20"><i className="icon2-smile text-success"></i> {this.state.staisfied}</Col>
                <Col sm="4" className="text-center f20"><i className="icon2-normal-face text-warning"></i> {this.state.average}</Col>
                <Col sm="4" className="text-center f20"><i className="icon2-angry-face text-danger"></i> {this.state.poor}</Col>
              </Row>
            </CardBody>
          </Card>

          </Col>
        </Row>

        <div className="dash-content">
        <Row>
          <Col>
            <Card>
              <CardBody>
                <Row>
                  <Col sm="5">
                    <CardTitle className="mb-0">Traffic</CardTitle>
                    <div className="small text-light-grey">November 2015</div>
                  </Col>
                  <Col sm="7" className="d-none d-sm-inline-block">
                    <Button color="primary" className="float-right"><i className="icon-cloud-download"></i></Button>
                    <ButtonToolbar className="float-right" aria-label="Toolbar with button groups">
                      <ButtonGroup className="mr-3" aria-label="First group">
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(1)} active={this.state.radioSelected === 1}>Day</Button>
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(2)} active={this.state.radioSelected === 2}>Month</Button>
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(3)} active={this.state.radioSelected === 3}>Year</Button>
                      </ButtonGroup>
                    </ButtonToolbar>
                  </Col>
                </Row>
                <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                  <Line data={mainChart} options={mainChartOpts} height={300} />
                </div>
              </CardBody>
              <CardFooter>
                <Row className="text-center">
                  <Col sm={12} md className="mb-sm-2 mb-0">
                    <div className="text-light-grey">Visits</div>
                    <strong>29.703 Users (40%)</strong>
                    <Progress className="progress-xs mt-2" color="success" value="40" />
                  </Col>
                  <Col sm={12} md className="mb-sm-2 mb-0 d-md-down-none">
                    <div className="text-light-grey">Unique</div>
                    <strong>24.093 Users (20%)</strong>
                    <Progress className="progress-xs mt-2" color="info" value="20" />
                  </Col>
                  <Col sm={12} md className="mb-sm-2 mb-0">
                    <div className="text-light-grey">Pageviews</div>
                    <strong>78.706 Views (60%)</strong>
                    <Progress className="progress-xs mt-2" color="warning" value="60" />
                  </Col>
                  <Col sm={12} md className="mb-sm-2 mb-0">
                    <div className="text-light-grey">New Users</div>
                    <strong>22.123 Users (80%)</strong>
                    <Progress className="progress-xs mt-2" color="danger" value="80" />
                  </Col>
                  <Col sm={12} md className="mb-sm-2 mb-0 d-md-down-none">
                    <div className="text-light-grey">Bounce Rate</div>
                    <strong>Average Rate (40.15%)</strong>
                    <Progress className="progress-xs mt-2" color="primary" value="40" />
                  </Col>
                </Row>
              </CardFooter>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col xs="6" sm="6" lg="3">
            <Suspense fallback={this.loading()}>
              <Widget03 dataBox={() => ({ variant: 'facebook', friends: '89k', feeds: '459' })} >
                <div className="chart-wrapper">
                  <Line data={makeSocialBoxData(0)} options={socialChartOpts} height={90} />
                </div>
              </Widget03>
            </Suspense>
          </Col>

          <Col xs="6" sm="6" lg="3">
            <Suspense fallback={this.loading()}>
              <Widget03 dataBox={() => ({ variant: 'twitter', followers: '973k', tweets: '1.792' })} >
                <div className="chart-wrapper">
                  <Line data={makeSocialBoxData(1)} options={socialChartOpts} height={90} />
                </div>
              </Widget03>
            </Suspense>
          </Col>

          <Col xs="6" sm="6" lg="3">
            <Suspense fallback={this.loading()}>
              <Widget03 dataBox={() => ({ variant: 'linkedin', contacts: '500+', feeds: '292' })} >
                <div className="chart-wrapper">
                  <Line data={makeSocialBoxData(2)} options={socialChartOpts} height={90} />
                </div>
              </Widget03>
            </Suspense>
          </Col>

          <Col xs="6" sm="6" lg="3">
            <Suspense fallback={this.loading()}>
              <Widget03 dataBox={() => ({ variant: 'google-plus', followers: '894', circles: '92' })} >
                <div className="chart-wrapper">
                  <Line data={makeSocialBoxData(3)} options={socialChartOpts} height={90} />
                </div>
              </Widget03>
            </Suspense>
          </Col>
        </Row>

        <Row>
          <Col>
            <Card>
              <CardHeader>
                Traffic {' & '} Sales
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="6" xl="6">
                    <Row>
                      <Col sm="6">
                        <div className="callout callout-info">
                          <small className="text-light-grey">New Clients</small>
                          <br />
                          <strong className="h4">9,123</strong>
                          <div className="chart-wrapper">
                            <Line data={makeSparkLineData(0, brandPrimary)} options={sparklineChartOpts} width={100} height={30} />
                          </div>
                        </div>
                      </Col>
                      <Col sm="6">
                        <div className="callout callout-danger">
                          <small className="text-light-grey">Recurring Clients</small>
                          <br />
                          <strong className="h4">22,643</strong>
                          <div className="chart-wrapper">
                            <Line data={makeSparkLineData(1, brandDanger)} options={sparklineChartOpts} width={100} height={30} />
                          </div>
                        </div>
                      </Col>
                    </Row>
                    <hr className="mt-0" />
                    <div className="progress-group mb-4">
                      <div className="progress-group-prepend">
                        <span className="progress-group-text">
                          Monday
                        </span>
                      </div>
                      <div className="progress-group-bars">
                        <Progress className="progress-xs" color="info" value="34" />
                        <Progress className="progress-xs" color="danger" value="78" />
                      </div>
                    </div>
                    <div className="progress-group mb-4">
                      <div className="progress-group-prepend">
                        <span className="progress-group-text">
                        Tuesday
                        </span>
                      </div>
                      <div className="progress-group-bars">
                        <Progress className="progress-xs" color="info" value="56" />
                        <Progress className="progress-xs" color="danger" value="94" />
                      </div>
                    </div>
                    <div className="progress-group mb-4">
                      <div className="progress-group-prepend">
                        <span className="progress-group-text">
                        Wednesday
                        </span>
                      </div>
                      <div className="progress-group-bars">
                        <Progress className="progress-xs" color="info" value="12" />
                        <Progress className="progress-xs" color="danger" value="67" />
                      </div>
                    </div>
                    <div className="progress-group mb-4">
                      <div className="progress-group-prepend">
                        <span className="progress-group-text">
                        Thursday
                        </span>
                      </div>
                      <div className="progress-group-bars">
                        <Progress className="progress-xs" color="info" value="43" />
                        <Progress className="progress-xs" color="danger" value="91" />
                      </div>
                    </div>
                    <div className="progress-group mb-4">
                      <div className="progress-group-prepend">
                        <span className="progress-group-text">
                        Friday
                        </span>
                      </div>
                      <div className="progress-group-bars">
                        <Progress className="progress-xs" color="info" value="22" />
                        <Progress className="progress-xs" color="danger" value="73" />
                      </div>
                    </div>
                    <div className="progress-group mb-4">
                      <div className="progress-group-prepend">
                        <span className="progress-group-text">
                        Saturday
                        </span>
                      </div>
                      <div className="progress-group-bars">
                        <Progress className="progress-xs" color="info" value="53" />
                        <Progress className="progress-xs" color="danger" value="82" />
                      </div>
                    </div>
                    <div className="progress-group mb-4">
                      <div className="progress-group-prepend">
                        <span className="progress-group-text">
                        Sunday
                        </span>
                      </div>
                      <div className="progress-group-bars">
                        <Progress className="progress-xs" color="info" value="9" />
                        <Progress className="progress-xs" color="danger" value="69" />
                      </div>
                    </div>
                    <div className="legend text-center">
                      <small>
                        <sup className="px-1"><Badge pill color="info">&nbsp;</Badge></sup>
                        New clients
                        &nbsp;
                        <sup className="px-1"><Badge pill color="danger">&nbsp;</Badge></sup>
                        Recurring clients
                      </small>
                    </div>
                  </Col>
                  <Col xs="12" md="6" xl="6">
                    <Row>
                      <Col sm="6">
                        <div className="callout callout-warning">
                          <small className="text-light-grey">Pageviews</small>
                          <br />
                          <strong className="h4">78,623</strong>
                          <div className="chart-wrapper">
                            <Line data={makeSparkLineData(2, brandWarning)} options={sparklineChartOpts} width={100} height={30} />
                          </div>
                        </div>
                      </Col>
                      <Col sm="6">
                        <div className="callout callout-success">
                          <small className="text-light-grey">Organic</small>
                          <br />
                          <strong className="h4">49,123</strong>
                          <div className="chart-wrapper">
                            <Line data={makeSparkLineData(3, brandSuccess)} options={sparklineChartOpts} width={100} height={30} />
                          </div>
                        </div>
                      </Col>
                    </Row>
                    <hr className="mt-0" />
                    <ul>
                      <div className="progress-group">
                        <div className="progress-group-header">
                          <i className="icon2-username progress-group-icon"></i>
                          <span className="title">Male</span>
                          <span className="ml-auto font-weight-bold">43%</span>
                        </div>
                        <div className="progress-group-bars">
                          <Progress className="progress-xs" color="warning" value="43" />
                        </div>
                      </div>
                      <div className="progress-group mb-5">
                        <div className="progress-group-header">
                          <i className="icon2-username-female progress-group-icon"></i>
                          <span className="title">Female</span>
                          <span className="ml-auto font-weight-bold">37%</span>
                        </div>
                        <div className="progress-group-bars">
                          <Progress className="progress-xs" color="warning" value="37" />
                        </div>
                      </div>
                      <div className="progress-group">
                        <div className="progress-group-header">
                          <i className="icon-globe progress-group-icon"></i>
                          <span className="title">Organic Search</span>
                          <span className="ml-auto font-weight-bold">191,235 <span className="text-light-grey small">(56%)</span></span>
                        </div>
                        <div className="progress-group-bars">
                          <Progress className="progress-xs" color="success" value="56" />
                        </div>
                      </div>
                      <div className="progress-group">
                        <div className="progress-group-header">
                          <i className="icon-social-facebook progress-group-icon"></i>
                          <span className="title">Facebook</span>
                          <span className="ml-auto font-weight-bold">51,223 <span className="text-light-grey small">(15%)</span></span>
                        </div>
                        <div className="progress-group-bars">
                          <Progress className="progress-xs" color="success" value="15" />
                        </div>
                      </div>
                      <div className="progress-group">
                        <div className="progress-group-header">
                          <i className="icon-social-twitter progress-group-icon"></i>
                          <span className="title">Twitter</span>
                          <span className="ml-auto font-weight-bold">37,564 <span className="text-light-grey small">(11%)</span></span>
                        </div>
                        <div className="progress-group-bars">
                          <Progress className="progress-xs" color="success" value="11" />
                        </div>
                      </div>
                      <div className="progress-group">
                        <div className="progress-group-header">
                          <i className="icon-social-linkedin progress-group-icon"></i>
                          <span className="title">LinkedIn</span>
                          <span className="ml-auto font-weight-bold">27,319 <span className="text-light-grey small">(8%)</span></span>
                        </div>
                        <div className="progress-group-bars">
                          <Progress className="progress-xs" color="success" value="8" />
                        </div>
                      </div>
                      <div className="divider text-center">
                        <Button color="link" size="sm" className="text-light-grey" data-toggle="tooltip" data-placement="top"
                                title="" data-original-title="show more"><i className="icon-options"></i></Button>
                      </div>
                    </ul>
                  </Col>
                </Row>
                <br />
                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                  <thead className="thead-light">
                  <tr>
                    <th className="text-center"><i className="icon-people"></i></th>
                    <th>User</th>
                    <th className="text-center">Country</th>
                    <th>Usage</th>
                    <th className="text-center">Payment Method</th>
                    <th>Activity</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img src={'assets/img/avatars/1.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
                        <span className="avatar-status badge-success"></span>
                      </div>
                    </td>
                    <td>
                      <div>Yiorgos Avraamu</div>
                      <div className="small text-light-grey">
                        <span>New</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i className="flag-icon flag-icon-us h4 mb-0" title="us" id="us"></i>
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>50%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-light-grey">Jun 11, 2015 - Jul 10, 2015</small>
                        </div>
                      </div>
                      <Progress className="progress-xs" color="success" value="50" />
                    </td>
                    <td className="text-center">
                      <i className="fa fa-cc-mastercard" style={{ fontSize: 24 + 'px' }}></i>
                    </td>
                    <td>
                      <div className="small text-light-grey">Last login</div>
                      <strong>10 sec ago</strong>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img src={'assets/img/avatars/2.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
                        <span className="avatar-status badge-danger"></span>
                      </div>
                    </td>
                    <td>
                      <div>Avram Tarasios</div>
                      <div className="small text-light-grey">

                        <span>Recurring</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i className="flag-icon flag-icon-br h4 mb-0" title="br" id="br"></i>
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>10%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-light-grey">Jun 11, 2015 - Jul 10, 2015</small>
                        </div>
                      </div>
                      <Progress className="progress-xs" color="info" value="10" />
                    </td>
                    <td className="text-center">
                      <i className="fa fa-cc-visa" style={{ fontSize: 24 + 'px' }}></i>
                    </td>
                    <td>
                      <div className="small text-light-grey">Last login</div>
                      <strong>5 minutes ago</strong>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img src={'assets/img/avatars/3.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
                        <span className="avatar-status badge-warning"></span>
                      </div>
                    </td>
                    <td>
                      <div>Quintin Ed</div>
                      <div className="small text-light-grey">
                        <span>New</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i className="flag-icon flag-icon-in h4 mb-0" title="in" id="in"></i>
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>74%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-light-grey">Jun 11, 2015 - Jul 10, 2015</small>
                        </div>
                      </div>
                      <Progress className="progress-xs" color="warning" value="74" />
                    </td>
                    <td className="text-center">
                      <i className="fa fa-cc-stripe" style={{ fontSize: 24 + 'px' }}></i>
                    </td>
                    <td>
                      <div className="small text-light-grey">Last login</div>
                      <strong>1 hour ago</strong>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img src={'assets/img/avatars/4.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
                        <span className="avatar-status badge-secondary"></span>
                      </div>
                    </td>
                    <td>
                      <div>Enéas Kwadwo</div>
                      <div className="small text-light-grey">
                        <span>New</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i className="flag-icon flag-icon-fr h4 mb-0" title="fr" id="fr"></i>
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>98%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-light-grey">Jun 11, 2015 - Jul 10, 2015</small>
                        </div>
                      </div>
                      <Progress className="progress-xs" color="danger" value="98" />
                    </td>
                    <td className="text-center">
                      <i className="fa fa-paypal" style={{ fontSize: 24 + 'px' }}></i>
                    </td>
                    <td>
                      <div className="small text-light-grey">Last login</div>
                      <strong>Last month</strong>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img src={'assets/img/avatars/5.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
                        <span className="avatar-status badge-success"></span>
                      </div>
                    </td>
                    <td>
                      <div>Agapetus Tadeáš</div>
                      <div className="small text-light-grey">
                        <span>New</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i className="flag-icon flag-icon-es h4 mb-0" title="es" id="es"></i>
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>22%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-light-grey">Jun 11, 2015 - Jul 10, 2015</small>
                        </div>
                      </div>
                      <Progress className="progress-xs" color="info" value="22" />
                    </td>
                    <td className="text-center">
                      <i className="fa fa-google-wallet" style={{ fontSize: 24 + 'px' }}></i>
                    </td>
                    <td>
                      <div className="small text-light-grey">Last login</div>
                      <strong>Last week</strong>
                    </td>
                  </tr>
                  <tr>
                    <td className="text-center">
                      <div className="avatar">
                        <img src={'assets/img/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
                        <span className="avatar-status badge-danger"></span>
                      </div>
                    </td>
                    <td>
                      <div>Friderik Dávid</div>
                      <div className="small text-light-grey">
                        <span>New</span> | Registered: Jan 1, 2015
                      </div>
                    </td>
                    <td className="text-center">
                      <i className="flag-icon flag-icon-pl h4 mb-0" title="pl" id="pl"></i>
                    </td>
                    <td>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>43%</strong>
                        </div>
                        <div className="float-right">
                          <small className="text-light-grey">Jun 11, 2015 - Jul 10, 2015</small>
                        </div>
                      </div>
                      <Progress className="progress-xs" color="success" value="43" />
                    </td>
                    <td className="text-center">
                      <i className="fa fa-cc-amex" style={{ fontSize: 24 + 'px' }}></i>
                    </td>
                    <td>
                      <div className="small text-light-grey">Last login</div>
                      <strong>Yesterday</strong>
                    </td>
                  </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        </div>
      
      </div>
    );
  }
}

export default Dashboardd;



