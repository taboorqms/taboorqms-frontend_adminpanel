import React, { Component } from 'react';
import { Dropdown,Card, CardBody, CardHeader, Alert, Col, Row, Table, Button, FormGroup, Label, Input, ButtonDropdown, DropdownItem,  DropdownMenu,  DropdownToggle } from 'reactstrap';
import { Link,NavLink } from 'react-router-dom';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';
import toaster from "toasted-notes";
import moment from 'moment';
import "toasted-notes/src/styles.css";
import { confirmAlert } from 'react-confirm-alert';
import * as router from 'react-router-dom';
import { Badge, UncontrolledDropdown, Nav, NavItem } from 'reactstrap';
import {AppBreadcrumb2 as AppBreadcrumb,AppSidebarNav2 as AppSidebarNav,} from '@coreui/react';
import { AppAsideToggler, AppSidebarToggler } from '@coreui/react';
import routes from './routes';
import axios from "axios";
import LoadingBar from 'react-top-loading-bar'
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
class Payment extends Component {
  prev=[]
  tempFastPass=''
  configId=0
  serviceCenterId=0
  serviceCenter={}
  tempTaboorAmount=''
  tempFastPassCount=''
  tempFastPassTime=''
  tempNormalPassCount=''
  tempNormalPassTime=''
  countries= [
    {
        "code": "+1",
        "name": "Canada"
    },
    {
        "code": "+1",
        "name": "United States"
    },
    {
        "code": "+1 242",
        "name": "Bahamas"
    },
    {
        "code": "+1 246",
        "name": "Barbados"
    },
    {
        "code": "+1 264",
        "name": "Anguilla"
    },
    {
        "code": "+1 268",
        "name": "Antigua and Barbuda"
    },
    {
        "code": "+1 268",
        "name": "Barbuda"
    },
    {
        "code": "+1 284",
        "name": "British Virgin Islands"
    },
    {
        "code": "+1 340",
        "name": "U.S. Virgin Islands"
    },
    {
        "code": "+1 441",
        "name": "Bermuda"
    },
    {
        "code": "+1 473",
        "name": "Grenada"
    },
    {
        "code": "+1 649",
        "name": "Turks and Caicos Islands"
    },
    {
        "code": "+1 670",
        "name": "Northern Mariana Islands"
    },
    {
        "code": "+1 671",
        "name": "Guam"
    },
    {
        "code": "+1 684",
        "name": "American Samoa"
    },
    {
        "code": "+1 767",
        "name": "Dominica"
    },
    {
        "code": "+1 787",
        "name": "Puerto Rico"
    },
    {
        "code": "+1 808",
        "name": "Midway Island"
    },
    {
        "code": "+1 808",
        "name": "Wake Island"
    },
    {
        "code": "+1 809",
        "name": "Dominican Republic"
    },
    {
        "code": "+1 868",
        "name": "Trinidad and Tobago"
    },
    {
        "code": "+1 869",
        "name": "Nevis"
    },
    {
        "code": "+1 876",
        "name": "Jamaica"
    },
    {
        "code": "+1664",
        "name": "Montserrat"
    },
    {
        "code": "+20",
        "name": "Egypt"
    },
    {
        "code": "+212",
        "name": "Morocco"
    },
    {
        "code": "+213",
        "name": "Algeria"
    },
    {
        "code": "+216",
        "name": "Tunisia"
    },
    {
        "code": "+218",
        "name": "Libya"
    },
    {
        "code": "+220",
        "name": "Gambia"
    },
    {
        "code": "+221",
        "name": "Senegal"
    },
    {
        "code": "+222",
        "name": "Mauritania"
    },
    {
        "code": "+223",
        "name": "Mali"
    },
    {
        "code": "+224",
        "name": "Guinea"
    },
    {
        "code": "+225",
        "name": "Ivory Coast"
    },
    {
        "code": "+226",
        "name": "Burkina Faso"
    },
    {
        "code": "+227",
        "name": "Niger"
    },
    {
        "code": "+228",
        "name": "Togo"
    },
    {
        "code": "+229",
        "name": "Benin"
    },
    {
        "code": "+230",
        "name": "Mauritius"
    },
    {
        "code": "+231",
        "name": "Liberia"
    },
    {
        "code": "+232",
        "name": "Sierra Leone"
    },
    {
        "code": "+233",
        "name": "Ghana"
    },
    {
        "code": "+234",
        "name": "Nigeria"
    },
    {
        "code": "+235",
        "name": "Chad"
    },
    {
        "code": "+236",
        "name": "Central African Republic"
    },
    {
        "code": "+237",
        "name": "Cameroon"
    },
    {
        "code": "+238",
        "name": "Cape Verde"
    },
    {
        "code": "+240",
        "name": "Equatorial Guinea"
    },
    {
        "code": "+241",
        "name": "Gabon"
    },
    {
        "code": "+242",
        "name": "Congo"
    },
    {
        "code": "+243",
        "name": "Congo, Dem. Rep. of (Zaire)"
    },
    {
        "code": "+244",
        "name": "Angola"
    },
    {
        "code": "+245",
        "name": "Guinea-Bissau"
    },
    {
        "code": "+246",
        "name": "British Indian Ocean Territory"
    },
    {
        "code": "+246",
        "name": "Diego Garcia"
    },
    {
        "code": "+247",
        "name": "Ascension"
    },
    {
        "code": "+248",
        "name": "Seychelles"
    },
    {
        "code": "+249",
        "name": "Sudan"
    },
    {
        "code": "+250",
        "name": "Rwanda"
    },
    {
        "code": "+251",
        "name": "Ethiopia"
    },
    {
        "code": "+253",
        "name": "Djibouti"
    },
    {
        "code": "+254",
        "name": "Kenya"
    },
    {
        "code": "+255",
        "name": "Tanzania"
    },
    {
        "code": "+255",
        "name": "Zanzibar"
    },
    {
        "code": "+256",
        "name": "Uganda"
    },
    {
        "code": "+257",
        "name": "Burundi"
    },
    {
        "code": "+260",
        "name": "Zambia"
    },
    {
        "code": "+261",
        "name": "Madagascar"
    },
    {
        "code": "+262",
        "name": "Mayotte"
    },
    {
        "code": "+262",
        "name": "Reunion"
    },
    {
        "code": "+263",
        "name": "Zimbabwe"
    },
    {
        "code": "+264",
        "name": "Namibia"
    },
    {
        "code": "+265",
        "name": "Malawi"
    },
    {
        "code": "+266",
        "name": "Lesotho"
    },
    {
        "code": "+267",
        "name": "Botswana"
    },
    {
        "code": "+268",
        "name": "Swaziland"
    },
    {
        "code": "+269",
        "name": "Comoros"
    },
    {
        "code": "+27",
        "name": "South Africa"
    },
    {
        "code": "+291",
        "name": "Eritrea"
    },
    {
        "code": "+297",
        "name": "Aruba"
    },
    {
        "code": "+298",
        "name": "Faroe Islands"
    },
    {
        "code": "+299",
        "name": "Greenland"
    },
    {
        "code": "+30",
        "name": "Greece"
    },
    {
        "code": "+31",
        "name": "Netherlands"
    },
    {
        "code": "+32",
        "name": "Belgium"
    },
    {
        "code": "+33",
        "name": "France"
    },
    {
        "code": "+34",
        "name": "Spain"
    },
    {
        "code": "+350",
        "name": "Gibraltar"
    },
    {
        "code": "+351",
        "name": "Portugal"
    },
    {
        "code": "+352",
        "name": "Luxembourg"
    },
    {
        "code": "+353",
        "name": "Ireland"
    },
    {
        "code": "+354",
        "name": "Iceland"
    },
    {
        "code": "+355",
        "name": "Albania"
    },
    {
        "code": "+356",
        "name": "Malta"
    },
    {
        "code": "+358",
        "name": "Finland"
    },
    {
        "code": "+359",
        "name": "Bulgaria"
    },
    {
        "code": "+36",
        "name": "Hungary"
    },
    {
        "code": "+370",
        "name": "Lithuania"
    },
    {
        "code": "+371",
        "name": "Latvia"
    },
    {
        "code": "+372",
        "name": "Estonia"
    },
    {
        "code": "+373",
        "name": "Moldova"
    },
    {
        "code": "+374",
        "name": "Armenia"
    },
    {
        "code": "+375",
        "name": "Belarus"
    },
    {
        "code": "+376",
        "name": "Andorra"
    },
    {
        "code": "+377",
        "name": "Monaco"
    },
    {
        "code": "+378",
        "name": "San Marino"
    },
    {
        "code": "+380",
        "name": "Ukraine"
    },
    {
        "code": "+381",
        "name": "Serbia"
    },
    {
        "code": "+382",
        "name": "Montenegro"
    },
    {
        "code": "+385",
        "name": "Croatia"
    },
    {
        "code": "+386",
        "name": "Slovenia"
    },
    {
        "code": "+387",
        "name": "Bosnia and Herzegovina"
    },
    {
        "code": "+389",
        "name": "Macedonia"
    },
    {
        "code": "+39",
        "name": "Italy"
    },
    {
        "code": "+40",
        "name": "Romania"
    },
    {
        "code": "+41",
        "name": "Switzerland"
    },
    {
        "code": "+420",
        "name": "Czech Republic"
    },
    {
        "code": "+421",
        "name": "Slovakia"
    },
    {
        "code": "+423",
        "name": "Liechtenstein"
    },
    {
        "code": "+43",
        "name": "Austria"
    },
    {
        "code": "+44",
        "name": "United Kingdom"
    },
    {
        "code": "+45",
        "name": "Denmark"
    },
    {
        "code": "+46",
        "name": "Sweden"
    },
    {
        "code": "+47",
        "name": "Norway"
    },
    {
        "code": "+48",
        "name": "Poland"
    },
    {
        "code": "+49",
        "name": "Germany"
    },
    {
        "code": "+500",
        "name": "Falkland Islands"
    },
    {
        "code": "+500",
        "name": "South Georgia and the South Sandwich Islands"
    },
    {
        "code": "+501",
        "name": "Belize"
    },
    {
        "code": "+502",
        "name": "Guatemala"
    },
    {
        "code": "+503",
        "name": "El Salvador"
    },
    {
        "code": "+504",
        "name": "Honduras"
    },
    {
        "code": "+505",
        "name": "Nicaragua"
    },
    {
        "code": "+506",
        "name": "Costa Rica"
    },
    {
        "code": "+507",
        "name": "Panama"
    },
    {
        "code": "+509",
        "name": "Haiti"
    },
    {
        "code": "+51",
        "name": "Peru"
    },
    {
        "code": "+52",
        "name": "Mexico"
    },
    {
        "code": "+53",
        "name": "Cuba"
    },
    {
        "code": "+537",
        "name": "Cyprus"
    },
    {
        "code": "+54",
        "name": "Argentina"
    },
    {
        "code": "+55",
        "name": "Brazil"
    },
    {
        "code": "+56",
        "name": "Chile"
    },
    {
        "code": "+56",
        "name": "Easter Island"
    },
    {
        "code": "+57",
        "name": "Colombia"
    },
    {
        "code": "+58",
        "name": "Venezuela"
    },
    {
        "code": "+590",
        "name": "Guadeloupe"
    },
    {
        "code": "+591",
        "name": "Bolivia"
    },
    {
        "code": "+593",
        "name": "Ecuador"
    },
    {
        "code": "+594",
        "name": "French Guiana"
    },
    {
        "code": "+595",
        "name": "Guyana"
    },
    {
        "code": "+595",
        "name": "Paraguay"
    },
    {
        "code": "+596",
        "name": "French Antilles"
    },
    {
        "code": "+596",
        "name": "Martinique"
    },
    {
        "code": "+597",
        "name": "Suriname"
    },
    {
        "code": "+598",
        "name": "Uruguay"
    },
    {
        "code": "+599",
        "name": "Curacao"
    },
    {
        "code": "+599",
        "name": "Netherlands Antilles"
    },
    {
        "code": "+60",
        "name": "Malaysia"
    },
    {
        "code": "+61",
        "name": "Australia"
    },
    {
        "code": "+61",
        "name": "Christmas Island"
    },
    {
        "code": "+61",
        "name": "Cocos-Keeling Islands"
    },
    {
        "code": "+62",
        "name": "Indonesia"
    },
    {
        "code": "+63",
        "name": "Philippines"
    },
    {
        "code": "+64",
        "name": "New Zealand"
    },
    {
        "code": "+65",
        "name": "Singapore"
    },
    {
        "code": "+66",
        "name": "Thailand"
    },
    {
        "code": "+670",
        "name": "East Timor"
    },
    {
        "code": "+670",
        "name": "Timor Leste"
    },
    {
        "code": "+672",
        "name": "Australian External Territories"
    },
    {
        "code": "+672",
        "name": "Norfolk Island"
    },
    {
        "code": "+673",
        "name": "Brunei"
    },
    {
        "code": "+674",
        "name": "Nauru"
    },
    {
        "code": "+675",
        "name": "Papua New Guinea"
    },
    {
        "code": "+676",
        "name": "Tonga"
    },
    {
        "code": "+677",
        "name": "Solomon Islands"
    },
    {
        "code": "+678",
        "name": "Vanuatu"
    },
    {
        "code": "+679",
        "name": "Fiji"
    },
    {
        "code": "+680",
        "name": "Palau"
    },
    {
        "code": "+681",
        "name": "Wallis and Futuna"
    },
    {
        "code": "+682",
        "name": "Cook Islands"
    },
    {
        "code": "+683",
        "name": "Niue"
    },
    {
        "code": "+685",
        "name": "Samoa"
    },
    {
        "code": "+686",
        "name": "Kiribati"
    },
    {
        "code": "+687",
        "name": "New Caledonia"
    },
    {
        "code": "+688",
        "name": "Tuvalu"
    },
    {
        "code": "+689",
        "name": "French Polynesia"
    },
    {
        "code": "+690",
        "name": "Tokelau"
    },
    {
        "code": "+691",
        "name": "Micronesia"
    },
    {
        "code": "+692",
        "name": "Marshall Islands"
    },
    {
        "code": "+7",
        "name": "Russia"
    },
    {
        "code": "+7 7",
        "name": "Kazakhstan"
    },
    {
        "code": "+7840",
        "name": "Abkhazia"
    },
    {
        "code": "+81",
        "name": "Japan"
    },
    {
        "code": "+82",
        "name": "South Korea"
    },
    {
        "code": "+84",
        "name": "Vietnam"
    },
    {
        "code": "+850",
        "name": "North Korea"
    },
    {
        "code": "+852",
        "name": "Hong Kong SAR China"
    },
    {
        "code": "+853",
        "name": "Macau SAR China"
    },
    {
        "code": "+855",
        "name": "Cambodia"
    },
    {
        "code": "+856",
        "name": "Laos"
    },
    {
        "code": "+86",
        "name": "China"
    },
    {
        "code": "+880",
        "name": "Bangladesh"
    },
    {
        "code": "+886",
        "name": "Taiwan"
    },
    {
        "code": "+90",
        "name": "Turkey"
    },
    {
        "code": "+91",
        "name": "India"
    },
    {
        "code": "+92",
        "name": "Pakistan"
    },
    {
        "code": "+93",
        "name": "Afghanistan"
    },
    {
        "code": "+94",
        "name": "Sri Lanka"
    },
    {
        "code": "+95",
        "name": "Myanmar"
    },
    {
        "code": "+960",
        "name": "Maldives"
    },
    {
        "code": "+961",
        "name": "Lebanon"
    },
    {
        "code": "+962",
        "name": "Jordan"
    },
    {
        "code": "+963",
        "name": "Syria"
    },
    {
        "code": "+964",
        "name": "Iraq"
    },
    {
        "code": "+965",
        "name": "Kuwait"
    },
    {
        "code": "+966",
        "name": "Saudi Arabia"
    },
    {
        "code": "+967",
        "name": "Yemen"
    },
    {
        "code": "+968",
        "name": "Oman"
    },
    {
        "code": "+970",
        "name": "Palestinian Territory"
    },
    {
        "code": "+971",
        "name": "United Arab Emirates"
    },
    {
        "code": "+972",
        "name": "Israel"
    },
    {
        "code": "+973",
        "name": "Bahrain"
    },
    {
        "code": "+974",
        "name": "Qatar"
    },
    {
        "code": "+975",
        "name": "Bhutan"
    },
    {
        "code": "+976",
        "name": "Mongolia"
    },
    {
        "code": "+977",
        "name": "Nepal"
    },
    {
        "code": "+98",
        "name": "Iran"
    },
    {
        "code": "+992",
        "name": "Tajikistan"
    },
    {
        "code": "+993",
        "name": "Turkmenistan"
    },
    {
        "code": "+994",
        "name": "Azerbaijan"
    },
    {
        "code": "+995",
        "name": "Georgia"
    },
    {
        "code": "+996",
        "name": "Kyrgyzstan"
    },
    {
        "code": "+998",
        "name": "Uzbekistan"
    }
  ]
  constructor(props) {
    super(props);
    this.state = {
      isDisableSave:false,
      progress:0,
      dropdownOpen: false,
      radioSelected: 2,
      modal: false,
      large: false,
      largeedit:false,
      addlarge:false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
      selectedOption :0,
      centerid:'',
      centername:'',
      centernamearab:'',
      centercountry:'',
      centerphone:'',
      centeremail:'',
      profileimg:'',
      servicenameeng:'',
      servicenamearab:'',
      reqdoceng:'',
      reqdocarab:'',
      showeditservice:false,
      isShowServiceCenterProfilePrev:false,
      isShowServiceCenterProfileEditPrev:false,
      reqlist:[],
      getsubslist:[],
      getprofilelist:[],
      getconfiglist:[],
      getdefaultconfiglist:[],
      getpaymentmethodlist:[]
    };
    this.onValueChange = this.onValueChange.bind(this);
    this.addpaymentmethod = this.addpaymentmethod.bind(this);
    this.editpaymentmethod= this.editpaymentmethod.bind(this);
  }

  componentDidMount()
  {
    if(localStorage.getItem('isShowArabicLanguage')=='true')
    {
      global.isShowArabicLanguage=true
    }
    else
    {
      global.isShowArabicLanguage=false
    }
    this.setState({phoneCode:this.countries[0].code})
    this.getprofile()
    var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Service_Center_Profile_View')
        {
          this.setState({isShowServiceCenterProfilePrev:true})
          isPrev=true
        }
        else {
          if(!isPrev==true)
          {
            isPrev=false
          }
         
        }
        if(prev[i]=='Service_Center_Profile_Edit')
        {
          this.setState({isShowServiceCenterProfileEditPrev:true})
        }
      }
    }
    if(isPrev==false)
    {
      toaster.notify("User don't have privilege", {
        // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
        duration:5000 // This notification will not automatically close
      });
    }
  
    
  }
  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  toggleLarge() {
    this.setState({
      addlarge: !this.state.addlarge,
    });
    if(this.state.addlarge==false){
      this.setState({
        servicenameeng:'',
        servicenamearab:'',
        reqdoceng:'',
        reqdocarab:'',
        reqlist:[]
      });
    }
  }


  
getconfig=(id)=>{
  let token = localStorage.getItem('sessionToken')
  const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/config/get';
  const header={headers: {
    'Authorization': `Bearer ${token}`
  }} 
  var  body={
    'id':id
  }
  axios.post(url,body,header).then(responseJson => {
    if(responseJson.data.applicationStatusCode==0)
    {
      this.setState({getconfiglist:responseJson.data })
      this.configId=responseJson.data.serviceCenterConfiguration.configurationId
      this.serviceCenter=responseJson.data.serviceCenterConfiguration.serviceCenter
    }
  }).catch(err=>{
    console.log(err);
    // alert('Network Error')
  });
}
  getprofile=()=>{
    console.log('===============global.centerid=====================');
    console.log(localStorage.getItem('role'));
    console.log('===============global.centerid=====================');
    let token = localStorage.getItem('sessionToken');
    var body={
      "id":global.centerid?global.centerid:null
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getProfile';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.post(url,body,header).then(responseJson => {
        console.log('==================getprofile==================');
        console.log(responseJson.data);
        console.log('=====================getprofile===============');
        this.serviceCenterId=responseJson.data.serviceCenter.serviceCenterId
        this.setState({getprofilelist:responseJson.data});
        this.setState({centerid:responseJson.data.serviceCenter.serviceCenterId});
        this.setState({centername:responseJson.data.serviceCenter.serviceCenterName});
        this.setState({centernamearab:responseJson.data.serviceCenter.serviceCenterArabicName});
        this.setState({centercountry:responseJson.data.serviceCenter.country});
        this.setState({centerphone:responseJson.data.serviceCenter.phoneNumber});
        this.setState({centeremail:responseJson.data.serviceCenter.email,
          profileimg:responseJson.data.serviceCenter.logoDataString});
        // this.getprofileimg(responseJson.data.serviceCenter.imageUrl)
        this.getconfig(responseJson.data.serviceCenter.serviceCenterId);
        var date = new Date();
        var last = new Date(date.getTime() - (6 * 24 * 60 * 60 * 1000))
        this.getpaymentmethod(responseJson.data.serviceCenter.serviceCenterId);
        this.getsubs(responseJson.data.serviceCenter.serviceCenterId);
    }).catch(err=>{
      console.log(err)
    });
  }
  getprofileimg=(file)=>{
    let token = localStorage.getItem('sessionToken')
    var body = file
    const url = 'https://apicall.taboor.ae/taboor-qms/usermanagement/fileData/get';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.post(url,body,header).then(responseJson => {
      console.log('==================getprofileimg==================');
      console.log(responseJson.data);
      console.log('=====================getprofileimg===============');  
      // this.setState({profileimg:responseJson.data.applicationStatusResponse})
    }).catch(err=>{
      console.log(err);
      alert('Network Error')
    });
  }

  addpaymentmethod() {
    this.setState({
      large: !this.state.large,
    });
    if(this.state.large==false){
      this.setState({
        cardnumber:'',
        carddate:'',
        cardcvv:'',
        cardholdername:'',
      })
    }
  }
  editpaymentmethod() {
    this.setState({
      largeeditpay: !this.state.largeeditpay,
    });
  }

  
deletepayment=(id)=>{
  let token = localStorage.getItem('sessionToken');
  var body = 
  {
    "id": id
  }
  const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/paymentMethod/delete';
  const header={headers: {
    'Authorization': `Bearer ${token}`
  }} 
  confirmAlert({
    title: '',
    message: 'Do you want to delete card?',
    buttons: [
      {
        label: 'Yes',
        onClick: () => {
          axios.post(url,body,header).then(responseJson => {
            console.log(responseJson.data);
            this.getpaymentmethod(this.state.centerid)
          }).catch(err=>{
            console.log(err);
          });
        }
      },
      {
        label: 'No',
        onClick: () => { console.log('no selected'); }
      }
    ]
  });
  
}
  getsubs=(id)=>{
    let token = localStorage.getItem('sessionToken')
    var body = {
      "id": id
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getSubscription';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.post(url,body,header).then(responseJson => { 
      console.log('============getSubscription========================');
      console.log(responseJson.data);
      console.log('==============getSubscription======================');
      this.setState({getsubslist:responseJson.data.subscriptions,servicesCount:responseJson.data.servicesCount})
    }).catch(err=>{
      console.log(err);
      alert('Network Error')
    });
  }

  getpaymentmethod=(id)=>{
    let token = localStorage.getItem('sessionToken')
    var body = {
      "id": id
    }
    const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/getPaymentMethods';
    const header={headers: {
      'Authorization': `Bearer ${token}`
    }} 
    axios.post(url,body,header).then(responseJson => {
      console.log('==================getpaymentmethod==================');
      console.log(responseJson.data);
      console.log('=====================getpaymentmethod===============');   
      if(responseJson.data.applicationStatusCode==-1){
        this.setState({getpaymentmethodlist:[]})
      }
      else{
        this.setState({getpaymentmethodlist:responseJson.data})
      }
    }).catch(err=>{
      console.log(err);
      alert('Network Error')
    });
  }

  onValueChange(event) {
    console.log(event.target.value)
    this.setState({
      selectedOption: event.target.value
    });
  }

  updatesubscription=()=>{
    this.setState({
      progress:50
    })
    var total = this.total((this.state.getsubslist.planSubscriptionType=='ANNUALY'?this.state.getsubslist.paymentPlan.prices[0].priceAnnually:this.state.getsubslist.paymentPlan.prices[0].priceMonthly)*this.state.noofbranches,this.state.getsubslist.paymentPlan.prices[0].perUserPrice*this.state.noofusersp,this.state.getsubslist.paymentPlan.prices[0].perSMSPrice*this.state.noofsms);
    var totaltax = this.tatalvtax((this.state.getsubslist.planSubscriptionType=='ANNUALY'?this.state.getsubslist.paymentPlan.prices[0].priceAnnually:this.state.getsubslist.paymentPlan.prices[0].priceMonthly)*this.state.noofbranches,this.state.getsubslist.paymentPlan.prices[0].perUserPrice*this.state.noofusersp,this.state.getsubslist.paymentPlan.prices[0].perSMSPrice*this.state.noofsms);
    let token = localStorage.getItem('sessionToken');
      var body = {
        "serviceCenterId" : this.state.getsubslist.serviceCenter.serviceCenterId,
        "kioskEnabled": this.state.getsubslist.paymentPlan.kioskEnabled,
        "EasyPaymentEnabled": this.state.getsubslist.paymentPlan.easyPaymentEnabled,
        "advanceReportingEnabled": this.state.getsubslist.paymentPlan.advanceReportingEnabled,
        "noOfBranches": this.state.getsubslist.noOfBranches+this.state.noofbranches,
        "noOfUsersPerBranch": this.state.getsubslist.noOfUsersPerBranch+this.state.noofusersp,
        "noOfSMS": this.state.getsubslist.noOfSMS+this.state.noofsms,
        "chargesRemaining":  this.state.getsubslist.chargesTotal-this.state.getsubslist.chargesPaid,
        "totalChargesBeforeTax": total+(this.state.getsubslist.chargesTotal-this.state.getsubslist.chargesPaid),
        "totalCharges": totaltax+(this.state.getsubslist.chargesTotal-this.state.getsubslist.chargesPaid),
      }    
      const url = 'https://apicall.taboor.ae/taboor-qms/adminpanel/serviceCenter/upgradeSubscription';
      const header={headers: {
        'Authorization': `Bearer ${token}`
      }} 
      axios.post(url,body,header).then(responseJson => {
        console.log(responseJson.data);
        this.setState({
          progress:100
        })
        this.setState({noofbranches:0, noofusersp:0, noofsms:0})
        this.getsubs(this.state.getsubslist.serviceCenter.serviceCenterId);
      }).catch(err=>{
        console.log(err);
      });
      this.props.history.push('/centerProfile')
  }
  total=(b,u,s)=>{
  
    return b+u+s;
  }
  tatalvtax=(b,u,s)=>{
    return (b+u+s)+((b+u+s)*5/100);
  }

  render() {
    return (
      <div className="animated fadeIn">
         <LoadingBar
        color='#2f49da'
        progress={this.state.progress}
        onLoaderFinished={() => this.setState({
          progress:0
        })}
        />
        <DefaultHeader/>
        <h4>Payment</h4>
        <Card>
        <CardBody className="p-0">
          <Table responsive className="bg-white m-0 table-light table-card table-hover">
            <thead>
            <tr >
              <th width="30"></th>
              <th className="text-left" width="27%">Card</th>
              <th width="30%">Card Type</th>
              <th>Network</th>
              <th width="50"></th>
            </tr>
            </thead>
            <tbody>
              {this.state.getpaymentmethodlist.map((item,index)=>(
                <tr key={index}>
                  <td>
                    <input type ="radio" 
                    value={item.bankCardId}          
                    checked={this.state.selectedOption == item.bankCardId}
                    onChange={this.onValueChange}/>
                  </td>
                  <td>
                    {item.cardNumber}<br></br>
                    <small className="text-success">Valid</small>
                  </td>
                  <td className="text-center">{item.cardType} Card</td>
                  <td className="text-center">
                    {item.paymentMethod.paymentMethodName=="VISA" && (
                    <img src={require('../assets/img/visa.png')} style={{objectFit:'contain'}}/>
                    )}
                    {item.paymentMethod.paymentMethodName=="MasterCard" && (
                    <img src={require('../assets/img/master.png')} style={{objectFit:'contain'}}/>
                    )}
                    {item.paymentMethod.paymentMethodName=="Paypal" && (
                    <img src={require('../assets/img/paypal.png')} style={{objectFit:'contain'}}/>
                    )}
                  </td>
                </tr>
              ))}
              </tbody>
          </Table>

          {(!global.centerid)&&(this.state.isShowServiceCenterProfileEditPrev==true) && (
             <Button block color="warning"  onClick={()=>{this.updatesubscription()}}>Checkout</Button>
            )}
        </CardBody>
      </Card>

        </div>
    );
  }
}
export default Payment;