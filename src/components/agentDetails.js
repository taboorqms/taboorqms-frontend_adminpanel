import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap';
import { Button, FormGroup,Label,Input, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import {Line, Pie } from 'react-chartjs-2';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import LoadingBar from 'react-top-loading-bar'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
const DefaultHeader = React.lazy(() => import('./defaultHeader'));
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
var canvas = document.createElement('canvas');
const ctx = canvas.getContext("2d");
const gradient = ctx.createLinearGradient(0, 0, 500, 0);
gradient.addColorStop(0, 'orange');
gradient.addColorStop(0.5, 'red');
gradient.addColorStop(1, 'green');
ctx.fillStyle = gradient;
ctx.fillRect(20, 20, 150, 100);
class Breadcrumbs extends Component {
  totalPriv=0
  id=0
  branchId=0
  tempAgentDetails=[]
  tempAssigendCounter=[]
  tempAllPrivileges=[]
  selectedPrivileges=[]
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
    this.state = {
      progress:0,
      card1:false,
      assignedCounters:{},
      selectedWeek:'1 week',
      isShowAgentEditPrev:false,
      isAgentModal:false,
      agentLastLoginTime:'',
      agentLastLoginDate:'',
      agentId:0,
      allPrivileges:[],
      selectedAvialableBranchCounterId:null,
      agentCounter:0,
      agentName:0,
      agentLogin:'',
      isLogin:false,
      agentBranchName:'',
      agentEmail:'',
      agentServiceTime:0,
      agentTotalWorkTime:0,
      agentTotalTicketServed:0,
      userEmail:'',
      userPhoneNo:'',
      dropdownOpen: false,
      radioSelected: 2,
      NextAgentId:'',
      avialableBranchCounter:[],
      errorMsg:{},
      line : {
        labels: [],
        datasets: [
          {
            borderColor: gradient,
            pointRadius: 0,
            fill: false,
            borderWidth: 2,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: []
          }
        ]
      },
      pie:{
        labels: ["Satisfied", "Average", "Poor"],
        datasets: [
          {
            label: ["Satisfied","Average","Poor"],
            data: [],
            backgroundColor: [
              '#07cf98',
              '#f7b62b',
              '#f72c39',
            ],
            hoverBackgroundColor: [
              '#07cf98',
              '#f7b62b',
              '#f72c39',
            ],
            hoverBorderColor:[
              '#07cf98',
              '#f7b62b',
              '#f72c39',
            ]
          }],
      },
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }
  componentDidMount()
 {
  if(localStorage.getItem('isShowArabicLanguage')=='true')
  {
    global.isShowArabicLanguage=true
  }
  else
  {
    global.isShowArabicLanguage=false
  }
  var prev=JSON.parse(localStorage.getItem('privileges'))
    var isPrev=false
    if(prev!=null)
    {
      for(var i=0;i<prev.length;i++)
      {
        if(prev[i]=='Agent_Edit')
        {
          this.setState({isShowAgentEditPrev:true})
        }
      }
    }
   var data=this.props.location
  this.id=parseInt(data.agentId)
   this.agentDetails(parseInt(data.agentId))
   
 }
 deleteAgent()
 {
  let token = localStorage.getItem('sessionToken');
  confirmAlert({
    title: '',
    message: 'Do you want to delete agent?',
    buttons: [
      {
        label: 'Yes',
        onClick: () => {
          this.setState({ progress:50 })
          fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/delete', {
            method: 'POST',
            body:JSON.stringify({
              'ids':[parseInt(this.id)]
            }),
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            },
          }).then(response => response.json()).then(responseJson => {
            if(responseJson.applicationStatusCode==0)  {
              this.setState({ progress:100 })
              global.isBranchAgentCounterUpdated.next(true)
              this.props.history.push('/agent-list')
            }
            else{
              this.setState({ progress:100 })
              toaster.notify(responseJson.devMessage, { duration:5000 });
            }
          })
        }
      },
      {
        label: 'No',
        onClick: () => { console.log('no selected'); }
      }
    ]
  });
 }
 agentDetails(id)
 {
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getDetails', {
    method: 'POST',
    body:JSON.stringify({
     "id": parseInt(id)
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
     
      if(responseJson.agentList[0].agent.serviceCenterEmployee.user.lastLoginTime!=null)
      {
        var createdDate= new Date(responseJson.agentList[0].agent.serviceCenterEmployee.user.lastLoginTime) 
        var sHours = createdDate.getHours();
        var sMinutes = createdDate.getMinutes();
        var sDate = createdDate.getDate();
        var sMonth = createdDate.getMonth()+1;
        var sYear = createdDate.getFullYear();
        var date= sDate+'.'+sMonth+'.'+sYear
        var ampm = sHours >= 12 ? 'PM' : 'AM';
        sHours = sHours % 12;
        sHours = sHours ? sHours : 12;
        sHours = sHours < 10 ?'0' + sHours : sHours;
        sMinutes = sMinutes < 10 ? '0' + sMinutes : sMinutes;
        var strTime = sHours + ':' + sMinutes + ' ' + ampm;
        this.setState({
          agentLastLoginDate:date,
          agentLastLoginTime:strTime,
        })
      }
      else
      {
        this.setState({
          agentLastLoginDate:'',
          agentLastLoginTime:'',
        })
      }
      if(responseJson.agentList[0].agent.loginStatus==true)
      {
        this.setState({
          isLogin:true
        })
      }
      else{
        this.setState({
          isLogin:false
        })
      }
     


        var hms = responseJson.agentList[0].agent.averageServiceTime;  
        var a = hms.split(':'); 
        var minutes = (+a[0]) * 60 + (+a[1]); 

        var hmss = responseJson.agentList[0].agent.totalWorkHours;  
        var a = hmss.split(':'); 
        var minutess =(+a[0]) * 60
        this.totalPriv=responseJson.agentList[0].assignedPrivileges.length
        if(responseJson.agentList[0].assignedBranchCounter!=null)
        {
          this.setState({
            agentCounter:responseJson.agentList[0].assignedBranchCounter.counterNumber
          })
        }
        else{
          this.setState({
            agentCounter:''
          })
        }
        this.tempAgentDetails=responseJson.agentList[0]
        var date = new Date();
        var last = new Date(date.getTime() - (6 * 24 * 60 * 60 * 1000))
        this.getStatictics(last,new Date())
        if(responseJson.agentList[0].agent.branch!=null)
        {
          this.branchId=responseJson.agentList[0].agent.branch.branchId
          this.setState({
            agentBranchName:responseJson.agentList[0].agent.branch.branchName,
          })
        }
        else{
          this.branchId=0
        }
        
        if(responseJson.agentList[0].assignedBranchCounter!=null)
        {
          this.setState({
            assignedCounters:responseJson.agentList[0].assignedBranchCounter,
          })
        }
        else{
          this.setState({
            assignedCounters:null,
          })
        }
        this.setState({
          
          agentId:responseJson.agentList[0].agent.serviceCenterEmployee.employeeNumber,
          agentName:responseJson.agentList[0].agent.serviceCenterEmployee.user.name,
          agentTotalTicketServed:responseJson.agentList[0].agent.totalTicketServed,
          agentServiceTime:minutes,
          agentTotalWorkTime:minutess,
         
          userEmail:responseJson.agentList[0].agent.serviceCenterEmployee.user.email,
          // userPhoneNo:responseJson.agentList[0].agent.serviceCenterEmployee.user.email,
          allPrivileges:responseJson.agentList[0].assignedPrivileges
          
        })
    }
   

  })
 }
 getStatictics(startDate,endDate){
  let token = localStorage.getItem('sessionToken')
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getStatistics', {
    method: 'POST',
    body:JSON.stringify({
      "idList": [this.id],
      "endDate": endDate,
      "startDate": startDate
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responsejson => {
    if(responsejson.applicationStatusCode==0)
    {
      var localLine={
        labels: [],
        datasets: [
          {
            borderColor: gradient,
            pointRadius: 0,
            fill: false,
            borderWidth: 2,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: []
          }
        ]

      }
      if(responsejson.values!=null)
      {
        var tempSatisfied=0
        var tempAvg=0
        var tempPoor=0
        for(var i=0;i<responsejson.values.length;i++)
        {
          if(responsejson.values[i].key=='SATISFIED' || responsejson.values[i].key=='AVERAGE'|| responsejson.values[i].key=='POOR' )
          {
            if(responsejson.values[i].key=='SATISFIED')
            {
              tempSatisfied=parseInt(responsejson.values[i].value)
              this.state.pie.datasets[0].data[0]=parseInt(responsejson.values[i].value);
              this.setState({
                staisfied:parseInt(responsejson.values[i].value)
              })
            }
            if(responsejson.values[i].key=='AVERAGE')
            {
              tempAvg=parseInt(responsejson.values[i].value);
              this.state.pie.datasets[0].data[1]=parseInt(responsejson.values[i].value);
              this.setState({
                average:parseInt(responsejson.values[i].value)
              })
            }
            if(responsejson.values[i].key=='POOR')
            {
              tempPoor=parseInt(responsejson.values[i].value);
              this.state.pie.datasets[0].data[2]=parseInt(responsejson.values[i].value)
              this.setState({
                poor:parseInt(responsejson.values[i].value)
              })
            }
          }
          else
          {
            localLine.labels.push(responsejson.values[i].key)
            localLine.datasets[0].data.push(responsejson.values[i].value)
          }
        } 
        var t=0
        for(var j=0;j<localLine.datasets[0].data.length;j++)
        { 
    
          t=t + parseInt(localLine.datasets[0].data[j])
        }
        this.setState({
          line:localLine,
          totalTempTickets:t,
          pie:this.state.pie
        }) 
        var total=tempAvg+tempSatisfied+tempPoor
        this.setState({
          totalTempRatings:total
        }) 
      }
     
    }
   

  })
  }

 editAgent()
 {
  this.setState({
    isAgentModal: true
  });
  this.tempAssigendCounter=[]
  this.tempAllPrivileges=[]
  let token = localStorage.getItem('sessionToken')
  this.getAgentPrevileges()
  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/branchCounter/get/branch', {
    method: 'POST',
    body:JSON.stringify({
      'id':parseInt(this.branchId)
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
  })
  .then(response => response.json())
  .then(responseJson => {
    if(responseJson.applicationStatusCode==0)
    {
      for(var i=0;i<responseJson.branchCounterList.length;i++)
      {
        this.tempAssigendCounter.push(responseJson.branchCounterList[i])
      }
      var isMatch=false
      if(this.tempAgentDetails.assignedBranchCounter!=null)
      {
        for(var x=0;x<this.tempAssigendCounter.length;x++)
        {
          if(this.tempAssigendCounter[x].branchCounter.counterId==this.tempAgentDetails.assignedBranchCounter.counterId)
          {
            isMatch=true
            break;
          }
          else{
              isMatch=false
            }
          
        }
        if(isMatch==false)
        {
          this.tempAssigendCounter.push({'branchCounter':this.tempAgentDetails.assignedBranchCounter})
          // this.tempAssigendCounter[this.tempAssigendCounter.length-1]['isDisabled']=false
        }
        this.setState({
          selectedAvialableBranchCounterId:this.tempAgentDetails.assignedBranchCounter.counterId,
          avialableBranchCounter:this.tempAssigendCounter
        })
      }
      else
      {
        if(this.tempAssigendCounter.length!=0)
        {
          this.setState({
            selectedAvialableBranchCounterId:this.tempAssigendCounter[0].branchCounter.counterId,
            avialableBranchCounter:this.tempAssigendCounter
          })
        }
       
      }
      

    
      this.setState({
        NextAgentId:this.tempAgentDetails.agent.serviceCenterEmployee.employeeNumber,
        agentPk:this.tempAgentDetails.agent.agentId,
      })
      this.updateAgentName(this.tempAgentDetails.agent.serviceCenterEmployee.user.name)
      this.updateAgentEmail(this.tempAgentDetails.agent.serviceCenterEmployee.user.email)
      }
      else{
        this.setState({
          NextAgentId:this.tempAgentDetails.agent.serviceCenterEmployee.employeeNumber,
          agentPk:this.tempAgentDetails.agent.agentId,
        })
        this.updateAgentName(this.tempAgentDetails.agent.serviceCenterEmployee.user.name)
        this.updateAgentEmail(this.tempAgentDetails.agent.serviceCenterEmployee.user.email)
      }
    })
 }
 getAgentPrevileges()
  {
    this.tempAllPrivileges=[]
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/getPrivileges', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        for(var i=0;i<responseJson.privilegeList.length;i++)
        {
          var key=responseJson.privilegeList[i].privilegeName
            var replaced = key.split('_').join(' ');
            responseJson.privilegeList[i].privilegeName=replaced
          responseJson.privilegeList[i]['isChecked']=false
        }
        for(var y=0;y<responseJson.privilegeList.length;y++)
        {
          for(var j=0;j<this.tempAgentDetails.assignedPrivileges.length;j++)
          {
            if(this.tempAgentDetails.assignedPrivileges[j].privilegeId==responseJson.privilegeList[y].privilegeId)
            {
              responseJson.privilegeList[y]['isChecked']=true
              // this.selectedPrivileges
            }
            
          }
        }
        this.tempAllPrivileges.push(responseJson.privilegeList)
        this.setState({allPrivileges:this.tempAllPrivileges[0]})
        
      }
    })
  }
  changePriveleges(data)
  {
    for(var i=0;i<this.tempAllPrivileges[0].length;i++)
    {
      if(data.privilegeId==this.tempAllPrivileges[0][i].privilegeId)
      {
        if(this.tempAllPrivileges[0][i]['isChecked']==true)
        {
          this.tempAllPrivileges[0][i]['isChecked']=false
          
        }
        else
        {
          this.tempAllPrivileges[0][i]['isChecked']=true
          
          // this.selectedPrivileges.push(this.tempAllPrivileges[i])
        }

      }
    }
    this.setState({allPrivileges:this.tempAllPrivileges[0]})
  }
 resetAgent()
{
  for(var i=0;i<this.selectedPrivileges.length;i++)
  {
     this.selectedPrivileges[i]['isChecked']=false
  }
 this.selectedPrivileges=[]
 this.tempAllPrivileges=[]
 this.setState({agentEmail:'',agentName:'',formAgentValid:false,allPrivileges:[]})
 this.getAgentPrevileges()
}
closeAgentModel()
{
  this.selectedPrivileges=[]
  this.tempAllPrivileges=[]
  this.setState({agentEmail:'',agentName:'',formAgentValid:false,allPrivileges:[],isAgentModal:false})
}
addUpdateAgent()
{
 
  for(var i=0;i<this.tempAllPrivileges[0].length;i++)
  {
    if(this.tempAllPrivileges[0][i]['isChecked']==true)
    {
      var key=this.tempAllPrivileges[0][i].privilegeName
      var replaced = key.split(' ').join('_');
      this.tempAllPrivileges[0][i].privilegeName=replaced
      this.selectedPrivileges.push(this.tempAllPrivileges[0][i])
    }
  }
  if(this.selectedPrivileges.length==0)
  {
   toaster.notify('Kindly select atleast one privilage',{
    duration:5000
   })
     return
  }
  for(var i=0;i<this.selectedPrivileges.length;i++)
  {
      delete this.selectedPrivileges[i]['isChecked']
  }
  
  this.setState({
    formAgentValid:false,progress:50
  })
  let token = localStorage.getItem('sessionToken')

  fetch('https://apicall.taboor.ae/taboor-qms/adminpanel/agent/update', {
  method: 'POST',
  body:JSON.stringify({
    "agentId": this.state.NextAgentId,
    "agentName": this.state.agentName,
    "agentPk": this.state.agentPk,
    "assignedCounterId": this.state.selectedAvialableBranchCounterId,
    "branchId": parseInt(this.branchId),
    "email": this.state.agentEmail,
    "privileges":this.selectedPrivileges,
    "updateStatus": 1
    }),
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        global.isNotificationUpdated.next(true)
        this.selectedPrivileges=[]
        this.setState({
          formAgentValid:true,progress:100
        })
        this.closeAgentModel()
       this.agentDetails(this.id)
      }
      else{
        this.setState({
          formAgentValid:true,progress:100
        })
        toaster.notify(responseJson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
      }
    })
}
updateAgentName = (agentName) => {
  this.setState({agentName}, this.validateAgentName)
}
validateAgentName = () => {
  const {agentName} = this.state;
  let agentNameValid = true;
  let errorMsg = {...this.state.errorMsg}

  if (agentName.length == 0 ||agentName=='' ) {
    agentNameValid = false;
    errorMsg.agentName = 'Required'
  }
  // else if(!/^[a-zA-Z\s]*$/.test(agentName))
  // {
  //   agentNameValid = false;
  //   errorMsg.agentName =  'Only alphabets are allowed'
  // }
  this.setState({agentNameValid, errorMsg}, this.validateAgentForm)
}
updateAgentEmail= (agentEmail) => {
  this.setState({agentEmail}, this.validateAgentEmail)
}
validateAgentEmail = () => {
  const {agentEmail} = this.state;
  let agentEmailValid = true;
  let errorMsg = {...this.state.errorMsg}

  // checks for format _@_._
  if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(agentEmail)){
    agentEmailValid = false;
    errorMsg.agentEmail = 'Invalid email format'
  }

  this.setState({agentEmailValid, errorMsg}, this.validateAgentForm)
}
validateAgentForm = () => {
  const {agentEmailValid, agentNameValid} = this.state;
  this.setState({
    formAgentValid: agentEmailValid && agentNameValid
  })
}
changeDays=e=>{
  this.setState({selectedWeek: e.currentTarget.textContent})
  {
    if(e.currentTarget.textContent=='1 week')
    {
      var date = new Date();
      var last = new Date(date.getTime() - (6 * 24 * 60 * 60 * 1000))
      this.getStatictics(last,new Date())
    }
    else if(e.currentTarget.textContent=='2 week'){
      var date = new Date();
      var last = new Date(date.getTime() - (13 * 24 * 60 * 60 * 1000))
      this.getStatictics(last,new Date())
    }
    else if(e.currentTarget.textContent=='1 month'){
      var oneMonthAgo = new Date(
        new Date().getFullYear(),
        new Date().getMonth() - 1, 
        new Date().getDate()
    );
      this.getStatictics(oneMonthAgo,new Date())
    }
    else if(e.currentTarget.textContent=='6 month')
    {
      var oneMonthAgo = new Date(
        new Date().getFullYear(),
        new Date().getMonth() - 6, 
        new Date().getDate()
    );
      this.getStatictics(oneMonthAgo,new Date())
    }
  }
  
}
  render() {
    return (
      <div className="animated fadeIn">
         <LoadingBar
          color='#2f49da'
          progress={this.state.progress}
          onLoaderFinished={() => this.setState({
            progress:0
          })}
        />
      <DefaultHeader/>
        <Row className="mb-3 hidden">
          <Col sm="6">
            <div className="float-left mr-3">
              <span className="badge badge-primary font-2xl p-2 bg-box">A</span>
            </div>
            <div className="float-left">
              <p className="text-warning mb-0">{this.state.agentId} </p>
             {this.state.agentCounter!='' && <h4 className="mb-1">{this.state.agentName} <sup className="up-dot">
    {this.state.isLogin==true && <i className="icon2-ellipse text-success"></i> }
    {this.state.isLogin==false && <i className="icon2-ellipse text-danger"></i> }
               </sup> <small>
               <span className="badge badge-success ml-2">Counter {this.state.agentCounter}</span></small></h4>}
             {this.state.agentCounter=='' && <h4 className="mb-1">{this.state.agentName} 
             <sup className="up-dot pl-2">
             {this.state.isLogin==true && <i className="icon2-ellipse text-success"></i> }
    {this.state.isLogin==false && <i className="icon2-ellipse text-danger"></i> }
               </sup> 
             <small>
               <span className="badge badge-danger ml-2">Nil</span></small></h4>}

             {this.state.agentLastLoginDate!='' && <p className="text-light-grey mb-1"><i className="icon2-calendar"></i> Last Login {this.state.agentLastLoginTime} - {this.state.agentLastLoginDate}</p> } 
             {this.state.agentLastLoginDate=='' && <p className="text-light-grey mb-1"><i className="icon2-calendar"></i> Last Login Nil</p> } 

              <p className="text-light-grey"><i className="icon2-email-envelop"></i> {this.state.userEmail}</p>
            </div>
            <div className="clearfix"></div>
          </Col>

          <Col sm="6" className="text-right">
          
          {this.state.isShowAgentEditPrev==true &&<button className="btn btn-outline-secondary ml-2" onClick={()=>this.deleteAgent()}>Delete <i className="icon2-trash-remove" ></i></button>}
          {this.state.isShowAgentEditPrev==true && <button className="btn btn-outline-secondary ml-2" onClick={()=>this.editAgent()}>Edit <i className="icon2-edit-pencil" ></i></button>}
          {/* {this.state.isShowAgentEditPrev==true && <button className="btn btn-outline-secondary ml-2">More <i className="icon2-menu-options font-sm"></i></button>} */}
           
          </Col>
        </Row>


        <Row className="mb-3">

          <Col xs="12" sm="6" lg="3">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                        <i className="icon2-branches font-3xl text-light-grey"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p className="mb-2 text-light-grey">Branch &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; </p>
                      {this.branchId!=0 && <h5 className="mb-2">{this.state.agentBranchName}&nbsp;</h5>}
                      {this.branchId==0 && <h5 className="mb-2">Nil&nbsp;</h5>}

                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                        <i className="icon2-pie-chart1 font-3xl text-light-grey"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p className="mb-2 text-light-grey text-truncate">Average Handle Time</p>
                      <h5 className="mb-2">{this.state.agentServiceTime} min</h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                        <i className="icon2-wall-clock font-3xl text-light-grey"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p className="mb-2 text-light-grey text-truncate">Total Work Time</p>
                      <h5 className="mb-2">{this.state.agentTotalWorkTime} hours</h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="card-line">
              <CardBody>
                
                <div className="row">
                  <div className="col-2">
                    <div className="widget-icon">
                        <i className="icon2-recipt font-3xl text-light-grey"></i>
                      </div>
                  </div>
                  <div className="col-10">
                    <div className="widget-title">
                      <p className="mb-2 text-light-grey text-truncate">Total Tickets Served</p>
                      <h5 className="mb-2">{this.state.agentTotalTicketServed} Tickets</h5>
                    </div>
                  </div>
                   </div>

              </CardBody>
            </Card>
          </Col>
        </Row>
        


        <Row>
          <Col sm="8">


          <Card className="mb-0">
            
            <CardBody className="text-left">
              <h4 className="float-left">Performance</h4>
              <ButtonDropdown style={{ float: 'right' }} id="card1" isOpen={this.state.card1} toggle={() => { this.setState({ card1: !this.state.card1 }); }}>
                <DropdownToggle caret className="p-0 mb-0 text-light-grey" color="transparent">
                {this.state.selectedWeek}
                </DropdownToggle>
                <DropdownMenu right>
               
                  <DropdownItem onClick={this.changeDays} >1 week</DropdownItem>
                  <DropdownItem onClick={this.changeDays}>2 week</DropdownItem>
                  <DropdownItem onClick={this.changeDays}>1 month</DropdownItem>
                  <DropdownItem onClick={this.changeDays}>6 month</DropdownItem>

                </DropdownMenu>
              </ButtonDropdown>
              <div className="clearfix"></div>
              <div className="chart-wrapper line-graph" >
                <Line data={this.state.line}  
                  options={{
                    legend:{
                      display:false
                    },
                    scales: {
                    yAxes: [{
                        ticks: {
                          fontColor: 'white'
                        },
                        gridLines: {
                          display: true
                        },
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: 'grey'
                        },
                        gridLines: {
                          display: false
                        },
                    }]
                    }
                  }}
                />
              </div>
            </CardBody>
          </Card>

         
          </Col>
          <Col sm="4">
          <Card>
              
              <CardBody>
                
                <Row className="mb-3">
                  <Col sm="10">
                      {this.state.assignedCounters!=null && <h4>Assigned Counters . 1</h4>}
                      {this.state.assignedCounters==null && <h4>Assigned Counters . 0</h4>}
                  </Col>
                  <Col sm="2" className="text-right">
                  {/* {this.state.isShowAgentEditPrev==true && <button className="btn btn-outline-warning ml-2 btn-sm"><i className="icon2-addition-sign"></i> Add</button>} */}
                  </Col>
                </Row>
                

                {this.state.assignedCounters!=null && <Card className="card-box">
                  <CardHeader>
                    <h5 className="text-primary float-left">{this.state.assignedCounters.counterNumber}</h5>
                    <div className="card-header-actions float-right">
                    </div>
                  </CardHeader>
                  
                </Card>}
              </CardBody>
            </Card>
          


         
          


            <Card className="mb-0">
            
            <CardBody className="text-left">
              <div className="float-left">
                <h4 className="text-light-grey">{this.state.totalTempRatings} <sub>Votes</sub></h4>
              </div>
              <div className="float-right" style={{width:'40%'}}>
                <Pie data={this.state.pie} options={{legend:{display:false},tooltips:{enabled:false}}}/>
              </div>
              <div className="clearfix"></div>
              <h5>Service Ratings</h5>
              <Row>
                <Col sm="4" className="text-center f20"><i className="icon2-smile text-success"></i> {this.state.staisfied}</Col>
                <Col sm="4" className="text-center f20"><i className="icon2-smile text-warning"></i> {this.state.average}</Col>
                <Col sm="4" className="text-center f20"><i className="icon2-smile text-danger"></i> {this.state.poor}</Col>
              </Row>
            </CardBody>
          </Card>

          </Col>
        </Row>

        <Modal isOpen={this.state.isAgentModal} className={'modal-lg ' + this.props.className}>
            {global.isShowArabicLanguage==false && <ModalHeader>Edit Agent</ModalHeader>}
            {global.isShowArabicLanguage==true && <ModalHeader>تحرير الوكيل</ModalHeader>}
          <ModalBody>


                  <Row className="mt-3">
                  <Col sm="6">
                <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Agent ID</Label>}
                    {global.isShowArabicLanguage==true &&<Label className="text-light-grey">رمز العميل</Label>}
                    <Input type="text" placeholder="Service Center Name" autoComplete="username" readOnly value={this.state.NextAgentId} />
                    {global.isShowArabicLanguage==false &&<small><span className="text-danger">*</span> Generated by System</small>}
                    {global.isShowArabicLanguage==true &&<small><span className="text-danger">*</span> تم إنشاؤها بواسطة النظام</small>}
                  </FormGroup>
                </Col>
                <Col sm="6">
                <FormGroup className="input-line">
                    {global.isShowArabicLanguage==false && <Label className="text-light-grey">PIN</Label>}
                    {global.isShowArabicLanguage==true && <Label className="text-light-grey">رقم التعريف الشخصي</Label>}
                    <Input type="text" placeholder="Service Center Name" autoComplete="username" readOnly value='....' />
                    {global.isShowArabicLanguage==false && <small ><span className="text-danger">*</span> Changed on first login</small>}
                    {global.isShowArabicLanguage==true && <small ><span className="text-danger">*</span> يستبدل عند أول تسجيل دخول</small>}
                  </FormGroup>
                </Col>
              </Row>
              <FormGroup className="input-line">
                    <Label className="text-light-grey">Assigned Counters</Label>
                    <select className="form-control" disabled onChange={this.changeAssigendCounter} value={this.state.selectedAvialableBranchCounterId}>
                    {this.state.avialableBranchCounter.map(service => (
                      <option value={service.branchCounter.counterId} key={service.branchCounter.counterId}>
                        {service.branchCounter.counterNumber}
                      </option>
                    ))}
                    </select>
                  </FormGroup>
                 
               <FormGroup className="input-line">
                    <Label className="text-light-grey">Name</Label>
                    <Input type="text" placeholder="Agent Name" 
                     value={this.state.agentName} onChange={(e) => this.updateAgentName(e.target.value)} />
                     < ValidationMessage valid={this.state.agentNameValid} message={this.state.errorMsg.agentName} />
               </FormGroup>
              
               <FormGroup className="input-line">
                  <Label className="text-light-grey">Email</Label>
                  <Input type="email" placeholder="Email" autoComplete="username" name="email" id="exampleEmail"
                  value={this.state.agentEmail} readOnly />
                  
                </FormGroup>
                  <FormGroup className="input-line">
                    <Label className="text-light-grey">Branch</Label>
                    <Input type="text" autoComplete="username" readOnly value={this.state.agentBranchName} />
                  </FormGroup>
                 <div className="mb-2">
                 {global.isShowArabicLanguage==false &&<Label className="text-light-grey">Privilages</Label>}
                  {global.isShowArabicLanguage==true &&<Label className="text-light-grey">الصلاحيات</Label>}
                  {this.state.allPrivileges.map(prev => (
                    <div className="checkbox">
                    <input type="checkbox"  checked={prev.isChecked} value={prev.privilegeId}  onChange={()=>this.changePriveleges(prev)}></input>
                    <label  data-content={prev.privilegeName}>{prev.privilegeName}</label>
                  </div>
                  ))}
                  </div>
                  



                  </ModalBody>
                  <ModalFooter>
                  <Button outline color="primary" onClick={()=>this.closeAgentModel()}>Cancel</Button>
                    {/* <Button outline color="primary" onClick={()=>this.resetAgent()}>Reset</Button> */}
                    <Button color="primary" disabled={!this.state.formAgentValid} onClick={()=>this.addUpdateAgent()}>Save</Button>
                  </ModalFooter>
                </Modal>
            
          


      


      </div>
    );
  }
}

export default Breadcrumbs;
