import React, { Component } from 'react';
import { Nav, NavItem, NavLink, Progress, TabContent, TabPane,Row, Col, ListGroup, ListGroupItem, Badge } from 'reactstrap';
import { AppAsideToggler, AppSidebarToggler } from '@coreui/react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { AppSwitch } from '@coreui/react'
import axios from "axios";
import toaster from "toasted-notes";
import TimeAgo from 'timeago-react';
import "toasted-notes/src/styles.css";
import moment from 'moment';
import LoadingBar from 'react-top-loading-bar'
const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class NotificationAside extends Component {
  tempNotifications=[]
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      progress:0,
      totalUnreadCount:0,
      allNotifications:[],
      activeTab: '1',
      userName:'',
      userImage:'',
      userRole:'',
      userPhoneNo:'',
      userEmail:'',
      userAddress:'',
      branchCount:0,
      agentCount:0,
      privilegeInfo:[]
    };
  }

  toggle(tab) {
    var temp=!this.state.isShow
      this.setState({
        isShow:temp
      });
  }
  getNotifications()
  {
    this.tempNotifications=[]
    let token = localStorage.getItem('sessionToken')
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/notification/user/all', {
      method: 'POST',
      body:JSON.stringify({
        "pageNo": 0,
        "limit": 10    
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    })
    .then(response => response.json())
    .then(responseJson => {
      if(responseJson.applicationStatusCode==0)
      {
        var counter=0
        for(var i=0;i<responseJson.userNotificationList.length;i++)
        {
          if(responseJson.userNotificationList[i].readStatus==false)
          {
            counter=counter+1
          }
          this.tempNotifications.push(responseJson.userNotificationList[i])
        }
        this.setState({
          allNotifications:responseJson.userNotificationList.reverse(),
          totalUnreadCount:counter
        })
      }
    })
  }
  readNotification(data)
  {
    
    if(data.readStatus==false)
    {
      this.setState({
        progress:50
      })
      let token = localStorage.getItem('sessionToken')
      fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/notification/user/update/readstatus', {
        method: 'POST',
        body:JSON.stringify({
         'ids':[parseInt(data.notificationId)]
        }),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
      })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.applicationStatusCode==0)
        {
          this.setState({
            progress:100
          })
          var counter=this.state.totalUnreadCount
          this.getNotifications()

          // counter=counter-1
          // for(var i=0;i<this.tempNotifications.length;i++)
          // {
          //   if(this.tempNotifications[i].notificationId==data.notificationId)
          //   {
          //     this.tempNotifications[i].readStatus=true
              
          //   }
          // }
          // this.setState({
          //   allNotifications:this.tempNotifications,
          //   totalUnreadCount:counter
          // })
        }
      })
    }
   
  }
 componentDidMount()
 {
  if(localStorage.getItem('isShowArabicLanguage')=='true')
  {
    global.isShowArabicLanguage=true
  }
  else
  {
    global.isShowArabicLanguage=false
  }
    this.getNotifications()
    global.isNotificationUpdated.subscribe(val => {
      if(val==true)
      {
        this.getNotifications()
      }
     
    });
 }
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <LoadingBar
        color='#2f49da'
        progress={this.state.progress}
        onLoaderFinished={() => this.setState({
          progress:0
        })}
        />
        <div className="notification-sidebar">
        <Nav tabs className="mb-2">
          <NavItem>
            <NavLink to="#" className={classNames({ active: this.state.isShow == true })}
                    //  onClick={() => {
                    //    this.toggle();
                    //  }}
                     >
                       <div className="text-light-grey">
                    {/* <i className="icon-arrow-right"></i>  */}
                    {global.isShowArabicLanguage==false && <span>Notifications</span>}
                    {global.isShowArabicLanguage==true && <span>إشعارات</span>}
                    <a className="float-right">
                      <Badge color="warning" className="badge-warning-dark">{this.state.totalUnreadCount}</Badge>
                      {/* <AppAsideToggler className="icon2-edit-pencil p-0" /> */}
                    </a>
                    <div className="clearfix"></div>
              </div>
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent >
          <TabPane  className="p-3">
                        <ul className="notifications-list">
                        {this.state.allNotifications.map(noti => (
                          <li onClick={()=>this.readNotification(noti)}>
                              {noti.readStatus==false &&<Row>
                                <Col sm="3" className="pt-3">
                                  <Badge color="warning" className="bg-box badge-warning-dark"><i className="icon2-username"></i></Badge>
                                </Col>
                                <Col sm="9">
                                  <p className="text-light-grey mb-1">
                                    {moment(noti.createdAt).fromNow()}
                                    {/* <TimeAgo
                                    datetime={noti.createdAt} 
                                    // locale='zh_CN'
                                  /> */}
                                  </p>
                                  <h5><span className="text-black">{noti.title}</span></h5>
                                  <p className="text-light-grey"><i className="icon2-branches mr-2"></i> {noti.message}</p>
                                </Col>
                              </Row>}
                              {noti.readStatus==true  &&<Row>
                                <Col sm="3" className="pt-3">
                                  <Badge color="warning" className="bg-box bg-gray-100" ><i className="icon2-username"></i></Badge>
                                </Col>
                                <Col sm="9">
                                <p className="text-light-grey mb-1">
                                {moment(noti.createdAt).fromNow()}
                                  {/* <TimeAgo
                                    datetime={noti.createdAt} 
                                    // locale='zh_CN'
                                  /> */}
                                  </p>
                                  <h5><span className="text-primary">{noti.title}</span></h5>
                                  <p className="text-light-grey mb-0"><i className="icon2-branches mr-2"></i> {noti.message}</p>
                                </Col>
                              </Row>}
                            </li>
                        ))}
                            
                            {/* <li>
                              <Row>
                                <Col sm="3" className="pt-3">
                                  <Badge color="warning" className="bg-box badge-warning-dark"><i className="icon2-username"></i></Badge>
                                </Col>
                                <Col sm="9">
                                  <p className="text-light-grey mb-1">6 sec ago</p>
                                  <h5>New Agent <span className="text-black">was added by</span> admin ABC</h5>
                                  <p className="text-light-grey mb-0"><i className="icon2-branches mr-2"></i> Branch name here</p>
                                </Col>
                              </Row>
                            </li>
                            <li>
                              <Row>
                                <Col sm="3" className="pt-3">
                                  <Badge color="warning" className="bg-box bg-gray-100"><i className="icon2-username"></i></Badge>
                                </Col>
                                <Col sm="9">
                                  <p className="text-light-grey mb-1">6 sec ago</p>
                                  <h5><span className="text-primary">New Agent</span> <span className="text-black">was added by</span> admin ABC</h5>
                                  <p className="text-light-grey mb-0"><i className="icon2-branches mr-2"></i> Branch name here</p>
                                </Col>
                              </Row>
                            </li>
                            <li>
                              <Row>
                                <Col sm="3" className="pt-3">
                                  <Badge color="warning" className="bg-box badge-warning-dark"><i className="icon2-username"></i></Badge>
                                </Col>
                                <Col sm="9">
                                  <p className="text-light-grey mb-1">6 sec ago</p>
                                  <h5>New Agent <span className="text-black">was added by</span> admin ABC</h5>
                                  <p className="text-light-grey mb-0"><i className="icon2-branches mr-2"></i> Branch name here</p>
                                </Col>
                              </Row>
                            </li> */}
                     </ul>
</TabPane>
</TabContent>
        {/* {this.state.isShow ==true &&  <TabContent >
          <TabPane  className="p-3">

                     <ul>
                     {this.state.allCenters.map(center => (
                       <li>
                       <Row>
                         <Col sm="3">
                           <Badge color="warning"><i className="icon2-username"></i></Badge>
                         </Col>
                         <Col sm="9">
                           <p>6 sec ago</p>
                           <h4>New Agent <span className="text-black">was added by</span> admin ABC</h4>
                           <p><i className="icon2-branches mr-2"></i> Branch name here</p>
                         </Col>
                       </Row>
                     </li>
                     ))} 
                     </ul>

          </TabPane>
        </TabContent>} */}
        </div>
      </React.Fragment>
    );
  }
}

NotificationAside.propTypes = propTypes;
NotificationAside.defaultProps = defaultProps;

export default NotificationAside;
