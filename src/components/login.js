import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col,FormGroup, Container, Form,Label, 
  Input, InputGroup, InputGroupAddon, InputGroupText, Row,DropdownItem,  DropdownMenu,  DropdownToggle,ButtonDropdown,
  Modal, ModalBody, ModalFooter, ModalHeader, } from 'reactstrap';
import { AppNavbarBrand } from '@coreui/react';
import logo from '../assets/img/brand/logo.png';
import toaster from "toasted-notes";
import "toasted-notes/src/styles.css";
import axios from "axios";
import adminNavigation from './adminNav';
import superAdminNavigation from './superAdminNav';
function ValidationMessage(props) {
  if (!props.valid) {
    return(
      <div className='error-msg' style={{ color: 'red' }} >{props.message}</div>
    )
  }
  return null;
}
class Login extends Component {
  constructor()
  {
    super()
    global.isShowArabicLanguage=false
    this.state={
      selectedLan:'English',
      navData: null,
      superAdminNavData:null,
      roleType:'',
      isShowRemember:false,
      email: '', emailValid: false,
      forgotEmail:'',forgotEmailValid:false,
      password: '', passwordValid: false,
      formValid: false,
      errorMsg: {},
      errorForgotMsg: {},
      formForgetValid: false,
      isForgotPassword:false
    }
    if(localStorage.getItem('sessionToken')!=undefined)
    {
      global.centerid=null;
      let token = localStorage.getItem('sessionToken');
      const url = 'https://apicall.taboor.ae/taboor-qms/usermanagement/user/logout';
      const header={headers: {
        'Authorization': `Bearer ${token}`
      }} 
      axios.get(url,header).then(responseJson => {
        if(responseJson.data.applicationStatusCode===0)
        {
          var type=localStorage.getItem('role')
          if(type!=undefined)
          {
            if(type.startsWith('Taboor'))
            {
              this.setState({superAdminNavData:superAdminNavigation})
              this.setState({
                roleType:'SuperAdmin'
              })
              var data=this.state.superAdminNavData
               data.items[0].badge.text=0
            }
            else {
              this.setState({navData:adminNavigation})
              this.setState({
                roleType:'Admin'
              })
              var data=this.state.navData
              data.items[1].badge.text=0
              data.items[2].badge.text=0
              data.items[3].badge.text=0
            }
          }
          
          localStorage.removeItem('sessionToken')
          localStorage.removeItem('refreshToken')
          localStorage.removeItem('role')
          localStorage.removeItem('privileges')
          localStorage.removeItem('branchId')

        }
      }).catch(err=>{
        console.log(err)
      });
    }
  }
  login()
  {
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/user/login', {
      method: 'POST',
      body:JSON.stringify({
        "deviceCode": 1,
        "deviceToken": global.deviceToken?global.deviceToken:null,
        "email": this.state.email,
        "password": this.state.password
      }),
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(response => response.json())
    .then(responsejson => {
      if(responsejson.applicationStatusCode==0)
      {
        localStorage.setItem('sessionToken',responsejson.sessionToken)
        localStorage.setItem('refreshToken',responsejson.refreshToken)
        localStorage.setItem('role',responsejson.role)
        localStorage.setItem('privileges',JSON.stringify(responsejson.privileges))
        console.log(responsejson)
        this.props.history.push('/')
       
      }
      else
      {

        if (responsejson.devMessage=="Payment Not Cleared"){
          this.props.history.push({
            
            pathname:'/paymentSummary',
            state: {
              email:this.state.email,
            },

    
          })

        }
        
        toaster.notify(responsejson.devMessage, {
          // position: "bottom-left", // top-left, top, top-right, bottom-left, bottom, bottom-right
          duration:5000 // This notification will not automatically close
        });
        // NotificationManager.error(responsejson.devMessage, 'Error');
      }
    })
  }
  changeRememberMe()
  {
    if(this.state.emailValid && this.state.passwordValid)
    {
      var temp=!this.state.isShowRemember
      // if(localStorage.getItem('isRemember')!=undefined)
      // {
      //   if(localStorage.getItem('isRemember')=='true')
      //   {
      //     localStorage.setItem('userEmail',this.state.email)
      //     localStorage.setItem('userPassword',this.state.password)
      //     localStorage.setItem('isRemember',true)
      //     this.setState({
      //       isShowRemember:temp
      //     })
      //   }
      //   else
      //   {
      //     localStorage.setItem('isRemember',false)
      //     this.setState({
      //       isShowRemember:temp
      //     })
      //   }
        
      // }
      // else
      // {
        if(temp==true)
        {
          localStorage.setItem('isRemember',true)
          localStorage.setItem('userEmail',this.state.email)
          localStorage.setItem('userPassword',this.state.password)
          this.setState({
            isShowRemember:temp
          })
        }
        else
        {
          localStorage.removeItem('isRemember')
          localStorage.removeItem('userEmail')
          localStorage.removeItem('userPassword')
          this.setState({
            isShowRemember:temp
          })
        }
       
      // }
    }

  }
  validateForm = () => {
    const {passwordValid, emailValid} = this.state;
    this.setState({
      formValid: passwordValid && emailValid 
    })
  }
  validateForgotForm = () => {
    const {forgotEmailValid} = this.state;
    this.setState({
      formForgetValid: forgotEmailValid 
    })
  }
  updateEmail = (email) => {
    this.setState({email}, this.validateEmail)
  }

  validateEmail = () => {
    const {email} = this.state;
    let emailValid = true;
    let errorMsg = {...this.state.errorMsg}

    // checks for format _@_._
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){
      emailValid = false;
      errorMsg.email = 'Invalid email format'
      this.setState({
        isShowRemember:false,
      })
      localStorage.removeItem('isRemember')
      localStorage.removeItem('userEmail')
      localStorage.removeItem('userPassword')
    }
    this.setState({emailValid, errorMsg}, this.validateForm)
  }
  updatePassword = (password) => {
    this.setState({password}, this.validatePassword)
  }

  validatePassword = () => {
    const {password} = this.state;
    let passwordValid = true;
    let errorMsg = {...this.state.errorMsg}

    // checks for format _@_._
    if (password==''|| password.length==0){
      passwordValid = false;
      errorMsg.password = 'Required'
      this.setState({
        isShowRemember:false,
      })
      localStorage.removeItem('isRemember')
      localStorage.removeItem('userEmail')
      localStorage.removeItem('userPassword')

    }
    this.setState({passwordValid, errorMsg}, this.validateForm)
  }
  updateForgotEmail = (forgotEmail) => {
    this.setState({forgotEmail}, this.validateForgotEmail)
  }
  forgotPasswordModel()
  {
    this.setState({
      isForgotPassword:true
    })
    this.updateForgotEmail(this.state.email)
  }
  closeForgetPasswordModel()
  {
    this.setState({
      isForgotPassword:false,forgotEmail:'',formForgetValid:false
    })
  }
  validateForgotEmail = () => {
    const {forgotEmail} = this.state;
    let forgotEmailValid = true;
    let  errorForgotMsg= {...this.state.errorForgotMsg}
  
    if (forgotEmail.length == 0 || forgotEmail=='') {
      forgotEmailValid = false;
      errorForgotMsg.forgotEmail = 'Required'
    }
    else if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(forgotEmail)){
      forgotEmailValid = false;
      errorForgotMsg.email = 'Invalid email format'
    }
  
    this.setState({forgotEmailValid, errorForgotMsg}, this.validateForgotForm)
  }
  forgotPassword()
  {
    fetch('https://apicall.taboor.ae/taboor-qms/usermanagement/password/forget', {
      method: 'POST',
      body:JSON.stringify({
        'email':this.state.forgotEmail,
      }),
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(response => response.json())
    .then(responsejson => {
      if(responsejson.applicationStatusCode==0)
      {
        this.closeForgetPasswordModel()
      }
      else
      {
        
        toaster.notify(responsejson.devMessage, {
          duration:5000 // This notification will not automatically close
        });
      }
    })
  }
  componentDidMount()
  {
    localStorage.setItem('isShowArabicLanguage',false)
    if(localStorage.getItem('isRemember')=='true')
    {
      this.updateEmail(localStorage.getItem('userEmail'))
      this.updatePassword(localStorage.getItem('userPassword'))
      this.setState({
        isShowRemember:true
      })
    }
  }
  changeLanguage=e=>{
    this.setState({selectedLan: e.currentTarget.textContent})
    if(e.currentTarget.textContent=='عربى')
    {
      global.isShowArabicLanguage=true
      localStorage.setItem('isShowArabicLanguage',true)
    }
    else
    {
      global.isShowArabicLanguage=false
      localStorage.setItem('isShowArabicLanguage',false)
    }
  }
  registerPg=()=>{
    console.log('==============registerPg======================');
    console.log('registerPg');
    console.log('===============registerPg=====================');
    this.props.history.push('/register');
  }
  render() {
    return (
      <div className="app flex-row align-items-center login-page">
        <Container>
          <Row className="justify-content-center">
            <Col md="9">
              <CardGroup className="card bg-primary">
              <Card className="text-white bg-primary pb-5 d-md-down-none" style={{ width: '50%', flex: 'inherit' }}>
                  <CardBody className="pl-5">
                  <ButtonDropdown className="mb-5" id="pagedrop" isOpen={this.state.pagedrop} toggle={() => { this.setState({ pagedrop: !this.state.pagedrop }); }}>
                    <DropdownToggle caret className="p-0 mb-0 text-white" color="transparent">
                    {this.state.selectedLan} &nbsp;
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem onClick={this.changeLanguage} >عربى</DropdownItem>
                      <DropdownItem onClick={this.changeLanguage} >English</DropdownItem>
                    </DropdownMenu>
                  </ButtonDropdown>
                    <div className="login-logo mb-3">
                      <AppNavbarBrand
                        full={{ src: logo, width: 250, alt: 'Taboor Logo' }}
                      />
                    </div>
                    { global.isShowArabicLanguage==false &&<p className="text-info mb-5">Queueing Reimagined.</p>}
                    { global.isShowArabicLanguage==true &&<p className="text-info mb-5">طابور بصورة مختلفة</p>}
                    { global.isShowArabicLanguage==false && <Button color="link" onClick={this.registerPg} className="mt-5 text-white text-underline" active tabIndex={-1} style={{fontSize:16}}>Register Here</Button>}
                    { global.isShowArabicLanguage==true && <Button color="link" onClick={this.registerPg} className="mt-5 text-white text-underline" active tabIndex={-1} style={{fontSize:16}}>سجل هنا</Button>}
                  </CardBody>
                </Card>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={()=>{this.login()}}>
                     { global.isShowArabicLanguage==false && <h3 className="text-primary mb-5">Login</h3> } 
                     { global.isShowArabicLanguage==true && <h3 className="text-primary mb-5">تسجيل الدخول</h3> } 
                      <FormGroup className="input-line">
                        <InputGroup className="input-line">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon2-username"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input type="text" placeholder="Email" autoComplete="username"  value={this.state.email} onChange={(e) => this.updateEmail(e.target.value)}/>                                   
                        </InputGroup>
                        <ValidationMessage valid={this.state.emailValid} message={this.state.errorMsg.email} />
                      </FormGroup>
                      <FormGroup className="input-line">
                        <InputGroup className="input-line">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon2-password"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input type="password" placeholder="Password" autoComplete="current-password"  value={this.state.password} onChange={(e) => this.updatePassword(e.target.value)}/>                                   
                        </InputGroup>
                        <ValidationMessage valid={this.state.passwordValid} message={this.state.errorMsg.password} />
                      </FormGroup>
                      <Row className="mt-5">
                        <Col xs="6">
                          <div className="checkbox form-check">
                            <input  type="checkbox" className="form-check-input" checked={this.state.isShowRemember} onChange={()=>this.changeRememberMe()}></input>
                            { global.isShowArabicLanguage==false &&  <label  className="form-check-label form-check-label"  onClick={()=>this.changeRememberMe()}> Remember me</label>}
                            { global.isShowArabicLanguage==true &&  <label  className="form-check-label form-check-label"  onClick={()=>this.changeRememberMe()}> حفظ تسجيل الدخول</label>}
                          </div>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button type="submit" color="primary" className="px-4" disabled={!this.state.formValid}>Login</Button>
                        </Col>
                      </Row>
                      <div className="text-center mt-5">
                        { global.isShowArabicLanguage==false &&  <Button color="link" className="px-0 text-light-grey text-underline" onClick={()=>this.forgotPasswordModel()}>Forgot password?</Button>}
                        { global.isShowArabicLanguage==true &&  <Button color="link" className="px-0 text-light-grey text-underline" onClick={()=>this.forgotPasswordModel()}>نسيت كلمة المرور</Button>}
                      </div>
                    </Form>
                  </CardBody>
                </Card>
                
              </CardGroup>
            </Col>
          </Row>
          <div className="copyright">
                    <Link to="https://taboor.ae/">Terms of Use</Link>
          </div>
        </Container>
        <Modal isOpen={this.state.isForgotPassword} className={'modal-lg ' + this.props.className}>
          <ModalHeader >Forgot Password</ModalHeader>
          <ModalBody>
          <FormGroup className="input-line">
            <Label className="text-light-grey">Email</Label> 
            <Input type="email" placeholder="Email"  name="email" id="exampleEmail"
            value={this.state.forgotEmail} onChange={(e) => this.updateForgotEmail(e.target.value)}/>
            < ValidationMessage valid={this.state.forgotEmailValid} message={this.state.errorForgotMsg.forgotEmail} />
          </FormGroup>
          </ModalBody>
          <ModalFooter>
          <Button outline color="primary" onClick={()=>this.closeForgetPasswordModel()}>Cancel</Button>
            <Button color="primary" disabled={!this.state.formForgetValid} onClick={()=>this.forgotPassword()} >Save</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default Login;
