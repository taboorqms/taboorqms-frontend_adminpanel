import * as firebase from "firebase/app";
import "firebase/messaging";
const firebaseConfig = {
  apiKey: "AIzaSyC_Pl5udaAwtATdcxsp5DyOXLlyyUDqm9w",
  authDomain: "taboorqms-ca965.firebaseapp.com",
  databaseURL: "https://taboorqms-ca965.firebaseio.com",
  projectId: "taboorqms-ca965",
  storageBucket: "taboorqms-ca965.appspot.com",
  messagingSenderId: "580234484527",
  appId: "1:580234484527:web:6770d17f0ff9c99b5c5fda",
  measurementId: "G-X9HGCYJ3Z9"
};
const initializedFirebaseApp = firebase.initializeApp(firebaseConfig);
const messaging = initializedFirebaseApp.messaging();
messaging.usePublicVapidKey(
  // Project Settings => Cloud Messaging => Web Push certificates
  "BLFEVk3WFc2mrbhuQRZ3gmKAyYtTOvjxixxYJadiPGfd1X8-S6dtS43e8-Qc2WZQGXn5u8Fpk08SgVax4acImt8"
);
export { messaging };
