
import React, { Component } from 'react';
import { BrowserRouter, HashRouter, Route, Switch } from 'react-router-dom';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import { messaging } from "./init-fcm";
import './App.scss';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;
const Home = React.lazy(() => import('./components/home'));
const Landing = React.lazy(() => import('./components/landing'));
const Login = React.lazy(() => import('./components/login'));
const Register = React.lazy(() => import('./components/register'));
const SuperAdminRegister = React.lazy(() => import('./components/register2'));
// const paymentSummary = React.lazy(() => import('./components/paymentSummary'));
function App() {
  // async componentDidMount() {
    messaging.requestPermission()
      .then(async function () {
        const token = await messaging.getToken();
        global.deviceToken = token;
        console.log("==========deviceToken", token);
      })
      .catch(function (err) {
        console.log("Unable to get permission to notify.", err);
      });
  // };
  return (
      <HashRouter>
          <React.Suspense fallback={loading()}>
            <Switch>
            <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
            <Route exact path="/landing" name="Landing Page" render={props => <Landing {...props}/>} />
            <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
            <Route exact path="/register2" name="Register 2" render={props => <SuperAdminRegister {...props}/>} />
            <Route path="/" name="Home" render={props => <Home {...props}/>} />
            </Switch>
          </React.Suspense>
          <NotificationContainer/>
      </HashRouter>
    );
}

export default App;
