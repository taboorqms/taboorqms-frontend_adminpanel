importScripts("https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js");
const config = {
  apiKey: "AIzaSyC_Pl5udaAwtATdcxsp5DyOXLlyyUDqm9w",
  authDomain: "taboorqms-ca965.firebaseapp.com",
  databaseURL: "https://taboorqms-ca965.firebaseio.com",
  projectId: "taboorqms-ca965",
  storageBucket: "taboorqms-ca965.appspot.com",
  messagingSenderId: "580234484527",
  appId: "1:580234484527:web:6770d17f0ff9c99b5c5fda",
  measurementId: "G-X9HGCYJ3Z9"
};
firebase.initializeApp(config);
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  const notificationTitle = payload.data.title;
  const notificationOptions = {
    body: payload.data.body,
    icon: '/firebase-logo.png'
  };
  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});

self.addEventListener('notificationclick', event => {
  console.log(event)
  return event;
});
